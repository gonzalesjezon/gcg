@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Applicant on Boarding</h2>
    </div>

    <!-- Assumption Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create an applicant on boarding in the form below.</span></div>
                <div class="card-body">
                    @include('boarding_applicant._form', [
                        'action' => ['BoardApplicantController@update', $boarding->id],
                        'method' => 'PATCH',
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
