@extends('layouts.print')

@section('css')
@endsection

@section('content')
  <div class="row mb-4">
    <div class="col-sm-3"><img src="{{URL::asset('img/pcc-logo-small.png')}}" class="img-fluid" alt="pcc logo" style="height: 120px;" /></div>
  </div>

  <div class="dropdown-divider"></div>

  <div class="row mb-1">
      <div class="col-2"><b>Position</b></div>
      <div class="col-2"><b>{{$jobs->title}}</b></div>
  </div>
  <div class="row mb-1">
      <div class="col-2"><b>Job Grade</b></div>
      <div class="col-2"><b>{{$jobs->grade}}</b></div>
  </div>
  <div class="row mb-1">
      <div class="col-2"><b>Item Number</b></div>
      <div class="col-2"><b>{{$jobs->plantilla_item_number}}</b></div>
  </div>
  <div class="row mb-1">
      <div class="col-2"><b>Dvision/Office</b></div>
      <div class="col-2"><b>{{config('params.division.'.$jobs->division)}}</b></div>
  </div>
  <div class="row mb-1">
      <div class="col-2"><b>Qualification Standards</b></div>
  </div>
  <div class="row mb-1">
      <div class="col-2" style="text-indent: 20px;">Education</div>
      <div class="col-2"><b>{{$jobs->education}}</b></div>
  </div>
  <div class="row mb-1">
      <div class="col-2" style="text-indent: 20px;">Experience</div>
      <div class="col-2"><b>{{$jobs->experience}}</b></div>
  </div>
  <div class="row mb-1">
      <div class="col-2" style="text-indent: 20px;">Training</div>
      <div class="col-2"><b>{{$jobs->training}}</b></div>
  </div>
  <div class="row mb-1">
      <div class="col-2" style="text-indent: 20px;">Eligibility</div>
      <div class="col-2"><b>{{$jobs->elgibility}}</b></div>
  </div>

  <div class="row mb-1 text-center">
    <div class="col-sm-12"><h4 style="text-decoration: underline;"><b>COMPARATIVE RANKING SUMMARY</b></h4></div>
  </div>

  <table id="table1" class="table table-striped table-hover table-fw-widget table-bordered">
  <thead>
  <tr>
    <th rowspan="2">NAME</th>
    <th colspan="2" scope="colgroup">PERFORMANCE</th>
    <th colspan="3" scope="colgroup">EDUCATION AND TRAINING</th>
    <th colspan="5" scope="colgroup">EXPERIENCE AND OUTSTANDING ACCOMPLISHMENTS</th>
    <th colspan="2" scope="colgroup">PYSCHOSOCIAL</th>
    <th colspan="2" scope="colgroup">POTENTIAL</th>
  </tr>
  <tr>
    <th>POINTS</th>
    <th scope="col">40%</th>
    <th scope="col">POINTS(Education)+</th>
    <th scope="col"><i>POINTS(Training)</i></th>
    <th scope="col">20%</th>
    <th colspan="2" scope="col">POINTS(Relevant)+</th>
    <th scope="col"><i>POINTS <br> (Specialized)</i></th>
    <th scope="col"></th>
    <th scope="col">20%</th>
    <th scope="col">POINTS</th>
    <th scope="col">10%</th>
    <th scope="col">POINTS</th>
    <th scope="col">10%</th>
  </tr>
  </thead>
  <tbody>
    @foreach($evaluations as $key => $evaluation)
    <tr>
      <td>{{$evaluation->applicant->getFullname()}}</td>
      <td>{{$evaluation->performance_score}}</td>
      <td>{{$evaluation->performance_percent}}</td>
      <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</td>
      <td>{{$evaluation->education_training_total_points}}</td>
      <td>{{$evaluation->education_training_score}}</td>
      <td>{{$evaluation->experience_accomplishments_total_points}}</td>
      <td></td>
      <td></td>
      <td></td>
      <td>{{$evaluation->experience_accomplishments_score}}</td>
      <td>{{$evaluation->psychosocial_percentage_rating}}</td>
      <td>{{$evaluation->psychosocial_score}}</td>
      <td>{{$evaluation->psychosocial_percentage_rating}}</td>
      <td>{{$evaluation->psychosocial_score}}</td>
    </tr>
  @endforeach
  </tbody>
</table>

<div class="row mb-1">
  <div class="col">
    Reference (based n Merit Selection Plan):
  </div>
</div>

<div class="row mb-1">
  <div class="col col-sm-2">Performance</div>
  <div class="col col-sm-1">40%</div>
  <div class="col-sm-9">
    indicates the employee's efficiency in discharging his/her duties and responsibilities.
  </div>
</div>

<div class="row mb-2">
  <div class="col col-sm-2">Education & Training</div>
  <div class="col col-sm-1">20%</div>
  <div class="col-sm-9">
    includes educational background, trainings/seminars attended relevant to the duties of the position to be filled. To
    enhance the effectiveness of the employee in achieving objectives and goals, PCC gives credit to candidates with
    credentials in higher education. However, only relevant educational degree or units/trainings in excess of minimum
    requirements shall be given extra points. For extra points, only the trainings/seminars for the least five(5) years
    are credited and not to exceed ten(10) points.
  </div>
</div>

<div class="row mb-2">
  <div class="col col-sm-2">Experience</div>
  <div class="col col-sm-1">20%</div>
  <div class="col-sm-9">
    refers to occupational history, work experience related/relevant to the area of knowledge or activity required for
    the
    position.
  </div>
</div>

<div class="row mb-2">
  <div class="col col-sm-2">Psychosocial Attributes</div>
  <div class="col col-sm-1">10%</div>
  <div class="col-sm-9">
    refers to the characteristics or traits of a person which involve both physical and social aspects. Pyschological
    attributes include the way the applicant/candidate perceives things, ideas, beliefs and understanding and how the
    applicant/candidate acts and relates them to others and social situations. This shall be measured in the panel
    interview to be conducted by the internal selection committee.
  </div>
</div>

<div class="row mb-2">
  <div class="col col-sm-2">Potential</div>
  <div class="col col-sm-1">10%</div>
  <div class="col-sm-9">
    refers to the employee's capacity to perform the duties and assume the responsibilities of the position to be filled
    and those of higher positions. This shall be measured in the panel interview to be conducted by the internal
    selection
    committee.
  </div>
</div>

<div class="row mb-8">
  <div class="col col-sm-2">Total</div>
  <div class="col col-sm-1">100%</div>
</div>

<div class="row mb-4">
  <div class="col">Evaluated By:</div>
</div>

<div class="form-group row text-center">
  <div class="col-3">
    <hr>
    ISC Chairperson
  </div>
  <div class="col-3">
    <hr>
    ISC Member
  </div>
  <div class="col-3">
    <hr>
    ISC Member
  </div>
  <div class="col-3">
    <hr>
    EA Representative
  </div>
</div>

  <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection