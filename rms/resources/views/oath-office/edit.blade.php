@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Oath of Office</h2>
    </div>

    <!-- Assumption Form -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header card-header-divider">
                    <span class="card-subtitle">You can create an oath of office in the form below.</span></div>
                <div class="card-body">
                    @include('oath-office._form', [
                        'action' => ['OathOfficeController@update', $oathoffice->id],
                        'method' => 'PATCH',
                        'oathoffice' => $oathoffice
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection
