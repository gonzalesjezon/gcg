@section('css')
   <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/select2/css/select2.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/bootstrap-slider/css/bootstrap-slider.min.css') }}">
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'evaluation-form']) !!}

<div class="form-group row">
   {{ Form::label('', 'Applicant Name', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-5">
        {{ Form::label('', $joboffer->applicant->getFullName(), ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-2 col-form-label text-sm-right"> Date </label>
    <div class="col-12 col-sm-7 col-md-5 col-lg-4 col-xl-3">
        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
            <input size="16" type="text" value="{{ date('Y-m-d',time()) }}" name="created_at"
                   class="form-control form-control-sm"
                   placeholder="Date Now"
            >
            <div class="input-group-append">
                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
            </div>
        </div>
    </div>
</div>

@if($status == 'plantilla')
    <div class="form-group row">
        {{ Form::label('personal_economic_relief_allowance', 'PERA', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
        <div class="col-5">
            {{ Form::text('pera_amount', $joboffer->pera_amount, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Personal Economic Relief Allowance',
                    'required' => 'true'
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('clothing_allowance', 'Clothing Allowance', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
        <div class="col-5">
            {{ Form::text('clothing_allowance', $joboffer->clothing_allowance, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Clothing Allowance',
                    'required' => 'true'
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('year_end_bonus', 'Year End Bonus', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
        <div class="col-5">
            {{ Form::text('year_end_bonus', $joboffer->year_end_bonus, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Year End Bonus',
                    'required' => 'true'
                ])
            }}
        </div>
    </div>

    <div class="form-group row">
        {{ Form::label('cash_gift', 'Cash Gift', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
        <div class="col-5">
            {{ Form::text('cash_gift', $joboffer->cash_gift, [
                    'class' => 'form-control form-control-sm',
                    'placeholder' => 'Cash Gift',
                    'required' => 'true'
                ])
            }}
        </div>
    </div>
@endif

<div class="form-group row">
    {{ Form::label('executive_director', 'Executive Director', ['class'=>'col-12 col-sm-2 col-form-label text-sm-right']) }}
    <div class="col-5">
        {{ Form::text('executive_director', $joboffer->executive_director, [
                'class' => 'form-control form-control-sm',
                'placeholder' => 'Executive Director',
                'required' => 'true'
            ])
        }}
    </div>
</div>


<div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
        <input type="hidden" name="joboffer_id" value="{{ $joboffer->id }}">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>

{!! Form::close() !!}

@section('scripts')
    <!-- JS Libraries -->
    <script src="{{ URL::asset('beagle-assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/bootstrap-slider/bootstrap-slider.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('beagle-assets/lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        //initialize the javascript
        App.init();
        App.formElements();
        $('#evaluation-form').parsley(); // frontend validation
      });
    </script>
@endsection
