@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-6">
    <div class="col-sm-3">CS Form No. 4 <br> Revised 2017</div>
    <div class="col-sm-6"></div>
  </div>

  <div class="row mb-8">
  	<div class="col-sm-12 text-center">
  		<h4 class="font-weight-bold">Republic of the Philippines</h4>
  		<h4 class="font-weight-bold">E2E SOLUTIONS MANAGEMENT PHILS INC</h4>
  		<h3 class="font-weight-bold mt-8">CERTIFICATION OF ASSUMPTION TO DUTY</h3>
  	</div>
  </div>

  <div class="row mb-8">
  	<div class="col-sm-12">
  		<p class="text-justify" style="text-indent: 50px;">
  			This is to certify that Ms/Mr. <b>{!! $assumption->applicant->getFullName() !!}</b> has assumed
  			the duties and responsibilities as <b>{!! $assumption->applicant->job->psipop->position_title !!}</b> of  <b>{!! $assumption->applicant->job->psipop->division->name !!}</b> effective on <b>{!! date('F d, Y',strtotime($assumption->assumption_date)) !!}</b>
  		</p>

  		<p class="text-justify" style="text-indent: 50px;">
  			This certification is issued in connection with the issuance of the appointment of Ms/Mr. {!! $assumption->applicant->getFullName() !!} as {!! $assumption->applicant->job->psipop->position_title !!}
  		</p>

  		<p class="text-justify" style="text-indent: 50px;">
  			Done this {!! date('d',strtotime($assumption->assumption_date)) !!} day of {!! date('F',strtotime($assumption->assumption_date)) !!} in {!! date('Y',strtotime($assumption->assumption_date)) !!}
  		</p>

  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-6"></div>
  	<div class="col-sm-3"></div>
  	<div class="col-sm-3 text-center">{!! $assumption->head_of_office !!}</div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-6"></div>
  	<div class="col-sm-3"></div>
  	<div class="col-sm-3 text-center border-top border-dark">Head Office/Department/Unit</div>
  </div>

  <div class="row form-group mb-8">
  	<div class="col-sm-1 text-right">Date: </div>
    <div class="border-bottom border-dark col-sm-2 text-center">{!! $assumption->assumption_date !!}</div>
  </div>

  <div class="row mb-8">
  	<div class="col-sm-2 text-left">Attested By </div>
  </div>


  <div class="row mb-1">
  	<div class="col-sm-3 text-center">{!! $assumption->attested_by !!}</div>
  </div>

  <div class="row mb-8">
  	<div class="col-sm-3 text-center border-top border-dark">Highest Ranking HRMO</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-6" style="font-size: 11px;">
  		201 File <br>
  		Admin <br>
  		COA <br>
  		CSC
  	</div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-10"></div>
  	<div class="col-sm-2 border border-dark font-weight-bold">
  		<i>
  			For submission to CSCFO
  		with 30 days from the
  		date of assumption of the appointee
  		</i>
  	</div>
  </div>


</div>

  <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3 d-print-none">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection