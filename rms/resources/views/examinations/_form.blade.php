@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection

<div class="row">
    <div class="col-6">
		{!! Form::open(['action' => $actionPosition, 'method' => 'GET', 'id' => 'get-form']) !!}
        <div class="form-group row {{ $errors->has('job_id') ? 'has-error' : ''}}">
            {{ Form::label('job_id', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                <select name="job_id" id="job_id" class="form-control form-control-xs">
				      <option value="0">Select position</option>
				      @foreach($jobs as $job)
				      <option value="{{$job->id}}" {{ ($job->id == @$currentJob->id) ? 'selected' : '' }}>{{$job->title}}</option>
				      @endforeach
			    </select>
                {!! $errors->first('job_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
		{!! Form::close() !!}
    </div>
</div>

{!! Form::open(['action' => $action, 'method' => $method, 'id'=>'examination-form']) !!}
<input type="hidden" name="examination_id" value="{{ @$examination->id}}">
<div class="row">
	<div class="col-6">
		<div class="form-group row">
			{{ Form::label('', 'Applicant', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				<select name="applicant_id" class="form-control form-control-xs" id="applicant_id">
					<option value="0">Select applicant</option>
					@if(@$qualified)
						@foreach($qualified as $qualify)
						<option value="{{$qualify->id}}" data-email={{$qualify->email_address}} {{ ($qualify->id == @$examination->applicant_id) ? 'selected' : '' }}>{{ $qualify->getFullName()}}</option>
						@endforeach
					@endif
				</select>
			</div>
		</div>

		<div class="form-group row">
		    <label class="col-12 col-sm-3 col-form-label text-sm-right"> Examination Date </label>
		    <div class="col-12 col-sm-7 col-md-5 col-lg-6">
		        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
		            <input size="16" type="text" value="{{@$examination->exam_date}}" name="exam_date"
		                   class="form-control form-control-sm" >
		            <div class="input-group-append">
		                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Time', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('exam_time', @$examination->exam_time, [
					'class' => 'form-control form-control-sm',
					'required' => true
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Location', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('exam_location', @$examination->exam_location, [
					'class' => 'form-control form-control-sm',
					'required' => true
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Email Address', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('email', @$examination->applicant->email_address, [
					'id' => 'email',
					'class' => 'form-control form-control-sm',
					'readonly' => true
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Confirmation', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
              <div class="switch-button switch-button-success switch-button-yesno">
              	 @if(@$examination->confirmed == 1)
                  <input type="checkbox" name="confirmed" id="confirmed" checked="true"><span>
                 @else
                 <input type="checkbox" name="confirmed" id="confirmed"><span>
                 @endif
                 <label for="confirmed"></label></span>
              </div>
           </div>
		</div>
	</div>

	<div class="col-6">
		<div class="form-group row">
			<label class="col-12 col-sm-3 col-form-label text-sm-right font-weight-bold"> If Reschedule </label>
		</div>

		<div class="form-group row">
		    <label class="col-12 col-sm-3 col-form-label text-sm-right"> Date </label>
		    <div class="col-12 col-sm-7 col-md-5 col-lg-6">
		        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
		            <input size="16" type="text" value="{{ @$examination->resched_exam_date }}" name="resched_exam_date"
		                   class="form-control form-control-sm">
		            <div class="input-group-append">
		                <button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Time', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::text('resched_exam_time', @$examination->resched_exam_time, [
					'class' => 'form-control form-control-sm',
				]) }}
			</div>
		</div>


		<hr>

		<div class="form-group row">
			{{ Form::label('','Examination Status', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
				{{ Form::select('exam_status', config('params.examination_status'), @$examination->exam_status,[
					'class' => 'form-control form-control-xs',
					'placeholder' => 'Select examination status'
				]) }}
			</div>
		</div>

		<div class="form-group row">
			{{ Form::label('','Notify Applicant', ['class' => 'col-12 col-sm-3 col-form-label text-sm-right']) }}
			<div class="col-12 col-sm-8 col-lg-6">
              <div class="switch-button switch-button-success switch-button-yesno">
              	 @if(@$examination->notify == 1)
                  <input type="checkbox" name="notify" id="notify" checked="true"><span>
                 @else
                 <input type="checkbox" name="notify" id="notify"><span>
                 @endif
                 <label for="notify"></label></span>
              </div>
           </div>
		</div>
	</div>
</div>


<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    @include('examinations._form-script')
@endsection
