@extends('layouts.app')

@section('css')
  <link rel="stylesheet"
        type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" />
@endsection

@section('content')
  <div class="page-head">
    <h2 class="page-head-title">Itemization & Plantilla of Personnel</h2>
  </div>
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-table">
          <div class="card-header">
            <a href="{{ route('psipop.create') }}"
               class="btn btn-space btn-primary"
               title="Add a vacant position">
              <i class="icon icon-left mdi mdi-account-add"></i> Add
            </a>
            <div class="tools dropdown">
              <span class="icon mdi mdi-download"></span>
              <a href="#" role="button" data-toggle="dropdown" class="dropdown-toggle">
                <span class="icon mdi mdi-more-vert"></span>
              </a>
              <div role="menu" class="dropdown-menu">
                <a href="#" class="dropdown-item">Action</a>
                <a href="#" class="dropdown-item">Another action</a>
                <a href="#" class="dropdown-item">Something else here</a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">Separated link</a>
              </div>
            </div>
          </div>
          <div class="card-body">
            <table id="table1" class="table table-striped table-hover table-fw-widget">
              <thead>
              <tr>
                <th>Actions</th>
                <th>Item Number</th>
                <th>Position Title</th>
                <th>Name of Incumbent</th>
                <th>SG/JG</th>
                <th>Employment Status</th>
                <th>Status</th>
                <th>Date Created</th>
              </tr>
              </thead>
              <tbody>
              @foreach($psipop as $value)
                <tr>
                  <td class="actions text-left">
                    <div class="tools">
                      <button type="button" data-toggle="dropdown"
                              class="btn btn-secondary dropdown-toggle"
                              aria-expanded="false">
                        <i class="icon icon-left mdi mdi-settings-square"> </i>
                        <span class="icon-dropdown mdi mdi-chevron-down"> </span>
                      </button>

                      <div role="menu" class="dropdown-menu" x-placement="bottom-start">

                        <a href="{{ route('psipop.edit', ['id' => $value->id]) }}"
                           class="dropdown-item">
                          <i class="icon icon-left mdi mdi-edit"></i>Edit</a>
                        <div class="dropdown-divider"></div>

                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['psipop', $value->id],
                            'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="icon icon-left mdi mdi-delete mr-1"></i> Delete', [
                            'type' => 'submit',
                            'style' => 'color: #504e4e',
                            'class' => 'dropdown-item',
                            'title' => 'Delete PSIPOP',
                            'onclick'=>'return confirm("Confirm delete?")'
                        ])!!}
                        {!! Form::close() !!}
                      </div>
                    </div>
                  </td>
                  <td>{!! $value->item_number !!} </td>
                  <td>{!! $value->position_title !!} </td>
                  <td>{!! ($value->applicant) ? $value->applicant->getFullName() : '' !!}</td>
                  <td>{{ $value->salary_grade }}</td>
                  <td>{{ config('params.employee_status.'.$value->employee_status)}}</td>
                  <td>{{ ($value->status == 0) ? 'Unfilled' : 'Filled' }}</td>
                  <td class="center">{{ $value->created_at->diffForHumans() }}
                    / {{ $value->created_at->format('Y-m-d')}}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  @include('jobs._index-script')
@endsection
