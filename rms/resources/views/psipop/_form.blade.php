@section('css')
<link rel="stylesheet" type="text/css"
        href="{{ URL::asset('beagle-assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('beagle-assets/lib/summernote/summernote-bs4.css') }}" />
@endsection

{!! Form::open(['action' => $action, 'method' => $method, 'id' => 'psipop-form']) !!}


<div class="row">
    <div class="col-6">
        <div class="form-group {{ $errors->has('employee_status') ? 'has-error' : ''}} row">
            {{ Form::label('employee_status', 'Employee Status', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('employee_status', config('params.employee_status'),@$psipop->employee_status,[
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select employee status',
                        'required' => true,
                    ])
                }}
                {!! $errors->first('employee_status', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('position_title') ? 'has-error' : ''}}">
            {{ Form::label('position_title', 'Position Title', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('position_title', @$psipop->position_title, [
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                ])
                }}
                {!! $errors->first('position_title', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('item_number') ? 'has-error' : ''}}">
            {{ Form::label('item_number', 'Plantilla Item No', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('item_number', @$psipop->item_number, [
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                ])
                }}
                {!! $errors->first('item_number', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('office_id') ? 'has-error' : ''}}">
            {{ Form::label('office_id', 'Office', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('office_id', $offices, @$psipop->office_id,[
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select office',
                    ])
                }}
                {!! $errors->first('office_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('division_id') ? 'has-error' : ''}}">
            {{ Form::label('division_id', 'Division', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('division_id', $divisions, @$psipop->division_id,[
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select division',
                        'required' => true,
                    ])
                }}
                {!! $errors->first('division_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>

    <div class="col-6">
    	<div class="form-group row {{ $errors->has('department_id') ? 'has-error' : ''}}">
            {{ Form::label('department_id', 'Department', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('department_id', $departments, @$psipop->department_id,[
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select department',
                    ])
                }}
                {!! $errors->first('department_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('section_id') ? 'has-error' : ''}}">
            {{ Form::label('section_id', 'Section', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('section_id', $sections, @$psipop->section_id,[
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select section',
                    ])
                }}
                {!! $errors->first('section_id', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('ppa_attribution') ? 'has-error' : ''}}">
            {{ Form::label('ppa_attribution', 'PPA Attribution', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('ppa_attribution', @$psipop->ppa_attribution, [
                    'class' => 'form-control form-control-sm',
                    'required' => true,
                ])
                }}
                {!! $errors->first('ppa_attribution', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>
</div>

<hr>

<div class="row">
	<div class="col-6">
    	<div class="form-group row {{ $errors->has('salary_grade') ? 'has-error' : ''}}">
            {{ Form::label('salary_grade', 'Salary Grade', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('salary_grade', config('params.salary_grade'), @$psipop->salary_grade,[
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select salary grade',
                    ])
                }}
                {!! $errors->first('salary_grade', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>

        <div class="form-group row {{ $errors->has('step') ? 'has-error' : ''}}">
            {{ Form::label('step', 'Step', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::select('step', config('params.step'), @$psipop->step,[
                        'class' => 'form-control form-control-xs',
                        'placeholder' => 'Select step',
                    ])
                }}
                {!! $errors->first('step', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
            </div>
        </div>
    </div>

    <div class="col-6">
    	<div class="form-group row {{ $errors->has('annual_authorized_salary') ? 'has-error' : '' }}">
    		{{ Form::label('annual_authorized_salary', 'Annual Authorized Salary', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
    		<div class="col-12 col-sm-8 col-lg-6">
    			{{ Form::text('annual_authorized_salary', @$psipop->annual_authorized_salary, [
    				'class' => 'form-control form-control-sm',
    				'required' => true
    			]) }}
    			{!! $errors->first('annual_authorized_salary', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    		</div>
    	</div>

    	<div class="form-group row {{ $errors->has('annual_actual_salary') ? 'has-error' : '' }}">
    		{{ Form::label('annual_actual_salary', 'Annual Actual Salary', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
    		<div class="col-12 col-sm-8 col-lg-6">
    			{{ Form::text('annual_actual_salary', @$psipop->annual_actual_salary, [
    				'class' => 'form-control form-control-sm',
    				'required' => true
    			]) }}
    			{!! $errors->first('annual_actual_salary', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    		</div>
    	</div>
    </div>
</div>

<hr>

<div class="row">
	<div class="col-6">
		<div class="form-group row">
            {{ Form::label('', 'Name of Incumbent', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', (@$psipop->applicant) ?  @$psipop->applicant->getFullName() : '', [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Sex', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', strtoupper(@$psipop->applicant->gender), [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Date of Birth', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', date('Y-m-d',strtotime(@$psipop->applicant->birthday)), [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'TIN', ['class'=>'col-12 col-sm-3 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', @$psipop->applicant->tin, [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true
                ])
                }}
            </div>
        </div>
	</div>

	<div class="col-6">
		<div class="form-group row">
            {{ Form::label('', 'Date of Original Appointment', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true
                ])
                }}
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('', 'Date of Last Promotion', ['class'=>'col-12 col-sm-4 col-form-label text-sm-right']) }}
            <div class="col-12 col-sm-8 col-lg-6">
                {{ Form::text('', '', [
                    'class' => 'form-control form-control-sm',
                    'readOnly' => true
                ])
                }}
            </div>
        </div>
	</div>
</div>


<div class="form-group row text-right">
    <div class="col col-sm-12 ">
        {{ Form::submit('Submit', ['id' => 'job-submit', 'class'=>'btn btn-primary btn-space']) }}
        {{ Form::reset('Clear Form', ['id'=>'clear-form', 'class'=>'btn btn-space btn-danger']) }}
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    @include('psipop._form-script')
@endsection
