{{--PRIMARY--}}
<div class="form-group row font-weight-bold">
  <div class="col-3 text-center">CAREER SERVICE/RA 1080(BOARD/BAR) UNDER SPECIAL LAWS/CES/CSEE BARANGAY
                                 ELIGIBILITY/DRIVER'S LICENSE
  </div>
  <div class="col-1 text-center">RATING(If Applicable)</div>
  <div class="col-2 text-center">DATE OF EXAMINATION/CONFERMENT</div>
  <div class="col-3 text-center">PLACE OF EXAMINATION/CONFERMENT</div>
  <div class="col-2 text-center">LICENSE(If Applicable)</div>
</div>

<div class="row">
  <div class="col-12 text-right">
    <a href="#" id="add_eligibility" class="btn btn-sm btn-info">Add</a>
  </div>
</div>

<div class="form-group row eligibility">
  <input type="hidden" name="eligibility[0][applicant_id]" >
  <div class="col-3 mt-4">
    {{ Form::text('eligibility[0][name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[0][name]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 mt-4">
    {{ Form::text('eligibility[0][rating]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[0][rating]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center font-weight-bold mt-4">
    {{ Form::text('eligibility[0][exam_date]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[0][exam_date]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 text-center font-weight-bold mt-4">
    {{ Form::text('eligibility[0][exam_place]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[0][exam_place]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center">
    NUMBER
    {{ Form::text('eligibility[0][license_number]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[0][license_number]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center">
    Date of Validity
    {{ Form::text('eligibility[0][license_validity]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[0][license_validity]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>

<div class="form-group row text-right">
  <div class="col-12">
    {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
    {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>