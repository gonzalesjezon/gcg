@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div class="reports" style="width: 760px;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">

	<div class="row mb-2">
		<div class="col-12">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<td>
							<img src="{{ asset('img/report-logo.png') }}" height="80px;">
						</td>
						<td colspan="5" class="text-center">
							<h3>NOTICE OF VACANCY</h3>
						</td>
					</tr>
					<tr class="text-center">
						<th>Subject</th>
						<th colspan="5" >Publication of Vacant Position</th>
					</tr>
					<tr class="text-center">
						<th>Position</th>
						<th>Item Number</th>
						<th>Salary</th>
						<th>Status of Employment</th>
						<th>Reporting Line</th>
						<th>Station</th>
					</tr>
				</thead>
				<tbody>
					<tr class="text-center">
						<td >{!! $jobs->psipop->position_title !!}</td>
						<td >{!! $jobs->psipop->item_number !!}</td>
						<td>
							{{ config('params.salary_grade.'.$jobs->psipop->salary_grade) }} <br>
                          	{{ number_format($jobs->monthly_basic_salary,2)}} / Monthly <br>
                         	{{ number_format($jobs->pera_amount,2) }} / PERA
						</td>
						<td>{{ config('params.employee_status.'.$jobs->psipop->employee_status)}}</td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<table class="table table-striped table-bordered">
				<thead>
					<tr class="text-center">
						<th colspan="4" >PREFERED QUALIFICATION</th>
					</tr>
					<tr class="text-center">
						<th>EDUCATION</th>
						<th>TRAINING</th>
						<th>WORK EXPERIENCE</th>
						<th>ELIGIBILITY</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{!! $jobs->education !!}</td>
						<td>{!! $jobs->training !!}</td>
						<td>{!! $jobs->experience !!}</td>
						<td>{!! $jobs->eligibility !!}</td>
					</tr>
					<tr>
						<td colspan="4"> <b>Duties & Responsibilities</b></td>
					</tr>
					@if($jobs->compentency_1)
					<tr>
						<td colspan="4">
							<b>A .General Functions</b>: <br>
							<div class="pl-2 text-justify">{!! $jobs->compentency_1 !!}</div>
						</td>
					</tr>
					@endif

					@if($jobs->compentency_2)
					<tr>
						<td colspan="4">
							<b>B. Specific Duties & Responsibilities</b>: <br>
							<div class="pl-2 text-justify">{!! $jobs->compentency_2 !!}</div>
						</td>
					</tr>
					@endif

					@if($jobs->compentency_3)
					<tr>
						<td colspan="4">
							<b>C. Secondary Functions</b>: <br>
							<div class="pl-2 text-justify">{!! $jobs->compentency_3 !!}</div>
						</td>
					</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<div class="mb-1">JOIN THE DEDICATED AND HARDWORKING WORKFORCE AND FAMILIY OF PHILIPPINE COMMISSION ON WOMEN PCW</div>
			<div class="mb-2">Interested and qualified applicants must fill out the application form through by clicking the "Apply Button"</div>

			<div class="mb-1">
			<b>Please also attach following documents through the link on or before {{ date('F, d Y',strtotime($jobs->deadline_date)) }} The assessment process will start the following day</b>
			<ol>
              <li>Fully Accomplished Personal Data Sheet (revised 2017); </li>
              <li>Curriculum Vitae with detailed job description;  </li>
              <li>Copy of Transcript of Records and Diploma; </li>
              <li>Copy of Certificate of training/seminars attended; </li>
              <li>Copy of Certificate of Employment  </li>
              <li>Performance Evaluation (if applicable) </li>
              <li>Certificate of eligibility/rating/license (if applicable)  </li>
            </ol>
            <b>Address your application letter to:</b>

            <div class="mt-5">
	            <span class="font-weight-bold">RAYMOND JAY L. MAZON</span> <br>
		          Supervising Gender and Development Specialists <br>
		          Head, Human Resource Management and Development Section <br>
		          Philippine Commission on Women <br>
		          1145 JP Laurel St. San Miguel Manila
	            </div>
			</div>

			<div class="mt-5">
				<p class="text-justify" style="text-indent: 25px;">
					This Office highly encourage all interested and qualified applicants including persons with disablity (PWD)
					members of indigenous communities, and those from any sexual orientation and gender identities (SOGI)
					Philippine Commission on Women (PCW) comiles with Equal Employment Opportunity Policy (EEOP)
					and that No person with disability shall be denied access to opportunities for suitable employment. A
					qualified employee with disability shall be a subject to the same terms conditions of employment and the
					same compensation privilleges, benefits, fringe benefits, incentives or allowances as a qualified able-bodied
					person.
				</p>
			</div>
		</div>
	</div>

</div>


@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection