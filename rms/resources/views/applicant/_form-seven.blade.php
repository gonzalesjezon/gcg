{{--PRIMARY--}}
<div class="form-group row font-weight-bold">
    <div class="col-2 text-center">INCLUSIVE DATES (mm/dd/yyyy)</div>
    <div class="col-3 text-center">POSITION TITLE</div>
    <div class="col-3 text-center">DEPARTMENT/AGENCY/OFFICE/COMPANY</div>
    <div class="col-1 text-center">MONTHLY SALARY</div>
    <div class="col-1 text-center"><small><strong>SALARY/JOB/PAY GRADE(If Applicable) & STEP(Format '00-0')/INCREMENT</strong></small></div>
    <div class="col-1 text-center">STATUS OF APPOINTMENT</div>
    <div class="col-1 text-center">GOV'T SERVICE (Y/N)</div>
</div>

<div class="row">
  <div class="col-12 text-right">
    <a href="#" id="add_workexperience" class="btn btn-sm btn-info">Add</a>
  </div>
</div>

@if(count($applicant->workexperience) > 0)

@foreach($applicant->workexperience as $key => $value)
<input type="hidden" name="work_experience[{{$key}}][id]" value="{{$value->id}}">
<div class="form-group row work_experience">
    <div class="col-1 text-center">
        FROM
        <input type="text" name="work_experience[{{$key}}][inclusive_date_from]" class="form-control form-control-sm" value="{{$value->inclusive_date_from}}">

        {!! $errors->first('work_experience[$key][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center">
        TO
        <input type="text" name="work_experience[{{$key}}][inclusive_date_to]" class="form-control form-control-sm" value="{{$value->inclusive_date_to}}">

        {!! $errors->first('work_experience[$key][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-3 text-center font-weight-bold">
        (Write in full/Do not abbreviate)
        <input type="text" name="work_experience[{{$key}}][position_title]" class="form-control form-control-sm" value="{{$value->position_title}}">

        {!! $errors->first('work_experience[$key][position_title]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-3 text-center font-weight-bold">
        (Write in full/Do not abbreviate)
        <input type="text" name="work_experience[{{$key}}][department]" class="form-control form-control-sm" value="{{$value->department}}">

        {!! $errors->first('work_experience[$key][department]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center mt-4">
        <input type="text" name="work_experience[{{$key}}][monthly_salary]" class="form-control form-control-sm" value="{{$value->monthly_salary}}">

        {!! $errors->first('work_experience[$key][monthly_salary]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center mt-4">
        <input type="text" name="work_experience[{{$key}}][salary_grade]" class="form-control form-control-sm" value="{{$value->salary_grade}}">

        {!! $errors->first('work_experience[$key][salary_grade]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center mt-4">
        <input type="text" name="work_experience[{{$key}}][status_of_appointment]" class="form-control form-control-sm" value="{{$value->status_of_appointment}}">

        {!! $errors->first('work_experience[$key][status_of_appointment]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center mt-4">
        <input type="text" name="work_experience[{{$key}}][govt_service]" class="form-control form-control-sm" value="{{$value->govt_service}}">

        {!! $errors->first('work_experience[$key][govt_service]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>
@endforeach
@else
<div class="form-group row work_experience">
    <div class="col-1 text-center">
        FROM
        {{ Form::text('work_experience[1][inclusive_date_from]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center">
        TO
        {{ Form::text('work_experience[1][inclusive_date_to]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-3 text-center font-weight-bold">
        (Write in full/Do not abbreviate)
        {{ Form::text('work_experience[1][position_title]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][position_title]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-3 text-center font-weight-bold">
        (Write in full/Do not abbreviate)
        {{ Form::text('work_experience[1][department]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][department]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center mt-4">
        {{ Form::text('work_experience[1][monthly_salary]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][monthly_salary]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center mt-4">
        {{ Form::text('work_experience[1][salary_grade]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][salary_grade]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center mt-4">
        {{ Form::text('work_experience[1][status_of_appointment]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][status_of_appointment]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
    <div class="col-1 text-center mt-4">
        {{ Form::text('work_experience[1][govt_service]', '', [
                'class' => 'form-control form-control-sm',
            ])
        }}
        {!! $errors->first('work_experience[1][govt_service]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
    </div>
</div>
@endif


<div class="form-group row text-right">
    <div class="col-12">
        {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
        {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
    </div>
</div>