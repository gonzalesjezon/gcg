{{--PRIMARY--}}
<div class="form-group row font-weight-bold">
  <div class="col-4 text-center">TITLE OF LEARNING AND DEVELOPMENT INTERVENTIONS/TRAINING PROGRAMS</div>
  <div class="col-2 text-center">INCLUSIVE DATES OF ATTENDANCE</div>
  <div class="col-1 text-center">NUMBER OF HOURS</div>
  <div class="col-2 text-center">TYPE OF LD</div>
  <div class="col-3 text-center">CONDUCTED/SPONSORED BY</div>
</div>
<div class="row">
  <div class="col-12 text-right">
    <a href="#" id="add_training" class="btn btn-sm btn-info">Add</a>
  </div>
</div>
@if(count($applicant->training) > 0)

@foreach($applicant->training as $key => $value)
<input type="hidden" name="training[{{$key}}][id]" value="{{$value->id}}">
<div class="form-group row training">
  <div class="col-3 text-center">
    (Write in full/Do no abbreviate)
    <input type="text" name="training[{{$key}}][title_learning_programs]" class="form-control form-control-sm" value="{{$value->title_learning_programs}}">
    {!! $errors->first('training[1][title_learning_programs]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center">
    FROM
    <input type="text" name="training[{{$key}}][inclusive_date_from]" class="form-control form-control-sm" value="{{$value->inclusive_date_from}}">
    {!! $errors->first('training[1][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    TO
    <input type="text" name="training[{{$key}}][inclusive_date_to]" class="form-control form-control-sm" value="{{$value->inclusive_date_to}}">
    {!! $errors->first('training[1][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold mt-4">
    <input type="text" name="training[{{$key}}][number_hours]" class="form-control form-control-sm" value="{{$value->number_hours}}">
    {!! $errors->first('training[1][number_hours]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 text-center">
    <span style="font-size: 11px;">(Managerial/Supervisor/Technical/etc.)</span>
    <input type="text" name="training[{{$key}}][ld_type]" class="form-control form-control-sm" value="{{$value->ld_type}}">
    {!! $errors->first('training[1][ld_type]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 text-center">
    (Write in full/Do no abbreviate)
    <input type="text" name="training[{{$key}}][sponsored_by]" class="form-control form-control-sm" value="{{$value->sponsored_by}}">
    {!! $errors->first('training[1][sponsored_by]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>
@endforeach
@else
<div class="form-group row training">
  <div class="col-3 text-center">
    (Write in full/Do no abbreviate)
    {{ Form::text('training[1][title_learning_programs]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][title_learning_programs]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center">
    FROM
    {{ Form::text('training[1][inclusive_date_from]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][inclusive_date_from]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    TO
    {{ Form::text('training[1][inclusive_date_to]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][inclusive_date_to]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold mt-3">
    {{ Form::text('training[1][number_hours]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][number_hours]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 text-center">
    <span style="font-size: 11px;">(Managerial/Supervisor/Technical/etc.)</span>
    {{ Form::text('training[1][ld_type]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][ld_type]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 text-center">
    (Write in full/Do no abbreviate)
    {{ Form::text('training[1][sponsored_by]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('training[1][sponsored_by]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>
@endif


<div class="form-group row text-right">
  <div class="col-12">
    {{ Form::button('Previous', ['class'=>'btn btn-secondary btn-space wizard-previous', 'data-wizard' => '#wizard1']) }}
    {{ Form::submit('Save', ['class'=>'btn btn-space btn-primary']) }}
    {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>