{{--PRIMARY--}}
<div class="form-group row font-weight-bold">
  <div class="col-2 offset-1 text-center">NAME OF SCHOOL (Write in Full)</div>
  <div class="col-2 offset-1">DEGREE/COURSE (Write in Full)</div>
  <div class="col-2 text-center">Period of Attendance</div>
  <div class="col-1">Highest Level/Units Earned</div>
  <div class="col-1">Year Graduated</div>
  <div class="col-2">Scholarship/Academic Honors Received</div>
</div>

@if(count($applicant->education) > 0)

@foreach($applicant->education as $key => $educ)

@if($educ->educ_level == 1)
<div class="form-group row">
  <div class="col-form-label col-1 font-weight-bold mt-4">
    Primary
  </div>
  <input type="hidden" name="primary[0][id]" value="{{$educ->id}}">
  <input type="hidden" name="primary[0][applicant_id]" value="{{$educ->applicant_id}}">
  <div class="col-3 mt-4">
    {{ Form::text('primary[0][school_name]', $educ->school_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 mt-4">
    {{ Form::text('primary[0][course]', $educ->course, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    From
    {{ Form::text('primary[0][attendance_from]', $educ->attendance_from, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    To
    {{ Form::text('primary[0][attendance_to]', $educ->attendance_to, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 mt-4">
    {{ Form::text('primary[0][level]', $educ->level, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 mt-4">
    {{ Form::text('primary[0][graduated]', $educ->graduated, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 mt-4">
    {{ Form::text('primary[0][awards]', $educ->awards, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>
@endif

@if($educ->educ_level == 2)

{{--SECONDARY--}}
<input type="hidden" name="secondary[0][id]" value="{{$educ->id}}">
<input type="hidden" name="secondary[0][applicant_id]" value="{{$applicant->id}}">
<div class="form-group row">
  {{ Form::label('primary_name', 'Secondary', ['class'=>'col-form-label col-1 font-weight-bold col']) }}
  <div class="col-3">
    {{ Form::text('secondary[0][school_name]', $educ->school_name, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2">
    {{ Form::text('secondary[0][course]', $educ->course, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('secondary[0][attendance_from]', $educ->attendance_from, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('secondary[0][attendance_to]', $educ->attendance_to, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1">
    {{ Form::text('secondary[0][level]', $educ->level, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1">
    {{ Form::text('secondary[0][graduated]', $educ->graduated, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2">
    {{ Form::text('secondary[0][awards]', $educ->awards, [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>

@endif
@endforeach

@else

<div class="form-group row">
  <div class="col-form-label col-1 font-weight-bold mt-4">
    Primary
  </div>
  <input type="hidden" name="primary[0][applicant_id]" value="{{$applicant->id}}">
  <div class="col-3 mt-4">
    {{ Form::text('primary[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 mt-4">
    {{ Form::text('primary[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    From
    {{ Form::text('primary[0][attendance_from]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    To
    {{ Form::text('primary[0][attendance_to]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 mt-4">
    {{ Form::text('primary[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 mt-4">
    {{ Form::text('primary[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 mt-4">
    {{ Form::text('primary[0][awards]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>

{{--SECONDARY--}}
<input type="hidden" name="secondary[0][applicant_id]" value="{{$applicant->id}}">
<div class="form-group row">
  {{ Form::label('primary_name', 'Secondary', ['class'=>'col-form-label col-1 font-weight-bold col']) }}
  <div class="col-3">
    {{ Form::text('secondary[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2">
    {{ Form::text('secondary[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('secondary[0][attendance_from]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('secondary[0][attendance_to]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1">
    {{ Form::text('secondary[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1">
    {{ Form::text('secondary[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2">
    {{ Form::text('secondary[0][awards]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('XXXXX', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>
@endif

{{--VOCATIONAL--}}
<div class="row">
  <div class="offset-1 col-1">
    <a href="#" id="add_vocational" class="btn btn-sm btn-info">Add</a>
  </div>
</div>

@if(count($applicant->education) > 0)
<?php $vocational = ''; ?>
@foreach($applicant->education as $key => $educ)
  @if($educ->educ_level == 3)
  <input type="hidden" name="vocational[{{$key}}][id]" value="{{$educ->id}}">
  <input type="hidden" name="vocational[{{$key}}][applicant_id]" value="{{$applicant->id}}">
  <div class="form-group row vocational">
    <?php
      $vocational = ($vocational == '') ? 'Vocational' : '-';
    ?>
    {{ Form::label('primary_name', $vocational, ['class'=>'col-form-label col-1 font-weight-bold col']) }}
    <div class="col-3">
      <input type="text" name="vocational[{{$key}}][school_name]" class="form-control form-control-sm" value="{{$educ->school_name}}">
    </div>
    <div class="col-2">
      <input type="text" name="vocational[{{$key}}][course]" class="form-control form-control-sm" value="{{$educ->course}}">
    </div>
    <div class="col-1 text-center font-weight-bold">
      <input type="text" name="vocational[{{$key}}][attendance_from]" class="form-control form-control-sm" value="{{$educ->attendance_from}}">
    </div>
    <div class="col-1 text-center font-weight-bold">
      <input type="text" name="vocational[{{$key}}][attendance_to]" class="form-control form-control-sm" value="{{$educ->attendance_to}}">
    </div>
    <div class="col-1">
      <input type="text" name="vocational[{{$key}}][level]" class="form-control form-control-sm" value="{{$educ->level}}">
    </div>
    <div class="col-1">
      <input type="text" name="vocational[{{$key}}][graduated]" class="form-control form-control-sm" value="{{$educ->graduated}}">
    </div>
    <div class="col-2">
      <input type="text" name="vocational[{{$key}}][awards]" class="form-control form-control-sm" value="{{$educ->awards}}">
    </div>
  </div>
  @endif
@endforeach
@endif

@if(!in_array(3,$has_educ))
<input type="hidden" name="vocational[0][applicant_id]" value="{{$applicant->id}}">
<div class="form-group row vocational">
  {{ Form::label('primary_name', 'Vocational', ['class'=>'col-form-label col-1 font-weight-bold col']) }}
  <div class="col-3">
    {{ Form::text('vocational[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('vocational[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('vocational[0][attendance_from]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('vocational[0][attendance_to]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('vocational[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('vocational[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('vocational[0][awards]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
</div>
@endif

{{--COLLEGE--}}
<div class="row">
  <div class="offset-1 col-1">
    <a href="#" id="add_college" class="btn btn-sm btn-info">Add</a>
  </div>
</div>

@if(count($applicant->education) > 0)
<?php $college = ''; ?>
@foreach($applicant->education as $key => $educ)

@if($educ->educ_level == 4)
<input type="hidden" name="college[{{$key}}][id]" value="{{$educ->id}}">
<input type="hidden" name="college[{{$key}}][applicant_id]" value="{{$applicant->id}}">
<div class="form-group row college">
  <?php
    $college = ($college == '') ? 'College' : '-';
  ?>
  {{ Form::label('primary_name', $college, ['class'=>'col-form-label col-1 font-weight-bold col']) }}
  <div class="col-3">
    <input type="text" name="college[{{$key}}][school_name]" class="form-control form-control-sm" value="{{$educ->school_name}}">
  </div>
  <div class="col-2">
    <input type="text" name="college[{{$key}}][course]" class="form-control form-control-sm" value="{{$educ->course}}">
  </div>
  <div class="col-1 text-center font-weight-bold">
    <input type="text" name="college[{{$key}}][attendance_from]" class="form-control form-control-sm" value="{{$educ->attendance_from}}">
  </div>
  <div class="col-1 text-center font-weight-bold">
    <input type="text" name="college[{{$key}}][attendance_to]" class="form-control form-control-sm" value="{{$educ->attendance_to}}">
  </div>
  <div class="col-1">
    <input type="text" name="college[{{$key}}][level]" class="form-control form-control-sm" value="{{$educ->level}}">
  </div>
  <div class="col-1">
    <input type="text" name="college[{{$key}}][graduated]" class="form-control form-control-sm" value="{{$educ->graduated}}">
  </div>
  <div class="col-2">
    <input type="text" name="college[{{$key}}][awards]" class="form-control form-control-sm" value="{{$educ->awards}}">
  </div>
</div>
@endif

@endforeach
@endif

@if(!in_array(4,$has_educ))
<input type="hidden" name="college[0][applicant_id]" value="{{$applicant->id}}">
<div class="form-group row college">
  {{ Form::label('primary_name', 'College', ['class'=>'col-form-label col-1 font-weight-bold col']) }}
  <div class="col-3">
    {{ Form::text('college[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('college[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('college[0][attendance_from]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('college[0][attendance_to]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('college[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('college[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('college[0][awards]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
</div>
@endif

{{--GRADUATE STUDIES--}}
<div class="row">
  <div class="offset-1 col-1">
    <a href="#" id="add_graduate_studies" class="btn btn-sm btn-info">Add</a>
  </div>
</div>

@if(count($applicant->education) > 0)
<?php $graduate = ''; ?>
@foreach($applicant->education as $key => $educ)

@if($educ->educ_level == 5)
<input type="hidden" name="graduate[{{$key}}][id]" value="{{$educ->id}}">
<input type="hidden" name="graduate[{{$key}}][applicant_id]" value="{{$applicant->id}}">
<?php
    $graduate = ($graduate == '') ? 'Graduate <br> Studies' : '-';
?>
<div class="form-group row graduate-studies">
  <label class="col-form-label col-1 font-weight-bold col">{!! $graduate !!}</label>
  <div class="col-3">
    <input type="text" name="graduate[{{$key}}][school_name]" class="form-control form-control-sm" value="{{$educ->school_name}}">
  </div>
  <div class="col-2">
    <input type="text" name="graduate[{{$key}}][course]" class="form-control form-control-sm" value="{{$educ->course}}">
  </div>
  <div class="col-1 text-center font-weight-bold">
    <input type="text" name="graduate[{{$key}}][attendance_from]" class="form-control form-control-sm" value="{{$educ->attendance_from}}">
  </div>
  <div class="col-1 text-center font-weight-bold">
    <input type="text" name="graduate[{{$key}}][attendance_to]" class="form-control form-control-sm" value="{{$educ->attendance_to}}">
  </div>
  <div class="col-1">
    <input type="text" name="graduate[{{$key}}][level]" class="form-control form-control-sm" value="{{$educ->level}}">
  </div>
  <div class="col-1">
    <input type="text" name="graduate[{{$key}}][graduated]" class="form-control form-control-sm" value="{{$educ->graduated}}">
  </div>
  <div class="col-2">
    <input type="text" name="graduate[{{$key}}][awards]" class="form-control form-control-sm" value="{{$educ->awards}}">
  </div>
</div>
@endif

@endforeach
@endif

@if(!in_array(5,$has_educ))
<input type="hidden" name="graduate[0][applicant_id]" value="{{$applicant->id}}">
<div class="form-group row graduate-studies">
  <label class="col-form-label col-1 font-weight-bold col">Graduate <br> Studies</label>
  <div class="col-3">
    {{ Form::text('graduate[0][school_name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('graduate[0][course]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('graduate[0][attendance_from]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1 text-center font-weight-bold">
    {{ Form::text('graduate[0][attendance_to]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('graduate[0][level]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-1">
    {{ Form::text('graduate[0][graduated]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
  <div class="col-2">
    {{ Form::text('graduate[0][awards]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
  </div>
</div>
@endif
<div class="form-group row text-right">
  <div class="col-12">
    {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
    {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>