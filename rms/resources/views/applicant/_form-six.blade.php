{{--PRIMARY--}}
<div class="form-group row font-weight-bold">
  <div class="col-3 text-center">CAREER SERVICE/RA 1080(BOARD/BAR) UNDER SPECIAL LAWS/CES/CSEE BARANGAY
                                 ELIGIBILITY/DRIVER'S LICENSE
  </div>
  <div class="col-1 text-center">RATING(If Applicable)</div>
  <div class="col-2 text-center">DATE OF EXAMINATION/CONFERMENT</div>
  <div class="col-3 text-center">PLACE OF EXAMINATION/CONFERMENT</div>
  <div class="col-2 text-center">LICENSE(If Applicable)</div>
</div>

<div class="row">
  <div class="col-12 text-right">
    <a href="#" id="add_eligibility" class="btn btn-sm btn-info">Add</a>
  </div>
</div>
@if(count($applicant->eligibility) > 0)

@foreach($applicant->eligibility as $key => $value)

<div class="form-group row eligibility">
  <input type="hidden" name="eligibility[{{$key}}][id]" value="{{$value->id}}">
  <div class="col-3 mt-4">
    <input type="text" name="eligibility[{{$key}}][name]" class="form-control form-control-sm" value="{{$value->name}}">
    {!! $errors->first('eligibility[$key][name]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 mt-4">
    <input type="text" name="eligibility[{{$key}}][rating]" class="form-control form-control-sm" value="{{$value->rating}}">
    {!! $errors->first('eligibility[$key][rating]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center font-weight-bold mt-4">
    <input type="text" name="eligibility[{{$key}}][exam_date]" class="form-control form-control-sm" value="{{$value->exam_date}}">
    {!! $errors->first('eligibility[$key][exam_date]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 text-center font-weight-bold mt-4">
    <input type="text" name="eligibility[{{$key}}][exam_place]" class="form-control form-control-sm" value="{{$value->exam_place}}">
    {!! $errors->first('eligibility[$key][exam_place]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center">
    NUMBER
    <input type="text" name="eligibility[{{$key}}][license_number]" class="form-control form-control-sm" value="{{$value->license_number}}">
    {!! $errors->first('eligibility[$key][license_number]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center">
    Date of Validity
    <input type="text" name="eligibility[{{$key}}][license_validity]" class="form-control form-control-sm" value="{{$value->license_validity}}">
    {!! $errors->first('eligibility[$key][license_validity]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>

@endforeach

@else
<div class="form-group row eligibility">
  <input type="hidden" name="eligibility[1][applicant_id]" value="{{$applicant->id}}">
  <div class="col-3 mt-4">
    {{ Form::text('eligibility[1][name]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[1][name]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 mt-4">
    {{ Form::text('eligibility[1][rating]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[1][rating]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center font-weight-bold mt-4">
    {{ Form::text('eligibility[1][exam_date]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[1][exam_date]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-3 text-center font-weight-bold mt-4">
    {{ Form::text('eligibility[1][exam_place]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[1][exam_place]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-1 text-center">
    NUMBER
    {{ Form::text('eligibility[1][license_number]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[1][license_number]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
  <div class="col-2 text-center">
    Date of Validity
    {{ Form::text('eligibility[1][license_validity]', '', [
            'class' => 'form-control form-control-sm',
        ])
    }}
    {!! $errors->first('eligibility[1][license_validity]', '<ul class="parsley-errors-list filled"><li class="parsley-required">:message</li></ul>') !!}
  </div>
</div>
@endif

<div class="form-group row text-right">
  <div class="col-12">
    {{ Form::button('Next Step', ['id' => 'btn-form-one', 'class'=>'btn btn-primary btn-space wizard-next', 'data-wizard' => '#wizard1']) }}
    {{ Form::reset('Clear Form', ['class'=>'btn btn-space btn-danger']) }}
  </div>
</div>