@extends('layouts.app')

@section('content')
    <div class="page-head">
        <h2 class="page-head-title">Job Posting Details</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('jobs.index' ) }}">Jobs ( {{ $job->title }} )</a></li>
            </ol>
        </nav>
    </div>
    <!-- Job Form -->
    <div class="row">
        <div class="col">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header">
                    <div class="btn-group mb-4" role="group" aria-label="Basic example">
                        <a href="{{ route('dashboard') }}" class="btn btn-success"><i class="icon icon-left mdi mdi-home"></i> Dashboard</a>
                        <a href="{{ route('jobs.index') }}" class="btn btn-success"><i class="icon icon-left mdi mdi-view-list-alt"></i> Jobs</a>
                        <a href="{{ route('jobs.create') }}" class="btn btn-success"><i class="icon icon-left mdi mdi-account-add"></i> Post a job</a>
                    </div>
                    <div class="tools">
                        <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false">
                        <i class="icon icon-left mdi mdi-settings-square"></i> Options <span class="icon-dropdown mdi mdi-chevron-down"></span>
                        </button>
                        <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                            <a href="{{ route('jobs.edit', $job->id) }}" class="dropdown-item"><i class="icon icon-left mdi mdi-edit"></i>Edit this post</a>
                            <div class="dropdown-divider"></div>
                            <a href="{{ route('jobs.edit', $job->id) }}" class="dropdown-item"><i class="icon icon-left mdi mdi-delete"></i>Delete this post</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="user-info-list card">
        <div class="card-header card-header-divider">
            {{ $job->title }}
            <span class="card-subtitle">
            Expires: {{ $job->expires }} / Created: {{ $job->created_at }} /
            Created By: {{ $job->created_by }} / Updated By: {{ $job->updated_by }}
            </span>
        </div>
        <div class="card-body">
            <table class="no-border no-strip skills">
                <tbody class="no-border-x no-border-y">
                    <tr>
                        <td class="icon"><span class="mdi mdi-case"></span></td>
                        <td class="item">Occupation<span class="icon s7-portfolio"></span></td>
                        <td>{{ $job->psipop->position_title }}</td>
                    </tr>
                    <tr>
                        <td class="icon"><span class="mdi mdi-collection-item-2"></span></td>
                        <td class="item">Plantilla Item Number<span class="icon s7-gift"></span></td>
                        <td>{{ $job->psipop->item_number }}</td>
                    </tr>
                    <tr>
                        <td class="icon"><span class="mdi mdi-seat"></span></td>
                        <td class="item">Grade<span class="icon s7-phone"></span></td>
                        <td>{{ $job->psipop->salary_grade }}</td>
                    </tr>
                    <tr>
                        <td class="icon"><span class="mdi mdi-alert-polygon"></span></td>
                        <td class="item">Status<span class="icon s7-map-marker"></span></td>
                        <td>{{ $job->status }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card card-contrast">
                <div class="card-header card-header-contrast">
                    Job Description
                    <span class="card-subtitle">General idea of the job at hand</span>
                </div>
                <div class="card-body">
                    {!! $job->description !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card card-contrast">
                <div class="card-header card-header-contrast">
                    Standards Qualifications
                    <span class="card-subtitle">All job qualifications are listed below</span>
                </div>
                <div class="card-body">
                    {!! $job->standards_qualifications !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card card-contrast">
                <div class="card-header card-header-contrast">
                    Requirements
                    <span class="card-subtitle">All job requirements are listed below</span>
                </div>
                <div class="card-body">
                    {!! $job->requirements !!}
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
<script src="{{ URL::asset('beagle-assets/js/app-tables-datatables.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dataTables();
    });
</script>
@endsection
