@extends('layouts.print')

@section('css')
@endsection

@section('content')

<div id="reports" style="margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-5">
    <div class="col-sm-3">CS Form No. 34-A <br> <i>Revised 2017</i></div>
    <div class="col-sm-6"></div>
    <div class="col-sm-3 text-center">
        <p class="border border-dark p-1" style="line-height: 1.2em;">
        For Regulated <br> National Government Agencies/ <br> Government-Owned or Controlled <br> Corporations/State Universities and  <br>Colleges
      </p>
    </div>
  </div>

  <div class="row mb-4">
    <div class="col-sm-12 text-center">
      <h4 style="line-height: 0.5em !important;">Republic of the Philippines</h4>
      <span>E2E SOLUTIONS MANAGEMENT PHILS INC</span>
    </div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-12 text-center"><h4><b>PLANTILLA OF CASUAL APPOINTMENTS</b></h4></div>
  </div>

  <div class="row mb-3">
    <div class="col-sm-1 text-right">Department/Office:</div>
    <div class="col-sm-4 border-bottom border-dark"></div>

    <div class="col-sm-2 text-right"></div>
    <div class="col-sm-1">&nbsp;</div>

    <div class="col-sm-2 text-right">Source of Funds</div>
    <div class="col-sm-2 border-bottom border-dark">&nbsp;</div>
  </div>

  <div class="row mb-1">
    <div class="col-sm-10"><b>INSTRUCTIONS:</b> (1) Only a maximum of fifteen (15) appointees must be listed on each page of the Plantilla of Casual Appointments </div>
  </div>
  <div class="row mb-1">
    <div class="col-sm-10" style="text-indent: 96px;">(2) Indicate ‘NOTHING FOLLOWS’ on the row following the name of the last appointee on the last page of the Plantilla.  </div>
  </div>
  <div class="row mb-1">
    <div class="col-sm-10" style="text-indent: 96px;">(3) Provide proper pagination (Page n of n page/s).</div>
  </div>
  <div class="row mb-1">
      <div class="col-sm-12">
        <table id="table1" class="table table-striped table-hover table-fw-widget table-bordered" style="font-size: 10px;">
          <thead>
            <tr class="text-center">
              <th colspan="5">Name of Appointers</th>
              <th rowspan="2" style="vertical-align: middle;">Position <br> Title</th>
              <th rowspan="2" style="vertical-align: middle;">Item No</th>
              <th rowspan="2" style="vertical-align: middle;">Salary/ <br>JOB/ <br>Pay Grade</th>
              <th rowspan="2" style="vertical-align: middle;">Daily <br>Wage</th>
              <th colspan="2" style="vertical-align: middle;">PERIOD OF EMPLOYMENT</th>
              <th colspan="1" style="vertical-align: middle;">NATURE OF <br> APPOINTMENT</th>
              <th colspan="2" style="vertical-align: middle;">ACKNOWLEDGEMENT OF <br> APPOINTEE</th>
              <th colspan="2">CSC Action</th>
            </tr>
            <tr class="text-center">
              <th></th>
              <th style="vertical-align: middle;">Last Name</th>
              <th style="vertical-align: middle;">First Name</th>
              <th style="vertical-align: middle;">Name Extension <br> (Jr./III)</th>
              <th style="vertical-align: middle;">Middle Name</th>
              <th style="vertical-align: middle;">From</th>
              <th style="vertical-align: middle;">To</th>
              <th style="vertical-align: middle;">(Original/ <br> Reappointment/ <br> Reemployment)</th>
              <th style="vertical-align: middle;">Signature</th>
              <th style="vertical-align: middle;">Date Received</th>
              <th style="vertical-align: middle;">A-Approved <br> D - Disapproved</th>
              <th style="vertical-align: middle;">Date of Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($casuals as $key => $casual)
            <tr>
              <td style="vertical-align: middle;">{!! $key+1 !!}</td>
              <td style="vertical-align: middle;">{!! $casual->applicant->last_name !!}</td>
              <td style="vertical-align: middle;">{!! $casual->applicant->first_name !!}</td>
              <td style="vertical-align: middle;">{!! $casual->applicant->extension_name !!}</td>
              <td style="vertical-align: middle;">{!! $casual->applicant->middlename_name !!}</td>
              <td style="vertical-align: middle;" nowrap>{!! $casual->applicant->job->psipop->position_title !!}</td>
              <td style="vertical-align: middle;" nowrap>{!! $casual->applicant->job->psipop->item_number !!}</td>
              <td class="text-center" style="vertical-align: middle;" nowrap>{!! $casual->applicant->job->psipop->salary_grade !!}</td>
              <td style="vertical-align: middle;" nowrap>{!! number_format($casual->applicant->job->daily_salary,2) !!}</td>
              <td style="vertical-align: middle;">{!! $casual->period_emp_from !!}</td>
              <td style="vertical-align: middle;">{!! $casual->period_emp_to !!}</td>
              <td style="vertical-align: middle;"></td>
              <td style="vertical-align: middle;"></td>
              <td style="vertical-align: middle;"></td>
              <td style="vertical-align: middle;"></td>
              <td style="vertical-align: middle;"></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
  </div>
  <div class="row mb-2">
    <div class="col-sm-12">
      The abovenamed personnel are hereby hired/appointed as casuals at the rate of compensation stated opposite their names for the period indicated. It is understood that such employment will cease  automatically at the end of the period stated unless renewed. Any or all of them may be laid-off any time before the expiration of the employment period when their services are no longer needed or funds are no longer available or the project has already been completed/finished or their performance are below par.
    </div>
  </div>
  <div class="row mb-4">
      <div class="col-sm-4"><b>CERTIFCATION</b></div>
      <div class="col-sm-4"><b>APPOINTING OFFICER / AUTHORITY</b></div>
  </div>

  <div class="row mb-4">
      <div class="col-sm-4">
        <p style="text-indent: 20px;font-size: 12px;padding: 10px;" class="text-justify">
            This is to certify that all requirements and supporting papers pursuant to CSC MC No. 24, s. 2017, as amended, have been  complied  with, reviewed and found in order.
        </p>
      </div>
      <div class="col-sm-4">
      </div>
      <div class="col-sm-4">
      </div>
  </div>


  <div class="row mb-4 text-center">
      <div class="col-sm-4">
          <p class="border-bottom border-dark pb-0"></p>
          <p>
            Supervising Gender and Development Specialist <br>
            Head, Human Resource Management and Development Section
          </p>
      </div>
      <div class="col-sm-4">
          <p class="border-bottom border-dark pb-0"></p>
          <p>
            Chief Administrative Officer <br>
            Administrative and Finance Division
          </p>
      </div>
      <div class="col-sm-4">
          <p class="border-bottom border-dark pb-0">&nbsp;</p>
          <p>
            CSC Official
          </p>
      </div>
  </div>


</div>

 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection