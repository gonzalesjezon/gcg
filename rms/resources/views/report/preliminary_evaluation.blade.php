@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div class="reports" style="width: 960px;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
	<div class="row mb-2">
		<div class="col-sm-12 text-center">
			<table class="table table-striped table-fw-widget table-bordered">
				<thead>
					<tr>
						<th colspan="7">Result of Preliminary Evaluation of Candidate Qualifications</th>
					</tr>
					<tr>
						<th colspan="7">{{ strtoupper($jobs->psipop->position_title) }}</th>
					</tr>
					<tr>
						<th>No</th>
						<th>Name</th>
						<th>Date and Time <br> Application was Received</th>
						<th>Education</th>
						<th>Experience(s)</th>
						<th>Training(s)</th>
						<th>Eligibility</th>
					</tr>
				</thead>
				<tbody>
					@foreach($preliminary as $key => $value)
					<tr>
						<td style="vertical-align: top;">{{ $key+1 }}</td>
						<td style="vertical-align: top;">{{ $value->applicant->getFullName()}}</td>
						<td style="vertical-align: top;">{{ $value->created_at }}</td>
						<td style="vertical-align: top;">{!! $value->education !!}</td>
						<td style="vertical-align: top;">{!! $value->experience !!}</td>
						<td style="vertical-align: top;">{!! $value->training !!}</td>
						<td style="vertical-align: top;">{!! $value->eligibility !!}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

</div>


 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection