@extends('layouts.print')

@section('css')
@endsection

@section('content')

<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-1">
    <div class="col-sm-3">CS Form No. 8</div>
  </div>

  <div class="row mb-4">
    <div class="col-sm-6"><i>Revised 2017</i></div>
  </div>

  <div class="row mb-6">
    <div class="col-sm-12 text-center">
    	<h4 class="font-weight-bold">REPORT ON  DATABASE OF INDIVIDUALS BARRED FROM ENTERING SERVICE <br> AND TAKING CIVIL SERVICE EXAMINATIONS (DIBAR)</h4>
    </div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-12">
  		Report for the Month of:
  	</div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-12">
  		<table class="table table-striped table-hover table-fw-widget table-bordered" style="font-size: 10px;">
  			<thead>
  				<tr class="text-center">
  					<th colspan="12">INDIVIDUAL INFORMATION</th>
  				</tr>
  				<tr class="text-center">
  					<th colspan="5">NAME</th>
  					<th rowspan="2" style="vertical-align: middle;">SEX</th>
  					<th rowspan="2" style="vertical-align: middle;">DATE AND PLACE OF BIRTH</th>
  					<th rowspan="2" style="vertical-align: middle;">OCCUPATION CATEGORY</th>
  					<th rowspan="2" style="vertical-align: middle;">POSITION </th>
  					<th rowspan="2" style="vertical-align: middle;"> AGENCY</th>
  					<th rowspan="2" style="vertical-align: middle;">DATE & PLACE OF EXAM</th>
  					<th rowspan="2" style="vertical-align: middle;">ELIGIBILITY / RATING</th>
  				</tr>
  				<tr class="text-center">
  					<th>LAST NAME</th>
  					<th>FIRST NAME</th>
  					<th>NAME EXTENSION</th>
  					<th>MIDDLE NAME</th>
  					<th>MAIDEN NAME</th>
  				</tr>
  			</thead>
  			<tbody>
  			</tbody>
  		</table>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-12">
  		<table class="table table-striped table-hover table-fw-widget table-bordered" style="font-size: 10px;">
  			<thead>
  				<tr class="text-center">
  					<th colspan="12">DECISION INFORMATION*</th>
  				</tr>
  				<tr class="text-center">
  					<th>DOCUMENT TYPE/ NO./ DATE</th>
  					<th>SIGNATORY</th>
  					<th>POSITION</th>
  					<th>AGENCY</th>
  					<th>OFFENSE</th>
  					<th>PENALTY</th>
  					<th>REMARKS</th>
  				</tr>
  			</thead>
  			<tbody>
  			</tbody>
  		</table>
  	</div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-12">
  		<b>*DECISION INFORMATION</b> - Decision/Resolution/Order issued to bar certain individuals from entering government service and taking civil service examinations
  	</div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-12">
  		<p style="text-indent: 30px;">I hereby certify that the Decisions/Resolutions/Orders enumerated are executory.</p>
  	</div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-6">
  		Prepared By:
  	</div>
  	<div class="col-sm-6">
  		Cerified Correct:
  	</div>
  </div>

   <div class="row mb-4">
  	<div class="col-sm-6 text-center">
  		<hr>
  		Chief/Head of Division <br> (Signature over Printed Name)
  	</div>
  	<div class="col-sm-6 text-center">
  		<hr>
  		Director IV/Head of Department or Office <br> (Signature over Printed Name)
  	</div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-6">
  		Date
  		<hr>
  	</div>
  	<div class="col-sm-6">
  		Date
  		<hr>
  	</div>
  </div>

</div>

 <div class="form-group row text-right d-print-none">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection