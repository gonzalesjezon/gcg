@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div class="reports" style="width: 960px;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
	<div class="row mb-2">
		<div class="col-sm-12">
			CS Form No. 5 <br>
			Series of 2018
		</div>
	</div>

	<div class="row mb-6">
		<div class="col-sm-12 text-center">
			<p class="m-0 p-0 font-weight-bold" style="font-size: 1.5em;">Republic of the Philippines</p>
			<p class="mt-0 p-0">E2E SOLUTIONS MANAGEMENT PHILS INC</p>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-sm-12 text-center">
			<p style="font-size:1.8em; ">CERTIFICATION</p>
		</div>
	</div>

	<div class="row mb-2">
		<div class="col-sm-12">
			<p style="text-indent: 60px;" class="text-justify">This is to certify that based on the records of this Office, there is no qualified eligible who actually applied to the _(Position Title)_ position in _(Name of Office/Agency Name)_ , _(Location)_. </p>
		</div>
	</div>

	<div class="row mb-2">
		<div class="col-sm-12">
			<p style="text-indent: 60px;" class="text-justify">This certification is issued pursuant to Section 5 (k), Rule II of CSC Memorandum No. ___, s. 2017 (2017 Omnibus Rules on Appointments and Other Human Resource Actions). </p>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-sm-12">
			<p style="text-indent: 60px;" class="text-justify">I agree that any misrepresentation made in this certification shall cause the filing of administrative/criminal case/s against me. </p>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-sm-8"></div>
		<div class="col-sm-4 text-center">
			<hr>
			 Highest Official In Charge of HRM
		</div>
	</div>

	<div class="row mb-6">
		<div class="col-sm-4 text-center">
			<hr>
			Date: _________________
		</div>
	</div>
</div>


 <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection