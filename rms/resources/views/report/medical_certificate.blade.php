@extends('layouts.print')

@section('css')
@endsection

@section('content')
<div class="reports" style="width: 100%;margin: auto;font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
	<div class="row mb-2">
		<div class="col-sm-12">
			CS Form No. 211 <br>
			Revised 2017
		</div>
	</div>

	<div class="row mb-6">
		<div class="col-sm-12 text-center">
			<h4><b>MEDICAL CERTIFICATE</b></h4>
			(Employee Certificate)
		</div>
	</div>
	<hr>
	<div class="row mb-2">
		<div class="col-sm-12 text-center">
			<h4><b>I N S T R U C T I O N S</b></h4>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-sm-12 offset-sm-2">
			<ol type="a">
				<li>This medical certificate should be accomplished by a licensed government physician.</li>
				<li>Attach this certificate to original appointment, transfer and reemployment.</li>
				<li>The results of the following pre-employment medical/physical/mental examinations must be attached to this form:</li>
			</ol>
			<ul style="list-style: square;" class="offset-sm-1">
				<li>Blood Test</li>
				<li>Urinalysis</li>
				<li>Chest X-Ray</li>
				<li>Drug Test</li>
				<li>Psychological Test</li>
				<li>Neuro-Psychiatric Examination (if applicable)</li>
			</ul>
		</div>
	</div>
	<hr>

	<div class="row mb-2">
		<div class="col-sm-12 text-center">
			<h4><b>FOR THE PROPOSED APPOINTEE</b></h4>
		</div>
	</div>

	<div class="row mb-2">
		<div class="col-sm-12">
			<table class="table table-fw-widget table-bordered">
				<tr>
					<td colspan="3">NAME  (Last Name, First Name, Name Extension (if any) and Middle Name)</td>
					<td class="text-center">AGENCY / ADDRESS</td>
				</tr>
				<tr>
					<td colspan="3">{!! $applicant->last_name !!}, {!! $applicant->first_name !!}, {!! $applicant->extension_name !!} {!! $applicant->middle_name !!}</td>
					<td rowspan="2" class="text-center">
						PHILIPPINE COMMISSION ON WOMEN <br>
						1145 J.P Laurel St. San Miguel Manila
					</td>
				</tr>
				<tr>
					<td colspan="3">
						ADDRESS: {!! $applicant->barangay !!} {!! $applicant->street !!} {!! $applicant->city !!}
					</td>
				</tr>
				<tr>
					<td>Age</td>
					<td>Sex</td>
					<td>Civil Status</td>
					<td class="text-center">PROPOSED POSITION</td>
				</tr>
				<tr>
					<td>{{ Carbon\Carbon::today()->diffInYears($applicant->birthday) }}</td>
					<td>{!! $applicant->gender !!}</td>
					<td>{!! $applicant->civil_status !!}</td>
					<td class="text-center">{!! $applicant->job->psipop->position_title !!}</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="row mb-2">
		<div class="col-sm-12 text-center">
			<h4><b>FOR THE LICENSED GOVERNMENT PHYSICIAN</b></h4>
		</div>
	</div>

	<div class="row mb-2">
		<div class="col-sm-12">
			<table class="table table-fw-widget table-bordered">

				<tr>
					<td colspan="7">I hereby certify that I have reviewed and evaluated the attached examination results, personally examined the above named individual and found him/her to be </td>
				</tr>
				<tr>
					<td colspan="3">SIGNATURE over PRINTED NAME OF LICENSED GOVERNMENT PHYSICIAN: </td>
					<td class="text-center" colspan="3">OTHER INFORMATION ABOUT THE PROPOSED APPOINTEE</td>
				</tr>
				<tr>
					<td colspan="3"></td>
					<td rowspan="3" colspan="3" ></td>
				</tr>
				<tr>
					<td colspan="3">AGENCY/Affiliation of Licensed Government Physician: </td>
				</tr>
				<tr>
					<td colspan="3"></td>
				</tr>
				<tr>
					<td colspan="3">LICENSE NO.</td>
					<td>HEIGHT (M) Bare Foot</td>
					<td>WEIGHT (KG) Stripped</td>
					<td>BLOOD TYPE</td>
				</tr>
				<tr>
					<td colspan="3"></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3">OFFICIAL DESIGNATION</td>
					<td colspan="3">DATE EXAMINED</td>
				</tr>
				<tr>
					<td colspan="3"></td>
					<td colspan="3"></td>
				</tr>
			</table>
		</div>
	</div>
	<hr>
</div>


 <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3 d-print-none">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection