@extends('layouts.print')

@section('css')
@endsection

@section('content')

<div id="reports" style="width: 960px;margin: auto; font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
  <div class="row mb-1">
    <div class="col-sm-3">CS Form No. 9 <br> Revised 2017</div>
    <div class="col-sm-6"></div>
    <div class="col-sm-3 text-center">
    	<p class="border p-1">
    		Electronic copy to be submitted to the CSC FO must be in MS Excel format
    	</p>
    </div>
  </div>

  <div class="row mb-4">
  	<div class="col-sm-12 text-center">
  		<div style="font-size: 16px;" class="font-weight-bold">Republic of the Philippines</div>
  		<div>E2E SOLUTIONS MANAGEMENT PHILS INC</div>
      <div>Room 304 URC Bldg. 2123 Espana Blvd<br>Brgy 512 Sampaloc Manila</div>
  		<div class="font-weight-bold pt-1" style="font-size:16px;">Request for Publication of Vacant Positions</div>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-12">
  		<h4 class="font-weight-bold">To: CIVIL SERVICE COMMISSION (CSC)</h4>
  		<p style="text-indent: 70px;">This is to request the publication of the following vacant positions of (Name of Agency) in the CSC website:</p>
  	</div>
  </div>

  <div class="row">
    <div class="col-sm-8"></div>
    <div class="col-sm-4 text-center">
    </div>
  </div>

  <div class="row mb-2">
  	<div class="col-sm-8"></div>
  	<div class="col-sm-4 text-center border-top">
  		Chief Administrative Officer
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-8"></div>
  	<div class="col-sm-4">
  		Date
  	</div>
  </div>

  <div class="row mb-2">
  	<div class="col-sm-12">
  		<table class="table table-fw-widget table-bordered">
  			<thead>
  				<tr class="text-center">
  					<th rowspan="2" style="vertical-align: middle;">No.</th>
  					<th rowspan="2" style="vertical-align: middle;">Position Title</th>
  					<th rowspan="2" style="vertical-align: middle;">Plantilla Item No.</th>
  					<th rowspan="2" style="vertical-align: middle;">Annual Salary</th>
  					<th colspan="5">Qualification Standards</th>
  					<th rowspan="2" style="vertical-align: middle;">Place of Assignment</th>
  				</tr>
  				<tr class="text-center">
  					<th style="vertical-align: middle;">Education</th>
  					<th style="vertical-align: middle;">Training</th>
  					<th style="vertical-align: middle;">Experience</th>
  					<th style="vertical-align: middle;">Eligibility</th>
  					<th style="vertical-align: middle;">Competency <br> (if applicable)</th>
  				</tr>
  			</thead>
        <tbody>
          @foreach($jobs as $key => $job)
          <tr>
            <td class="text-center" style="vertical-align: top">{!! $key+1 !!}</td>
            <td style="vertical-align: top">{{ $job->psipop->position_title}}</td>
            <td style="vertical-align: top">{{ $job->psipop->item_number}}</td>
            <td style="vertical-align: top">{{ number_format($job->psipop->annual_actual_salary,2)}}</td>
            <td style="vertical-align: top">
              {!! $job->education !!}
            </td>
            <td style="vertical-align: top">
              {!! $job->training !!}
            </td>
            <td style="vertical-align: top">
              {!! $job->experience !!}
            </td>
            <td style="vertical-align: top">
              {!! $job->eligibility !!}
            </td>
            <td></td>
            <td style="vertical-align: top;">{!! $job->reporting_line !!}</td>
          </tr>
          @endforeach
        </tbody>
  		</table>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-12">
  		<p>Interested and qualified applicants should signify their interest in writing. Attach the following documents to the application letter and send to the address below not later _____</p>
  	</div>
  </div>

  <div class="row mb-2">
  	<div class="col-sm-12">
  		<ol>
  			<li>Fully accomplished Personal Data Sheet (PDS) with recent passport-sized picture (CS Form No. 212, Revised 2017) which can be downloaded at www.csc.gov.ph;</li>
  			<li>Performance rating  in the present position for one (1) year (if applicable);</li>
  			<li>Photocopy of certificate of eligibility/rating/license; and</li>
  			<li>Photocopy of Transcript of Records.</li>
  		</ol>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-12">
  		<p><b>QUALIFIED APPLICANTS</b> are advised to hand in or send through courier/email their application to:</p>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-3 text-center">
  		<p class="border-bottom"></p>
  		<p class="border-bottom">Chief Administrative Officer</p>
  		<p class="border-bottom">1145 J.P Laurel St. San Miguel Manila</p>
  		<p class="border-bottom">(E-mail Address)</p>
  	</div>
  </div>

  <div class="row mb-1">
  	<div class="col-sm-12">
  		<p><b>APPLICATIONS WITH INCOMPLETE DOCUMENTS SHALL NOT BE ENTERTAINED.</b></p>
  	</div>
  </div>

</div>

 <div class="form-group row text-right">
    <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-3 d-print-none">
      {{ Form::button('Print', ['id' => 'evaluation-report', 'class'=>'btn btn-primary btn-space', 'type'=>'submit']) }}
      {{ Form::reset('Cancel', ['class'=>'btn btn-space btn-danger']) }}
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('#evaluation-report').click(function() {
      window.print();
    });
  });
</script>
@endsection