<body style="background-color: white !important;">
   <div class="be-wrapper be-nosidebar-left">
        @include('layouts._main-content')
   </div>
   <script src="{{ URL::asset('beagle-assets/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
   <script src="{{ URL::asset('beagle-assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
   <script src="{{ URL::asset('beagle-assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
   <script src="{{ URL::asset('beagle-assets/js/app.js') }}" type="text/javascript"></script>
   @yield('scripts')
</body>
