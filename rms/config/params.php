<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Parameters
    |--------------------------------------------------------------------------
    |
    | Accessible via config('param.key');
    | eg: config('params._SUPER_ADMIN_ID_')
    | any other location as required by the application or its packages.
    */

    '_SUPER_ADMIN_ID_' => 1,
    'employee_status' => [
        1 => 'Permanent',
        2 => 'Project',
        3 => 'Contract of Service',
        4 => 'Contractual'
    ],
    'nature_of_appointment' => [
        1 => 'Original',
        2 => 'Promotion',
    ],
    'publication' => [
        'agency' => 'Agency Web Site',
        'csc_bulletin' => 'CSC Bulletin of Vacant Position',
        'newspaper' => 'Newspaper',
        'others' => 'Others',
    ],
    'step' => [
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8'
    ],
    'salary_grade' => [
        1 => 'SG 1',
        2 => 'SG 2',
        3 => 'SG 3',
        4 => 'SG 4',
        5 => 'SG 5',
        6 => 'SG 6',
        7 => 'SG 7',
        8 => 'SG 8',
        9 => 'SG 9',
        10 => 'SG 10',
        11 => 'SG 11',
        12 => 'SG 12',
        13 => 'SG 13',
        14 => 'SG 14',
        15 => 'SG 15',
        16 => 'SG 16',
        17 => 'SG 17',
        18 => 'SG 18',
        19 => 'SG 19',
        20 => 'SG 20',
        21 => 'SG 21',
        22 => 'SG 22',
        23 => 'SG 23',
        24 => 'SG 24',
        25 => 'SG 25',
        26 => 'SG 26',
        27 => 'SG 27',
        28 => 'SG 28',
        29 => 'SG 29',
        30 => 'SG 30',
        31 => 'SG 31',
        32 => 'SG 32',
        33 => 'SG 33'
    ],
    'examination_status' => [
        1 => 'For Examination',
        2 => 'Failed Exam',
        3 => 'No Show',
        4 => 'Reschedule',
        5 => 'Withdrawn',
        6 => 'For Reference',
        7 => 'For Interview',
    ],
    'interview_status' => [
        1 => 'Set Interview',
        2 => 'Failed Interview',
        3 => 'Approved for Requirements',
        4 => 'No Show',
        5 => 'Reschedule',
        6 => 'Withdrawn',
        7 => 'For Reference',
    ],
    'option_1' => [
        1 => 'Occassional',
        2 => 'Frequent'
    ],
    'boarding_status' => [
        1 => 'Hired',
        2 => 'Pending for Requirements'
    ]


];
