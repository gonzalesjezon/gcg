<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'interviews';
    protected $fillable = [

		'applicant_id',
		'interview_date',
		'interview_time',
		'interview_location',
		'resched_interview_date',
		'resched_interview_time',
		'interview_status',
		'notify',
		'noftiy_resched_interview',
		'confirmed',
		'psb_chairperson',
		'psb_secretariat',
		'psb_member',
		'psm_sweap_rep',
		'psb_end_user'

    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
