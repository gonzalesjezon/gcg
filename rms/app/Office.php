<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'offices';

    protected $fillable = [

		'name',
		'created_by',
		'updated_by',
    ];

    public function psipop(){
    	return $this->belongsTo('App\PSIPOP');
    }
}
