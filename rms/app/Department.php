<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'departments';

    protected $fillable = [
    	'name'
    ];

    public function psipop(){
    	return $this->belongsTo('App\PSIPOP');
    }
}
