<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentForm extends Model
{
	protected $primaryKey = 'id';
    protected $table = 'appointment_forms';
    protected $fillable = [
		'applicant_id',
		'employee_status',
		'nature_of_appointment',
		'appointing_officer',
		'hrmo',
		'chairperson',
		'date_sign',
		'publication_date_from',
		'publication_date_to',
		'hrmo_date_sign',
		'chairperson_date_sign',
		'period_emp_from',
		'period_emp_to',
		'date_issued',
		'assessment_date',
		'publication_assessment_date',
		'deliberation_date',
		'created_by',
		'updated_by'
    ];

    public function applicant(){
    	return $this->belongsTo('App\Applicant');
    }
}
