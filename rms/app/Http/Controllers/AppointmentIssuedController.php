<?php

namespace App\Http\Controllers;

use App\AppointmentIssued;
use App\AppointmentProcessing;
use App\Applicant;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AppointmentIssuedController extends Controller
{

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Appointment Issued');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $issued = AppointmentIssued::latest()
            ->paginate($perPage);

        return view('appointment-issued.index', [
            'issued' => $issued
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $applicant       = new Applicant;
        $issued = new AppointmentIssued;
        if (isset($request->applicant_id)) {
            $applicant = $applicant->where('id', $request->applicant_id)
                ->first();
            $issued = $issued->where('applicant_id',$request->applicant_id)->first();
        }

        return view('appointment-issued.create')->with([
            'applicant' => $applicant,
            'issued' => $issued,
            'action' => 'AppointmentIssuedController@store',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $issued = AppointmentIssued::find($request->issued_id);
        if(empty($issued)){
            $issued = new AppointmentIssued;
        }
        $issued->fill($request->all());
        if($issued->exists()){
            $issued->updated_by = Auth::id();
            $response = 'The Appointment issued was successfully updated.';
        }else{
            $issued->created_by = Auth::id();
            $response = 'The Appointment issued was successfully created.';
        }
        if($issued->save()){
            // $assumption = $this->saveAssumption($request);
            // $oathoffice = $this->saveOath($request);
            $processing = $this->storeAppointee($issued->applicant_id);
        }
        return redirect('/appointment-issued')->with('success', $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function show(AppointmentIssued $appointmentIssued)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function edit(AppointmentIssued $appointmentIssued)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppointmentIssued $appointmentIssued)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppointmentIssued  $appointmentIssued
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AppointmentIssued::destroy($id);
        return redirect('/appointment-issued')->with('success', 'Appointment issued data deleted!');
    }

    public function storeAppointee($applicant_id){

        $appointment = AppointmentProcessing::where('applicant_id',$applicant_id)->first();
        if (empty($appointment)) {
            $appointment = new AppointmentProcessing;
        }
        if($appointment->exists()){
            $appointment->updated_by = Auth::id();
            $response = redirect('/interviews')->with('success', 'Applicant appointed was updated successfully.');
        }else{
            $appointment->created_by = Auth::id();

            $response = redirect('/interviews')->with('success', 'Applicant appointed was created successfully.');
        }
        $appointment->applicant_id = $applicant_id;
        $appointment->save();
    }
}
