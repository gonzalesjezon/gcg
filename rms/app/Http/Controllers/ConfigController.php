<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests,
    App\Config,
    Illuminate\Http\Request,
    Illuminate\Support\Facades\View;

class ConfigController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules=[
        'name' => 'required|max:255',
        'value' => 'required|max:255',
    ];

    /**
     * Call behavior handling for authentication
     * authentication section via middleware
     * =====================================
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        View::share('title', 'Configuration');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $perPage = 100;
        $config = Config::latest()->paginate($perPage);

        return view('config.index', ['config' => $config]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $config = new Config();
        return view('config.create')->with([
            'config' => $config
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request,$this->validationRules);

        $config = new Config;
        $config->fill($request->all());
        $config->created_by = \Auth::id();
        $config->save();

        return redirect('/config')->with('success', 'The configuration was successfully created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $config = Config::findOrFail($id);

        return view('config.edit', [
            'config' => $config,
            'action' => 'ConfigController@update'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,$this->validationRules);

        $config = Config::findOrFail($id);
        $config->fill($request->all());
        $config->update();

        return redirect()->route('config.edit',['id'=>$config->id] )->with('success', 'Configuration updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Config::destroy($id);

        return redirect('config')->with('success', 'Config deleted!');
    }
}
