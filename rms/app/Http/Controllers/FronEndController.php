<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Job;
use App\Countries;
use App\Applicant;
use App\WorkExperience;
use App\Eligibility;
use App\Training;
class FronEndController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
        'first_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'middle_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'last_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        'email_address' => 'required|email'
    ];

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Applicant Form');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $jobs = Job::where('publish',1)
        ->latest()->paginate($perPage);
        return view('frontend.index')
        ->with([
            'jobs' => $jobs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $countries = Countries::all('code', 'name')
            ->pluck('name', 'code')->toArray();

        $jobs = Job::where('id',$request->id)->first();

        return view('frontend.create')->with([
            'action' => 'FronEndController@store',
            'civilStatus' => Applicant::getCivilStatus(),
            'gender' => Applicant::getGender(),
            'countries' => $countries,
            'jobs' => $jobs,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->validationRules);

        $applicant = new Applicant;
        $applicant->fill($request->all());
        $applicant->reference_no = uniqid();
        $applicant->saveImageFileNames($request);
        $applicant->saveDocumentFileNames($request);
        $applicant->setIsFilipino($request->filipino);
        $applicant->setIsNaturalized($request->naturalized);
        $applicant->created_by = (\Auth::id()) ? \Auth::id() : 88888888;


        if ($applicant->save()) {
            $applicant->uploadImageFiles($request);
            $applicant->uploadDocumentFiles($request);
            $eligibility = $this->saveEligibility($request->eligibility,$applicant->id);
            $workexperience = $this->saveWorkExperience($request->work_experience,$applicant->id);
            $training = $this->saveTraining($request->training,$applicant->id);
        }

        return redirect('/frontend')->with('success', 'Successfully applied to vacant position.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jobs = Job::where('id',$id)->first();

        return view('frontend.show')->with([
            'jobs' => $jobs,

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function saveEligibility($data,$applicant_id){

        foreach ($data as $key => $value) {

            if(isset($value['name'])){
                $eligibility = Eligibility::find(@$value['id']);

                if(empty($eligibility)){
                    $eligibility = new Eligibility;
                }
                $eligibility->applicant_id         = $applicant_id;
                $eligibility->name                 = $value['name'];
                $eligibility->rating               = $value['rating'];
                $eligibility->exam_place           = $value['exam_place'];
                $eligibility->license_number       = $value['license_number'];
                $eligibility->license_validity     = $value['license_validity'];
                $eligibility->exam_date            = $value['exam_date'];
                $eligibility->save();
            }

        }
    }

    public function saveWorkExperience($data,$applicant_id){

        foreach ($data as $key => $value) {

            if(isset($value['position_title'])){
                $workexperience = WorkExperience::find(@$value['id']);

                if(empty($workexperience)){
                    $workexperience = new WorkExperience;
                }
                $workexperience->applicant_id           = $applicant_id;
                $workexperience->inclusive_date_from    = $value['inclusive_date_from'];
                $workexperience->inclusive_date_to      = $value['inclusive_date_to'];
                $workexperience->position_title         = $value['position_title'];
                $workexperience->department             = $value['department'];
                $workexperience->salary_grade           = $value['salary_grade'];
                $workexperience->status_of_appointment  = $value['status_of_appointment'];
                $workexperience->govt_service           = $value['govt_service'];
                $workexperience->save();
            }

        }
    }

    public function saveTraining($data,$applicant_id){

        foreach ($data as $key => $value) {

            if(isset($value['title_learning_programs'])){
                $training = Training::find(@$value['id']);

                if(empty($training)){
                    $training = new Training;
                }
                $training->applicant_id             = $applicant_id;
                $training->title_learning_programs  = $value['title_learning_programs'];
                $training->inclusive_date_from      = $value['inclusive_date_from'];
                $training->inclusive_date_to        = $value['inclusive_date_to'];
                $training->number_hours             = $value['number_hours'];
                $training->ld_type                  = $value['ld_type'];
                $training->sponsored_by             = $value['sponsored_by'];
                $training->save();
            }

        }
    }
}
