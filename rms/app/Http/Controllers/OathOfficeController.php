<?php

namespace App\Http\Controllers;

use App\OathOffice;
use App\Applicant;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class OathOfficeController extends Controller
{

    public function __construct()
    {
        View::share('title', 'Oath of Office');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $oathoffice = OathOffice::with([
                'applicant.job' => function ($query) {
                    $query->where('status', '=', 'plantilla');
                }
            ]
        )->paginate($perPage);

        return view('oath-office.index', [
            'oathoffices' => $oathoffice
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function show(OathOffice $oathOffice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $oathoffice = new OathOffice();
        $applicant = new Applicant();

        if ($id) {
            $oathoffice = OathOffice::find($id)->first();
        }

        return view('oath-office.edit')->with([
            'oathoffice' => $oathoffice,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oathoffice = OathOffice::find($id);
        $oathoffice->fill($request->all());
        $oathoffice->updated_by = Auth::id();

        $oathoffice->save();

       return redirect('/oath-office')->with('success', 'Oath of Office was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OathOffice  $oathOffice
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OathOffice::destroy($id);
        return redirect('/oath-office')->with('success', 'Oath of Office was successfully deleted.');
    }

    public function oathOfficeReport(Request $request){

        $oathoffice = OathOffice::find($request->id);

        return view('oath-office.report',[
            'oathoffice' => $oathoffice,
        ]);
    }
}
