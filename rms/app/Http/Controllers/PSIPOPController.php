<?php

namespace App\Http\Controllers;

use App\PSIPOP;
use App\Department;
use App\Division;
use App\Office;
use App\Section;
use Auth;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class PSIPOPController extends Controller
{
    /**
     * Call behavior handling for authentication
     * authentication section via middleware
     * =====================================
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'careers']]);
        View::share('title', 'Itemization & Plantilla of Personnel');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $psipop = PSIPOP::latest()
        ->paginate($perPage);

        return view('psipop.index', [
            'psipop' => $psipop
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $offices = Office::pluck('name','id')->toArray();
        $divisions = Division::pluck('name','id')->toArray();
        $departments = Department::pluck('name','id')->toArray();
        $sections = Section::pluck('name','id')->toArray();

        return view('psipop.create',[
            'offices' => $offices,
            'divisions' => $divisions,
            'departments' => $departments,
            'sections' => $sections,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $psipop =  new PSIPOP();
        $psipop->fill($request->all());
        $psipop->created_by = Auth::id();
        $psipop->save();

        return redirect()
            ->route('psipop.edit', [
                'id' => $psipop->id,
            ])
            ->with('success', 'The itemization & plantilla of personnel was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PSIPOP  $pSIPOP
     * @return \Illuminate\Http\Response
     */
    public function show(PSIPOP $pSIPOP)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PSIPOP  $pSIPOP
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $psipop = PSIPOP::findOrFail($id);
        $offices = Office::pluck('name','id')->toArray();
        $divisions = Division::pluck('name','id')->toArray();
        $departments = Department::pluck('name','id')->toArray();
        $sections = Section::pluck('name','id')->toArray();

        return view('psipop.edit',[
            'offices' => $offices,
            'divisions' => $divisions,
            'departments' => $departments,
            'sections' => $sections,
            'psipop' => $psipop
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PSIPOP  $pSIPOP
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $psipop = PSIPOP::findOrFail($id);
        $psipop->fill($request->all());
        $psipop->updated_by = Auth::id();
        $psipop->save();

        return redirect()
            ->route('psipop.edit', [
                'id' => $psipop->id,
            ])
            ->with('success', 'The itemization & plantilla of personnel was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PSIPOP  $pSIPOP
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        PSIPOP::destroy($id);
        return redirect('psipop')->with('success', 'Record successfully deleted!');
    }
}
