<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\AppointmentForm;
use App\AppointmentIssued;
use App\Job;
use App\JobOffer;
use App\Assumption;
use App\OathOffice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AppointmentFormController extends Controller
{
    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Appointment Form');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = AppointmentForm::latest()
            ->paginate($perPage);

        return view('appointment-form.index', [
            'appointments' => $appointments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $applicant       = new Applicant;
        $appointmentform = new AppointmentForm;
        if (isset($request->applicant_id)) {
            $applicant = $applicant->where('id', $request->applicant_id)
                ->first();
            $appointmentform = $appointmentform->where('applicant_id',$request->applicant_id)->first();
        }

        return view('appointment-form.create')->with([
            'applicant' => $applicant,
            'appointmentform' => $appointmentform,
            'action' => 'AppointmentFormController@store',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $appointmentform = AppointmentForm::find($request->appointmentform_id);
        if(empty($appointmentform)){
            $appointmentform = new AppointmentForm;
        }
        $appointmentform->fill($request->all());
        if($appointmentform->exists()){
            $appointmentform->updated_by = Auth::id();
            $response = 'The Appointment was successfully updated.';
        }else{
            $appointmentform->created_by = Auth::id();
            $response = 'The Appointment was successfully created.';
        }
        if($appointmentform->save()){
            // $assumption = $this->saveAssumption($request);
            // $oathoffice = $this->saveOath($request);
            $issued = $this->storeAppointee($request->applicant_id);
        }
        return redirect('/appointment-form')->with('success', $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AppointmentForm::destroy($id);
        return redirect('/appointment-form')->with('success', 'Appointment data deleted!');
    }

    public function saveAssumption($request){
        $assumption = Assumption::where('applicant_id',$request->applicant_id)->first();
        if(empty($assumption)){
            $assumption = new Assumption();
        }
        $assumption->fill($request->all());

        if($assumption->exists()){
            $assumption->updated_by = Auth::id();
        }else{
            $assumption->created_by = Auth::id();
        }
        $assumption->save();
    }

    public function saveOath($request){
        $oathoffice = OathOffice::where('applicant_id',$request->applicant_id)->first();
        if(empty($oathoffice)){
            $oathoffice = new OathOffice();
        }
        $oathoffice->fill($request->all());

        if($oathoffice->exists()){
            $oathoffice->updated_by = Auth::id();
        }else{
            $oathoffice->created_by = Auth::id();
        }
        $oathoffice->save();
    }

    public function storeAppointee($applicant_id){

        $appointment = AppointmentIssued::where('applicant_id',$applicant_id)->first();
        if (empty($appointment)) {
            $appointment = new AppointmentIssued;
        }
        if($appointment->exists()){
            $appointment->updated_by = Auth::id();
            $response = redirect('/interviews')->with('success', 'Applicant appointed was updated successfully.');
        }else{
            $appointment->created_by = Auth::id();

            $response = redirect('/interviews')->with('success', 'Applicant appointed was created successfully.');
        }
        $appointment->applicant_id = $applicant_id;
        $appointment->save();
    }
}
