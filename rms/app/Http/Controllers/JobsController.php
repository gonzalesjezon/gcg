<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests,
    App\Job,
    App\Office,
    App\Division,
    App\PSIPOP,
    Illuminate\Http\Request,
    Illuminate\Support\Facades\View;
use http\Env\Response;
use Illuminate\Http\JsonResponse;

class JobsController extends Controller
{
    /**
     * Define your validation rules in a property in
     * the controller to reuse the rules.
     */
    protected $validationRules = [
        'psipop_id' => 'required|max:255',
    ];

    /**
     * Call behavior handling for authentication
     * authentication section via middleware
     * =====================================
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'careers']]);
        View::share('title', 'Job Posting');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $perPage = 100;
        $jobs = Job::where('publish',1)
            ->where('status', '=', 'plantilla')
            ->latest()
            ->paginate($perPage);

        return view('jobs.index', [
            'jobs' => $jobs,
        ]);
    }

    /**
     * Display a listing of the resource [non-plantilla].
     *
     * @return \Illuminate\View\View
     */
    public function nonPlantilla(Request $request)
    {
        $perPage = 100;
        $jobs = Job::latest()
            ->where('status', '=', 'non-plantilla')
            ->paginate($perPage);

        return view('jobs.non-plantilla', [
            'jobs' => $jobs,
            'division' => config('params.division')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $request \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $job = new Job();
        if (empty($job->status)) {
            $job->status = $request->type;
        }
        $office = Office::pluck('name','id')->toArray();
        $division = Division::pluck('name','id')->toArray();
        $psipop = PSIPOP::where('status',0)
        ->pluck('position_title','id')
        ->toArray();

        return view('jobs.create')->with([
            'job' => $job,
            'offices' => $office,
            'divisions' => $division,
            'status' => $request->status,
            'psipop' => $psipop
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $this->validate($request, $this->validationRules);
        $job = new Job;
        $job->fill($request->all());
        if (!empty($job->publish)) {
            $job->publish = 1;
        }
        $job->publication_1 = ($request->publication_1) ? 1 : 0;
        $job->publication_2 = ($request->publication_2) ? 1 : 0;
        $job->publication_3 = ($request->publication_3) ? 1 : 0;
        $job->publication_4 = ($request->publication_4) ? 1 : 0;
        $job->created_by = \Auth::id();
        $job->save();

        return redirect()
            ->route('jobs.edit', [
                'id' => $job->id,
                'status' => $job->status
            ])
            ->with('success', 'The job post was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $job = Job::findOrFail($id);

        return view('jobs.show', compact('job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id, Request $request)
    {

        $job = Job::findOrFail($id);
        $office = Office::pluck('name','id')->toArray();
        $division = Division::pluck('name','id')->toArray();

        $currentPsipop = new PSIPOP();
        if(isset($job->psipop_id)){
            $currentPsipop = $currentPsipop->find($job->psipop_id);
        }

        $psipop = PSIPOP::pluck('position_title','id')->where('status',0)->toArray();

        return view('jobs.edit', [
            'job' => $job,
            'status' => $request->status,
            'offices' => $office,
            'divisions' => $division,
            'psipop' => $psipop,
            'currentPsipop' => $currentPsipop,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, $this->validationRules);

        $job = Job::findOrFail($id);
        $job->fill($request->all());
        $job->publish = (!empty($request->publish)) ? 1 : 0;
        $job->publication_1 = ($request->publication_1) ? 1 : 0;
        $job->publication_2 = ($request->publication_2) ? 1 : 0;
        $job->publication_3 = ($request->publication_3) ? 1 : 0;
        $job->publication_4 = ($request->publication_4) ? 1 : 0;

        $job->update();

        return redirect()
            ->route('jobs.edit', [
                'id' => $job->id,
                'status' => $job->status
            ])
            ->with('success', 'The job post was successfully updated.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id job id value
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish(Request $request)
    {
        $job = Job::findOrFail($request->id);
        $job->publish = (empty($job->publish)) ? 1 : 0;
        $job->update();

        return \response()->json([
                'status' => 'success',
                'data' => [
                    'id' => $job->id
                ],
            ]
            , 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Job::destroy($id);

        return redirect('jobs')->with('success', 'Job post deleted!');
    }

    public function getPsipop(Request $request){
        $currentPsipop = new PSIPOP();
        $job = new Job();
        $page = 'create';
        if(isset($request->job_id)){
            $job = $job->find($request->job_id);
            $page = 'edit';
        }

        if(isset($request->psipop_id)){
            $currentPsipop = $currentPsipop->find($request->psipop_id);
        }
        $psipop = PSIPOP::pluck('position_title','id')->where('status',0)->toArray();

        return view('jobs.'.$page, [
            'psipop' => $psipop,
            'currentPsipop' => $currentPsipop,
            'status' => $request->request_status,
            'job' => $job
        ]);
    }
}
