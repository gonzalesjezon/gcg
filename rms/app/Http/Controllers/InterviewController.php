<?php

namespace App\Http\Controllers;

use App\Examination;
use App\Interview;
use App\Job;
use App\SelectionLineup;
use App\Applicant;
use App\AppointmentForm;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

class InterviewController extends Controller
{

    public function __construct()
    {
        View::share('title', 'Schedule of Interview');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $interview = Interview::latest()
        ->paginate($perPage);

        return view('interviews.index',[
            'interviews' => $interview,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobs = Job::leftJoin('psipop as p','p.id','=','jobs.psipop_id')
            ->where('publish',1)
            ->select('p.position_title as title', 'jobs.id as id')->getModels();

            return view('interviews.create',[
                'jobs' => $jobs,
                'method' => 'POST',
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview =  new Interview();
        $interview->fill($request->all());
        $interview->notify = ($request->notify) ? 1 : 0;
        $interview->created_by = Auth::id();

        if($request->interview_status == 3){
            $appointment = $this->storeAppointee($request->applicant_id);
        }

        if($interview->notify == 1){
            $message['status'] = $request->interview_status;
            $message['data'] = $interview;
            $message['type'] = 'interview';

            $this->mail($request->email, 'Subject', $message);
        }

        $interview->save();

        return redirect()
            ->route('interviews.edit',[
                'interview_id' => $interview->id,
            ])->with('success', 'The interview status was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function show(Interview $interview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $interview = Interview::find($id);
        $applicant = Applicant::find($interview->applicant_id);
        $currentJob = 0;
        $selected_applicant = [];
        if(isset($applicant->job_id)){
            $currentJob = Job::find($applicant->job_id);
            $applicants = Applicant::where('job_id',$applicant->job_id)
            ->pluck('id')->toArray();

            $selected_applicant = Examination::whereIn('applicant_id',$applicants)
            ->where('exam_status',7)->getModels();
        }

        $jobs = Job::leftJoin('psipop as p','p.id','=','jobs.psipop_id')
            ->where('publish',1)
            ->select('p.position_title as title', 'jobs.id as id')->getModels();

        return view('interviews.edit',[
            'jobs' => $jobs,
            'currentJob' => $currentJob,
            'selected' => $selected_applicant,
            'interview' => $interview,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $interview = Interview::find($request->interview_id);
        $interview->fill($request->all());
        $interview->notify = ($request->notify) ? 1 : 0;
        $interview->updated_by = Auth::id();
        $interview->save();

        if($request->interview_status == 3){
            $appointment = $this->storeAppointee($request->applicant_id);
        }

        if($interview->notify == 1){
            $message['status'] = $request->interview_status;
            $message['data'] = $interview;
            $message['type'] = 'interview';

            $this->mail($request->email, 'Subject', $message);
        }

        $jobs = Job::leftJoin('psipop as p','p.id','=','jobs.psipop_id')
            ->where('publish',1)
            ->select('p.position_title as title', 'jobs.id as id')->getModels();

        return redirect()
            ->route('interviews.edit', [
                'interview_id' => $interview->id,
            ])
            ->with('success', 'The interview status was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Interview::destroy($id);
        return redirect('interviews')->with('success', 'Interview schedule deleted!');
    }

    public function getApplicant(Request $request){

        $applicants = new Applicant();

        $currentJob = 0;
        if(isset($request->job_id)){
            $currentJob = Job::find($request->job_id);
            $applicants = $applicants
            ->where('job_id',$request->job_id)
            ->pluck('id')->toArray();
        }

        $selected_applicant = Examination::whereIn('applicant_id',$applicants)
            ->where('exam_status',7)->getModels();

        $jobs = Job::leftJoin('psipop as p','p.id','=','jobs.psipop_id')
            ->where('publish',1)
            ->select('p.position_title as title', 'jobs.id as id')->getModels();

        return view('interviews.create',[
            'selected' => $selected_applicant,
            'jobs' => $jobs,
            'currentJob' => $currentJob,
        ]);

    }

    public function storeAppointee($applicant_id){

        $appointment = AppointmentForm::where('applicant_id',$applicant_id)->first();
        if (empty($appointment)) {
            $appointment = new AppointmentForm;
        }
        if($appointment->exists()){
            $appointment->updated_by = Auth::id();
            $response = redirect('/interviews')->with('success', 'Applicant appointed was updated successfully.');
        }else{
            $appointment->created_by = Auth::id();

            $response = redirect('/interviews')->with('success', 'Applicant appointed was created successfully.');
        }
        $appointment->applicant_id = $applicant_id;
        $appointment->save();
    }

    public function mail($to, $subject, $message){

        Mail::to($to)->send(new SendMailable($message));

    }
}
