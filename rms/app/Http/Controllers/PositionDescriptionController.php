<?php

namespace App\Http\Controllers;

use App\AppointmentForm;
use App\Applicant;
use App\PositionDescription;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PositionDescriptionController extends Controller
{

    /**
     * Modify the globally used view variable here
     * initialization found on app\Providers\AppServiceProvider
     */
    public function __construct()
    {
        View::share('title', 'Position Description');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $appointments = AppointmentForm::latest()
            ->paginate($perPage);

        return view('position-descriptions.index', [
            'appointments' => $appointments
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $applicant       = new Applicant;
        $position_desc = new PositionDescription;
        if (isset($request->applicant_id)) {
            $applicant = $applicant->where('id', $request->applicant_id)
                ->first();
            $position_desc = $position_desc->where('applicant_id',$request->applicant_id)->first();
        }

        return view('position-descriptions.create')->with([
            'applicant' => $applicant,
            'position_desc' => $position_desc,
            'action' => 'PositionDescriptionController@store',
            'option' => config('params.option_1')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $position_desc = PositionDescription::find($request->position_desc_id);
        if(empty($position_desc)){
            $position_desc = new PositionDescription;
        }

        $position_desc->fill($request->all());
        if($position_desc->exists()){
            $position_desc->updated_by = Auth::id();
            $response = 'The Postion Description was successfully updated.';
        }else{
            $position_desc->created_by = Auth::id();
            $response = 'The Postion Description was successfully created.';
        }

        $position_desc->save();
        return redirect('/position-descriptions')->with('success', $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function show(PositionDescription $positionDescription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PositionDescription $positionDescription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PositionDescription  $positionDescription
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PositionDescription::destroy($id);
        return redirect('/position-descriptions')->with('success', 'Position description data deleted!');
    }

    public function posDescriptionReport(Request $request)
    {
        $pos_desc = new PositionDescription;
        if($request->id){
            $pos_desc = PositionDescription::find($request->id);
        }
        return view('position-descriptions.report',[
            'pos_desc' => $pos_desc
        ]);
    }
}
