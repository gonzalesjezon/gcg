<?php

namespace App\Http\Controllers;

use App\ErasureAlteration;
use App\Applicant;
use App\Job;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ErasureAlterationController extends Controller
{

    public function __construct()
    {
        View::share('title', 'Erasures and Alteration');
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 100;
        $erasures = ErasureAlteration::latest()
        ->paginate($perPage);

        return view('erasure_alterations.index', [
            'erasures' => $erasures
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobs = Job::leftJoin('psipop as p','p.id','=','jobs.psipop_id')
            ->where('publish',1)
            ->select('p.position_title as title', 'jobs.id as id')->getModels();

        return view('erasure_alterations.create', [
            'jobs' => $jobs
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $erasures = new ErasureAlteration();
        $erasures->fill($request->all());
        $erasures->created_by = Auth::id();

        $erasures->save();

       return redirect('/erasure_alterations')->with('success', 'Erasure and alteration was successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ErasureAlteration  $erasureAlteration
     * @return \Illuminate\Http\Response
     */
    public function show(ErasureAlteration $erasureAlteration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ErasureAlteration  $erasureAlteration
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $erasures = new ErasureAlteration();
        if ($id) {
            $erasures = ErasureAlteration::find($id)->first();
        }

        $jobs = Job::leftJoin('psipop as p','p.id','=','jobs.psipop_id')
            ->where('publish',1)
            ->select('p.position_title as title', 'jobs.id as id')->getModels();

        $applicants = Applicant::where('qualified',1)->getModels();

        return view('erasure_alterations.edit')->with([
            'erasures' => $erasures,
            'jobs' => $jobs,
            'applicants' => $applicants
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ErasureAlteration  $erasureAlteration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $erasures = ErasureAlteration::find($id);
        $erasures->fill($request->all());
        $erasures->updated_by = Auth::id();

        $erasures->save();

       return redirect('/erasure_alterations')->with('success', 'Erasure and alteration was successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ErasureAlteration  $erasureAlteration
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ErasureAlteration::destroy($id);
        return redirect('erasure_alterations')->with('success', 'Erasure and alteration record deleted!');
    }


    public function getApplicant(Request $request){

        $applicants = new Applicant();

        $currentJob = 0;
        if(isset($request->job_id)){
            $currentJob = Job::find($request->job_id);
        }
        $applicants = $applicants
        ->where('qualified',1)
        ->getModels();

        $jobs = Job::leftJoin('psipop as p','p.id','=','jobs.psipop_id')
            ->where('publish',1)
            ->select('p.position_title as title', 'jobs.id as id')->getModels();

        return view('erasure_alterations.create',[
            'applicants' => $applicants,
            'jobs' => $jobs,
            'currentJob' => $request->job_id
        ]);

    }

    public function report(Request $request){

        $erasures = ErasureAlteration::find($request->id)->first();

        return view('erasure_alterations.report',[
            'erasures' => $erasures
        ]);
    }
}
