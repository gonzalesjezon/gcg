<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eligibility extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'eligibilities';
    protected $fillable = [
		'applicant_id',
		'name',
		'rating',
		'exam_place',
		'license_number',
		'license_validity',
		'exam_date'

    ];

    public function applicants(){
    	return $this->belongsTo('App\Applicant');
    }
}
