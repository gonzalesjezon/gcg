<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PSIPOP extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'psipop';

    protected $fillable = [
		'employee_status',
        'applicant_id',
		'office_id',
		'division_id',
		'department_id',
		'section_id',
		'salary_grade',
		'step',
		'position_title',
		'item_number',
		'code',
		'level',
		'type',
        'status',
        'annual_authorized_salary',
        'annual_actual_salary',
        'ppa_attribution'

    ];

    public function office(){
    	return $this->belongsTo('App\Office');
    }

    public function division(){
    	return $this->belongsTo('App\Division');
    }

    public function department(){
    	return $this->belongsTo('App\Department');
    }

    public function job(){
        return $this->belongsTo('App\Job');
    }

    public function applicant(){
        return $this->belongsTo('App\Applicant');
    }


}
