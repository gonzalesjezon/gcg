<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'divisions';

    protected $fillable = [
    	'name'
    ];

    public function psipop(){
    	return $this->belongsTo('App\PSIPOP');
    }
}
