<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'psipop_id',
        'description',
        'education',
        'experience',
        'training',
        'eligibility',
        'duties_responsibilities',
        'key_competencies',
        'monthly_basic_salary',
        'daily_salary',
        'pera_amount',
        'clothing_amount',
        'midyear_amount',
        'yearend_amount',
        'cashgift_amount',
        'status',
        'requirements',
        'compentency_1',
        'compentency_2',
        'compentency_3',
        'compentency_4',
        'compentency_5',
        'expires',
        'deadline_date',
        'publish',
        'station',
        'reporting_line',
        'publication'

    ];

    /**
     * Relation: Job has many applicants
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicants()
    {
        return $this->hasMany('App\Applicant');
    }

    public function psipop(){
        return $this->belongsTo('App\PSIPOP');
    }

    public function evaluations()
    {
        return $this->hasMany('App\Evaluation');
    }

    public function office(){
        return $this->belongsTo('App\Office');
    }

    public function division(){
        return $this->belongsTo('App\Division');
    }
}
