$(document).ready(function () {

	$("#bAdd").click(function () {
      $.get("trn.e2e.php",
      {
         fn:"LoadDefValue",
         trnmode:"NEW",
         json:$("#json").val()
      },
      function(data,status) {
         if (status == "success") {
            try {
               $("#bSave, #bCancel").show();
               $("#DataEntry").modal();
               eval(data);
            } catch (e) {
               if (e instanceof SyntaxError) {
                  alert(e.message);
               }
            }
         }
         else {
            alert("Ooops Error : " + status + "[005]");
         }
      });
	});

	$("#bCancel").click(function () {
	  $("#DataEntry").modal("hide");
	});

	$("#bSave").click(function () {
		if (saveProceed() == 0) {
	      $("#fn").val("TrnSaveRecord");
			$.ajax({
		    	url: "trn.e2e.php",
		      	type: "POST",
		      	data: new FormData($("[name='xForm']")[0]),
		      	success : function(responseTxt){
		      		eval(responseTxt);
		      	},
		      	enctype: 'multipart/form-data',
		      	processData: false,
		      	contentType: false,
		      	cache: false
		    });
		}
	});
});

function modifyRecord(refid) {
   $("#DataEntry").modal();
   LoadRecord(refid,"MODIFY");
}

function viewRecord(refid) {
   $("#bSave, #bCancel").hide();
	$("#DataEntry").modal();
   LoadRecord(refid,"VIEW");
}

function LoadRecord(refid,trnmode) {
   $.get("trn.e2e.php",
   {
      fn:"LoadRecord",
      refid:refid,
      trnmode:trnmode,
      json:$("#json").val()
   },
   function(data,status) {
      if (status == "success") {
         try {
            eval(data);
         } catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
      }
      else {
         alert("Ooops Error : " + status + "[005]");
      }
   });

}