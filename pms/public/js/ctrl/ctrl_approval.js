$(document).ready(function () {
   $(".workSched").click(function () {
      $("#workSched_modal").modal();
   });

   $("[name*='btnApproved']").each(function() {
      $(this).click(function() {
         if ($("#hTable").val() == "") {
            alert("Ooops!!! No Table Assigned");
            return;
         }
         $.post("trn.e2e.php",
         {
            fn:"PostStatus",
            hUser: $("#hUser").val(),
            tbl:$("#hTable").val(),
            refid:$(this).attr("refid"),
            apprvBy:$("#hEmpRefId").val(),
            status:"Approved"
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);
                  gotoscrn($("#hProg").val(),"");
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
            else {
               alert("Ooops Error : " + status);
            }
         });
      });
   });

   $("[name*='btnReject']").each(function() {
      $(this).click(function() {
         $("#hRefIdReject").val($(this).attr("refid"));
         if ($("#hRefIdReject").val() > 0) {
            $("#modalReject").modal();
         } else {
            alert("Ooops!!! No RefId Assigned");
         }

      });
   });

   $("#PostReason").click(function () {

      if ($("#hTable").val() == "") {
         alert("Ooops!!! No Table Assigned");
         return;
      }

      $.post("trn.e2e.php",
      {
         fn:"PostStatus",
         hUser: $("#hUser").val(),
         tbl:$("#hTable").val(),
         refid:$("#hRefIdReject").val(),
         reason:$("[name='char_Reason']").val(),
         status:"Rejected"
      },
      function(data,status) {
         if (status == "success") {
            try {
               eval(data);
               gotoscrn($("#hProg").val(),"");
            } catch (e) {
               if (e instanceof SyntaxError) {
                  alert(e.message);
               }
            }
         }
         else {
            alert("Ooops Error : " + status);
         }
      });
   });
});