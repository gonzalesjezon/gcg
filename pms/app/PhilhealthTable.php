<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhilhealthTable extends Model
{
    protected $table = 'philhealth_tables';
    protected $fillable = [
    	'salary_bracket',
    	'monthly_salary_range',
    	'salary_base',
    	'monthly_contribution',
    	'ee_share',
    	'er_share',
        'effectivity_date',
    	'remarks'
    ];
}
