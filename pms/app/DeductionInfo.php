<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeductionInfo extends Model
{
    protected $table = 'pms_deductioninfo';
    protected $fillable = [
        'employee_number',
    	'employeeinfo_id',
        'employee_id',
    	'deduction_id',
    	'deduct_pay_period',
    	'deduct_amount',
    	'deduct_date_start',
    	'deduct_date_end',
    	'deduct_date_terminated',
        'terminated'
    ];

    public function deductions(){
        return $this->belongsTo('App\Deduction','deduction_id');
    }

    public function employeeinfo(){
        return $this->belongsTo('App\EmployeeInfo','employeeinfo_id');
    }

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }
}
