<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RataDeduction extends Model
{
    protected $table = 'pms_rata_deductions';
    protected $fillable = [
    	'employee_id',
        'employee_number',
    	'rata_id',
    	'deduction_id',
    	'tax_deduction',
    	'ra_deduction',
    	'ta_deduction',
    	'year',
    	'month',
    	'created_by',
    	'updated_by'
    ];
}
