<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'pms_transactions';
    protected $fillable = [
        'employee_id',
        'employee_number',
        'salaryinfo_id',
        'division_id',
        'company_id',
        'position_item_id',
        'position_id',
        'office_id',
        'department_id',
        'empstatus_id',
        'employeeinfo_id',
        'actual_workdays',
        'adjust_workdays',
        'actual_absences',
        'adjust_absences',
        'actual_tardiness',
        'adjust_tardiness',
        'actual_undertime',
        'adjust_undertime',
        'gsis_ee_share',
        'gsis_er_share',
        'actual_basicpay_amount',
        'adjust_basicpay_amount',
        'total_basicpay_amount',
        'actual_absences_amount',
        'adjust_absences_amount',
        'total_absences_amount',
        'actual_tardines_amount',
        'adjust_tardines_amount',
        'total_tardines_amount',
        'actual_undertime_amount',
        'adjust_undertime_amount',
        'total_undertime_amount',
        'basic_net_pay',
        'actual_contribution',
        'adjust_contribution',
        'total_contribution',
        'actual_loan',
        'adjust_loan',
        'total_loan',
        'actual_otherdeduct',
        'adjust_otherdeduct',
        'total_otherdeduct',
        'net_deduction',
        'ecc_amount',
        'tax_amount',
        'gross_pay',
        'gross_taxable_pay',
        'pagibig_loan',
        'gsis_loan',
        'net_pay',
        'pay_period',
        'hold',
        'hold_status',
        'year',
        'month',
        'updated_by',
        'additional_tax_amount',
        'responsibility_id',
        'remittance_remarks',
        'over_remittance',
        'net_first_half',
        'net_second_half',
        'additional_adj',
        'deduction_adj',

    ];

    public function employees(){
    	return $this->belongsTo('App\Employee','employee_id');
    }

    public function divisions(){
    	return $this->belongsTo('App\Employee','division_id');
    }

    public function companies(){
    	return $this->belongsTo('App\Company','company_id');
    }

    public function positionitems(){
    	return $this->belongsTo('App\PositionItem','position_item_id');
    }

    public function employeepositions(){
        return $this->belongsTo('App\PositionItem','position_item_id')->with('positions');
    }

    public function offices(){
    	return $this->belongsTo('App\Office','office_id');
    }

    public function departments(){
    	return $this->belongsTo('App\Department','department_id');
    }

    public function empstatus(){
    	return $this->belongsTo('App\EmployeeStatus','empstatus_id');
    }

    public function employeeinfo(){
        return $this->belongsTo('App\EmployeeInfo','employeeinfo_id');
    }

    public function employeeinformation(){
        return $this->belongsTo('App\EmployeeInformation','employee_id','employee_id');
    }

    public function salaryinfo(){
        return $this->belongsTo('App\SalaryInfo','salaryinfo_id')->with('jobgrade');
    }
    public function loaninfo(){
        return $this->belongsTo('App\LoanInfo','employee_id');
    }

    public function positions(){
        return $this->belongsTo('App\Position','position_id');
    }

    public function responsibilities(){
        return $this->belongsTo('App\ResponsibilityCenter','responsibility_id');
    }

    public function benefitinfo(){
        return $this->belongsTo('App\BenefitInfo','employee_id','employee_id')->with('benefits');
    }

    public function benefitTransactions(){
        return $this->belongsTo('App\BenefitInfoTransaction','employee_id','employee_id')->with('benefits');
    }
    public function loaninfoTransaction(){
        return $this->hasMany('App\LoanInfoTransaction','employee_id','employee_id')->with('loans');
    }

    public function geteme(){
        return $this->hasMany('App\BenefitInfoTransaction','employee_id','employee_id')->with('benefits');
    }

    public function special(){
        return $this->hasMany('App\SpecialPayrollTransaction','employee_id','employee_id');
    }

    public function transactions(){
        return $this->hasMany('App\Transaction','employee_id');
    }

}
