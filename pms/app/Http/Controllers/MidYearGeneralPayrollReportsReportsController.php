<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SpecialPayrollTransaction;
use Input;
class MidYearGeneralPayrollReportsReportsController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL';
    	$this->module = 'midyeargeneralpayroll';
        $this->module_prefix = 'payrolls/reports/midyearreports';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new SpecialPayrollTransaction;

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $query['transaction'] = $transaction
        ->with(['salaryinfo','positions','employees'])
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','midyearbonus')
        ->get();

       return json_encode($query);
    }
}
