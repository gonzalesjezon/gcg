<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Crypt;
use App\OvertimePay;
use App\Transaction;
use App\EmployeeStatus;
use App\Employee;
use App\EmployeeInfo;
use App\EmployeeInformation;
use App\BenefitInfo;
use App\SalaryInfo;
use App\Benefit;
use App\LongevityPay;
use App\SpecialPayrollTransaction;
use App\AnnualTaxPolicy;
use App\AnnualTaxRate;
use App\SpecialAnnualTax;
use App\BeginningBalance;
use App\PreviousEmployer;
use App\Deduction;
use App\DeductionInfo;
use Carbon\Carbon;
use Session;
use DateTime;
use Auth;
use App\Rate;
class AnnualTaxSetupController extends Controller
{
    function __construct(){
        $this->title = 'ANNUAL TAXATION SETUP';
        $this->controller = $this;
        $this->module = 'annualtaxsetup';
        $this->module_prefix = 'payrolls/admin';
    }

     public function index(){

        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title
                        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q              = Input::get('q');
        $year           = Input::get('year');
        $month          = Input::get('month');

        $data = $this->searchName($q);


        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }
    public function searchName($q){

        $employeestatus             = new EmployeeStatus;
        $employeeinfo              = new EmployeeInformation;
        $employee                   = new Employee;
        $transaction                = new Transaction;

        $cols = ['lastname','firstname'];

        $empstatus_id = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo_id = $employeeinfo
        ->whereIn('employee_status_id',$empstatus_id)
        ->select('employee_id')->get()->toArray();

        $query = $employee->whereIn('id',$employeeinfo_id)
            ->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }


    public function getEmployeesinfo(){

        $data = Input::all();

        $year        = $data['year'];
        $employee_id = $data['id'];

        $transaction        = new Transaction;
        $overtimepay        = new OvertimePay;
        $salaryinfo         = new SalaryInfo;
        $benefit            = new Benefit;
        $benefitTransaction = new BenefitInfo;
        $employeeinfo       = new EmployeeInfo;
        $longevitypay       = new LongevityPay;
        $specialpayroll     = new SpecialPayrollTransaction;
        $taxrates           = new AnnualTaxRate;
        $specialtax         = new SpecialAnnualTax;
        $beginningbalance   = new BeginningBalance;
        $previousemployer   = new PreviousEmployer;
        $deduction          = new Deduction;
        $deducationinfo     = new DeductionInfo;

        $rataCode = ['RA','TA'];

        $peraId = $benefit
        ->where('code','PERA')
        ->select('id')
        ->first();

        $deductionId = $deduction
        ->where('name','EA')
        ->first();

        $rataId = $benefit
        ->whereIn('code',$rataCode)
        ->select('id')
        ->get()
        ->toArray();

        $hpId = $benefit
        ->where('code','HP')
        ->select('id')
        ->first();

        $query['salaryinfo'] = $salaryinfo
        ->where('employee_id',$employee_id)
        ->orderBy('salary_effectivity_date','desc')
        ->first();

        $query['employeeinfo'] = $employeeinfo
        ->with('employees')
        ->where('employee_id',$employee_id)
        ->orderBy('created_at','desc')
        ->first();

        $query['transaction'] = $transaction
        ->where('year',$year)
        ->where('employee_id',$employee_id)
        ->get();

        $query['overtimepay'] = $overtimepay
        ->where('year',$year)
        ->where('employee_id',$employee_id)
        ->where('used_amount','!=',null)
        ->orderBy('created_at','asc')
        ->get();

        $query['pera'] = $benefitTransaction
        ->where('benefit_id',$peraId->id)
        ->where('employee_id',$employee_id)
        ->first();

        $query['hp'] = $benefitTransaction
        ->where('benefit_id',@$hpId->id)
        ->where('employee_id',$employee_id)
        ->first();

        $query['ea'] = $deducationinfo
        ->where('deduction_id',@$deductionId->id)
        ->where('employee_id',$employee_id)
        ->select('deduct_amount')
        ->first();

        $query['lp'] = $longevitypay
        ->where('employee_id',$employee_id)
        ->orderBy('longevity_date','desc')
        ->first();

        $query['rata'] = $benefitTransaction
        ->with('benefits')
        ->whereIn('benefit_id',$rataId)
        ->where('employee_id',$employee_id)
        ->get();

        $query['yearendbonus'] = $this->getYearEndBonus($employee_id);
        $query['midyearbonus'] = $this->getMidYearBonus($employee_id);

        $query['pbb'] = $specialpayroll
        ->where('employee_id',$employee_id)
        ->where('year',$year)
        ->where('status','pbb')
        ->sum('amount');

        $query2 = $this->getPei($employee_id);

        $query['pei'] = $query2['pei_amount'];
        $query['cna'] = $query2['cna_amount'];

        $query['taxrates'] = $taxrates
        ->where('employee_id',$employee_id)
        ->where('for_year',$year)
        ->get();

        $query['specialtax'] = $specialtax
        ->where('employee_id',$employee_id)
        ->where('for_year',$year)
        ->first();

        $query['previousemployer'] = $previousemployer
        ->where('employee_id',$employee_id)
        ->first();

        $query['beginningbalance'] = $beginningbalance
        ->where('employee_id',$employee_id)
        ->first();

        return json_encode($query);
    }

    public function getAnnualTaxPolicy(){
        $data = Input::all();

        $year        = $data['year'];
        $tax_income  = $data['tax_income'];
        $employee_id = $data['employee_id'];
        // $employee_number = $data['employee_number'];
        $count       = @$data['limit'];
        $count = $count - 1;
        $annualtax = new AnnualTaxPolicy;
        $taxrates  = new AnnualTaxRate;
        $specialtax = new SpecialAnnualTax;
        $takeAll = $taxrates->count();

        if($takeAll !== 0){
            $take = $takeAll - (int)$count;
        }else{
            $take = $count;
        }

        $annualtax = $annualtax
        ->where('from_year','<=',$year)
        ->where('to_year','>=',$year)
        ->orderBy('below_amount','asc')
        ->get();

        $query = [];

        $query['taxrates'] = $taxrates
        ->where('employee_id',$employee_id)
        // ->where('employee_number',$employee_number)
        ->where('for_year',$year)
        // ->take($count)
        ->get();

        $query['specialtax'] = $specialtax
        ->where('employee_id',$employee_id)
        // ->where('employee_number',$employee_number)
        ->where('for_year',$year)
        ->first();

        $query['rate_id'] = $taxrates
        ->where('employee_id',$employee_id)
        // ->where('employee_number',$employee_number)
        ->where('for_year',$year)
        ->orderBy('for_year','desc')
        // ->skip($count)
        // ->take($take)
        ->get();

        $query['recordsCount'] = count($query['taxrates']);

        foreach ($annualtax as $key => $value) {
            $below_amount = $value->below_amount;
            $above_amount = $value->above_amount;

            if($tax_income > $below_amount && $tax_income < $above_amount){
                $query['rates'] = $value;
                return $query;
            }
        }

        return $query;

    }

    public function store(Request $request){

        $dataTax    = array_filter(explode(',', $request->array_tax));
        $dataPera   = array_filter(explode(',', $request->array_pera));
        $dataCont   = array_filter(explode(',', $request->array_cont));
        $dataRata   = array_filter(explode(',', $request->array_rata));
        $dataBasic  = array_filter(explode(',', $request->array_basic));
        $dataLP     = array_filter(explode(',', $request->array_lp));
        $dataHP     = array_filter(explode(',', $request->array_hp));
        $dataOT     = array_filter(explode(',', $request->array_ot));
        $dataLWOP   = array_filter(explode(',', $request->array_lwop));
        $dataMonth  = array_filter(explode(',', $request->array_month));
        $dataId     = array_filter(explode(',', $request->array_id));

        $year = $request->for_year;
        $loyalty_amount = ($request->loyalty_amount !== null) ? str_replace(',', '', $request->loyalty_amount) : 0;
        $pb_amount = ($request->pb_amount !== null) ? str_replace(',', '', $request->pb_amount) : 0;
        $cn_amount = ($request->cn_amount !== null) ? str_replace(',', '', $request->cn_amount) : 0;
        $pei_amount = ($request->pei_amount !== null) ? str_replace(',', '', $request->pei_amount) : 0;
        $myb_amount = ($request->myb_amount !== null) ? str_replace(',', '', $request->myb_amount) : 0;
        $yeb_amount = ($request->yeb_amount !== null) ? str_replace(',', '', $request->yeb_amount) : 0;
        $clothing_amount = ($request->total_clothing_amount !== null) ? str_replace(',', '', $request->total_clothing_amount) : 0;
        $cash_gift_amount = ($request->total_cg_amount !== null) ? str_replace(',', '', $request->total_cg_amount) : 0;

        $thirteen_month_amount = ($request->thirteen_month !== null) ? str_replace(',', '', $request->thirteen_month) : 0;
        $mandatory_amount = ($request->mandatory_deductions !== null) ? str_replace(',', '', $request->mandatory_deductions) : 0;
        $gross_taxable_income_amount = ($request->gross_taxable_income_amount !== null) ? str_replace(',', '', $request->gross_taxable_income_amount) : 0;
        $tax_withheld = ($request->tax_withheld !== null) ? str_replace(',', '', $request->tax_withheld) : 0;

        $thirteen_month_prev_employer = ($request->thirteen_month_prev_employer !== null) ? str_replace(',', '', $request->thirteen_month_prev_employer) : 0;
        $mandatory_deductions_prev_employer = ($request->mandatory_deductions_prev_employer !== null) ? str_replace(',', '', $request->mandatory_deductions_prev_employer) : 0;
        $gross_taxable_prev_employer_amount = ($request->gross_taxable_prev_employer_amount !== null) ? str_replace(',', '', $request->gross_taxable_prev_employer_amount) : 0;
        $tax_withheld_prev_employer = ($request->tax_withheld_prev_employer !== null) ? str_replace(',', '', $request->tax_withheld_prev_employer) : 0;

        if(count($dataTax) !== 1){
            if(count($dataId) > 0 && count($dataId) !== 1){
                $taxrates =  new AnnualTaxRate;

                foreach ($dataTax as $key => $value) {
                    if(!empty($value)){

                        $taxrates = $taxrates->find($dataId[$key]);
                        $taxrates->employee_id          = $request->employee_id;
                        $taxrates->tax_amount           = $dataTax[$key];
                        $taxrates->pera_amount          = $dataPera[$key];
                        $taxrates->contribution_amount  = $dataCont[$key];
                        $taxrates->rata_amount          = $dataRata[$key];
                        $taxrates->basic_amount         = $dataBasic[$key];
                        $taxrates->longevity_amount     = $dataLP[$key];
                        $taxrates->hp_amount            = $dataHP[$key];
                        $taxrates->lwop_amount          = $dataLWOP[$key];
                        $taxrates->overtime_amount      = $dataOT[$key];
                        $taxrates->for_month            = $dataMonth[$key];
                        $taxrates->for_year             = $year;
                        $taxrates->updated_by           = Auth::User()->id;
                        $taxrates->save();
                    }
                }

                if(isset($request->specialtax_id)){
                    $specialtax =  new SpecialAnnualTax;
                    $specialtax = $specialtax->find($request->specialtax_id);
                    $specialtax->employee_id                    = $request->employee_id;
                    $specialtax->clothing_allowance_amount      = $clothing_amount;
                    $specialtax->cash_gift_amount               = $cash_gift_amount;
                    $specialtax->loyalty_amount                 = $loyalty_amount;
                    $specialtax->performance_base_amount        = $pb_amount;
                    $specialtax->collective_negotiation_amount  = $cn_amount;
                    $specialtax->mid_year_amount                = $myb_amount;
                    $specialtax->year_end_amount                = $yeb_amount;
                    $specialtax->pei_amount                     = $pei_amount;
                    $specialtax->for_year                       = $year;

                    $specialtax->save();
                }


                $beginningbalance = new BeginningBalance;

                if(isset($request->begbalance_id)){
                    $beginningbalance = $beginningbalance->find($request->begbalance_id);
                    $beginningbalance->employee_id = $request->employee_id;
                    $beginningbalance->gross_taxable_income = $gross_taxable_income_amount;
                    $beginningbalance->thirteen_month_pay = $thirteen_month_amount;
                    $beginningbalance->tax_withheld = $tax_withheld;
                    $beginningbalance->mandatory_deduction = $mandatory_amount;
                    $beginningbalance->as_of_date = $request->as_of_date;

                    $beginningbalance->save();

                }


                $previousemployer = new PreviousEmployer;

                if(isset($request->prevemployer_id)){

                    $previousemployer = $previousemployer->find($request->prevemployer_id);
                    $previousemployer->employee_id = $request->employee_id;
                    $previousemployer->gross_taxable_income = $gross_taxable_prev_employer_amount;
                    $previousemployer->thirteen_month_pay = $thirteen_month_prev_employer;
                    $previousemployer->tax_withheld = $tax_withheld_prev_employer;
                    $previousemployer->mandatory_deduction = $mandatory_deductions_prev_employer;
                    $previousemployer->as_of_date = $request->as_of_date_prev_employer;
                    $previousemployer->save();

                }

                $response = json_encode(['status'=>true,'response'=>'Update Successfully']);

            }else{

                $ctr = 1;
                foreach ($dataBasic as $key => $value) {
                    if(isset($value) && $value !== ""){
                        $taxrates =  new AnnualTaxRate;
                        $taxrates->employee_id          = $request->employee_id;
                        $taxrates->tax_amount           = @$dataTax[$key];
                        $taxrates->pera_amount          = $dataPera[$key];
                        $taxrates->contribution_amount  = $dataCont[$key];
                        $taxrates->rata_amount          = (!empty($dataRata[$key])) ? $dataRata[$key] : 0;
                        $taxrates->basic_amount         = $dataBasic[$key];
                        $taxrates->longevity_amount     = (!empty($dataLP[$key])) ? $dataLP[$key] : 0;
                        $taxrates->hp_amount            = (!empty($dataHP[$key])) ? $dataHP[$key] : 0;
                        $taxrates->lwop_amount          = (!empty($dataLWOP[$key])) ? $dataLWOP[$key] : 0;
                        $taxrates->overtime_amount      = (!empty($dataOT[$key])) ? $dataOT[$key] : 0;
                        $taxrates->for_month            = $dataMonth[$key];
                        $taxrates->for_year             = $year;
                        $taxrates->created_by           = Auth::User()->id;
                        $taxrates->save();
                    }
                    $ctr++;

                }

                $specialtax =  new SpecialAnnualTax;
                $specialtax->employee_id                    = $request->employee_id;
                $specialtax->clothing_allowance_amount      = $clothing_amount;
                $specialtax->cash_gift_amount               = $cash_gift_amount;
                $specialtax->loyalty_amount                 = $loyalty_amount;
                $specialtax->performance_base_amount        = $pb_amount;
                $specialtax->collective_negotiation_amount  = $cn_amount;
                $specialtax->mid_year_amount                = $myb_amount;
                $specialtax->year_end_amount                = $yeb_amount;
                $specialtax->pei_amount                     = $pei_amount;
                $specialtax->for_year                       = $year;
                $specialtax->save();

                if(isset($request->as_of_date)){
                    $beginningbalance = new BeginningBalance;
                    $beginningbalance->employee_id = $request->employee_id;
                    $beginningbalance->gross_taxable_income = $gross_taxable_income_amount;
                    $beginningbalance->thirteen_month_pay = $thirteen_month_amount;
                    $beginningbalance->tax_withheld = $tax_withheld;
                    $beginningbalance->mandatory_deduction = $mandatory_amount;
                    $beginningbalance->as_of_date = $request->as_of_date;

                    $beginningbalance->save();
                }


                if(isset($request->as_of_date_prev_employer)){
                    $previousemployer = new PreviousEmployer;
                    $previousemployer->employee_id = $request->employee_id;
                    $previousemployer->gross_taxable_income = $gross_taxable_prev_employer_amount;
                    $previousemployer->thirteen_month_pay = $thirteen_month_prev_employer;
                    $previousemployer->tax_withheld = $tax_withheld_prev_employer;
                    $previousemployer->mandatory_deduction = $mandatory_deductions_prev_employer;
                    $previousemployer->as_of_date = $request->as_of_date_prev_employer;
                    $previousemployer->save();
                }



                $response = json_encode(['status'=>true,'response'=>'Save Successfully']);
            }
        }else{
            $response = json_encode(['status'=>false,'response'=>'Pleas click compute button']);
        }

        return $response;
    }

    private function getYearEndBonus($employee_id){

        $employeeinformation = new EmployeeInformation;
        $salaryinfo          = new SalaryInfo;
        $cash_gift = 5000;
        $arr = array();
        $employeeinfo    = $employeeinformation->where('employee_id',$employee_id)->first();
        $salary          = $salaryinfo->where('employee_id',$employee_id)->orderBy('salary_effectivity_date','desc')->first();
        $assumption_date = ($employeeinfo->assumption_date) ? $employeeinfo->assumption_date : '';

        $basic_amount_one = ($salary->salary_new_rate) ? $salary->salary_new_rate : 0;
        $basic_amount = $basic_amount_one;

        $first_date = new DateTime($assumption_date);
        $nov = date('Y').'-10-31 00:00:00';

        $second_date = DateTime::createFromFormat('Y-m-d H:i:s', $nov);
        $interval = $first_date->diff($second_date);

        // $x = $interval->format('%Y years %M months and %D days.');
        $number_of_month = (int)$interval->format('%M');
        $number_of_year = (int)$interval->format('%Y');

        $year_end_amount  = 0;
        $cash_gift_amount = 0;
        if($number_of_year !== 0){
            $year_end_amount   = $basic_amount;
            $cash_gift_amount  = $cash_gift;

        }else{

            if($number_of_month >= 10){

                $year_end_amount     = $basic_amount;
                $cash_gift_amount    = $cash_gift;

            }elseif($number_of_month >= 9 && $number_of_month < 10){

                $new_cash_gift      =  ((float)$cash_gift*.95);
                $new_basic          =  ((float)$basic_amount*.95);
                $year_end_amount    = $new_basic;
                $cash_gift_amount   = $new_cash_gift;

            }elseif($number_of_month >= 8 && $number_of_month < 9){

                $new_cash_gift      =  ((float)$cash_gift*.90);
                $new_basic          =  ((float)$basic_amount*.90);
                $year_end_amount    = $new_basic;
                $cash_gift_amount   = $new_cash_gift;

            }elseif($number_of_month >= 7 && $number_of_month < 8){

                $new_cash_gift      =  ((float)$cash_gift*.80);
                $new_basic          =  ((float)$basic_amount*.80);
                $year_end_amount    = $new_basic;
                $cash_gift_amount   = $new_cash_gift;

            }elseif($number_of_month >= 6 && $number_of_month < 7){

                $new_cash_gift      =  ((float)$cash_gift*.70);
                $new_basic          =  ((float)$basic_amount*.70);
                $year_end_amount    = $new_basic;
                $cash_gift_amount   = $new_cash_gift;

            }elseif($number_of_month >= 5 && $number_of_month < 6){

                $new_cash_gift      =  ((float)$cash_gift*.60);
                $new_basic          =  ((float)$basic_amount*.60);
                $year_end_amount    = $new_basic;
                $cash_gift_amount   = $new_cash_gift;

            }elseif($number_of_month >= 4 && $number_of_month < 5){

                $new_cash_gift      =  ((float)$cash_gift*.50);
                $new_basic          =  ((float)$basic_amount*.50);
                $year_end_amount    = $new_basic;
                $cash_gift_amount   = $new_cash_gift;

            }elseif($number_of_month >= 3 && $number_of_month < 4){

                $new_cash_gift      =  ((float)$cash_gift*.40);
                $cash_gift_amount   = $new_cash_gift;

            }elseif($number_of_month >= 2 && $number_of_month < 3){

                $new_cash_gift      =  ((float)$cash_gift*.30);
                $cash_gift_amount   = $new_cash_gift;

            }elseif($number_of_month >= 1 && $number_of_month < 2){

                $new_cash_gift      =  ((float)$cash_gift*.20);
                $cash_gift_amount   = $new_cash_gift;

            }else{

                $new_cash_gift      =  ((float)$cash_gift*.10);
                $cash_gift_amount   = $new_cash_gift;

            }
        }
        $arr["yearend"] = $year_end_amount;
        $arr["cashgift"] = $cash_gift_amount;

        return $arr;
    }

    private function getMidYearBonus($employee_id){

        $employeeinformation = new EmployeeInformation;
        $salaryinfo          = new SalaryInfo;

        $mid_year_amount = 0;

        $employeeinfo    = $employeeinformation->where('employee_id',$employee_id)->first();
        $salary          = $salaryinfo->where('employee_id',$employee_id)->orderBy('salary_effectivity_date','desc')->first();
        $assumption_date = ($employeeinfo->assumption_date) ? $employeeinfo->assumption_date : '';

        $basic_amount_one = ($salary->salary_new_rate) ? $salary->salary_new_rate : 0;
        $basic_amount = $basic_amount_one;

        $first_date = new DateTime($assumption_date);
        $nov = date('Y').'-05-30 00:00:00';

        $second_date = DateTime::createFromFormat('Y-m-d H:i:s', $nov);
        $interval = $first_date->diff($second_date);

        // $x = $interval->format('%Y years %M months and %D days.');
        $number_of_month = (int)$interval->format('%M');
        $number_of_year = (int)$interval->format('%Y');

        if($number_of_year !== 0){
            $mid_year_amount  = $basic_amount;
        }else{
            if($number_of_month >= 4){
                $mid_year_amount = $basic_amount;

            }else{
                $mid_year_amount = 0;
            }
        }

        return $mid_year_amount;
    }

    private function getPei($employee_id){

        $employeeinformation = new EmployeeInformation;
        $employee            = new Employee;

        $rate                = new Rate;
        $pei_amount = 5000;
        $cna_amount = 25000;

        $rate = $rate->where('status','pei')->get();

        $employeeinfo    = $employeeinformation->where('employee_id',$employee_id)->first();
        $employee        = $employee->where('id',$employee_id)->first();
        $assumption_date = ($employeeinfo->assumption_date) ? $employeeinfo->assumption_date : '';

        $first_date = new DateTime($assumption_date);
        $nov = date('Y').'-11-30 00:00:00';

        $second_date = DateTime::createFromFormat('Y-m-d H:i:s', $nov);
        $interval = $first_date->diff($second_date);

        // $x = $interval->format('%Y years %M months and %D days.');
        $number_of_month = (int)$interval->format('%M');
        $number_of_year = (int)$interval->format('%Y');

        $cnaAmount = 0;
        $peiAmount = 0;

        $arr = [];
        if($number_of_year !== 0){

            $cnaAmount = $cna_amount;
            $peiAmount = $pei_amount;

        }else{

            if($number_of_month >= 3 && $number_of_month <= 4){

                $new_amount     =  ((float)$pei_amount*.5);
                $cna_new_amount =  ((float)$cna_amount*.5);

                $cnaAmount            = $cna_new_amount;
                $peiAmount            = $new_amount;

            }elseif($number_of_month >= 2 && $number_of_month <= 3){

                $new_amount =  ((float)$pei_amount*.4);
                $cna_new_amount =  ((float)$cna_amount*.4);

                $cnaAmount            = $cna_new_amount;
                $peiAmount            = $new_amount;

            }elseif($number_of_month >= 1 && $number_of_month <= 2){

                $new_amount =  ((float)$pei_amount*.3);
                $cna_new_amount =  ((float)$cna_amount*.3);

                $cnaAmount            = $cna_new_amount;
                $peiAmount            = $new_amount;

            }else{

                $new_amount =  ((float)$pei_amount*.2);
                $cna_new_amount =  ((float)$cna_amount*.2);

                $cnaAmount            = $cna_new_amount;
                $peiAmount            = $new_amount;

            }
        }

        $arr['cna_amount'] = $cnaAmount;
        $arr['pei_amount'] = $peiAmount;


        return $arr;

    }



}
