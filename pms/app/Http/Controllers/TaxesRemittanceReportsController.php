<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\Transaction;
class TaxesRemittanceReportsController extends Controller
{
    function __construct(){
		$this->title = 'TAX REMITTANCE';
    	$this->module = 'taxesremittance';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$data = Input::all();
    	$transaction = new Transaction;

        $query = $transaction->with(['employeeinfo','employees'=>function($qry){ 
									        	$qry->orderBy('lastname','asc'); 
									        },'salaryinfo'])
                                              ->where('year',$data['year'])
                                              ->where('month',$data['month'])
                                              ->get()->toArray();


    	return json_encode($query);
    }
}
