<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
class PremiumContributionReportsController extends Controller
{
    function __construct(){
    	$this->title  = 'PREMIUM CONTRIBUTION';
    	$this->module = 'premiumcontributions';
    	$this->controller = $this;
    	$this->module_prefix = 'payrolls/reports';
    }

    public function index(){

    	$response =  array(
    		'controller'	=> $this->controller,
    		'module'		=> $this->module,
    		'title'			=> $this->title,
    		'module_prefix'	=> $this->module_prefix,
    	);

    	return view($this->module_prefix.'.'.$this->module,$response);

    }

    public function show(){

    	$data = Input::all();
    	$transaction = new Transaction;

        $query = $transaction->with('employees','employeeinfo','salaryinfo','employeepositions','positions')
        					 ->where('year',$data['year'])
                             ->where('month',$data['month'])
                                              ->get();

    	return json_encode($query);

    }
}
