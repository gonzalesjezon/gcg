<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeInfo;
use App\Transaction;
use App\LoanInfoTransaction;
use App\Employee;
use App\LoanInfo;
use App\Loan;
use App\Office;
use App\Benefit;
use App\EmployeeStatus;
use App\EmployeeInformation;
use Input;
class PayrollTransfersReportController extends Controller
{
    function __construct(){
    	$this->title = 'PAYROLL TRANSFERS';
    	$this->module = 'payrolltransfers';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

        $loan                = new Loan;
        $employee            = new Employee;
        $employeestatus      = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;


        $query = $loan->where('loan_type','GSIS')->get();
        $col   = count($query);

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $employee = $employee
        ->with([
            'employeeinformation' => function($qry){
                $qry->with('positions');
            },
        ])
        ->whereIn('id',$employeeinfo)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

        $response = array(
                        'employee'      => $employee,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    // WITH PAGINATION

    public function showWPage(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $transaction = new Transaction;
        $office      = new Office;
        $benefit     = new Benefit;
        $loaninfo    = new LoanInfoTransaction;
        $loan        = new Loan;

        $benefit = $benefit->where('name','PERA')->first();

        $data2 = $this->getDeductions($year,$month,$loan,$loaninfo);

        $gsisLoans = $data2['gsis_loans'];
        $loanGsisCount = $data2['gsis_loan_count'];
        $pagibigLoans = $data2['pagibig_loans'];
        $loanPagibigCount = $data2['pagibig_loan_count'];
        $loans = $data2['loans'];

        $query = $transaction
        ->leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->leftJoin('office as o','o.RefId','=','pms_transactions.office_id')
        ->with([
            'employees',
            'loaninfoTransaction' => function($qry) use($loans,$year,$month){
                $qry = $qry
                ->whereIn('loan_id',$loans);
            },
            'benefitTransactions' => function($qry) use($benefit,$year,$month){
                $qry = $qry
                ->where('year',$year)
                ->where('month',$month)
                ->where('benefit_id',$benefit->id);
            },
            'benefitinfo' => function($qry) use($benefit){
                $qry = $qry->where('benefit_id',$benefit->id);
            },
            'positionitems',
            'positions',
            'employeeinformation',
            'salaryinfo',
            'employeeinfo'
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('o.sort_by','asc')
        ->orderBy('e.lastname','asc')
        ->get();

        $data = [];
        if(count($query) > 0){
            foreach ($query as $key => $value) {
                $data[@$value->offices->Name][$key] = $value;
            }

        }else{
            $data = [];
        }

        $data2 = [];
        foreach ($data as $key => $value) {
            $data2[$key] = array_values($value);
        }


        $sameOffice = '';
        $data3 = [];
        $ctr = 1;
        $ctr2 = 1;
        foreach ($data2 as $key => $value) {
            foreach ($value as $k => $val) {
                if($ctr <= 21){
                    $data3[$ctr2][$key][$ctr] = $val;
                }else{
                    $ctr = 0;
                    $ctr2++;
                }
                $ctr++;
            }
            // $ctr  = 1;
            // $ctr2 = 1;
        }

        return json_encode([
            'transaction'=>$data3,
            'gsisLoanList'=>$gsisLoans,
            'gsisLoanCount'=> $loanGsisCount,
            'pagibigLoanList'=>$pagibigLoans,
            'pagibigLoanCount'=> $loanPagibigCount,
            'totalPage' => $ctr2
        ]);
    }

    public function show(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $transaction = new Transaction;
        $office      = new Office;
        $benefit     = new Benefit;
        $loaninfo    = new LoanInfoTransaction;
        $loan        = new Loan;

        $benefit = $benefit->where('name','PERA')->first();

        $data2 = $this->getDeductions($year,$month,$loan,$loaninfo);

        $gsisLoans = $data2['gsis_loans'];
        $loanGsisCount = $data2['gsis_loan_count'];
        $pagibigLoans = $data2['pagibig_loans'];
        $loanPagibigCount = $data2['pagibig_loan_count'];
        $loans = $data2['loans'];

        $query = $transaction
        ->leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->leftJoin('office as o','o.RefId','=','pms_transactions.office_id')
        ->with([
            'employees',
            'loaninfoTransaction' => function($qry) use($loans,$year,$month){
                $qry = $qry
                ->whereIn('loan_id',$loans);
            },
            'benefitTransactions' => function($qry) use($benefit,$year,$month){
                $qry = $qry
                ->where('year',$year)
                ->where('month',$month)
                ->where('benefit_id',$benefit->id);
            },
            'benefitinfo' => function($qry) use($benefit){
                $qry = $qry->where('benefit_id',$benefit->id);
            },
            'positionitems',
            'positions',
            'employeeinformation',
            'salaryinfo',
            'employeeinfo'
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('o.sort_by','asc')
        ->orderBy('e.lastname','asc')
        ->get();


        $data = [];
        if(count($query) > 0){
            foreach ($query as $key => $value) {
                $data[@$value->offices->Name][$key] = $value;
            }

        }else{
            $data = [];
        }


        // $data2 = [];
        // foreach ($data as $key => $value) {
        //     $data2[$key] = array_values($value);
        // }


        // $sameOffice = '';
        // $data3 = [];
        // $ctr = 1;
        // $ctr2 = 1;
        // foreach ($data2 as $key => $value) {
        //     foreach ($value as $k => $val) {
        //         if($ctr <= 21){
        //             $data3[$ctr2][$key][$ctr] = $val;
        //         }else{
        //             $ctr = 0;
        //             $ctr2++;
        //         }
        //         $ctr++;
        //     }
        //     // $ctr  = 1;
        //     // $ctr2 = 1;
        // }


        return json_encode([
            'transaction'=>$data,
            'gsisLoanList'=>$gsisLoans,
            'gsisLoanCount'=> $loanGsisCount,
            'pagibigLoanList'=>$pagibigLoans,
            'pagibigLoanCount'=> $loanPagibigCount,
            // 'totalPage' => $ctr2
        ]);
    }

    public function getDeductions($year,$month,$loan,$loaninfo){
        $loans = $loan
        ->select('id')
        ->whereIn('loan_type',['GSIS','Pagibig Loan'])
        ->get()->toArray();

        $gsis = $loan
        ->select('id')
        ->where('loan_type','GSIS')
        ->get()->toArray();

        $gsisLoans = $loaninfo
        ->with('loans')
        ->where('year',$year)
        ->where('month',$month)
        ->whereIn('loan_id',$gsis)
        ->selectRaw('*,loan_id,sum(amount) as net_amount')
        ->groupBy('loan_id')
        ->get();

        $loanGsisCount = count($gsisLoans);

        $pagibig = $loan
        ->select('id')
        ->where('loan_type','Pagibig Loan')
        ->get()->toArray();

        $pagibigLoans = $loaninfo
        ->with('loans')
        ->where('year',$year)
        ->where('month',$month)
        ->whereIn('loan_id',$pagibig)
        ->selectRaw('*,loan_id,sum(amount) as net_amount')
        ->groupBy('loan_id')
        ->get();

        $loanPagibigCount = count($pagibigLoans);

        $arr = [];

        $arr['loans'] = $loans;
        $arr['gsis_loans'] = $gsisLoans;
        $arr['gsis_loan_count'] = $loanGsisCount;
        $arr['pagibig_loans'] = $pagibigLoans;
        $arr['pagibig_loan_count'] = $loanPagibigCount;

        return $arr;
    }
}
