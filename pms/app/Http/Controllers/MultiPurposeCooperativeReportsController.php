<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\DeductionInfo;
use App\Deduction;
use App\Loan;
use App\LoanInfo;
use App\EmployeeInfo;
class MultiPurposeCooperativeReportsController extends Controller
{
    function __construct(){
		$this->title = 'MWSS-RO Multi-Purpose Cooperative';
    	$this->module = 'mwsscooperative';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$data = Input::all();
    	$transaction 	  = new Transaction;
        $deduction 		  = new Deduction;
        $deductioninfo    = new DeductionInfo;
        $loan 			  = new Loan;
        $loaninfo 		  = new LoanInfo;
        $employeeinfo     = new EmployeeInfo;


        $deduction_id = $deduction->where('code','MPCC')->select('id')->first();
        $loan_id 	  = $loan->where('code','MPLP')->select('id')->first();

        $deduction_employeeid = $deductioninfo->where('deduction_id',$deduction_id->id)->select('employee_id')->get()->toArray();
        $loaninfo_employeeid = $loaninfo->where('loan_id',$loan_id->id)->select('employee_id')->get()->toArray();

        $loaninfo_id = [];
        foreach ($loaninfo_employeeid as $key => $value) {
           $loaninfo_id[$key] = $value;
        }

        $deductioninfo_id = [];
        foreach ($deduction_employeeid as $key => $value) {
           $deductioninfo_id[$key] = $value;
        }

        $new = array_unique(array_merge($loaninfo_id,$deductioninfo_id), SORT_REGULAR);

        $transaction = $transaction->select('employee_id')
                                    ->whereIn('employee_id',$new)
                                    ->where('year',$data['year'])
                                    ->where('month',$data['month'])
                                    ->get()->toArray();

        $query = $employeeinfo->with(['employees','mwssLoaninfo' => function($qry) use($loan_id){
                                        $qry = $qry->where('loan_id',$loan_id->id);
                                    }
                                     ,'mwssDeductionInfo' => function($qry) use($deduction_id){
                                         $qry = $qry->where('deduction_id',$deduction_id->id);   
                                     }])->whereIn('employee_id',$transaction)->get();
    	return json_encode($query);
    }
}
