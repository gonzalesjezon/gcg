<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\SpecialPayrollTransaction;
class CashGiftsAndMidYearReportController extends Controller
{
    function __construct(){
		$this->title = 'CASH GIFT AND MID YEAR BONUS';
    	$this->module = 'cashmidyearbonus';
        $this->module_prefix = 'payrolls/reports/othercompensations';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function getEmployeeinfo(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $transaction =  new SpecialPayrollTransaction;


        $query = $transaction
        ->with([
            'employees',
            'offices',
            'positions',
            'salaryinfo',
            'employeeinformation',
        ])
        ->where('month',$month)
        ->where('year',$year)
        ->where('status','yearendbonus')
        ->get();

        $data = [];
        if(count($query) > 0){
            foreach ($query as $key => $value) {

                if($value->offices){
                 $data[$value->offices->Name][$key] = $value;
                }
            }

        }else{
            $data = [];
        }

        return json_encode($data);
    }
}
