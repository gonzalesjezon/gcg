<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PayrollsController extends Controller
{
	
	function __construct(){
		$this->module = 'payrolls';
		$this->controller = $this;
	}    

    public function index(){


    	return view($this->module.'.index');
    	
    }
}
