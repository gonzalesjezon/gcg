<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\NonPlantillaTransaction;
use App\NonPlantillaEmployeeInfo;
use App\AttendanceInfo;
use App\EmployeeStatus;
class GIPPayrollWorksheetReportsController extends Controller
{
        function __construct(){
    	$this->title = 'GIP PAYROLL WORKSHEETS';
    	$this->module = 'gippayrollworksheet';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new NonPlantillaTransaction;
    	$employeeinfo = new NonPlantillaEmployeeInfo;
    	$attendanceinfo = new AttendanceInfo;
        $employeeStatus =  new EmployeeStatus;

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $employeeStatus = $employeeStatus->where('code','GIP')->first();

        $query['transaction'] = $transaction
        ->with('employee_status')
        ->where('employee_status_id',$employeeStatus->id)
        ->with(['employeeinfo'=> function($qry){
            $qry->with('taxpoliciesOne','taxpoliciesTwo');
        },
            'employees'])
        ->where('year',$year)
        ->where('month',$month)
        ->get();

       return json_encode($query);
    }
}
