<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\OvertimePay;
class OvertimePayrollWorksheetReportsController extends Controller
{
    function __construct(){
    	$this->title = 'OVERTIME PAYROLL WORKSHEET';
    	$this->module = 'overtimepayrollworksheet';
        $this->module_prefix = 'payrolls/reports/overtimereports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new OvertimePay;

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $query['transaction'] = $transaction->with(['employeeinfo','employeeinformation','salaryinfo','employees'])
        								->where('year',$year)
								        ->where('month',$month)
                                        ->where('status','plantilla')
								        ->get();
       return json_encode($query);
    }
}
