<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\BenefitInfo;
use App\Benefit;
use App\SpecialPayrollTransaction;
use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;

class EmeReportsController extends Controller
{
    function __construct(){
		$this->title = 'EXTRAORDINARY AND MISCELLANEOUS EXPENSES';
    	$this->module = 'eme';
        $this->module_prefix = 'payrolls/reports/othercompensations';
    	$this->controller = $this;
	}

	public function index(){


    	$employee            = new Employee;
        $employeestatus      = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = $employee
        ->with([
            'employeeinformation' => function($qry){
                $qry->with('positions');
            },
        ])
        ->whereIn('id',$employeeinfo)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

        $response = array(
                        'employee'      => $query,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function getEmployeeinfo(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $transaction =  new Transaction;
        $special     =  new SpecialPayrollTransaction;
        $benefit     =  new Benefit;

        // $special  = $special
        // ->where('month',$month)
        // ->where('year',$year)
        // ->whereIn('status',['ee','me'])
        // ->select('employee_id')
        // ->get()->toArray();

        // $query = $transaction
        // ->with([
        //     'employees',
        //     'offices',
        //     'positions',
        //     'salaryinfo',
        //     'employeeinformation',
        //     'responsibilities',
        //     'special' => function($qry) use($year,$month,$special){
        //         $qry = $qry
        //         ->whereIn('employee_id',$special)
        //         ->whereIn('status',['ee','me'])
        //         ->where('month',$month)
        //         ->where('year',$year);
        //     }
        // ])
        // ->whereIn('employee_id',$special)
        // ->where('month',$month)
        // ->where('year',$year)
        // ->get();

        $query = $special
        ->join('pms_employees as e','e.id','=','pms_specialpayroll_transactions.employee_id')
        ->with([
            'responsibilities',
            'employees',
            'positions',
            'offices',
            'salaryinfo',
            'special'])
        ->whereIn('status',['ee','me'])
        ->where('month',$month)
        ->where('year',$year)
        ->groupBy('pms_specialpayroll_transactions.employee_id')
        ->orderBy('job_grade','desc')
        ->orderBy('e.lastname','asc')
        ->get();


        $data = [];
        if(count($query) > 0){
            foreach ($query as $key => $value) {
                if($value->responsibilities){
                 $data[$value->responsibilities->name][$key] = $value;
                }
            }
        }else{
            $data = [];
        }

        // $data2 = [];
        // foreach ($data as $key => $value) {
        //     $data2[$key] = array_values($value);
        // }


        // $sameOffice = '';
        // $data3 = [];
        // $ctr = 1;
        // $ctr2 = 1;
        // foreach ($data2 as $key => $value) {
        //     foreach ($value as $k => $val) {
        //         if($ctr <= 15){
        //             $data3[$key][$ctr2][$ctr] = $val;
        //         }else{
        //             $ctr = 0;
        //             $ctr2++;
        //         }
        //         $ctr++;
        //     }
        //     $ctr  = 1;
        //     $ctr2 = 1;
        // }

        return json_encode([
            'transaction'=>$data,
        ]);
    }
}
