<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GovernmentsReportController extends Controller
{
    function __construct(){
    	$this->title = 'GOVERNMENT REPORT';
    	$this->module = 'government';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }
}
