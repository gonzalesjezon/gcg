<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\LoanInfoTransaction;
use App\Loan;
use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
class PagibigRemittancesController extends Controller
{
    function __construct(){
		$this->title = 'PAG-IBIG PREMIUMS/LOANS';
    	$this->module = 'pagibig';
        $this->module_prefix = 'payrolls/reports/remittances';
    	$this->controller = $this;
	}

	public function index(){


    	$employee            = new Employee;
        $employeestatus      = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = $employee
        ->with([
            'employeeinformation' => function($qry){
                $qry->with('positions');
            },
        ])
        ->whereIn('id',$employeeinfo)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

        $response = array(
                        'employee'      => $query,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function getEmployeeinfo(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $transaction = new Transaction;
        $loaninfo    = new LoanInfoTransaction;
        $loan        = new Loan;


        $pagibig = $loan
        ->select('id')
        ->where('loan_type','Pagibig Loan')
        ->get()->toArray();

        $pagibigLoans = $loaninfo
        ->with('loans')
        ->where('year',$year)
        ->where('month',$month)
        ->whereIn('loan_id',$pagibig)
        ->selectRaw('*,loan_id,sum(amount) as net_amount')
        ->groupBy('loan_id')
        ->get();

        $loanPagibigCount = count($pagibigLoans);

        $query = $transaction
        ->leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->leftJoin('office as o','o.RefId','=','pms_transactions.office_id')
        ->with([
            'employees' => function($qry){
                $qry->orderBy('lastname','asc');
                },
            'loaninfoTransaction' => function($qry) use($pagibig,$year,$month){
                $qry = $qry
                ->whereIn('loan_id',$pagibig);
            },
            'positionitems',
            'positions',
            'offices'=>function($qry){
                $qry = $qry->orderBy('Name','asc');
            },
            'employeeinformation',
            'salaryinfo',
            'employeeinfo'
        ])
        ->where('hold',0)
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('o.sort_by','asc')
        ->orderBy('e.lastname','asc')
        ->get();

        $data = [];
        if(count($query) > 0){
            foreach ($query as $key => $value) {
                $data[@$value->offices->Name][$key] = $value;
            }
        }else{
            $data = [];
        }

        // $data2 = [];
        // foreach ($data as $key => $value) {
        //     $data2[$key] = array_values($value);
        // }


        // $sameOffice = '';
        // $data3 = [];
        // $ctr = 1;
        // $ctr2 = 1;
        // foreach ($data2 as $key => $value) {
        //     foreach ($value as $k => $val) {
        //         if($ctr <= 25){
        //             $data3[$key][$ctr2][$ctr] = $val;
        //         }else{
        //             $ctr = 0;
        //             $ctr2++;
        //         }
        //         $ctr++;
        //     }
        //     $ctr  = 1;
        //     $ctr2 = 1;
        // }

        return json_encode([
            'transaction'=>$data,
            'pagibigLoanList'=>$pagibigLoans,
            'pagibigLoanCount'=> $loanPagibigCount
        ]);
    }
}
