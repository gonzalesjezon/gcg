<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\EmployeeStatus;
class EmployeeStatusController extends Controller
{
    function __construct(){
    	$this->title 		 = 'EMPLOYEE STATUS SETUP';
    	$this->module_prefix = 'payrolls/admin/filemanagers';
    	$this->module 		 = 'employeestatus';
    	$this->controller 	 = $this;
    }

    public function index(){

    	$response = array(
						'module'        => $this->module,
						'controller'    => $this->controller,
		                'module_prefix' => $this->module_prefix,
						'title'		    => $this->title
					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    private function get_records($q){

        $cols = ['code','name'];

        $query = EmployeeStatus::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->orderBy('name','asc')->get();

        return $response;

    }

    public function store(Request $request){

    	$employeestatus = new EmployeeStatus;

    	if(isset($request->employeestatus_id)){

    		$employeestatus = EmployeeStatus::find($request->employeestatus_id);

	    	$employeestatus->code 	   = $request->code;
	    	$employeestatus->name 	   = $request->name;
            $employeestatus->category  = $request->category;
	    	$employeestatus->updated_by = Auth::User()->id;

	    	$employeestatus->save();

	    	$response = json_encode(['status'=>true,'response'=>'Update Successfully!']);
    	}else{

    		$this->validate($request,[
	    		'code' 	=> 'required|unique:pms_employee_status',
	    		'name' 	=> 'required'
    		]);

    		$employeestatus->code 	   = $request->code;
	    	$employeestatus->name 	   = $request->name;
            $employeestatus->category  = $request->category;
	    	$employeestatus->created_by = Auth::User()->id;

	    	$employeestatus->save();

	    	$response = json_encode(['status'=>true,'response'=>'Save Successfully!']);
    	}


    	return $response;
    }
}
