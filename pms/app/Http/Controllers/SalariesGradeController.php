<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\SalaryGrade;
use App\SalarySetup;

class SalariesGradeController extends Controller
{
    
    function __construct(){
        $this->controller = $this;
        $this->title = 'SALARY GRADE';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'salariesgrade';
        $this->table = 'pms_salarygrade';
    }

    public function index(){

        $sg_data = SalaryGrade::get();

        $response = array(
            'sg_data'       => $sg_data,
            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){

        $this->validate(request(),[
            'salary_grade'         => 'required',
            'step_inc'             => 'required',
            'amount'               => 'required',
            // 'effectivity_date'     => 'required',
        ]);

        $salarygrade = new SalaryGrade;
        $amount = str_replace(',', '', $request->amount);
        $step = "";
        switch ($request->step_inc){
            case 'step1':
                $step = 'step1';
                $salarygrade->step1 = $amount;
                break;
            case 'step2':
                $step = 'step2';
                $salarygrade->step2 = $amount;
                break;
            case 'step3':
                $step = 'step3';
                $salarygrade->step3 = $amount;
                break;
            case 'step4':
                $step = 'step4';
                $salarygrade->step4 = $amount;
                break;
            case 'step5':
                $step = 'step5';
                $salarygrade->step5 = $amount;
                break;
            case 'step6':
                $step = 'step6';
                $salarygrade->step6 = $amount;
                break;
            case 'step7':
                $step = 'step7';
                $salarygrade->step7 = $amount;
                break;
            case 'step8':
                $step = 'step8';
                $salarygrade->step8 = $amount;
                break;
            default:
                # code...
                break;
        }

        

        $check = SalaryGrade::where('salary_grade',$request->salary_grade)->first();

        if(isset($check)){

            $salarygrade->where('id',$check->id)->update([$step => $amount]);

            $response = json_encode(['status'=>true,'response' => 'Save Successfully!']);

        }else{

            $salarygrade->fill($request->all())->save();

            $response = json_encode(['status'=>true,'response' => 'Save Successfully!']);

        }

        return $response;
        
    }

    public function show(){

        $q = Input::get('q');
        // $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; } 

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                        'cols'          => $this->table_columns($this->table)

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($q){

        $cols = ['salary_grade'];

        $query = SalaryGrade::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function getSgstep(){
        $id = Input::get('id');
        $step_no = Input::get('step_no');

        $amount = SalaryGrade::where('id',$id)->first([$step_no])->toArray();

        return $amount;
    }
}

