<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\AccessType;
use App\AccessModule;
class AccessTypesController extends Controller
{
	function __construct(){
    	$this->controller = $this;
    	$this->title = 'ACCESS TYPE';
    	$this->module_prefix = 'payrolls/admin';
    	$this->module = 'accesstypes';
    }

    public function index(){

    	$accessmodule = new AccessModule;

    	$response = array(
    		'controller'		=> $this->controller,
    		'title'				=> $this->title,
    		'module'			=> $this->module,
    		'module_prefix'		=> $this->module_prefix,
    		'access_modules' 	=> $accessmodule->get(),
            'accessmodules'     => $this->accessModules()
    	);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function create()
    {
		$accessmodule = new AccessModule;

        $response =     array(  'controller'    	=>  $this->controller,
                                'module'        	=>  $this->module,
                                'title'				=> $this->title,
                                'module_prefix'		=> $this->module_prefix,
                                'access_modules' 	=> $accessmodule->get(),
                                'accessmodules'     => $this->accessModules()
                             );

        return view($this->module_prefix.'.'.$this->module.'.create',$response);

    }

    public function show(){

        $q = Input::get('q');
        $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    public function store(Request $request){
    	$data = Input::all();

        $model = new AccessType;

        $res = $model->save();
        if($res){
            $this->saveAccessRights($data,$model->id);
            $response = json_encode(['status'=>true,'response'=>'Saving was successful!']);
        }else{
            $response = json_encode(['status'=>false,'response'=>'Saving Failed!']);
        }

        return $response;
    }


    private function get_records($q){

        $cols = ['name'];


        $query = AccessType::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function deleteItem(){

        $data = Input::all();

        $id = $data['id'];

        $users = new AccessType;

        $users->destroy($id);

        return json_encode(['status'=>true]);

    }

    private function saveAccessRights($data, $access_type_id){

        $this->clearAccessRights($access_type_id);
        if(isset($data['crud'])){
            foreach ($data['crud'] as $key => $value) {

               $arr = [
                        'subscriber_id'=>Auth::User()->subscriber_id,
                        'access_type_id'=>$access_type_id,
                        'access_module_id'=>$key,
                        'to_add' => (in_array(1, $value)) ? 1 : 0,
                        'to_view' => (in_array(2, $value)) ? 1 : 0,
                        'to_edit' => (in_array(3, $value)) ? 1 : 0,
                        'to_delete' => (in_array(4, $value)) ? 1 : 0,
                        ];

                \App\AccessRight::insert($arr);
                // dd($arr);
            }
        }
    }

    private function clearAccessRights($access_type_id){
        \App\AccessRight::where('access_type_id',$access_type_id)->delete();
    }
}
