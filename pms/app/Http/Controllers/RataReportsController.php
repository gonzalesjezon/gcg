<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Rata;
use App\EmployeeStatus;
use App\Employee;
use App\EmployeeInformation;
class RataReportsController extends Controller
{
    function __construct(){
    	$this->title = 'REPRESENTATION AND TRANSPORTATION ALLOWANCE';
    	$this->module = 'rata';
        $this->module_prefix = 'payrolls/reports/othercompensations';
    	$this->controller = $this;

    }

    public function index(){


    	$employee            = new Employee;
        $employeestatus      = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = $employee
        ->with([
            'employeeinformation' => function($qry){
                $qry->with('positions');
            },
        ])
        ->whereIn('id',$employeeinfo)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

        $response = array(
                        'employee'      => $query,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }


    public function getEmployeeinfo(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];

        $query = Rata::join('pms_employees as e','e.id','pms_rata.employee_id')
            ->with([
            'employees',
            'positions',
            'departments',
            'offices',
            'responsibilities',
            'leave'
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('job_grade','desc')
        ->orderBy('e.lastname','asc')
        ->get();

        $data = [];
        if(count($query) > 0){

            foreach ($query as $key => $value) {

                if(isset($value->leave)){
                    foreach ($value->leave as $k => $val) {
                        $data['leave'][$val->employee_id][$val->leave_type][$k] = date('d',strtotime($val->leave_date));
                    }
                }
                $data[@$value->responsibilities->name][$key] = $value;
            }

        }else{

            $data = [];
        }

        // $data2 = [];
        // foreach ($data as $key => $value) {
        //     $data2[$key] = array_values($value);
        // }


        // $sameOffice = '';
        // $data3 = [];
        // $ctr = 1;
        // $ctr2 = 1;
        // foreach ($data2 as $key => $value) {
        //     foreach ($value as $k => $val) {
        //         if($ctr <= 15){
        //             $data3[$key][$ctr2][$ctr] = $val;
        //         }else{
        //             $ctr = 0;
        //             $ctr2++;
        //         }
        //         $ctr++;
        //     }
        //     $ctr  = 1;
        //     $ctr2 = 1;
        // }

        return json_encode([
            'transaction'=>$data,
        ]);
    }
}
