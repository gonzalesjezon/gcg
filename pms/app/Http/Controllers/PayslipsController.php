<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\LoanInfo;
use App\Transaction;
use App\EmployeeInfo;
use App\BenefitInfo;
use App\DeductionInfo;
use App\SalaryInfo;
use App\LoanInfoTransaction;
use App\BenefitInfoTransaction;
use App\DeductionInfoTransaction;
use App\Company;
use App\Office;
use App\Division;
use App\Department;
use App\Position;
use App\AttendanceInfo;
use App\Benefit;
use Input;

class PayslipsController extends Controller
{
    function __construct(){
    	$this->title = 'PAYSLIP';
    	$this->module = 'payslips';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

        $employeeinfo = Employee::where('active','!=',0)->orderBy('lastname','asc')->get();

    	$response = array(
                        'employeeinfo'  => $employeeinfo,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    			);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function getPayslip(){
        $data = Input::all();

        // $filtered = $this->filter($data['emp_status'],$data['category'],$data['emp_type'],$data['searchby']);
        $transaction    = new Transaction;
        $loaninfo       = new LoanInfoTransaction;
        $benefitinfo    = new BenefitInfoTransaction;
        $employeeinfo   = new EmployeeInfo;
        $deductioninfo  = new DeductionInfoTransaction;
        $salaryinfo     = new SalaryInfo;
        $benefit        = new Benefit;

        $year        = $data['year'];
        $month       = $data['month'];
        $employee_id = $data['id'];

        $benefit = $benefit->where('name','PERA')->first();


        if(isset($data['id'])){

            $query['transaction'] = $transaction
            ->with([
                'benefitTransactions' => function($qry) use($benefit){
                    $qry->where('benefit_id',$benefit->id);
                },
                'employees',
                'positionitems',
                'positions',
                'offices'
            ])
            ->where('employee_id',$employee_id)
            ->where('year',$year)
            ->where('month',$month)
            ->first();

            $query['employeeinfo'] = $employeeinfo
            ->with('employees')
            ->where('employee_id',$employee_id)
            ->first();

            $query['loaninfo'] = $loaninfo
            ->with('loans')
            ->where('employee_id',$employee_id)
            ->where('year',$year)
            ->where('month',$month)
            ->get();

            $query['benefitinfo'] = $benefitinfo
            ->with('benefits')
            ->where('employee_id',$employee_id)
            ->where('year',$year)
            ->where('month',$month)
            ->get();

            $query['deductioninfo'] = $deductioninfo
            ->with('deductions')
            ->where('employee_id',$employee_id)
            ->where('year',$year)
            ->where('month',$month)
            ->get();

            $query['salaryinfo'] = $salaryinfo
            ->where('employee_id',$employee_id)
            ->orderBy('salary_effectivity_date','desc')
            ->first();

            date_default_timezone_set('Asia/Manila');

            $query['date_run'] = date('F d, Y h:i A',time());

            return json_encode($query);

        }
    }


}
