<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\NonPlantillaTransaction;
use App\NonPlantillaEmployeeInfo;
use App\AttendanceInfo;
use App\EmployeeStatus;
class COCGeneralPayrollReportController extends Controller
{
    function __construct(){
    	$this->title = 'COC GENERAL PAYROLL';
    	$this->module = 'cocgeneralpayroll';
        $this->module_prefix = 'payrolls/reports/nonplantillareports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function getGeneralPayrollReport(){

    	$transaction = new NonPlantillaTransaction;
    	$employeeinfo = new NonPlantillaEmployeeInfo;
    	$attendanceinfo = new AttendanceInfo;
        $employeeStatus =  new EmployeeStatus;

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $employeeStatus = $employeeStatus->where('code','GIP')->first();

        $query['transaction'] = $transaction
        ->where('employee_status_id','!=',$employeeStatus->id)
        ->with(['employeeinfo'=>function($qry){
            $qry->with('taxpoliciesOne','taxpoliciesTwo');
        },'employees'])
        ->where('year',$year)
        ->where('month',$month)
        ->get();
       return json_encode($query);
    }
}
