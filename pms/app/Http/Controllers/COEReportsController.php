<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeInfo;
use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
use Input;
class COEReportsController extends Controller
{
    function __construct(){
    	$this->title = 'CERTIFICATE OF EMPLOYEE';
    	$this->module = 'coereports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

        $employee            = new Employee;
        $employeestatus      = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = $employee
        ->with([
            'employeeinformation' => function($qry){
                $qry->with('positions');
            },
        ])
        ->whereIn('id',$employeeinfo)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

        $response = array(
                        'employee'      => $query,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function getCOE(){
        $data = Input::all();

        $employeeinfo   = new EmployeeInfo;

        $year        = $data['year'];
        $month       = $data['month'];
        $employee_id = $data['id'];

        $query = $employeeinfo
        ->with('employees','benefitinfo','employeeinformation')
        ->where('employee_id',$employee_id)
        ->first();

        return json_encode($query);

    }
}
