<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\SpecialPayrollTransaction;
class MidYearTransmitalReportsReportsController extends Controller
{
    function __construct(){
    	$this->title = 'TRANSMITAL';
    	$this->module = 'midyeartransmital';
        $this->module_prefix = 'payrolls/reports/midyearreports';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new SpecialPayrollTransaction;

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $query['transaction'] = $transaction
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','midyearbonus')
        ->sum('amount');

       	return json_encode($query);
    }
}
