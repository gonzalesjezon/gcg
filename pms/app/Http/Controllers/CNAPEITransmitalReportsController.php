<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\SpecialPayrollTransaction;
class CNAPEITransmitalReportsController extends Controller
{
       function __construct(){
    	$this->title = 'CNA & PEI TRANSMITAL';
    	$this->module = 'cnapeitransmital';
        $this->module_prefix = 'payrolls/reports/cnapeireports';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new SpecialPayrollTransaction;

        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $query = $transaction
        ->where('year',$year)
        ->where('month',$month)
        ->where('status','pei')
        ->get();

        $net_amount = 0;

        foreach ($query as $key => $value) {

        	$cna_amount = $value->cna_amount;
        	$pei_amount = $value->amount;
			$total_amount = (float)$cna_amount + (float)$pei_amount;
			$agency_amount = (float)$cna_amount*0.1;
			$amount = (float)$total_amount - (float)$agency_amount;
			$net_amount += $amount;

        }


       $response['transaction'] = $net_amount;

       return json_encode($response);
    }
}
