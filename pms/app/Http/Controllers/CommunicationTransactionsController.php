<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Employee;
use App\EmployeeStatus;
use App\Benefit;
use App\BenefitInfo;
use App\SpecialPayrollTransaction;
use App\EmployeeInformation;
use App\PositionItem;
use App\EmployeeInfo;
use App\SalaryInfo;
class CommunicationTransactionsController extends Controller
{
    function __construct(){
    	$this->title = 'COMMUNICATION TRANSACTIONS';
    	$this->controller = $this;
    	$this->module = 'communicationtransactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){


    	$response = array(
           'title' 	        	=> $this->title,
           'controller'         => $this->controller,
           'module'	        	=> $this->module,
           'module_prefix'      => $this->module_prefix,
           'months'             => config('params.months'),
           'latest_year'        => $this->latestYear(),
           'earliest_year'      => $this->earliestYear(),
           'current_month'      => (int)date('m')

       );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q 			= Input::get('q');
        $year 		= Input::get('year');
        $month 		= Input::get('month');
        $_year       = Input::get('_year');
        $_month      = Input::get('_month');
        $check_cea 	= Input::get('check_cea');
        $checkcea 	= Input::get('checkcea');
        $data = "";

        $data = $this->searchName($q,$check_cea,$_year,$_month);

        if(isset($year) || isset($month) || isset($checkcea)){
            $data = $this->filter($year,$month,$checkcea);
        }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$checkcea,$year,$month){

        $employee 	 	  = new Employee;
        $benefit  	 	  = new Benefit;
        $benefitinfo 	  = new BenefitInfo;
        $transaction 	  = new SpecialPayrollTransaction;

        $cols = ['lastname','firstname'];

        $arrBenefit  = $benefit->where('code','CEA')->select('id')->first();

        $arrEmployee = $benefitinfo
        ->where('benefit_id',$arrBenefit->id)
        ->select('employee_id')
        ->get()->toArray();

        $employee_id = $transaction
        ->where('year',$year)
        ->where('month',$month)
        ->whereIn('employee_id',$arrEmployee)
        ->where('status','cea')
        ->select('employee_id')->get()->toArray();

        $query = [];
        switch ($checkcea) {
            case 'wcea':
                $query = $employee->whereIn('id',$employee_id);
                break;

            default:
                $query = $employee->whereNotIn('id',$employee_id)->whereIn('id',$arrEmployee);
                break;
        }

      	$query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query
        ->orderBy('lastname','asc')
        ->where('active',1)
        ->get();

        return $response;
    }

    public function filter($year,$month,$checkcea){


        $employee           = new Employee;
        $benefit  	 	  	= new Benefit;
        $benefitinfo 	  	= new BenefitInfo;
        $transaction        = new SpecialPayrollTransaction;

        $arrBenefit 	 = $benefit->where('code','CEA')->select('id')->first();

        $arrEmployee 	 = $benefitinfo
        ->where('benefit_id',$arrBenefit->id)
        ->select('employee_id')->get()->toArray();

        $query = [];
        $response = "";

        switch ($checkcea) {
            case 'wcea':

                $query =  $transaction->select('employee_id')->where('status','cea');

                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)
                    ->where('active',1)
                    ->orderBy('lastname','asc')->get();


                break;

            case 'wocea':

                 $query =  $transaction->select('employee_id')->where('status','cea');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee
                    ->whereIn('id',$arrEmployee)
                    ->whereNotIn('id',$query)
                    ->where('active',1)
                    ->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }

    public function store(Request $request){

        $comm = SpecialPayrollTransaction::find($request->id);
        $comm->special_remarks = $request->remarks;
        $comm->add_amount = $request->add_amount;
        $comm->less_amount = $request->less_amount;
        if($comm->save()){
            $response = json_encode(['status'=>true,'response'=>'Communication allowance updated successfully!']);
        }else{
            $response = json_encode(['status'=>true,'response'=>'Communication allowance deleted successfully!']);
        }

        return $response;
    }

    public function processCea(Request $request){
        $data = Input::all();

        $benefit = new Benefit;

        $benefit =  $benefit->where('code','CEA')->select('id')->first();

        foreach ($data['list_id'] as $key => $value) {

            if(isset($value)){

                $benefitinfo         = new BenefitInfo;
                $employeeinfo        = new EmployeeInformation;
                $payrollinfo         = new EmployeeInfo;
                $employee            = new Employee;
                $salaryinfo          = new SalaryInfo;

                $employee = $employee->where('id',$value)->first();

                $employeeinfo   = $employeeinfo->where('employee_id',$value)->first();

                $payrollinfo   = $payrollinfo->where('employee_id',$value)->first();
                $salaryinfo    = $salaryinfo->with('jobgrade')->where('employee_id',$value)->orderBy('salary_effectivity_date','desc')->first();

                $benefitinfo    = $benefitinfo
                ->with('benefits')
                ->where('employee_id',$employee->id)
                ->where('benefit_id',$benefit->id)
                ->first();

                if(isset($benefitinfo)){

                    $job_grade = str_replace('JG', '', @$salaryinfo->jobgrade->Code);

                    $transaction = new SpecialPayrollTransaction;

                    $transaction->employee_id       = $value;
                    $transaction->employee_number   = @$employee->employee_number;
                    $transaction->office_id         = @$employeeinfo->office_id;
                    $transaction->position_id       = @$employeeinfo->position_id;
                    $transaction->job_grade         = $job_grade;
                    $transaction->division_id       = @$employeeinfo->division_id;
                    $transaction->responsibility_id = @$payrollinfo->responsibility_id;
                    $transaction->benefit_info_id   = $benefitinfo->id;
                    $transaction->amount            = $benefitinfo->benefit_amount;
                    $transaction->year              = $data['year'];
                    $transaction->month             = $data['month'];
                    $transaction->status            = 'cea';
                    $transaction->created_by        = Auth::User()->id;
                    $transaction->save();

                }
            }

        }
        $response = json_encode(['status'=>true,'response'=>'Process Successfully!']);

        return $response;
    }


    public function showCeaDatatable(){

    	$response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getCeaInfo(){
    	$data = Input::all();

        $transaction =  new SpecialPayrollTransaction;

        $query = $transaction
        ->with([
            'positions',
            'offices',
        ])
        ->where('year',$data['year'])
        ->where('month',$data['month'])
        ->where('status','cea')
        ->where('employee_id',@$data['employee_id'])
        ->first();

        return json_encode($query);
    }

    public function deleteCea(){
        $data = Input::all();

        $transaction = new SpecialPayrollTransaction;

        foreach ($data['empid'] as $key => $value) {

            $transaction
            ->where('employee_id',$data['empid'][$key])
            ->where('month',$data['month'])
            ->where('year',$data['year'])
            ->where('status','cea')
            ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }
}
