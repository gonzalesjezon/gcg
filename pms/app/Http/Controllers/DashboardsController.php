<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
class DashboardsController extends Controller
{
    function __construct(){
        $this->title = "DASHBOARD";
        $this->controller = $this;
        $this->module_prefix  = 'payrolls';
        $this->module = 'dashboards';
    }

    public function index(){

        $employestatus        = new EmployeeStatus;
        $employeeinfo         = new EmployeeInformation;
        $employee             = new Employee;

        $code = ['P','PT','COTER','COS','T'];

        $data['permanent'] = $employestatus->where('Code',$code[0])->first();
        $data['pt'] = $employestatus->where('Code',$code[1])->first();
        $data['cot'] = $employestatus->where('Code',$code[2])->first();
        $data['cos'] = $employestatus->where('Code',$code[3])->first();
        $data['temp'] = $employestatus->where('Code',$code[4])->first();

        $employee = $employee->where('active',1)->select('id')->get()->toArray();

        $employees = $employeeinfo
        ->whereIn('employee_id',$employee)
        ->select('employee_id')
        ->get()->toArray();

        $permanent = $employeeinfo
        ->whereIn('employee_id',$employee)
        ->where('employee_status_id',@$data['permanent']->RefId)
        ->get();

        $pt = $employeeinfo
        ->whereIn('employee_id',$employee)
        ->where('employee_status_id',@$data['pt']->RefId)
        ->get();

        $cot = $employeeinfo
        ->whereIn('employee_id',$employee)
        ->where('employee_status_id',@$data['cot']->RefId)
        ->get();

        $cos = $employeeinfo
        ->whereIn('employee_id',$employee)
        ->where('employee_status_id',@$data['cos']->RefId)
        ->get();

        $temp = $employeeinfo
        ->WhereIn('employee_id',$employee)
        ->where('employee_status_id',@$data['temp']->RefId)
        ->get();


        $response = array(
                        'allemployees'=> count(@$employee),
                        'permanent'  => count(@$permanent),
                        'pt'         => count(@$pt),
                        'cot'        => count(@$cot),
                        'cos'        => count(@$cos),
                        'temp'        => count(@$temp),
                        'title'      => $this->title,
                        'controller' => $this->controller,
                        'module'     => $this->module,

                        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');

        $data = $this->searchName($q);

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    public function searchName($q){

        $cols = ['lastname','firstname'];

        $employee             = new Employee;

        $query = $employee->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->with('employeeinformation','salaryinfo')
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

        return $response;
    }
}
