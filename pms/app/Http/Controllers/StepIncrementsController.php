<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\SalaryInfo;
use App\EmployeeInfo;
use App\DeductionInfo;
use App\LoanInfo;
use App\Deduction;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\StepIncrement;
use App\PhilhealthPolicy;
use Carbon\Carbon;
use Auth;
class StepIncrementsController extends Controller
{
    function __construct(){
        $this->title = 'STEP INCREMENT';
        $this->module = 'stepincrements';
        $this->module_prefix = 'payrolls/otherpayrolls';
        $this->controller = $this;

    }

    public function index(){


        $response = array(
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title
                        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');

        $data = "";

        $data = $this->searchName($q);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){
        $cols = ['lastname','firstname'];

        $status              = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;
        $employee            = new Employee;

        $status = $status
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $arr_employeeid = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();


        $query = $employee
                ->whereIn('id',$arr_employeeid)
                ->where(function($query) use($cols,$q){
                    $query = $query->where(function($qry) use($q, $cols){
                        foreach ($cols as $key => $value) {
                            $qry->orWhere($value,'like','%'.$q.'%');
                        }
                    });
                });


        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }


    public function store(Request $request){

        $stepincrement = new StepIncrement;

        $transaction_date   = $request->transaction_date;
        $employee_id        = $request->employee_id;
        $actual_workdays    = $request->actual_workdays;
        $year     = $request->year;
        $month     = $request->month;

        $new_basic_pay = (isset($request->new_rate_amount)) ? str_replace(',', '', $request->new_rate_amount) : 0;
        $old_basic_pay = (isset($request->old_rate_amount)) ? str_replace(',', '', $request->old_rate_amount) : 0;
        $salary_adjustment = (isset($request->adjustment_amount)) ? str_replace(',', '', $request->adjustment_amount) : 0;
        $gsis_cont_amount = (isset($request->gsis_cont_amount)) ? str_replace(',', '', $request->gsis_cont_amount) : 0;
        $philhealth_cont_amount = (isset($request->philhealth_cont_amount)) ? str_replace(',', '', $request->philhealth_cont_amount) : 0;
        $provident_cont_amount = (isset($request->pf_cont_amount)) ? str_replace(',', '', $request->pf_cont_amount) : 0;
        $gross_pay = (isset($request->basic_amount)) ? str_replace(',', '', $request->basic_amount) : 0;
        $wtax_amount = (isset($request->tax_amount)) ? str_replace(',', '', $request->tax_amount) : 0;
        $first_amount = (isset($request->first_amount)) ? str_replace(',', '', $request->first_amount) : 0;
        $second_amount = (isset($request->second_amount)) ? str_replace(',', '', $request->second_amount) : 0;
        $third_amount = (isset($request->third_amount)) ? str_replace(',', '', $request->third_amount) : 0;
        $fourth_amount = (isset($request->fourth_amount)) ? str_replace(',', '', $request->fourth_amount) : 0;
        $total_deduction = (isset($request->total_deduction)) ? str_replace(',', '', $request->total_deduction) : 0;

        if($salary_adjustment != $total_deduction){
            $response =  json_encode(['status'=>false, 'response'=>'The adjustment amount is not balance!']);
        }else{

            if(isset($transaction_id)){

                $stepincrement = $stepincrement->find($transaction_id);

                $stepincrement->employee_id                = $employee_id;
                $stepincrement->actual_workdays            = $actual_workdays;
                $stepincrement->old_basic_pay_amount       = $old_basic_pay;
                $stepincrement->new_basic_pay_amount       = $new_basic_pay;
                $stepincrement->salary_adjustment_amount   = $salary_adjustment;
                $stepincrement->gsis_cont_amount           = $gsis_cont_amount;
                $stepincrement->philhealth_cont_amount     = $philhealth_cont_amount;
                $stepincrement->provident_fund_amount      = $provident_cont_amount;
                $stepincrement->wtax_amount                = $wtax_amount;
                $stepincrement->first_deduction_amount     = $first_amount;
                $stepincrement->second_deduction_amount    = $second_amount;
                $stepincrement->third_deduction_amount     = $third_amount;
                $stepincrement->fourth_deduction_amount    = $fourth_amount;
                $stepincrement->gross_pay_amount           = $gross_pay;
                $stepincrement->date_from                  = $request->date_from;
                $stepincrement->date_to                    = $request->date_to;
                $stepincrement->year                       = $year;
                $stepincrement->month                      = $month;
                $stepincrement->updated_by                 = Auth::User()->id;

                $stepincrement->save();

                $response = json_encode(['status'=>true,'response' => 'Update Successfully']);


            }else{

                // $this->validate($request,[
                // ]);

                $stepincrement->actual_workdays            = $actual_workdays;
                $stepincrement->employee_id                = $employee_id;
                $stepincrement->old_basic_pay_amount       = $old_basic_pay;
                $stepincrement->new_basic_pay_amount       = $new_basic_pay;
                $stepincrement->salary_adjustment_amount   = $salary_adjustment;
                $stepincrement->gsis_cont_amount           = $gsis_cont_amount;
                $stepincrement->philhealth_cont_amount     = $philhealth_cont_amount;
                $stepincrement->provident_fund_amount      = $provident_cont_amount;
                $stepincrement->wtax_amount                = $wtax_amount;
                $stepincrement->first_deduction_amount     = $first_amount;
                $stepincrement->second_deduction_amount    = $second_amount;
                $stepincrement->third_deduction_amount     = $third_amount;
                $stepincrement->fourth_deduction_amount    = $fourth_amount;
                $stepincrement->gross_pay_amount           = $gross_pay;
                $stepincrement->date_from                  = $request->date_from;
                $stepincrement->date_to                    = $request->date_to;
                $stepincrement->year                       = $year;
                $stepincrement->month                      = $month;
                $stepincrement->created_by                 = Auth::User()->id;

                $stepincrement->save();

                $response = json_encode(['status'=>true,'response' => 'Save Successfully']);

            }
        }



        return $response;


    }

    public function showStepIncrement(){

         $response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);


    }

    public function getStepIncrement(){

        $employee_id = Input::get('id');

        $salaryinfo            = new SalaryInfo;
        $philhealth_policy     = new PhilhealthPolicy;
        $deductioninfo         = new DeductionInfo;
        $deduction             = new Deduction;
        $adjustment            = new StepIncrement;
        $employeeinfo          = new EmployeeInfo;

        $deduction = $deduction->where('code','PF')->first();

        $deductioninfo = $deductioninfo
        ->where('deduction_id',@$deduction->id)
        ->where('employee_id',@$employee_id)
        ->orWhereNotNull('deduct_date_terminated')
        ->orderBy('created_at','desc')
        ->first();

        $salaryinfo = $salaryinfo
                    ->with('positions')
                    ->where('employee_id',@$employee_id)
                    ->orderBy('salary_effectivity_date','desc')
                    ->take(2)
                    ->get();

        $employeeinfo = $employeeinfo->where('employee_id',@$employee_id)->first();

        $adjustment = $adjustment
        ->where('employee_id',$employee_id)
        ->get();

        $query['old_position']    = (isset($salaryinfo[1]->positions)) ? @$salaryinfo[1]->positions->Name : '';
        $query['new_position']    = (isset($salaryinfo[0]->positions)) ? @$salaryinfo[0]->positions->Name : '';
        $query['old_rate_amount'] = @$salaryinfo[0]->salary_old_rate;
        $query['new_rate_amount'] = @$salaryinfo[0]->salary_new_rate;
        $query['philhealth_policy'] = $employeeinfo->philhealth_contribution;
        $query['pf_rate'] = @$deductioninfo->deduction_rate;
        $query['adjustments'] = @$adjustment;

        return $query;

    }

    public function deleteStepIncrement(){
        $data = Input::all();

        // $date           = $data['date'];
        $id             = $data['id'];
        $employee_id    = $data['employee_id'];

        $stepincrement =  new StepIncrement;

        $stepincrement->destroy($id);

        $query['stepincrement'] = $stepincrement
        ->where('employee_id',$employee_id)
        ->get();

        return $query;

    }

     public function getDaysInAMonth(){
        $data = Input::all();

        $year = @$data['year'];
        $month = @$data['month'];

        $y = date('Y', strtotime($year));
        $m = date('m', strtotime($month));

        $workdays = cal_days_in_month(CAL_GREGORIAN, $m, $y);

        return json_encode(['workdays'=>$workdays]);

    }

    public function getCountedDays(){
        $data = Input::all();

        $from         = @$data['date_from'];
        $to           = @$data['date_to'];

        if(isset($from) && isset($to)){
            $start_date   = date('Y-m-d', strtotime($from));
            $end_date     = date('Y-m-d', strtotime($to));
            $counted_days = $this->getWorkingDays($start_date,$end_date);

            return json_encode(['counted_days'=>$counted_days]);
        }

    }
}
