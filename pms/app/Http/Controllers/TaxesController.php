<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\TaxTable;
use App\AnnualTaxPolicy;
use App\TaxStatusTable;
use App\TaxPolicy;
use Auth;

class TaxesController extends Controller
{

    function __construct(){
        $this->controller = $this;
        $this->title = 'TAX TABLE & POLICES';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'taxes';
    }

    public function index(){

        $response = array(

            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){

        $this->validate($request,[
            'effectivity_date'         => 'required|unique:pms_taxtable',
            // 'payperiod'                => 'required'

        ]);
        if(isset($request->daily)){

            $daily = new TaxTable;
            $daily->payperiod = 'Daily';

            $daily->salary_bracket_level1 = str_replace(',', '', $request->daily[0]);
            $daily->salary_bracket_level2 = str_replace(',', '', $request->daily[1]);
            $daily->salary_bracket_level3 = str_replace(',', '', $request->daily[2]);
            $daily->salary_bracket_level4 = str_replace(',', '', $request->daily[3]);
            $daily->salary_bracket_level5 = str_replace(',', '', $request->daily[4]);
            $daily->salary_bracket_level6 = str_replace(',', '', $request->daily[5]);
            $daily->effectivity_date = $request->effectivity_date;

            $daily->save();
        }

        if(isset($request->monthly)){
            $monthly = new TaxTable;
            $monthly->payperiod = 'Monthly';
            $monthly->salary_bracket_level1 = str_replace(',', '', $request->monthly[0]);
            $monthly->salary_bracket_level2 = str_replace(',', '', $request->monthly[1]);
            $monthly->salary_bracket_level3 = str_replace(',', '', $request->monthly[2]);
            $monthly->salary_bracket_level4 = str_replace(',', '', $request->monthly[3]);
            $monthly->salary_bracket_level5 = str_replace(',', '', $request->monthly[4]);
            $monthly->salary_bracket_level6 = str_replace(',', '', $request->monthly[5]);
            $monthly->effectivity_date = $request->effectivity_date;

            $monthly->save();
        }

        if(isset($request->semimonthly)){
            $semimonthly = new TaxTable;
            $semimonthly->payperiod = 'Semi Monthly';
            $semimonthly->salary_bracket_level1 = str_replace(',', '', $request->semimonthly[0]);
            $semimonthly->salary_bracket_level2 = str_replace(',', '', $request->semimonthly[1]);
            $semimonthly->salary_bracket_level3 = str_replace(',', '', $request->semimonthly[2]);
            $semimonthly->salary_bracket_level4 = str_replace(',', '', $request->semimonthly[3]);
            $semimonthly->salary_bracket_level5 = str_replace(',', '', $request->semimonthly[4]);
            $semimonthly->salary_bracket_level6 = str_replace(',', '', $request->semimonthly[5]);
            $semimonthly->effectivity_date = $request->effectivity_date;

            $semimonthly->save();
        }

        if(isset($request->weekly)){
            $weekly = new TaxTable;
            $weekly->payperiod = 'Weekly';
            $weekly->salary_bracket_level1 = str_replace(',', '', $request->weekly[0]);
            $weekly->salary_bracket_level2 = str_replace(',', '', $request->weekly[1]);
            $weekly->salary_bracket_level3 = str_replace(',', '', $request->weekly[2]);
            $weekly->salary_bracket_level4 = str_replace(',', '', $request->weekly[3]);
            $weekly->salary_bracket_level5 = str_replace(',', '', $request->weekly[4]);
            $weekly->salary_bracket_level6 = str_replace(',', '', $request->weekly[5]);
            $weekly->effectivity_date = $request->effectivity_date;

            $weekly->save();
        }


        return json_encode(['status'=>true,'response' => 'Save Successfully!']);

    }

    public function show(){

        $q = Input::get('q');
        $limit = Input::get('limit');
        if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records($limit,$q);

        $taxtable = TaxTable::get();

        $response = array(
                        'taxtable'      => json_encode($taxtable),
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($limit,$q){

        $cols = ['status'];


        $query = TaxTable::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->paginate($limit);

        return $response;

    }


    public function storeAnnualTaxPolicy(Request $request){

        // $this->validate(request(),[
        //     'below'               => 'required',
        //     'above'               => 'required',
        //     'rate'                => 'required',
        //     'effectivity_date'    => 'required',


        // ]);

        $below_amount = ($request->below_amount) ? str_replace(',', '', $request->below_amount) : 0;
        $above_amount = ($request->above_amount) ? str_replace(',', '', $request->above_amount) : 0;
        $rate_amount = ($request->rate_amount) ? str_replace(',', '', $request->rate_amount) : 0;
        $excess_amount = ($request->excess_amount) ? str_replace(',', '', $request->excess_amount) : 0;

        $taxpolicy = new AnnualTaxPolicy;

        if(isset($request->tax_annual_id)){

            $taxpolicy = $taxpolicy->find($request->tax_annual_id);

            $taxpolicy->from_year           = $request->from_year;
            $taxpolicy->to_year             = $request->to_year;
            $taxpolicy->below_amount        = $below_amount;
            $taxpolicy->above_amount        = $above_amount;
            $taxpolicy->rate_percentage     = $request->rate_percentage;
            $taxpolicy->rate_amount         = $rate_amount;
            $taxpolicy->excess_amount       = $excess_amount;
            $taxpolicy->effectivity_date    = $request->annual_effectivity_date;
            $taxpolicy->updated_by          = Auth::User()->id;

            $taxpolicy->save();


            $response = json_encode(['status'=>true,'response' => 'Update Successfully!', 'data'=>$taxpolicy->get() ]);

        }else{

            $taxpolicy->from_year           = $request->from_year;
            $taxpolicy->to_year             = $request->to_year;
            $taxpolicy->below_amount        = $below_amount;
            $taxpolicy->above_amount        = $above_amount;
            $taxpolicy->excess_amount       = $excess_amount;
            $taxpolicy->rate_percentage     = $request->rate_percentage;
            $taxpolicy->rate_amount         = $rate_amount;
            $taxpolicy->effectivity_date    = $request->annual_effectivity_date;
            $taxpolicy->created_by          = Auth::User()->id;

            $taxpolicy->save();

            $response = json_encode(['status'=>true,'response' => 'Save Successfully!', 'data'=>$taxpolicy->get() ]);


        }

        return $response;

    }

    public function showAnnualTaxPolicy(){

        $q = Input::get('q');
        $limit = Input::get('limit');
        if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records_annual_tax($limit,$q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,

                        );

        return view($this->module_prefix.'.'.$this->module.'.datatabletaxannual',$response);

    }


    private function get_records_annual_tax($limit,$q){

        $cols = ['below_amount','above_amount','rate_percentage'];

        $taxpolicy =  new AnnualTaxPolicy;

        $query = $taxpolicy->where(function($query) use($cols,$q){
                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });

        $response = $query->paginate($limit);

        return $response;

    }

    public function storeTaxpolicy(Request $request){

        $taxpolicy = new TaxPolicy;

        if($request->is_withholding == 'Monthly'){
            $request->is_withholding    = 'Monthly';
        }else{
            $request->is_withholding    = 'Annual';
        }

        if(isset($request->update_taxpolicy)){

            $taxpolicy = TaxPolicy::find((int)$request->update_taxpolicy);

            $taxpolicy->fill($request->all())->save();

            $response = json_encode(['status'=>true,'response' => 'Update Successfully!','data'=>$taxpolicy->get() ]);

        }else{

            $this->validate($request,[
                'pay_period'        => 'required',
                'policy_name'        => 'required',
                'deduction_period'  => 'required',
                'policy_type'       => 'required',
            ]);

            $taxpolicy->fill($request->all())->save();

            $response = json_encode(['status'=>true,'response' => 'Save Successfully!','data'=>$taxpolicy->get() ]);

        }

        return $response;

    }

    public function showTaxpolicy(){

        $q = Input::get('q');
        // $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records_policy($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,

                        );

        return view($this->module_prefix.'.'.$this->module.'.datatabletaxpolicy',$response);

    }


    private function get_records_policy($q){

        $cols = ['policy_type','policy_name','deduction_period','pay_period'];

        $query = TaxPolicy::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function getTaxtable(){
        $id = Input::get('id');

        $query = TaxTable::where('id',$id)->first();

        return json_encode($query);
    }

    public function getTaxannual(){
        $id = Input::get('id');

        $query = AnnualTaxPolicy::where('id',$id)->first();

        return json_encode($query);
    }

    public function getTaxpolicy(){
        $id = Input::get('id');

        $query = TaxPolicy::where('id',$id)->first();

        return json_encode($query);
    }

    public function getTaxstatus(){
        $id = Input::get('id');

        $query = TaxStatusTable::where('id',$id)->first();

        return json_encode($query);
    }

     public function delete(){
        $data = Input::all();
        $id   = $data['id'];

        $policy = new TaxPolicy;
        $policy->destroy($id);

        return json_encode(['status'=>true]);
    }
}
