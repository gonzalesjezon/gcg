<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeInfo;
use App\Transaction;
use App\Employee;
use App\LoanInfo;
use Input;
class PayrollWorksheetReportController extends Controller
{
    function __construct(){
    	$this->title = 'PAYROLL WORKSHEETS';
    	$this->module = 'payrollworksheets';
        $this->module_prefix = 'payrolls/reports/plantillareports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function getPayrollWorksheetReport(){

        $q = Input::all();

        $query = Transaction::with(['employees',
        'positions',
        'offices',
        'employeeinformation',
        'salaryinfo'=>function($qry){
        	$qry->with('salarygrade');
        }])
        ->where('year',$q['year'])
        ->where('month',$q['month']);

        $query = $query->with(['employeeinfo'=> function($qry){
                            $qry = $qry->with(['loaninfo' => function($qry){
                                $qry->with('loans:id,name')->get();
                            },'payrolldeduction' => function($qry){
                                $qry->with('deductions:id,code')->get();
                            }]);
                        }])->orderBy('office_id','asc')->get();


        return json_encode(collect($query)->reverse()->toArray());
    }
}
