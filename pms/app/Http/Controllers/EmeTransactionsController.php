<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Employee;
use App\EmployeeStatus;
use App\Benefit;
use App\BenefitInfo;
use App\SpecialPayrollTransaction;
use App\EmployeeInformation;
use App\PositionItem;
use App\EmployeeInfo;
use App\SalaryInfo;
class EmeTransactionsController extends Controller
{
    function __construct(){
    	$this->title = 'EXTRAORDINARY AND MISCELLANEOUS TRANSACTIONS';
    	$this->controller = $this;
    	$this->module = 'emetransactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){


    	$response = array(
           'title' 	            => $this->title,
           'controller'         => $this->controller,
           'module'	            => $this->module,
           'module_prefix'      => $this->module_prefix,
           'months'             => config('params.months'),
           'latest_year'        => $this->latestYear(),
           'earliest_year'      => $this->earliestYear(),
           'current_month'      => (int)date('m')

       );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q 			= Input::get('q');
        $year 		= Input::get('year');
        $month 		= Input::get('month');
        $_year      = Input::get('_year');
        $_month     = Input::get('_month');
        $check_eme 	= Input::get('check_eme');
        $checkeme 	= Input::get('checkeme');
        $data = "";

        $data = $this->searchName($q,$check_eme,$_year,$_month);

        if(isset($year) || isset($month) || isset($checkeme)){
            $data = $this->filter($year,$month,$checkeme);
        }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

      public function searchName($q,$checkeme,$year,$month){

        $employee         = new Employee;
        $benefit          = new Benefit;
        $benefitinfo      = new BenefitInfo;
        $transaction      = new SpecialPayrollTransaction;

        $cols = ['lastname','firstname'];

        $arrBenefit  = $benefit
        ->whereIn('code',['EE','ME'])
        ->select('id')
        ->get()->toArray();

        $arrEmployee = $benefitinfo
        ->whereIn('benefit_id',$arrBenefit)
        ->select('employee_id')
        ->get()->toArray();

        $employee_id = $transaction
        ->where('year',$year)
        ->where('month',$month)
        ->whereIn('employee_id',$arrEmployee)
        ->select('employee_id')
        ->whereIn('status',['ee','me'])
        ->get()->toArray();

        $query = [];
        switch ($checkeme) {
            case 'weme':
                $query = $employee->whereIn('id',$employee_id);
                break;

            default:
                $query = $employee->whereNotIn('id',$employee_id)->whereIn('id',$arrEmployee);
                break;
        }

        $query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;
    }

    public function store(Request $request){

        $eme = SpecialPayrollTransaction::find($request->id);
        $eme->special_remarks = $request->remarks;
        if($eme->save()){
            $response = json_encode(['status'=>true,'response'=>'Eme updated successfully!']);
        }else{
            $response = json_encode(['status'=>true,'response'=>'Eme deleted successfully!']);
        }

        return $response;
    }

    public function filter($year,$month,$checkeme){


        $employee           = new Employee;
        $benefit            = new Benefit;
        $benefitinfo        = new BenefitInfo;
        $transaction        = new SpecialPayrollTransaction;

        $arrBenefit      = $benefit->whereIn('code',['EE','ME'])->select('id')->get()->toArray();
        $arrEmployee     = $benefitinfo->whereIn('benefit_id',$arrBenefit)->select('employee_id')->get()->toArray();

        $query = [];
        $response = "";

        switch ($checkeme) {
            case 'weme':

                $query =  $transaction->select('employee_id')->whereIn('status',['ee','me']);

                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)->whereIn('id',$arrEmployee)->where('active',1)
                                        ->orderBy('lastname','asc')->get();

                break;

            case 'woeme':

                 $query =  $transaction->select('employee_id')->whereIn('status',['ee','me']);


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$arrEmployee)->where('active',1)
                                        ->whereNotIn('id',$query)
                                        ->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }


    public function processEme(Request $request){
    	$data = Input::all();

        $benefit = new Benefit;

        $benefit =  $benefit->whereIn('code',['EE','ME'])->select('id')->get()->toArray();

    	foreach ($data['list_id'] as $key => $value) {

	    	if(isset($value)){

                $benefitinfo         = new BenefitInfo;
                $employeeinfo        = new EmployeeInformation;
                $payrollinfo         = new EmployeeInfo;
                $employee            = new Employee;
                $salaryinfo          = new SalaryInfo;
                $employee       = $employee->where('id',$value)->first();
                $employeeinfo   = $employeeinfo->where('employee_id',$value)->first();
                $payrollinfo   = $payrollinfo->where('employee_id',$value)->first();
                $salaryinfo    = $salaryinfo->with('jobgrade')->where('employee_id',$value)->orderBy('salary_effectivity_date','desc')->first();

        		$benefitinfo 	= $benefitinfo
                ->with('benefits')
                ->where('employee_id',$employee->id)
	    		->whereIn('benefit_id',$benefit)
	    		->get();

                if(isset($benefitinfo)){
                    $job_grade = str_replace('JG', '', @$salaryinfo->jobgrade->Code);
                    $status = '';
                    foreach ($benefitinfo as $key => $val) {

                        switch ($val->benefits->code) {
                            case 'EE':
                                $status = 'ee';
                                break;
                            case 'ME':
                                $status = 'me';
                                break;
                            default:
                                $status = '';
                                break;
                        }

                        $transaction = new SpecialPayrollTransaction;

                        $transaction->employee_id       = $value;
                        $transaction->employee_number   = @$employeeinfo->employee_number;
                        $transaction->office_id         = @$employeeinfo->office_id;
                        $transaction->position_id       = @$employeeinfo->position_id;
                        $transaction->division_id       = @$employeeinfo->division_id;
                        $transaction->job_grade         = $job_grade;
                        $transaction->responsibility_id = @$payrollinfo->responsibility_id;
                        $transaction->benefit_id        = $val->benefit_id;
                        $transaction->benefit_info_id   = $val->id;
                        $transaction->amount            = $val->benefit_amount;
                        $transaction->year              = $data['year'];
                        $transaction->month             = $data['month'];
                        $transaction->status            = $status;
                        $transaction->created_by        = Auth::User()->id;
                        $transaction->save();

                    }
                }

	    	}
    	}

    	$response = json_encode(['status'=>true,'response'=>'Process Successfully!']);

    	return $response;
    }

    public function showEmeDatatable(){

    	$response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getEmeInfo(){
    	$data = Input::all();

    	$transaction =  new SpecialPayrollTransaction;

    	$query = $transaction
        ->with([
            'positions',
            'offices',
    	])
        ->where('year',$data['year'])
		->where('month',$data['month'])
        ->whereIn('status',['ee','me'])
		->where('employee_id',@$data['employee_id'])
        ->first();

    	return json_encode($query);
    }

    public function deleteEme(){
        $data = Input::all();

        $rata = new SpecialPayrollTransaction;

        foreach ($data['empid'] as $key => $value) {

            $rata->where('employee_id',$data['empid'][$key])
            ->where('month',$data['month'])
            ->where('year',$data['year'])
            ->whereIn('status',['ee','me'])
            ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }

}
