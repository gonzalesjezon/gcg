<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Benefit;
use App\EmployeeInfo;
use App\BenefitInfo;
use App\Company;
use App\Division;
use App\Position;
use App\Department;
use App\Office;
use App\Rata;
use App\Leave;
use App\Transaction;
use Input;
use App\Deduction;
use App\RataDeduction;
use App\SalaryInfo;
use Auth;

class RataTransactionsController extends Controller
{
    function __construct(){
        $this->title = 'RATA TRANSACTIONS';
        $this->controller = $this;
        $this->module = 'ratatransactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){

        $deductions = new Deduction;
        $deductions = $deductions->where('payroll_group','rata')->get();


        $response = array(
           'deductions'     => $deductions,
           'title'          => $this->title,
           'controller'     => $this->controller,
           'module'         => $this->module,
           'module_prefix'  => $this->module_prefix,
           'months'             => config('params.months'),
           'latest_year'        => $this->latestYear(),
           'earliest_year'      => $this->earliestYear(),
           'current_month'      => (int)date('m')

       );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function storeRata(Request $request){

        $rata                = new Rata;
        $employeeinformation = new EmployeeInformation;
        $payrollinfo         = new EmployeeInfo;


        if(isset($request->rata_id)){

            $rata = $rata->find($request->rata_id);

            $rata->number_of_actual_work    = $request->no_of_actual_work;
            $rata->percentage_of_rata       = $request->percentage_of_rata;
            $rata->representation_amount    = $request->representation_amount;
            $rata->transportation_amount    = $request->transportation_amount;
            $rata->ra_diff_amount           = $request->ra_diff_amount;
            $rata->ta_diff_amount           = $request->ta_diff_amount;
            $rata->hold                     = ($request->hold) ? 1 : 0;
            $rata->number_of_leave_filed    = $request->number_of_leave;
            $rata->remarks                  = $request->remarks;

            $rata->save();

            $response = json_encode(['status'=>true,'response'=>'Update Successfully!']);

        }


        return $response;

    }

    public function show(){

        $q = Input::get('q');
        $check_rata = Input::get('check_rata');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');
        $_year = Input::get('_year');
        $_month = Input::get('_month');

        $data = "";

        $data = $this->searchName($q,$check_rata,$_year,$_month);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$check_rata,$year,$month){

        $cols = ['lastname','firstname'];

        $benefit     = new Benefit;
        $benefitinfo = new BenefitInfo;
        $employee    = new Employee;

        $benefit_id = $benefit
        ->whereIn('code',['TRANSPO','REP'])
        ->select('id')
        ->get()->toArray();

        $benefitinfo = $benefitinfo
        ->whereIn('benefit_id',$benefit_id)
        ->select('employee_id')
        ->groupBy('employee_id')
        ->get()->toArray();

        $rata_employee_id = Rata::whereIn('employee_id',$benefitinfo)
                                ->where('year',$year)
                                ->where('month',$month)
                                ->select('employee_id')
                                ->get()->toArray();

        $arr_employee_id = array();

        switch ($check_rata) {
            case 'wrata':
                $arr_employee_id = $rata_employee_id;
            break;

            default:
                $_employee_id = $rata_employee_id;

                if(count($benefitinfo) > 0){

                    foreach ($benefitinfo as $key => $value) {
                        $new_employee_id[] =  $value['employee_id'];
                    }

                    foreach ($_employee_id as $key => $value) {
                        $new_rata_employee_id[] =  $value['employee_id'];
                    }

                    if(isset($new_employee_id) && isset($new_rata_employee_id)) {

                        $arr_employee_id = array_diff($new_employee_id, $new_rata_employee_id);

                        if(!isset($arr_employee_id)){
                            $arr_employee_id = [];
                        }
                    }else{
                        $arr_employee_id = $new_employee_id;
                    }
                }
                break;

        }

        $query = $employee->whereIn('id',@$arr_employee_id)
                    ->where(function($query) use($cols,$q){
                    $query = $query->where(function($qry) use($q, $cols){
                        foreach ($cols as $key => $value) {
                            $qry->orWhere($value,'like','%'.$q.'%');
                        }
                    });
                });

        $response = $query->where('active','!=',0)->orderBy('lastname','asc')->get();
        return $response;
    }

    public function showRataDatatable(){

        $response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.ratadatatable',$response);
    }

    public function getRataInfo(){

        $data = Input::all();

        $benefitinfo = new BenefitInfo;
        $benefit     = new Benefit;

        $rata_id = $benefit
        ->whereIn('code',['TRANSPO','REP'])
        ->select('id')
        ->get()->toArray();

        $query['rata_setup'] = $benefitinfo
        ->with('benefits')
        ->whereIn('benefit_id',$rata_id)
        ->where('employee_id',@$data['id'])
        ->get();

        $query['rata'] = Rata::where('employee_id',@$data['id'])
                        ->where('year',@$data['year'])
                        ->where('month',@$data['month'])
                        ->first();


        // $query['employeeinfo'] = $this->getRataInfo($data['year'],$data['month'],@$data['id']);



        return json_encode($query);

    }

    public function processRata(){
        $data = Input::all();


        $benefit             = new Benefit;
        $employee            = new Employee;
        $rata                = new Rata;
        $benefitInfo         = new BenefitInfo;
        $employeeinformation = new EmployeeInformation;
        $payrollinfo         = new EmployeeInfo;

        $benefit = $benefit
        ->whereIn('code',['REP','TRANSPO'])
        ->select('id')
        ->get()->toArray();

        if(isset($data['rata_id'])){

            $rata = $rata->find($data['rata_id']);

            $benefitInfo = $benefitInfo
            ->whereIn('benefit_id',$benefit)
            ->where('employee_id',$data['employee_id'])
            ->select('benefit_amount','benefit_id')
            ->get()->toArray();

            $employeeinfo = $employeeinformation
            ->where('employee_id',$data['employee_id'])
            ->select('office_id','department_id')
            ->first();

            $payrollinfo = $payrollinfo
            ->where('employee_id',$data['employee_id'])
            ->first();

            $representation_amount = (isset($benefitInfo[0]['benefit_amount'])) ? $benefitInfo[0]['benefit_amount'] : NULL;
            $transportation_amount = (isset($benefitInfo[1]['benefit_amount'])) ? $benefitInfo[1]['benefit_amount'] : NULL;

            $office_id = (isset($employeeinfo->office_id)) ? $employeeinfo->office_id : NULL;

            // $rata->position_item_id      = (isset($employeeinfo->position_item_id)) ? $employeeinfo->position_item_id : NULL;
            $rata->department_id         = @$employeeinfo->department_id;
            $rata->responsibility_id     = @$payrollinfo->responsibility_id;
            $rata->employee_id           = $data['employee_id'];
            $rata->representation_amount = $representation_amount;
            $rata->transportation_amount = $transportation_amount;
            $rata->office_id             = $office_id;
            $rata->number_of_actual_work = 22;
            $rata->percentage_of_rata    = '100 %';
            $rata->year                  = $data['year'];
            $rata->month                 = $data['month'];
            $rata->save();
            $response = json_encode(['status'=>true,'response'=>'Update Successfully!']);

        }else{

            foreach ($data['list_id'] as $key => $value) {
                if($value !== null){

                    $benefitInfo = new BenefitInfo;
                    $salaryinfo  = new SalaryInfo;

                    $benefitInfo = $benefitInfo
                     ->whereIn('benefit_id',$benefit)
                     ->where('employee_id',$value)
                     ->select('benefit_amount','benefit_id')
                     ->get()->toArray();

                     $employeeinfo = $employeeinformation->where('employee_id',$value)->first();
                     $employee  = $employee->where('id',$value)->first();

                     $payrollinfo = $payrollinfo->where('employee_id',$value)->first();

                     $salaryinfo    = $salaryinfo->with('jobgrade')->where('employee_id',$value)->orderBy('salary_effectivity_date','desc')->first();

                     $job_grade = str_replace('JG', '', @$salaryinfo->jobgrade->Code);

                     $rata = new Rata;

                     $representation_amount = @$benefitInfo[0]['benefit_amount'];
                     $transportation_amount = @$benefitInfo[1]['benefit_amount'];
                     $rata->office_id             = @$employeeinfo->office_id;
                     $rata->department_id         = @$employeeinfo->department_id;
                     $rata->responsibility_id     = @$payrollinfo->responsibility_id;
                     $rata->position_id           = @$employeeinfo->position_id;
                     $rata->employee_number       = @$employee->employee_number;
                     $rata->job_grade             = $job_grade;
                     $rata->employee_id           = $value;
                     $rata->representation_amount = $representation_amount;
                     $rata->transportation_amount = $transportation_amount;
                     $rata->number_of_actual_work = 22;
                     $rata->percentage_of_rata = '100 %';
                     $rata->year                  = $data['year'];
                     $rata->month                 = $data['month'];

                     $rata->save();
                }
            }
            $response = json_encode(['status'=>true,'response'=>'Process Successfully!']);
        }


       return $response;
    }

    public function deleteRata(){
        $data = Input::all();

        $rata = new Rata;

        $leave = new Leave;

        foreach ($data['empid'] as $key => $value) {

            if(isset($value)){
                $id =  $rata->where('employee_id',$value)
                                    ->where('month',$data['month'])
                                    ->where('year',$data['year'])
                                    ->delete();

                 $leave->where('employee_id',$value)
                                    ->where('rata_id',$id)
                                    ->delete();
            }


        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
   }

   public function store(Request $request){

        $tax_deduction = (isset($request->tax_deduction)) ? str_replace(',','', $request->tax_deduction) : NULL;
        $ta_deduction = (isset($request->ta_deduction)) ? str_replace(',','', $request->ta_deduction) : NULL;
        $ra_deduction = (isset($request->ra_deduction)) ? str_replace(',','', $request->ra_deduction) : NULL;

        $deductions = new RataDeduction;

        if(isset($request->rata_deduction_id)){

            $deductions = $deductions->find($request->rata_deduction_id);

            $deductions->employee_id    = $request->employee_id;
            $deductions->employee_number = $request->employee_number;
            $deductions->rata_id        = $request->rata_id;
            $deductions->deduction_id   = $request->deduction_id;
            $deductions->year           = $request->year;
            $deductions->month          = $request->month;
            $deductions->tax_deduction  = $tax_deduction;
            $deductions->ta_deduction   = $ta_deduction;
            $deductions->ra_deduction   = $ra_deduction;
            $deductions->created_by     = Auth::User()->id;

            $deductions->save();

            $response = json_encode(['status'=>true,'response'=>'Update Successfully']);

        }else{

            $this->validate($request,[
                'deduction_id' => 'required',
            ]);

            $deductions->employee_id    = $request->employee_id;
            $deductions->rata_id        = $request->rata_id;
            $deductions->employee_number = $request->employee_number;
            $deductions->deduction_id   = $request->deduction_id;
            $deductions->year           = $request->year;
            $deductions->month          = $request->month;
            $deductions->tax_deduction  = $tax_deduction;
            $deductions->ta_deduction   = $ta_deduction;
            $deductions->ra_deduction   = $ra_deduction;
            $deductions->created_by     = Auth::User()->id;

            $deductions->save();

            $response = json_encode(['status'=>true,'response'=>'Save Successfully']);
        }


        return $response;
   }

}
