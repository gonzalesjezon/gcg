<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Office;
use App\Transaction;
use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
class PayrollRegisterReportController extends Controller
{
    function __construct(){
    	$this->title = 'PAYROLL REGISTER REPORT';
    	$this->module = 'payrollregister';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

        $employee            = new Employee;
        $employeestatus      = new EmployeeStatus;
        $employeeinformation = new EmployeeInformation;

        $status = $employeestatus
        ->where('category',1)
        ->select('RefId')
        ->get()->toArray();

        $employeeinfo = $employeeinformation
        ->whereIn('employee_status_id',$status)
        ->select('employee_id')
        ->get()->toArray();

        $query = $employee
        ->with([
            'employeeinformation' => function($qry){
                $qry->with('positions');
            },
        ])
        ->whereIn('id',$employeeinfo)
        ->where('active',1)
        ->orderBy('lastname','asc')
        ->get();

    	$response = array(
                        'employee'      => $query,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title,
                        'months'             => config('params.months'),
                       'latest_year'        => $this->latestYear(),
                       'earliest_year'      => $this->earliestYear(),
                       'current_month'      => (int)date('m')
    	);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::all();

        $transaction = new Transaction;
        $office      = new Office;

        $year = $q['year'];
        $month = $q['month'];
        $pay_period = $q['pay_period'];


        $query2 = $transaction
        ->where('year',$year)
        ->where('hold',0)
        ->orderBy('month','asc')
        ->groupBy('month')
        ->get();

        $holdCount = count($query2);

        $query = $transaction
        ->leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->leftJoin('office as o','o.RefId','=','pms_transactions.office_id')
        ->with([
            'offices',
            'employees',
            'employeeinformation',
            'employeeinfo',
            'salaryinfo',
            'transactions' => function($qry) use($year, $pay_period,$month){
                $qry = $qry->where('month','!=',$month)
                            ->where('year',$year)
                            ->where('hold',0);
            }
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('o.sort_by','asc')
        ->orderBy('e.lastname','asc')
        ->get();

        $data = [];
        if(isset($query)){
            foreach ($query as $key => $value) {
                $data[strtoupper(@$value->offices->Name)][$key] = $value;
            }
        }else{

            $data = [];
        }

        return json_encode([
           'transaction' => $data,
           'hold_pay' => $query2,
           'hold_count' => $holdCount
        ]);
    }

    public function showWPage(){

        $q = Input::all();

        $transaction = new Transaction;
        $office      = new Office;

        $year = $q['year'];
        $month = $q['month'];
        $pay_period = $q['pay_period'];


        $query2 = $transaction
        ->where('year',$year)
        ->whereIn('hold_status',[$pay_period,'both'])
        ->where('hold',0)
        ->orderBy('month','asc')
        ->groupBy('month')
        ->get();

        $holdCount = count($query2);

        $query = $transaction
        ->leftJoin('pms_employees as e','e.id','=','pms_transactions.employee_id')
        ->leftJoin('office as o','o.RefId','=','pms_transactions.office_id')
        ->with([
            'offices',
            'employees',
            'employeeinformation',
            'employeeinfo',
            'salaryinfo',
            'transactions' => function($qry) use($year, $pay_period){
                $qry = $qry->where('year',$year)
                            ->whereIn('hold_status',[$pay_period,'both'])
                            ->where('hold',0);
            }
        ])
        ->where('hold',0)
        ->where('year',$year)
        ->where('month',$month)
        ->orderBy('o.sort_by','asc')
        ->orderBy('e.lastname','asc')
        ->get();

        $data = [];
        if(isset($query)){
            foreach ($query as $key => $value) {
                $data[@$value->offices->Name][$key] = $value;
            }
        }else{

            $data = [];
        }

        $data2 = [];
        foreach ($data as $key => $value) {
            $data2[$key] = array_values($value);
        }


        $sameOffice = '';
        $data3 = [];
        $ctr = 1;
        $ctr2 = 1;
        foreach ($data2 as $key => $value) {
            foreach ($value as $k => $val) {
                if($ctr <= 30){
                    $data3[$ctr2][$key][$ctr] = $val;
                }else{
                    $ctr = 0;
                    $ctr2++;
                }
                $ctr++;
            }
            // $ctr  = 1;
            // $ctr2 = 1;
        }

        return json_encode([
           'transaction' => $data3,
           'hold_pay' => $query2,
           'hold_count' => $holdCount,
           'totalPage' => $ctr2
        ]);
    }
}
