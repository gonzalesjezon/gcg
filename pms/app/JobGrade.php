<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobGrade extends Model
{
	protected $primaryKey = 'RefId';
    protected $table = 'jobgrade';
	protected $fillable = [
		'Code',
		'Name',
		'Step1',
		'AnnualS1',
		'Step2',
		'AnnualS2',
		'Step3',
		'AnnualS3',
		'Step4',
		'AnnualS4',
		'Step5',
		'AnnualS5',
		'Step6',
		'AnnualS6',
		'Step7',
		'AnnualS7',
		'Step8'

	];
}
