<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class NonPlantillaTransaction extends Model
{
    protected $table ='pms_nonplantilla_transactions';
    protected $fillable = [
    	'employee_id',
		'employee_number',
		'division_id',
		'company_id',
		'position_item_id',
		'position_id',
		'office_id',
		'department_id',
		'employee_status_id',
		'employeeinfo_id',
		'actual_workdays',
		'adjust_workdays',
		'actual_absences',
		'adjust_absences',
		'actual_tardiness',
		'adjust_tardiness',
		'actual_undertime',
		'adjust_undertime',
		'actual_basicpay_amount',
		'adjust_basicpay_amount',
		'total_basicpay_amount',
		'actual_absences_amount',
		'adjust_absences_amount',
		'total_absences_amount',
		'actual_tardiness_amount',
		'adjust_tardiness_amount',
		'total_tardiness_amount',
		'actual_undertime_amount',
		'adjust_undertime_amount',
		'total_undertime_amount',
		'monthly_rate_amount',
		'annual_rate_amount',
		'tax_rate_amount_one',
		'tax_rate_amount_two',
		'basic_net_pay',
		'net_deduction',
		'gross_pay',
		'net_pay',
		'year',
		'month',
		'hold',
		'pay_period',
		'sub_pay_period,',
		'created_by',
		'updated_by',
    ];

    public function employees(){
    	return $this->belongsTo('App\Employee','employee_id');
    }
    public function divisions(){
    	return $this->belongsTo('App\Division','division_id');
    }
    public function offices(){
    	return $this->belongsTo('App\Office','office_id');
    }
    public function positions(){
    	return $this->belongsTo('App\Position','position_id');
    }
    public function employee_status(){
    	return $this->belongsTo('App\EmployeeStatus','employee_status_id');
    }
    public function positionitems(){
    	return $this->belongsTo('App\PositionItem','position_item_id');
    }

    public function attendanceinfo(){
    	return $this->hasOne('App\AttendanceInfo','employee_id');
    }
    public function employeeinfo(){
    	return $this->belongsTo('App\NonPlantillaEmployeeInfo');
    }
}
