<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OvertimePay extends Model
{
    protected $table = 'pms_overtime';
    protected $fillable = [
		'employee_id',
		'actual_regular_amount',
		'actual_holiday_amount',
		'actual_other_amount',
		'regular_rate',
		'holiday_rate',
		'actual_regular_rate',
		'actual_holiday_rate',
		'actual_other_rate',
		'other_rate',
		'max_amount_to_avail',
		'overtime_pay_for_period',
		'balance_forwarded',
		'balance_previous_month',
		'year',
		'month',
		'pay_period',
		'sub_pay_period',
		'status',
		'created_by',
		'updated_by'
    ];

    public function employeeinformation(){
    	return $this->belongsTo('App\EmployeeInformation','employee_id')->with('positions');
    }
    public function salaryinfo(){
    	return $this->belongsTo('App\SalaryInfo','employee_id')->with('salarygrade');
    }
    public function employees(){
    	return $this->belongsTo('App\Employee','employee_id');
    }
    public function employeeinfo(){
    	return $this->belongsTo('App\EmployeeInfo','employee_id');
    }
}
