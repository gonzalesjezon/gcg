<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PositionItem extends Model
{

	protected $primaryKey = 'RefId';
	protected $table = 'positionitem';
    // protected $table = 'pms_position_items';
    protected $fillable = [

		'Code',
		'Name',
		'position_id',
		'position_level_id',
		'position_classification_id',
		'created_by',
		'updated_by'

    ];

    public function positions(){
    	return $this->belongsTo('App\Position','position_id');
    }

}
