<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessRight extends Model
{
    protected $table = 'access_rights';
    protected $fillable = [
        'access_type_id',
        'access_module_id',
        'to_view',
        'to_add',
        'to_edit',
        'to_delete'
    ];


    public function accessType(){
        return $this->belongsTo('App\AccessType');
    }

    public function accessModule(){
        return $this->belongsTo('App\accessModule');
    }
}
