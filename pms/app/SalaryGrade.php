<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryGrade extends Model
{
	protected $table = 'pms_salarygrade';
	protected $fillable = [
		'salary_grade',
		'step1',
		'step2',
		'step3',
		'step4',
		'step5',
		'step6',
		'step7',
		'step8',
		'remarks',
	];
    
}
