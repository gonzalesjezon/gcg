<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

    protected $table = 'pms_departments';
    protected $fillable = [
        'name',
        'code',
        'created_by',
        'updated_by'
    ];
}
