<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BenefitInfoTransaction extends Model
{
    protected $table 	= 'pms_benefitinfo_transactions';
    protected $fillable = [
        'employee_number',
		'employee_id',
		'transaction_id',
		'benefit_id',
		'benefit_info_id',
		'amount',
		'status',
        'year',
        'month',
		'created_by',
		'updated_by',
    ];

    public function benefitInfo(){
    	return $this->belongsTo('App\BenefitInfo','benefit_info_id')->with('benefits');
    }
    public function benefits(){
    	return $this->belongsTo('App\Benefit','benefit_id');
    }
    public function employeeinformation(){
        return $this->belongsTo('App\EmployeeInformation','employee_number')->with('positions');
    }

}
