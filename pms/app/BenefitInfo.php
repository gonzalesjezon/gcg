<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BenefitInfo extends Model
{
    protected $table = 'pms_benefitsinfo';
    protected $fillable = [
        'employee_number',
        'employee_id',
    	'employeeinfo_id',
        'benefit_id',
    	'benefit_description',
    	'benefit_amount',
    	'benefit_pay_period',
        'benefit_pay_sub',
    	'benefit_effectivity_date',
        'date_from',
        'date_to'
    ];

    public function benefits(){
    	return $this->belongsTo('App\Benefit','benefit_id');
    }

    public function employeeInfo(){
    	return $this->belongsTo('App\EmployeeInfo','employeeinfo_id');
    }

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }

    public function  employeeinformation(){
        return $this->belongsTo('App\EmployeeInformation','employee_id');
    }

    public function  salaryinfo(){
        return $this->belongsTo('App\SalaryInfo','employee_id');
    }

}
