<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialPayrollTransaction extends Model
{
    protected $table = 'pms_specialpayroll_transactions';
    protected $fillable = [
        'employee_number',
		'employee_id',
		'benefit_info_id',
        'benefit_id',
		'position_item_id',
        'responsibility_id',
        'division_id',
        'position_id',
		'office_id',
		'year',
		'month',
        'percentege',
        'no_of_months_entitled',
        'cost_uniform_amount',
        'cash_gift_amount',
        'cna_amount',
        'rate',
        'amount',
        'remarks',
        'add_amount',
        'less_amount',
		'created_by',
		'updated_by',
        'job_grade'

    ];

    public function employees(){
    	return $this->belongsTo('App\Employee','employee_id');
    }
    public function benefitinfo(){
    	return $this->belongsTo('App\BenefitInfo','benefit_info_id');
    }
    public function offices(){
    	return $this->belongsTo('App\Office','office_id');
    }
    public function positions(){
        return $this->belongsTo('App\Position','position_id');
    }
    public function positionitems(){
    	return $this->belongsTo('App\PositionItem','position_item_id');
    }
    public function divisions(){
        return $this->belongsTo('App\Division','division_id');
    }
    public function employeeinformation(){
        return $this->belongsTo('App\EmployeeInformation','employee_id')->with('employees','positions');
    }
    public function salaryinfo(){
        return $this->belongsTo('App\SalaryInfo','employee_id','employee_id')->with('jobgrade');
    }
    public function responsibilities(){
        return $this->belongsTo('App\ResponsibilityCenter','responsibility_id');
    }
    public function special(){
        return $this->hasMany('App\SpecialPayrollTransaction','employee_id','employee_id');
    }
}
