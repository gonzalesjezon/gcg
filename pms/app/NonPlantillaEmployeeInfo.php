<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NonPlantillaEmployeeInfo extends Model
{
    protected $table = 'pms_nonplantilla_employeeinfo';
    protected $fillable = [
        'employee_number',
		'employee_id',
		'bank_id',
        'taxpolicy_id',
        'tax_amount_one',
        'tax_amount_two',
        'atm_no',
        'daily_rate_amount',
        'monthly_rate_amount',
        'annual_rate_amount',
        'overtime_balance_amount',
        'tax_id_number',
        'taxpolicy_two_id',
        'created_by',
        'updated_by',
    ];

    public function taxpolicy(){
        return $this->belongsTo('App\TaxPolicy','jo_taxpolicy_id');
    }
    public function taxpoliciesOne(){
        return $this->belongsTo('App\TaxPolicy','taxpolicy_id');
    }
    public function taxpoliciesTwo(){
        return $this->belongsTo('App\TaxPolicy','taxpolicy_two_id');
    }
    public function banks(){
        return $this->belongsTo('App\Bank','jo_bank_id');
    }
    public function taxpolicyOne(){
        return $this->belongsTo('App\TaxPolicy','taxpolicy_id')->where('policy_name','like','%'.'ITW'.'%');
    }
    public function taxpolicyTwo(){
        return $this->belongsTo('App\TaxPolicy','taxpolicy_two_id')->where('policy_name','like','%'.'ITW'.'%');
    }
    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }
}
