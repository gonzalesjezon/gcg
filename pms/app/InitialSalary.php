<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InitialSalary extends Model
{
    protected $table = 'pms_initial_salary';
    protected $fillable = [
    	'emloyee_id',
		'month',
		'year',
		'date_from',
		'date_to',
		'actual_workdays',
		'basic_amount',
		'pera_amount',
		'gsis_cont_amount',
		'philhealth_cont_amount',
		'pagibig_amount',
		'tax_amount',
		'gross_amount',
		'total_deduction_amount',
		'net_amount',
		'created_by',
		'updated_by'
    ];

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }

    public function employeeinformation(){
        return $this->belongsTo('App\EmployeeInformation','employee_id','employee_id')->with('positions','employeestatus','offices');
    }


}
