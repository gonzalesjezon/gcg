<?php 
   class Templates {
      public function splitSettings() {
         echo 
            '<div class="modal fade" id="split" role="dialog">
               <div class="modal-dialog modal-md>
                  <div class="row" style="margin-bottom:5px;">
                     <div class="mypanel">
                        <div class="panel-top" id="splitSettings">
                           Split Settings
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="panel-mid" id="splitSettingsContent">
                           <div class="row">
                              <div class="col-sm-6 label">Educational Background:</div>
                              <div class="col-sm-6">
                                 <select class="form-input split--" name="splitEduc">
                                    <option value="">--No Split--</option>
                                    <option value=1>Grad. Studies (1st Row)</option>
                                    <option value=2>Grad. Studies (2nd Row)</option>
                                    <option value=3>Grad. Studies (3rd Row)</option>
                                 </select>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-sm-6 label">Civil Service Eligibility:</div>
                              <div class="col-sm-6"><input type="text" name="splitEligibility" class="form-input alphanum-- split--"></div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-sm-6 label">Work Experience:</div>
                              <div class="col-sm-6"><input type="text" name="splitWorkExp" class="form-input alphanum-- split--"></div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-sm-6 label">Voluntary Work:</div>
                              <div class="col-sm-6"><input type="text" name="splitVolWork" class="form-input alphanum-- split--"></div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-sm-6 label">Learning And Development:</div>
                              <div class="col-sm-6"><input type="text" name="splitLearning" class="form-input alphanum-- split--"></div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-sm-6 label">Other Information:</div>
                              <div class="col-sm-6"><input type="text" name="splitOtherInfo" class="form-input alphanum-- split--"></div>
                           </div>

                        </div>
                        <div class="panel-bottom"></div>
                     </div>
                  </div>
               </div>   
            </div>';
         }
      public function doSelectedEmployee() {
         echo '
            <div class="row">
               <div class="mypanel"> 
                  <div class="panel-top">
                     <b>SELECTED EMPLOYEE:</b>
                  </div>
                  <div class="panel-mid">
                     <div class="row">
                        <div class="col-sm-4 label">REFERENCE ID:</div>
                        <div class="col-sm-8" id="lblRefIdSelected">&nbsp;</div>
                     </div>
                     <div class="row">
                        <div class="col-sm-4 label">LAST NAME:</div>
                        <div class="col-sm-8" id="lblEmpLastName">&nbsp;</div>
                     </div>
                     <div class="row">
                        <div class="col-sm-4 label">FIRST NAME:</div>
                        <div class="col-sm-8" id="lblEmpFirstName">&nbsp;</div>
                     </div>
                     <div class="row">
                        <div class="col-sm-4 label">MIDDLE NAME:</div>
                        <div class="col-sm-8" id="lblEmpMiddleName">&nbsp;</div>
                     </div>
                     <div class="row">
                        <div class="col-sm-4 label">POSITION:</div>
                        <div class="col-sm-8" id="lblEmpPosition">&nbsp;</div>
                     </div>
                  </div>
                  <div class="panel-bottom"></div>
               </div>  
            </div>';
         spacer(5);   
      }

      public function doEmployeeInfo($info) {
         echo 
         '<div class="row">
            <div class="col-sm-5 label"><label>EMPLOYEE NAME:</label></div>
            <div class="col-sm-7">'.$info["LastName"].", ".$info["FirstName"].'</div>
         </div>';
            /*$EmployeesRefId = getvalue("hEmpRefId");
            $info = FindFirst('empinformation',"WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId","*");
            if ($info) {*/
         echo 
         '<div class="row">
            <div class="col-sm-5 label">
               <label>PLANTILLA ITEM NO.:</label>
            </div>
            <div class="col-sm-7">
               '.getRecord("positionitem",$info["PositionItemRefId"],"Name").'
            </div>
         </div>
         <div class="row">
            <div class="col-sm-5 label">
               <label>POSITION:</label>
            </div>
            <div class="col-sm-7">
               '.getRecord("position",$info["PositionRefId"],"Name").'
            </div>
         </div>

         <div class="row">
            <div class="col-sm-5 label">
               <label>OFFICE:</label>
            </div>
            <div class="col-sm-7">
               '.getRecord("interimoffice",$info["InterimOfficeRefId"],"Name").'
            </div>
         </div>
         <div class="row">
            <div class="col-sm-5 label">
               <label>DIVISION:</label>
            </div>
            <div class="col-sm-7">
               '.getRecord("interimdivision",$info["InterimDivisionRefId"],"Name").'
            </div>
         </div>
         <div class="row">
            <div class="col-sm-5 label">
               <label>EMPLOYEE ID:</label>
            </div>
            <div class="col-sm-7">
               '.$info["AgencyId"].'
            </div>
         </div>
         <div class="row">
            <div class="col-sm-5 label">
               <label>EMP STATUS:</label>
            </div>
            <div class="col-sm-7">
               '.getRecord("empstatus",$info["EmpStatusRefId"],"Name").'
            </div>
         </div>';

      }
      function btn_apprvReject($table,$refid) {
         echo '
            <div>
               <button type="button" name="btnApproved_'.$refid.'" class="btn-cls4-tree" tbl="'.$table.'" refid="'.$refid.'">Approved</button>
               <button type="button" name="btnReject_'.$refid.'" class="btn btn-link btn-sm" tbl="'.$table.'" refid="'.$refid.'">REJECT</button>
            </div>
         ';
      }

      
   }
?>


                                    