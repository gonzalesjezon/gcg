@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-7">
            <img src="{{ url('images/e2e_logo_main.png') }}" style="  position: relative;margin-top: 50px;width: 460px;left:90px;">
        </div>
        <div class="col-md-5" style="margin-top: 150px;">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                    <label for="username" class="col-md-4 control-label">Username</label>

                    <div class="col-md-6">
                        <input id="text" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8">
                        <div class="checkbox text-right">

                        </div>
                    </div>
                    <div class="col-md-4 text-left" style="padding-left: 0px;">
                        <button type="submit" class="btn btn-primary">
                            Sign In
                        </button>
                    </div>
                </div>
<!--
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-8">
                        <button type="submit" class="btn btn-primary">
                            Login
                        </button>

                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                    </div>
                </div> -->
            </form>
        </div>
    </div>
</div>
@endsection
