
<?php
(strtolower($module) == 'cocgeneralpayroll') ? $cocgeneralpayroll = 'active' : $cocgeneralpayroll = '';
(strtolower($module) == 'cocpayrollworksheets') ? $cocpayrollworksheets = 'active' : $cocpayrollworksheets = '';
(strtolower($module) == 'costransmital') ? $costransmital = 'active' : $costransmital = '';
?>

<nav class="nav-sidebar">
    <ul class="nav">
        <li class="{{ $cocgeneralpayroll }}">
        	<a href="{{ url('payrolls/reports/nonplantillareports/cocgeneralpayroll') }}"  >
				<span>COS GENERAL PAYROLL</span>
			</a>
        </li>
        <li class="{{ $cocpayrollworksheets }}">
            <a href="{{ url('payrolls/reports/nonplantillareports/cocpayrollworksheets') }}"  >
                <span>COS PAYROLL WORKSHEETS</span>
            </a>
        </li>
        <li class="{{ $costransmital }}">
            <a href="{{ url('payrolls/reports/nonplantillareports/costransmital') }}"  >
                <span>COS TRANSMITAL</span>
            </a>
        </li>
    </ul>
</nav>




