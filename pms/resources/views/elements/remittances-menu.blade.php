
<?php
(strtolower($module) == 'gsis') ? $gsis = 'active' : $gsis = '';
(strtolower($module) == 'ecip') ? $ecip = 'active' : $ecip = '';
(strtolower($module) == 'pagibig') ? $pagibig = 'active' : $pagibig = '';
(strtolower($module) == 'philhealth') ? $philhealth = 'active' : $philhealth = '';
?>
<nav class="nav-sidebar">
    <ul class="nav">
        <li class="{{ $gsis }}">
        	<a href="{{ url('payrolls/reports/remittances/gsis') }}"  >
			GSIS</a>
        </li>
         <!-- <li class="{{ $ecip }}">
        	<a href="{{ url('payrolls/reports/remittances/ecip') }}"  >
			ECIP</a>
        </li> -->
         <li class="{{ $pagibig }}">
        	<a href="{{ url('payrolls/reports/remittances/pagibig') }}"  >
			PAGIBIG</a>
        </li>
        <li class="{{ $philhealth }}">
        	<a href="{{ url('payrolls/reports/remittances/philhealth') }}"  >
			PHILHEALTH</a>
        </li>
        <!-- <li class="nav-divider"></li> -->
    </ul>
</nav>




