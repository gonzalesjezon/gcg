
<?php
(strtolower($module) == 'loansreport') ? $loansreport = 'active' : $loansreport = '';
(strtolower($module) == 'payrollregister') ? $payrollregister = 'active' : $payrollregister = '';
(strtolower($module) == 'payslips') ? $payslips = 'active' : $payslips = '';
(strtolower($module) == 'government') ? $government = 'active' : $government = '';
(strtolower($module) == 'banksreport') ? $banksreport = 'active' : $banksreport = '';
(strtolower($module) == 'payrolltransfers') ? $payrolltransfers = 'active' : $payrolltransfers = '';
(strtolower($module) == 'remittances') ? $remittances = 'active' : $remittances = '';
(strtolower($module) == 'performancesreport') ? $performancesreport = 'active' : $performancesreport = '';
(strtolower($module) == 'communicationexpense') ? $communicationexpense = 'active' : $communicationexpense = '';
(strtolower($module) == 'othercompensations') ? $othercompensations = 'active' : $othercompensations = '';
(strtolower($module) == 'joborder') ? $joborder = 'active' : $joborder = '';
(strtolower($module) == 'overtimereport') ? $overtimereport = 'active' : $overtimereport = '';
(strtolower($module) == 'ptmidyearbonus') ? $ptmidyearbonus = 'active' : $ptmidyearbonus = '';
(strtolower($module) == 'pbbreports') ? $pbbreports = 'active' : $pbbreports = '';
(strtolower($module) == 'yearendbonusreports') ? $yearendbonusreports = 'active' : $yearendbonusreports = '';
(strtolower($module) == 'coewcreports') ? $coewcreports = 'active' : $coewcreports = '';
(strtolower($module) == 'coereports') ? $coereports = 'active' : $coereports = '';
(strtolower($module) == 'initialsalaryreports') ? $initialsalaryreports = 'active' : $initialsalaryreports = '';
?>

<nav class="nav-sidebar">
    <ul class="nav">
       <!--  <li class="{{ $loansreport }}">
            <a href="{{ url('payrolls/reports/loansreport') }}"  >
            LOAN REPORTS</a>
        </li> -->

        <li class="{{ $payrollregister }}">
            <a href="{{ url('payrolls/reports/payrollregister') }}"  >
                <span>PAYROLL REGISTER</span>
            </a>
        </li>
        <li class="{{ $payrolltransfers }}">
            <a href="{{ url('payrolls/reports/payrolltransfers') }}"  >
                <span>PAYROLL TRANSFERS</span>
            </a>
        </li>
        <li class="{{ $payslips }}">
            <a href="{{ url('payrolls/reports/payslips') }}"  >
                <span>PAYSLIP</span>
            </a>
        </li>
        <!-- <li class="{{ $banksreport }}">
            <a href="{{ url('payrolls/reports/banksreport') }}"  >
                <span>BANK REPORTS</span>
            </a>
        </li> -->
        <!-- <li class="{{ $performancesreport }}">
            <a href="{{ url('payrolls/reports/performancesreport') }}"  >
                <span>PEI REPORTS</span>
            </a>
        </li> -->
        <li class="{{ $othercompensations }}">
            <a href="{{ url('payrolls/reports/othercompensations/rata') }}"  >
                <span>OTHER COMPENSATIONS</span>
            </a>
        </li>
        <li class="{{ $remittances }}">
            <a href="{{ url('payrolls/reports/remittances/gsis') }}"  >
                <span>GOVERNMENT REPORTS</span>
            </a>
        </li>
        <!-- <li class="{{ $joborder }}">
            <a href="{{ url('payrolls/reports/joborder') }}"  >
                <span>JO INDIVIDUAL SALARY REPORT</span>
            </a>
        </li> -->
        <li class="{{ $overtimereport }}">
            <a href="{{ url('payrolls/reports/overtimereport') }}"  >
                <span>OVERTIME REPORTS</span>
            </a>
        </li>
        <li class="{{ $ptmidyearbonus }}">
            <a href="{{ url('payrolls/reports/ptmidyearbonus') }}"  >
                <span>MID YEAR BONUS REPORTS</span>
            </a>
        </li>
        <li class="{{ $pbbreports }}">
            <a href="{{ url('payrolls/reports/pbbreports') }}"  >
                <span>PBB REPORTS</span>
            </a>
        </li>
        <li class="{{ $yearendbonusreports }}">
            <a href="{{ url('payrolls/reports/yearendbonusreports') }}"  >
                <span>YEAR END BONUS AND CASH GIFT REPORTS</span>
            </a>
        </li>
        <li class="{{ $coewcreports }}">
            <a href="{{ url('payrolls/reports/coewcreports') }}"  >
                <span>CERTIFICATE OF EMPLOYEE WITH COMPENSATION</span>
            </a>
        </li>
        <li class="{{ $coereports }}">
            <a href="{{ url('payrolls/reports/coereports') }}"  >
                <span>CERTIFICATE OF EMPLOYEE</span>
            </a>
        </li>
<!--         <li class="{{ $initialsalaryreports }}">
            <a href="{{ url('payrolls/reports/initialsalaryreports') }}"  >
                <span>INITIAL SALARY REPORTS</span>
            </a>
        </li> -->
        <!-- <li class="nav-divider"></li> -->

    </ul>
</nav>
<br><br>




