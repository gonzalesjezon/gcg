
<table class="table table-responsive datatable nowrap" id="tbl_loaninfo">
	<thead>
		<tr>
			<!-- <th>Effectivity Date</th> -->
			<th>Loan</th>
			<th>Total Loan</th>
			<th>Loan Balance</th>
			<th>Amortization</th>
			<th>For Year</th>
			<th>For Month</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>

	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_loaninfo').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	columnDefs:[
	 		{
	 			targets:[1,2,3],
	 			className:'dt-body-right'
	 		}
	 	]
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_loaninfo tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {
	        $(this).removeClass('selected');
			employee_id 		= $(this).data('employee_id');
			$('#employee_id').val(employee_id);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');
			btndelete = $(this).data('btndelete');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btndelete).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}
	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }

	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
