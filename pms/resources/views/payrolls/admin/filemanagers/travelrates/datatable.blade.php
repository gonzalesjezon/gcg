<br><br>
<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_positions">
		<thead>
			<tr>
				<th>Travel Rate</th>
				<th>Status</th>
				<th>Created</th>
			</tr>
		</thead>
		<tbody class="text-center">
			@foreach($data as $value)
			<tr data-id="{{ $value->id }}" data-rate="{{ $value->rate_amount }}" data-status="{{ $value->status }}"  data-btnnew="newPositions" data-btnedit="editPositions" data-btnsave="savePositions" data-btncancel="cancelPositions">
				<td>{{ $value->rate_amount }}</td>
				<td>{{ $value->status }}</td>
				<td>{{ $value->created_at }}</td>
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_positions').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_positions tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	       	rate_id 	= $(this).data('id');
			rate 		= $(this).data('rate');
			status 		= $(this).data('status');

			$('#rate_id').val(rate_id);
			$('#rate_amount').val(rate);
			$('#status').val(status);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});


})
</script>
