<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_deductions">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
				<th>Type</th>
				<th>Amount</th>
				<th>Payroll Group</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $value)
				<tr data-id="{{ $value->id }}">
					<td>{{ $value->code }}</td>
					<td>{{ $value->name }}</td>
					<td>{{ $value->tax_type }}</td>
					<td>{{ $value->amount }}</td>
					<td>{{ $value->payroll_group }}</td>
					<td class="text-center">
						<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteItem" data-id="{{ $value->id }}"><i class="fa fa-trash"></i> Delete</a>'
					</td>
				</tr>
				@endforeach

		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_deductions').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	'responsive':true,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_deductions tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );


	$('#tbl_deductions tr').on('click',function(){
		var id = 0;
		id = $(this).data('id');
		$.ajax({
			url:base_url+module_prefix+module+'/getItem',
			data:{'id':id},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				$('#code').val(data.code);
				$('#name').val(data.name);
				$('#tax_type').val(data.tax_type);
				$('#amount').val(data.amount);
				$('#payroll_group').val(data.payroll_group);
				$('#itr_classification').val(data.itr_classification);
				$('#alphalist_classification').val(data.alphalist_classification);
				$('#for_update').val(data.id);
			}
		})
	});

})
</script>
