<div class="col-md-12">
	<table class="table datatable" id="taxannual">
		<thead>
			<tr>
				<th>For the Year</th>
				<th>Below</th>
				<th>Above</th>
				<th>Rate (%)</th>
				<th>Rate Amount</th>
				<th>Excess Over</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $key => $value)
				<tr data-id="{{ $value->id }}">
					<td>{{ $value->from_year }} - {{ $value->to_year }}</td>
					<td>{{ number_format($value->below_amount,2) }}</td>
					<td>{{ number_format($value->above_amount,2) }}</td>
					<td>{{ $value->rate_percentage }}</td>
					<td>{{ number_format($value->rate_amount,2) }}</td>
					<td>{{ number_format($value->excess_amount,2) }}</td>

				</tr>
				@endforeach

		</tbody>
	</table>

</div>
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#taxannual').DataTable({
		'dom':'<lf<t>pi>',
		"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	});

	$('#taxannual tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});


})
</script>
