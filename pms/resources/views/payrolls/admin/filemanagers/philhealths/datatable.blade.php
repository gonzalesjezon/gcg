<div class="col-md-12">
	<table class="table" id="philtables">
		<thead>
			<tr>
				<th>Salary Bracket</th>
				<th>Salary Range</th>
				<th>Salary Base</th>
				<th>Monthly Contrib.</th>
				<th>Employee Share</th>
				<th>Employer Share</th>
				<th>Effectivity Date</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $value)
				<tr>
					<td>{{ $value->salary_bracket }}</td>
					<td>{{ $value->monthly_salary_range }}</td>
					<td>{{ $value->salary_base }}</td>
					<td>{{ $value->monthly_contribution }}</td>
					<td>{{ $value->ee_share }}</td>
					<td>{{ $value->er_share }}</td>
					<td>{{ $value->effectivity_date }}</td>
					<td class="text-center">
						<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteItem" data-id="{{ $value->id }}"><i class="fa fa-trash"></i> Delete</a>'
					</td>
				</tr>
				@endforeach

		</tbody>
	</table>
	<span class="paginate-style"> {!! $data->render() !!}</span>
</div>
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#philtables').DataTable({
		'dom':'<lf<t>pi>',
	});

	$('#philtables tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});

})
</script>
