@extends('app-front')

@section('content')
<style type="text/css">
.noborder{
    border:none !important;
}
.noborder-top{
    border-top:none !important;
}

input{
	text-align: right;
}
</style>
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<table class="table borderless" style="border:none;font-weight: bold">
				<tr class="text-left">
					<td><span>Transaction Year</span></td>
				</tr>
				<tr>
					<td colspan="2">
						<select class="employee-type form-control font-style2 select2" id="select_year" name="select_year" placeholder="Year" >
							<option value=""></option>
						</select>
					</td>
				</tr>
			</table>
			<div class="col-md-4">
				<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
			</div>
			<div  style="margin-bottom: 30px;">
				<input type="text" name="filter_search" class="form-control _searchname text-left" placeholder="Search Name">
			</div>
			<div class="namelist" style="position: relative;top: -25px;">
				{!! $controller->show() !!}
			</div>
		</div>
		<div class="col-md-9">
			<div class="col-md-12" style="padding: 10px;">
				<div class="col-md-6">
					<div class="progress hidden">
						  <div class="progress-bar" role="progressbar"  id="progressBar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
						  </div>
						  <span class="sr-only">0% Complete</span>
					</div>
					<div class="newSummary-name">
						<label id="lbl_empname" style="text-transform: uppercase;"></label>
					</div>
				</div>
				<div class="col-md-6 text-right">
					<div class="btn-group">
						<button class="btn btn-md btn-info submitme" style="font-weight: bold;" id="btn_save">SAVE</button>
					</div>
					<div class="btn-group">
						<button class="btn btn-md btn-success" style="font-weight: bold;" id="btn_compute">COMPUTE</button>
					</div>
				</div>
			</div>
			<div class="tab-container" style="margin-top: 15px;">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#payrollsummary">Summary</a></li>
					<li><a href="#attendance">Previous and Beg Bal.</a></li>
					<li><a href="#benefitsinfo">Gross Compensation</a></li>
					<li><a href="#contribution">Mandatory and Tax</a></li>
					<li><a href="#loansinfo">Non Taxable</a></li>
					<li><a href="#deducinfo">Taxable Benefits</a></li>
				</ul>
			</div>
			<form method="post" action="{{ url($module_prefix.'/'.$module) }}" onsubmit="return false" id="form" class="myForm">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="employee_number" id="employee_number">
			<div class="tab-content myForm">
				<!-- PAYROLL SUMMARY -->
				<div id="payrollsummary" class="tab-pane fade in active">
					<div class="row">
						<div class="col-md-12" style="padding-top: 10px;">
							<div class="col-md-6">
								<div class="panel" style="padding: 25px;">
									<table class="table noborder borderless">
										<tbody class="noborder-top">
											<tr>
												<td>Gross Compensation</td>
												<td>
													<input type="text" name="gross_compensation_amount" id="gross_compensation_amount" class="form-control font-style2" readonly>
												</td>
											</tr>
											<tr>
												<td>Mandatory Deductions</td>
												<td>
													<input type="text" name="md_amount" id="md_amount" class="form-control font-style2" readonly>
												</td>
											</tr>
											<tr>
												<td>Taxable Benefits in Excess of</td>
												<td>
													<input type="text" name="taxable_benefits_amount" id="taxable_benefits_amount" class="form-control font-style2" readonly>
												</td>
											</tr>
											<tr>
												<td>Taxable Income</td>
												<td>
													<input type="text" name="taxable_income_amount" id="taxable_income_amount" class="form-control font-style2" readonly>
												</td>
											</tr>
											<tr>
												<td>Tax Rate</td>
												<td>
													<input type="text" name="tax_rate" id="tax_rate" class="form-control font-style2" readonly>
												</td>
											</tr>
											<tr>
												<td>Up To</td>
												<td>
													<input type="text" name="up_to" id="up_to" class="form-control font-style2" readonly>
												</td>
											</tr>
											<tr>
												<td>Tax</td>
												<td>
													<input type="text" name="tax" id="tax" class="form-control font-style2 "readonly>
												</td>
											</tr>
											<tr>
												<td>In Excess</td>
												<td>
													<input type="text" name="in_excess" id="in_excess" class="form-control font-style2" readonly>
												</td>
											</tr>
											<tr>
												<td>Total Tax Due</td>
												<td>
													<input type="text" name="total_tax_due" id="total_tax_due" class="form-control font-style2" readonly>
												</td>
											</tr>
											<tr>
												<td>Tax Due as of</td>
												<td>
													<input type="text" name="tax_due_as_of" id="tax_due_as_of" class="form-control font-style2" readonly>
												</td>
											</tr>
											<tr>
												<td>Tax Due previous employer</td>
												<td>
													<input type="text" name="tax_due_prev_employer" id="tax_due_prev_employer" class="form-control font-style2" readonly>
												</td>
											</tr>
											<tr>
												<td>Tax Net Due</td>
												<td>
													<input type="text" name="tax_net_due" id="tax_net_due" class="form-control font-style2" readonly>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- PAYROLL SUMMARY -->
				<!-- 1st TAB -->
				<div id="attendance" class="tab-pane fade in">
					<div class="row">
						<div class="col-md-12" style="padding-top: 10px;">
							<div class="col-md-9">
								<div class="panel" style="padding: 25px;">
									<label>Previous Employer</label>
									<table class="table noborder borderless">
										<tbody class="noborder-top prevForm">
											<tr>
												<td>Gross Taxable Income</td>
												<td>
													<input type="text" name="gross_taxable_prev_employer_amount" id="gross_taxable_prev_employer_amount" class="form-control font-style2 onlyNumber">
												</td>
												<td>Date</td>
												<td>
													<input type="text" name="as_of_date_prev_employer" id="as_of_date_prev_employer" class="form-control font-style2 datepicker">
												</td>
											</tr>
											<tr>
												<td>13th Month Pay</td>
												<td>
													<input type="text" name="thirteen_month_prev_employer" id="thirteen_month_prev_employer" class="form-control font-style2 onlyNumber">
												</td>
											</tr>
											<tr>
												<td>Tax Withheld</td>
												<td>
													<input type="text" name="tax_withheld_prev_employer" id="tax_withheld_prev_employer" class="form-control font-style2 onlyNumber">
												</td>
											</tr>
											<tr>
												<td>Mandatory Deductions</td>
												<td>
													<input type="text" name="mandatory_deductions_prev_employer" id="mandatory_deductions_prev_employer" class="form-control font-style2 onlyNumber">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="panel" style="padding: 25px;">
									<label>Beginning Balance</label>
									<table class="table noborder borderless">
										<tbody class="noborder-top prevForm">
											<tr>
												<td ">Gross Taxable Income</td>
												<td>
													<input type="text" name="gross_taxable_income_amount" id="gross_taxable_income_amount" class="form-control font-style2 onlyNumber" placeholder="0.00">
												</td>
												<td>As Of</td>
												<td>
													<input type="text" name="as_of_date" id="as_of_date" class="form-control font-style2 datepicker">
												</td>
											</tr>
											<tr>
												<td>13th Month Pay</td>
												<td>
													<input type="text" name="thirteen_month" id="thirteen_month" class="form-control font-style2 onlyNumber" placeholder="0.00">
												</td>
											</tr>
											<tr>
												<td>Tax Withheld</td>
												<td>
													<input type="text" name="tax_withheld" id="tax_withheld" class="form-control font-style2 onlyNumber" placeholder="0.00">
												</td>
											</tr>
											<tr>
												<td>Mandatory Deductions</td>
												<td>
													<input type="text" name="mandatory_deductions" id="mandatory_deductions" class="form-control font-style2 onlyNumber" placeholder="0.00">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div style="height: 50px;"></div>
							</div>
						</div>
					</div>
				</div>
				<!-- 1st TAB -->
				<!-- 2nd TAB -->
				<div id="benefitsinfo" class="tab-pane fade in">
					<div class="row">
						<div class="col-md-12" style="padding-top: 10px;">
							<div class="col-md-9">
								<div class="panel" style="padding: 25px;">
									<table class="table noborder borderless">
										<tbody class="noborder-top clearForm">
											<tr class="text-center">
												<td></td>
												<td>Basic Salary</td>
												<td>Overtime</td>
												<td>LWOP</td>
											</tr>
											<tr>
												<td>January</td>
												<td>
													<input type="text" name="basic_jan" id="basic_jan" class="form-control font-style2 onlyNumber bs_1" >
												</td>
												<td>
													<input type="text" name="overtime_jan" id="overtime_jan" class="form-control font-style2 onlyNumber ot_1 ot_amount">
												</td>
												<td>
													<input type="text" name="lwop_jan" id="lwop_jan" class="form-control font-style2 onlyNumber lwop_1 lwop_amount">
												</td>
											</tr>
											<tr>
												<td>February</td>
												<td>
													<input type="text" name="basic_feb" id="basic_feb" class="form-control font-style2 onlyNumber bs_2" >
												</td>
												<td>
													<input type="text" name="overtime_feb" id="overtime_feb" class="form-control font-style2 onlyNumber ot_2 ot_amount">
												</td>
												<td>
													<input type="text" name="lwop_feb" id="lwop_feb" class="form-control font-style2 onlyNumber lwop_2 lwop_amount">
												</td>
											</tr>
											<tr>
												<td>March</td>
												<td>
													<input type="text" name="basic_mar" id="basic_mar" class="form-control font-style2 onlyNumber bs_3" >
												</td>
												<td>
													<input type="text" name="overtime_mar" id="overtime_mar" class="form-control font-style2 onlyNumber ot_3 ot_amount">
												</td>
												<td>
													<input type="text" name="lwop_mar" id="lwop_mar" class="form-control font-style2 onlyNumber lwop_3 lwop_amount">
												</td>
											</tr>
											<tr>
												<td>April</td>
												<td>
													<input type="text" name="basic_apr" id="basic_apr" class="form-control font-style2 onlyNumber bs_4" >
												</td>
												<td>
													<input type="text" name="overtime_apr" id="overtime_apr" class="form-control font-style2 onlyNumber ot_4 ot_amount">
												</td>
												<td>
													<input type="text" name="lwop_apr" id="lwop_apr" class="form-control font-style2 onlyNumber lwop_4 lwop_amount">
												</td>
											</tr>
											<tr>
												<td>May</td>
												<td>
													<input type="text" name="basic_may" id="basic_may" class="form-control font-style2 onlyNumber bs_5" >
												</td>
												<td>
													<input type="text" name="overtime_may" id="overtime_may" class="form-control font-style2 onlyNumber ot_5 ot_amount">
												</td>
												<td>
													<input type="text" name="lwop_may" id="lwop_may" class="form-control font-style2 onlyNumber lwop_5 lwop_amount">
												</td>
											</tr>
											<tr>
												<td>June</td>
												<td>
													<input type="text" name="basic_jun" id="basic_jun" class="form-control font-style2 onlyNumber bs_6" >
												</td>
												<td>
													<input type="text" name="overtime_jun" id="overtime_jun" class="form-control font-style2 onlyNumber ot_6 ot_amount">
												</td>
												<td>
													<input type="text" name="lwop_jun" id="lwop_jun" class="form-control font-style2 onlyNumber lwop_6 lwop_amount">
												</td>
											</tr>
											<tr>
												<td>July</td>
												<td>
													<input type="text" name="basic_jul" id="basic_jul" class="form-control font-style2 onlyNumber bs_7" >
												</td>
												<td>
													<input type="text" name="overtime_jul" id="overtime_jul" class="form-control font-style2 onlyNumber ot_7 ot_amount">
												</td>
												<td>
													<input type="text" name="lwop_jul" id="lwop_jul" class="form-control font-style2 onlyNumber lwop_7 lwop_amount">
												</td>
											</tr>
											<tr>
												<td>August</td>
												<td>
													<input type="text" name="basic_aug" id="basic_aug" class="form-control font-style2 onlyNumber bs_8" >
												</td>
												<td>
													<input type="text" name="overtime_aug" id="overtime_aug" class="form-control font-style2 onlyNumber ot_8 ot_amount">
												</td>
												<td>
													<input type="text" name="lwop_aug" id="lwop_aug" class="form-control font-style2 onlyNumber lwop_8 lwop_amount">
												</td>
											</tr>
											<tr>
												<td>September</td>
												<td>
													<input type="text" name="basic_sep" id="basic_sep" class="form-control font-style2 onlyNumber bs_9" >
												</td>
												<td>
													<input type="text" name="overtime_sep" id="overtime_sep" class="form-control font-style2 onlyNumber ot_9 ot_amount">
												</td>
												<td>
													<input type="text" name="lwop_sep" id="lwop_sep" class="form-control font-style2 onlyNumber lwop_9 lwop_amount">
												</td>
											</tr>
											<tr>
												<td>October</td>
												<td>
													<input type="text" name="basic_oct" id="basic_oct" class="form-control font-style2 onlyNumber bs_10" >
												</td>
												<td>
													<input type="text" name="overtime_oct" id="overtime_oct" class="form-control font-style2 onlyNumber ot_10 ot_amount">
												</td>
												<td>
													<input type="text" name="lwop_oct" id="lwop_oct" class="form-control font-style2 onlyNumber lwop_10 lwop_amount">
												</td>
											</tr>
											<tr>
												<td>November</td>
												<td>
													<input type="text" name="basic_nov" id="basic_nov" class="form-control font-style2 onlyNumber bs_11" >
												</td>
												<td>
													<input type="text" name="overtime_nov" id="overtime_nov" class="form-control font-style2 onlyNumber ot_11 ot_amount">
												</td>
												<td>
													<input type="text" name="lwop_nov" id="lwop_nov" class="form-control font-style2 onlyNumber lwop_11 lwop_amount">
												</td>
											</tr>
											<tr>
												<td>December</td>
												<td>
													<input type="text" name="basic_dec" id="basic_dec" class="form-control font-style2 onlyNumber bs_12" >
												</td>
												<td>
													<input type="text" name="overtime_dec" id="overtime_dec" class="form-control font-style2 onlyNumber ot_12 ot_amount">
												</td>
												<td>
													<input type="text" name="lwop_dec" id="lwop_dec" class="form-control font-style2 onlyNumber lwop_12 lwop_amount">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-3">
								<div class="panel" style="padding: 10px;">
									<table class="table noborder borderless">
										<tbody class="noborder-top">
											<tr class="text-left">
												<td>
													<span>Total Basic</span>
													<input type="text" name="total_basic" id="total_basic" class="form-control font-style2" readonly placeholder="0.00">
												</td>
											</tr>
											<tr class="text-left">
												<td>
													<span>Total Overtime</span>
													<input type="text" name="total_overtime" id="total_overtime" class="form-control font-style2" readonly placeholder="0.00">
												</td>
											</tr>
											<tr class="text-left">
												<td>
													<span>Total LWOP</span>
													<input type="text" name="total_lwop" id="total_lwop" class="form-control font-style2" readonly placeholder="0.00">
												</td>
											</tr>
											<tr class="text-left">
												<td>
													<span>Total Gross</span>
													<input type="text" name="total_gross" id="total_gross" class="form-control font-style2" readonly placeholder="0.00">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- 2nd TAB -->
				<!-- 3rd TAB -->
				<div id="contribution" class="tab-pane fade in">
					<div class="row">
						<div class="col-md-12" style="padding-top: 10px;">
							<div class="col-md-9">
								<div class="panel" style="padding: 25px;">
									<table class="table noborder borderless">
										<tbody class="noborder-top clearForm">
											<tr class="text-center">
												<td></td>
												<td>Mandatory Deductions</td>
												<td>Tax Withheld</td>
											</tr>
											<tr>
												<td>January</td>
												<td>
													<input type="text" name="contribution_jan" id="contribution_jan" class="form-control font-style2 onlyNumber cont_1 cont_amount" >
												</td>
												<td>
													<input type="text" name="taxwithheld_jan" id="taxwithheld_jan" class="form-control font-style2 onlyNumber taxwithheld_1">
												</td>
											</tr>
											<tr>
												<td>February</td>
												<td>
													<input type="text" name="contribution_feb" id="contribution_feb" class="form-control font-style2 onlyNumber cont_2 cont_amount" >
												</td>
												<td>
													<input type="text" name="taxwithheld_feb" id="taxwithheld_feb" class="form-control font-style2 onlyNumber taxwithheld_2">
												</td>
											</tr>
											<tr>
												<td>March</td>
												<td>
													<input type="text" name="contribution_mar" id="contribution_mar" class="form-control font-style2 onlyNumber cont_3 cont_amount"  >
												</td>
												<td>
													<input type="text" name="taxwithheld_mar" id="taxwithheld_mar" class="form-control font-style2 onlyNumber taxwithheld_3" >
												</td>
											</tr>
											<tr>
												<td>April</td>
												<td>
													<input type="text" name="contribution_apr" id="contribution_apr" class="form-control font-style2 onlyNumber cont_4 cont_amount" >
												</td>
												<td>
													<input type="text" name="taxwithheld_apr" id="taxwithheld_apr" class="form-control font-style2 onlyNumber taxwithheld_4">
												</td>
											</tr>
											<tr>
												<td>May</td>
												<td>
													<input type="text" name="contribution_may" id="contribution_may" class="form-control font-style2 onlyNumber cont_5 cont_amount" >
												</td>
												<td>
													<input type="text" name="taxwithheld_may" id="taxwithheld_may" class="form-control font-style2 onlyNumber taxwithheld_5">
												</td>
											</tr>
											<tr>
												<td>June</td>
												<td>
													<input type="text" name="contribution_jun" id="contribution_jun" class="form-control font-style2 onlyNumber cont_6 cont_amount" >
												</td>
												<td>
													<input type="text" name="taxwithheld_jun" id="taxwithheld_jun" class="form-control font-style2 onlyNumber taxwithheld_6">
												</td>
											</tr>
											<tr>
												<td>July</td>
												<td>
													<input type="text" name="contribution_jul" id="contribution_jul" class="form-control font-style2 onlyNumber cont_7 cont_amount" >
												</td>
												<td>
													<input type="text" name="taxwithheld_jul" id="taxwithheld_jul" class="form-control font-style2 onlyNumber taxwithheld_7">
												</td>
											</tr>
											<tr>
												<td>August</td>
												<td>
													<input type="text" name="contribution_aug" id="contribution_aug" class="form-control font-style2 onlyNumber cont_8 cont_amount" >
												</td>
												<td>
													<input type="text" name="taxwithheld_aug" id="taxwithheld_aug" class="form-control font-style2 onlyNumber taxwithheld_8">
												</td>
											</tr>
											<tr>
												<td>September</td>
												<td>
													<input type="text" name="contribution_sep" id="contribution_sep" class="form-control font-style2 onlyNumber cont_9 cont_amount" >
												</td>
												<td>
													<input type="text" name="taxwithheld_sep" id="taxwithheld_sep" class="form-control font-style2 onlyNumber taxwithheld_9">
												</td>
											</tr>
											<tr>
												<td>October</td>
												<td>
													<input type="text" name="contribution_oct" id="contribution_oct" class="form-control font-style2 onlyNumber cont_10 cont_amount" >
												</td>
												<td>
													<input type="text" name="taxwithheld_oct" id="taxwithheld_oct" class="form-control font-style2 onlyNumber taxwithheld_10">
												</td>
											</tr>
											<tr>
												<td>November</td>
												<td>
													<input type="text" name="contribution_nov" id="contribution_nov" class="form-control font-style2 onlyNumber cont_11 cont_amount" >
												</td>
												<td>
													<input type="text" name="taxwithheld_nov" id="taxwithheld_nov" class="form-control font-style2 onlyNumber taxwithheld_11">
												</td>
											</tr>
											<tr>
												<td>December</td>
												<td>
													<input type="text" name="contribution_dec" id="contribution_dec" class="form-control font-style2 onlyNumber cont_12 cont_amount" >
												</td>
												<td>
													<input type="text" name="taxwithheld_dec" id="taxwithheld_dec" class="form-control font-style2 onlyNumber taxwithheld_12">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-3">
								<div class="panel" style="padding: 10px;">
									<table class="table noborder borderless">
										<tbody class="noborder-top">
											<tr class="text-left">
												<td>
													<span>Total Mandatory</span>
													<input type="text" name="total_contribution" id="total_contribution" class="form-control font-style2" readonly placeholder="0.00">
												</td>
											</tr>
											<tr class="text-left">
												<td>
													<span>Total Tax Withheld</span>
													<input type="text" name="total_taxwithheld" id="total_taxwithheld" class="form-control font-style2" readonly placeholder="0.00">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- 3rd TAB -->
				<!-- 4th TAB -->
				<div id="loansinfo" class="tab-pane fade in">
					<div class="row">
						<div class="col-md-12" style="padding-top: 10px;">
							<div class="col-md-9">
								<div class="panel" style="padding: 25px;">
									<table class="table noborder borderless">
										<tbody class="noborder-top clearForm">
											<tr class="text-center">
												<td></td>
												<td>PERA</td>
												<td>RATA</td>
											</tr>
											<tr>
												<td>January</td>
												<td>
													<input type="text" name="pera_jan" id="pera_jan" class="form-control font-style2 onlyNumber pera_1 pera_amount">
												</td>
												<td>
													<input type="text" name="rata_jan" id="rata_jan" class="form-control font-style2 onlyNumber rata_1 rata_amount">
												</td>
											</tr>
											<tr>
												<td>February</td>
												<td>
													<input type="text" name="pera_feb" id="pera_feb" class="form-control font-style2 onlyNumber pera_2 pera_amount">
												</td>
												<td>
													<input type="text" name="rata_feb" id="rata_feb" class="form-control font-style2 onlyNumber rata_2 rata_amount">
												</td>
											</tr>
											<tr>
												<td>March</td>
												<td>
													<input type="text" name="pera_mar" id="pera_mar" class="form-control font-style2 onlyNumber pera_3 pera_amount">
												</td>
												<td>
													<input type="text" name="rata_mar" id="rata_mar" class="form-control font-style2 onlyNumber rata_3 rata_amount">
												</td>
											</tr>
											<tr>
												<td>April</td>
												<td>
													<input type="text" name="pera_apr" id="pera_apr" class="form-control font-style2 onlyNumber pera_4 pera_amount">
												</td>
												<td>
													<input type="text" name="rata_apr" id="rata_apr" class="form-control font-style2 onlyNumber rata_4 rata_amount">
												</td>
											</tr>
											<tr>
												<td>May</td>
												<td>
													<input type="text" name="pera_may" id="pera_may" class="form-control font-style2 onlyNumber pera_5 pera_amount">
												</td>
												<td>
													<input type="text" name="rata_may" id="rata_may" class="form-control font-style2 onlyNumber rata_5 rata_amount">
												</td>
											</tr>
											<tr>
												<td>June</td>
												<td>
													<input type="text" name="pera_jun" id="pera_jun" class="form-control font-style2 onlyNumber pera_6 pera_amount">
												</td>
												<td>
													<input type="text" name="rata_jun" id="rata_jun" class="form-control font-style2 onlyNumber rata_6 rata_amount">
												</td>
											</tr>
											<tr>
												<td>July</td>
												<td>
													<input type="text" name="pera_jul" id="pera_jul" class="form-control font-style2 onlyNumber pera_7 pera_amount">
												</td>
												<td>
													<input type="text" name="rata_jul" id="rata_jul" class="form-control font-style2 onlyNumber rata_7 rata_amount">
												</td>
											</tr>
											<tr>
												<td>August</td>
												<td>
													<input type="text" name="pera_aug" id="pera_aug" class="form-control font-style2 onlyNumber pera_8 pera_amount">
												</td>
												<td>
													<input type="text" name="rata_aug" id="rata_aug" class="form-control font-style2 onlyNumber rata_8 rata_amount">
												</td>
											</tr>
											<tr>
												<td>September</td>
												<td>
													<input type="text" name="pera_sep" id="pera_sep" class="form-control font-style2 onlyNumber pera_9 pera_amount">
												</td>
												<td>
													<input type="text" name="rata_sep" id="rata_sep" class="form-control font-style2 onlyNumber rata_9 rata_amount">
												</td>
											</tr>
											<tr>
												<td>October</td>
												<td>
													<input type="text" name="pera_oct" id="pera_oct" class="form-control font-style2 onlyNumber pera_10 pera_amount">
												</td>
												<td>
													<input type="text" name="rata_oct" id="rata_oct" class="form-control font-style2 onlyNumber rata_10 rata_amount">
												</td>
											</tr>
											<tr>
												<td>November</td>
												<td>
													<input type="text" name="pera_nov" id="pera_nov" class="form-control font-style2 onlyNumber pera_11 pera_amount">
												</td>
												<td>
													<input type="text" name="rata_nov" id="rata_nov" class="form-control font-style2 onlyNumber rata_11 rata_amount">
												</td>
											</tr>
											<tr>
												<td>December</td>
												<td>
													<input type="text" name="pera_dec" id="pera_dec" class="form-control font-style2 onlyNumber pera_12 pera_amount">
												</td>
												<td>
													<input type="text" name="rata_dec" id="rata_dec" class="form-control font-style2 onlyNumber rata_12 rata_amount">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-3">
								<div class="panel" style="padding: 10px;">
									<table class="table noborder borderless">
										<tbody class="noborder-top">
											<tr class="text-left">
												<td>
													<span>Total PERA</span>
													<input type="text" name="total_pera_amount" id="total_pera_amount" class="form-control font-style2" readonly>
												</td>
											</tr>
											<tr class="text-left">
												<td>
													<span>Total RATA</span>
													<input type="text" name="total_rata_amount" id="total_rata_amount" class="form-control font-style2" readonly>
												</td>
											</tr>
											<tr class="text-left">
												<td>
													<span>Clothing Allowance</span>
													<input type="text" name="total_clothing_amount" id="total_clothing_amount" class="form-control font-style2 onlyNumber">
												</td>
											</tr>
											<tr class="text-left">
												<td>
													<span>Total Non Taxable</span>
													<input type="text" name="total_nontaxable_amount" id="total_nontaxable_amount" class="form-control font-style2" readonly>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- 4th TAB  -->
				<!-- 5th TAB -->
				<div id="deducinfo" class="tab-pane fade in">
					<div class="row">
						<div class="col-md-12" style="padding-top: 10px;">
							<div class="col-md-6">
								<div class="panel" style="padding: 25px;">
									<table class="table noborder borderless">
										<tbody class="noborder-top clearForm">
											<tr class="text-center">
												<td></td>
												<td>LONGEVITY PAY</td>
												<td>HAZARD PAY</td>
											</tr>
											<tr>
												<td>January</td>
												<td>
													<input type="text" name="pera_jan" id="lp_jan" class="form-control font-style2 onlyNumber lp_1 lp_amount">
												</td>
												<td>
													<input type="text" name="hp_jan" id="hp_jan" class="form-control font-style2 onlyNumber hp_1 hp_amount">
												</td>
											</tr>
											<tr>
												<td>February</td>
												<td>
													<input type="text" name="lp_feb" id="lp_feb" class="form-control font-style2 onlyNumber lp_2 lp_amount">
												</td>
												<td>
													<input type="text" name="hp_feb" id="hp_feb" class="form-control font-style2 onlyNumber hp_2 hp_amount">
												</td>
											</tr>
											<tr>
												<td>March</td>
												<td>
													<input type="text" name="lp_mar" id="lp_mar" class="form-control font-style2 onlyNumber lp_3 lp_amount">
												</td>
												<td>
													<input type="text" name="hp_mar" id="hp_mar" class="form-control font-style2 onlyNumber hp_3 hp_amount">
												</td>
											</tr>
											<tr>
												<td>April</td>
												<td>
													<input type="text" name="lp_apr" id="lp_apr" class="form-control font-style2 onlyNumber lp_4 lp_amount">
												</td>
												<td>
													<input type="text" name="hp_apr" id="hp_apr" class="form-control font-style2 onlyNumber hp_4 hp_amount">
												</td>
											</tr>
											<tr>
												<td>May</td>
												<td>
													<input type="text" name="lp_may" id="lp_may" class="form-control font-style2 onlyNumber lp_5 lp_amount">
												</td>
												<td>
													<input type="text" name="hp_may" id="hp_may" class="form-control font-style2 onlyNumber hp_5 hp_amount">
												</td>
											</tr>
											<tr>
												<td>June</td>
												<td>
													<input type="text" name="lp_jun" id="lp_jun" class="form-control font-style2 onlyNumber lp_6 lp_amount">
												</td>
												<td>
													<input type="text" name="hp_jun" id="hp_jun" class="form-control font-style2 onlyNumber hp_6 hp_amount">
												</td>
											</tr>
											<tr>
												<td>July</td>
												<td>
													<input type="text" name="lp_jul" id="lp_jul" class="form-control font-style2 onlyNumber lp_7 lp_amount">
												</td>
												<td>
													<input type="text" name="hp_jul" id="hp_jul" class="form-control font-style2 onlyNumber hp_7 hp_amount">
												</td>
											</tr>
											<tr>
												<td>August</td>
												<td>
													<input type="text" name="lp_aug" id="lp_aug" class="form-control font-style2 onlyNumber lp_8 lp_amount">
												</td>
												<td>
													<input type="text" name="hp_aug" id="hp_aug" class="form-control font-style2 onlyNumber hp_8 hp_amount">
												</td>
											</tr>
											<tr>
												<td>September</td>
												<td>
													<input type="text" name="lp_sep" id="lp_sep" class="form-control font-style2 onlyNumber lp_9 lp_amount">
												</td>
												<td>
													<input type="text" name="hp_sep" id="hp_sep" class="form-control font-style2 onlyNumber hp_9 hp_amount">
												</td>
											</tr>
											<tr>
												<td>October</td>
												<td>
													<input type="text" name="lp_oct" id="lp_oct" class="form-control font-style2 onlyNumber lp_10 lp_amount">
												</td>
												<td>
													<input type="text" name="hp_oct" id="hp_oct" class="form-control font-style2 onlyNumber hp_10 hp_amount">
												</td>
											</tr>
											<tr>
												<td>November</td>
												<td>
													<input type="text" name="lp_nov" id="lp_nov" class="form-control font-style2 onlyNumber lp_11 lp_amount">
												</td>
												<td>
													<input type="text" name="hp_nov" id="hp_nov" class="form-control font-style2 onlyNumber hp_11 hp_amount">
												</td>
											</tr>
											<tr>
												<td>December</td>
												<td>
													<input type="text" name="lp_dec" id="lp_dec" class="form-control font-style2 onlyNumber lp_12 lp_amount">
												</td>
												<td>
													<input type="text" name="hp_dec" id="hp_dec" class="form-control font-style2 onlyNumber hp_12 hp_amount">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel" style="padding: 25px;">
									<table class="table noborder borderless">
										<tbody class="noborder-top">
											<tr>
												<td>Loyalty</td>
												<td>
													<input type="text" name="loyalty_amount" id="loyalty_amount" class="form-control onlyNumber font-style2" placeholder="0.00">
												</td>
											</tr>
											<tr>
												<td>Honoraria</td>
												<td>
													<input type="text" name="honoraria_amount" id="honoraria_amount" class="form-control onlyNumber font-style2" placeholder="0.00">
												</td>
											</tr>
											<tr>
												<td>Performance Base</td>
												<td>
													<input type="text" name="pb_amount" id="pb_amount" class="form-control onlyNumber font-style2" placeholder="0.00">
												</td>
											</tr>
											<tr>
												<td>Collective Negotiation</td>
												<td>
													<input type="text" name="cn_amount" id="cn_amount" class="form-control onlyNumber font-style2" placeholder="0.00">
												</td>
											</tr>
											<tr>
												<td>Productivity Enhancement</td>
												<td>
													<input type="text" name="pei_amount" id="pei_amount" class="form-control onlyNumber font-style2" placeholder="0.00">
												</td>
											</tr>
											<tr>
												<td>Mid Year Bonus</td>
												<td>
													<input type="text" name="myb_amount" id="myb_amount" class="form-control onlyNumber font-style2" placeholder="0.00" >
												</td>
											</tr>
											<tr>
												<td>Year End Bonus</td>
												<td>
													<input type="text" name="yeb_amount" id="yeb_amount" class="form-control onlyNumber font-style2" placeholder="0.00" >
												</td>
											</tr>
											<tr>
												<td><span>Cash Gift</span></td>
												<td>
													<input type="text" name="total_cg_amount" id="total_cg_amount" class="form-control font-style2 onlyNumber">
												</td>
											</tr>
											<tr>
												<td>Total Taxable Benefits</td>
												<td>
													<input type="text" name="total_taxable_amount" id="total_taxable_amount" class="form-control font-style2" readonly placeholder="0.00">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- 5th TAB -->
			</div>
			<input type="hidden" name="array_tax" id="array_tax">
			<input type="hidden" name="array_pera" id="array_pera">
			<input type="hidden" name="array_cont" id="array_cont">
			<input type="hidden" name="array_rata" id="array_rata">
			<input type="hidden" name="array_basic" id="array_basic">
			<input type="hidden" name="array_ot" id="array_ot">
			<input type="hidden" name="array_lwop" id="array_lwop">
			<input type="hidden" name="array_hp" id="array_hp">
			<input type="hidden" name="array_lp" id="array_lp">
			<input type="hidden" name="array_month" id="array_month">
			<input type="hidden" name="array_id" id="array_id">
			<input type="hidden" name="for_year" id="for_year">
			<input type="hidden" name="employee_id" id="employee_id">
			<input type="hidden" name="specialtax_id" id="specialtax_id">
			<input type="hidden" name="begbalance_id" id="begbalance_id">
			<input type="hidden" name="prevemployer_id" id="prevemployer_id">
			</form>
		</div>
	</div>
</div>
@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){

// GENERATE YEAR
var year = [];
year += '<option ></option>';
for(y = 2018; y <= 2100; y++) {
    year += '<option value='+y+'>'+y+'</option>';
}
$('#select_year').html(year);

// GENERATE MONTH
month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
mArr = [];

mArr += '<option ></option>';
for ( m =  0; m <= month.length - 1; m++) {
	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
}
$('#select_month').html(mArr);


// ************************************************
var _Year;
var _Month;
$(document).on('change','#select_year',function(){
	_Year = "";
	_Year = $(this).find(':selected').val();

})
$(document).on('change','#select_month',function(){
	_Month = "";
	_Month = $(this).find(':selected').val();
})

$('.select2').select2();

$('#select_month').select2({
    allowClear:true,
    placeholder: "Month",
});

$('#select_year').select2({
    allowClear:true,
    placeholder: "Year"
});
$('.newAttendance :input').attr('disabled',true);
$('.newAttendance').attr('disabled',true);

$('.datepicker').datepicker({
	dateFormat:'yy-mm-dd'
})


$(document).on('change','#select_period',function(){
	period = $(this).val();
	arr = [];
	switch(period){
		case 'semimonthly':
			arr += '<option value="First Half">First Half</option>';
			arr += '<option value="Second Half">Second Half</option>';
		break;
		case 'monthly':
		break;
		default:
		$('#select_subperiod').html();
	}
	$('#select_subperiod').html(arr);
});


$(document).on('change','#searchby',function(){
	var val = $(this).val();
	console.log(base_url+module_prefix+module)
	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){
			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})
			$('#select_searchvalue').html(arr);
		}
	})
});


$('.nav-tabs a').click(function(){
	$(this).tab('show');
});

$('.isNumber').keypress(function (event) {
    return isNumber(event, this)
});

$('.onlyNumber').keypress(function (event) {
    return isNumber(event, this)
});

$('.onlyNumber').prop('placeholder','0.00');

$(".onlyNumber").keyup(function(){
	amount  = $(this).val();
	if(amount == 0){
		$(this).val(parseFloat(0).toFixed(2));
	}else{
		plainAmount = amount.replace(/\,/g,'')
		$(this).val(commaSeparateNumber(plainAmount));
	}
});

$('.attendance').keypress(function (event) {
	return isNumber(event, this)
});

var _listId = [];
$(document).on('click','#check_all',function(){
	if(!_Year && !_Month){
		swal({
			title: "Select year and month first",
			type: "warning",
			showCancelButton: false,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes",
			closeOnConfirm: false
		});
		$('#check_all').prop('checked',false);
	}else{
		if ($(this).is(':checked')) {
	        $('.emp_select').prop('checked', 'checked');
	        $('.emp_select:checked').each(function(){
	        	_listId.push($(this).val())
	        });
	    } else {
	        $('.emp_select').prop('checked', false)
	        _listId = [];
	    }
	}
});

$(document).on('click','.emp_select',function(){
	empid = $(this).val();
	index = $(this).data('key');
	if(!_Year && !_Month){
		swal({
			title: "Select year and month first",
			type: "warning",
			showCancelButton: false,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes",
			closeOnConfirm: false
		});
		$('.emp_select').prop('checked',false);
	}else{
		if($(this).is(':checked')){
			_listId[index] =  empid;

		}else{
			delete _listId[index];
		}
	}
});

var months = {
	'January':1,
	'February':2,
	'March':3,
	'April':4,
	'May':5,
	'June':6,
	'July':7,
	'August':8,
	'September':9,
	'October':10,
	'November':11,
	'December':12
}


var totalBasicAmount 		= 0;
var totalLWOPAmount 		= 0;
var totalOvertimeAmount 	= 0;
var totalGrossAmount 		= 0;
var totalContAmount 		= 0;
var totalPERAAmount 		= 0;
var totalRATAAmount 		= 0;
var	yearEndBonusAmount 		= 0;
var midYearBonusAmount 		= 0;
var peiAmount 				= 0;
var pbbAmount 				= 0;
var honorariaAmount 		= 0;
var clothingAmount 			= 0;
var cashGiftAmount 			= 0;
var cnaAmount 				= 0;
var loyaltyAmount 			= 0;
var count 					= 0;
var count2 					= 0;
var grossTaxableIncomeBegBalanceAmount 	= 0
var thirteenMonthBegBalanceAmount 		= 0
var taxWithheldBegBalanceAmount 		= 0
var mandatoryBegBalanceAmount 			= 0
var thirteenMonthPrevEmployer 			= 0;
var taxWitheldPrevEmployer 				= 0;
var mandatoryDeductionsPrevEmployer 	= 0;
var asOfDatePrevEmployer;
var asOfDate;
var grossTaxableIncomePrevEmployer 		= 0;


$(document).on('click','#namelist tr',function(){
	_id = $(this).data('empid');
	employeeNumber = $(this).data('employee_number');

	if(!_Year){
		swal({
			title: "Select Year First",
			type: "warning",
			showCancelButton: false,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes",
			closeOnConfirm: false
		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/getEmployeesinfo',
			data:{
				'id':_id,
				'employee_number':employeeNumber,
				'year':_Year
			},
			type:'get',
			dataType:'JSON',
			success:function(data){
				// clear_form_elements('myForm');
				$('#total_overtime').val('');
				$('#total_lwop').val('');
				$('#total_pera_amount').val('');
				$('#total_rata_amount').val();
				$('.cont_amount').val('');
				$('.ot_amount').val('');
				$('.rata_amount').val('');
				$('.pera_amount').val('');
				$('.lwop_amount').val('');

				$('#prevemployer_id').val('');
				$('#begbalance_id').val('');
				$('#specialtax_id').val('');
				clear_form_elements('prevForm');
				clear_form_elements('clearForm');

				totalBasicAmount 	= 0;
				totalLWOPAmount 	= 0;
				totalOvertimeAmount = 0;
				totalGrossAmount 	= 0;
				totalContAmount 	= 0;
				totalPERAAmount 	= 0;
				totalRATAAmount 	= 0;
				basicAmount 		= 0;
				lwopAmount 			= 0;
				contributionAmount 	= 0;
				overtimeAmount 		= 0;
				peraAmount 			= 0;
				rataAmount 			= 0;
				hpAmount 			= 0;
				lpAmount 			= 0;

				grossTaxableIncomeBegBalanceAmount 	= 0
				thirteenMonthBegBalanceAmount 		= 0
				taxWithheldBegBalanceAmount 		= 0
				mandatoryBegBalanceAmount 			= 0
				thirteenMonthPrevEmployer 			= 0;
				taxWitheldPrevEmployer 				= 0;
				mandatoryDeductionsPrevEmployer 	= 0;
				grossTaxableIncomePrevEmployer 		= 0;
				asOfDatePrevEmployer = '';
				asOfDate = '';

				firstname = (data.employeeinfo.employees) ? data.employeeinfo.employees.firstname : '';
				lastname = (data.employeeinfo.employees) ? data.employeeinfo.employees.lastname : '';
				middlename = (data.employeeinfo.employees) ? data.employeeinfo.employees.middlename : '';
				employee_id = data.employeeinfo.employee_id;

				fullname = lastname+' '+firstname+' '+middlename;
				$('#lbl_empname').text(fullname);
				$('#employee_id').val(employee_id);
				$('#employee_number').val(employeeNumber);

				if(data.taxrates.length !== 0){

					$('#btn_save').text('UPDATE');
					$('#btn_compute').text('RECOMPUTE');

					// yearEndBonusAmount
					// midYearBonusAmount
					// peiAmount
					// pbbAmount
					ctr = 1;
					$.each(data.taxrates,function(k,v){

						basicAmount 	   = (v.basic_amount) ? v.basic_amount : 0;
						contributionAmount = (v.contribution_amount) ? v.contribution_amount : 0;
						peraAmount 		   = (v.pera_amount) ? v.pera_amount : 0;
						hpAmount 		   = (v.hp_amount) ? v.hp_amount : 0;
						lpAmount 		   = (v.longevity_amount) ? v.longevity_amount : 0;
						rataAmount 		   = (v.rata_amount) ? v.rata_amount : 0;
						otAmount 		   = (v.overtime_amount) ? v.overtime_amount : 0;
						lwopAmount 		   = (v.lwop_amount) ? v.lwop_amount : 0;
						taxAmount 		   = (v.tax_amount) ? v.tax_amount : 0;
						  // ======================== //
						 // ==== BASIC SALARY  ==== //
					 	// ======================= //

						basic_amount = (basicAmount !== 0) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
						$('.bs_'+v.for_month).val(basic_amount);

						  // =============================== //
						 // ==== MANDATORY DEDUCTIONS  ==== //
					 	// =============================== //

						cont_amount = (contributionAmount !== 0) ? commaSeparateNumber(parseFloat(contributionAmount).toFixed(2)) : '';
				 		$('.cont_'+v.for_month).val(cont_amount)

				 		  // ======================== //
						 // ========= PERA  ======= //
					 	// ======================= //

					 	// totalPERAAmount += parseFloat(peraAmount);
						pera_amount = (peraAmount !== 0) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '';
				 		$('.pera_'+v.for_month).val(pera_amount)


				 		  // ======================== //
						 // ========= RATA  ======= //
					 	// ======================= //
				 		// totalRATAAmount += parseFloat(rataAmount);
						rata_amount = (rataAmount !== 0) ? commaSeparateNumber(parseFloat(rataAmount).toFixed(2)) : '';
				 		$('.rata_'+v.for_month).val(rata_amount);

				 		  // ======================= //
						 // === HAZARD PAY  ======= //
					 	// ======================= //
						hp_amount = (hpAmount !== 0) ? commaSeparateNumber(parseFloat(hpAmount).toFixed(2)) : '';
				 		$('.hp_'+v.for_month).val(hp_amount);

				 		  // ======================= //
						 // === lONGEVITY PAY  ==== //
					 	// ======================= //
						lp_amount = (lpAmount !== 0) ? commaSeparateNumber(parseFloat(lpAmount).toFixed(2)) : '';
						$('.lp_'+v.for_month).val(lp_amount);

						  // ======================= //
						 // =======LWOP  ========== //
					 	// ======================= //
						lwop_amount = (lwopAmount !== 0) ? commaSeparateNumber(parseFloat(lwopAmount).toFixed(2)) : '';
						$('.lwop_'+v.for_month).val(lwop_amount);

						  // ======================= //
						 // =======  OT  ========== //
					 	// ======================= //
						ot_amount = (otAmount !== 0) ? commaSeparateNumber(parseFloat(otAmount).toFixed(2)) : '';
						$('.ot_'+v.for_month).val(ot_amount);

						  // ======================= //
						 // =======  TAX  ========== //
					 	// ======================= //
						tax_amount = (taxAmount !== 0) ? commaSeparateNumber(parseFloat(taxAmount).toFixed(2)) : '';
						$('.taxwithheld_'+v.for_month).val(tax_amount);

						ctr++;
					});

					if(data.specialtax !== null){
						$('#specialtax_id').val(data.specialtax.id);
						yearEndBonusAmount = (data.specialtax.year_end_amount) ? data.specialtax.year_end_amount : 0;
						midYearBonusAmount = (data.specialtax.mid_year_amount) ? data.specialtax.mid_year_amount : 0;
						loyaltyAmount = (data.specialtax.loyalty_amount) ? data.specialtax.loyalty_amount : 0;
						peiAmount = (data.specialtax.pei_amount) ? data.specialtax.pei_amount : 0;
						pbbAmount = (data.specialtax.performance_base_amount) ? data.specialtax.performance_base_amount : 0;
						honorariaAmount = (data.specialtax.honoraria_amount) ? data.specialtax.honoraria_amount : 0;
						cnaAmount = (data.specialtax.collective_negotiation_amount) ? data.specialtax.collective_negotiation_amount : 0;
						cashGiftAmount = (data.specialtax.cash_gift_amount) ? data.specialtax.cash_gift_amount : 0;
						clothingAmount = (data.specialtax.clothing_allowance_amount) ? data.specialtax.clothing_allowance_amount : 0;

						yearend_amount = (yearEndBonusAmount !== 0) ? commaSeparateNumber(parseFloat(yearEndBonusAmount).toFixed(2)) : '';
						midyear_amount = (midYearBonusAmount !== 0) ? commaSeparateNumber(parseFloat(midYearBonusAmount).toFixed(2)) : '';
						loyalty_amount = (loyaltyAmount !== 0) ? commaSeparateNumber(parseFloat(loyaltyAmount).toFixed(2)) : '';
						pei_amount = (peiAmount !== 0) ? commaSeparateNumber(parseFloat(peiAmount).toFixed(2)) : '';
						pbb_amount = (pbbAmount !== 0) ? commaSeparateNumber(parseFloat(pbbAmount).toFixed(2)) : '';
						cn_amount = (cnaAmount !== 0) ? commaSeparateNumber(parseFloat(cnaAmount).toFixed(2)) : '';
						cash_gift = (cashGiftAmount !== 0) ? commaSeparateNumber(parseFloat(cashGiftAmount).toFixed(2)) : '';
						clothing_amount = (clothingAmount !== 0) ? commaSeparateNumber(parseFloat(clothingAmount).toFixed(2)) : '';
						honoraria_amount = (honorariaAmount !== 0) ? commaSeparateNumber(parseFloat(honorariaAmount).toFixed(2)) : '';

						$('#yeb_amount').val(yearend_amount);
						$('#myb_amount').val(midyear_amount);
						$('#loyalty_amount').val(loyalty_amount);
						$('#honoraria_amount').val(honoraria_amount);
						$('#pei_amount').val(pei_amount);
						$('#pb_amount').val(pbb_amount);
						$('#cn_amount').val(cn_amount);
						$('#total_cg_amount').val(cash_gift)
						$('#total_clothing_amount').val(clothing_amount);
					}

					if(data.previousemployer !== null){
						$('#prevemployer_id').val(data.previousemployer.id);
						prevTaxableIncome = (data.previousemployer.gross_taxable_income) ? data.previousemployer.gross_taxable_income : 0;
						prevThirteenMonth = (data.previousemployer.thirteen_month_pay) ? data.previousemployer.thirteen_month_pay : 0;
						prevMandatory = (data.previousemployer.mandatory_deduction) ? data.previousemployer.mandatory_deduction : 0;
						prevTaxWitheld = (data.previousemployer.tax_withheld) ? data.previousemployer.tax_withheld : 0;
						prevAsOfDate = (data.previousemployer.as_of_date) ? data.previousemployer.as_of_date : 0;

						prev_taxable_income = (prevTaxableIncome) ? commaSeparateNumber(parseFloat(prevTaxableIncome).toFixed(2)) : '';
						prev_thirteen_month = (prevThirteenMonth) ? commaSeparateNumber(parseFloat(prevThirteenMonth).toFixed(2)) : '';
						prev_mandatory = (prevMandatory) ? commaSeparateNumber(parseFloat(prevMandatory).toFixed(2)) : '';
						prev_tax_withheld = (prevTaxWitheld) ? commaSeparateNumber(parseFloat(prevTaxWitheld).toFixed(2)) : '';

						$('#gross_taxable_prev_employer_amount').val(prev_taxable_income).trigger('keyup');
						$('#thirteen_month_prev_employer').val(prev_thirteen_month).trigger('keyup');
						$('#mandatory_deductions_prev_employer').val(prev_mandatory).trigger('keyup');
						$('#tax_withheld_prev_employer').val(prev_tax_withheld).trigger('keyup');
						$('#as_of_date_prev_employer').val(prevAsOfDate).trigger('keyup');
					}

					if(data.beginningbalance !== null){
						$('#begbalance_id').val(data.beginningbalance.id);
						begTaxableIncome = (data.beginningbalance.gross_taxable_income) ? data.beginningbalance.gross_taxable_income : 0;
						begThirteenMonth = (data.beginningbalance.thirteen_month_pay) ? data.beginningbalance.thirteen_month_pay : 0;
						begMandatory = (data.beginningbalance.mandatory_deduction) ? data.beginningbalance.mandatory_deduction : 0;
						begTaxWitheld = (data.beginningbalance.tax_withheld) ? data.beginningbalance.tax_withheld : 0;
						begAsOfDate = (data.beginningbalance.as_of_date) ? data.beginningbalance.as_of_date : 0;

						beg_taxable_income = (begTaxableIncome) ? commaSeparateNumber(parseFloat(begTaxableIncome).toFixed(2)) : '';
						beg_thirteen_month = (begThirteenMonth) ? commaSeparateNumber(parseFloat(begThirteenMonth).toFixed(2)) : '';
						beg_mandatory = (begMandatory) ? commaSeparateNumber(parseFloat(begMandatory).toFixed(2)) : '';
						beg_tax_withheld = (begTaxWitheld) ? commaSeparateNumber(parseFloat(begTaxWitheld).toFixed(2)) : '';

						$('#gross_taxable_income_amount').val(beg_taxable_income).trigger('keyup');
						$('#thirteen_month').val(beg_thirteen_month).trigger('keyup');
						$('#mandatory_deductions').val(beg_mandatory).trigger('keyup');
						$('#tax_withheld').val(beg_tax_withheld).trigger('keyup');
						$('#as_of_date').val(begAsOfDate).trigger('keyup');
					}


				}else{

					$('#btn_save').text('SAVE');
					$('#btn_compute').text('COMPUTE');

					basic_one = (data.salaryinfo.salary_new_rate) ? data.salaryinfo.salary_new_rate : 0;
					// basic_two = (data.salaryinfo.basic_amount_two) ? data.salaryinfo.basic_amount_two : 0;
					basicAmount = basic_one;

					gsis_cont_amount = (data.employeeinfo.gsis_contribution) ? data.employeeinfo.gsis_contribution : 0;
					philhealth_cont_amount = (data.employeeinfo.philhealth_contribution) ? data.employeeinfo.philhealth_contribution : 0;
					pagibig_cont_amount = (data.employeeinfo.pagibig_contribution) ? data.employeeinfo.pagibig_contribution : 0;
					eaAmount = (data.ea) ? data.ea.deduct_amount : 0;
					contributionAmount = (parseFloat(gsis_cont_amount) + parseFloat(philhealth_cont_amount) + parseFloat(pagibig_cont_amount) + parseFloat(eaAmount));

					// contributionAmount = parseFloat(gsis_cont_amount);

					peraAmount = (data.pera) ? data.pera.benefit_amount : 0;
					hpAmount = (data.hp) ? data.hp.benefit_amount : 0;
					lpAmount = (data.lp) ? data.lp.longevity_amount : 0;
					yearEndBonusAmount = (data.yearendbonus.yearend) ? data.yearendbonus.yearend : 0;
					cashGiftAmount = (data.yearendbonus.cashgift) ? data.yearendbonus.cashgift : 0;
					midYearBonusAmount = (data.midyearbonus) ? data.midyearbonus : 0;
					peiAmount = (data.pei) ? data.pei : 0;
					pbbAmount = (data.pbb) ? data.pbb : 0;
					honorariaAmount = (data.honoraria) ? data.honoraria.amount : 0;
					cnaAmount = (data.cna) ? data.cna : 0;

					rataCode = ['TRANSPO','REP'];
					rata_one = 0;
					rata_two = 0;
					$.each(data.rata,function(k,v){
						if(rataCode[0] === v.benefits.code){
							rata_one = (v.benefit_amount) ? v.benefit_amount : 0;
						}
						if(rataCode[1] === v.benefits.code){
							rata_two = (v.benefit_amount) ? v.benefit_amount : 0;
						}
						rataAmount = parseFloat(rata_one) + parseFloat(rata_two);
					});

					for (var i = 1; i <= 12; i++) {

						  // ======================== //
						 // ==== BASIC SALARY  ==== //
					 	// ======================= //

						basic_amount = (basicAmount !== 0) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
						$('.bs_'+i).val(basic_amount);

						  // =============================== //
						 // ==== MANDATORY DEDUCTIONS  ==== //
					 	// =============================== //

						cont_amount = (contributionAmount !== 0) ? commaSeparateNumber(parseFloat(contributionAmount).toFixed(2)) : '';
				 		$('.cont_'+i).val(cont_amount)

				 		  // ======================== //
						 // ========= PERA  ======= //
					 	// ======================= //

					 	// totalPERAAmount += parseFloat(peraAmount);
						pera_amount = (peraAmount !== 0) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '';
				 		$('.pera_'+i).val(pera_amount)


				 		  // ======================== //
						 // ========= RATA  ======= //
					 	// ======================= //
				 		// totalRATAAmount += parseFloat(rataAmount);
						rata_amount = (rataAmount !== 0) ? commaSeparateNumber(parseFloat(rataAmount).toFixed(2)) : '';
				 		$('.rata_'+i).val(rata_amount);

				 		  // ======================= //
						 // === HAZARD PAY  ======= //
					 	// ======================= //
						hp_amount = (hpAmount !== 0) ? commaSeparateNumber(parseFloat(hpAmount).toFixed(2)) : '';
				 		$('.hp_'+i).val(hp_amount);

				 		  // ======================= //
						 // === lONGEVITY PAY  ==== //
					 	// ======================= //
						lp_amount = (lpAmount !== 0) ? commaSeparateNumber(parseFloat(lpAmount).toFixed(2)) : '';
				 		$('.lp_'+i).val(lp_amount);


					}

					yearend_amount = (yearEndBonusAmount !== 0) ? commaSeparateNumber(parseFloat(yearEndBonusAmount).toFixed(2)) : '';
					midyear_amount = (midYearBonusAmount !== 0) ? commaSeparateNumber(parseFloat(midYearBonusAmount).toFixed(2)) : '';
					pei_amount = (peiAmount !== 0) ? commaSeparateNumber(parseFloat(peiAmount).toFixed(2)) : '';
					pbb_amount = (pbbAmount !== 0) ? commaSeparateNumber(parseFloat(pbbAmount).toFixed(2)) : '';
					honoraria_amount = (honorariaAmount !== 0) ? commaSeparateNumber(parseFloat(honorariaAmount).toFixed(2)) : '';
					cash_gift_amount = (cashGiftAmount !== 0) ? commaSeparateNumber(parseFloat(cashGiftAmount).toFixed(2)) : '';
					cna_amount = (cnaAmount !== 0) ? commaSeparateNumber(parseFloat(cnaAmount).toFixed(2)) : '';

					$('#cn_amount').val(cna_amount);
					$('#yeb_amount').val(yearend_amount);
					$('#honoraria_amount').val(honoraria_amount);
					$('#myb_amount').val(midyear_amount);
					$('#pei_amount').val(pei_amount);
					$('#pb_amount').val(pbb_amount);
					$('#total_cg_amount').val(cash_gift_amount);
				}



				  // ======================= //
				 // ========= LWOP  ======= //
			 	// ======================= //
				if(data.transaction.length !== 0){
			 		count = 1;
					$.each(data.transaction,function(k,v){
						month = months[v.month];

						lwopAmount = (v.total_absences_amount) ? v.total_absences_amount : 0;
						// totalLWOPAmount += parseFloat(lwopAmount);
						lwop_amount = (lwopAmount !== 0) ? commaSeparateNumber(parseFloat(lwopAmount).toFixed(2)) : '';
						if(lwop_amount !== ''){
							count = month;
							$('.lwop_'+month).val(lwop_amount);
						}

					});
				}

				  // ======================= //
				 // ===== OVERTIME  ======= //
			 	// ======================= //

				if(data.overtimepay.length !== 0){
					count = 1;
					$.each(data.overtimepay,function(k,v){
						month = months[v.month];
						overtimeAmount = (v.used_amount) ? v.used_amount : 0;
						// totalOvertimeAmount += parseFloat(overtimeAmount);
						overtime_amount = (overtimeAmount !== 0) ? commaSeparateNumber(parseFloat(overtimeAmount).toFixed(2)) : '';
						if(overtime_amount !== ''){
							count = month;
						}

						$('.ot_'+month).val(overtime_amount);

					});
				}

				for (var i = 1; i <= 12 - parseInt(count); i++) {
					totalBasicAmount += parseFloat(basicAmount);
					totalContAmount += parseFloat(contributionAmount);
				}
				$('#btn_compute').trigger('click');
			}
		})
	}
});


$(document).on('keyup','#total_clothing_amount',function(){
	clothingAmount = $(this).val().replace(/,/g, '');
})

$(document).on('keyup','#yeb_amount',function(){
	yearEndBonusAmount = $(this).val().replace(/,/g, '');
})
$(document).on('keyup','#myb_amount',function(){
	midYearBonusAmount = $(this).val().replace(/,/g, '');
})
$(document).on('keyup','#total_cg_amount',function(){
	cashGiftAmount = $(this).val().replace(/,/g, '');
})

$(document).on('keyup','#pb_amount',function(){
	pbbAmount = $(this).val().replace(/,/g, '');
})
$(document).on('keyup','#pei_amount',function(){
	peiAmount = $(this).val().replace(/,/g, '');
})
$(document).on('keyup','#cn_amount',function(){
	cnaAmount = $(this).val().replace(/,/g, '');
})
$(document).on('keyup','#loyalty_amount',function(){
	loyaltyAmount = $(this).val().replace(/,/g, '');
})
$(document).on('keyup','#honoraria_amount',function(){
	honorariaAmount = $(this).val().replace(/,/g, '');
})

$(document).on('keyup','#gross_taxable_income_amount',function(){
	grossTaxableIncomeBegBalanceAmount = $(this).val().replace(/,/g, '');
})
$(document).on('keyup','#thirteen_month',function(){
	thirteenMonthBegBalanceAmount = $(this).val().replace(/,/g, '');
})
$(document).on('keyup','#tax_withheld',function(){
	taxWithheldBegBalanceAmount = $(this).val().replace(/,/g, '');
})
$(document).on('keyup','#mandatory_deductions',function(){
	mandatoryBegBalanceAmount = $(this).val().replace(/,/g, '');
})
$(document).on('change','#as_of_date',function(){
	asOfDate = $(this).val();
})

$(document).on('change','#as_of_date_prev_employer',function(){
	asOfDatePrevEmployer = $(this).val();
})

$(document).on('keyup','#gross_taxable_prev_employer_amount',function(k,v){
	grossTaxableIncomePrevEmployer = $(this).val().replace(/,/g, '');
});

$(document).on('keyup','#thirteen_month_prev_employer',function(k,v){
	thirteenMonthPrevEmployer = $(this).val().replace(/,/g, '');
});
$(document).on('keyup','#tax_withheld_prev_employer',function(k,v){
	taxWitheldPrevEmployer = $(this).val().replace(/,/g, '');
});
$(document).on('keyup','#mandatory_deductions_prev_employer',function(k,v){
	mandatoryDeductionsPrevEmployer = $(this).val().replace(/,/g, '');
});
$(document).on('keyup','#as_of_date_prev_employer',function(k,v){
	asOfDatePrevEmployer = $(this).val().replace(/,/g, '');
});

$(document).on('click','#btn_compute',function(){

	if(!_Year){

		swal({
			  title: 'Select Year and Employee First',
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-warning",
			  confirmButtonText: "OK",
			  closeOnConfirm: false
		})

	}else{

		$('#as_of_date').trigger('change');
		$('#as_of_date_prev_employer').trigger('change');

		otAmount = 0;
		lwopAmount = 0;
		peraAmount = 0;
		rataAmount = 0;
		contAmount = 0;
		mandatoryDeduct = 0;
		taxAmount  = 0;
		basicAmount  = 0;
		lpAmount  	 = 0;
		hpAmount  	 = 0;
		taxableInExcess = 0;

		arrPera  = [];
		arrBasic = [];
		arrRata  = [];
		arrCont  = [];
		arrLP 	 = [];
		arrHP  	 = [];
		arrOT 	 = [];
		arrLWOP	 = [];
		arrMonth = [];


		grossTaxableIncome 			= 0;
		thirteenMonthAmount 		= 0;
		mandatoryAmount 			= 0;
		taxableAmount 				= 0;
		grossBegBalTaxableIncome 	= 0;
		thirteenMonthBegBalAmount 	= 0;
		mandatoryBegBalAmount 		= 0;
		taxWitheldBegBal 			= 0;

		if(asOfDate !== undefined && asOfDate !== ''){

			date = asOfDate.split('-');
			// if(date[2] == 30 || date[2] == 31){
			// 	count = parseInt(date[1]) + 1;
			// }else{
			// 	count = parseInt(date[1]);
			// }

			// if current month is equal to december
			if(date[1] == 12){
				count = parseInt(date[1])
			}else{
				// current month + 1
				count = parseInt(date[1]) + 1;
				count2 = parseInt(date[1]);

				for (var i = 1; i <= count2; i++) {
					$('.ot_'+i).val('');
					$('.lwop_'+i).val('');
					$('.pera_'+i).val('');
					$('.rata_'+i).val('');
					$('.cont_'+i).val('');
					$('.taxwithheld_'+i).val('');
					$('.bs_'+i).val('');
					$('.lp_'+i).val('');
					$('.hp_'+i).val('');
				}
			}

			grossBegBalTaxableIncome = (grossTaxableIncomeBegBalanceAmount) ? grossTaxableIncomeBegBalanceAmount : 0;
			thirteenMonthBegBalAmount = (thirteenMonthBegBalanceAmount) ? thirteenMonthBegBalanceAmount : 0;
			mandatoryBegBalAmount = (mandatoryBegBalanceAmount) ? mandatoryBegBalanceAmount : 0;
			taxWitheldBegBal = (taxWithheldBegBalanceAmount) ? taxWithheldBegBalanceAmount : 0;

		}else if(asOfDatePrevEmployer !== undefined && asOfDatePrevEmployer){

			date = asOfDatePrevEmployer.split('-');
			// if(date[2] == 30 || date[2] == 31){
			// 	count = parseInt(date[1]) + 1;
			// }else{
			// 	count = parseInt(date[1]);
			// }

			// if current month is equal to december
			if(date[1] == 12){
				count = parseInt(date[1])
			}else{
				// current month + 1
				count = parseInt(date[1]) + 1;
				count2 = parseInt(date[1]);
			}

			grossTaxableIncome = (grossTaxableIncomePrevEmployer) ? grossTaxableIncomePrevEmployer : 0;
			thirteenMonthAmount 		= (thirteenMonthPrevEmployer) ? thirteenMonthPrevEmployer : 0;
			taxableAmount 		= (taxWitheldPrevEmployer) ? taxWitheldPrevEmployer : 0;
			mandatoryAmount 	= (mandatoryDeductionsPrevEmployer) ? mandatoryDeductionsPrevEmployer : 0;

		}else{
			count = 1;
		}


		var inc = 0;
		for (var i = count; i <= 12; i++) {

			ot_amount = $('.ot_'+i).val().replace(/,/g, '');
			absent_amount = $('.lwop_'+i).val().replace(/,/g, '');
			pera_amount = $('.pera_'+i).val().replace(/,/g, '');
			rata_amount = $('.rata_'+i).val().replace(/,/g, '');
			cont_amount = $('.cont_'+i).val().replace(/,/g, '');
			tax_amount = $('.taxwithheld_'+i).val().replace(/,/g, '');
			basic_amount = $('.bs_'+i).val().replace(/,/g, '');
			lp_amount = $('.lp_'+i).val().replace(/,/g, '');
			hp_amount = $('.hp_'+i).val().replace(/,/g, '');

			ot_amount = (ot_amount) ? ot_amount : 0;
			absent_amount = (absent_amount) ? absent_amount : 0;
			pera_amount = (pera_amount) ? pera_amount : 0;
			rata_amount = (rata_amount) ? rata_amount : 0;
			cont_amount = (cont_amount) ? cont_amount : 0;
			tax_amount = (tax_amount) ? tax_amount : 0;
			basic_amount = (basic_amount) ? basic_amount : 0;
			lp_amount = (lp_amount) ? lp_amount : 0;
			hp_amount = (hp_amount) ? hp_amount : 0;

			arrPera[i]  = pera_amount;
			arrBasic[i] = basic_amount;
			arrRata[i]  = rata_amount;
			arrCont[i]  = cont_amount;
			arrLP[i] 	= lp_amount;
			arrHP[i] 	= hp_amount;
			arrOT[i] 	= ot_amount;
			arrLWOP[i] 	= absent_amount;
			arrMonth[i] = i;

			lwopAmount += parseFloat(absent_amount);
			peraAmount += parseFloat(pera_amount);
			otAmount += parseFloat(ot_amount);
			rataAmount += parseFloat(rata_amount);
			contAmount += parseFloat(cont_amount);
			taxAmount += parseFloat(tax_amount);
			basicAmount += parseFloat(basic_amount);
			lpAmount += parseFloat(lp_amount);
			hpAmount += parseFloat(hp_amount);

			inc++;
		}

		grossPayAmount = compute_gross_pay(basicAmount,otAmount,lwopAmount);
		grossPayAmount = parseFloat(grossPayAmount) + parseFloat(grossTaxableIncome) + parseFloat(grossBegBalTaxableIncome);

		nonTaxableAmount = compute_nontaxable(peraAmount,rataAmount,clothingAmount)

		// taxableBenefitsAmount = compute_taxable(hpAmount,lpAmount,loyaltyAmount,pbbAmount,cnaAmount,peiAmount,midYearBonusAmount,yearEndBonusAmount);

		taxableBenefitsAmount = compute_taxable(hpAmount,lpAmount,loyaltyAmount,pbbAmount,cnaAmount,peiAmount,midYearBonusAmount,yearEndBonusAmount,cashGiftAmount);


		taxableBenefitsAmount = parseFloat(taxableBenefitsAmount) + parseFloat(thirteenMonthAmount) + parseFloat(thirteenMonthBegBalAmount);
		if(taxableBenefitsAmount > 90000){
			taxableInExcess = parseFloat(taxableBenefitsAmount) - 90000;
		}else{
			taxableInExcess = 0;
		}

		if(mandatoryAmount > 0 || mandatoryBegBalAmount > 0){
			mandatoryDeduct = parseFloat(contAmount) + parseFloat(mandatoryAmount) + parseFloat(mandatoryBegBalAmount);
		}

		taxableIncome = parseFloat(grossPayAmount) - parseFloat(mandatoryDeduct) + parseFloat(taxableInExcess);

		total_pera_amount = (peraAmount !== 0) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '';
		total_rata_amount = (rataAmount !== 0) ? commaSeparateNumber(parseFloat(rataAmount).toFixed(2)) : '';
		total_overtime_amount = (otAmount !== 0) ? commaSeparateNumber(parseFloat(otAmount).toFixed(2)) : '';
		total_lwop_amount = (lwopAmount !== 0) ? commaSeparateNumber(parseFloat(lwopAmount).toFixed(2)) : '';
		total_gross_amount = (grossPayAmount !== 0) ? commaSeparateNumber(parseFloat(grossPayAmount).toFixed(2)) : '';
		total_nontaxable_amount = (nonTaxableAmount !== 0) ? commaSeparateNumber(parseFloat(nonTaxableAmount).toFixed(2)) : '';
		total_cont_amount = (contAmount !== 0) ? commaSeparateNumber(parseFloat(contAmount).toFixed(2)) : '';
		total_tax_amount = (taxAmount !== 0) ? commaSeparateNumber(parseFloat(taxAmount).toFixed(2)) : '';
		total_basic_amount = (basicAmount !== 0) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
		total_taxable_amount = (taxableBenefitsAmount !== 0) ? commaSeparateNumber(parseFloat(taxableBenefitsAmount).toFixed(2)) : '';
		taxable_inexcess_amount = (taxableInExcess !== 0) ? commaSeparateNumber(parseFloat(taxableInExcess).toFixed(2)) : '';
		taxable_income_amount = (taxableIncome !== 0) ? commaSeparateNumber(parseFloat(taxableIncome).toFixed(2)) : '';
		mandatory_amount = (mandatoryDeduct !== 0) ? commaSeparateNumber(parseFloat(mandatoryDeduct).toFixed(2)) : '';

		$('#total_pera_amount').val(total_pera_amount);
		$('#total_rata_amount').val(total_rata_amount);
		$('#total_overtime').val(total_overtime_amount);
		$('#total_lwop').val(total_lwop_amount);
		$('#total_gross').val(total_gross_amount);
		$('#total_nontaxable_amount').val(total_nontaxable_amount);
		$('#total_contribution').val(total_cont_amount);
		$('#total_taxwithheld').val(total_tax_amount);
		$('#total_basic').val(total_basic_amount);
		$('#total_taxable_amount').val(total_taxable_amount);

		$('#gross_compensation_amount').val(total_gross_amount);
		$('#md_amount').val(mandatory_amount);
		$('#taxable_benefits_amount').val(taxable_inexcess_amount);
		$('#taxable_income_amount').val(taxable_income_amount);

		$.ajax({
			url:base_url+module_prefix+module+'/getAnnualTaxPolicy',
			data:{
				'year':_Year,
				'tax_income':taxableIncome,
				'limit':count,
				'employee_id':$('#employee_id').val(),
				'employee_number':$('#employee_number').val()
			},
			type:'GET',
			dataType:'JSON',
			success:function(res){

				excess_amount = (res.rates) ? res.rates.excess_amount : 0 ;
				rateAmount = (res.rates) ? res.rates.rate_amount : 0;
				rate_percentage = (res.rates) ? res.rates.rate_percentage : 0 ;

				taxPercentage = parseInt(rate_percentage)*100/100;
				excessAmount = excess_amount;

				total_amount = parseFloat(taxableIncome) - parseFloat(excessAmount);
				percentage = (parseFloat(taxPercentage)/100);
				inExcessAmount = parseFloat(total_amount) * percentage;
				taxAmountDue = parseFloat(rateAmount) + parseFloat(inExcessAmount);
				taxWithheldAmount = parseFloat(taxAmountDue) - parseFloat(taxableAmount) - parseFloat(taxWitheldBegBal);

				taxDistributed = parseFloat(taxWithheldAmount) / inc;

				excess_over_amount = (excessAmount !== 0) ? commaSeparateNumber(parseFloat(excessAmount).toFixed(2)) : '';
				tax_rate_amount = (rateAmount !== 0) ? commaSeparateNumber(parseFloat(rateAmount).toFixed(2)) : '';
				in_excess_amount = (inExcessAmount !== 0) ? commaSeparateNumber(parseFloat(inExcessAmount).toFixed(2)) : '';
				tax_amount_due = (taxAmountDue !== 0) ? commaSeparateNumber(parseFloat(taxAmountDue).toFixed(2)) : '';
				tax_amount = (taxWithheldAmount !== 0) ? commaSeparateNumber(parseFloat(taxWithheldAmount).toFixed(2)) : '';
				tax_beg_amount = (taxWitheldBegBal !== 0) ? commaSeparateNumber(parseFloat(taxWitheldBegBal).toFixed(2)) : '';
				tax_prev_amount = (taxableAmount !== 0) ? commaSeparateNumber(parseFloat(taxableAmount).toFixed(2)) : '';

				$('#up_to').val(excess_over_amount);
				$('#tax_rate').val(taxPercentage+'%');
				$('#tax').val(tax_rate_amount);
				$('#in_excess').val(in_excess_amount);
				$('#total_tax_due').val(tax_amount_due);
				$('#tax_net_due').val(tax_amount);
				$('#tax_due_as_of').val(tax_beg_amount);
				$('#tax_due_prev_employer').val(tax_prev_amount);

				totalTaxWitheld = 0;
				taxRate = [];

				// if(res.taxrates.length !== 0){
				// 	$.each(res.taxrates,function(k,v){
				// 		tb_taxAmount = (v.tax_amount) ? v.tax_amount : 0;
				// 		totalTaxWitheld += parseFloat(tb_taxAmount);
				// 		tb_taxAmount = (tb_taxAmount !== 0) ? commaSeparateNumber(parseFloat(tb_taxAmount).toFixed(2)) : '';
			 // 			$('.taxwithheld_'+v.for_month).val(tb_taxAmount)
				// 	})
				// }else{

				for (var i = count; i <= 12; i++) {
					totalTaxWitheld += parseFloat(taxDistributed);
					if(taxDistributed){
						taxRate[i] = taxDistributed;
					}
					tax_withheld_amount = (taxDistributed !== 0) ? commaSeparateNumber(parseFloat(taxDistributed).toFixed(2)) : '';
			 		$('.taxwithheld_'+i).val(tax_withheld_amount)
				}

				// }

				rateId = [];
				if(res.rate_id.length !== 0){
					ctr2 = 1;
					$.each(res.rate_id,function(k,v){
						rateId[ctr2] = v.id;
						ctr2++;
					});
				}

				total_tax_withheld = (totalTaxWitheld !== 0) ? commaSeparateNumber(parseFloat(totalTaxWitheld).toFixed(2)) : '';
				$('#total_taxwithheld').val(total_tax_withheld);

				$('#array_id').val(rateId);
				$('#array_tax').val(taxRate);
				$('#for_year').val(_Year);

				$('#array_pera').val(arrPera);
				$('#array_cont').val(arrCont);
				$('#array_rata').val(arrRata);
				$('#array_basic').val(arrBasic);
				$('#array_lp').val(arrLP);
				$('#array_hp').val(arrHP);
				$('#array_ot').val(arrOT);
				$('#array_lwop').val(arrLWOP);
				$('#array_month').val(arrMonth);

			}

		});

		swal({  title: 'Tax Computed!',
			text: '',
			type: "success",
			icon: 'success',

		})
	}
});



// function compute_taxable(hp_amount,lp_amount,loyalty_amount,pbb_amount,cna_amount,pei_amount,midyear_amount,yearend_amount){
// 	hp_amount = (hp_amount) ? hp_amount :0;
// 	lp_amount = (lp_amount) ? lp_amount :0;
// 	loyalty_amount = (loyalty_amount) ? loyalty_amount :0;
// 	pbb_amount = (pbb_amount) ? pbb_amount :0;
// 	cna_amount = (cna_amount) ? cna_amount :0;
// 	pei_amount = (pei_amount) ? pei_amount :0;
// 	midyear_amount = (midyear_amount) ? midyear_amount :0;
// 	yearend_amount = (yearend_amount) ? yearend_amount :0;

// 	taxable_amount = parseFloat(hp_amount) + parseFloat(lp_amount) + parseFloat(loyalty_amount) + parseFloat(pbb_amount) + parseFloat(cna_amount) + parseFloat(pei_amount) + parseFloat(midyear_amount) + parseFloat(yearend_amount);
// 	return taxable_amount;
// }

function compute_taxable(hp_amount,lp_amount,loyalty_amount,pbb_amount,cna_amount,pei_amount,midyear_amount,year_end,cash_gift){
	hp_amount = (hp_amount) ? hp_amount :0;
	lp_amount = (lp_amount) ? lp_amount :0;
	loyalty_amount = (loyalty_amount) ? loyalty_amount :0;
	pbb_amount = (pbb_amount) ? pbb_amount :0;
	cna_amount = (cna_amount) ? cna_amount :0;
	pei_amount = (pei_amount) ? pei_amount :0;
	midyear_amount = (midyear_amount) ? midyear_amount :0;
	year_end = (year_end) ? year_end :0;
	cash_gift = (cash_gift) ? cash_gift :0;

	taxable_amount = parseFloat(hp_amount) + parseFloat(lp_amount) + parseFloat(loyalty_amount) + parseFloat(pbb_amount) + parseFloat(cna_amount) + parseFloat(pei_amount) + parseFloat(midyear_amount) + parseFloat(year_end) + parseFloat(cash_gift);
	return taxable_amount;
}

function compute_nontaxable(total_pera,total_rata,total_clothing,total_cg){
	total_pera = (total_pera) ? total_pera : 0;
	total_rata = (total_rata) ? total_rata : 0;
	total_clothing = (total_clothing) ? total_clothing : 0;
	total_cg = (total_cg) ? total_cg : 0;

	nontaxable_amount = parseFloat(total_pera) + parseFloat(total_rata) + parseFloat(total_clothing) + parseFloat(total_cg);
	return nontaxable_amount;
}

function compute_gross_pay(basic_amount,overtime_amount,lwop_amount){
	basic_amount = (basic_amount !== 0) ? basic_amount : 0;
	overtime_amount = (overtime_amount !== 0) ? overtime_amount : 0;
	lwop_amount = (lwop_amount !== 0) ? lwop_amount : 0;

	gross_pay_amount = parseFloat(basic_amount) + parseFloat(overtime_amount) - parseFloat(lwop_amount);
	return gross_pay_amount;
}


_checkpayroll = ""
$('input[type=radio][name=chk_wpayroll]').change(function() {
 	if(!_Year){
 		swal({
			  title: 'Select year and month first',
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-warning",
			  confirmButtonText: "OK",
			  closeOnConfirm: false
		})
		$(this).prop('checked',false);
 	}else{
		if (this.value == 'wpayroll') {

			$('#process_payroll').prop('disabled',true);
			$('#updateButton').removeClass('hidden');
        	_checkpayroll = 'wpayroll';
        	_listId = [];
        }
        else if (this.value == 'wopayroll') {
        	$('#process_payroll').prop('disabled',false);
        	$('#updateButton').addClass('hidden');
            _checkpayroll = 'wopayroll';
            _listId = [];
        }
        $('.btnfilter').trigger('click');
 	}
});


var timer;
$(document).on('click','.btnfilter',function(){
	// $('input.search').addClass('searchSpinner');
	tools  	  = $('#tools-form').serialize()
	year 	  = $('#select_year :selected').val();
	month 	  = $('#select_month :selected').val();
	category  = $('#select_searchvalue :selected').val();
	empstatus = $('#emp_status :selected').val();
	searchby  = $('#searchby :selected').val();
	subperiod = $('#select_subperiod :selected').val();
	period	  = $('#select_period :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {'category':category,'empstatus':empstatus,'searchby':searchby,'year':year,'month':month,'subperiod':subperiod,'period':period,'checkpayroll':_checkpayroll },
			   beforeSend:function(){
			   		$('#loading').removeClass('hidden');
			   },
			   complete:function(){
			   		$('#loading').addClass('hidden');
			   },
			   success: function(res){
			   	// console.log(res);
			      $(".namelist").html(res);
			   }
			});
		},500);
});


$(document).on('keyup','._searchname',function(){

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	"q":$('._searchname').val(),
			   	'check_payroll':_checkpayroll
			},
			   success: function(res){
			      $(".namelist").html(res);
			   }
			});
		},500);
});

$('.btn_new').on('click',function(){
	// $('#transaction_id').val('');
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_edit').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_cancel').on('click',function(){
	$('#transaction_id').val('');
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');

	$('.'+btnnew+' :input').attr("disabled",true);
	$('.'+btnnew).attr('disabled',true);
	$('#'+btnnew).removeClass('hidden');
	// $('#'+btnedit).addClass('hidden');
	$('#'+btnedit).removeClass('hidden');
	$('#'+btnsave).addClass('hidden');
	$('#'+btncancel).addClass('hidden');
	clear_form_elements('nonplantilla');
	$('.error-msg').remove();

});

$(document).off('click',".submitme").on('click',".submitme",function(){
	btn = $(this);

	$("#form").ajaxForm({
		beforeSend:function(){
		},
		success:function(data){
			par  =  JSON.parse(data);
			if(par.status){

				swal({  title: par.response,
						text: '',
						type: "success",
						icon: 'success',

					}).then(function(){

						window.location.href = base_url+module_prefix+module;
						// clear_form_elements('myform')
					});
			}else{

				swal({  title: par.response,
						text: '',
						type: "error",
						icon: 'error',

					});
			}

			btn.button('reset');
		},
		error:function(data){
			$error = data.responseJSON;
			/*reset popover*/
			$('input[type="text"], select').popover('destroy');

			/*add popover*/
			block = 0;
			$(".error-msg").remove();
			$.each($error.errors,function(k,v){
				var messages = v.join(', ');
				msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
				$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
				if(block == 0){
					$('html, body').animate({
				        scrollTop: $('.err-'+k).offset().top - 250
				    }, 500);
				    block++;
				}
			})
			$('.saving').replaceWith(btn);
		},
		always:function(){
			setTimeout(function(){
					$('.saving').replaceWith(btn);
				},300)
		}
	}).submit();

});


});

</script>
@endsection