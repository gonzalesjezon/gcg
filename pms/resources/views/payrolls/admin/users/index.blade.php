@extends('app-front')

@section('content')
<style type="text/css">
.table thead>tr>td{
	font-size: 12px !important;
}

.table tbody>tr>td{
	font-size: 12px !important;
}

.tr td{
	border-bottom: none;
}
.noborder{
    border:none !important;
}
.noborder-top{
    border-top:none !important;
}
</style>
<div class="row" style="margin-top: 90px;">
	<div class="col-md-6">
		<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id" id="id">
			<input type="hidden" name="name" id="name">
			<div class="box-1 button-style-wrapper" style="z-index: 2">
				<div class="col-md-12">
					<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savebenefits"><i class="fa fa-save"></i> New</a>
					<a class="btn btn-xs btn-info btn-savebg btn_save hidden submit"><i class="fa fa-save"></i> Save </a>
					<a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
					<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
				</div>
			</div>

			<div class="panel benefits-content" style="padding: 10px;">
				<div class="form-group">
					<table class="table borderless noborder" style="border: none;">
						<tbody class="noborder-top">
							<tr>
								<td><label>Select Employee</label></td>
								<td>
									@if ($errors->has('employee_id'))
									    <span class="text-danger">{{ $errors->first('employee_id') }}</span>
									@endif
									<select class="form-control font-style2 select2" id="employee_id" name="employee_id">
										<option value=""></option>
										@foreach($employee as $key => $value)
										<option value="{{ $value->id }}">{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</option>
										@endforeach
									</select>

								</td>
							</tr>
							<tr>
								<td><label>Username</label></td>
								<td>
									@if ($errors->has('username'))
									    <span class="text-danger">{{ $errors->first('username') }}</span>
									@endif
									<input type="text" name="username" id="username" class="form-control font-style2"  >

								</td>
							</tr>
							<tr>
								<td><label>Password</label></td>
								<td>
									@if ($errors->has('password'))
									    <span class="text-danger">{{ $errors->first('password') }}</span>
									@endif
									<input type="password" name="password" id="password" class="form-control font-style2" >
								</td>
							</tr>
							<tr>
								<td><label>Access Type</label></td>
								<td>
									<select class="form-control font-style2" id="access_type_id" name="access_type_id">
										<option></option>
									</select>
								</td>
							</tr>

						</tbody>
					</table>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-6">
		<div class="panel" style="margin-top: 10px;">
			<div class="form-group" style="padding: 10px;">
				{!! $controller->show() !!}
			</div>
		</div>
	</div>
</div>



@endsection

@section('js-logic1')
<!-- <script src="{{ asset('js/payroll-vue/benefits.js') }} "></script> -->
<script type="text/javascript">
	$(document).ready(function(){

		$('.benefits-content :input').attr("disabled",true);

		$('.btn_new').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_new').addClass('hidden');
			$('.btn_edit').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_edit').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_edit').addClass('hidden');
			$('.btn_new').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_cancel').on('click',function(){
			$('.benefits-content :input').attr("disabled",true);
			$('.btn_new').removeClass('hidden');
			$('.btn_save').addClass('hidden');
			$('.btn_edit').removeClass('hidden');
		});

		$('.select2').select2({});

		$('#employee_id').change(function(){
			name = $(this).find(':selected').text();
			$('#name').val(name)
		})


		$(".btn_cancel").click(function() {
			myform = "myform";
			clear_form_elements(myform);
			$('#for_update').val('');
			$('.error-msg').remove();
		});

		$('#tbl_benefits tr').on('click',function(){

			id = $(this).data('id');
			name = $(this).data('name');
			username = $(this).data('username');
			password = $(this).data('password');

			$('#id').val(id)
			$('#username').val(username)
			$('#name').val(name)
		});

	    $('.onlyNumber').keypress(function (event) {
	    	return isNumber(event, this)
		});

		$(".onlyNumber").keyup(function(){
			amount  = $(this).val();
			if(amount == 0){
				$(this).val('');
			}else{
				plainAmount = amount.replace(/\,/g,'')
				$(this).val(commaSeparateNumber(plainAmount));
			}
		})

	// ======================================================= //
  // ============ DELETE  FUNCTION ==================== //
// ===================================================== //

$(document).on('click','.delete_item',function(){
	id 			= $(this).data('id');
	function_name = $(this).data('function_name')

	if(id){
		swal({
			title: "Delete?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.ajax({
					url:base_url+module_prefix+module+'/'+function_name,
					data:{
						'id':id,
						'_token':"{{ csrf_token() }}"
					},
					type:'post',
					dataType:'JSON',
					success:function(res){
						swal({
							  title: 'Deleted Successfully!',
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-warning",
							  confirmButtonText: "OK",
							  closeOnConfirm: false
						}).then(function(isConfirm){
							window.location.href = base_url+module_prefix+module;
						})
					}

				})
			}else{
				return false;
			}
		});
	}
})

	});

</script>
@endsection

