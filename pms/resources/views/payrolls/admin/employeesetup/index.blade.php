@extends('app-front')
@section('content')
<div style="height: 50px;"></div>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">{{ $title }}</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="sub-panel">
						{!! $controller->show() !!}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="button-wrapper" style="position: relative;top: -20px;margin-left: 7px;">
					<a class="btn btn-xs btn-info btn-savebg btn_new" id="newEmployeeStatus" data-btnnew="newEmployeeStatus" data-btncancel="cancelEmployeeStatus" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus"><i class="fa fa-save"></i> New</a>

					<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editEmployeeStatus" data-btnnew="newEmployeeStatus" data-btncancel="cancelEmployeeStatus" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus"><i class="fa fa-save"></i> Edit</a>

					<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" data-form="form" data-btnnew="newEmployeeStatus" data-btncancel="cancelEmployeeStatus" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus" id="saveEmployeeStatus"><i class="fa fa-save"></i> Save</a>

					<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newEmployeeStatus" data-btncancel="cancelEmployeeStatus" data-form="myform" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus"id="cancelEmployeeStatus"> Cancel</a>
				</div>
				<form method="POST" action="{{ url($module_prefix.'/'.$module) }}" onsubmit="return false" id="form" class="myForm" >
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="employee_id" id="employee_id">
					<input type="hidden" name="employee_information_id" id="employee_information_id">
					<div class="col-md-3">
						<div class="form-group newEmployeeStatus">
							<label>Lastname</label>
							<input type="text" name="lastname" id="lastname" class="form-control">
						</div>
						<div class="form-group newEmployeeStatus">
							<label>Position Item</label>
							<select class="form-control" id="position_item_id" name="position_item_id">
								<option value=""></option>
								@foreach($position_items as $value)
								<option value="{{ $value->id }}">{{ $value->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group newEmployeeStatus">
							<label>Office</label>
							<select class="form-control" id="office_id" name="office_id">
								<option value=""></option>
								@foreach($offices as $value)
								<option value="{{ $value->id }}">{{ $value->Name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group newEmployeeStatus">
							<label>Status</label>
							<select id="status" name="status" class="form-control">
								<option value=""></option>
								<option value="1">Active</option>
								<option value="0">InActive</option>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group newEmployeeStatus">
							<label>Firstname</label>
							<input type="text" name="firstname" id="firstname" class="form-control">
						</div>
						<div class="form-group newEmployeeStatus">
							<label>Division</label>
							<select class="form-control" id="division_id" name="division_id">
								<option value=""></option>
								@foreach($divisions as $value)
								<option value="{{ $value->id }}">{{ $value->Name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group newEmployeeStatus">
							<label>Assumption Date</label>
							<input type="text" name="assumption_date" id="assumption_date" class="form-control datepicker">
						</div>
						<div class="form-group newEmployeeStatus">
							<label>Start Date</label>
							<input type="text" name="start_date" id="start_date" class="form-control datepicker">
						</div>
						
					</div>
					<div class="col-md-3">
						<div class="form-group newEmployeeStatus">
							<label>Middlename</label>
							<input type="text" name="middlename" id="middlename" class="form-control">
						</div>
						<div class="form-group newEmployeeStatus">
							<label>Employee Status</label>
							<select class="form-control" id="employee_status_id" name="employee_status_id">
								<option value=""></option>
								@foreach($employee_status as $value)
								<option value="{{ $value->id }}">{{ $value->Name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group newEmployeeStatus">
							<label>Hired Date</label>
							<input type="text" name="hired_date" id="hired_date" class="form-control datepicker">
						</div>
						<div class="form-group newEmployeeStatus">
							<label>End Date</label>
							<input type="text" name="end_date" id="end_date" class="form-control datepicker">
						</div>
						

					</div>
				</form>
			</div>
		</div>
	</div>
	<br>
</div>

@endsection
@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
$('.newEmployeeStatus :input').attr('disabled',true);
$('.newEmployeeStatus').attr('disabled',true);
$('.btn_new').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_edit').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_cancel').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');

	$('.'+btnnew+' :input').attr("disabled",true);
	$('.'+btnnew).attr('disabled',true);
	$('#'+btnnew).removeClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).addClass('hidden');
	$('#'+btncancel).addClass('hidden');

	$('#employee_information_id').val('');
	$('#employee_id').val('');

	form = $(this).data('form');
	clear_form_elements('myForm');
	$('.error-msg').remove();
});

$('.select2').select2();

$('.datepicker').datepicker({
	dateFormat:'yy-mm-dd'
});

})
</script>
@endsection