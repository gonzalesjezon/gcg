<table class="table scroll datatable" id="namelist">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
		</tr>
	</thead>
	<tbody >

		@foreach($data as $key => $value)

			<tr data-empid="{{ $value->id }}" data-employee_number="{{ $value->employee_number }}" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary">
				<td>[{!! $key+1 !!}] {{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</td>
			</tr>

		@endforeach

	</tbody>
</table>

<!-- <div class="ajax-loader">
  	<img src="{{ asset('images/30.gif') }}" class="img-responsive" />
</div> -->
<script type="text/javascript">
$(document).ready(function(){

});
</script>