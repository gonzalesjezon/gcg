<table class="table">
    <thead>
        <!-- <th class="check-col"><input type="checkbox" class="chk-all"></th> -->

        <?php //foreach ($cols as $key => $value):?>
            <th>Access Name</th>
            <th>Action</th>
        <?php //endforeach; ?>
    </thead>

    <tbody>
        <?php foreach ($data as $key => $value):?>
        <tr id="<?=$value->id?>">
            <!-- <td><input type="checkbox" class="chk-list"></td> -->
            <td><?=ucwords($value->name) ?></td>
            <td class="action-buttons">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                            <li><a href="{{ url($module.'/edit/'.Crypt::encrypt($value->id)) }}">Edit</a></li>
                            <li><a href="#" class="single-delete" data-id="{{ Crypt::encrypt($value->id) }}">Delete</a></li>
                    </ul>
                </div>

            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>