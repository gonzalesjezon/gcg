@extends('app-front')

@section('content')
<!-- page content -->
    <div class="row" style="margin-top: 70px;">
        <div class="col-md-10 col-sm-12 col-xs-12">
                <div class="box-1 button-style-wrapper" style="z-index: 2">
                    <div class="col-md-12">
                        <a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savebenefits"><i class="fa fa-save"></i> New</a>
                        <a class="btn btn-xs btn-info btn-savebg btn_save hidden submit"><i class="fa fa-save"></i> Save </a>
                        <a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
                        <a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
                    </div>
                </div>
                <div class="benefits-content">
                    <form method="POST" action="{{ url($module_prefix.'/'.$module.'/store') }}" id="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-xs-12 col-md-6">
                            <div class="panel panel-default" style="padding: 20px;">
                                <div class="form-group">
                                    <label>Access Name</label>
                                    <input type="text" name="name" class="form-control">
                                </div>
                                <table class="table">
                                    <thead>
                                        <th style="width:50%">Modules</th>
                                        <th>Create</th>
                                        <th>Read</th>
                                        <th>Update</th>
                                        <th>Delete</th>
                                    </thead>

                                    <tbody>
                                    <?php foreach ($access_modules as $key => $value): ?>
                                        <?php $crud = array_flip(explode('/',$value->crud));   ?>

                                        <tr>
                                            <td>{{ $value->description }}</td>
                                            <td>
                                                <?php if (isset($crud['c'])): ?>
                                                    <input type="checkbox" name="crud[{{$value->id}}][]" value="1" class="flat">
                                                <?php endif ?>
                                            </td>

                                            <td>
                                                <?php if (isset($crud['r'])): ?>
                                                    <input type="checkbox" name="crud[{{$value->id}}][]" value="2" class="flat">
                                                <?php endif ?>
                                            </td>

                                            <td>
                                                <?php if (isset($crud['u'])): ?>
                                                    <input type="checkbox" name="crud[{{$value->id}}][]" value="3" class="flat">
                                                <?php endif ?>
                                            </td>
                                            <td>
                                                <?php if (isset($crud['d'])): ?>
                                                    <input type="checkbox" name="crud[{{$value->id}}][]" value="4" class="flat">
                                                <?php endif ?>
                                            </td>

                                        </tr>
                                    <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <br />
<!-- /page content -->
@endsection

@section('js-logic1')
<!-- <script src="{{ asset('js/payroll-vue/benefits.js') }} "></script> -->
<script type="text/javascript">
    $(document).ready(function(){

        $('.benefits-content :input').attr("disabled",true);

        $('.btn_new').on('click',function(){
            $('.benefits-content :input').attr("disabled",false);
            $('.btn_new').addClass('hidden');
            $('.btn_edit').addClass('hidden');
            $('.btn_save').removeClass('hidden');
        });
        $('.btn_edit').on('click',function(){
            $('.benefits-content :input').attr("disabled",false);
            $('.btn_edit').addClass('hidden');
            $('.btn_new').addClass('hidden');
            $('.btn_save').removeClass('hidden');
        });
        $('.btn_cancel').on('click',function(){
            $('.benefits-content :input').attr("disabled",true);
            $('.btn_new').removeClass('hidden');
            $('.btn_save').addClass('hidden');
            $('.btn_edit').removeClass('hidden');
        });


        $(".btn_cancel").click(function() {
            myform = "myform";
            clear_form_elements(myform);
            $('#for_update').val('');
            $('.error-msg').remove();
        });

        $('#tbl_benefits tr').on('click',function(){

            id = $(this).data('id');
            name = $(this).data('name');
            username = $(this).data('username');
            password = $(this).data('password');

            $('#id').val(id)
            $('#username').val(username)
            $('#name').val(name)
        });

        $('.onlyNumber').keypress(function (event) {
            return isNumber(event, this)
        });

        $(".onlyNumber").keyup(function(){
            amount  = $(this).val();
            if(amount == 0){
                $(this).val('');
            }else{
                plainAmount = amount.replace(/\,/g,'')
                $(this).val(commaSeparateNumber(plainAmount));
            }
        })

    // ======================================================= //
  // ============ DELETE  FUNCTION ==================== //
// ===================================================== //

$(document).on('click','.delete_item',function(){
    id          = $(this).data('id');
    function_name = $(this).data('function_name')

    if(id){
        swal({
            title: "Delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-warning",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
        }).then(function(isConfirm){
            if(isConfirm.value == true){
                $.ajax({
                    url:base_url+module_prefix+module+'/'+function_name,
                    data:{
                        'id':id,
                        '_token':"{{ csrf_token() }}"
                    },
                    type:'post',
                    dataType:'JSON',
                    success:function(res){
                        swal({
                              title: 'Deleted Successfully!',
                              type: "warning",
                              showCancelButton: false,
                              confirmButtonClass: "btn-warning",
                              confirmButtonText: "OK",
                              closeOnConfirm: false
                        }).then(function(isConfirm){
                            window.location.href = base_url+module_prefix+module;
                        })
                    }

                })
            }else{
                return false;
            }
        });
    }
})

    });

</script>
@endsection




