@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<div>
				<h5 ><b>Search</b></h5>
<!-- 				<table class="table borderless" style="border:none;font-weight: bold">
					<tr class="text-left">
						<td colspan="2"><span>Transaction Period</span></td>
					</tr>
					<tr>
						<td>
							<div class="col-md-6" style="padding-left: 0px;">
								<select class="employee-type form-control font-style2 select2" id="select_month" name="select_month" placeholder="Month">
									<option value=""></option>
								</select>
							</div>
							<div class="col-md-6" style="padding-right: 0px;">
								<select class="employee-type form-control font-style2 select2" id="select_year" name="select_year" placeholder="Year" >
									<option value=""></option>
								</select>
							</div>
						</td>

					</tr>
				</table> -->
				<div >
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
				<div style="height: 5px;"></div>
				<div class="sub-panelnamelist ">
					{!! $controller->show() !!}
				</div>

			</div>
		</div>

		<div class="col-md-9" id="pei">
			<div class="row">
				<div class="col-md-12">
					<label style="font-weight: 600;font-size: 15px;margin-left: 20px;">&nbsp;&nbsp;{{ $title }}</label>
					<div class="sub-panel">
						{!! $controller->showTravelAllowanceDatatable() !!}
					</div>
				</div>
			</div>
			<div class="row" style="padding: 20px;">
				<div class="col-md-12">
					<div class="button-wrapper" style="position: relative;top: -20px;margin-left: 7px;">
						<a class="btn btn-xs btn-info btn-savebg btn_new" id="newEmployeeStatus" data-btnnew="newEmployeeStatus" data-btncancel="cancelEmployeeStatus" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus"><i class="fa fa-save"></i> New</a>

						<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editEmployeeStatus" data-btnnew="newEmployeeStatus" data-btncancel="cancelEmployeeStatus" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus"><i class="fa fa-save"></i> Edit</a>

						<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" data-form="form" data-btnnew="newEmployeeStatus" data-btncancel="cancelEmployeeStatus" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus" id="saveEmployeeStatus"><i class="fa fa-save"></i> Save</a>

						<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newEmployeeStatus" data-btncancel="cancelEmployeeStatus" data-form="myform" data-btnedit="editEmployeeStatus" data-btnsave="saveEmployeeStatus"id="cancelEmployeeStatus"> Cancel</a>
					</div>
						<h5 id="employee_name" style="margin-left: 5px;font-weight: bold"></h5>
						<hr>
						<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="travel_allowance_id" id="travel_allowance_id">
							<input type="hidden" name="employee_id" id="employee_id">
							<input type="hidden" name="rate_amount" id="rate_amount">
							<input type="hidden" name="rate" id="rate">

							<div class="col-md-3">
								<div class="form-group newEmployeeStatus">
									<label> Rate/ Day</label>
									<select id="travel_rate_id" name="travel_rate_id" class="form-control">
										<option value=""></option>
										@foreach($travelrate as $key => $value)
										<option value="{{ $value->id}}" data-rate_amount="{{ $value->rate_amount}}">{{ $value->rate_amount}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group newEmployeeStatus">
									<label> Rate</label>
									<select id="rate_id" name="rate_id" class="form-control">
										<option value=""></option>
										@foreach($rate as $key => $value)
										<option value="{{ $value->id}}" data-rate="{{ $value->rate }}">{{ $value->rate*100 }} %</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group newEmployeeStatus">
									<label>Number of Days</label>
									<input type="text" name="number_of_days" id="number_of_days" class="form-control onlyNumber"  maxlength="2">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
	@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var tblTravelRates = $('#tbl_pei').DataTable();
	var uniform_id;
	number_of_actual_work = 0;
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
		year += '<option value='+y+'>'+y+'</option>';
	}
	$('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);

    $('#inclusive_leave_date').datepicker({
    	dateFormat:'yy-mm-dd'
    });

    var _Year;
    var _Month;
    $(document).on('change','#select_year',function(){
    	_Year = "";
    	_Year = $(this).find(':selected').val();

    });
    $(document).on('change','#select_month',function(){
    	_Month = "";
    	_Month = $(this).find(':selected').val();
    });

    $('.select2').select2();

    $('#wopei').prop('checked','checked').trigger('keyup');

 $(document).on('change','#rate_id',function(){
 	rate = $(this).find(':selected').data('rate');
 	$('#rate').val(rate);

 })
  $(document).on('change','#travel_rate_id',function(){
  	rate_amount = $(this).find(':selected').data('rate_amount');
  	$('#rate_amount').val(rate_amount);
 })

$('.newEmployeeStatus :input').attr('disabled',true);
$('.newEmployeeStatus').attr('disabled',true);
$('.btn_new').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_edit').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_cancel').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');

	$('.'+btnnew+' :input').attr("disabled",true);
	$('.'+btnnew).attr('disabled',true);
	$('#'+btnnew).removeClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).addClass('hidden');
	$('#'+btncancel).addClass('hidden');

	$('#employee_information_id').val('');
	$('#employee_id').val('');

	form = $(this).data('form');
	clear_form_elements('myForm');
	$('.error-msg').remove();
});

    ;

    $('.onlyNumber').keypress(function (event) {
    	return isNumber(event, this)
    });


    var employee_id = '';
    $(document).on('click','#namelist tr',function(){
    	employee_id = $(this).data('empid');
    	employee_name = $(this).data('empname');

    	$('#employee_id').val(employee_id);
    	$('#employee_name').text(employee_name);


		$.ajax({
			url:base_url+module_prefix+module+'/getTravelAllowance',
			data:{
				'employee_id':employee_id,
				'year':_Year,
				'month':_Month,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data);

				tblTravelRates.clear().draw();
				if(data.length !== 0){
    				clear_form_elements('benefits-content');
    				pei_id = '';

    				uniform_amount = 0;
    				cost_uniform_amount = 0
    				rate_amount = 0;
    				rate = 0;

    				$.each(data,function(k,v){

    					rate_amount 		= (v.travelrates) ? v.travelrates.rate_amount : 0;
    					rates 				= (v.rates) ? v.rates.rate : '';
    					rate 				= (v.rates) ? v.rates.rate : '';
    					travel_rate_amount 	= (v.travel_rate_amount) ? v.travel_rate_amount : 0;
    					net_amount 			= (v.net_amount) ? v.net_amount : 0;

    					rates = (rates*100)+'%';

	    				rate_amount = (rate_amount) ? commaSeparateNumber(parseFloat(rate_amount).toFixed(2)) : '';
	    				travel_rate_amount = (travel_rate_amount) ? commaSeparateNumber(parseFloat(travel_rate_amount).toFixed(2)) : '';
	    				net_amount = (net_amount) ? commaSeparateNumber(parseFloat(net_amount).toFixed(2)) : '';

						tblTravelRates.row.add( [
							rate_amount,
							rates,
							travel_rate_amount,
							net_amount,
							'<a class="btn btn-xs btn-danger delete_item" data-function_name="deleteTravelAllowance" data-loan_id="'+v.id+'" data-year="'+v.year+'" data-month="'+v.month+'" data-employee_id="'+v.employee_id+'"><i class="fa fa-trash"></i> Delete</a>'

						]).draw( false );

				        tblTravelRates.rows(k).nodes().to$().attr("data-id", v.id);
				        tblTravelRates.rows(k).nodes().to$().attr("data-employee_id", v.employee_id);
				        tblTravelRates.rows(k).nodes().to$().attr("data-travel_rate_id", v.travel_rate_id);
				        tblTravelRates.rows(k).nodes().to$().attr("data-rate_id", v.rate_id);
				        tblTravelRates.rows(k).nodes().to$().attr("data-number_of_days", v.number_of_days);
				        tblTravelRates.rows(k).nodes().to$().attr("data-btnnew", "newEmployeeStatus");
				        tblTravelRates.rows(k).nodes().to$().attr("data-btnsave", "saveEmployeeStatus");
				        tblTravelRates.rows(k).nodes().to$().attr("data-btnedit", "editEmployeeStatus");
				        tblTravelRates.rows(k).nodes().to$().attr("data-btndelete", "deleteEmployeeStatus");
				        tblTravelRates.rows(k).nodes().to$().attr("data-btncancel", "cancelEmployeeStatus");
    				});

    				$('#rate_amount').val(rate_amount);
    				$('#rate').val(rate);


				}

			}
		});


    })

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
				type: "GET",
				url: base_url+module_prefix+module+'/show',
				data: {
					'q':$('._searchname').val(),
					// 'check_pei':_checkpei
				},
				beforeSend:function(){
				   		// $('.ajax-loader').css("visibility", "visible");

				   	},
				   	success: function(res){
				   		$(".sub-panelnamelist").html(res);

				   	},
				   	complete:function(){
				   		// $('.ajax-loader').css("visibility", "hidden");
				   	}
				 });
		},500);
});

$(document).on('change','#searchby',function(){
	var val = $(this).val();
	console.log(base_url+module_prefix+module)
	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})

			$('#select_searchvalue').html(arr);
		}
	})

});




$(document).on('click','#delete_pei',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Performance Enhancement Incentive?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteUniform();
			}else{
				return false;
			}
		});
	}
})

$.deleteUniform = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteUniform',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		success:function(response){
			par = JSON.parse(response)

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})

				_listId = [];
				$('.btnfilter').trigger('click');
			}
		}
	});
}


var timer;
$(document).on('click','.btnfilter',function(){
	year 	  = $('#select_year :selected').val();
	month 	  = $('#select_month :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {'year':_Year,'month':_Month,'checkpei':_checkpei },
			   beforeSend:function(){
			   		$('#loading').removeClass('hidden');
			   },
			   complete:function(){
			   		$('#loading').addClass('hidden');
			   },
			   success: function(res){
			   	// console.log(res);
			      $(".sub-panelnamelist").html(res);
			   }
			});
		},500);
});


$(document).off('click',".submitme").on('click',".submitme",function(){
		btn = $(this);
alert();
		$("#form").ajaxForm({
			beforeSend:function(){

			},
			success:function(data){
				par  =  JSON.parse(data);
				if(par.status){

					swal({  title: par.response,
							text: '',
							type: "success",
							icon: 'success',

						}).then(function(){

							window.location.href = base_url+module_prefix+module;
							// clear_form_elements('myform')

						});

				}else{

					swal({  title: par.response,
							text: '',
							type: "error",
							icon: 'error',

						});

				}

				btn.button('reset');
			},
			error:function(data){
				$error = data.responseJSON;
				/*reset popover*/
				$('input[type="text"], select').popover('destroy');

				/*add popover*/
				block = 0;
				$(".error-msg").remove();
				$.each($error,function(k,v){
					var messages = v.join(', ');
					msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
					$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
					if(block == 0){
						$('html, body').animate({
					        scrollTop: $('.err-'+k).offset().top - 250
					    }, 500);
					    block++;
					}
				})
				$('.saving').replaceWith(btn);
			},
			always:function(){
				setTimeout(function(){
						$('.saving').replaceWith(btn);
					},300)
			}
		}).submit();

});


// ======================================================= //
// ============ DELETE  FUNCTION ==================== //
// ===================================================== //

$(document).on('click','.delete_item',function(){
	loan_id 	= $(this).data('loan_id');
	year 		= $(this).data('year');
	month 		= $(this).data('month');
	employee_id = $(this).data('employee_id');
	function_name = $(this).data('function_name')

	if(loan_id){
		swal({
			title: "Delete?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.ajax({
					url:base_url+module_prefix+module+'/'+function_name,
					data:{
						'id':loan_id,
						'year':year,
						'month':month,
						'employee_id':employee_id,
						'_token':"{{ csrf_token() }}"
					},
					type:'post',
					dataType:'JSON',
					success:function(res){
						swal({
							  title: 'Deleted Successfully!',
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-warning",
							  confirmButtonText: "OK",
							  closeOnConfirm: false
						})

						window.location.href = base_url+module_prefix+module;

						// switch(res.status){
						// 	case 'loans':
						// 		LoanInfo(res.data)
						// 		$('#input_actualloan').val(_totalLoans).trigger('keyup');
						// 	break;
						// 	case 'deductions':
						// 		DeductionInfo(res.data)
						// 		$('#input_actualotherdeduct').val(_totalOtherDeductions).trigger('keyup');
						// 	break;
						// 	case 'benefits':
						// 		BenefitInfo(res.data)
						// 	break;
						// }
					}

				})
			}else{
				return false;
			}
		});
	}
})

});


</script>
@endsection