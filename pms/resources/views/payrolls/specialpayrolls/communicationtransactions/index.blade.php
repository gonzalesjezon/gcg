@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<div>
				<h5 ><b>Filter Employee By</b></h5>
				<table class="table borderless" style="border:none;font-weight: bold">
					<tr class="text-left">
						<td colspan="2"><span>Transaction Period</span></td>
					</tr>
					<tr>
						<td>
							@include('payrolls.includes._months-year')
						</td>
					</tr>
				</table>
				<div class="col-md-12 text-right">
					<button class="btn btn-xs btn-info" id="btn_process_cea" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_cea" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>
				</div>
				<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
				<div class="search-btn">
					<div class="col-md-4">
						<span>Search</span>
					</div>
					<div class="col-md-8">
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_cea" id="wcea" value="wcea">
							With
						</label>
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_cea" id="wocea" value="wocea">
							W/Out
						</label>
					</div>
				</div>
				<div >
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
				<div style="height: 5px;"></div>
				<div class="sub-panelnamelist ">
					{!! $controller->show() !!}
				</div>

			</div>
		</div>

		<div class="col-md-9" id="cea">
			<label style="font-weight: 600;font-size: 15px;">&nbsp;&nbsp;{{ $title }}</label>
			<div class="sub-panel">
				{!! $controller->showCeaDatatable() !!}
			</div>
			<div class="benefits-content style-box2 hidden" id="addRemarks">
				<div class="col-md-5">
					<form action="{{ url($module_prefix.'/'.$module)}}" method="post" id="form" onsubmit="return false">
						<input type="hidden" name="_token" value="{{ csrf_token()}}">
						<div class="box-1 button-style-wrapper" style="top: -10px !important;">
							<!-- <a class="btn btn-xs btn-info btn-savebg btn_new"><i class="fa fa-save"></i> New</a> -->
							<a class="btn btn-xs btn-info btn-savebg submit btn_save hidden"><i class="fa fa-save"></i> Save</a>
							<a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
							<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
						</div>

						<div class="panel" style="padding: 20px;font-size: 12px;">
							<label>COMMUNICATION EXPENSE DIFFERENTIAL</label>
							<table class="table borderless" style="border:none;">
								<tbody style="border-top: none;">
									<tr>
										<td>
											<span>ADD:</span>
										</td>
										<td>
											<input type="text" name="add_amount" id="add_amount" class="form-control font-style2 onlyNumber">
										</td>
									</tr>
									<tr>
										<td>
											<span>LESS:</span>
										</td>
										<td>
											<input type="text" name="less_amount" id="less_amount" class="form-control font-style2 onlyNumber">
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="panel" style="padding: 20px;font-size: 12px;">
							<span>Remarks</span>
							<div class="form-group" >
								<textarea name="remarks" id="remarks" rows="4" cols="50"></textarea>
							</div>
						</div>


						<input type="hidden" name="id" id="cea_id">
					</form>
				</div>
				<div class="col-md-6">
				</div>
			</div>
		</div>


		</div>
	</div>
	@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var tblCea = $('#tbl_cea').DataTable();
	number_of_actual_work = 0;
	var _Year;
    var _Month;
    $(document).on('change','#select_year',function(){
    	_Year = "";
    	_Year = $(this).find(':selected').val();

    });
    $(document).on('change','#select_month',function(){
    	_Month = "";
    	_Month = $(this).find(':selected').val();
    });

    $('#select_year').trigger('change');
    $('#select_month').trigger('change');

    $('#inclusive_leave_date').datepicker({
    	dateFormat:'yy-mm-dd'
    });

    $('.select2').select2();

    $('#wocea').prop('checked',false);

    $('.benefits-content :input').attr("disabled",true);
    $('.btn_new').on('click',function(){
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_new').addClass('hidden');
    	$('.btn_edit').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_edit').on('click',function(){
    	// $('.leave-field').attr("disabled",false);
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_edit').addClass('hidden');
    	$('.btn_new').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_cancel').on('click',function(){
    	$('.benefits-content :input').attr("disabled",true);
    	// $('.btn_new').removeClass('hidden');
    	$('.btn_save').addClass('hidden');
    	$('.btn_edit').removeClass('hidden');

    	myform = "myform";
    	clear_form_elements(myform);
    	clear_form_elements('benefits-content');
    	$('#tbl_leave').html('');
    	$('.error-msg').remove();
    	$('#for_update').val('');
    });

    ;

    $('.onlyNumber').keypress(function (event) {
    	return isNumber(event, this)
    });


    var _listId = [];
    $(document).on('click','#check_all',function(){

    	if(!_Year && !_Month){
    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('#check_all').prop('checked',false);

    	}else{
    		if ($(this).is(':checked')) {
    			$('.emp_select').prop('checked', 'checked');

    			$('.emp_select:checked').each(function(){
    				_listId.push($(this).val())
    			});

    		} else {
    			$('.emp_select').prop('checked', false)
    			_listId = [];
    		}

    	}

    });

    _checkcea = ""
    $('input[type=radio][name=chk_cea]').change(function() {

    	if(!_Year){
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "OK",
    			closeOnConfirm: false
    		})

    		$(this).prop('checked',false);

    	}else{

    		if (this.value == 'wcea') {
    			$('#btn_process_cea').prop('disabled',true);
    			_checkcea = 'wcea';
    			$('#addRemarks').removeClass('hidden');

    		}
    		else if (this.value == 'wocea') {
    			$('#btn_process_cea').prop('disabled',false);
    			_checkcea = 'wocea';
    			$('#addRemarks').addClass('hidden');

    		}
    		$('.btnfilter').trigger('click');

    	}

    });


    $(document).on('click','.emp_select',function(){
    	empid = $(this).val();
    	index = $(this).data('key');
    	if(!_Year && !_Month){

    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('.emp_select').prop('checked',false);

    	}else{

    		if($(this).is(':checked')){
    			_listId[index] =  empid;

    		}else{
    			delete _listId[index];
    		}

    	}
    });

    var cea_id;
    var employee_id;
    $(document).on('click','#namelist tr',function(){
    	employee_id = $(this).data('empid');

    	if(_Year && _Month){

    		$.ajax({
    			url:base_url+module_prefix+module+'/getCeaInfo',
    			data:{
    				'employee_id':employee_id,
    				'year':_Year,
    				'month':_Month,
    			},
    			type:'GET',
    			dataType:'JSON',
    			success:function(data){

    				clear_form_elements('benefits-content');
    				cea_id = '';

    				var	cea_amount = 0;
    				var cea_total = 0;
    				var office = '';
    				var position = '';

    				if(data){
    					$('#cea_id').val(data.id);
						tblCea.clear().draw();
    					office = data.offices.Name;
    					position = (data.positions !== null) ? data.positions.Name : '';
    					cea_amount = (data.amount) ? data.amount : 0;
    					remarks = (data.special_remarks) ? data.special_remarks : '';
    					add_amount = (data.add_amount) ? data.add_amount : 0;
    					less_amount = (data.less_amount) ? data.less_amount : 0;

    					$('#remarks').val(remarks);
    					$('#add_amount').val(add_amount);
    					$('#less_amount').val(less_amount);

	    				cea_total = (cea_amount) ? commaSeparateNumber(parseFloat(cea_amount).toFixed(2)) : '';
						tblCea.row.add( [
							position,
							office,
							cea_total

						]).draw( false );
    				}
				}
			});
    	}else{
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});
    	}

    })

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
				type: "GET",
				url: base_url+module_prefix+module+'/show',
				data: {
					'q':$('._searchname').val(),
					'check_cea':_checkcea,
					'_year':_Year,
					'_month':_Month
				},
				beforeSend:function(){
				   		// $('.ajax-loader').css("visibility", "visible");

				   	},
				   	success: function(res){
				   		$(".sub-panelnamelist").html(res);

				   	},
				   	complete:function(){
				   		// $('.ajax-loader').css("visibility", "hidden");
				   	}
				 });
		},500);
});

$(document).on('change','#searchby',function(){
	var val = $(this).val();
	console.log(base_url+module_prefix+module)
	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})

			$('#select_searchvalue').html(arr);
		}
	})

});

$(document).on('click','#btn_process_cea',function(){
	console.log(_listId);
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Communication Expense?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processCea();
			}else{
				return false;
			}
		});
	}

});

$.processCea = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/processCea',
		data:{
			'_token':'{{ csrf_token() }}',
			'list_id':_listId,
			'year':_Year,
			'month':_Month,
			'cea_id':cea_id,
			'employee_id':employee_id
		},
		type:'POST',
		beforeSend:function(){
        	$('#btn_process_cea').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
		success:function(data){
			par = JSON.parse(data);
			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
					// window.location.href = base_url+module_prefix+module;
					$('#btn_process_cea').html('<i class="fa fa-save"></i> Process').prop('disabled',true);
					_checkcea = 'wocea';
					_listId = [];
					$('.btnfilter').trigger('click');
				}else{

				}
			}
	});
}

$(document).on('click','#btn_save_cea',function(){

	if(no_of_actual_work == null){
		swal("No of field is empty!", "", "warning");
	}else{
		$.saveCea();
	}

});

$(document).on('click','#delete_cea',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Communication Expense?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteCea();
			}else{
				return false;
			}
		});
	}
})

$.deleteCea = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteCea',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
			$('#delete_cea').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response)

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})

				_listId = [];
				$('#delete_cea').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
				$('.btnfilter').trigger('click');
			}
		}
	});
}


var timer;
$(document).on('click','.btnfilter',function(){
	year 	  = $('#select_year :selected').val();
	month 	  = $('#select_month :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {'year':_Year,'month':_Month,'checkcea':_checkcea },
			   beforeSend:function(){
			   		$('#loading').removeClass('hidden');
			   },
			   complete:function(){
			   		$('#loading').addClass('hidden');
			   },
			   success: function(res){
			   	// console.log(res);
			      $(".sub-panelnamelist").html(res);
			   }
			});
		},500);
});



});


</script>
@endsection