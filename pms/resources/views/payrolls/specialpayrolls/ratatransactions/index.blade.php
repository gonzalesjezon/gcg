@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<div>
				<h5 ><b>Filter Employee By</b></h5>
				<table class="table borderless" style="border:none;font-weight: bold">
					<tr class="text-left">
						<td colspan="2"><span>Transaction Period</span></td>
					</tr>
					<tr>
						<td>
							@include('payrolls.includes._months-year')
						</td>
					</tr>
				</table>
				<div class="col-md-12 text-right">
					<button class="btn btn-xs btn-info" id="btn_process_rata" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_rata" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>
				</div>
				<div class="search-btn">
					<div class="col-md-4">
						<span>Search</span>
					</div>
					<div class="col-md-8">
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_wrata" id="wrata" value="wrata">
							With
						</label>
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_wrata" id="worata" value="worata">
							W/Out
						</label>
					</div>
				</div>
				<div >
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
				<div style="height: 5px;"></div>
				<div class="sub-panelnamelist ">
					{!! $controller->show() !!}
				</div>

			</div>
		</div>

		<div class="col-md-9" id="rata">
			<label style="font-weight: 600;font-size: 15px;">&nbsp;&nbsp;RATA</label>
			<div class="row" style="margin-left: 15px;margin-right: 15px;">
				<div class="col-md-12">
					<div class="sub-panel" style="z-index: 1;">
						{!! $controller->showRataDatatable() !!}
					</div>
				</div>
			</div>
			<div class="row" style="margin-left: 15px;margin-right: 15px;">
				<div class="col-md-12">
					<div class="benefits-content style-box2 hidden" id="formAddRata">
						<div class="form-group">
							<div class="box-1 button-style-wrapper">
								<!-- <a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savebanks"><i class="fa fa-save"></i> New</a> -->
								<a class="btn btn-xs btn-info btn-savebg btn_save hidden" id="btn_save_rata"><i class="fa fa-save"></i> Save</a>
								<a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
								<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-6"></div>
							<div class="col-md-5 text-right">
								<input type="checkbox" name="hold" id="hold">
								<span style="font-size:12px;">Hold</span>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-6">
								<div class="panel" style="padding: 20px;font-size: 12px;">
									<!-- <label>&nbsp;</label> -->
									<label id="employee_name"></label>
									<table class="table borderless" style="border:none;">
										<tbody style="border-top: none;">
											<tr>
												<td>
													<span>No. of Actual Work Days</span>
												</td>
												<td>
													<input type="text" name="no_of_actual_work" id="no_of_actual_work" class="form-control font-style2">
												</td>
											</tr>
											<tr>
												<td>
													<span>Rata Percentage</span>
												</td>
												<td>
													<input type="text" name="rata_percentage" id="rata_percentage" class="form-control font-style2" readonly>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="panel" style="padding: 20px;font-size: 12px">
									<span>Leave Filed</span>
									<div class="form-group" >
										<textarea id="number_of_leave" rows="4" cols="50"></textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel" style="padding: 20px;font-size: 12px;">
									<label>RATA DIFFERENTIAL</label>
									<table class="table borderless" style="border:none;">
										<tbody style="border-top: none;">
											<tr>
												<td>
													<span>Representation Allowance</span>
												</td>
												<td>
													<input type="text" name="ra_diff_amount" id="ra_diff_amount" class="form-control font-style2 onlyNumber">
												</td>
											</tr>
											<tr>
												<td>
													<span>Transportation Allowance</span>
												</td>
												<td>
													<input type="text" name="ta_diff_amount" id="ta_diff_amount" class="form-control font-style2 onlyNumber">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="panel" style="padding: 20px;font-size: 12px;">
									<span>Remarks</span>
									<div class="form-group" >
										<textarea id="remarks" rows="4" cols="50"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div style="height: 50px;"></div>
		</div>
	</div>
</div>
	@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var tblRata = $('#tbl_rata').DataTable();
	var tblEoam = $('#tbl_eoam').DataTable();
	var tblCead = $('#tbl_cead').DataTable();
	var tblPei = $('#tbl_pe').DataTable();
	var tblCashGift = $('#tbl_cashgift').DataTable();
	number_of_actual_work = 0;

    $('#inclusive_leave_date').datepicker({
    	dateFormat:'yy-mm-dd'
    });

	var _bool = false;
	var _Year;
	var _Month;
	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();
		if(_bool == true){
			$('.btnfilter').trigger('click');
		}

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
		if(_bool == true){
			$('.btnfilter').trigger('click');
		}
	})

	$('#select_year').trigger('change');
    $('#select_month').trigger('change');

    $('.select2').select2();

    $('#worata').prop('checked',false);

    $('.benefits-content :input').attr("disabled",true);
    $('.btn_new').on('click',function(){
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_new').addClass('hidden');
    	$('.btn_edit').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_edit').on('click',function(){
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_edit').addClass('hidden');
    	$('.btn_new').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_cancel').on('click',function(){
    	$('.benefits-content :input').attr("disabled",true);
    	// $('.btn_new').removeClass('hidden');
    	$('.btn_save').addClass('hidden');
    	$('.btn_edit').removeClass('hidden');

    	myform = "myform";
    	clear_form_elements(myform);
    	clear_form_elements('benefits-content');
    	$('#tbl_leave').html('');
    	$('.error-msg').remove();
    	$('#for_update').val('');
    });

    var raDiffAmount = 0;
    var taDiffAmount = 0;
    $('#ra_diff_amount').keyup(function(){
    	raDiffAmount = $(this).val().replace(/\,/g,'');
    });

    $('#ta_diff_amount').keyup(function(){
    	taDiffAmount = $(this).val().replace(/\,/g,'');
    });

    var _Hold = 0;
    $(document).on('change','#hold',function(){
    	if($(this).is(':checked')){
			_Hold = 1;
		}else{
			_Hold = 0;
		}
    });

    $('.onlyNumber').keypress(function (event) {
    	return isNumber(event, this)
    });

    $(".onlyNumber").keyup(function(){
		amount  = $(this).val();
		if(amount == 0){
			$(this).val('');
		}else{
			plainAmount = amount.replace(/\,/g,'')
			$(this).val(commaSeparateNumber(plainAmount));
		}
	});


    var _listId = [];
    $(document).on('click','#check_all',function(){

    	if(!_Year && !_Month){
    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('#check_all').prop('checked',false);

    	}else{
    		if ($(this).is(':checked')) {
    			$('.emp_select').prop('checked', 'checked');

    			$('.emp_select:checked').each(function(){
    				_listId.push($(this).val())
    			});

    		} else {
    			$('.emp_select').prop('checked', false)
    			_listId = [];
    		}

    	}

    });

    var percentage;
    var repAmount = 0;
	var transpoAmount = 0;
	var prodRaAmount = 0;
	var prodTaAmount = 0;
    $(document).on('keyup','#no_of_actual_work',function(){
    	rataAmount = 0;
    	num = 0;
    	num = $(this).val();
    	$('#rata_percentage').val('');

    	if(num >= 17){
    		$('#rata_percentage').val('100 %');
    		percentage = 1.00;
    	}else if(num <= 16 && num >= 12){
    		$('#rata_percentage').val('75 %');
    		percentage = .75;
    	}else if(num <= 11 && num >= 6){
    		$('#rata_percentage').val('50 %');
    		percentage = .50;
    	}else if(num <= 5 && num >= 1){
    		$('#rata_percentage').val('25 %');
    		percentage = .25;
    	}

    	prodRaAmount = parseFloat(repAmount) * percentage;
    	prodTaAmount = parseFloat(transpoAmount) * percentage;

    });

    _checkrata = ""
    $('input[type=radio][name=chk_wrata]').change(function() {

    	if(!_Year){
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "OK",
    			closeOnConfirm: false
    		})

    		$(this).prop('checked',false);

    	}else{


    		if (this.value == 'wrata') {
    			$('#btn_process_rata').prop('disabled',true);
    			// $('#formAddDeduction').removeClass('hidden');
    			$('#formAddRata').removeClass('hidden');
    			_bool = true;
    			_checkrata = 'wrata';

    		}
    		else if (this.value == 'worata') {
    			$('#btn_process_rata').prop('disabled',false);
    			// $('#formAddDeduction').addClass('hidden');
    			$('#formAddRata').addClass('hidden');
    			_checkrata = 'worata';

    		}
    		$('._searchname').trigger('keyup');

    	}

    });


    $(document).on('click','.emp_select',function(){
    	empid = $(this).val();
    	index = $(this).data('key');
    	if(!_Year && !_Month){

    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('.emp_select').prop('checked',false);

    	}else{

    		if($(this).is(':checked')){
    			_listId[index] =  empid;

    		}else{
    			delete _listId[index];
    		}

    	}

    });

    var representation_amount = 0;
    var transportation_amount = 0;
    var rata_id;
    var employee_id;
    $(document).on('click','#namelist tr',function(){
    	employee_id = $(this).data('empid');
    	firstname = $(this).data('firstname');
    	lastname = $(this).data('lastname');
    	middlename = $(this).data('middlename');

    	$('#employee_name').text('');

    	lastname = (lastname) ? lastname : '';
    	firstname = (firstname) ? firstname : '';
    	middlename = (middlename) ? middlename : '';

    	fullname = lastname+' '+firstname+' '+middlename
    	fullname = (fullname) ? fullname : '';
    	$('#employee_name').text(fullname)

    	// if(_Year && _Month){

    		$.ajax({
    			url:base_url+module_prefix+module+'/getRataInfo',
    			data:{
    				'id':employee_id,
    				'year':_Year,
    				'month':_Month,
    			},
    			type:'GET',
    			dataType:'JSON',
    			success:function(data){

    				clear_form_elements('benefits-content');
    				tblRata.clear().draw();

    				rata_id = '';
    				if(data.rata !== null){

	    				// $.each(data.rata,function(k,v){
	    					rata_id = data.rata.id;
		    				percentage = (data.rata.percentage_of_rata) ? data.rata.percentage_of_rata : '';
		    				numberActualWork = (data.rata.number_of_actual_work) ? data.rata.number_of_actual_work : '';
		    				ra_dff = (data.rata.ra_diff_amount) ? data.rata.ra_diff_amount : '';
		    				ta_dff = (data.rata.ta_diff_amount) ? data.rata.ta_diff_amount : '';
		    				numberLeaveFiled = (data.rata.number_of_leave_filed) ? data.rata.number_of_leave_filed : '';
		    				remarks = (data.rata.special_remarks) ? data.rata.special_remarks : '';
		    				_Hold = (data.rata.hold) ? data.rata.hold : 0;

		    				$('#no_of_actual_work').val(numberActualWork).trigger('keyup');
		    				$('#ra_diff_amount').val(ra_dff).trigger('keyup');
		    				$('#ta_diff_amount').val(ta_dff).trigger('keyup');
		    				$('#number_of_leave').val(numberLeaveFiled).trigger('keyup');
		    				$('#remarks').val(remarks).trigger('keyup');

		    				if(_Hold){
		    					$('#hold').prop('checked',true);
		    				}else{
		    					$('#hold').prop('checked',false);
		    				}

							// <!-- GENERATE RATA TABLE --!>

							prodRaAmount = (data.rata.representation_amount) ? data.rata.representation_amount : 0;
							prodTaAmount = (data.rata.transportation_amount) ? data.rata.transportation_amount : 0;
							totalRata = (parseFloat(prodRaAmount) + parseFloat(prodTaAmount));

							representation_amount = (prodRaAmount) ? commaSeparateNumber(parseFloat(prodRaAmount).toFixed(2)) : '';
							transportation_amount = (prodTaAmount) ? commaSeparateNumber(parseFloat(prodTaAmount).toFixed(2)) : '';
							total_rata = (totalRata) ? commaSeparateNumber(parseFloat(totalRata).toFixed(2)) : '';

							tblRata.row.add( [
								representation_amount,
								transportation_amount,
								numberLeaveFiled,
								numberActualWork,
								percentage,
								representation_amount,
								transportation_amount,
								total_rata

								]).draw( false );

							tblRata.rows(0).nodes().to$().attr("data-id", data.rata.id);
					        tblRata.rows(0).nodes().to$().attr("data-employee_id", data.rata.employee_id);

	    				// });

    				}

					if(data.rata_setup.length !== 0){

						$.each(data.rata_setup,function(k,v){
							switch(v.benefits.code){
								case 'REP':
									repAmount = (v.benefit_amount) ? v.benefit_amount : 0;
									break;
								case 'TRANSPO':
									transpoAmount = (v.benefit_amount) ? v.benefit_amount : 0;
									break;
							}
						})

					}

				}
			});
    	// }else{
    	// 	swal({
    	// 		title: 'Select year and month first',
    	// 		type: "warning",
    	// 		showCancelButton: false,
    	// 		confirmButtonClass: "btn-warning",
    	// 		confirmButtonText: "Yes",
    	// 		closeOnConfirm: false

    	// 	});
    	// }

    })

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
				type: "GET",
				url: base_url+module_prefix+module+'/show',
				data: {
					'q':$('._searchname').val(),
					'limit':$(".limit").val(),
					'check_rata':_checkrata,
					'_year':_Year,
					'_month':_Month,
				},
				beforeSend:function(){
				   		// $('.ajax-loader').css("visibility", "visible");

				   	},
				   	success: function(res){
				   		$(".sub-panelnamelist").html(res);

				   	},
				   	complete:function(){
				   		// $('.ajax-loader').css("visibility", "hidden");
				   	}
				 });
		},500);
});

$(document).on('change','#searchby',function(){
	var val = $(this).val();
	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})

			$('#select_searchvalue').html(arr);
		}
	})

});

$(document).on('click','#btn_process_rata',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Rata?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processRata();
			}else{
				return false;
			}
		});
	}

});

$.processRata = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/processRata',
		data:{
			'_token':'{{ csrf_token() }}',
			'list_id':_listId,
			'year':_Year,
			'month':_Month,
			'rata_id':rata_id,
			'employee_id':employee_id
		},
		type:'POST',
		beforeSend:function(){
        	$('#btn_process_rata').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
		success:function(data){
			par = JSON.parse(data);
			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
					// window.location.href = base_url+module_prefix+module;
					$('#btn_process_rata').html('<i class="fa fa-save"></i> Process').prop('disabled',true);
					_listId = [];
					$('._searchname').trigger('keyup');
				}else{

				}
			}
	});
}

$(document).on('click','#btn_save_rata',function(){

	if(no_of_actual_work == null){
		swal("No of field is empty!", "", "warning");
	}else{
		$.saveRata();
	}

});

$.saveRata = function(){
	$.ajax({
		type:'POST',
		url:base_url+module_prefix+module+'/storeRata',
		data:{
			'_token':'{{ csrf_token() }}',
			'employee_id':employee_id,
			'rata_id':rata_id,
			'no_of_actual_work':$('#no_of_actual_work').val(),
			'percentage_of_rata':$('#rata_percentage').val(),
			'number_of_leave':$('#number_of_leave').val(),
			'remarks':$('#remarks').val(),
			'representation_amount':prodRaAmount,
			'transportation_amount':prodTaAmount,
			'ra_diff_amount':raDiffAmount,
			'ta_diff_amount':taDiffAmount,
			'hold':_Hold,
			'year':_Year,
			'month':_Month

		},
		success:function(data){
			par = JSON.parse(data);

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				}).then(function(){

					window.location.href = base_url+module_prefix+module;
				});


			}else{
				swal({
					title: par.response,
					type: "warning",
					showCancelButton: false,
					confirmButtonClass: "btn-warning",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})
			}
		},error:function(data){
				$error = data.responseJSON;
				/*reset popover*/
				$('input[type="text"], select').popover('destroy');

				/*add popover*/
				block = 0;
				$(".error-msg").remove();
				$.each($error.errors,function(k,v){
					var messages = v.join(', ');
					msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
					$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
					if(block == 0){
						$('html, body').animate({
					        scrollTop: $('.err-'+k).offset().top - 250
					    }, 500);
					    block++;
					}
				})
				$('.saving').replaceWith(btn);
			}
	});
}

$(document).on('click','#delete_rata',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Rata?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteRata();
				tblRata.clear().draw();
			}else{
				return false;
			}
		});
	}
})

$.deleteRata = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteRata',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
			$('#delete_rata').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response)

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})

				$('#delete_rata').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
				_listId = [];
				$('._searchname').trigger('keyup');
			}
		}
	});
}
var btn;
/*serialize All form ON SUBMIT*/
$(document).off('click',".submitme").on('click',".submitme",function(){
	btn = $(this);

	$("#form").ajaxForm({
		beforeSend:function(){

		},
		success:function(data){
			par  =  JSON.parse(data);
			if(par.status){

				swal({  title: par.response,
						text: '',
						type: "success",
						icon: 'success',

					}).then(function(){

						window.location.href = base_url+module_prefix+module;
						// clear_form_elements('myform')

					});

			}else{

				swal({  title: par.response,
						text: '',
						type: "error",
						icon: 'error',

					});

			}

			btn.button('reset');
		},
		error:function(data){
			$error = data.responseJSON;
			/*reset popover*/
			$('input[type="text"], select').popover('destroy');

			/*add popover*/
			block = 0;
			$(".error-msg").remove();
			$.each($error.errors,function(k,v){
				var messages = v.join(', ');
				msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
				$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
				if(block == 0){
					$('html, body').animate({
				        scrollTop: $('.err-'+k).offset().top - 250
				    }, 500);
				    block++;
				}
			})
			$('.saving').replaceWith(btn);
		},
		always:function(){
			setTimeout(function(){
					$('.saving').replaceWith(btn);
				},300)
		}
	}).submit();

});

});


</script>
@endsection