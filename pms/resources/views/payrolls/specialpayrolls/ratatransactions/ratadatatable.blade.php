
<table class="table table-responsive datatable" id="tbl_rata" >
	<thead>
		<tr >
			<th rowspan="2"  style="vertical-align: middle;">Representation</th>
			<th rowspan="2"  style="vertical-align: middle;">Transportation</th>
			<th rowspan="2"  style="vertical-align: middle;">Leave Filed for previous Month</th>
			<th rowspan="2"  style="vertical-align: middle;">Actual Work for previous Month</th>
			<th rowspan="2"  style="vertical-align: middle;">Actual Rate Percentage</th>
			<th colspan="3" style="vertical-align: middle;">NET</th>
		</tr>
		<tr>
			<th>RA</th>
			<th>TA</th>
			<th>TOTAL</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_rata').DataTable({
	 	'dom':'<lf<t>pi>',
	 	'responsive': true,
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_rata tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        employee_id = $(this).data('employee_id');
	        $('#employee_id').val(employee_id);

	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );


})
</script>
