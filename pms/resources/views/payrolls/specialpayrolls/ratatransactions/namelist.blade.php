<table class="table scroll datatable" id="namelist">
	<thead>
		<tr>
			<th><input type="checkbox" name="check_all" id="check_all"></th>
			<th>Name</th>
		</tr>
	</thead>
	<tbody >
		@foreach($data as $key => $value)
			<tr data-empid="{{ $value->id }}"  data-firstname="{{ $value->firstname }}" data-lastname="{{ $value->lastname }}" data-middlename="{{ $value->middlename }}">
				<td>
					<input type="checkbox" name="checked_emp_id[]" value="{{ $value->id  }}" data-employee_number="{{ $value->employee_number  }}" class="emp_select" data-key="{{ $key }}" >
					<span style="padding-left: 10px;position: relative;">{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</span>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){


});
</script>