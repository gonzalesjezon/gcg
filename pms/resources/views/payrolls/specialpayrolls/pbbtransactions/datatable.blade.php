<div class="col-md-12">
	<div class="panel" style="padding: 10px;">
		<table class="table table-striped table-responsive datatable" id="tbl_pbb" style="border: none;">
			<thead>
				<tr >
					<th>Position</th>
					<th>Office</th>
					<th>PBB Amount</th>
					<th>Tax Amount</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody class="text-right">
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_pbb').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_pbb tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];

	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

	$('#tbl_pbb tr').on('click',function(){
		// var id = 0;
		// id = $(this).data('id');
		// $.ajax({
		// 	url:base_url+module_prefix+module+'/getItem',
		// 	data:{'id':id},
		// 	type:'GET',
		// 	dataType:'JSON',
		// 	success:function(data){

		// 		$('#code').val(data.code);
		// 		$('#name').val(data.name);
		// 		$('#tax_type').val(data.tax_type);
		// 		$('#amount').val(data.amount);
		// 		$('#payroll_group').val(data.payroll_group);
		// 		$('#itr_classification').val(data.itr_classification);
		// 		$('#alphalist_classification').val(data.alphalist_classification);
		// 		$('#for_update').val(data.id);
		// 	}
		// })
	});

})
</script>
