@extends('app-front')

@section('content')
<div style="margin-top: 50px;">
<div class="col-md-3">
	<h5 ><b>Filter Employee By</b></h5>
	<table class="table borderless" style="border:none;font-weight: bold">
		<tr>
			<td>
				<div class="row form-group">
					<span>Transaction Year</span>
					<select class="employee-type form-control font-style2 select2" id="select_year" name="year" placeholder="Month">
						@foreach( range($latest_year,$earliest_year) as $i)
						<option value="{{$i}}">{{$i}}</option>
						@endforeach
					</select>
				</div>

				<div class="row form-group">
					<span>Filtered By</span>
					<select class="form-control select2" name="office_id" id="filter_by_office">
						@foreach($offices as $key => $office)
						<option value="{{$office->RefId}}">{{$office->Name}}</option>
						@endforeach
					</select>
				</div>

				<div class="row form-group">
					<span>Rate</span>
					<input type="text" name="rate" id="pbb_rate" class="form-control" placeholder="Input rate">
				</div>

				<div class="row form-group text-right">
					<button class="btn btn-xs btn-info" id="btn_process_pbb" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_pbb" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>
				</div>

				<div class="row form-group text-right">
					<label class="radiobut-style radio-inline ">
						<input type="radio" name="chk_pbb" id="wpbb" value="wpbb">
						<span  style="font-size:12px;">With</span>
					</label>
					<label class="radiobut-style radio-inline ">
						<input type="radio" name="chk_pbb" id="wopbb" value="wopbb">
						<span style="font-size:12px;">W/Out</span>
					</label>
				</div>

				<div class="row form-group">
					<input type="text" name="filter_search" class="form-control _searchname" placeholder="Search here">
				</div>

				<div class="row form-group">
					<div class="panel-namelist">
						{!! $controller->show() !!}
					</div>
				</div>
			</td>
		</tr>
	</table>

	<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
</div>


<div class="col-md-9" id="pbb">
	<div class="row" style="margin:0 15px 0 15px;">
		<label style="font-weight: 600;font-size: 15px;">&nbsp;&nbsp;{{ $title }}</label>
		<div class="sub-panel">
			{!! $controller->showpbbDatatable() !!}
		</div>
	</div>
</div>

</div>
	@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){

	var tblPbb = $('#tbl_pbb').DataTable();

    var _Year;
    var _Month;
    $('#select_year').on('change',function(){
    	_Year = "";
    	_Year = $(this).find(':selected').val();
    });
    $('#select_year').trigger('change');

    $('.select2').select2();

    $('#wopbb').prop('checked',false);

    $('.benefits-content :input').attr("disabled",true);
   $('.btn_new').on('click',function(){
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_new').addClass('hidden');
    	$('.btn_edit').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_edit').on('click',function(){
    	$('.leave-field').attr("disabled",false);
    	$('.btn_edit').addClass('hidden');
    	$('.btn_new').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_cancel').on('click',function(){
    	$('.benefits-content :input').attr("disabled",true);
    	$('.btn_new').removeClass('hidden');
    	$('.btn_save').addClass('hidden');
    	$('.btn_edit').removeClass('hidden');

    	myform = "myform";
    	clear_form_elements(myform);
    	clear_form_elements('benefits-content');
    	$('#tbl_leave').html('');
    	$('.error-msg').remove();
    	$('#for_update').val('');
    });


    var _listId = [];
    $(document).on('click','#check_all',function(){

    	if(!_Year){
    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('#check_all').prop('checked',false);

    	}else{
    		if ($(this).is(':checked')) {
    			$('.emp_select').prop('checked', 'checked');

    			$('.emp_select:checked').each(function(){
    				_listId.push($(this).val())
    			});

    		} else {
    			$('.emp_select').prop('checked', false)
    			_listId = [];
    		}

    	}

    });
    var officeId;
    $('#filter_by_office').on('change',function(){
    	officeId = $(this).find(':selected').val();
    	$('.btnfilter').trigger('click');
    })

    $('#filter_by_office').trigger('change');

    var pbbRate;
    $('#pbb_rate').on('keyup',function(){
    	pbbRate = $(this).val();
    })

    _checkpbb = ""
    $('input[type=radio][name=chk_pbb]').change(function() {

    	if(!_Year){
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "OK",
    			closeOnConfirm: false
    		})

    		$(this).prop('checked',false);

    	}else{

    		if (this.value == 'wpbb') {
    			$('#btn_process_pbb').prop('disabled',true);
    			_checkpbb = 'wpbb';

    		}
    		else if (this.value == 'wopbb') {
    			$('#btn_process_pbb').prop('disabled',false);
    			_checkpbb = 'wopbb';

    		}
    		$('.btnfilter').trigger('click');

    	}

    });


    $(document).on('click','.emp_select',function(){
    	empid = $(this).val();
    	index = $(this).data('key');
    	if(!_Year){

    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('.emp_select').prop('checked',false);

    	}else{

    		if($(this).is(':checked')){
    			_listId[index] =  empid;

    		}else{
    			delete _listId[index];
    		}

    	}
    });

    var pbb_id;
    var employee_id;
    $(document).on('click','#namelist tr',function(){
    	employee_id = $(this).data('empid');

    	if(_Year){

    		$.ajax({
    			url:base_url+module_prefix+module+'/getPbbInfo',
    			data:{
    				'employee_id':employee_id,
    				'year':_Year,
    				'month':_Month,
    			},
    			type:'GET',
    			dataType:'JSON',
    			success:function(data){
    				console.log(data);
    				clear_form_elements('benefits-content');
    				pbb_id = '';

    				if(data !== null){
	    				var	pbb_amount = 0;
	    				var pbb_total = 0;
	    				var office = '';
	    				var position = '';
						tblPbb.clear().draw();
	    				office = (data.offices !== null) ? data.offices.Name : '';
						position = (data.positions !== null) ? data.positions.Name : '';
						pbb_amount = (data.amount) ? data.amount : 0;


	    				pbb_total = (pbb_amount) ? commaSeparateNumber(parseFloat(pbb_amount).toFixed(2)) : '';
						tblPbb.row.add( [
							position,
							office,
							pbb_total,
							'',
							pbb_total

						]).draw( false );
    				}

				}
			});
    	}else{
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});
    	}

    })

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
				type: "GET",
				url: base_url+module_prefix+module+'/show',
				data: {
					'q':$('._searchname').val(),
					'check_pbb':_checkpbb,
					'_year':_Year,
					'_month':_Month,
					'_officeId':officeId
				},
				beforeSend:function(){
				   		// $('.ajax-loader').css("visibility", "visible");

				   	},
				   	success: function(res){
				   		$(".panel-namelist").html(res);

				   	},
				   	complete:function(){
				   		// $('.ajax-loader').css("visibility", "hidden");
				   	}
				 });
		},500);
});

$(document).on('change','#searchby',function(){
	var val = $(this).val();
	console.log(base_url+module_prefix+module)
	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})

			$('#select_searchvalue').html(arr);
		}
	})

});

$(document).on('click','#btn_process_pbb',function(){
	console.log(_listId);
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Performance Base Bonus?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processPbb();
			}else{
				return false;
			}
		});
	}

});

$.processPbb = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/processPbb',
		data:{
			'_token':'{{ csrf_token() }}',
			'list_id':_listId,
			'year':_Year,
			'month':_Month,
			'employee_id':employee_id,
			'office_id':officeId,
			'rate':pbbRate,
		},
		type:'POST',
		beforeSend:function(){
			$('#btn_process_pbb').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
		},
		success:function(data){
			par = JSON.parse(data);
			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
					// window.location.href = base_url+module_prefix+module;
					_listId = [];
					$('.btn_cancel').trigger('click');
					$('.btnfilter').trigger('click');
					_checkpbb = 'wopbb';
					$('#btn_process_pbb').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
				}else{

				}
			}
	});
}

$(document).on('click','#btn_save_pbb',function(){

	if(no_of_actual_work == null){
		swal("No of field is empty!", "", "warning");
	}else{
		$.savePbb();
	}

});

$(document).on('click','#delete_pbb',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Transaction?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deletePbb();
			}else{
				return false;
			}
		});
	}
})

$.deletePbb = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deletePbb',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
			$('#delete_pbb').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response)

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})

				_listId = [];
				$('#delete_pbb').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
				$('.btnfilter').trigger('click');
			}
		}
	});
}


var timer;
$(document).on('click','.btnfilter',function(){
	year 	  = $('#select_year :selected').val();
	month 	  = $('#select_month :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	'year':_Year,
			   	'month':_Month,
			   	'checkpbb':_checkpbb,
			   	'officeId':officeId,
			   	},
			   beforeSend:function(){
			   		$('#loading').removeClass('hidden');
			   },
			   complete:function(){
			   		$('#loading').addClass('hidden');
			   },
			   success: function(res){
			   	// console.log(res);
			      $(".panel-namelist").html(res);
			   }
			});
		},500);
});



});


</script>
@endsection