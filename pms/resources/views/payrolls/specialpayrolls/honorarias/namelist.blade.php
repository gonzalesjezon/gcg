<table class="table scroll datatable" id="namelist">
	<thead>
		<tr>
			<th><input type="checkbox" name="check_all" id="check_all"></th>
			<th>Name</th>
		</tr>
	</thead>
	<tbody >
		@foreach($data as $key => $value)
			<tr data-empid="{{ $value->id }}"  >
				<td ><input type="checkbox" name="checked_emp_id[]" value="{{ $value->id  }}" class="emp_select" data-key="{{ $key }}" ></td>
				<td>{{ ucfirst(strtolower($value->lastname)) }} {{ ucfirst(strtolower($value->firstname)) }} {{ ucfirst(strtolower($value->middlename)) }}</td>
			</tr>
		@endforeach
	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){


});
</script>