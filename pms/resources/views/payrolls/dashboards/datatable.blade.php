<div class="tableFixHead">
	<table class="table table-striped"  id="namelist">
		<thead>
			<tr class="text-center">
				<th  >#</th>
				<th  >Employee Name</th>
				<th  >Assumption of Duty</th>
				<th  >Status</th>
				<th  >Job Grade</th>
				<th  >Monthly Salary</th>
				<th  >Action</th>
			</tr>
		</thead>
		<tbody >
			@foreach($data as $key => $value)
			<tr data-empid="{{ $value->id }}"  >
				<td ><input type="checkbox" name="checked_emp_id[]" value="{{ $value->id  }}" class="emp_select" data-key="{{ $key }}" ></td>
				<td >{{ ucfirst($value->lastname) }} {{ ucfirst($value->firstname) }} {{ ucfirst($value->middlename) }}</td>
				<td class="text-center">{{ @$value->employeeinformation->assumption_date }}</td>
				<td class="text-center">{{ @$value->employeeinformation->employeestatus->Name }}</td>
				<td class="text-center">{{ @$value->salaryinfo->jobgrade->job_grade }}</td>
				<td class="text-right">{{ number_format(@$value->salaryinfo->salary_new_rate,2) }}</td>
				<td ></td>
			</tr>
			@endforeach

		</tbody>
	</table>
	<br>
</div>
