<div class="row" style="margin-left: -5px;margin-right: -5px;">
	<table class="table table-responsive datatable" id="tbl_show">
		<thead>
			<tr>
				<th colspan="2">No of Hrs.</th>
				<th colspan="2">Total</th>
				<th rowspan="2" style="vertical-align: middle;">OT for the period</th>
				<th rowspan="2" style="vertical-align: middle;">Bal. Previous <br> Month</th>
				<th rowspan="2" style="vertical-align: middle;">OT Pay for <br> the period</th>
				<th rowspan="2" style="vertical-align: middle;">Total bal. <br> to be carried</th>
			</tr>
			<tr>
				<th>125 %</th>
				<th>150 %</th>
				<th>125 %</th>
				<th>150 %</th>
			</tr>
		</thead>
	</table>
</div>