@extends('app-front')
@section('content')
<div style="height: 3.5em;"></div>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-3">
			<div>
				<h5 ><b>Filter Employee By</b></h5>
				<table class="table borderless" style="border:none;font-weight: bold">
					<tr class="text-left">
						<td><span>Transaction Period</span></td>
					</tr>
					<tr>
						<td >
							<select class="employee-type form-control font-style2 select2" id="select_month" name="select_month" placeholder="Month">
								<option value=""></option>
								<?php
									$current = date('F',time());
								 ?>
								@foreach($months as $key => $month)
								<option value="{{$month}}" {{($current == $month) ? 'selected' : '' }}>{{ $month }}</option>
								@endforeach
							</select>
						</td>
						<td colspan="1">
							<select class="employee-type form-control font-style2 select2" id="select_year" name="select_year" placeholder="Year" >
								@foreach( range($latest_year,$earliest_year) as $i)
								<option value="{{$i}}">{{$i}}</option>
								@endforeach
							</select>
						</td>
					</tr>
					<!-- <tr>
						<td>Pay Period</td>
					</tr>
					<tr>
						<td>
							<select id="pay_period" class="form-control font-style2" name="pay_period">
								<option value=""></option>
								<option value="semimonthly">Semi Monthly</option>
								<option value="monthly">Monthly</option>
							</select>
						</td>
						<td >
							<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</td>
					</tr> -->
					<tr>
						<td>Employee Status</td>
					</tr>
					<tr>
						<td >
							<select class="form-control font-style2" id="status" name="status">
								<option value=""></option>
								<option value="plantilla">Plantilla</option>
								<option value="nonplantilla">Non Plantilla</option>
							</select>
						</td>
					</tr>
				</table>
				<div class="col-md-4">
					<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
					</div>
					<div class="col-md-8 text-right">
						<button class="btn btn-xs btn-info process_overtime" id="process_overtime" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
						<a class="btn btn-xs btn-danger" id="delete_overtime" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>

					</div>
					<div class="search-btn">
						<div class="col-md-12">
							<span >Search</span>
							<span class="radiobut-style radio-inline " style="margin-left: 70px;">
								<input type="radio" name="chk_overtime" id="wovertime" value="wovertime">
								W/ OT
							</span>

							<span class="radiobut-style radio-inline ">
								<input type="radio" name="chk_overtime" id="woovertime" value="woovertime">
								W/Out OT
							</span>
						</div>
					</div>
				<div >
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
				<br>
				<br>
				<div class="namelist" style="position: relative;top: -25px;">
					{!! $controller->show() !!}
				</div>

			</div>
		</div>
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-12">
					<label id="lbl_empname" style="text-transform: uppercase;"></label>
				</div>
			</div>
			{!! $controller->showDatatable() !!}

			<div class="row" style="margin-left: -5px;margin-right: -5px;">
				<div class="col-md-12">
					<div class="panel panel-default" style="padding: 10px;">
						<a class="btn btn-xs btn-info btn-editbg btn_edit" id="editOvertime" data-btnnew="newOvertime" data-btncancel="cancelOvertime" data-btnedit="editOvertime" data-btnsave="saveOvertime"><i class="fa fa-save"></i> Edit</a>
						<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" data-form="form" data-btnnew="newOvertime" data-btncancel="cancelOvertime" data-btnedit="editOvertime" data-btnsave="saveOvertime" id="saveOvertime"><i class="fa fa-save"></i> Save</a>
						<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newOvertime" data-btncancel="cancelOvertime" data-form="myform" data-btnedit="editOvertime" data-btnsave="saveOvertime"id="cancelOvertime"> Cancel</a>
						<hr>

						<form method="post" action="{{ url($module_prefix.'/'.$module)}}" id="form" onsubmit="return false">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="overtime_id" id="overtime_id">
							<table class="table borderless" style="border: none;">
								<tbody style="border-top:none;">
									<tr class="text-center">
										<td>OT Type</td>
										<td>Rate</td>
										<td>Actual</td>
										<td>Total</td>
									</tr>
									<tr>
										<td>Regular Day (125%) </td>
										<td>
											<input type="text" name="regular_rate" class="form-control" readonly="true" id="regular_rate">
										</td>
										<td>
											<input type="text" name="actual_regular_rate" class="form-control editOvertime" id="actual_regular_rate">
										</td>
										<td>
											<input type="text" name="actual_regular_amount" class="form-control" id="actual_regular_amount" placeholder="0.00" readonly>
										</td>
									</tr>
									<tr>
										<td>Holiday/Weekend (150%)</td>
										<td>
											<input type="text" name="holiday_rate" class="form-control" readonly="true" id="holiday_rate">
										</td>
										<td>
											<input type="text" name="actual_holiday_rate" class="form-control editOvertime" id="actual_holiday_rate">
										</td>
										<td>
											<input type="text" name="actual_holiday_amount" class="form-control" id="actual_holiday_amount" placeholder="0.00" readonly>
										</td>
									</tr>
									<tr>
										<td>Others</td>
										<td>
											<select class="form-control" name="other_rate" id="other_rate">
												<option></option>
												<option value="0.50">50%</option>
											</select>
										</td>
										<td></td>
										<td>
											<input type="text" name="actual_other_amount" class="form-control" id="actual_other_amount" placeholder="0.00" readonly>
										</td>
									</tr>
									<tr>
										<td colspan="2"></td>
										<td>Total Overtime Pay</td>
										<td>
											<input type="text" name="total_overtime_pay" class="form-control" id="total_overtime_pay" placeholder="0.00" readonly>
										</td>
									</tr>
									<tr>
										<td colspan="2"></td>
										<td>Max Amount to Avail</td>
										<td>
											<input type="text" name="max_amount_to_avail" class="form-control" id="max_amount_to_avail" placeholder="0.00" readonly>
										</td>
									</tr>
									<tr>
										<td colspan="2"></td>
										<td>Total Overtime Pay for the Period</td>
										<td>
											<input type="text" name="overtime_pay_for_period" class="form-control" id="overtime_pay_for_period" placeholder="0.00" readonly>
										</td>
									</tr>
									<tr>
										<td colspan="2"></td>
										<td>Balance Forwarded</td>
										<td>
											<input type="text" name="balance_forwarded" class="form-control" id="balance_forwarded" placeholder="0.00" readonly>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
// GENERATE YEAR

var table = $('#tbl_show').DataTable({
	columnDefs: [
    {
        targets: '_all',
        className: 'dt-body-right'
    }
  	],
	'dom':'<lf<t>pi>',
	"paging": false,
});


// ************************************************
var _Year;
var _Month;
var _bool = false;
$(document).on('change','#select_year',function(){
	_Year = "";
	_Year = $(this).find(':selected').val();
	if(_bool == true){
		$('.btnfilter').trigger('click');
	}
})
$(document).on('change','#select_month',function(){
	_Month = "";
	_Month = $(this).find(':selected').val();
	if(_bool == true){
		$('.btnfilter').trigger('click');
	}
})

$('.editOvertime').prop('readonly',true);
$('.select2').select2();
$('#select_month').select2({
    allowClear:true,
    placeholder: "Month",
});

$('#select_year').select2({
    allowClear:true,
    placeholder: "Year"
});

$('#select_year').trigger('change');
$('#select_month').trigger('change');

$('.isNumber').keypress(function (event) {
    return isNumber(event, this)
});
//  SEPARATE NUMBER IN COMMA
$(".onlyNumber").keyup(function(){
	amount  = $(this).val();
	if(amount == 0){
		$(this).val('');
	}else{
		plainAmount = amount.replace(/\,/g,'')
		$(this).val(commaSeparateNumber(plainAmount));
	}
});
var list_id = [];
// CHECK EMPLOYEE WITH OR W/ OUT OVERTIME
check_overtime = ""
 $('input[type=radio][name=chk_overtime]').change(function() {
 	if(!_Year){
 		swal({
		  	title: 'Select year and month first',
		  	type: "warning",
		  	showCancelButton: false,
		  	confirmButtonClass: "btn-warning",
		  	confirmButtonText: "OK",
		  	closeOnConfirm: false
		})
		$(this).prop('checked',false);
 	}else{
		if (this.value == 'wovertime') {
			$('#process_overtime').prop('disabled',true);
			$('#editForm').removeClass('hidden');
			_bool = true;
        	check_overtime = this.value;
        	list_id
        }
        else if (this.value == 'woovertime') {
        	$('#process_overtime').prop('disabled',false);
        	$('#editForm').addClass('hidden');
            check_overtime = this.value;
            list_id
        }
        $('.btnfilter').trigger('click');
 	}
});

// var payPeriod;
// var semiPayPeriod;
// $(document).on('change','#pay_period',function(){
// 	payPeriod = $(this).find(':selected').val();
// 	switch(payPeriod){
// 		case 'semimonthly':
// 			$('#semi_pay_period').removeClass('hidden');
// 		break;
// 		default:
// 			$('#semi_pay_period').addClass('hidden');
// 		break;
// 	}
// });

// $(document).on('change','#semi_pay_period',function(){
// 	semiPayPeriod = $(this).find(':selected').val();
// })

// GET SELECTED ID AND PUSH IT INTO ARRAY

$(document).on('click','.emp_select',function(){
	empid = $(this).val();
	index = $(this).data('key');
	if(!_Year && !_Month && !list_id){
		swal({
			  title: "Select year and month first",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
		$('.emp_select').prop('checked',false);
	}else{
		if($(this).is(':checked')){
			list_id[index] =  empid;

		}else{
			delete list_id[index];
		}
	}
});
$(document).on('click','#check_all',function(){
	if(!_Year && !_Month){
		swal({
			  title: "Select year and month first",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
		$('#check_all').prop('checked',false);
	}else{
		if ($(this).is(':checked')) {
	        $('.emp_select').prop('checked', 'checked');

	        $('.emp_select:checked').each(function(){
	        	list_id.push($(this).val())
	        });

	    } else {
	        $('.emp_select').prop('checked', false)
	        list_id = [];
	    }
	}
});


var actualRate;
var holidayRate;
var regularAmount 		= 0;
var holAmount 			= 0;
var otherAmount 		= 0;
var monthlyRateAmount 	= 0;
var totalOvertime 		= 0;
var maxAmountAvail 		= 0;
$('#actual_regular_rate').on('keyup',function(){
	actualRegular = $(this).val();
	regularAmount = compute_actual(actualRegular,actualRate);
	totalOvertime = compute_totalOvertime(regularAmount,holAmount,otherAmount);
	compute_overtimeForPeriod(totalOvertime,maxAmountAvail);
	regular_amount = (regularAmount) ? commaSeparateNumber(parseFloat(regularAmount).toFixed(2)) : '';
	total_overtime_amount = (totalOvertime) ? commaSeparateNumber(parseFloat(totalOvertime).toFixed(2)) : '';
	$('#total_overtime_pay').val(total_overtime_amount);
	$('#actual_regular_amount').val(regular_amount);
});

$('#actual_holiday_rate').on('keyup',function(){
	actualHol = $(this).val();
	holAmount = compute_actual(actualHol,holidayRate);
	totalOvertime = compute_totalOvertime(regularAmount,holAmount,otherAmount);
	compute_overtimeForPeriod(totalOvertime,maxAmountAvail);
	hol_amount = (holAmount) ? commaSeparateNumber(parseFloat(holAmount).toFixed(2)) : '';
	total_overtime_amount = (totalOvertime) ? commaSeparateNumber(parseFloat(totalOvertime).toFixed(2)) : '';
	$('#total_overtime_pay').val(total_overtime_amount);
	$('#actual_holiday_amount').val(hol_amount);
});

$('#other_rate').on('change',function(){
	value = 0;
	value = $(this).find(':selected').val();
	if(value){
		value = monthlyRateAmount;
	}
	otherAmount = parseFloat(value) / 2;
	totalOvertime = compute_totalOvertime(regularAmount,holAmount,otherAmount);
	other_amount = (otherAmount) ? commaSeparateNumber(parseFloat(otherAmount).toFixed(2)) : '';
	compute_overtimeForPeriod(totalOvertime,maxAmountAvail);
	total_overtime_amount = (totalOvertime) ? commaSeparateNumber(parseFloat(totalOvertime).toFixed(2)) : '';
	$('#total_overtime_pay').val(total_overtime_amount);
	$('#actual_other_amount').val(other_amount);
});

function compute_rate(monthly_amount){
	var regRate = 1.25;
	var holRate = 1.50;

	maxAmountAvail = parseFloat(monthly_amount) / 2;

	dailyRate = parseFloat(monthly_amount) / 22;
	hourRate = parseFloat(dailyRate) / 8;
	actualRate = parseFloat(hourRate) * regRate;
	holidayRate = parseFloat(hourRate) * holRate;

	holiday_rate = (holidayRate) ? commaSeparateNumber(parseFloat(holidayRate).toFixed(2)) : '';
	actual_rate = (actualRate) ? commaSeparateNumber(parseFloat(actualRate).toFixed(2)) : '';
	max_avail = (maxAmountAvail) ? commaSeparateNumber(parseFloat(maxAmountAvail).toFixed(2)) : '';

	$('#holiday_rate').val(holiday_rate);
	$('#regular_rate').val(actual_rate);
	$('#max_amount_to_avail').val(max_avail);
}

function compute_actual(actual,rate){
	actual = (actual) ? actual : 0;
	return parseFloat(actual) * parseFloat(rate);
}

function compute_totalOvertime(regular,holiday,other){
	regular = (regular) ? regular : 0;
	holiday = (holiday) ? holiday : 0;
	other = (other) ? other : 0;
	return parseFloat(regular) + parseFloat(holiday) + parseFloat(other);
}

function compute_overtimeForPeriod(total_overtime,max_amount){
	var overtimeForPeriod = 0;
	var balanceForwarded = 0;
	if(max_amount >  total_overtime){
		overtimeForPeriod = total_overtime;
	}else{
		overtimeForPeriod = max_amount;
		balanceForwarded = parseFloat(total_overtime) - parseFloat(max_amount);
	}

	overtime_for_period = (overtimeForPeriod) ? commaSeparateNumber(parseFloat(overtimeForPeriod).toFixed(2)) : '';
	balance_forwarded = (balanceForwarded) ? commaSeparateNumber(parseFloat(balanceForwarded).toFixed(2)) : '';

	$('#overtime_pay_for_period').val(overtime_for_period);
	$('#balance_forwarded').val(balance_forwarded);
}



// GET EMPLOYEE OVERTIME PAY DETAILS
$(document).on('click','#namelist tr',function(){
	employee_id = $(this).data('empid');
	if(_Year && _Month){
		$.ajax({
			url:base_url+module_prefix+module+'/getOvertimeInfo',
			data:{
				'employee_id':employee_id,
				'year':_Year,
				'month':_Month,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data);
				$('#net_overtime').text('')
				clear_form_elements('editOvertime');
				clear_form_elements('clear-input');
				$('#employee_id').val('');
				$('#overtime_id').val('');
				if(data.plantilla !== null){
					monthlyRateAmount = (data.plantilla.monthly_rate_amount)
				}

				if(data.nonplantilla !== null){
					monthlyRateAmount = (data.nonplantilla.monthly_rate_amount)
				}

				compute_rate(monthlyRateAmount);

				if(data.overtimepay){
					$('#overtime_id').val(data.overtimepay.id);
					actualRegularAmount = data.overtimepay.actual_regular_amount;
					actualHolidayAmount = data.overtimepay.actual_holiday_amount;
					overtimePayForPeriod = data.overtimepay.overtime_pay_for_period;
					balancePrevMonth = (data.overtimepay.balance_previous_month) ? data.overtimepay.balance_previous_month : 0;
					balanceForwarded = (data.overtimepay.balance_forwarded) ? data.overtimepay.balance_forwarded : 0;
					$('#actual_regular_rate').val(data.overtimepay.actual_regular_rate).trigger('keyup');
					$('#actual_holiday_rate').val(data.overtimepay.actual_holiday_rate).trigger('keyup');
					totalOtForPeriod = parseFloat(actualRegularAmount) + parseFloat(actualHolidayAmount);

					actual_regular_amount = (actualRegularAmount) ? commaSeparateNumber(parseFloat(actualRegularAmount).toFixed(2)) : "";
					actual_holiday_amount = (actualHolidayAmount) ? commaSeparateNumber(parseFloat(actualHolidayAmount).toFixed(2)) : "";
					total_ot_for_period = (totalOtForPeriod) ? commaSeparateNumber(parseFloat(totalOtForPeriod).toFixed(2)) : "";
					overtime_pay_for_period = (overtimePayForPeriod) ? commaSeparateNumber(parseFloat(overtimePayForPeriod).toFixed(2)) : "";
					balance_prev_month = (balancePrevMonth) ? commaSeparateNumber(parseFloat(balancePrevMonth).toFixed(2)) : "";
					balance_forwarded = (balanceForwarded) ? commaSeparateNumber(parseFloat(balanceForwarded).toFixed(2)) : "";

					table.clear().draw();
					table.row.add( [
						data.overtimepay.actual_regular_rate,
						data.overtimepay.actual_holiday_rate,
						actual_regular_amount,
						actual_holiday_amount,
						total_ot_for_period,
						balance_prev_month,
						overtime_pay_for_period,
						balance_forwarded

			        ]).draw( false );

			        table.rows(0).nodes().to$().attr("data-id", data.overtimepay.id);
			        table.rows(0).nodes().to$().attr("data-employeeid", data.overtimepay.employee_id);
			        table.rows(0).nodes().to$().attr("data-btnnew", "newDeduct");
			        table.rows(0).nodes().to$().attr("data-btnsave", "saveDeduct");
			        table.rows(0).nodes().to$().attr("data-btnedit", "editDeduct");
			        table.rows(0).nodes().to$().attr("data-btncancel", "cancelDeduct");
			        table.rows(0).nodes().to$().attr("data-btndelete", "deleteDeduct");
				}
			}
		});
	}else{
		swal({
			title: 'Select year and month first',
			type: "warning",
			showCancelButton: false,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			closeOnConfirm: false
		});
	}
});

$('#tbl_show tbody').on( 'click', 'tr', function () {
	if ( $(this).hasClass('selected') ) {
	    $(this).removeClass('selected');
	}else {
	    table.$('tr.selected').removeClass('selected');
	    $(this).addClass('selected');
	}
});

// function datatable(data){

// 	table.clear().draw();

// 	$.each(data,function(k,v){

// 		table.row.add( [

//         ]).draw( false );

//         table.rows(k).nodes().to$().attr("data-id", v.id);
//         table.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
//         table.rows(k).nodes().to$().attr("data-deductionid", v.deduction_id);
//         table.rows(k).nodes().to$().attr("data-amount", v.deduct_amount);
//         table.rows(k).nodes().to$().attr("data-datestart", v.deduct_date_start);
//         table.rows(k).nodes().to$().attr("data-dateend", v.deduct_date_end);
//         table.rows(k).nodes().to$().attr("data-payperiod", v.deduct_pay_period);
//         table.rows(k).nodes().to$().attr("data-dateterminated", v.deduct_date_terminated);
//         table.rows(k).nodes().to$().attr("data-btnnew", "newDeduct");
//         table.rows(k).nodes().to$().attr("data-btnsave", "saveDeduct");
//         table.rows(k).nodes().to$().attr("data-btnedit", "editDeduct");
//         table.rows(k).nodes().to$().attr("data-btncancel", "cancelDeduct");
//         table.rows(k).nodes().to$().attr("data-btndelete", "deleteDeduct");
// 	});
// }

$(document).on('click','#process_overtime',function(){
	if(list_id.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Overtime?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processOvertime();
			}else{
				return false;
			}
		});
	}
});

$(document).on('click','.save_overtime',function(){
	if(list_id.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Update Overtime?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processOvertime();
			}else{
				return false;
			}
		});
	}
});

// SAVE COMPUTED OVERTIME PAY
$.processOvertime = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/storeOvertimeInfo',
		type:'POST',
		data:{
			'_token':"{{ csrf_token() }}",
			'overtime_id':$('#overtime_id').val(),
			'employee_id':list_id,
			'empid':$('#employee_id').val(),

			// 'pay_period':payPeriod,
			// 'sub_pay_period':semiPayPeriod,
			'year':_Year,
			'month':_Month,
			'status':status
		},
		beforeSend:function(){
        	$('#process_overtime').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
		success:function(data){
			par = JSON.parse(data)
			swal({
			  	title: par.response,
			  	type: "success",
			  	showCancelButton: false,
			  	confirmButtonClass: "btn-success",
			  	confirmButtonText: "Yes",
			  	closeOnConfirm: false
			});
			$('#process_overtime').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
			clear_form_elements('editOvertime');
			$('#employee_id').val('');
			$('#overtime_id').val('');
			$('#editOvertime').removeClass('hidden');
			$('#saveOvertime').addClass('hidden');
			$('#cancelOvertime').addClass('hidden');
			$('.editOvertime').prop('readonly',true);
			list_id = [];
			$('.btnfilter').trigger('click')
		}
	});
}

var status;
$('#status').change(function(){
	status = $(this).find(':selected').val();
	$('.btnfilter').trigger('click');
})


// FILTER BY
var timer;
$(document).on('click','.btnfilter',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	'year':_Year,
			   	'month':_Month,
			   	'check_overtime':check_overtime,
			   	'status':status
			   },
			   success: function(res){
			      $(".namelist").html(res);
			   }
			});
		},500);
});


$(document).on('click','#delete_overtime',function(){
	if(list_id.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Overtime?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteOvertime();
			}else{
				return false;
			}
		});
	}
});

$.deleteOvertime = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteOvertime',
		data:{'empid':list_id,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
        	$('#delete_overtime').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
		success:function(response){
			par = JSON.parse(response)
			if(par.status){
				swal({
				  title: par.response,
				  type: "success",
				  showCancelButton: false,
				  confirmButtonClass: "btn-success",
				  confirmButtonText: "OK",
				  closeOnConfirm: false
				});

				list_id = [];
				$('#delete_overtime').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
				$('.btnfilter').trigger('click');
				clear_form_elements('myForm');
				$("#overtime_pay").text(0.00)
			}
		}
	})
}

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	"q":$('._searchname').val(),
			   	'chk_overtime':check_overtime,
			   	'_year':_Year,
			   	'_month':_Month
			},
			   success: function(res){
			      $(".namelist").html(res);
			   }
			});
		},500);
});

$('.btn_edit').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');
	$('.'+btnedit+' :input').attr("readonly",false);
	$('.'+btnedit).attr('readonly',false);
	$('#'+btnedit).addClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_cancel').on('click',function(){
	$('#overtime_id').val('');
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');

	$('.'+btnedit+' :input').attr("readonly",true);
	$('.'+btnedit).attr('readonly',true);
	// $('#'+btnnew).removeClass('hidden');
	$('#'+btnedit).removeClass('hidden');
	$('#'+btnsave).addClass('hidden');
	$('#'+btncancel).addClass('hidden');
	clear_form_elements('editOvertime');
	$('.error-msg').remove();

});

});
</script>
@endsection