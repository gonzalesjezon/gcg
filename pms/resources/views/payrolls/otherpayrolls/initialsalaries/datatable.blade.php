
<table class="table table-responsive datatable" id="tbl_initial_salary">
	<thead>
		<tr>
			<th>Transaction Date</th>
			<th>Gross Amount</th>
			<th>Total Deduction</th>
			<th>Net Amount</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody class="text-right">

	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_initial_salary').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_initial_salary tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {
	        $(this).removeClass('selected');
	        clear_form_elements('myForm4');

	        id 							= $(this).data('id');
			employee_id 				= $(this).data('employee_id');
			actual_workdays 			= $(this).data('actual_workdays');
			basic_amount 				= $(this).data('basic_amount');
			gsisContAmount 				= $(this).data('gsis_cont_amount');
			philhealthContAmount 		= $(this). data('philhealth_cont_amount');
			wTaxAmount 					= $(this).data('wtax_amount');
			date_from 					= $(this).data('date_from');
			date_to 					= $(this).data('date_to');
			year 						= $(this).data('year');
			month 						= $(this).data('month');

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');
			btndelete = $(this).data('btndelete');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btndelete).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}
	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }

	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
