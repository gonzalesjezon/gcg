@extends('layouts.app-ratareports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Pay Period</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select id="pay_period" class="form-control font-style2 select2" name="pay_period">
								<option value=""></option>
								<option value="semimonthly">Semi Monthly</option>
								<option value="monthly">Monthly</option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-6">
	       				<img src="{{url('/images/e2e_logo_header.png')}}" style="height: 50px;margin-top: 20px;">
	       				<h5><b>{{ $title }}</b></h5>
	       				<h6><b>Covered Period:</b> <span id="month_year"></span></h6>
	       			</div>
					<table class="table" id="payroll_transfer" style="border: 2px solid #333;">
						<thead>
							<tr class="text-center" style="font-weight:bold" >
								<td rowspan="2"></td>
								<td rowspan="2">Dept Code</td>
								<td rowspan="2">Name</td>
								<td rowspan="2">Designation</td>
								<td rowspan="2">SG</td>
								<td rowspan="2">RA</td>
								<td rowspan="2">TA</td>
								<td rowspan="2">TOTAL</td>
								<td colspan="{{ $cols+4}}">Deductions</td>
								<td rowspan="2">Net Amount</td>
								<td rowspan="2">Landbank Acct No.</td>
							</tr>
							<tr style="font-weight: bold;" class="text-center">
								<td>Add. Tax</td>
								<td>RA</td>
								<td>TA</td>
								@foreach($deductions as $key => $value)
								<td data-id="{{ $value->id }}" class="loanlists">{{ $value->name }}</td>
								@endforeach
								<td>Total</td>
							</tr>
						</thead>
						</thead>
						 <tbody id="tbl_body">
						 </tbody>
					</table>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
	for ( m =  0; m <= month.length - 1; m++) {
		mArr += '<option data-month="'+month[m]+'"" value="'+(m+1)+'">'+month[m]+'</option>';
	}
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	var _monthNumber;
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').data('month');
		_monthNumber = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false



			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				console.log(data);
					if(data.length !== 0){

						arr = [];
						ctr = 1;
						net_ra_amount = 0;
						net_ta_amount = 0;
						net_total_amount = 0;
						net_tax_amount = 0;
						net_ra_deduction_amount = 0;
						net_ta_deduction_amount = 0;
						net_deduction_amount = 0;
						net_amount_total = 0;
						$.each(data.transaction,function(k,v){
							ra_amount = 0;
							ta_amount = 0;
							total_amount = 0;
							tax_amount = 0;
							ra_deduction_amount = 0;
							ta_deduction_amount = 0;

							firstname = (v.employees) ? v.employees.firstname : '';
							lastname = (v.employees) ? v.employees.lastname : '';
							middlename = (v.employees) ? v.employees.middlename : '';
							position = (v.positions) ? v.positions.name : '';
							salarygrade = (v.salaryinfo) ? v.salaryinfo.salarygrade.salary_grade : '';
							ra_amount = (v.representation_amount) ? v.representation_amount : 0;
							ta_amount = (v.transportation_amount) ? v.transportation_amount : 0;
							tax_amount = (v.deductions) ? v.deductions.tax_deduction : 0;
							ra_deduction_amount = (v.deductions) ? v.deductions.ra_deduction : 0;
							ta_deduction_amount = (v.deductions) ? v.deductions.ta_deduction : 0;
							fullname = lastname+' '+firstname+' '+middlename;

							total_amount = (parseFloat(ra_amount) + parseFloat(ta_amount));
							total_deduction_amount = (parseFloat(tax_amount) + parseFloat(ra_deduction_amount) + parseFloat(ta_deduction_amount));
							net_amount = parseFloat(total_amount) - parseFloat(total_deduction_amount);

							net_ra_amount += parseFloat(ra_amount);
							net_ta_amount += parseFloat(ta_amount);
							net_total_amount += parseFloat(total_amount);
							net_tax_amount += parseFloat(tax_amount);
							net_ra_deduction_amount += parseFloat(ra_deduction_amount);
							net_ta_deduction_amount += parseFloat(ta_deduction_amount);
							net_deduction_amount += parseFloat(total_deduction_amount);
							net_amount_total += parseFloat(net_amount);



							ra_amount = (ra_amount !== 0) ? commaSeparateNumber(parseFloat(ra_amount).toFixed(2)) : '';
							ta_amount = (ta_amount !== 0) ? commaSeparateNumber(parseFloat(ta_amount).toFixed(2)) : '';
							total_amount = (total_amount !== 0) ? commaSeparateNumber(parseFloat(total_amount).toFixed(2)) : '';
							tax_amount = (tax_amount !== 0) ? commaSeparateNumber(parseFloat(tax_amount).toFixed(2)) : '';
							ra_deduction_amount = (ra_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(ra_deduction_amount).toFixed(2)) : '';
							ta_deduction_amount = (ta_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(ta_deduction_amount).toFixed(2)) : '';
							total_deduction_amount = (total_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(total_deduction_amount).toFixed(2)) : '';
							net_amount = (net_amount !== 0) ? commaSeparateNumber(parseFloat(net_amount).toFixed(2)) : '';

							arr += '<tr>';
							arr += '<td>'+ctr+'</td>';
							arr += '<td></td>';
							arr += '<td>'+fullname+'</td>';
							arr += '<td class="text-center">'+position+'</td>';
							arr += '<td class="text-center">'+salarygrade+'</td>';
							arr += '<td class="text-right">'+ra_amount+'</td>';
							arr += '<td class="text-right">'+ta_amount+'</td>';
							arr += '<td class="text-right">'+total_amount+'</td>';
							arr += '<td class="text-right">'+tax_amount+'</td>';
							arr += '<td class="text-right">'+ra_deduction_amount+'</td>';
							arr += '<td class="text-right">'+ta_deduction_amount+'</td>';
							$('.loanlists').each(function(k2,v2){
								arr += '<td class="text-right"></td>'
							});
							arr += '<td class="text-right">'+total_deduction_amount+'</td>';
							arr += '<td class="text-right">'+net_amount+'</td>';
							arr += '<td class="text-right"></td>';
							arr += '</tr>';

							ctr++;
						});

						net_ra_amount = (net_ra_amount !== 0) ? commaSeparateNumber(parseFloat(net_ra_amount).toFixed(2)) : '';
						net_ta_amount = (net_ta_amount !== 0) ? commaSeparateNumber(parseFloat(net_ta_amount).toFixed(2)) : '';
						net_total_amount = (net_total_amount !== 0) ? commaSeparateNumber(parseFloat(net_total_amount).toFixed(2)) : '';
						net_tax_amount = (net_tax_amount !== 0) ? commaSeparateNumber(parseFloat(net_tax_amount).toFixed(2)) : '';
						net_ra_deduction_amount = (net_ra_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(net_ra_deduction_amount).toFixed(2)) : '';
						net_ta_deduction_amount = (net_ta_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(net_ta_deduction_amount).toFixed(2)) : '';
						net_deduction_amount = (net_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(net_deduction_amount).toFixed(2)) : '';
						net_amount_total = (net_amount_total !== 0) ? commaSeparateNumber(parseFloat(net_amount_total).toFixed(2)) : '';

						arr += '<tr style="font-weight:bold;">';
						arr += '<td></td>';
						arr += '<td>Total</td>';
						arr += '<td></td>';
						arr += '<td class="text-center"></td>';
						arr += '<td class="text-center"></td>';
						arr += '<td class="text-right">'+net_ra_amount+'</td>';
						arr += '<td class="text-right">'+net_ta_amount+'</td>';
						arr += '<td class="text-right">'+net_total_amount+'</td>';
						arr += '<td class="text-right">'+net_tax_amount+'</td>';
						arr += '<td class="text-right">'+net_ra_deduction_amount+'</td>';
						arr += '<td class="text-right">'+net_ta_deduction_amount+'</td>';
						$('.loanlists').each(function(k2,v2){
							arr += '<td class="text-right"></td>'
						});
						arr += '<td class="text-right">'+net_deduction_amount+'</td>';
						arr += '<td class="text-right">'+net_amount_total+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '</tr>';

						$('#tbl_body').html(arr);

						days = daysInMonth(_monthNumber,_Year)

						if(_payPeriod == 'monthly'){
							_coveredPeriod = _Month+' 1-'+days+', '+_Year;
						}else{
							switch(_semiPayPeriod){
								case 'firsthalf':
									_coveredPeriod = _Month+' 1-15, '+_Year;
								break;
								default:
									_coveredPeriod =_Month+' 16-'+days+', '+_Year;
								break;
							}
						}

						$('#month_year').text(_coveredPeriod);

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

	function daysInMonth(month, year) {
	    return new Date(year, month, 0).getDate();
	}

	$('#print').on('click',function(){
		$('#reports').printThis();
	})

})
</script>
@endsection