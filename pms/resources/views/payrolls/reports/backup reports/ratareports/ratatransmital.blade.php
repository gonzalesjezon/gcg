@extends('layouts.app-ratareports')


@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css') }}">

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Pay Period</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select id="pay_period" class="form-control font-style2 select2" name="pay_period">
								<option value=""></option>
								<option value="semimonthly">Semi Monthly</option>
								<!-- <option value="monthly">Monthly</option> -->
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-6">
						<span class="lineheight" style="margin-left: 10px;"><b>Print Date</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<input type="text" name="print_date" class="form-control font-style2 datepicker" id="print_date">
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports" style="padding-bottom: 120px;">
	       		<div class="row">
	       			<div class="col-md-12 text-center" style="font-weight: bold">
	       				Republic of the Philippines				<br>
						E2E Solutions Management Phils. Inc.	 <br>
						Room 304 URC Bldg, 2123  Espana Blvd Brgy 513, Sampaloc Manila
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12 text-center">
	       				<h4 style="font-weight: bold;"></h4>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 50px;">
	       			<div class="col-md-12 text-left">
	       				<span style="font-size: 12px;padding-left: 25px;">
	       					<i>Control No. RO-18-103 (07/18 P-56)</i>
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12 text-right" style="padding-right: 50px">
	       				<span id="month_year"></span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12 text-left" style="padding-left: 30px;">
	       				<br>
	       				<span>
		       				<b>The Manager</b>	<br>
							Landbank 	<br>
							Katipunan Branch <br>
							Quezon City	 <br>
							<br>
							Madam:
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12" style="padding-left: 30px;">
	       				<br>
	       				<span>
			 <span style="padding-left: 50px;"></span>Please  debit  from  our  Current  Account  No. 1462-1011-78  the  amount  of  <span id="amount_word"></span> AND <span id="decimal"></span>/100 ( <span id="amount"></span>)  and  credit  the  same  to the  attached  list of  officials   with  their  corresponding Landbank Account Nos. for payment of  RATA for the period  <span id="for_month"></span>.		<br>

	       				</span>
	       			</div>
	       		</div>
	       		<br>
	       		<div class="row" style="padding-bottom: 30px;">
	       			<div class="col-md-12 text-center" style="padding-left: 250px;">
	       				<span>
	       					Very truly yours, <br><br>
	       					<b>Virginia V. Octa</b>	 <br>
        					Manager, Administration Dept.

	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12 text-center" style="padding-bottom: 30px;">
	       				<b>APPROVED</b>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-6 text-center">
	       				<span>
	       					<b>Claudine B. Orocio-Isorena</b> <br>
							Dep. Adm., Administration and Legal
	       				</span>
	       			</div>
	       			<div class="col-md-6 text-center">
	       				<span>
	       					<b>Patrick Lester N. Ty</b>  <br>
       						Chief Regulator
	       				</span>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option data-month="'+month[m]+'"" value="'+(m+1)+'">'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	var _monthNumber;
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').data('month');
		_monthNumber = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})

	$('.datepicker').datepicker({
		dateFormat:'MM d, yy'
	});

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})



	$(document).on('click','#preview',function(){
		if(!_Year && !_Month && !_payPeriod || !_semiPayPeriod){
			swal({
				  title: "Select Year, Month, Pay Period!",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					console.log(data);
					if(data.transaction !== 0){
						sub_total = 0;
						var sub_total_word;
						$.each(data.transaction,function(k,v){
							transportation_amount = (v.transportation_amount !== null) ? v.transportation_amount : 0;
							representation_amount = (v.representation_amount !== null) ? v.representation_amount : 0;
							total = parseFloat(transportation_amount) + parseFloat(representation_amount);

							sub_total += parseFloat(total);
						})

						decimal = sub_total.toString().split('.');

						sub_total_word = sub_total;
						convert_to_word = toWords(sub_total_word);
						sub_total = (sub_total !== 0) ? commaSeparateNumber(parseFloat(sub_total).toFixed(2)) : '';


						decimal = (decimal.length !== 1) ? decimal[1] : '00';

						$('#decimal').text(decimal);
						$('#amount').text(sub_total);
						$('#amount_word').text(convert_to_word.toUpperCase());

						days = daysInMonth(_monthNumber,_Year)

						if(_payPeriod == 'monthly'){
							_coveredPeriod = _Month+' 1-'+days+', '+_Year;
						}else{
							switch(_semiPayPeriod){
								case 'firsthalf':
									_coveredPeriod = _Month+' 1-15, '+_Year;
								break;
								default:
									_coveredPeriod =_Month+' 16-'+days+', '+_Year;
								break;
							}
						}
						// //  PRINT DATE
						// date = new Date();
						// current_day = date.getDate();
						print_date = $('#print_date').val();

						$('#for_month').text(_coveredPeriod);
						$('#month_year').text(print_date);

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection