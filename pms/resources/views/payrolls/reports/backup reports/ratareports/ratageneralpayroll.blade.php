@extends('layouts.app-ratareports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Pay Period</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select id="pay_period" class="form-control font-style2 select2" name="pay_period">
								<option value=""></option>
								<option value="semimonthly">Semi Monthly</option>
								<option value="monthly">Monthly</option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid" id="reports">
	       		<div class="row">
	       			<div class="col-md-12 text-center" style="font-weight: bold">
	       				<img src="{{url('/images/e2e_logo_header.png')}}" style="height: 50px;margin-top: 20px;"> <br>
						Room 304 URC Bldg, 2123  Espana Blvd Brgy 513, Sampaloc Manila <br>
						GENERAL PAYROLL  <br> <br>
						PAYMENT RATA FOR THE PERIOD OF <span id="month_year"></span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12">
	       				<table class="table" >
							<thead>
								<tr class="text-center" style="font-weight: bold">
									<td rowspan="2">#</td>
									<td rowspan="2">Code</td>
									<td rowspan="2">Name</td>
									<td rowspan="2">Dept. Designation</td>
									<td rowspan="2">SG</td>
									<td colspan="3">GAA Rates</td>
									<td rowspan="2">Deductions</td>
									<td rowspan="2">
										1/2 Month Net Amount
									</td>
									<td rowspan="2" colspan="2">Landbank Acct. No.</td>
								</tr>
								<tr class="text-center">
									<td>RA</td>
									<td>TA</td>
									<td>Gross</td>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td colspan="12" style="border:none">
										<div class="col-md-6">
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-12 text-center">
													<b>C E R T I F I C A T I O N</b>
												</div>
											</div>
											<div class="row" >
												<div class="col-md-6" style="text-align: justify;">
													This is to certify that the herein named
													 officials have actually incurred
													representation expenses and transportation
													 expenses during the period.
													Further certify that officials receiving
													transportation allowance for the said
													period are not actually using RP/MWSS vehicle.

												</div>
												<div class="col-md-6" style="text-align: justify;">
													This is to certify that the herein-named
													officials have been present for at least
													six (6) days for the period <span id="signatory_date"></span>.

												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan="4" style="border:none">
										Certified:  (1) as to accuracy of computation (2) accounting 	<br>
										codes and journal entries are proper; (3) properly entered in 	<br>
										the accounting records.
									</td>
									<td colspan="4" style="border:none">
										Approved for appropriation and funds <br>
										being available:
									</td >
									<td class="text-center" colspan="4" style="border:none">
								        <b>Claudine B. Orocio-Isorena</b> <br>
										Dep. Adm., Administration and Legal
									</td>
								</tr>
								<tr>
									<td style="border:none"></td>
									<td style="border:none"></td>
									<td colspan="2" class="text-center" style="border:none"></td>
									<td colspan="4" class="text-center" style="border:none"></td>
									<td colspan="4" class="text-center" style="border:none;padding-right: 120px;">Approved:</td>
								</tr>
								<tr>
									<td style="border:none"></td>
									<td style="border:none"></td>
									<td colspan="2" class="text-center" style="border:none">
										<b>Theresa V. Makiling </b> <br>
										Finance Officer  <br>
										RO-18-______
									</td>
									<td colspan="4" class="text-center" style="border:none">
										<b>Joriel M. Dagsa</b> <br>
										 Chief Corporate Accountant
									</td>
									<td colspan="4" class="text-center" style="border:none">
										<b> Patrick Lester N. Ty</b> <br>
										 Chief Regulator
									</td>
								</tr>
							</tfoot>
							 <tbody id="tbl_body">
							 </tbody>
						</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option data-month="'+month[m]+'"" value="'+(m+1)+'">'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	var _monthNumber;
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').data('month');
		_monthNumber = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month && !_payPeriod){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					console.log(data);
					if(data.length !== 0){

						arr = [];


						total_representation_amount = 0;
						total_transportation_amount = 0;
						total_gross_amount = 0;
						net_deduction_amount = 0;
						net_amount = 0;
						$.each(data,function(k,v){

							fullname = v.employees.lastname+' '+v.employees.firstname+' '+v.employees.middlename;

							salary_grade = (v.salaryinfo.salarygrade_id) ? v.salaryinfo.salarygrade.salary_grade : '';
							position = (v.positions) ? v.positions.name : '';

							ra_deduction_amount = (v.deductions) ? v.deductions.ra_deduction : 0;
							ta_deduction_amount = (v.deductions) ? v.deductions.ta_deduction : 0;
							tax_amount = (v.deductions) ? v.deductions.tax_deduction : 0;
							total_deduction = (parseFloat(ra_deduction_amount) + parseFloat(ta_deduction_amount) + parseFloat(tax_amount));

							representation_amount = (v.representation_amount !== null) ? v.representation_amount : 0;
							transportation_amount = (v.transportation_amount !== null) ? v.transportation_amount : 0;
							gross_amount = parseFloat(representation_amount) + parseFloat(transportation_amount);

							representation_amount = (_payPeriod == 'semimonthly') ? representation_amount/2 : representation_amount;
							transportation_amount = (_payPeriod == 'semimonthly') ? transportation_amount/2 : transportation_amount;
							gross_amount = (_payPeriod == 'semimonthly') ? gross_amount/2 : gross_amount;
							total_amount = parseFloat(gross_amount) - parseFloat(total_deduction);

							total_representation_amount += parseFloat(representation_amount);
							total_transportation_amount += parseFloat(transportation_amount);
							net_deduction_amount += parseFloat(total_deduction);
							total_gross_amount += parseFloat(gross_amount);
							net_amount += parseFloat(total_amount);

							representation_amount = (representation_amount !== 0) ? commaSeparateNumber(parseFloat(representation_amount).toFixed(2)) : '';
							transportation_amount = (transportation_amount !== 0) ? commaSeparateNumber(parseFloat(transportation_amount).toFixed(2)) : '';
							gross_amount = (gross_amount !== 0) ? commaSeparateNumber(parseFloat(gross_amount).toFixed(2)) : '';
							total_deduction = (total_deduction !== 0) ? commaSeparateNumber(parseFloat(total_deduction).toFixed(2)) : '';
							total_amount = (total_amount !== 0) ? commaSeparateNumber(parseFloat(total_amount).toFixed(2)) : '';

							arr += '<tr>';
							arr += '<td>'+(k+1)+'</td>';
							arr += '<td></td>';
							arr += '<td>'+fullname+'</td>';
							arr += '<td>'+position+'</td>';
							arr += '<td class="text-center">'+salary_grade+'</td>';
							arr += '<td class="text-right">'+representation_amount+'</td>';
							arr += '<td class="text-right">'+transportation_amount+'</td>';
							arr += '<td class="text-right">'+gross_amount+'</td>';
							arr += '<td class="text-right">'+total_deduction+'</td>';
							arr += '<td class="text-right">'+total_amount+'</td>';
							arr += '<td colspan="2"></td>';
							arr += '</tr>';

						});
						total_transportation_amount = (total_transportation_amount !== 0) ? commaSeparateNumber(parseFloat(total_transportation_amount).toFixed(2)) : '';
						total_representation_amount = (total_representation_amount !== 0) ? commaSeparateNumber(parseFloat(total_representation_amount).toFixed(2)) : '';
						total_gross_amount = (total_gross_amount !== 0) ? commaSeparateNumber(parseFloat(total_gross_amount).toFixed(2)) : '';
						net_deduction_amount = (net_deduction_amount !== 0) ? commaSeparateNumber(parseFloat(net_deduction_amount).toFixed(2)) : '';
						net_amount = (net_amount !== 0) ? commaSeparateNumber(parseFloat(net_amount).toFixed(2)) : '';
						arr += '<tr>';
						arr += '<td></td>';
						arr += '<td></td>';
						arr += '<td><b>Total</b></td>';
						arr += '<td></td>';
						arr += '<td></td>';
						arr += '<td class="text-right"><b>'+total_representation_amount+'</b></td>';
						arr += '<td class="text-right"><b>'+total_transportation_amount+'</b></td>';
						arr += '<td class="text-right"><b>'+total_gross_amount+'</b></td>';
						arr += '<td class="text-right"><b>'+net_deduction_amount+'</b></td>';
						arr += '<td class="text-right"><b>'+net_amount+'</b></td>';
						arr += '<td colspan="2"></td>';
						arr += '</tr>';


						$('#tbl_body').html(arr);

						days = daysInMonth(_monthNumber,_Year)

						if(_payPeriod == 'monthly'){
							_coveredPeriod = _Month+' 1-'+days+', '+_Year;
						}else{
							switch(_semiPayPeriod){
								case 'firsthalf':
									_coveredPeriod = _Month+' 1-15, '+_Year;
								break;
								default:
									_coveredPeriod =_Month+' 16-'+days+', '+_Year;
								break;
							}
						}

						$('#month_year').text(_coveredPeriod);
						$('#signatory_date').text(_coveredPeriod);

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection