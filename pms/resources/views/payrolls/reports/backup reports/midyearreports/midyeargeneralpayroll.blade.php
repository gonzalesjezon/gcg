@extends('layouts.app-midyearreports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>
<!-- 		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Pay Period</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select id="pay_period" class="form-control font-style2 select2" name="pay_period">
								<option value=""></option>
								<option value="semimonthly">Semi Monthly</option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr> -->
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-12 text-left" style="font-weight: bold">
	       				<img src="{{url('/images/e2e_logo_header.png')}}" style="height: 50px;margin-top: 20px;"> <br>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12 text-center">
	       				<h4>GENERAL PAYROLL</h4>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12 text-left">
	       				<span>Payment of Mid Year Bonus for FY 2017 per DBM BC No. 2017-3</span> <br>
	       				<span>dated 16 November 2017 and GCG MC No. 2017-05 dated 05 December 2017 respectively.</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12">
						<table class="table" style="border: 2px solid #5f5f5f">
						<thead class="text-center" style="font-weight: bold;">
							<tr>
								<td></td>
								<td>Name</td>
								<td>Designation</td>
								<td>SG</td>
								<td>Basic Salary</td>
								<td>%</td>
								<td>Mid Year Bonus</td>
								<td>Less: ITW</td>
								<td>Net Amount</td>
								<td>Land Bank <br> Accnt. No</td>
							</tr>
						</thead>
						 <tbody id="tbl_body">
						 </tbody>
						</table>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12 text-center">
	       				<span>
	       					I hereby certify that this payroll is correct and that	<br>
							the claims herein stated are for services actually	<br>
							and duly rendered as authorized by management.		 <br><br>

							<b>Claudine B. Orocio-Isorena</b>	<br	>
          					Dep. Adm., Administration and Legal
	       				</span>
	       				<br>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 50px;">
	       			<div class="col-md-4" style="padding-left: 30px;">
	       				<span>
		       				Certified:  (1) as to accuracy of computation (2) accounting <br>
							codes and journal entries are proper; (3) properly entered in 	<br>
							the accounting records.
	       				</span>
	       			</div>
	       			<div class="col-md-4 text-center" style="padding-left: 20px;">
	       				<span>
	       					Approved for appropriation and funds being
							available:
	       				</span>
	       			</div>
	       			<div class="col-md-4 text-center" style="padding-left: 20px;">
	       				<span>
	       					<b>APPROVED:</b>
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 30px;">
	       			<div class="col-md-4 text-center">
	       				<b>Theresa V. Makiling</b> <br>
					    Finance Officer <br>
						RO-18-______

	       			</div>
	       			<div class="col-md-4 text-center">
	       				<b>Joriel M. Dagsa</b> <br>
      					Chief Corporate Accountant	<br>

	       			</div>
	       			<div class="col-md-4 text-center">
	       				<b>Patrick Lester N. Ty</b> <br>
               			Chief Regulator

	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option data-month="'+month[m]+'"" value="'+(m+1)+'">'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	var _monthNumber;
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').data('month');
		_monthNumber = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})



	$(document).on('click','#preview',function(){
		if(!_Year && !_Month){
			swal({
				  title: "Select Year, Month, Pay Period First!",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					console.log(data);
					if(data.transaction !== 0){

						arr = [];

						sub_basic_salary = 0;
						sub_mid_year_amount = 0;

						$.each(data.transaction,function(k,v){

							firstname = (v.employees.firstname !== null) ? v.employees.firstname : '';
							lastname = (v.employees.lastname !== null) ? v.employees.lastname : '';
							middlename = (v.employees.middlename !== null) ? v.employees.middlename : '';
							positions = (v.positions !== null) ? v.positions.name : '';
							salarygrade = (v.salaryinfo.salarygrade !== null) ? v.salaryinfo.salarygrade.salary_grade : '';
							basic_salary = (v.salaryinfo !== null) ? v.salaryinfo.salary_new_rate : 0;
							percentage = (v.percentage !== null) ? v.percentage : '';
							mid_year_amount = (v.amount !== null) ? v.amount : 0;

							sub_mid_year_amount += parseFloat(mid_year_amount);
							sub_basic_salary += parseFloat(basic_salary);


							basic_salary = (basic_salary !== 0) ? commaSeparateNumber(parseFloat(basic_salary).toFixed(2)) : '';
							mid_year_amount = (mid_year_amount !== 0) ? commaSeparateNumber(parseFloat(mid_year_amount).toFixed(2)) : '';


							arr += '<tr>';
							arr += '<td>'+(parseInt(k)+1)+'</td>';
							arr += '<td class="text-left">'+lastname+' '+firstname+' '+middlename+'</td>';
							arr += '<td class="text-left">'+positions+'</td>';
							arr += '<td class="text-left">'+salarygrade+'</td>';
							arr += '<td class="text-right">'+basic_salary+'</td>';
							arr += '<td class="text-right">'+percentage+'</td>';
							arr += '<td class="text-right">'+mid_year_amount+'</td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right">'+mid_year_amount+'</td>';
							arr += '<td class="text-right"></td>';
							arr += '</tr>';
						});

						sub_basic_salary = (sub_basic_salary !== 0) ? commaSeparateNumber(parseFloat(sub_basic_salary).toFixed(2)) : '';
						sub_mid_year_amount = (sub_mid_year_amount !== 0) ? commaSeparateNumber(parseFloat(sub_mid_year_amount).toFixed(2)) : '';

						arr += '<tr style="font-weight:bold">';
						arr += '<td></td>';
						arr += '<td class="text-left">GRAND TOTAL</td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-right">'+sub_basic_salary+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '<td class="text-right">'+sub_mid_year_amount+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '<td class="text-right">'+sub_mid_year_amount+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '</tr>';






						$('#tbl_body').html(arr);

						// days = daysInMonth(_monthNumber,_Year);

						// if(_payPeriod == 'monthly'){
						// 	_coveredPeriod = _Month+' 1-'+days+', '+_Year;
						// }else{
						// 	switch(_semiPayPeriod){
						// 		case 'firsthalf':
						// 			_coveredPeriod = _Month+' 1-15, '+_Year;
						// 		break;
						// 		default:
						// 			_coveredPeriod =_Month+' 16-'+days+', '+_Year;
						// 		break;
						// 	}
						// }

						// $('#month_year').text(_coveredPeriod);

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
$('#print').on('click',function(){
	$('#reports').printThis();
})
})
</script>
@endsection