@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printpayslip.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td ><span><b>Employee Name</b></span>

				<select class="form-control select2" name="employee_id" id="employee_id" style="margin:auto;width: 100%;">
						<option value=""></option>
						@foreach($employeeinfo as $value)
						<option value="{{ $value->id }}">{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</option>
						@endforeach
					</select>

			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<a class="btn btn-xs btn-danger preview">Preview</a>
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="padding:0px;width:60%;">
	    <div class="mypanel border0" style="height:100%;width: 100%;">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="panel-body">
		       <div class="row">
		       		<div class="col-md-12">
		       			<div class="reports" id="reports">
		       				<div class="report-header">
		       					<div class="col-md-12 text-center">
		       						<img src="{{url('/images/e2e_logo_header.png')}}" style="height: 55px;margin-top: 20px;margin-bottom: 10px;">
		       						<i></i>
		       					</div>
		       				</div>
		       				<div style="border-bottom: 1px solid #e3e3e3; clear: both;"></div>
		       				<div class="reports-content">
		       					<div class="report-title">
		       						<h4 class="report-title">EMPLOYEE PAYSLIP</h4>
		       					</div>
		       					<br><br>
		       					<div class="report-name">
		       						<div class="row">
			       						<div class="col-md-2">
			       							Employee Name
			       						</div>
			       						<div class="col-md-1" style="width: 25px;">:</div>
		       							<div class="col-md-4 text-left">
		       								<span id="employee_name"></span>
		       							</div>
		       							<div class="col-md-2">Employee No</div>
		       							<div class="col-md-1"  style="width: 25px;">:</div>
		       							<div class="col-md-2"><span id="employee_number"></span></div>
		       						</div>
		       						<div class="row">
		       							<div class="col-md-2">Designation</div>
		       							<div class="col-md-1"  style="width: 25px;">:</div>
		       							<div class="col-md-4"><span id="designation"></span></div>
		       						</div>
		       						<div class="row">
		       							<div class="col-md-2">Payroll Period</div>
		       							<div class="col-md-1"  style="width: 25px;">:</div>
		       							<div class="col-md-2"><span id="payroll_period"></span></div>
		       						</div>
		       					</div>

			       				<div class="reports-body" style="margin: auto;width: 95%;">
			       					<div class="row">

			       						<div class="col-md-6 text-right" style="margin-right: 50px;"><span><b>Amount</b></span></div>
			       						<div class="col-md-4 text-right"> <span><b>Year to Date</b></span></div>

			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">BASIC</span></div>
			       						<div class="col-md-4 text-right"><span id="basic_salary_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">PERA</span></div>
			       						<div class="col-md-4 text-right"><span id="pera_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-3"><span style="margin-left: 15px">Overtime for</span> <span class="month_year"></span></div>
			       						<div class="col-md-3 text-right"><span id="overtime_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-3"><span style="margin-left: 15px">Uniform Allowances</span></div>
			       						<div class="col-md-3 text-right"><span id="uniform_allowance_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">CNA & PEI</span></div>
			       						<div class="col-md-4 text-right"><span id="cna_pei_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<br>
			       					<div class="row">
			       						<div class="col-md-2"><b>Deductions</b></div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">GSIS PS</span></div>
			       						<div class="col-md-4 text-right"><span id="gsis_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">PAGIBIG PS</span></div>
			       						<div class="col-md-4 text-right"><span id="pagibig_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">PH PS</span></div>
			       						<div class="col-md-4 text-right"><span id="philhealth_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">Opt. Ins</span></div>
			       						<div class="col-md-4 text-right"><span id="opt_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">Genesis</span></div>
			       						<div class="col-md-4 text-right"><span id="genesis_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">Hosp. Plus</span></div>
			       						<div class="col-md-4 text-right"><span id="hosp_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
									<div class="row">
			       						<div class="col-md-3"><span style="margin-left: 15px">GSIS Conso Loan</span></div>
			       						<div class="col-md-3 text-right"><span id="conso_loan_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">Pag-Ibig MPL</span></div>
			       						<div class="col-md-4 text-right"><span id="pagibig_mpl_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
									<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">Policy</span></div>
			       						<div class="col-md-4 text-right"><span id="policy_loan_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">WF Contribution</span></div>
			       						<div class="col-md-4 text-right"><span id="wf_contribution_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">WF Loan</span></div>
			       						<div class="col-md-4 text-right"><span id="wf_loan_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">WASSSLAI Dep.</span></div>
			       						<div class="col-md-4 text-right"><span id="wasslai_cont_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">WASSSLAI Ln.</span></div>
			       						<div class="col-md-4 text-right"><span id="wasslai_loan_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">WITH TAX</span></div>
			       						<div class="col-md-4 text-right"><span id="wtax_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">MPLP Loan</span></div>
			       						<div class="col-md-4 text-right"><span id="mplp_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">Union Dues</span></div>
			       						<div class="col-md-4 text-right"><span id="union_dues_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">MP Coop</span></div>
			       						<div class="col-md-4 text-right"><span id="mp_coop_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px">Agency Fee</span></div>
			       						<div class="col-md-4 text-right"><span id="agency_fee_amount"></span></div>
			       						<div class="col-md-4 text-right">0.00</div>
			       					</div>
			       					<br><br>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px"><b>Total Earnings</b></span></div>
			       						<div class="col-md-4 text-right"><span id="total_earning_amount"></span></div>
			       					</div>
			       					<div class="row">
			       						<div class="col-md-3"><span style="margin-left: 15px"><b>Total Deductions</b></span></div>
			       						<div class="col-md-3 text-right"><span id="total_deduction_amount"></span></div>
			       					</div>
			       					<br>
			       					<div class="row">
			       						<div class="col-md-2"><span style="margin-left: 15px"><b>NET Earnings</b></span></div>
			       						<div class="col-md-4 text-right"><span id="net_earning_amount"></span></div>

			       					</div>
			       					<br><br>
			       					<div class="row">
			       						<div class="col-md-4 text-left"><span style="margin-left: 15px"><b>Noted By:</b></span></div>
			       					</div>
			       					<br>
			       					<br><br>
			       					<div class="row">
			       						<div class="col-md-6 text-left" style="margin-left: 15px">
				       						<b>MA. THERESA V. MAKILING</b> <br>
				       						Finance Officer B
				       					</div>
			       					</div>
			       				</div>
		       				</div>
		       				<br>
		       				<div class="reports-footer">
		       					<div class="col-md-6">
		       						<div class="footer-left-message">
			       						<i>
			       							This is a computer generated document and does
											not require any signature if without alterations
			       						</i>

		       						</div>
		       					</div>
		       				</div>
		       			</div>
		       		</div>
		       </div>
	       </div>

	    </div>
	 </div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

$(document).on('click','.preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	$.ajax({
		url:base_url+module_prefix+module+'/getPayslip',
		data:{
			'id':_empid,
			'year':year,
			'month':month,
			'emp_type':emp_type,
			'emp_status':emp_status,
			'category':category,
			'searchby':searchby,
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){
			console.log(data)
			$('#basic_salary_amount').text('');
			$('.month').text('');
			$('.year').text('');
			$('#employee_name').text('');
			$('#designation').text('');
			$('#employee_number').text('');
			$('#pera_amount').text('');
			$('#overtime_amount').text('');
			$('#total_earning_amount').text('');
			$('#wtax_amount').text('');
			$('#gsis_amount').text('');
			$('#pagibig_amount').text('');
			$('#philhealth_amount').text('');
			$('#pagibig_mpl_amount').text('');
			$('#optionallife').text('');
			$('#calamityloan').text('');
			$('#policy_loan_amount').text('');
			$('#conso_loan_amount').text('');
			$('#educloan').text('');
			$('#wf_contribution_amount').text('');
			$('#wasslai_cont_amount').text('');
			$('#wasslai_loan_amount').text('');
			$('#union_dues_amount').text('');
			$('#mp_coop_amount').text('');
			$('#mplp_amount').text('');
			$('#wf_loan_amount').text('');
			$('#total_deduction_amount').text('');
			$('#net_earning_amount').text('');
			$('#payroll_period').text('');

			if(data.plantilla == true){
				if(data.transaction === null){
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}else{

					basic_salary = (data.transaction.basic_net_pay) ? data.transaction.basic_net_pay : 0.00;
					$('#basic_salary_amount').text(commaSeparateNumber(parseFloat(basic_salary).toFixed(2)));

					month = data.transaction.month;
					$('.month').text(month);

					year = data.transaction.year;
					$('.year').text(year);

					fullname = data.transaction.employees.lastname+' '+data.transaction.employees.firstname+' '+data.transaction.employees.middlename;
					$('#employee_name').text(fullname);

					position = (data.transaction.positions !== null) ? data.transaction.positions.name : '';
					$('#designation').text(position);

					empno = data.employeeinfo.employees.employee_number;
					$('#employee_number').text(empno);

					// office = data.transaction.offices.name;
					// $('#office').text(office);

					// position = (data.transaction.positionitems.position_id !== null) ? data.transaction.positionitems.positions.name : '';
					// $('#position').text(position);
					if(data.benefitinfo.length !== 0){
						$.each(data.benefitinfo,function(k,v) {
							pera = (data.benefitinfo[k].benefit_amount) ? data.benefitinfo[k].benefit_amount : 0.00;
						})
					}else{
						pera = 0;
					}
					if(data.overtime !== null){
						overtime = (data.overtime.total_overtime_amount !== null) ? data.overtime.total_overtime_amount : 0;
					}else{
						overtime = 0;
					}

					totalEarnings = (parseFloat(basic_salary) + parseFloat(pera) + parseFloat(overtime));
					pera = (pera !== 0) ? commaSeparateNumber(parseFloat(pera).toFixed(2)) : '0.00';
					overtime = (overtime !== 0) ? commaSeparateNumber(parseFloat(overtime).toFixed(2)) : '0.00';
					$('#pera_amount').text(pera);
					$('#overtime_amount').text(overtime);
					$('#total_earning_amount').text(commaSeparateNumber(parseFloat(totalEarnings).toFixed(2)));

					taxamount = (data.employeeinfo.tax_contribution) ? data.employeeinfo.tax_contribution : '0.00';
					$('#wtax_amount').text(commaSeparateNumber(parseFloat(taxamount).toFixed(2)));

					gsiscontribution = (data.employeeinfo.gsis_contribution) ? data.employeeinfo.gsis_contribution : '0.00';
					$('#gsis_amount').text(commaSeparateNumber(parseFloat(gsiscontribution).toFixed(2)));

					pagibigcontribution = (data.employeeinfo.pagibig_contribution) ? data.employeeinfo.pagibig_contribution : '0.00';
					$('#pagibig_amount').text(commaSeparateNumber(parseFloat(pagibigcontribution).toFixed(2)));

					philhealtcontribution = (data.employeeinfo.philhealth_contribution) ? data.employeeinfo.philhealth_contribution : '0.00';
					$('#philhealth_amount').text(commaSeparateNumber(parseFloat(philhealtcontribution).toFixed(2)));

					totalcontribution = (parseFloat(taxamount) + parseFloat(gsiscontribution) + parseFloat(pagibigcontribution) + parseFloat(philhealtcontribution))

					// $('#totalcontribution').text(commaSeparateNumber(parseFloat(totalcontribution).toFixed(2)));

					// if(data.benefitinfo.benefits.benefit_name == 'PERA'){
					// 	pera_amount = (data.benefitinfo.benefit_amount) ? data.benefitinfo.benefit_amount : 0.00;
					// 	$('#peraamount').text(commaSeparateNumber(parseFloat(pera_amount).toFixed(2)));
					// }
					var consoloan = 0;
					var policyloan = 0;
					var calamityloan = 0;
					var optionallife = 0;
					var mplp_amount = 0;
					var educloan = 0;
					var mpl_amount = 0;
					var wasslai_loan_amount = 0;
					var wf_loan_amount = 0;
					$.each(data.loaninfo,function(k,v){
						if(data.loaninfo[k].loans.name == 'POLICY LOAN'){
							policyloan = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
						}
						if(data.loaninfo[k].loans.name == 'CONSOLOAN'){
							consoloan = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
						}
						if(data.loaninfo[k].loans.name == 'EMERGENCY/CALAMITY LOAN'){
							calamityloan = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
						}
						if(data.loaninfo[k].loans.name == 'OPTIONAL LIFE'){
							optionallife = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
						}
						if(data.loaninfo[k].loans.code == 'MPL'){
							mpl_amount = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
						}
						if(data.loaninfo[k].loans.name == 'EDUC LOAN'){
							educloan = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
						}
						if(data.loaninfo[k].loans.name == 'WASSSLAI Loan'){
							wasslai_loan_amount = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
						}
						if(data.loaninfo[k].loans.code == 'WFLOAN'){
							wf_loan_amount = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
						}
						if(data.loaninfo[k].loans.code == 'MPLP'){
							mplp_amount = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
						}
					});

					var wf_contribution_amount = 0;
					var wasslai_cont_amount = 0;
					var union_dues_amount = 0;
					var mp_coop_amount = 0;
					$.each(data.deductioninfo,function(k,v){
						if(data.deductioninfo[k].deductions.name == 'WELFARE FUND'){
							wf_contribution_amount = (data.deductioninfo[k].deduct_amount) ? data.deductioninfo[k].deduct_amount : 0;
						}
						if(data.deductioninfo[k].deductions.name == 'WASSSLAI  Capital Contribution'){
							wasslai_cont_amount = (data.deductioninfo[k].deduct_amount) ? data.deductioninfo[k].deduct_amount : 0;
						}
						if(data.deductioninfo[k].deductions.name == 'UNION DUES'){
							union_dues_amount = (data.deductioninfo[k].deduct_amount) ? data.deductioninfo[k].deduct_amount : 0;
						}
						if(data.deductioninfo[k].deductions.name == 'MULTI-PURPOSE COOPERATIVE CONTRIBUTION'){
							mp_coop_amount = (data.deductioninfo[k].deduct_amount) ? data.deductioninfo[k].deduct_amount : 0;
						}


					});

					policyloan = (policyloan) ? policyloan : 0;
					calamityloan = (calamityloan) ? calamityloan : 0;
					optionallife = (optionallife) ? optionallife : 0;
					consoloan = (consoloan) ? consoloan : 0;
					mplp_amount = (mplp_amount) ? mplp_amount : 0;
					mpl_amount = (mpl_amount) ? mpl_amount : 0;
					educloan = (educloan) ? educloan : 0;
					wf_contribution_amount = (wf_contribution_amount) ? wf_contribution_amount : 0;
					wasslai_cont_amount = (wasslai_cont_amount) ? wasslai_cont_amount : 0;
					wasslai_loan_amount = (wasslai_loan_amount) ? wasslai_loan_amount : 0;
					union_dues_amount = (union_dues_amount) ? union_dues_amount : 0;

					mp_coop_amount = (mp_coop_amount) ? mp_coop_amount : 0;
					wf_loan_amount = (wf_loan_amount) ? wf_loan_amount : 0;

					$('#pagibig_mpl_amount').text(commaSeparateNumber(parseFloat(mpl_amount).toFixed(2)));
					$('#optionallife').text(commaSeparateNumber(parseFloat(optionallife).toFixed(2)));
					$('#calamityloan').text(commaSeparateNumber(parseFloat(calamityloan).toFixed(2)));
					$('#policy_loan_amount').text(commaSeparateNumber(parseFloat(policyloan).toFixed(2)));
					$('#conso_loan_amount').text(commaSeparateNumber(parseFloat(consoloan).toFixed(2)));
					$('#educloan').text(commaSeparateNumber(parseFloat(educloan).toFixed(2)));
					$('#wf_contribution_amount').text(commaSeparateNumber(parseFloat(wf_contribution_amount).toFixed(2)));
					$('#wasslai_cont_amount').text(commaSeparateNumber(parseFloat(wasslai_cont_amount).toFixed(2)));
					$('#wasslai_loan_amount').text(commaSeparateNumber(parseFloat(wasslai_loan_amount).toFixed(2)));
					$('#union_dues_amount').text(commaSeparateNumber(parseFloat(union_dues_amount).toFixed(2)));
					$('#mp_coop_amount').text(commaSeparateNumber(parseFloat(mp_coop_amount).toFixed(2)));
					$('#mplp_amount').text(commaSeparateNumber(parseFloat(mplp_amount).toFixed(2)));
					$('#wf_loan_amount').text(commaSeparateNumber(parseFloat(wf_loan_amount).toFixed(2)));

					total_deduction_amount = (parseFloat(policyloan) + parseFloat(consoloan) + parseFloat(calamityloan) + parseFloat(optionallife)+ parseFloat(mpl_amount) + parseFloat(educloan)) + parseFloat(totalcontribution) + parseFloat(wf_contribution_amount) + parseFloat(wasslai_loan_amount) + parseFloat(wasslai_cont_amount) + parseFloat(union_dues_amount) + parseFloat(mp_coop_amount) + parseFloat(mplp_amount) + parseFloat(wf_loan_amount);

					$('#total_deduction_amount').text(commaSeparateNumber(parseFloat(total_deduction_amount).toFixed(2)));

					netpay = (parseFloat(totalEarnings) - parseFloat(total_deduction_amount));
					$('#net_earning_amount').text(commaSeparateNumber(parseFloat(netpay).toFixed(2)));

					$('#payroll_period').text(_Month+' '+_Year);

					$('#btnModal').trigger('click');

					return false;
				}

			}else{
				if(data.nonplantilla === null){
					swal({
						title: "No Records Found",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});
				}else{
					basic_salary = (data.nonplantilla.basic_net_pay) ? data.nonplantilla.basic_net_pay : 0.00;
					$('#basic_salary_amount').text(commaSeparateNumber(parseFloat(basic_salary).toFixed(2)));

					position = (data.nonplantilla.positions !== null) ? data.nonplantilla.positions.name : '';
					$('#designation').text(position);

					month = data.nonplantilla.month;
					$('.month').text(month);

					year = data.nonplantilla.year;
					$('.year').text(year);

					fullname = data.nonplantilla.employees.lastname+' '+data.nonplantilla.employees.firstname+' '+data.nonplantilla.employees.middlename;
					$('#employee_name').text(fullname);

					empno = data.employeeinfo.employees.employee_number;
					$('#employee_number').text(empno);

					// office = data.transaction.offices.name;
					// $('#office').text(office);

					// position = (data.transaction.positionitems.position_id !== null) ? data.transaction.positionitems.positions.name : '';
					// $('#position').text(position);

					if(data.overtime !== null){
						overtime = (data.overtime.total_overtime_amount !== null) ? data.overtime.total_overtime_amount : 0;
					}else{
						overtime = 0;
					}

					totalEarnings = (parseFloat(basic_salary) + parseFloat(overtime));
					overtime = (overtime !== 0) ? commaSeparateNumber(parseFloat(overtime).toFixed(2)) : '0.00';
					$('#overtime_amount').text(overtime);
					$('#total_earning_amount').text(commaSeparateNumber(parseFloat(totalEarnings).toFixed(2)));

					taxamount_one = (data.nonplantilla.tax_rate_amount_one) ? data.nonplantilla.tax_rate_amount_one : 0;
					taxamount_two = (data.nonplantilla.tax_rate_amount_two) ? data.nonplantilla.tax_rate_amount_two : 0;

					taxamount = parseFloat(taxamount_one) + parseFloat(taxamount_two);
					$('#wtax_amount').text(commaSeparateNumber(parseFloat(taxamount).toFixed(2)));

					total_deduction_amount = taxamount;

					$('#total_deduction_amount').text(commaSeparateNumber(parseFloat(total_deduction_amount).toFixed(2)));

					netpay = (parseFloat(totalEarnings) - parseFloat(total_deduction_amount));
					$('#net_earning_amount').text(commaSeparateNumber(parseFloat(netpay).toFixed(2)));

					$('#payroll_period').text(_Month+' '+_Year);

					$('#btnModal').trigger('click');
				}

			}



		}
	})


});
$('#print').on('click',function(){
	$('#reports').printThis();
})
})
</script>
@endsection