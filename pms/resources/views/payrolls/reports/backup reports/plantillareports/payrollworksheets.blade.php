@extends('layouts.app-plantillareports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscape.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Pay Period</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select id="pay_period" class="form-control font-style2 select2" name="pay_period">
								<option value=""></option>
								<option value="semimonthly">Semi Monthly</option>
								<option value="monthly">Monthly</option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-6">
	       				<img src="{{url('/images/e2e_logo_header.png')}}" style="height: 50px;margin-top: 20px;">
	       				<h5><b>{{ $title }}</b></h5>
	       				<h6><b>Covered Period:</b> <span id="month_year"></span></h6>
	       			</div>
					<table class="table" id="payroll_transfer" style="width: 300em;border: 2px solid #333;">
						<thead>
							<tr class="text-center" style="font-weight:bold" >
								<td rowspan="2" >#</td>
								<td rowspan="2" >Name</td>
								<td rowspan="2" >Designation</td>
								<td rowspan="2" >SG</td>
								<td rowspan="2" >Basic Salary</td>
								<td rowspan="2" >PERA</td>
								<td rowspan="2" >GROSS</td>
								<td colspan="2">GSIS</td>
								<td colspan="1">GSIS</td>
								<td colspan="2">PAGIBIG</td>
								<td colspan="2">PHILHEALTH</td>
								<td colspan="7">GSIS</td>
								<td colspan="2">PAGIBIG</td>
								<td colspan="2">Welfare Fund</td>
								<td colspan="1">WASSSLAI</td>
								<td colspan="1">WASSSLAI</td>
								<td rowspan="2" >Fortunecare</td>
								<td rowspan="2" >Tax</td>
								<td colspan="2">Multi-Purpose Coop</td>
								<td rowspan="2" >Union Dues</td>
								<td rowspan="2" >MPLP Ln</td>
								<td rowspan="2" >SSS <br> Contribution</td>
								<td rowspan="2" >Total <br> Deductions</td>
								<td rowspan="2" >Net Amount</td>
								<td rowspan="2" >1/2 mo. Pay</td>
								<td rowspan="2" >Amount GSIS-HL</td>
								<td rowspan="2" >Travel All/ <br> Uniform Allowance</td>
								<td rowspan="2" >Smart/Globe <br> Mobilephone Bill</td>
								<td rowspan="2" >Net Pay</td>
								<td rowspan="2" >Landbank Acct. No.</td>
								<td rowspan="2" >Signature</td>
							</tr>
							<tr class="text-center" style="font-weight:bold">
								<td >PS</td>
								<td >GS</td>
								<td >EC</td>
								<td >PS</td>
								<td >GS</td>
								<td >PS</td>
								<td >GS</td>
								<td >Opt. Ins.</td>
								<td >Opt Loan</td>
								<td >Consoloan</td>
								<td >Policy Loan</td>
								<td >Emerg Loan</td>
								<td >LCH-DCS</td>
								<td >Educ Loan</td>
								<td >MPL</td>
								<td >Pagibig II</td>
								<td >PS</td>
								<td >GS</td>
								<td >Dep</td>
								<td >Loan</td>
								<td >Contribution</td>
								<td >Loan</td>
							</tr>
						</thead>
						</thead>
						 <tbody id="tbl_body">
						 </tbody>
					</table>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
	for ( m =  0; m <= month.length - 1; m++) {
		mArr += '<option data-month="'+month[m]+'"" value="'+(m+1)+'">'+month[m]+'</option>';
	}
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	var _monthNumber;
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').data('month');
		_monthNumber = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false



			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getPayrollWorksheetReport',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				console.log(data);
					if(data.length !== 0){

						arr = [];

						var net_salary 				 = 0;
						var net_pera 				 = 0;
						var net_gross_pay 			 = 0;
						var net_gsis_ps 			 = 0;
						var net_gsis_gs 			 = 0;
						var net_pagibig_ps 			 = 0;
						var net_pagibig_gs 			 = 0;
						var net_philhealth_ps 		 = 0;
						var net_philhealth_gs 		 = 0;
						var net_pagibig_two_amount 	 = 0;
						var net_tax_contribution 	 = 0;
						var net_consoloan_amount 	 = 0;
						var net_policy_amount 	 	 = 0;
						var net_optional_life_amount = 0;
						var net_education_loan_amount = 0;
						var net_calamity_amount 	 = 0;
						var net_deduction 	 	 	 = 0;
						var net_pay 		 	 	 = 0;
						var net_loans 		 	 	 = 0;
						var net_one_half_net_pay 	 = 0;
						var net_lch_loan 			 = 0;
						var net_wf_loan 			 = 0;
						var net_wasslai_loan 		 = 0;
						var net_mwss_loan 		 	 = 0;
						var net_multi_purpose_loan 	 = 0;
						var net_sss_cont_amount  	 = 0;
						var net_mpcc_amount  		 = 0;
						var net_ud_amount  			 = 0;
						var net_wf_amount  			 = 0;
						var net_wd_amount  			 = 0;
						var net_ecc_amount 			 = 0;

						$.each(data,function(k,v){
							var multi_purpose_loan 	 	 = 0;
							var consoloan_amount 		 = 0;
							var policy_amount 	 		 = 0;
							var optional_life_amount 	 = 0;
							var education_loan_amount 	 = 0;
							var calamity_amount 	 	 = 0;
							var lch_loan  			 	 = 0;
							var total_deduction 		 = 0;
							var total_net_pay			 = 0;
							var total_loans 			 = 0;
							var one_half_net_pay  		 = 0;
							var wf_loan  		 		 = 0;
							var wasslai_loan  		 	 = 0;
							var mwss_loan 				 = 0;
							var sss_cont_amount  		 = 0;
							var mpcc_amount  			 = 0;
							var ud_amount  				 = 0;
							var wf_amount  				 = 0;
							var wd_amount  				 = 0;
							var ecc_amount 				 = 0;

							fullname = v.employees.lastname+' '+v.employees.firstname+' '+v.employees.middlename
							salarygrade = v.salaryinfo.salarygrade.salary_grade;
							position = (v.positions) ? v.positions.name : '';

							monthly_rate_amount = (v.salaryinfo) ? v.salaryinfo.salary_new_rate : 0;
							net_salary += parseFloat(monthly_rate_amount);

							pera_amount = (v.allowances) ? v.allowances : 0;
							net_pera += parseFloat(pera_amount);

							gross_pay = (v.gross_pay) ? v.gross_pay : 0;
							net_gross_pay += parseFloat(gross_pay);

							gsis_ps = (v.employeeinfo.gsis_contribution) ? v.employeeinfo.gsis_contribution : 0;
							net_gsis_ps += parseFloat(gsis_ps);

							gsis_gs = (v.employeeinfo.er_gsis_share) ? v.employeeinfo.er_gsis_share : 0;
							net_gsis_gs += parseFloat(gsis_gs);

							pagibig_ps = (v.employeeinfo.pagibig_contribution) ? v.employeeinfo.pagibig_contribution : 0;
							net_pagibig_ps += parseFloat(pagibig_ps);

							pagibig_gs = (v.employeeinfo.er_pagibig_share) ? v.employeeinfo.er_pagibig_share : 0;
							net_pagibig_gs += parseFloat(pagibig_gs);

							philhealth_ps = (v.employeeinfo.philhealth_contribution) ? v.employeeinfo.philhealth_contribution : 0;
							net_philhealth_ps += parseFloat(philhealth_ps);

							philhealth_gs = (v.employeeinfo.er_philhealth_share) ? v.employeeinfo.er_philhealth_share : 0;
							net_philhealth_gs += parseFloat(philhealth_gs);

							pagibig_two_amount = (v.employeeinfo.pagibig2) ? v.employeeinfo.pagibig2 : 0;
							net_pagibig_two_amount += parseFloat(pagibig_two_amount);

							tax_contribution = (v.employeeinfo.tax_contribution) ? v.employeeinfo.tax_contribution : 0;
							net_tax_contribution += parseFloat(tax_contribution);


							if(v.employeeinfo.loaninfo.length !== 0){
								$.each(v.employeeinfo.loaninfo,function(key,val){
									if(val.loans.name){
										switch(val.loans.name){
											case 'CONSOLOAN':
												consoloan_amount = (val.loan_amortization) ? val.loan_amortization : 0
											break;
											case 'POLICY LOAN':
												policy_amount = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
											case 'OPTIONAL LIFE':
												optional_life_amount = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
											case 'EDUCATION LOAN':
												education_loan_amount = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
											case 'EMERGENCY/CALAMITY LOAN':
												calamity_amount = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
											case 'MULTI PURPOSE LOAN':
												multi_purpose_loan = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
											case 'LCH-DCS':
												lch_loan = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
											case 'WELFARE FUND LOAN':
												wf_loan = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
											case 'WASSSLAI Loan':
												wasslai_loan = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
											case 'MULTI-PURPOSE COOPERATIVE LOAN':
												mwss_loan = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
										}
									}
								})
								consoloan_amount = (consoloan_amount) ? consoloan_amount : 0;
								net_consoloan_amount += parseFloat(consoloan_amount);

								policy_amount = (policy_amount) ? policy_amount : 0;
								net_policy_amount += parseFloat(policy_amount);

								education_loan_amount = (education_loan_amount) ? education_loan_amount : 0;
								net_education_loan_amount += parseFloat(education_loan_amount);

								calamity_amount = (calamity_amount) ? calamity_amount : 0;
								net_calamity_amount += parseFloat(calamity_amount);

								net_lch_loan += parseFloat(lch_loan);
								net_wf_loan  += parseFloat(wf_loan);
								net_wasslai_loan  += parseFloat(wasslai_loan);
								net_mwss_loan  += parseFloat(mwss_loan);
								net_multi_purpose_loan  += parseFloat(multi_purpose_loan);
								net_optional_life_amount  += parseFloat(optional_life_amount);

								total_loans = (parseInt(consoloan_amount) + parseInt(policy_amount) + parseInt(education_loan_amount)  + parseInt(calamity_amount) + parseFloat(lch_loan) + parseFloat(wf_loan) + parseFloat(wasslai_loan) + parseFloat(mwss_loan) + parseFloat(multi_purpose_loan) + parseFloat(optional_life_amount)) ;

								net_loans += parseFloat(total_loans);
							}

							if(v.employeeinfo.payrolldeduction.length !== 0){
								$.each(v.employeeinfo.payrolldeduction,function(k1,v1){
									if(v1.deductions.code){
										switch(v1.deductions.code){
											case 'SSS CONT':
												sss_cont_amount = (v1.deduct_amount) ? v1.deduct_amount : 0
											break;
											case 'MPCC':
												mpcc_amount = (v1.deduct_amount) ? v1.deduct_amount : 0 ;
											break;
											case 'UD':
												ud_amount = (v1.deduct_amount) ? v1.deduct_amount : 0 ;
											break;
											case 'WF':
												wf_amount = (v1.deduct_amount) ? v1.deduct_amount : 0 ;
											break;
											case 'WD':
												wd_amount = (v1.deduct_amount) ? v1.deduct_amount : 0 ;
											break;
										}
									}
								})

								net_sss_cont_amount += parseFloat(sss_cont_amount);
								net_mpcc_amount += parseFloat(mpcc_amount);
								net_ud_amount += parseFloat(ud_amount);
								net_wf_amount += parseFloat(wf_amount);
								net_wd_amount += parseFloat(wd_amount);

							}

							ecc_amount = (v.ecc_amount) ? v.ecc_amount : 0;
							net_ecc_amount += parseFloat(ecc_amount);


							total_deduction = (parseFloat(gsis_ps) + parseFloat(pagibig_ps) + parseFloat(philhealth_ps)  + parseFloat(pagibig_two_amount) + parseFloat(tax_contribution) + parseFloat(total_loans) + parseFloat(sss_cont_amount) + parseFloat(mpcc_amount) + parseFloat(ud_amount) + parseFloat(wf_amount) + parseFloat(wd_amount));
							net_deduction += parseFloat(total_deduction);

							total_net_pay = (parseFloat(monthly_rate_amount)  - parseFloat(total_deduction));
							net_pay += parseFloat(total_net_pay);
							one_half_net_pay = (total_net_pay/2);
							net_one_half_net_pay += parseFloat(one_half_net_pay);

							monthly_rate_amount = (monthly_rate_amount) ? commaSeparateNumber(parseFloat(monthly_rate_amount).toFixed(2)) : '';
							pera_amount = (pera_amount) ? commaSeparateNumber(parseFloat(pera_amount).toFixed(2)) : '';
							sss_cont_amount = (sss_cont_amount) ? commaSeparateNumber(parseFloat(sss_cont_amount).toFixed(2)) : '';
							mpcc_amount = (mpcc_amount) ? commaSeparateNumber(parseFloat(mpcc_amount).toFixed(2)) : '';
							ud_amount = (ud_amount) ? commaSeparateNumber(parseFloat(ud_amount).toFixed(2)) : '';
							wf_amount = (wf_amount) ? commaSeparateNumber(parseFloat(wf_amount).toFixed(2)) : '';
							wd_amount = (wd_amount) ? commaSeparateNumber(parseFloat(wd_amount).toFixed(2)) : '';
							wf_loan = (wf_loan) ? commaSeparateNumber(parseFloat(wf_loan).toFixed(2)) : '';
							gross_pay = (gross_pay) ? commaSeparateNumber(parseFloat(gross_pay).toFixed(2)) : '';
							gsis_ps = (gsis_ps) ? commaSeparateNumber(parseFloat(gsis_ps).toFixed(2)) : '';
							gsis_gs = (gsis_gs) ? commaSeparateNumber(parseFloat(gsis_gs).toFixed(2)) : '';
							pagibig_ps = (pagibig_ps) ? commaSeparateNumber(parseFloat(pagibig_ps).toFixed(2)) : '';
							pagibig_gs = (pagibig_gs) ? commaSeparateNumber(parseFloat(pagibig_gs).toFixed(2)) : '';
							philhealth_ps = (philhealth_ps) ? commaSeparateNumber(parseFloat(philhealth_ps).toFixed(2)) : '';
							philhealth_gs = (philhealth_gs) ? commaSeparateNumber(parseFloat(philhealth_gs).toFixed(2)) : '';
							consoloan_amount = (consoloan_amount) ? commaSeparateNumber(parseFloat(consoloan_amount).toFixed(2)) : '';
							policy_amount = (policy_amount) ? commaSeparateNumber(parseFloat(policy_amount).toFixed(2)) : '';
							optional_life_amount = (optional_life_amount) ? commaSeparateNumber(parseFloat(optional_life_amount).toFixed(2)) : '';
							education_loan_amount = (education_loan_amount) ? commaSeparateNumber(parseFloat(education_loan_amount).toFixed(2)) : '';
							calamity_amount = (calamity_amount) ? commaSeparateNumber(parseFloat(calamity_amount).toFixed(2)) : '';
							lch_loan = (lch_loan) ? commaSeparateNumber(parseFloat(lch_loan).toFixed(2)) : '';
							wf_loan = (wf_loan) ? commaSeparateNumber(parseFloat(wf_loan).toFixed(2)) : '';
							wasslai_loan = (wasslai_loan) ? commaSeparateNumber(parseFloat(wasslai_loan).toFixed(2)) : '';
							mwss_loan = (mwss_loan) ? commaSeparateNumber(parseFloat(mwss_loan).toFixed(2)) : '';
							multi_purpose_loan = (multi_purpose_loan) ? commaSeparateNumber(parseFloat(multi_purpose_loan).toFixed(2)) : '';
							pagibig_two_amount = (pagibig_two_amount) ? commaSeparateNumber(parseFloat(pagibig_two_amount).toFixed(2)) : '';
							tax_contribution= (tax_contribution) ? commaSeparateNumber(parseFloat(tax_contribution).toFixed(2)) : '';
							total_deduction = (total_deduction) ? commaSeparateNumber(parseFloat(total_deduction).toFixed(2)) : '';
							total_net_pay = (total_net_pay) ? commaSeparateNumber(parseFloat(total_net_pay).toFixed(2)) : '';
							one_half_net_pay = (one_half_net_pay) ? commaSeparateNumber(parseFloat(one_half_net_pay).toFixed(2)) : '';
							ecc_amount = (ecc_amount !== 0) ? ecc_amount : '';



							arr += '<tr class="text-center">';
							arr += '<td >'+(parseInt(k) + 1)+'</td>';
							arr += '<td>'+fullname+'</td>';
							arr += '<td>'+position+'</td>';
							arr += '<td>'+salarygrade+'</td>';
							arr += '<td>'+monthly_rate_amount+'</td>';
							arr += '<td>'+pera_amount+'</td>';
							arr += '<td>'+gross_pay+'</td>';
							arr += '<td>'+gsis_ps+'</td>';
							arr += '<td>'+gsis_gs+'</td>';
							arr += '<td>'+ecc_amount+'</td>';
							arr += '<td>'+pagibig_ps+'</td>';
							arr += '<td>'+pagibig_gs+'</td>';
							arr += '<td>'+philhealth_ps+'</td>';
							arr += '<td>'+philhealth_gs+'</td>';
							arr += '<td></td>';
							arr += '<td>'+optional_life_amount+'</td>';
							arr += '<td>'+consoloan_amount+'</td>';
							arr += '<td>'+policy_amount+'</td>';
							arr += '<td>'+calamity_amount+'</td>';
							arr += '<td>'+lch_loan+'</td>';
							arr += '<td>'+education_loan_amount+'</td>';
							arr += '<td></td>';
							arr += '<td>'+pagibig_two_amount+'</td>';
							arr += '<td>'+wf_amount+'</td>';
							arr += '<td>'+wf_loan+'</td>';
							arr += '<td>'+wd_amount+'</td>';
							arr += '<td>'+wasslai_loan+'</td>';
							arr += '<td></td>';
							arr += '<td>'+tax_contribution+'</td>';
							arr += '<td>'+mpcc_amount+'</td>';
							arr += '<td>'+mwss_loan+'</td>';
							arr += '<td>'+ud_amount+'</td>';
							arr += '<td>'+multi_purpose_loan+'</td>';
							arr += '<td>'+sss_cont_amount+'</td>';
							arr += '<td>'+total_deduction+'</td>';
							arr += '<td>'+total_net_pay+'</td>';
							arr += '<td>'+one_half_net_pay+'</td>';
							arr += '<td></td>';
							arr += '<td></td>';
							arr += '<td></td>';
							arr += '<td>'+one_half_net_pay+'</td>';
							arr += '<td></td>';
							arr += '<td></td>';
							arr += '</tr>';
						});
						net_salary = (net_salary) ? commaSeparateNumber(parseFloat(net_salary).toFixed(2)) : '';
						net_pera = (net_pera) ? commaSeparateNumber(parseFloat(net_pera).toFixed(2)) : '';
						net_gross_pay = (net_gross_pay) ? commaSeparateNumber(parseFloat(net_gross_pay).toFixed(2)) : '';
						net_gsis_ps = (net_gsis_ps) ? commaSeparateNumber(parseFloat(net_gsis_ps).toFixed(2)) : '';
						net_gsis_gs = (net_gsis_gs) ? commaSeparateNumber(parseFloat(net_gsis_gs).toFixed(2)) : '';
						net_pagibig_ps = (net_pagibig_ps) ? commaSeparateNumber(parseFloat(net_pagibig_ps).toFixed(2)) : '';
						net_pagibig_gs = (net_pagibig_gs) ? commaSeparateNumber(parseFloat(net_pagibig_gs).toFixed(2)) : '';
						net_philhealth_ps = (net_philhealth_ps) ? commaSeparateNumber(parseFloat(net_philhealth_ps).toFixed(2)) : '';
						net_philhealth_gs = (net_philhealth_gs) ? commaSeparateNumber(parseFloat(net_philhealth_gs).toFixed(2)) : '';
						net_consoloan_amount = (net_consoloan_amount) ? commaSeparateNumber(parseFloat(net_consoloan_amount).toFixed(2)) : '';
						net_policy_amount = (net_policy_amount) ? commaSeparateNumber(parseFloat(net_policy_amount).toFixed(2)) : '';
						net_calamity_amount = (net_calamity_amount) ? commaSeparateNumber(parseFloat(net_calamity_amount).toFixed(2)) : '';
						net_lch_loan = (net_lch_loan) ? commaSeparateNumber(parseFloat(net_lch_loan).toFixed(2)) : '';
						net_education_loan_amount = (net_education_loan_amount) ? commaSeparateNumber(parseFloat(net_education_loan_amount).toFixed(2)) : '';
						net_wf_loan = (net_wf_loan) ? commaSeparateNumber(parseFloat(net_wf_loan).toFixed(2)) : '';
						net_wasslai_loan = (net_wasslai_loan) ? commaSeparateNumber(parseFloat(net_wasslai_loan).toFixed(2)) : '';
						net_mwss_loan = (net_mwss_loan) ? commaSeparateNumber(parseFloat(net_mwss_loan).toFixed(2)) : '';
						net_pagibig_two_amount = (net_pagibig_two_amount) ? commaSeparateNumber(parseFloat(net_pagibig_two_amount).toFixed(2)) : '';
						net_multi_purpose_loan = (net_multi_purpose_loan) ? commaSeparateNumber(parseFloat(net_multi_purpose_loan).toFixed(2)) : '';
						net_tax_contribution = (net_tax_contribution) ? commaSeparateNumber(parseFloat(net_tax_contribution).toFixed(2)) : '';
						net_optional_life_amount = (net_optional_life_amount) ? commaSeparateNumber(parseFloat(net_optional_life_amount).toFixed(2)) : '';
						net_sss_cont_amount = (net_sss_cont_amount) ? commaSeparateNumber(parseFloat(net_sss_cont_amount).toFixed(2)) : '';
						net_mpcc_amount = (net_mpcc_amount) ? commaSeparateNumber(parseFloat(net_mpcc_amount).toFixed(2)) : '';
						net_wf_amount = (net_wf_amount) ? commaSeparateNumber(parseFloat(net_wf_amount).toFixed(2)) : '';
						net_wd_amount = (net_wd_amount) ? commaSeparateNumber(parseFloat(net_wd_amount).toFixed(2)) : '';
						net_ud_amount = (net_ud_amount) ? commaSeparateNumber(parseFloat(net_ud_amount).toFixed(2)) : '';
						net_deduction = (net_deduction) ? commaSeparateNumber(parseFloat(net_deduction).toFixed(2)) : '';
						net_pay = (net_pay) ? commaSeparateNumber(parseFloat(net_pay).toFixed(2)) : '';
						net_one_half_net_pay = (net_one_half_net_pay) ? commaSeparateNumber(parseFloat(net_one_half_net_pay).toFixed(2)) : '';
						net_ecc_amount = (net_ecc_amount) ? commaSeparateNumber(parseFloat(net_ecc_amount).toFixed(2)) : '';

						arr += '<tr class="text-center" style="font-weight:bold">';
						arr += '<td></td>';
						arr += '<td colspan="3">Total</td>';
						arr += '<td><b>'+net_salary+'</b></td>';
						arr += '<td>'+net_pera+'</td>';
						arr += '<td>'+net_gross_pay+'</td>';
						arr += '<td>'+net_gsis_ps+'</td>';
						arr += '<td>'+net_gsis_gs+'</td>';
						arr += '<td>'+net_ecc_amount+'</td>';
						arr += '<td>'+net_pagibig_ps+'</td>';
						arr += '<td>'+net_pagibig_gs+'</td>';
						arr += '<td>'+net_philhealth_ps+'</td>';
						arr += '<td>'+net_philhealth_gs+'</td>';
						arr += '<td></td>';
						arr += '<td>'+net_optional_life_amount+'</td>';
						arr += '<td>'+net_consoloan_amount+'</td>';
						arr += '<td>'+net_policy_amount+'</td>';
						arr += '<td>'+net_calamity_amount+'</td>';
						arr += '<td>'+net_lch_loan+'</td>';
						arr += '<td>'+net_education_loan_amount+'</td>';
						arr += '<td></td>';
						arr += '<td>'+net_pagibig_two_amount+'</td>';
						arr += '<td>'+net_wf_amount+'</td>';
						arr += '<td>'+net_wf_loan+'</td>';
						arr += '<td>'+net_wd_amount+'</td>';
						arr += '<td>'+net_wasslai_loan+'</td>';
						arr += '<td></td>';
						arr += '<td>'+net_tax_contribution+'</td>';
						arr += '<td>'+net_mpcc_amount+'</td>';
						arr += '<td>'+net_mwss_loan+'</td>';
						arr += '<td>'+net_ud_amount+'</td>';
						arr += '<td>'+net_multi_purpose_loan+'</td>';
						arr += '<td>'+net_sss_cont_amount+'</td>';
						arr += '<td>'+net_deduction+'</td>';
						arr += '<td>'+net_pay+'</td>';
						arr += '<td>'+net_one_half_net_pay+'</td>';
						arr += '<td></td>';
						arr += '<td></td>';
						arr += '<td></td>';
						arr += '<td>'+net_one_half_net_pay+'</td>';
						arr += '<td></td>';
						arr += '<td></td>';
						arr += '</tr>';

						$('#tbl_body').html(arr);

						days = daysInMonth(_monthNumber,_Year)

						if(_payPeriod == 'monthly'){
							_coveredPeriod = _Month+' 1-'+days+', '+_Year;
						}else{
							switch(_semiPayPeriod){
								case 'firsthalf':
									_coveredPeriod = _Month+' 1-15, '+_Year;
								break;
								default:
									_coveredPeriod =_Month+' 16-'+days+', '+_Year;
								break;
							}
						}

						$('#month_year').text(_coveredPeriod);

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

	function daysInMonth(month, year) {
	    return new Date(year, month, 0).getDate();
	}

	$('#print').on('click',function(){
		$('#reports').printThis();
	})

})
</script>
@endsection