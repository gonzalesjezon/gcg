@extends('layouts.app-monetizationreports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="created_at" id="created_at">
								<option value=""></option>
								@foreach($transaction as $key => $value)
								<option value="{{ $value->created_at->format('Y-m-d') }}">{{ $value->created_at->format('Y-m-d') }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-12 text-left" style="font-weight: bold">
	       				<img src="{{url('/images/e2e_logo_header.png')}}" style="height: 50px;margin-top: 20px;"> <br>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12 text-center">
	       				<h4>GENERAL PAYROLL</h4>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12 text-left">
	       				<h4 style="font-weight: bold">OFFICE/DEPARTMENT: </h4>
	       				<span>Monetization of leave credits for CY <span id="month_year"></span></span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12">
						<table class="table" style="border: 2px solid #5f5f5f">
						<thead class="text-center" style="font-weight: bold;">
							<tr>
								<td></td>
								<td>Name</td>
								<td>Designation</td>
								<td>SG</td>
								<td>Basic Salary</td>
								<td>No. of Days <br> Applied</td>
								<td>Amount</td>
								<td>Land Bank <br> Accnt. No</td>
							</tr>
						</thead>
						 <tbody id="tbl_body">
						 </tbody>
						</table>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12 text-center">
	       				<span>
	       					I hereby certify that this payroll is correct and that	<br>
							the claims herein stated are for services actually	<br>
							and duly rendered as authorized by management.		 <br><br>

							<b>Claudine B. Orocio-Isorena</b>	<br	>
          					Dep. Adm., Administration and Legal
	       				</span>
	       				<br>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 50px;">
	       			<div class="col-md-4" style="padding-left: 30px;">
	       				<span>
		       				Certified:  (1) as to accuracy of computation (2) accounting <br>
							codes and journal entries are proper; (3) properly entered in 	<br>
							the accounting records.
	       				</span>
	       			</div>
	       			<div class="col-md-4 text-center" style="padding-left: 20px;">
	       				<span>
	       					Approved for appropriation and funds being
							available:
	       				</span>
	       			</div>
	       			<div class="col-md-4 text-center" style="padding-left: 20px;">
	       				<span>
	       					<b>APPROVED:</b>
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 30px;">
	       			<div class="col-md-4 text-center">
	       				<b>Theresa V. Makiling</b> <br>
					    Finance Officer <br>
						RO-18-______

	       			</div>
	       			<div class="col-md-4 text-center">
	       				<b>Joriel M. Dagsa</b> <br>
      					Chief Corporate Accountant	<br>

	       			</div>
	       			<div class="col-md-4 text-center">
	       				<b>Patrick Lester N. Ty</b> <br>
               			Chief Regulator

	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option data-month="'+month[m]+'"" value="'+(m+1)+'">'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	var _createdAt;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#created_at',function(){
		_createdAt = "";
		_createdAt = $(this).find(':selected').val();

	})

	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})



	$(document).on('click','#preview',function(){
		if(!_createdAt){
			swal({
				  title: "Select Year, Month, Pay Period First!",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'created_at':_createdAt},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					console.log(data);
					if(data.transaction !== 0){

						arr = [];

						sub_net_amount 	  = 0;
						$.each(data.transaction,function(k,v){
							basic_amount 	 	   = 0;

							firstname = (v.employees.firstname !== null) ? v.employees.firstname : '';
							lastname = (v.employees.lastname !== null) ? v.employees.lastname : '';
							middlename = (v.employees.middlename !== null) ? v.employees.middlename : '';
							positions = (v.positions !== null) ? v.positions.name : '';
							salarygrade = (v.salarygrade !== null) ? v.salarygrade.salary_grade : '';
							salary_amount = (v.salaryinfo !== null) ? v.salaryinfo.salary_new_rate : 0;
							number_of_days = (v.number_of_days !== null) ? v.number_of_days : 0;
							net_amount = (v.net_amount !== null) ? v.net_amount : 0;

							sub_net_amount += parseFloat(net_amount);



							salary_amount = (salary_amount !== 0) ? commaSeparateNumber(parseFloat(salary_amount).toFixed(2)) : '';
							net_amount = (net_amount !== 0) ? commaSeparateNumber(parseFloat(net_amount).toFixed(2)) : '';

							arr += '<tr>';
							arr += '<td>'+(parseInt(k)+1)+'</td>';
							arr += '<td class="text-left">'+lastname+' '+firstname+' '+middlename+'</td>';
							arr += '<td class="text-left">'+positions+'</td>';
							arr += '<td class="text-center">'+salarygrade+'</td>';
							arr += '<td class="text-right">'+salary_amount+'</td>';
							arr += '<td class="text-center">'+number_of_days+'</td>';
							arr += '<td class="text-right">'+net_amount+'</td>';
							arr += '<td class="text-right"></td>';
							arr += '</tr>';
						});

						sub_net_amount = (sub_net_amount !== 0) ? commaSeparateNumber(parseFloat(sub_net_amount).toFixed(2)) : '';

						arr += '<tr style="font-weight:bold">';
						arr += '<td></td>';
						arr += '<td class="text-left">TOTAL</td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-right"></td>';
						arr += '<td class="text-center"></td>';
						arr += '<td class="text-right"></td>';
						arr += '<td class="text-right">'+sub_net_amount+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '</tr>';


						$('#tbl_body').html(arr);

						// days = daysInMonth(_monthNumber,_Year);

						// if(_payPeriod == 'monthly'){
						// 	_coveredPeriod = _Month+' 1-'+days+', '+_Year;
						// }else{
						// 	switch(_semiPayPeriod){
						// 		case 'firsthalf':
						// 			_coveredPeriod = _Month+' 1-15, '+_Year;
						// 		break;
						// 		default:
						// 			_coveredPeriod =_Month+' 16-'+days+', '+_Year;
						// 		break;
						// 	}
						// }
						year = new Date();

						$('#month_year').text(year.getFullYear());

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
$('#print').on('click',function(){
	$('#reports').printThis();
})
})
</script>
@endsection