@extends('layouts.app-nonplantillareports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Pay Period</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select id="pay_period" class="form-control font-style2 select2" name="pay_period">
								<option value=""></option>
								<option value="semimonthly">Semi Monthly</option>
								<option value="monthly">Monthly</option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-6">
	       				<img src="{{url('/images/e2e_logo_header.png')}}" style="height: 50px;margin-top: 20px;">
	       				<h5><b>{{ $title }}</b></h5>
	       				<h6><b>Covered Period:</b> <span id="month_year"></span></h6>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12">
	       				<table class="table">
							<thead class="text-center" style="font-weight: bold;">
								 <tr>
								 	<td rowspan="2"style="line-height: 20px;">#</td>
								 	<td rowspan="2"style="line-height: 20px;">Name</td>
								 	<td rowspan="2"style="line-height: 20px;">Monthly Compensation</td>
								 	<td rowspan="2"style="line-height: 20px;">1/2 Monthly Compensation</td>
								 	<td rowspan="2"style="line-height: 20px;">Less (Undertime/Absences)</td>
								 	<td rowspan="2"style="line-height: 20px;">Compensation for the period</td>
								 	<td colspan="2" >LESS ITW</td>
								 	<td rowspan="2"style="line-height: 20px;">Tax Withheld</td>
								 	<td rowspan="2"style="line-height: 20px;">NET</td>
								 	<td rowspan="2"style="line-height: 20px;">Land Bank Account No.</td>
								 </tr>
								 <tr>
								 	<td><span id="percent_one">412-3 (5/10%)</span></td>
								 	<td><span id="percent_two">412-5(3%)</span></td>
								 </tr>
							</thead>
							<tfoot>
							 	<tr class="text-right">
							 		<td></td>
							 		<td><b>TOTAL</b></td>
							 		<td><b><span id="total_mca"></span></b></td>
							 		<td><b><span id="total_half_monthly_compensation_amount"></span></b></td>
							 		<td><b><span id="total_less_amount"></span></b></td>
							 		<td><b><span id="total_compensation_amount"></span></b></td>
							 		<td></td>
							 		<td><b><span id="total_percentTwo"></span></b></td>
							 		<td><b><span id="total_tax_wtihheld"></span></b></td>
							 		<td><b><span id="total_net_amount"></span></b></td>
							 		<td></td>
							 	</tr>
							 	<tr>
							 		<td colspan="4"  style="border: none">Prepared By</td>
							 		<td colspan="4"  style="border: none">Verified By</td>
							 		<td colspan="4"  style="border: none">Certified Correct</td>
							 	</tr>
							 	<tr>
							 		<td class="text-left" colspan="4" style="border: none">
							 			<b><span>Theresa V. Makiling</span></b> <br>
							 			<span>Finance Officer</span>
							 		</td>
							 		<td class="text-left" colspan="4"  style="border: none">
							 			<b><span>Marie Claudeline M. Tenorio</span></b> <br>
							 			<span>IRM Officer </span>
							 		</td>
							 		<td class="text-left" colspan="4"  style="border: none">
							 			<b><span>IRM Officer </span></b> <br>
							 			<span>Chief Corporate Accountant</span>
							 		</td>
							 	</tr>
							</tfoot>
							<tbody id="tbl_body">
							</tbody>
					</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option data-month="'+month[m]+'"" value="'+(m+1)+'">'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	var _payPeriod;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});
	$('#select_pay_period').select2({
		allowClear:true,
	    placeholder: "Pay Period",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	var _monthNumber;
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').data('month');
		_monthNumber = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});

		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getPayrollWorksheetReport',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				console.log(data);
					if(data.length !== 0){
						arr = [];
						var percentOne;
						var percentTwo;
						var	total_monthly_compensation_amount = 0;
						var total_half_monthly_compensation_amount = 0;
						var total_less_amount = 0;
						var total_compensation_amount = 0;
						var total_percentTwo = 0;
						var total_net_amount = 0;
						$.each(data.transaction,function(k,v){
							fullname = v.employees.lastname +' '+v.employees.firstname+' '+v.employees.middlename;
							monthly_compensation_amount =  (v.actual_basicpay) ? v.actual_basicpay*2 : 0;
							half_monthly_compensation_amount =  (v.actual_basicpay) ? v.actual_basicpay : 0;

							total_monthly_compensation_amount += parseFloat(monthly_compensation_amount);
							total_half_monthly_compensation_amount += parseFloat(half_monthly_compensation_amount);

							absence = (v.total_absences !== 0) ? v.total_absences : 0;
							undertime = (v.total_undertime !== 0) ? v.total_undertime : 0;
							less_amount = (absence + undertime);
							total_less_amount += parseFloat(less_amount);

							taxRateOne = (v.employeeinfo.taxpolicy_id !== null) ? v.employeeinfo.taxpolicies_one.job_grade_rate : 0;
							taxAmountTwo = (v.tax_rate_amount_two !== null) ? v.tax_rate_amount_two : 0;
							less_amount = (less_amount) ? less_amount : 0;

							compensation_amount = (parseFloat(half_monthly_compensation_amount) - parseFloat(less_amount));
							total_compensation_amount += parseFloat(compensation_amount);
							// 3 % percent tax computation
							taxAmountOne = (half_monthly_compensation_amount*taxRateOne);
							total_percentTwo += parseFloat(taxAmountOne);

							totalTaxAmount = parseFloat(taxAmountOne + parseFloat(taxAmountTwo));

							net_amount = (half_monthly_compensation_amount - totalTaxAmount);

							total_net_amount += parseFloat(net_amount);
							atm_number = (v.employeeinfo.atm_no !== null) ? v.employeeinfo.atm_no : '';

							monthly_compensation_amount = (monthly_compensation_amount) ? commaSeparateNumber(parseInt(monthly_compensation_amount).toFixed(2)) : '';
							half_monthly_compensation_amount = (half_monthly_compensation_amount) ? commaSeparateNumber(parseInt(half_monthly_compensation_amount).toFixed(2)) : '';
							less_amount = (less_amount) ? commaSeparateNumber(parseInt(less_amount).toFixed(2)) : '';
							compensation_amount = (compensation_amount) ? commaSeparateNumber(parseInt(compensation_amount).toFixed(2)) : '';
							taxAmountOne = (taxAmountOne) ? commaSeparateNumber(parseInt(taxAmountOne).toFixed(2)) : '';
							taxAmountTwo = (taxAmountTwo) ? commaSeparateNumber(parseInt(taxAmountTwo).toFixed(2)) : '';
							totalTaxAmount = (totalTaxAmount) ? commaSeparateNumber(parseInt(totalTaxAmount).toFixed(2)) : '';
							net_amount = (net_amount) ? commaSeparateNumber(parseInt(net_amount).toFixed(2)) : '';

							arr += '<tr>';
							arr += '<td>'+(k+1)+'</td>';
							arr += '<td>'+fullname+'</td>';
							arr += '<td class="text-right">'+monthly_compensation_amount+'</td>';
							arr += '<td class="text-right">'+half_monthly_compensation_amount+'</td>';
							arr += '<td class="text-right">'+less_amount+'</td>';
							arr += '<td class="text-right">'+compensation_amount+'</td>';
							arr += '<td class="text-right">'+taxAmountOne+'</td>';
							arr += '<td class="text-right">'+taxAmountTwo+'</td>';
							arr += '<td class="text-right">'+totalTaxAmount+'</td>';
							arr += '<td class="text-right">'+net_amount+'</td>';
							arr += '<td>'+atm_number+'</td>';

							arr += '</tr>';

						});

						total_monthly_compensation_amount = (total_monthly_compensation_amount) ? commaSeparateNumber(parseFloat(total_monthly_compensation_amount).toFixed(2))  : '';

						total_half_monthly_compensation_amount = (total_half_monthly_compensation_amount) ? commaSeparateNumber(parseFloat(total_half_monthly_compensation_amount).toFixed(2))  : '';

						total_less_amount = (total_less_amount) ? commaSeparateNumber(parseFloat(total_less_amount).toFixed(2))  : '';

						total_compensation_amount = (total_compensation_amount) ? commaSeparateNumber(parseFloat(total_compensation_amount).toFixed(2))  : '';

						total_percentTwo = (total_percentTwo) ? commaSeparateNumber(parseFloat(total_percentTwo).toFixed(2))  : '';
						total_net_amount = (total_net_amount) ? commaSeparateNumber(parseFloat(total_net_amount).toFixed(2))  : '';

						$('#total_mca').text(total_monthly_compensation_amount);
						$('#total_half_monthly_compensation_amount').text(total_half_monthly_compensation_amount);
						$('#total_less_amount').text(total_less_amount);
						$('#total_compensation_amount').text(total_compensation_amount);
						$('#total_tax_wtihheld').text(total_percentTwo);
						$('#total_net_amount').text(total_net_amount);

						$('#tbl_body').html(arr);
						days = daysInMonth(_monthNumber,_Year)

						if(_payPeriod == 'monthly'){
							_coveredPeriod = _Month+' 1-'+days+', '+_Year;
						}else{
							switch(_semiPayPeriod){
								case 'firsthalf':
									_coveredPeriod = _Month+' 1-15, '+_Year;
								break;
								default:
									_coveredPeriod =_Month+' 16-'+days+', '+_Year;
								break;
							}
						}

						$('#month_year').text(_coveredPeriod);
						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			});
		}
	});

	function daysInMonth(month, year) {
	    return new Date(year, month, 0).getDate();
	}

	$('#print').on('click',function(){
		$('#reports').printThis();
	})
})
</script>
@endsection