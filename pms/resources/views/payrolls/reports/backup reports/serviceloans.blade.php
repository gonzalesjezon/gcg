@extends('app-reports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-12 text-center" style="font-weight: bold">
	       				<img src="{{url('/images/e2e_logo_header.png')}}" style="height: 50px;margin-top: 20px;"> <br>
	       				Room 304 URC Bldg, 2123  Espana Blvd Brgy 513, Sampaloc Manila<br>
						For the month of <span id="pay_period"></span>		<br>	<br>
						Remittance of Service Loan Accounts
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12">
	       				<table class="table">
							<thead>
								<tr class="text-center" style="font-weight: bold;">
								<td rowspan="2" >No</td>
								<td rowspan="2" >Name</td>
								<td rowspan="2" >BP No</td>
								<td colspan="6" >Repayment on Service Loan Accounts</td>
								<td rowspan="2">Sub Total</td>
								<td rowspan="2">Remarks</td>
								</tr>
								<tr class="text-center" style="font-weight: bold">
									<td>Opt. Policy Loan</td>
									<td>Conso Loan</td>
									<td>Policy Loan</td>
									<td>Emergency Loan</td>
									<td>LCH-DCS</td>
									<td>Education Loan</td>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<td style="border:none"></td>
									<td colspan="3" class="text-center" style="border:none">Prepared  by:</td>
									<td colspan="3" class="text-center" style="border:none"> Checked & Verified:</td>
									<td colspan="4" class="text-center" style="border:none">Approved:</td>
								</tr>
								<tr>
									<td style="border:none"></td>
									<td colspan="3" class="text-center" style="border:none">
										<b>Theresa V. Makiling </b> <br>
										 Finance Office
									</td>
									<td colspan="3" class="text-center" style="border:none">
										<b>Joriel M. Dagsa</b> <br>
										 Chief Corporate Accountant
									</td>
									<td colspan="4" class="text-center" style="border:none">
										<b> Virginia V. Octa</b> <br>
										 Manager, Administration Dept.
									</td>
								</tr>
							</tfoot>
							 <tbody id="tbl_body">
							 </tbody>
						</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){
		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					console.log(data);
					if(data.length !== 0){


						arr = [];
						ctr = data.loans.length;


						sub_total_consoloan_amount 			= 0;
						sub_total_policy_amount 			= 0;
						sub_total_optional_life_amount 		= 0;
						sub_total_education_loan_amount 	= 0;
						sub_total_calamity_amount 			= 0;
						sub_total_lch_amount 				= 0;
						sub_total_loan 						= 0;


						$.each(data.transactions,function(k,v){
							consoloan_amount 		= 0;
							policy_amount 	 		= 0;
							optional_life_amount 	= 0;
							education_loan_amount 	= 0;
							calamity_amount 		= 0;
							lch_amount 				= 0;
							total_loan 				= 0;


							fullname = v.employees.lastname +' '+ v.employees.firstname +' '+ v.employees.middlename;

							if(v.employeeinfo.loaninfo.length !== 0){
								$.each(v.employeeinfo.loaninfo,function(key,val){
									if(val.loans.name){
										switch(val.loans.name){
											case 'CONSOLOAN':
												consoloan_amount = (val.loan_amortization) ? val.loan_amortization : 0
											break;
											case 'POLICY LOAN':
												policy_amount = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
											case 'OPTIONAL LIFE':
												optional_life_amount = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
											case 'EDUCATION LOAN':
												education_loan_amount = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
											case 'EMERGENCY/CALAMITY LOAN':
												calamity_amount = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
											case 'LCH-DCS':
												lch_amount = (val.loan_amortization) ? val.loan_amortization : 0 ;
											break;
										}
									}
								})
							}

							sub_total_consoloan_amount += parseFloat(consoloan_amount);
							sub_total_policy_amount += parseFloat(policy_amount);
							sub_total_optional_life_amount += parseFloat(optional_life_amount);
							sub_total_education_loan_amount += parseFloat(education_loan_amount);
							sub_total_calamity_amount += parseFloat(calamity_amount);
							sub_total_lch_amount += parseFloat(lch_amount);

							total_loan = (parseFloat(consoloan_amount) + parseFloat(policy_amount) + parseFloat(optional_life_amount) + parseFloat(education_loan_amount) + parseFloat(calamity_amount) + parseFloat(lch_amount))

							sub_total_loan += parseFloat(total_loan);

							consoloan_amount = (consoloan_amount !== 0) ? commaSeparateNumber(parseFloat(consoloan_amount).toFixed(2)) : '';
							policy_amount = (policy_amount !== 0) ? commaSeparateNumber(parseFloat(policy_amount).toFixed(2)) : '';
							optional_life_amount = (optional_life_amount !== 0) ? commaSeparateNumber(parseFloat(optional_life_amount).toFixed(2)) : '';
							education_loan_amount = (education_loan_amount !== 0) ? commaSeparateNumber(parseFloat(education_loan_amount).toFixed(2)) : '';
							calamity_amount = (calamity_amount !== 0) ? commaSeparateNumber(parseFloat(calamity_amount).toFixed(2)) : '';
							lch_amount = (lch_amount !== 0) ? commaSeparateNumber(parseFloat(lch_amount).toFixed(2)) : '';
							total_loan = (total_loan !== 0) ? commaSeparateNumber(parseFloat(total_loan).toFixed(2)) : '';
							arr += '<tr class="text-right">';
							arr += '<td class="text-left">'+(parseInt(k)+1)+'</td>';
							arr += '<td class="text-left">'+fullname+'</td>';
							arr += '<td></td>';
							arr += '<td>'+optional_life_amount+'</td>';
							arr += '<td>'+consoloan_amount+'</td>';
							arr += '<td>'+policy_amount+'</td>';
							arr += '<td>'+calamity_amount+'</td>';
							arr += '<td>'+lch_amount+'</td>';
							arr += '<td>'+education_loan_amount+'</td>';
							arr += '<td>'+total_loan+'</td>';
							arr += '<td></td>';
							arr += '</tr>';
						})
						sub_total_loan = (sub_total_loan !== 0) ? commaSeparateNumber(parseFloat(sub_total_loan).toFixed(2)) : '';
						sub_total_lch_amount = (sub_total_lch_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_lch_amount).
							toFixed(2)) : '';
						sub_total_calamity_amount = (sub_total_calamity_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_calamity_amount).toFixed(2)) : '';
						sub_total_consoloan_amount = (sub_total_consoloan_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_consoloan_amount).toFixed(2)) : '';
						sub_total_policy_amount = (sub_total_policy_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_policy_amount).toFixed(2)) : '';
						sub_total_education_loan_amount = (sub_total_education_loan_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_education_loan_amount).toFixed(2)) : '';
						sub_total_optional_life_amount = (sub_total_optional_life_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_optional_life_amount).toFixed(2)) : '';
						arr += '<tr class="text-right">';
						arr += '<td></td>';
						arr += '<td class="text-left"><b>Total</b></td>';
						arr += '<td></td>';
						arr += '<td><b>'+sub_total_optional_life_amount+'</b></td>';
						arr += '<td><b>'+sub_total_consoloan_amount+'</b></td>';
						arr += '<td><b>'+sub_total_policy_amount+'</b></td>';
						arr += '<td><b>'+sub_total_calamity_amount+'</b></td>';
						arr += '<td><b>'+sub_total_lch_amount+'</b></td>';
						arr += '<td><b>'+sub_total_education_loan_amount+'</b></td>';
						arr += '<td><b>'+sub_total_loan+'</b></td>';
						arr += '<td></td>';
						arr += '</tr>';

						$('#tbl_body').html(arr);

						$('#pay_period').text(_Month+' '+_Year)

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

$('#print').on('click',function(){
	$('#reports').printThis();
})
})
</script>
@endsection