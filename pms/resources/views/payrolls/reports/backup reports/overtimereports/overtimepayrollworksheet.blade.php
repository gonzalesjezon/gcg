@extends('layouts.app-overtimereports')


@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css') }}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Pay Period</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select id="pay_period" class="form-control font-style2 select2" name="pay_period">
								<option value=""></option>
								<option value="semimonthly">Semi Monthly</option>
								<!-- <option value="monthly">Monthly</option> -->
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control font-style2 hidden" id="semi_pay_period" name="semi_pay_period">
								<option value=""></option>
								<option value="firsthalf">First Half</option>
								<option value="secondhalf">Second Half</option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 100%;height: 100%;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-12 text-left" style="font-weight: bold">
	       				<img src="{{url('/images/e2e_logo_header.png')}}" style="height: 50px;margin-top: 20px;"> <br>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12 text-center">
	       				<h4>PAYROLL WORKSHEET</h4>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12 text-left">
	       				<span>Payment of salaries and allowances for the period <span id="month_year"></span></span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12">
						<table class="table" style="border: 2px solid #5f5f5f">
						<thead class="text-center" style="font-weight: bold;">
							<tr>
								<td rowspan="2"></td>
								<td rowspan="2">Name</td>
								<td rowspan="2">Designation</td>
								<td rowspan="2">SG</td>
								<td rowspan="2">Basic Salary</td>
								<td rowspan="2">Period Covered</td>
								<td colspan="6">HRS</td>
								<td rowspan="2">Total</td>
								<td rowspan="2">ITW</td>
								<td rowspan="2">Net Amount</td>
							</tr>
							<tr>
								<td>Reg. OT Rate</td>
								<td>OT Hr.</td>
								<td>Amount</td>
								<td>Sat/Sun</td>
								<td>OT Hr.</td>
								<td>Amount</td>
							</tr>
						</thead>
						 <tbody id="tbl_body">
						 </tbody>
						</table>
	       			</div>
	       		</div>
<!-- 	       		<div class="row">
	       			<div class="col-md-12 text-center">
	       				<span>
	       					I hereby certify that this payroll is correct and that	<br>
							the claims herein stated are for services actually	<br>
							and duly rendered as authorized by management.		 <br><br>

							<b>Claudine B. Orocio-Isorena</b>	<br	>
          					Dep. Adm., Administration and Legal
	       				</span>
	       				<br>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 50px;">
	       			<div class="col-md-4" style="padding-left: 30px;">
	       				<span>
		       				Certified:  (1) as to accuracy of computation (2) accounting <br>
							codes and journal entries are proper; (3) properly entered in 	<br>
							the accounting records.
	       				</span>
	       			</div>
	       			<div class="col-md-4 text-center" style="padding-left: 20px;">
	       				<span>
	       					Approved for appropriation and funds being
							available:
	       				</span>
	       			</div>
	       			<div class="col-md-4 text-center" style="padding-left: 20px;">
	       				<span>
	       					<b>APPROVED:</b>
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 30px;">
	       			<div class="col-md-4 text-center">
	       				<b>Theresa V. Makiling</b> <br>
					    Finance Officer <br>
						RO-18-______

	       			</div>
	       			<div class="col-md-4 text-center">
	       				<b>Joriel M. Dagsa</b> <br>
      					Chief Corporate Accountant	<br>

	       			</div>
	       			<div class="col-md-4 text-center">
	       				<b>Patrick Lester N. Ty</b> <br>
               			Chief Regulator

	       			</div>
	       		</div> -->
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option data-month="'+month[m]+'"" value="'+(m+1)+'">'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	var _monthNumber;
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').data('month');
		_monthNumber = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})

	var _payPeriod;
	var _semiPayPeriod;

	$(document).on('change','#pay_period',function(){
		_payPeriod = $(this).find(':selected').val();
		switch(_payPeriod){
			case 'semimonthly':
				$('#semi_pay_period').removeClass('hidden');
			break;
			default:
				$('#semi_pay_period').addClass('hidden');
			break;
		}
	});

	$(document).on('change','#semi_pay_period',function(){
		_semiPayPeriod = $(this).find(':selected').val();
	})



	$(document).on('click','#preview',function(){

		if(!_Year && !_Month && !_payPeriod || !_semiPayPeriod){
			swal({
				  title: "Select Year, Month, Pay Period First!",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
					console.log(data);
					if(data.transaction !== 0){

						arr = [];
						sub_total_basic_amount 		  = 0;
						sub_total_ot_hr_amount		  = 0;
						sub_total_ot_hr 			  = 0;
						sub_total_reg_ot_rate 		  = 0;
						sub_total_hs_hr 			  = 0;
						sub_total_hs_amount 		  = 0;
						sub_total 					  = 0;

						$.each(data.transaction,function(k,v){
							basic_amount 	 	   = 0;
							net_amount 			   = 0;
							firstname = (v.employees.firstname !== null) ? v.employees.firstname : '';
							lastname = (v.employees.lastname !== null) ? v.employees.lastname : '';
							middlename = (v.employees.middlename !== null) ? v.employees.middlename : '';
							positions = (v.employeeinformation.positions !== null) ? v.employeeinformation.positions.name : '';
							salarygrade = (v.salaryinfo.salarygrade !== null) ? v.salaryinfo.salarygrade.salary_grade : '';
							basic_amount = (v.salaryinfo !== null) ? v.salaryinfo.salary_new_rate : 0;
							ot_hr = (v.actual_regular_overtime !== null) ? v.actual_regular_overtime : 0;
							ot_hr_amount = (v.total_regular_amount !== null) ? v.total_regular_amount : 0;
							holiday_hr =(v.actual_regular_holiday_overtime !== null) ? v.actual_regular_holiday_overtime : 0;
							holiday_hr_amount =(v.total_regular_holiday_amount !== null) ? v.total_regular_holiday_amount : 0;
							special_hr = (v.actual_special_overtime !== null) ? v.actual_special_overtime : 0;
							special_hr_amount =(v.total_special_amount !== null) ? v.total_special_amount : 0;

							total_hs_hr = (parseFloat(holiday_hr) + parseFloat(special_hr));
							total_hs_amount = (parseFloat(holiday_hr_amount) + parseFloat(special_hr_amount));

							total = (parseFloat(ot_hr_amount) + parseFloat(total_hs_amount));

							reg_ot_rate = (parseFloat(basic_amount)/22/8);

							sub_total_basic_amount += parseFloat(basic_amount);
							sub_total_ot_hr_amount += parseFloat(ot_hr_amount);
							sub_total_ot_hr += parseFloat(ot_hr);
							sub_total_reg_ot_rate += parseFloat(reg_ot_rate);
							sub_total_hs_hr += parseFloat(total_hs_hr);
							sub_total_hs_amount += parseFloat(total_hs_amount);
							sub_total += parseFloat(total);

							basic_amount = (basic_amount !== 0) ? commaSeparateNumber(parseFloat(basic_amount).toFixed(2)) : '';
							ot_hr_amount = (ot_hr_amount !== 0) ? commaSeparateNumber(parseFloat(ot_hr_amount).toFixed(2)) : '';
							reg_ot_rate = (reg_ot_rate !== 0) ? commaSeparateNumber(parseFloat(reg_ot_rate).toFixed(2)) : '';
							total_hs_hr = (total_hs_hr !== 0) ? commaSeparateNumber(parseFloat(total_hs_hr).toFixed(2)) : '';
							total_hs_amount = (total_hs_amount !== 0) ? commaSeparateNumber(parseFloat(total_hs_amount).toFixed(2)) : '';
							total = (total !== 0) ? commaSeparateNumber(parseFloat(total).toFixed(2)) : '';


							arr += '<tr>';
							arr += '<td>'+(parseInt(k)+1)+'</td>';
							arr += '<td class="text-left">'+lastname+' '+firstname+' '+middlename+'</td>';
							arr += '<td class="text-left">'+positions+'</td>';
							arr += '<td class="text-center">'+salarygrade+'</td>';
							arr += '<td class="text-right">'+basic_amount+'</td>';
							arr += '<td class="text-right"></td>'; //covered period
							arr += '<td class="text-right">'+reg_ot_rate+'</td>'; // ot rate
							arr += '<td class="text-right">'+ot_hr+'</td>';
							arr += '<td class="text-right">'+ot_hr_amount+'</td>';
							arr += '<td class="text-right">'+reg_ot_rate+'</td>'; // ot rate
							arr += '<td class="text-right">'+total_hs_hr+'</td>';
							arr += '<td class="text-right">'+total_hs_amount+'</td>';
							arr += '<td class="text-right">'+total+'</td>';
							arr += '<td class="text-right"></td>';
							arr += '<td class="text-right">'+total+'</td>';
							arr += '</tr>';
						});

						sub_total_basic_amount = (sub_total_basic_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_basic_amount).toFixed(2)) : '';
						sub_total_ot_hr_amount = (sub_total_ot_hr_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_ot_hr_amount).toFixed(2)) : '';
						sub_total_ot_hr = (sub_total_ot_hr !== 0) ? commaSeparateNumber(parseFloat(sub_total_ot_hr).toFixed(2)) : '';
						sub_total_reg_ot_rate = (sub_total_reg_ot_rate !== 0) ? commaSeparateNumber(parseFloat(sub_total_reg_ot_rate).toFixed(2)) : '';
						sub_total_hs_hr = (sub_total_hs_hr !== 0) ? commaSeparateNumber(parseFloat(sub_total_hs_hr).toFixed(2)) : '';
						sub_total_hs_amount = (sub_total_hs_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_hs_amount).toFixed(2)) : '';
						sub_total = (sub_total !== 0) ? commaSeparateNumber(parseFloat(sub_total).toFixed(2)) : '';

						arr += '<tr style="font-weight:bold;">';
						arr += '<td></td>';
						arr += '<td class="text-left">Total</td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-left"></td>';
						arr += '<td class="text-right">'+sub_total_basic_amount+'</td>';
						arr += '<td class="text-right"></td>'; //covered period
						arr += '<td class="text-right">'+sub_total_reg_ot_rate+'</td>'; // ot rate
						arr += '<td class="text-right">'+sub_total_ot_hr+'</td>'; // ot hr
						arr += '<td class="text-right">'+sub_total_ot_hr_amount+'</td>';
						arr += '<td class="text-right">'+sub_total_reg_ot_rate+'</td>'; // ot rate
						arr += '<td class="text-right">'+sub_total_hs_hr+'</td>'; // ot rate
						arr += '<td class="text-right">'+sub_total_hs_amount+'</td>'; // ot rate
						arr += '<td class="text-right">'+sub_total+'</td>'; // ot rate
						arr += '<td class="text-right"></td>'; // ot rate
						arr += '<td class="text-right">'+sub_total+'</td>'; // ot rate
						arr += '</tr>';



						$('#tbl_body').html(arr);

						days = daysInMonth(_monthNumber,_Year);

						if(_payPeriod == 'monthly'){
							_coveredPeriod = _Month+' 1-'+days+', '+_Year;
						}else{
							switch(_semiPayPeriod){
								case 'firsthalf':
									_coveredPeriod = _Month+' 1-15, '+_Year;
								break;
								default:
									_coveredPeriod =_Month+' 16-'+days+', '+_Year;
								break;
							}
						}

						$('#month_year').text(_coveredPeriod);

						$('#btnModal').trigger('click');
					}else{
						swal({
							  title: "No Records Found",
							  type: "warning",
							  showCancelButton: false,
							  confirmButtonClass: "btn-danger",
							  confirmButtonText: "Yes",
							  closeOnConfirm: false

						});
					}



				}
			})
		}


	});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection