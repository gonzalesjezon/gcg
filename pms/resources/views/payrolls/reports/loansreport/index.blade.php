@extends('app-reports')

@section('reports-content')

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td ><span><b>Employee Name</b></span></td>
			<td colspan="3">
				<select class="form-control" name="employee_id" id="employee_id">
					<option value=""></option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="4"><span><b>Filtered By</b></span></td>
		</tr>
		<tr>
			<td><span class="lineheight"><b>Covered Date</b></span></td>
			<td><input type="text" name="date_from" id="date_from" class="form-control"></td>
			<td colspan="2"><input type="text" name="date_to" id="date_to" class="form-control"></td>
		</tr>
		<tr>
			<td ><span class="lineheight"><b>Report Option</b></span></td>
			<td colspan="3">
				<span class="padding01">
					<input type="radio" name="rd_option" id="rd_male">
					<span> Male</span>
				</span>
				<span class="padding01">
					<input type="radio" name="rd_option" id="rd_both">
					<span>Both</span>
				</span>
				<span class="padding01"><input type="radio" name="rd_option" id="rd_female">
					<span>Female</span>
				</span>
			</td>
		</tr>
		<tr>
			<td ><span class="lineheight"><b>Employee Status</b></span></td>
			<td colspan="3">
				<span class="padding01">
					<input type="radio" name="rd_option" id="rd_active">
					<span> Active</span>
				</span>
				<span class="padding01">
					<input type="radio" name="rd_option" id="rd_inactive">
					<span>Inactive</span>
				</span>
				<span class="padding01"><input type="radio" name="rd_option" id="rd_all">
					<span>All</span>
				</span>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<a class="btn btn-danger btn-xs">
				Preview
			</a>
		</div>
		
	</div>
</div>

@endsection

@section('js-logic1')
<script type="text/javascript">
	
</script>
@endsection