@extends('app-othercompensations')

@section('othercompensations-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<style type="text/css">
	.table2>thead>tr>td, .table2>tbody>tr>td{
	    padding: 3px !important;
	 }
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes.covereddate')
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Left</span>
							<select class="form-control font-style2 select2" id="sign_top_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Mid One</span>
							<select class="form-control font-style2 select2" id="sign_top_mid_one">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_mid_one="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Mid Two</span>
							<select class="form-control font-style2 select2" id="sign_top_mid_two">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_mid_two="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<span>Signatory Top Right One</span>
							<select class="form-control font-style2 select2" id="sign_top_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<span>Degree</span>
							<input type="text" name="degree" id="degree" class="form-control font-style2">
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Right Two</span>
							<select class="form-control font-style2 select2" id="sign_top_right_two">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_right_two="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="margin-right: -5px;margin-left: -5px;">
	       			<div class="col-md-12" id="payroll_transfer">
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){

// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$('#select_year').trigger('change');
	$('#select_month').trigger('change');

	var signTopLeft;
	var positionTopLeft;
	$('#sign_top_left').change(function(){
		signTopLeft = "";
		positionTopLeft  = "";
		signTopLeft = $(this).find(':selected').text()
		positionTopLeft = $(this).find(':selected').data('position_top_left')
	});

	var signTopMidOne;
	var positionTopMidOne;
	$('#sign_top_mid_one').change(function(){
		signTopMidOne = "";
		positionTopMidOne  = "";
		signTopMidOne = $(this).find(':selected').text()
		positionTopMidOne = $(this).find(':selected').data('position_top_mid_one')
	});

	var signTopMidTwo;
	var positionTopMidTwo;
	$('#sign_top_mid_two').change(function(){
		signTopMidTwo = "";
		positionTopMidTwo  = "";
		signTopMidTwo = $(this).find(':selected').text()
		positionTopMidTwo = $(this).find(':selected').data('position_top_mid_two')
	});

	var signTopRight;
	var positionTopRight;
	$('#sign_top_right').change(function(){
		signTopRight = "";
		positionTopRight  = "";
		signTopRight = $(this).find(':selected').text()
		positionTopRight = $(this).find(':selected').data('position_top_right')
	});

	var signTopRightTwo;
	var positionTopRightTwo;
	$('#sign_top_right_two').change(function(){
		signTopRightTwo = "";
		positionTopRightTwo  = "";
		signTopRightTwo = $(this).find(':selected').text()
		positionTopRightTwo = $(this).find(':selected').data('position_top_right_two')
	});

	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	var degree;
	$(document).on('keyup','#degree',function(){
		degree = "";
		degree = $(this).val();

	});

	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});

		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getEmployeeinfo',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
							body = [];
							ctr = 1;
							netCtr = 0;
							netTotalRata 		= 0;
							netRaAmount 	 	= 0;
							netTaAmount 	 	= 0;
							netRaDiffAmount 	= 0;
							netTaDiffAmount 	= 0;
							netReleaseAmount 	= 0;


							coveredPeriod = months[_Month]+' '+_Year;

							leaveCount = parseInt(_Month) - 1;

							if(leaveCount > 1){
								leave_month = months[leaveCount];
							}else{
								leave_month = months[1];
							}


							body += '<table class="table table2" style="border:none;">'
							body += '<thead class="text-center" style="font-weight: bold;">';
							body += '<tr>';
							body += '<td class="text-center" colspan="15" style="border:none !important;font-weight:bold;">PHILIPPINE COMPETITION COMMISSION <br> REPRESENTATION AND TRANSPORTATION ALLOWANCE </td>';
							body += '</tr>';
							body += '<tr>';
							body += '<td colspan="15" class="text-left border-left-0 border-right-0 border-top-0">Pay Period: '+coveredPeriod+'</td>';
							body += '</tr>';
							body += '<tr>';
							body += '<td rowspan="2" style="vertical-align:middle;">#</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">EMPLOYEE NAME</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">DESIGNATION</td>';
							body += '<td rowspan="2" style="vertical-align:middle;"  >REPRESENTATION</td>';
							body += '<td rowspan="2" style="vertical-align:middle;"  >TRANSPORTATION</td>';
							body += '<td rowspan="2" style="vertical-align:middle;"  >LEAVE FILED <br> ('+leave_month+' '+_Year+')</td>';
							body += '<td rowspan="2" style="vertical-align:middle;"  >NO. OF WORKDAYS OF ACTUAL WORK PERFORMANCE THE PREVIOUS MONTH</td>';
							body += '<td rowspan="2" style="vertical-align:middle;"  >PERCENTAGE OF ACTUAL RATA</td>';
							body += '<td colspan="2" style="vertical-align:middle;" >DIFFERENTIAL</td>';
							body += '<td colspan="3" style="vertical-align:middle;" >NET</td>';
							body += '<td rowspan="3" style="vertical-align:middle;" >FOR RELEASE</td>';
							body += '<td rowspan="3" style="vertical-align:middle;" >REMARKS</td>';
							body += '</tr>';
							body += '<tr>';
							body += '<td style="vertical-align:middle;">REPRESENTATION</td>'
							body += '<td style="vertical-align:middle;">TRANSPORTATION</td>'
							body += '<td style="vertical-align:middle;">REPRESENTATION</td>'
							body += '<td style="vertical-align:middle;">TRANSPORTATION</td>'
							body += '<td style="vertical-align:middle;">TOTAL</td>'
							body += '</tr>';
							body += '</thead>';

							body += '<tbody>';
							$.each(data.transaction,function(key,val){

								subTotalRata 		= 0;
								subRaAmount 	 	= 0;
								subTaAmount 	 	= 0;
								subRaDiffAmount 	= 0;
								subTaDiffAmount 	= 0;
								subReleaseAmount 	= 0;

								body += '<tr style="border:1px solid #c0c0c0;">';
								body += '<td style="border-right:none;"></td>';
								body += '<td style="font-weight:bold;border-left: none;" colspan="14">'+key+'</td>';
								body += '</tr>';

								$.each(val,function(k,v){

									position = (v.positions) ? v.positions.Name : '';
									firstname = (v.employees.firstname) ? v.employees.firstname : '';
									lastname = (v.employees.lastname) ? v.employees.lastname : '';
									middlename = (v.employees.middlename) ? v.employees.middlename : '';

									fullname = lastname+' '+firstname+' '+middlename;
									raAmount = (v.representation_amount) ? v.representation_amount : 0;
									taAmount = (v.transportation_amount) ? v.transportation_amount : 0;
									raDiffAmount = (v.ra_diff_amount) ? v.ra_diff_amount : 0;
									taDiffAmount = (v.ta_diff_amount) ? v.ta_diff_amount : 0;
									percentage = v.percentage_of_rata;
									noOfWorkDays = (v.number_of_actual_work) ? v.number_of_actual_work : '';
									leaveFiled = (v.number_of_leave_filed) ? v.number_of_leave_filed : '';
									hold = (v.hold) ? v.hold : '';
									remarks = (v.remarks) ? v.remarks : '';

									totalRaAmount = parseFloat(raAmount) + parseFloat(raDiffAmount);
									totalTaAmount = parseFloat(taAmount) + parseFloat(taDiffAmount);

									totalRata = (parseFloat(totalRaAmount) + parseFloat(totalTaAmount));

									releaseAmount = totalRata;

									if(hold){
										releaseAmount = 0;
									}else{
										releaseAmount = releaseAmount;
									}

									subTotalRata += parseFloat(totalRata);
									subRaAmount += parseFloat(totalRaAmount);
									subTaAmount += parseFloat(totalTaAmount);
									subRaDiffAmount += parseFloat(raDiffAmount);
									subTaDiffAmount += parseFloat(taDiffAmount);
									subReleaseAmount += parseFloat(releaseAmount);

									ra_amount = (totalRaAmount) ? commaSeparateNumber(parseFloat(totalRaAmount).toFixed(2)) : '0.00';
									ta_amount = (totalTaAmount) ? commaSeparateNumber(parseFloat(totalTaAmount).toFixed(2)) : '0.00';
									ra_diff_amount = (raDiffAmount) ? commaSeparateNumber(parseFloat(raDiffAmount).toFixed(2)) : '0.00';
									ta_diff_amount = (taDiffAmount) ? commaSeparateNumber(parseFloat(taDiffAmount).toFixed(2)) : '0.00';
									total_rata_amount = (totalRata) ? commaSeparateNumber(parseFloat(totalRata).toFixed(2)) : '0.00';
									release_amount = (releaseAmount) ? commaSeparateNumber(parseFloat(releaseAmount).toFixed(2)) : '0.00';

									body += '<tr class="border-top-black">';
									body += '<td class="border-black text-center">'+ctr+'</td>';
									body += '<td class="border-black text-left" nowrap >&nbsp;'+fullname+'</td>';
									body += '<td class="border-black" nowrap>&nbsp;'+position+'</td>';
									body += '<td class="text-right">'+ra_amount+'</td>';
									body += '<td class="text-right">'+ta_amount+'</td>';
									body += '<td class="text-center">'+leaveFiled+'</td>';
									body += '<td class="text-center">'+noOfWorkDays+'</td>';
									body += '<td class="text-center">'+percentage+'</td>';
									body += '<td class="text-right">'+ra_diff_amount+'</td>';
									body += '<td class="text-right">'+ta_diff_amount+'</td>';
									body += '<td class="text-right">'+ra_amount+'</td>';
									body += '<td class="text-right">'+ta_amount+'</td>';
									body += '<td class="text-right">'+total_rata_amount+'</td>';
									body += '<td class="text-right">'+release_amount+'</td>';
									body += '<td class="text-justify" style="font-size:10px !important;">'+remarks+'</td>';
									body += '</tr>';
									ctr++;


								});

								ctr = parseInt(ctr) - 1;
								netCtr += parseInt(ctr);


								netTotalRata += parseFloat(subTotalRata);
								netRaDiffAmount += parseFloat(subRaDiffAmount);
								netTaDiffAmount += parseFloat(subTaDiffAmount);
								netRaAmount += parseFloat(subRaAmount);
								netTaAmount += parseFloat(subTaAmount);
								netReleaseAmount += parseFloat(subReleaseAmount);

								sub_ra_amount = (subRaAmount) ? commaSeparateNumber(parseFloat(subRaAmount).toFixed(2)) : '0.00';
								sub_ta_amount = (subTaAmount) ? commaSeparateNumber(parseFloat(subTaAmount).toFixed(2)) : '0.00';
								sub_ra_diff_amount = (subRaDiffAmount) ? commaSeparateNumber(parseFloat(subRaDiffAmount).toFixed(2)) : '0.00';
								sub_ta_diff_amount = (subTaDiffAmount) ? commaSeparateNumber(parseFloat(subTaDiffAmount).toFixed(2)) : '0.00';
								sub_rata_amount = (subTotalRata) ? commaSeparateNumber(parseFloat(subTotalRata).toFixed(2)) : '0.00';
								sub_release_amount = (subReleaseAmount) ? commaSeparateNumber(parseFloat(subReleaseAmount).toFixed(2)) : '0.00';


								body += '<tr style="border:1px solid #c0c0c0;">';
								body += '<td  class="text-center">'+ctr+'</td>';
								body += '<td  colspan="2"><b>Sub Total</b></td>';
								body += '<td  class="text-right font-weight-bold">'+sub_ra_amount+'</td>';
								body += '<td  class="text-right font-weight-bold">'+sub_ta_amount+'</td>';
								body += '<td ></td>';
								body += '<td ></td>';
								body += '<td ></td>';
								body += '<td  class="text-right font-weight-bold">'+sub_ra_diff_amount+'</td>';
								body += '<td  class="text-right font-weight-bold">'+sub_ta_diff_amount+'</td>';
								body += '<td  class="text-right font-weight-bold">'+sub_ra_amount+'</td>';
								body += '<td  class="text-right font-weight-bold">'+sub_ta_amount+'</td>';
								body += '<td  class="text-right font-weight-bold">'+sub_rata_amount+'</td>';
								body += '<td  class="text-right font-weight-bold">'+sub_release_amount+'</td>';
								body += '<td></td>';
								body += '</tr>';
								ctr = 1;
							});

						net_ra_amount = (netRaAmount) ? commaSeparateNumber(parseFloat(netRaAmount).toFixed(2)) : '0.00';
						net_ta_amount = (netTaAmount) ? commaSeparateNumber(parseFloat(netTaAmount).toFixed(2)) : '0.00';
						net_ra_diff_amount = (netRaDiffAmount) ? commaSeparateNumber(parseFloat(netRaDiffAmount).toFixed(2)) : '0.00';
						net_ta_diff_amount = (netTaDiffAmount) ? commaSeparateNumber(parseFloat(netTaDiffAmount).toFixed(2)) : '0.00';
						net_rata_amount = (netTotalRata) ? commaSeparateNumber(parseFloat(netTotalRata).toFixed(2)) : '0.00';
						net_release_amount = (netReleaseAmount) ? commaSeparateNumber(parseFloat(netReleaseAmount).toFixed(2)) : '0.00';

						signTopLeft = (signTopLeft) ? signTopLeft : '';
						positionTopLeft = (positionTopLeft) ? positionTopLeft : '';
						signTopMidOne = (signTopMidOne) ? signTopMidOne : '';
						positionTopMidOne = (positionTopMidOne) ? positionTopMidOne : '';
						signTopMidTwo = (signTopMidTwo) ? signTopMidTwo : '';
						positionTopMidTwo = (positionTopMidTwo) ? positionTopMidTwo : '';
						signTopRight = (signTopRight) ? signTopRight : '';
						positionTopRight = (positionTopRight) ? positionTopRight : '';
						signTopRightTwo = (signTopRightTwo) ? signTopRightTwo : '';
						positionTopRightTwo = (positionTopRightTwo) ? positionTopRightTwo : '';
						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';
						degree = (degree) ? degree : '';

						body += '<tr style="border:1px solid #c0c0c0;">';
						body += '<td >'+netCtr+'</td>';
						body += '<td  colspan="2"><b>GRAND Total</b></td>';
						body += '<td  class="text-right font-weight-bold">'+net_ra_amount+'</td>';
						body += '<td  class="text-right font-weight-bold">'+net_ta_amount+'</td>';
						body += '<td ></td>';
						body += '<td ></td>';
						body += '<td ></td>';
						body += '<td  class="text-right font-weight-bold">'+net_ra_diff_amount+'</td>';
						body += '<td  class="text-right font-weight-bold">'+net_ta_diff_amount+'</td>';
						body += '<td  class="text-right font-weight-bold">'+net_ra_amount+'</td>';
						body += '<td  class="text-right font-weight-bold">'+net_ta_amount+'</td>';
						body += '<td  class="text-right font-weight-bold">'+net_rata_amount+'</td>';
						body += '<td  class="text-right font-weight-bold">'+net_release_amount+'</td>';
						body += '<td></td>';
						body += '</tr>';

						body += '<tr>';
					 	body += '<td style="border:none;" colspan="15" >';

					 	body += '<div class="row">';
					 	body += '<div class="col-md-6">';
					 	body += '</div>';
					 	body += '<div class="col-md-6">';
					 	// TABLE RATA
					 	body +=	'<span>*RATA Schedule follows:</span>';
					 	body +=	'<table class="table" style="font-size: 10px;">';
					 	body +=	'<thead class="text-center">';
					 	body +=  '<tr>';
					 	body += 	'<td><b>Number of Workdays of Actual Work Performance in a Month</b></td>';
					 	body += 	'<td><b>Actual RATA for a Month</b></td>';
					 	body +=	'</tr>';
					 	body +=	'</thead>';
					 	body +=	'<tbody class="text-center">';
					 	body +=	'<tr>';
					 	body +=	'<td>1 to 5</td>';
					 	body +=	'<td>25% of the monthly RATA</td>';
					 	body +=	'</tr>';
					 	body +=	'<tr>';
					 	body +=	'<td>6 to 11</td>';
					 	body +=	'<td>50% of the monthly RATA</td>';
					 	body +=	'</tr>';
					 	body +=	'<tr>';
					 	body +=	'<td>12 to 16</td>';
					 	body +=	'<td>75% of the monthly RATA</td>';
					 	body +=	'</tr>';
					 	body +=	'<tr>';
					 	body +=	'<td>17 and more</td>';
					 	body +=	'<td>100% of the monthly RATA</td>';
					 	body +=	'</tr>';
					 	body +=	'</tbody>';
					 	body +=	'</table>';
					 	// end TABLE
					 	body += '</div>';
					 	body += '</div>';

					 	body += '<div class="row">';
					 	body += '<div class="col-md-4">';
					 	body += '<b><i>CERTIFICATION:  This is to certify that the above-mentioned officials/personnel are entitled to the grant of RATA	per NBC No. 546 dated January 17, 2013 re: Rules and Regulations on the grant of RATA. </i></b>'
					 	body += '</div>';

					 	body += '<div class="col-md-3">';
					 	body +=	'<span><b>Certified:</b> Supporting documents complete and </span>';
						body +=	'<span>proper, and cash available in the amount of</span><br>';
						body +=	'<span>Php _________________________.</span>';
					 	body += '</div>';

					 	body += '<div class="col-md-2">Approved For Payment</div>';

					 	body += '<div class="col-md-3">'
					 	body +=	'<span><b>Certified:</b> Each employee whose name appears on the payroll has </span>';
						body +=	'<span>been paid the amount as indicated opposite his/her name.</span>';
					 	body += '</div>';

					 	body += '</div>';

					 	body += '<div style="height:80px;"></div>';

					 	body += '<div class="row">';
					 	body += '<div class="col-md-2">';
					 	body +=	'<b>'+signTopLeft+'</b>';
						body +=	'<br>';
						body +=	'<span>'+positionTopLeft+'</span>';
					 	body += '</div>';

					 	body += '<div class="col-md-2">';
					 	body +=	'<b>'+signTopMidOne+'</b>';
						body +=	'<br>';
						body +=	'<span>'+positionTopMidOne+'</span>';
					 	body += '</div>';

					 	body += '<div class="col-md-3">';
					 	body +=	'<b>'+signTopMidTwo+'</b>';
						body +=	'<br>';
						body +=	'<span>'+positionTopMidTwo+'</span>';
					 	body += '</div>';

					 	body += '<div class="col-md-3">';
					 	body +=	'<b>'+signTopRight+' '+degree+'</b>';
						body +=	'<br>';
						body +=	'<span>'+positionTopRight+'</span>';
					 	body += '</div>';

					 	body += '<div class="col-md-2">';
					 	body +=	'<b>'+signTopRightTwo+'</b>';
						body +=	'<br>';
						body +=	'<span>'+positionTopRightTwo+'</span>';
					 	body += '</div>';
					 	body += '</div>';

					 	body += '<div style="height:50px;"></div>';

					 	body += '<div class="row">';
					 	body += '<div class="col-md-12">';
					 	body += '<i>AO-'+_Year+_Month+'-'+codeOne+' <br> hrdd/'+codeTwo+' </i>';
					 	body += '</div>';
					 	body += '</div>';

					 	body +=	'</td>';
					 	body += '</tr>';


					 	body += '</tbody></table>'
						$('#payroll_transfer').html(body);

						$('#btnModal').trigger('click');
					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}


	});

$(document).on('click','#print',function(){
	$('#reports').printThis();
})
})
</script>
@endsection