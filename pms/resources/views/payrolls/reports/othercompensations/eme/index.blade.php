@extends('app-othercompensations')

@section('othercompensations-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<style type="text/css">
.table2>thead>tr>td, .table2>tbody>tr>td{
    padding: 3px !important;
 }
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes.covereddate')
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Left</span>
							<select class="form-control font-style2 select2" id="sign_top_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Mid</span>
							<select class="form-control font-style2 select2" id="sign_mid_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_mid_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Right</span>
							<select class="form-control font-style2 select2" id="sign_top_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<span>Signatory Bottom Left</span>
							<select class="form-control font-style2 select2" id="sign_bot_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_bot_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<span>Degree</span>
							<input type="text" name="degree" id="degree" class="form-control font-style2">
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Bottom Right</span>
							<select class="form-control font-style2 select2" id="sign_bot_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_bot_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="margin-right: -5px;margin-left: -5px;">
	       			<div class="col-md-12" id="payroll_transfer">
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	});

	$('#select_year').trigger('change');
	$('#select_month').trigger('change');

	var signTopLeft;
	var posTopLeft;

	$('#sign_top_left').change(function(){
		signTopLeft = "";
		posTopLeft  = "";
		signTopLeft = $(this).find(':selected').text()
		posTopLeft = $(this).find(':selected').data('position_top_left')

	});

	var signTopMid;
	var posTopMid;

	$('#sign_mid_left').change(function(){
		signTopMid = "";
		posTopMid  = "";
		signTopMid = $(this).find(':selected').text()
		posTopMid = $(this).find(':selected').data('position_mid_left')


	});

	var signTopRight;
	var posTopRight;

	$('#sign_top_right').change(function(){
		signTopRight = "";
		posTopRight  = "";
		signTopRight = $(this).find(':selected').text()
		posTopRight = $(this).find(':selected').data('position_top_right')


	});

	var signBotLeft;
	var posBotLeft;

	$('#sign_bot_left').change(function(){
		signBotLeft = "";
		posBotLeft  = "";
		signBotLeft = $(this).find(':selected').text()
		posBotLeft = $(this).find(':selected').data('position_bot_left')


	});

	var signBotRight;
	var posBotRight;

	$('#sign_bot_right').change(function(){
		signBotRight = "";
		posBotRight  = "";
		signBotRight = $(this).find(':selected').text()
		posBotRight = $(this).find(':selected').data('position_bot_right')


	});

	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	var degree;
	$(document).on('keyup','#degree',function(){
		degree = "";
		degree = $(this).val();

	});

	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getEmployeeinfo',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr = 1;
						netCtr = 0;
						netEEAmount = 0;
						netMEAmount = 0;
						netEMEAmount = 0;
						coveredPeriod = months[_Month]+' '+_Year;


						body += '<table class="table table2" style="border:none;">';
						body += '<thead class="text-center">';
						body += '<tr>';
						body += '<td class="text-center" colspan="9" style="border-left:none;border-right:none;font-weight:bold;">PHILIPPINE COMPETITION COMMISSION <br> GENERAL PAYROLL FOR EXTRAORDINARY AND MISCELLANEOUS EXPENSES </td>';
						body += '</tr>';
						body +=	'<tr ><td ><b >#</b></td>'
						body +=	'<td ><b >EMPLOYEE NAME</b></td>';
						body +=	'<td ><b >POSITION/DESIGNATION</b></td>';
						body +=	'<td ><b>OFFICE</b></td>';
						body +=	'<td ><b>EXTRAORDINARY EXPENSES</b></td>';
						body +=	'<td ><b>MISCELLANEOUS EXPENSE</b></td>';
						body +=	'<td ><b>TOTAL</b></td>';
						body +=	'<td ><b >SIGNATURE</b></td>';
						body +=	'<td ><b>REMARKS</b></td></tr>';
						body += '</thead>';

						body += '<tbody>';

						body += '<tr>';
						body += '<td colspan="9">';
						body += 'Pursuant to COA Circular No. 89-300 dated March 21, 1989, we hereby CERTIFY to have incurred the total amount provided chargeable against Extraordinary and Miscellaneous Expenses				for the month of <b>'+coveredPeriod+'</b>, in connection with and for the purpose on activities completed, authorized under Sec. 42 of GAA, FY 2017 particularly with respect to meetings and					conferences, official entertainment, public relations, education and cultural activities, and other similar expenses not supported by the regular budget allocation, respectively, in relation to, or by reason of our respective positions.';
						body += '</td>';
						body += '</tr>';
						$.each(data.transaction,function(key,val){

							subEEAmount = 0;
							subMEAmount = 0;
							subEMEAmount = 0;

							body += '<tr>';
							body += '<td colspan="9" style="font-weight:bold;">'+key+'</td>';
							body += '</tr>';
							$.each(val,function(k,v){
								var eeAmount = 0;
								var meAmount = 0;

								firstname = (v.employees) ? v.employees.firstname : '';
								lastname = (v.employees) ? v.employees.lastname : '';
								middlename = (v.employees) ? v.employees.middlename : '';
								jobgrade = (v.salaryinfo) ? v.salaryinfo.jobgrade.Code : '';
								position = (v.positions) ? v.positions.Name : '';
								office = (v.offices) ? v.offices.Name : '';
								remarks = (v.special_remarks) ? v.special_remarks : '';

								$.each(v.special,function(k2,v2){
									switch(v2.status){
										case 'ee':
											eeAmount = (v2.amount) ? v2.amount : 0;
										break;
										case 'me':
											meAmount = (v2.amount) ? v2.amount : 0;
										break;
										}
									});

								fullname = lastname+' '+firstname+' '+middlename;

								totalEMEAmount = parseFloat(eeAmount) + parseFloat(meAmount);

								subEEAmount += parseFloat(eeAmount);
								subMEAmount += parseFloat(meAmount);
								subEMEAmount += parseFloat(totalEMEAmount);

								ee_amount = (eeAmount !== 0) ? commaSeparateNumber(parseFloat(eeAmount).toFixed(2)) : '0.00';
								me_amount = (meAmount !== 0) ? commaSeparateNumber(parseFloat(meAmount).toFixed(2)) : '0.00';
								total_amount = (totalEMEAmount !== 0) ? commaSeparateNumber(parseFloat(totalEMEAmount).toFixed(2)) : '0.00';
								body += '<tr>';
								body += '<td>'+ctr+'</td>';
								body += '<td  nowrap>'+fullname+'</td>';
								// body += '<td  nowrap>'+jobgrade+'</td>';
								body += '<td nowrap>'+position+'</td>';
								body += '<td nowrap>'+office+'</td>';
								body += '<td class="text-right">'+ee_amount+'</td>';
								body += '<td class="text-right">'+me_amount+'</td>';
								body += '<td class="text-right">'+total_amount+'</td>';
								body += '<td><div style="width:150px;">&nbsp;</div></td>';
								body += '<td>'+remarks+'</td>';
								body += '</tr>';
								ctr++;
							});

							ctr = parseInt(ctr) - 1;
							netCtr += parseInt(ctr);

							netEEAmount += parseFloat(subEEAmount);
							netMEAmount += parseFloat(subMEAmount);
							netEMEAmount += parseFloat(subEMEAmount);

							sub_ee_amount = (subEEAmount !== 0) ? commaSeparateNumber(parseFloat(subEEAmount).toFixed(2)) : '0.00';
							sub_me_amount = (subMEAmount !== 0) ? commaSeparateNumber(parseFloat(subMEAmount).toFixed(2)) : '0.00';
							sub_total_amount = (subEMEAmount !== 0) ? commaSeparateNumber(parseFloat(subEMEAmount).toFixed(2)) : '0.00';

							body += '<tr style="border:1px solid #c0c0c0;font-weight:bold;">';
							body += '<td class="text-center">'+ctr+'</td>';
							body += '<td></td>';
							body += '<td></td>';
							body += '<td style="font-weight:bold;">SUB TOTAL</td>';
							body += '<td class="text-right">'+sub_ee_amount+'</td>';
							body += '<td class="text-right">'+sub_me_amount+'</td>';
							body += '<td class="text-right">'+sub_total_amount+'</td>';
							body += '<td></td>';
							body += '<td></td>';
							body += '</tr>';
							ctr = 1;
						});

						net_ee_amount = (netEEAmount) ? commaSeparateNumber(parseFloat(netEEAmount).toFixed(2)) : '0.00';
						net_me_amount = (netMEAmount) ? commaSeparateNumber(parseFloat(netMEAmount).toFixed(2)) : '0.00';
						net_total 	  = (netEMEAmount) ? commaSeparateNumber(parseFloat(netEMEAmount).toFixed(2)) : '0.00';

						body += '<tr style="border:1px solid #c0c0c0;font-weight:bold;">';
						body += '<td class="text-center">'+netCtr+'</td>';
						body += '<td></td>';
						body += '<td></td>';
						body += '<td style="font-weight:bold;">GRAND TOTAL</td>';
						body += '<td class="text-right">'+net_ee_amount+'</td>';
						body += '<td class="text-right">'+net_me_amount+'</td>';
						body += '<td class="text-right">'+net_total+'</td>';
						body += '<td></td>';
						body += '<td></td>';
						body += '</tr>';

						signTopLeft = (signTopLeft) ? signTopLeft : '';
						posTopLeft = (posTopLeft) ? posTopLeft : '';
						signTopMid = (signTopMid) ? signTopMid : '';
						posTopMid = (posTopMid) ? posTopMid : '';
						signTopRight = (signTopRight) ? signTopRight : '';
						posTopRight = (posTopRight) ? posTopRight : '';
						signBotLeft = (signBotLeft) ? signBotLeft : '';
						posBotLeft = (posBotLeft) ? posBotLeft : '';
						signBotRight = (signBotRight) ? signBotRight : '';
						posBotRight = (posBotRight) ? posBotRight : '';

						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';
						degree = (degree) ? degree : '';

						body += '<tr>';
					 	body += '<td style="border:none;" colspan="9" >';
					 	body += '<div style="height:20px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-4"> <div style="height:40px;"></div> Certified Correct:</div>';
					 	body += '<div class="col-md-4"></div>';
					 	body += '<div class="col-md-4">';
					 	body +=	'<span><b>Certified:</b> Supporting documents complete and </span>';
						body +=	'<span>proper, and cash available in the amount of</span><br>';
						body +=	'<span>Php _________________________.</span>';
					 	body += '</div>'
					 	body += '</div>'

					 	body += '<div style="height:30px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-4">';
					 	body +=	'<b>'+signTopLeft+'</b>';
						body +=	'<br>';
						body +=	'<span>'+posTopLeft+'</span>';
					 	body += '</div>'
					 	body += '<div class="col-md-4">';
					 	body +=	'<b>'+signTopMid+'</b>'
						body +=	'<br>'
						body +=	'<span>'+posTopMid+'</span>'
					 	body += '</div>'
					 	body += '<div class="col-md-4">';
					 	body +=	'<b>'+signTopRight+'</b>';
						body +=	'<br>';
						body +=	'<span>'+posTopRight+'</span>';
					 	body += '</div>'
					 	body += '</div>'

					 	body += '<div style="height:30px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-4"> Approved for Payment</div>';
					 	body += '<div class="col-md-4"></div>';
					 	body += '<div class="col-md-4">';
					 	body +=	'<span><b>Certified:</b> Each employee whose name appears on the payroll has </span>';
						body +=	'<span>been paid the amount as indicated opposite his/her name.</span>';
					 	body += '</div>'
					 	body += '</div>'

					 	body += '<div style="height:30px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-4">';
					 	body +=	'<b>'+signBotLeft+' '+degree+'</b>';
						body +=	'<br>';
						body +=	'<span>'+posBotLeft+'</span>';
					 	body += '</div>'
					 	body += '<div class="col-md-4"></div>';
					 	body += '<div class="col-md-4">';
					 	body +=	'<b>'+signBotRight+'</b>';
						body +=	'<br>';
						body +=	'<span>'+posBotRight+'</span>';
					 	body += '</div>'
					 	body += '</div>'

					 	body += '<div style="height:30px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-12">';
					 	body += '<i>AO-'+_Year+_Month+'-'+codeOne+' <br> hrdd/'+codeTwo+' </i>';
					 	body += '</div>'
					 	body += '</div>'

					 	body +=	'</td>';

					 	body += '<tbody></table>';



						$('#payroll_transfer').html(body);


						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$(document).on('click','#print',function(){
	$('#reports').printThis();
})
})
</script>
@endsection