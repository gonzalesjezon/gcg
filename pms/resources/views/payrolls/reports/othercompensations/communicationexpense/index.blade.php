@extends('app-othercompensations')


@section('othercompensations-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<style type="text/css">
.table2>thead>tr>td, .table2>tbody>tr>td{
    padding: 3px !important;
  }
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes.covereddate')
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Left</span>
							<select class="form-control font-style2 select2" id="sign_top_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Mid</span>
							<select class="form-control font-style2 select2" id="sign_mid_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_mid_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Right</span>
							<select class="form-control font-style2 select2" id="sign_top_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<span>Signatory Bottom Left</span>
							<select class="form-control font-style2 select2" id="sign_bot_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_bot_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<span>Degree</span>
							<input type="text" name="degree" id="degree" class="form-control font-style2">
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Bottom Right</span>
							<select class="form-control font-style2 select2" id="sign_bot_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_bot_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="margin-right: -5px;margin-left: -5px;">
	       			<div class="col-md-12" id="payroll_transfer">
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){

	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	});

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	var signTopLeft;
	var posTopLeft;

	$('#sign_top_left').change(function(){
		signTopLeft = "";
		posTopLeft  = "";
		signTopLeft = $(this).find(':selected').text()
		posTopLeft = $(this).find(':selected').data('position_top_left')

	});

	var signTopMid;
	var posTopMid;

	$('#sign_mid_left').change(function(){
		signTopMid = "";
		posTopMid  = "";
		signTopMid = $(this).find(':selected').text()
		posTopMid = $(this).find(':selected').data('position_mid_left')


	});

	var signTopRight;
	var posTopRight;

	$('#sign_top_right').change(function(){
		signTopRight = "";
		posTopRight  = "";
		signTopRight = $(this).find(':selected').text()
		posTopRight = $(this).find(':selected').data('position_top_right')


	});

	var signBotLeft;
	var posBotLeft;

	$('#sign_bot_left').change(function(){
		signBotLeft = "";
		posBotLeft  = "";
		signBotLeft = $(this).find(':selected').text()
		posBotLeft = $(this).find(':selected').data('position_bot_left')


	});

	var signBotRight;
	var posBotRight;

	$('#sign_bot_right').change(function(){
		signBotRight = "";
		posBotRight  = "";
		signBotRight = $(this).find(':selected').text()
		posBotRight = $(this).find(':selected').data('position_bot_right')


	});

	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	var degree;
	$(document).on('keyup','#degree',function(){
		degree = "";
		degree = $(this).val();

	});

	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getEmployeeinfo',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						netCtr = 0;
						ctr 		 = 1;
						netCeaAmount = 0;
						netAddAmount 	= 0;
						netLessAmount 	= 0;
						netNetAmount 	= 0;


						coveredPeriod = months[_Month]+' '+_Year;

						body += '<table class="table table2" style="border:none;">';
						body += '<thead class="text-center" style="font-weight: bold;">';
						body += '<tr>';
						body += '<td class="text-center" colspan="11" style="border:none !important;font-weight:bold; border-bottom:1px solid silver !important;">PHILIPPINE COMPETITION COMMISSION <br> COMMUNICATIONS EXPENSE ALLOTMENT</td>';
						body += '</tr>';
						body += '<tr>';
						body += '<tr>';
						body += '<td style="vertical-align:middle;" rowspan="2"><b >#</b></td>';
						body += '<td style="vertical-align:middle;" rowspan="2"><b >EMPLOYEE NAME</b></td>';
						body += '<td style="vertical-align:middle;" rowspan="2"><b >POSITION/DESIGNATION</b></td>';
						body += '<td style="vertical-align:middle;" rowspan="2"><b>OFFICE</b></td>';
						body += '<td style="vertical-align:middle;" rowspan="2"><b>AMOUNT</b></td>';
						body += '<td style="vertical-align:middle;" colspan="2"><b>DIFFERENTIAL</b></td>';
						body += '<td style="vertical-align:middle;" rowspan="2"><b >NET</b></td>'
						body += '<td style="vertical-align:middle;" rowspan="2"><b >FOR RELEASE</b></td>';
						body += '<td style="vertical-align:middle;" rowspan="2"><b >SIGNATURE</b></td>';
						body += '<td style="vertical-align:middle;" rowspan="2"><b>REMARKS</b></td>';
						body += '</tr>';
						body += '<tr>';
						body += '<td style="vertical-align:middle;" class="text-center">ADD</td>';
						body += '<td style="vertical-align:middle;" class="text-center">LESS</td>';
						body += '</tr>';
						body += '<tr>';
						body += '<td  colspan="11" class="text-left">We hereby certify to have incurred Communication expenses in the performance of our official functions in our respective office/divisions for the month of <b>'+coveredPeriod+'</b> as per Commission Resolution No. 80 series of 2016.</td>';
						body += '</tr>';
						body += '</thead>';

						body += '<tbody>';

						$.each(data.transaction,function(key,val){

							body += '<tr style="border:1px solid #c0c0c0;">';
							body += '<td style="border-right:none;"></td>';
							body += '<td  colspan="11" style="border-left:none;" class="text-left"><b>'+key+'</b></td>';
							body += '</tr>';

							subCeaAmount 	= 0;
							subAddAmount 	= 0;
							subLessAmount 	= 0;
							subNetAmount 	= 0;
							$.each(val,function(k,v){

								jobGrade = (v.salaryinfo.jobgrade) ? v.salaryinfo.jobgrade.Code : '';
								position = (v.positions) ? v.positions.Name : '';
								office = (v.offices !== null) ? v.offices.Name : '';
								firstname = (v.employees.firstname) ? v.employees.firstname : '';
								lastname = (v.employees.lastname) ? v.employees.lastname : '';
								middlename = (v.employees.middlename) ? v.employees.middlename : '';
								remarks = (v.special_remarks) ? v.special_remarks : '';
								addAmount = (v.add_amount) ? v.add_amount : 0;
								lessAmount = (v.less_amount) ? v.less_amount : 0;
								ceaAmount = (v.amount) ? v.amount : 0;

								fullname = lastname+' '+firstname+' '+middlename;

								netAmount = parseFloat(ceaAmount) + parseFloat(addAmount) - parseFloat(lessAmount)


								subCeaAmount += parseFloat(ceaAmount);
								subAddAmount += parseFloat(addAmount);
								subLessAmount += parseFloat(lessAmount);
								subNetAmount += parseFloat(netAmount);

								cea_amount = (ceaAmount) ? commaSeparateNumber(parseFloat(ceaAmount).toFixed(2)) : '0.00';
								add_amount = (addAmount) ? commaSeparateNumber(parseFloat(addAmount).toFixed(2)) : '0.00';
								less_amount = (lessAmount) ? commaSeparateNumber(parseFloat(lessAmount).toFixed(2)) : '0.00';
								net_amount = (netAmount) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '0.00';

								body += '<tr style="border:1px solid #c0c0c0;" class="text-right">';
								body += '<td >'+ctr+'</td>';
								body += '<td class="text-left" nowrap>'+fullname+'</td>';
								// body += '<td class="text-left" nowrap>'+jobGrade+'</td>';
								body += '<td class="text-left" nowrap>'+position+'</td>';
								body += '<td class="text-left" nowrap>'+office+'</td>';
								body += '<td class="text-right">'+cea_amount+'</td>';
								body += '<td class="text-right">'+add_amount+'</td>';
								body += '<td class="text-right">'+less_amount+'</td>';
								body += '<td class="text-right">'+net_amount+'</td>';
								body += '<td class="text-right">'+net_amount+'</td>';
								body += '<td ><div style="width:180px;">&nbsp;</div></td>';
								body += '<td class="text-justify" >'+remarks+'</td>';
								body += '</tr>';
								ctr++;

							});
							ctr = parseInt(ctr) - 1;
							netCtr += parseInt(ctr);
							netCeaAmount += parseFloat(subCeaAmount);
							netAddAmount += parseFloat(subAddAmount);
							netLessAmount += parseFloat(subLessAmount);
							netNetAmount += parseFloat(subNetAmount);

							sub_cea_amount = (subCeaAmount) ? commaSeparateNumber(parseFloat(subCeaAmount).toFixed(2)) : '0.00';
							sub_add_amount = (subAddAmount) ? commaSeparateNumber(parseFloat(subAddAmount).toFixed(2)) : '0.00';
							sub_less_amount = (subLessAmount) ? commaSeparateNumber(parseFloat(subLessAmount).toFixed(2)) : '0.00';
							sub_net_amount = (subNetAmount) ? commaSeparateNumber(parseFloat(subNetAmount).toFixed(2)) : '0.00';

							body += '<tr style="border:1px solid #c0c0c0;font-weight:bold;">';
							body += '<td class="text-center" >'+ctr+'</td>';
							body += '<td class="text-center">SUB TOTAL</td>';
							body += '<td class="text-center"></td>';
							body += '<td class="text-center"></td>';
							body += '<td class="text-right">'+sub_cea_amount+'</td>';
							body += '<td class="text-center">'+sub_add_amount+'</td>';
							body += '<td class="text-center">'+sub_less_amount+'</td>';
							body += '<td class="text-center">'+sub_net_amount+'</td>';
							body += '<td class="text-right">'+sub_net_amount+'</td>';
							body += '<td ></td>';
							body += '<td ></td>';
							body += '</tr>';
							ctr = 1;

						});

						net_cea_amount = (netCeaAmount) ? commaSeparateNumber(parseFloat(netCeaAmount).toFixed(2)) : '0.00';
						net_add_amount = (netAddAmount) ? commaSeparateNumber(parseFloat(netAddAmount).toFixed(2)) : '0.00';
						net_less_amount = (netLessAmount) ? commaSeparateNumber(parseFloat(netLessAmount).toFixed(2)) : '0.00';
						net_net_amount = (netNetAmount) ? commaSeparateNumber(parseFloat(netNetAmount).toFixed(2)) : '0.00';

						body += '<tr style="border:1px solid #c0c0c0;font-weight:bold;">';
						body += '<td class="text-center" >'+netCtr+'</td>';
						body += '<td class="text-center">GRAND TOTAL</td>';
						body += '<td class="text-center"></td>';
						body += '<td class="text-center"></td>';
						body += '<td class="text-right">'+net_cea_amount+'</td>';
						body += '<td class="text-right">'+net_add_amount+'</td>';
						body += '<td class="text-right">'+net_less_amount+'</td>';
						body += '<td class="text-right">'+net_net_amount+'</td>';
						body += '<td class="text-right">'+net_net_amount+'</td>';
						body += '<td ></td>';
						body += '<td ></td>';
						body += '</tr>';

						signTopLeft = (signTopLeft) ? signTopLeft : '';
						posTopLeft = (posTopLeft) ? posTopLeft : '';
						signTopMid = (signTopMid) ? signTopMid : '';
						posTopMid = (posTopMid) ? posTopMid : '';
						signTopRight = (signTopRight) ? signTopRight : '';
						posTopRight = (posTopRight) ? posTopRight : '';
						signBotLeft = (signBotLeft) ? signBotLeft : '';
						posBotLeft = (posBotLeft) ? posBotLeft : '';
						signBotRight = (signBotRight) ? signBotRight : '';
						posBotRight = (posBotRight) ? posBotRight : '';
						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';
						degree = (degree) ? degree : '';

						body += '<tr>';
					 	body += '<td style="border:none;" colspan="11" >';
					 	body += '<div class="row">';
					 	body += '<div class="col-md-12">';
					 	body += '<b><i>CERTIFICATION: This is to certify the above personal are entitled to receive monthly communications allotment pursuant to Commission Resolution No. 80 series of 2016, as amended, and Commission Resolution No. 30-2018. </i></b>';
					 	body += '</div>';
					 	body += '</div>';

					 	body += '<div style="height:30px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-6">';
					 	body += '</div>';
					 	body += '<div class="col-md-6">';
					 	body +=	'<span><b>Certified:</b> Supporting documents complete and <br> </span>';
						body +=	'<span>proper, and cash available in the amount of</span><br>';
						body +=	'<span>Php _________________________.</span>';
					 	body += '</div>';
					 	body += '</div>';
					 	body += '<div style="height:40px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-3">';
					 	body +=	'<b>'+signTopLeft+'</b>';
						body +=	'<br>';
						body +=	'<span>'+posTopLeft+'</span>';
					 	body += '</div>';
					 	body += '<div class="col-md-3">';
					 	body +=	'<b>'+signTopMid+'</b>'
						body +=	'<br>'
						body +=	'<span>'+posTopMid+'</span>'
					 	body += '</div>';
					 	// body += '<div class="col-md-3">';
					 	// body += '</div>';
					 	body += '<div class="col-md-6">';
					 	body +=	'<b>'+signTopRight+'</b>';
						body +=	'<br>';
						body +=	'<span>'+posTopRight+'</span>';
					 	body += '</div>';
					 	body += '</div>';
					 	body += '<div style="height:40px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-6">Approved for Payment';
					 	body += '</div>';
					 	body += '<div class="col-md-6">';
					 	body +=	'<span><b>Certified:</b> Each employee whose name appears on the payroll has </span> </br>';
						body +=	'<span>been paid the amount as indicated opposite his/her name.</span>';
					 	body += '</div>';
					 	body += '</div>';
					 	body += '<div style="height:40px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-6">';
					 	body +=	'<b>'+signBotLeft+' '+degree+'</b>';
						body +=	'<br>';
						body +=	'<span>'+posBotLeft+'</span>';
					 	body += '</div>';
					 	// body += '<div class="col-md-6">';
					 	// body += '</div>';
					 	body += '<div class="col-md-6">';
					 	body +=	'<b>'+signBotRight+'</b>';
						body +=	'<br>';
						body +=	'<span>'+posBotRight+'</span>';
					 	body += '</div>';
					 	body += '</div>';
					 	body += '<div style="height:40px;"></div>'

					 	body += '<div class="row">';
					 	body += '<div class="col-md-12">';
					 	body += '<i>AO-'+_Year+_Month+'-'+codeOne+' <br> hrdd/'+codeTwo+' </i>';
					 	body += '</div>';
					 	body += '</div>';


					 	body +=	'</td>';
						body += '</tr>';

					 	body += '</tbody></table>';

						$('#payroll_transfer').html(body);

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}


	});
$(document).on('click','#print',function(){
	$('#reports').printThis();
})

})
</script>
@endsection