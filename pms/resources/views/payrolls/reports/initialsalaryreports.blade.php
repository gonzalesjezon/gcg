@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}" media="print">
<style type="text/css">
	.table>tbody>tr>td{
		padding: 2px;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-12">
						<span>Employee Name</span>
						<div class="form-group">
							<select class="form-control select2" name="employee_id" id="employee_id" style="margin:auto;width: 100%;">
								<option value=""></option>
								@foreach($employee as $value)
								<option value="{{ $value->id }}">{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Date</span>
							<input type="text" name="print_date" id="print_date" class="form-control font-style2 datepicker">
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Certified Left</span>
							<select class="form-control font-style2 select2" id="sign_top_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Certified Right</span>
							<select class="form-control font-style2 select2" id="sign_top_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6" style="padding-top: 10px;">
						<div class="form-group">
							<span>Initial</span>
							<input type="text" name="code_2" id="code_2" class="form-control font-style2">
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div style="width: 960px;margin: auto;" id="reports">
		       <div class="row">
   					<div class="col-md-8">
   						<img src="{{ url('images/new-logo.jpg') }}" style="height: 80px;">
   						<i></i>
   					</div>
   					<div class="col-md-4">
   						<i style="font-size: 12px;color: #333;">
   							25/F Tower 1, Vertis North Corporate Center,
							North Avenue, Quezon City 1105
							queries@phcc.gov.ph
   						</i>
   					</div>
		       </div>
		       <hr>
		       <div class="row">
		       		<div class="col-md-12 text-center">
		       			<span style="font-weight: bold;border-bottom: 2px solid;font-size: 14px;"></span>
		       		</div>
		       </div>
		       <div class="row" style="margin-top: 30px;">
		       		<div class="col-md-4">
		       			<table class="table borderless" style="border: none;">
		       				<tbody style="border-top:none;">
		       					<tr>
		       						<td>Employee Name</td>
		       						<td class="text-left">
		       							<span id="employee_name">dsa</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td>Position</td>
		       						<td class="text-left">
		       							<span id="position">ds</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td>Office</td>
		       						<td class="text-left">
		       							<span id="office">ds</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td>Effectivity of Assumption</td>
		       						<td class="text-left">
		       							<span id="assumption_date">dsa</span>
		       						</td>
		       					</tr>
		       				</tbody>
		       			</table>
		       		</div>
		       </div>
		       <div class="row">
		       		<div class="col-md-5">
		       			<table class="table borderless" style="border: none;">
		       				<tbody style="border-top: none;">
		       					<tr>
		       						<td>Start Date</td>
		       						<td class="text-left">
		       							<span id="start_date"></span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td style="font-weight: bold;padding: 10px;" colspan="3" class="text-right">For GSIS Computation Only</td>
		       					</tr>
		       					<tr>
		       						<td>Basic (monthly)</td>
		       						<td class="text-right">
		       							<span id="basic_monthly">0.00</span>
		       						</td>
		       						<td class="text-right">
		       							<span id="gsis_basic_monthly">0.00</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td>Basic (daily)</td>
		       						<td class="text-right">
		       							<span id="basic_daily">0.00</span>
		       						</td>
		       						<td class="text-right">
		       							<span id="gsis_basic_daily">0.00</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td>Basic (hourly)</td>
		       						<td class="text-right">
		       							<span id="basic_hourly">0.00</span>
		       						</td>
		       						<td class="text-right">
		       							<span id="gsis_basic_hourly">0.00</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td>PERA (monthly)</td>
		       						<td class="text-right">
		       							<span id="pera_monthly">0.00</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td>PERA (for the period)</td>
		       						<td class="text-right">
		       							<span id="pera_period">0.00</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td colspan="4" style="padding: 8px;"></td>
		       					</tr>
		       					<tr>
		       						<td>No of Working Days</td>
		       						<td class="text-right">
		       							<span id="no_working_days">0</span>
		       						</td>
		       						<td class="text-right">
		       							<span id="gsis_no_working_days">0</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td>No of Days Worked</td>
		       						<td class="text-right">
		       							<span id="no_of_worked_days">0</span>
		       						</td>
		       						<td class="text-right">
		       							<span id="gsis_no_of_worked_days">0</span>
		       						</td>
		       					</tr>
		       				</tbody>
		       			</table>
		       		</div>
		       		<div class="col-md-6" style="padding: 20px;">
		       			<table class="table borderless" style="border: 2px solid #333;">
		       				<tbody>
		       					<tr>
		       						<td colspan="4" class="text-center">Initial Salary for: <span id="date_initial_salary"></span></td>
		       					</tr>
		       					<tr>
		       						<td colspan="2"></td>
		       						<td colspan="2">(Less)</td>
		       					</tr>
		       					<tr>
		       						<td>Basic</td>
		       						<td class="text-right">
		       							<span id="initial_basic_amount">0.00</span>
		       						</td>
		       						<td>W/Tax</td>
		       						<td class="text-right">
		       							<span id="initial_wtax_amount">0.00</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td>PERA</td>
		       						<td class="text-right">
		       							<span id="initial_pera_amount">0.00</span>
		       						</td>
		       						<td>GSIS</td>
		       						<td class="text-right">
		       							<span id="initial_gsis_amount">0.00</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td style="font-weight: bold;">Total</td>
		       						<td class="text-right" style="border-top: 1px solid #333 !important;">
		       							<span id="initial_gross_total" style="font-weight: bold;">0.00</span>
		       						</td>
		       						<td>Philhealth</td>
		       						<td class="text-right">
		       							<span id="initial_philhealth_amount">0.00</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td colspan="2"></td>
		       						<td>Pagibig</td>
		       						<td class="text-right">
		       							<span id="initial_pagibig_amount">0.00</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td colspan="2"></td>
		       						<td style="font-weight: bold;">Total Deduct</td>
		       						<td class="text-right" style="border-top: 1px solid #333 !important;">
		       							<span id="initial_total_deduct">0.00</span>
		       						</td>
		       					</tr>
		       					<tr style="font-weight: bold;">
		       						<td>Gross Pay</td>
		       						<td class="text-right" style="border-bottom: 1px solid #333 !important;">
		       							<span id="initial_gross_pay">0.00</span>
		       						</td>
		       						<td colspan="2"></td>
		       					</tr>
		       					<tr style="font-weight: bold;">
		       						<td>Net Pay</td>
		       						<td class="text-right" style="border-bottom: 2px solid #333 !important;">
		       							<span id="initial_net_pay">0.00</span>
		       						</td>
		       						<td colspan="2"></td>
		       					</tr>
		       				</tbody>
		       			</table>
		       		</div>
		       </div>
		       <div class="row">
		       		<div class="col-md-12">
		       			<table class="table borderless" style="border:none;">
		       				<tbody style="border-top:none;">
		       					<tr>
		       						<td colspan="2">Deductions</td>
		       						<td colspan="3" class="text-center" style="border-bottom: 1px solid #333;border-right: 1px solid #333;">Personal Share</td>
		       						<td colspan="3" class="text-center" style="border-bottom: 1px solid #333;border-left: 1px solid #333;">Government Share</td>
		       						<td colspan="2"></td>
		       					</tr>
		       					<tr>
		       						<td colspan="3"></td>
		       						<td class="text-center">For Period</td>
		       						<td class="text-center">Regular</td>
		       						<td></td>
		       						<td class="text-center">For Period</td>
		       						<td class="text-center">Regular</td>
		       						<td>PS</td>
		       						<td>
		       							<span id="ps">0.00</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td>GSIS</td>
		       						<td>Yes</td>
		       						<td>9.0%</td>
		       						<td class="text-right">
		       							<span id="gsis_ps_period">0.00</span>
		       						</td>
		       						<td class="text-right">
		       							<span id="gsis_ps_regular">0.00</span>
		       						</td>
		       						<td class="text-right">12%</td>
		       						<td class="text-right">
		       							<span id="gsis_gs_period">0.00</span>
		       						</td>
		       						<td class="text-right">
		       							<span id="gsis_gs_regular">0.00</span>
		       						</td>
		       						<td>GS</td>
		       						<td>
		       							<span id="gs">0.00</span>
		       						</td>
		       					</tr>
		       					<tr>
		       						<td>Philhealth</td>
		       						<td>Yes</td>
		       						<td>1.4%</td>
		       						<td class="text-right">
		       							<span id="phic_ps_period">0.00</span>
		       						</td>
		       						<td class="text-right">
		       							<span id="phic_ps_regular">0.00</span>
		       						</td>
		       						<td colspan="5"></td>
		       					</tr>
		       					<tr>
		       						<td>Pagibig</td>
		       						<td>Yes</td>
		       						<td>100%</td>
		       						<td class="text-right">
		       							<span id="pagibig_ps_period">0.00</span>
		       						</td>
		       						<td class="text-right">
		       							<span id="pagibig_ps_regular">0.00</span>
		       						</td>
		       						<td colspan="5"></td>
		       					</tr>
		       					<tr>
		       						<td colspan="3"></td>
		       						<td class="text-right" style="font-weight: bold;border-top: 1px solid #333 !important;">
		       							<span id="ps_regular_total">0.00</span>
		       						</td>
		       						<td class="text-right" style="font-weight: bold;border-top: 1px solid #333 !important;">
		       							<span id="gs_regular_total">0.00</span>
		       						</td>
		       						<td colspan="5"></td>
		       					</tr>
		       				</tbody>
		       			</table>
		       		</div>
		       </div>
		       <div class="row">
		       		<div class="col-md-4">
		       			<div class="form-group" style="font-size: 12px;">
		       				Initial Salary Attachments:
		       				<ol>
		       					<li> Daily Time Record</li>
		       					<li> Assumption to Duty</li>
		       					<li> Appointment</li>
		       					<li> Oath of Office</li>
		       					<li> SALN</li>
		       					<li> BIR 2305 (first employment)</li>
		       				</ol>
		       			</div>
		       		</div>
		       		<div class="col-md-8">
		       			<span style="font-weight: bold;font-size: 12px;">Certified Correct:</span>
		       		</div>
		       </div>
		       <div class="row">
		       		<div class="col-md-4"></div>
		       		<div class="col-md-4" style="font-size: 12px;">
		       			<span style="font-weight: bold;" id="sign_left"></span> <br>
		       			<span id="pos_left"></span>
		       		</div>
		       		<div class="col-md-4" style="font-size: 12px;">
		       			<span style="font-weight: bold;" id="sign_right"></span> <br>
		       			<span id="pos_right"></span>
		       		</div>
		       </div>
		       <div class="row">
		       		<div class="col-md-12" style="font-size: 12px;">
		       			<span style="font-weight: bold;"><i>FOR OBLIGATION PURPOSES ONLY</i></span> <br>
		       			<i>hrdd/<span id="hrrd_code"></span></i>
		       		</div>
		       </div>
	    </div>
	 </div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$('.datepicker').datepicker({
		dateFormat:'yy-mm-dd'
	})

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	var printDate;
	$('#print_date').on('change',function(){
		printDate = $(this).val().split('-');
	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})

	var signTopRight;
	var positionTopRight;
	$('#sign_top_right').change(function(){
		signTopRight = "";
		positionTopRight  = "";
		signTopRight = $(this).find(':selected').text()
		positionTopRight = $(this).find(':selected').data('position_top_right')

		$('#sign_right').text(signTopRight);
		$('#pos_right').text(positionTopRight);
	});

	var signTopLeft;
	var positionTopLeft;
	$('#sign_top_left').change(function(){
		signTopLeft = "";
		positionTopLeft  = "";
		signTopLeft = $(this).find(':selected').text()
		positionTopLeft = $(this).find(':selected').data('position_top_left')

		$('#sign_left').text(signTopLeft);
		$('#pos_left').text(positionTopLeft);
	});


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	var codeTwo;

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

		$('#hrrd_code').text(codeTwo);

	});

var months = {
	1:'January',
	2:'February',
	3:'March',
    4:'April',
    5:'May',
    6:'June',
    7:'July',
    8:'August',
    9:'September',
    10:'October',
    11:'November',
    12:'December'
}

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	$.ajax({
		url:base_url+module_prefix+module+'/show',
		data:{
			'id':_empid,
			'year':year,
			'month':month,
			'emp_type':emp_type,
			'emp_status':emp_status,
			'category':category,
			'searchby':searchby,
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			if(data !== null){

				firstname = (data.employees.firstname) ? data.employees.firstname : '';
				lastname = (data.employees.lastname) ? data.employees.lastname : '';
				middlename = (data.employees.middlename) ? data.employees.middlename : '';
				position = (data.employeeinformation.positions) ? data.employeeinformation.positions.Name : '';
				office = (data.employeeinformation.offices) ? data.employeeinformation.offices.Name : '';
				assumptionDate = (data.employeeinformation.assumption_date) ? data.employeeinformation.assumption_date : '';
				var d = new Date(assumptionDate)
				assumption_date = formatDate(d);

				fullname = firstname+' '+middlename+'. '+lastname;

				$('#employee_name').text(fullname.toUpperCase());
				$('#position').text(position)
				$('#office').text(office)
				$("#assumption_date").text(assumption_date);
				$("#start_date").text(assumption_date);

				basicInitialAmount = (data.basic_amount) ? data.basic_amount : 0;
				gsisInitialAmount = (data.gsis_cont_amount) ? data.gsis_cont_amount : 0;
				pagibigInitialAmount = (data.pagibig_amount) ? data.pagibig_amount : 0;
				peraInitialAmount = (data.pera_amount) ? data.pera_amount : 0;
				phicInitialAmount = (data.philhealth_cont_amount) ? data.philhealth_cont_amount : 0;
				taxInitialAmount = (data.tax_amount) ? data.tax_amount : 0;
				grossInitialAmount = (data.gross_amount) ? data.gross_amount : 0;
				deductionInitialAmount = (data.total_deduction_amount) ? data.total_deduction_amount : 0;
				netInitialAmount = (data.net_amount) ? data.net_amount : 0;

				basic_initial = (basicInitialAmount) ? commaSeparateNumber(parseFloat(basicInitialAmount).toFixed(2)) : '';
				gsis_initial = (gsisInitialAmount) ? commaSeparateNumber(parseFloat(gsisInitialAmount).toFixed(2)) : '';
				pagibig_initial = (pagibigInitialAmount) ? commaSeparateNumber(parseFloat(pagibigInitialAmount).toFixed(2)) : '';
				philhealth_initial = (phicInitialAmount) ? commaSeparateNumber(parseFloat(phicInitialAmount).toFixed(2)) : '';
				tax_initial = (taxInitialAmount) ? commaSeparateNumber(parseFloat(taxInitialAmount).toFixed(2)) : '';
				pera_initial = (peraInitialAmount) ? commaSeparateNumber(parseFloat(peraInitialAmount).toFixed(2)) : '';
				gross_initial = (grossInitialAmount) ? commaSeparateNumber(parseFloat(grossInitialAmount).toFixed(2)) : '';
				net_initial = (netInitialAmount) ? commaSeparateNumber(parseFloat(netInitialAmount).toFixed(2)) : '';
				deduct_initial = (deductionInitialAmount) ? commaSeparateNumber(parseFloat(deductionInitialAmount).toFixed(2)) : '';

				$('#initial_basic_amount').text(basic_initial)
				$('#initial_pera_amount').text(pera_initial)
				$('#initial_gsis_amount').text(gsis_initial)
				$('#initial_pagibig_amount').text(pagibig_initial)
				$('#initial_philhealth_amount').text(philhealth_initial)
				$('#initial_wtax_amount').text(tax_initial)
				$('#initial_gross_pay').text(gross_initial)
				$('#initial_total_deduct').text(deduct_initial)
				$('#initial_net_pay').text(net_initial)


				$('#btnModal').trigger('click');

			}else{

				swal({
					  title: "No Record Found",
					  type: "warning",
					  showCancelButton: false,
					  confirmButtonClass: "btn-danger",
					  confirmButtonText: "Yes",
					  closeOnConfirm: false

				});
			}

		}
	})

});

$(document).on('click','#print',function(){
	$('#reports').printThis();
})


function formatDate(date) {

  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return monthNames[monthIndex] +' '+day+ ' ' + year;
}

function ordinal_suffix_of(i) {
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

})
</script>
@endsection