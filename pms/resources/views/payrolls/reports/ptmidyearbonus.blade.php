@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscape.css')}}" media="print">

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes.covereddate')
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Certified Correct</span>
							<select class="form-control font-style2 select2" id="sign_top_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Approved Payment</span>
							<select class="form-control font-style2 select2" id="sign_mid_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_mid_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:600px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="margin-right: -5px;margin-left: -5px;">
	       			<div class="col-md-12" id="payroll_transfer">
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	});

	$('#select_year').trigger('change');
	$('#select_month').trigger('change');

	var signTopLeft;
	var posTopLeft;

	$('#sign_top_left').change(function(){
		signTopLeft = "";
		posTopLeft  = "";
		signTopLeft = $(this).find(':selected').text()
		posTopLeft = $(this).find(':selected').data('position_top_left')
	});

	var signTopMid;
	var posTopMid;

	$('#sign_mid_left').change(function(){
		signTopMid = "";
		posTopMid  = "";
		signTopMid = $(this).find(':selected').text()
		posTopMid = $(this).find(':selected').data('position_mid_left')


	});

	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr = 1;
						netCtr = 0;

						coveredPeriod = months[_Month]+" "+_Year;

						netMidYearAmount = 0;

						body += '<table class="table table2" style="border:none;">';
						body += '<thead>';
						body += '<tr>';
						body += '<td class="text-left" colspan="8" style="border-left:none;border-right:none;">';
						body += '<img src="{{ url("images/e2e_logo_header.png") }}" style="height: 50px;">';
	       				body += '<h5><b>PAYROLL TRANSFER-MID-YEAR BONUS</b></h5>';
	       				body += '<h6><b>Month/Year</b> <span id="month_year">'+coveredPeriod+'</span></h6></td>';
						body += '</tr>';
						body += '<tr class="text-center" style="font-weight:bold">';
						body += '<td></td>';
						body += '<td >EMPLOYEE NAME</td>';
						body += '<td>POSITION</td>';
						body += '<td>ASSUMPTION</td>';
						body += '<td>BASIC SALARY</td>';
						body += '<td>W/TAX</td>';
						body += '<td>NET PAY</td>';
						body += '</tr>';
						body += '</thead>';

						body += '<tbody>';
						$.each(data.transaction,function(key,val){


							body += '<tr>';
							body += '<td></td>'
							body += '<td style="font-weight:bold;" colspan="7">'+key+'</td>'
							body += '</tr>';

							subMidYearAmount = 0;

							// ======== BODY =======
							$.each(val,function(k,v){


								lastname = (v.employees) ? v.employees.lastname : '';
								firstname = (v.employees) ? v.employees.firstname : '';
								middlename = (v.employees) ? v.employees.middlename : '';
								fullname = lastname+' '+firstname+' '+middlename;
								position = (v.positions) ? v.positions.Name : '';
								assumptionDate = (v.employeeinformation) ? v.employeeinformation.assumption_date : '';

								midYearAmount = (v.amount) ? v.amount : 0;

								// ===== SUB TOTAL COMPUTATION =====

								subMidYearAmount += parseFloat(midYearAmount);

								mid_year_amount = (midYearAmount) ? commaSeparateNumber(parseFloat(midYearAmount).toFixed(2)) : '';

								// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====

								body += '<tr class="text-center">';
								body += '<td>'+ctr+'</td>'
								body += '<td class="text-left">'+fullname+'</td>'
								body += '<td>'+position+'</td>'
								body += '<td>'+assumptionDate+'</td>'
								body += '<td class="text-right">'+mid_year_amount+'</td>'
								body += '<td class="text-right"></td>'
								body += '<td class="text-right">'+mid_year_amount+'</td>'
								body += '</tr>';

								ctr++;

							});
							// ======== BODY =======

							ctr = parseInt(ctr) - 1;

							netCtr += parseInt(ctr);
							// ===== COMPUTE NET AMOUNT  =====

							netMidYearAmount += parseFloat(subMidYearAmount);

							sub_mid_year_amount = (subMidYearAmount) ? commaSeparateNumber(parseFloat(subMidYearAmount).toFixed(2)) : '';

							body += '<tr style="font-weight:bold;">';
							body += '<td class="text-center">'+ctr+'</td>'
							body += '<td></td>'
							body += '<td class="text-center">SUB TOTAL</td>'
							body += '<td></td>'
							body += '<td class="text-right">'+sub_mid_year_amount+'</td>'
							body += '<td class="text-right"></td>'
							body += '<td class="text-right">'+sub_mid_year_amount+'</td>'
							body += '</tr>';

							// ======= SUB TOTAL ======
							ctr = 1;

						});

						net_mid_year_amount = (netMidYearAmount) ? commaSeparateNumber(parseFloat(netMidYearAmount).toFixed(2)) : '';

						// ======= GRAND TOTAL ======
						body += '<tr  style="font-weight:bold;" class="style-td text-center">';
						body += '<td class="text-center">'+netCtr+'</td>'
						body += '<td>Total Employees</td>'
						body += '<td>GRAND TOTAL</td>' // appointment of nature
						body += '<td></td>'
						body += '<td class="text-right">'+net_mid_year_amount+'</td>'
						body += '<td class="text-right"></td>'
						body += '<td class="text-right">'+net_mid_year_amount+'</td>'
						body += '</tr>';
						// ======= GRAND TOTAL ======
						signTopLeft = (signTopLeft) ? signTopLeft : '';
						posTopLeft = (posTopLeft) ? posTopLeft : '';
						signTopMid = (signTopMid) ? signTopMid : '';
						posTopMid = (posTopMid) ? posTopMid : '';

						body +=	'<tr class="text-justify borderless" >';
						body +=	'<td colspan="8">';
						body += 'Certification: This is to certify that the aforementioned personnel has rendered at least a total or an aggregate of four (4) months of service from July 1 of the immediately preceding year to May 15 of the current year, remains in the government service as of May 15 of the current year and has obtained at least a satisfactory performance rating in the immediately preceding rating period, as stipulated under Items 5.1.1 to 5.1.3 of the Department of Budget and Management (DBM) Circular No. 2017-2 dated 8 May 2017.';
						body +=	'</td>';
						body +=	'</tr>';
						body += '<tr>';
						body +=	'<td style="border:none;padding-top:50px;" colspan="2" >Certificied Correct:</td>';
						body +=	'<td style="border:none;padding-top:50px;" colspan="6" >Approved for Payment:</td>';
						body += '</tr>';
						body += '<tr>';
						body +=	'<td style="border:none;padding-top:50px;" colspan="2">';
						body += 'Certified: Supporting documents complete and proper, <br> and cash available in the amount of Php _________________________.';
						body += '</td>';
						body +=	'<td style="border:none;padding-top:50px;" colspan="6">Certified: Each employee whose name appears on the payroll has been <br> paid the amount as indicated opposite his/her name.</td>';
						body += '</tr>';
						body += '<tr>';
						body += '<td style="border:none;padding-top:50px;" colspan="2"><b>'+signTopLeft+'</b> <br> '+posTopLeft+' </td>';
						body += '<td style="border:none;padding-top:50px;" colspan="6"><b>'+signTopMid+'</b> <br> '+posTopMid+'	</td>';
						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';
						body += '</tr>';
						body += '<tr class="text-left">';
						body += '<td style="border:none;padding-top:50px;" colspan="8" class="text-left"><i>AO-'+_Year+_Month+'-'+codeOne+' <br> hrdd/'+codeTwo+' </i></td>';
						body += '</tr>';

						body += '</tbody></table>';

						$('#payroll_transfer').html(body);

						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection