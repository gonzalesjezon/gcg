@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-12">
						<span>Employee Name</span>
						<div class="form-group">
							<select class="form-control select2" name="employee_id" id="employee_id" style="margin:auto;width: 100%;">
								<option value=""></option>
								@foreach($employeeinfo as $value)
								<option value="{{ $value->id }}">{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				@include('payrolls.reports.includes.covereddate')
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div style="width: 760px;margin: auto;" id="reports">
		       <div class="row" style="margin-left: -5px;">
   					<div class="col-md-8">
   						<img src="{{ url('images/e2e_logo_header.png') }}" style="height: 50px;">
   						<i></i>
   					</div>
   					<div class="col-md-4">
   						<span style="font-size: 12px;">
   							25/F Tower 1, Vertis North Corporate Center, <br>
							North Avenue, Quezon City 1105 <br>
							queries@phcc.gov.ph
   						</span>
   						</p>
   					</div>
		       </div>
		       <hr>
		       <div class="row" style="margin-left: -5px;">
		       		<div class="col-md-12 text-center">
		       			<span  style="font-size: 14px;font-weight: bold;">PAYSLIP</span> <br>
		       			<span id="for_month" style="font-size: 12px;"></span>
		       		</div>
		       </div>
		       <div style="height: 30px;"></div>
		       <div class="row" style="margin-left: -5px;font-size: 12px;">
		       		<div class="col-md-2">
		       			<span style="padding-left: 20px;">Name</span>
		       		</div>
		       		<div class="col-md-9">
		       			<span id="fullname"></span>
		       		</div>
		       </div>
		       <div class="row" style="margin-left: -5px;font-size: 12px;">
		       		<div class="col-md-2">
		       			<span style="padding-left: 20px;">Employee No.</span>
		       		</div>
		       		<div class="col-md-9">
		       			<span id="empno"></span>
		       		</div>
		       </div>
		       <div class="row" style="margin-left: -5px;font-size: 12px;">
		       		<div class="col-md-2">
		       			<span style="padding-left: 20px;">Position</span>
		       		</div>
		       		<div class="col-md-9">
		       			<span id="position"></span>
		       		</div>
		       </div>
		       <div class="row" style="margin-left: -5px;font-size: 12px;">
		       		<div class="col-md-2">
		       			<span style="padding-left: 20px;">Office</span>
		       		</div>
		       		<div class="col-md-9">
		       			<span id="office"></span>
		       		</div>
		       </div>
		       <hr>
		       <div class="row" style="margin-left: -5px;font-weight: bold;">
		       		<div class="col-md-6 text-right">
		       			<span>Monthly</span>
		       		</div>
		       		<div class="col-md-4 text-right">
		       			<span>Total</span>
		       		</div>
		       </div>
		       <div class="row" style="margin-left: -5px;font-size: 12px;">
		       		<div class="col-md-6">
		       			<span style="padding-left: 10px;font-weight: bold;">*EARNINGS*</span>
		       		</div>
		       </div>
		       <div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 15px;">Basic Salary</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="basic_salary"></span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 15px;">PERA</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="pera"></span>
					</div>
					<div class="col-md-4 text-right">
						<span id="totalearnings"></span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;font-size: 12px;">
		       		<div class="col-md-6">
		       			<span style="padding-left: 10px;font-weight: bold;">Mandatory Deductions</span>
		       		</div>
		        </div>
		        <div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 30px;">BIR Withholding Tax</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="taxamount"></span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 30px;">GSIS Contribution</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="gsiscontribution"></span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 30px;">HDMF Contribution</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="pagibigcontribution"></span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 30px;">PHIC Contribution</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="philhealtcontribution"></span>
					</div>
					<div class="col-md-2 text-right">
						<span id="totalcontribution"></span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;padding-top: 5px; font-size: 12px;">
		       		<div class="col-md-6">
		       			<span style="padding-left: 10px;font-weight: bold;">Other Deductions</span>
		       		</div>
		        </div>
		        <div id="loanlists" style="font-size: 12px;"></div>
		        <div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-3">
						<span style="padding-left: 30px;" class="hdmf2" id="hdmf2">HDMF MP2</span>
					</div>
					<div class="col-md-3 text-right">
						<span id="pagibig_two_amount" class="hdmf2"></span>
					</div>
					<div class="col-md-2 text-right">
						<span class="other_deduct_amount"></span>
					</div>
					<div class="col-md-2 text-right">
						<span id="total_deduction">0.00</span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-top: 20px;font-size: 12px;">
					<div class="col-md-6 text-right">
						<span style="font-weight: bold;">NET PAY</span>
					</div>
					<div class="col-md-4">
						<span id="netpay" style="position: absolute;right: -5px;font-weight: bold;"> </span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-6 text-right">
						<span  id="firsthalf">First Half</span>
					</div>
					<div class="col-md-4">
						<span id="firsthalf_amount" style="position: absolute;right: -5px;font-weight: bold;">0.00</span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;font-size: 12px;">
					<div class="col-md-6 text-right">
						<span  id="firsthalf">Second Half</span>
					</div>
					<div class="col-md-4">
						<span id="secondhalf_amount" style="position: absolute;right: -5px;font-weight: bold;">0.00</span>
					</div>
				</div>
				<div style="height: 5em;"></div>
				<div class="row form-group" style="margin-left: -5px;font-size: 10px;">
					<div class="col-md-12">
						<i >This is a computer generated document and does</i> <br>
						<i>not require any signature if without alterations</i>
					</div>
				</div>
				<div class="row form-group" style="margin-left: -5px; font-size: 10px;" >
					<div class="col-sm-6">
						<i id="ao"></i> <br>
						<i id="hrd"></i>
					</div>
					<div class="col-sm-6 text-right" >
						Run Date: <span id="run_date"></span>
					</div>
				</div>
	   	   </div>
		</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){

// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$('#select_year').trigger('change');
	$('#select_month').trigger('change');

	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})


var codeOne;
var codeTwo;
$(document).on('keyup','#code_1',function(){
	codeOne = "";
	codeOne = $(this).val();

});

$(document).on('keyup','#code_2',function(){
	codeTwo = "";
	codeTwo = $(this).val();

});

var months ={
	1:'January',
	2:'February',
	3:'March',
	4:'April',
	5:'May',
	6:'June',
	7:'July',
	8:'August',
	9:'September',
	10:'October',
	11:'November',
	12:'December',
}

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
	}else{

		$.ajax({
			url:base_url+module_prefix+module+'/getPayslip',
			data:{
				'id':_empid,
				'year':year,
				'month':month,
				'emp_type':emp_type,
				'emp_status':emp_status,
				'category':category,
				'searchby':searchby,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				if(data.transaction !== null){

					firstname = data.transaction.employees.firstname;
					lastname = data.transaction.employees.lastname;
					middlename = data.transaction.employees.middlename;
					employee_number = data.transaction.employees.employee_number;
					office = (data.transaction.offices) ? data.transaction.offices.Name : '';
					position = (data.transaction.positions !== null) ? data.transaction.positions.Name : '';

					fullname = lastname+' '+firstname+' '+middlename;

					basicSalary = (data.salaryinfo) ? data.salaryinfo.salary_new_rate : 0.00;
					peraAmount = (data.transaction.benefit_transactions) ? data.transaction.benefit_transactions.amount : 0;
					totalEarnings = (parseFloat(basicSalary) + parseFloat(peraAmount));
					taxAmount = (data.transaction.tax_amount) ? data.transaction.tax_amount : 0.00;
					taxAmountTwo = (data.transaction.additional_tax_amount) ? data.transaction.additional_tax_amount : 0.00;
					gsisContAmount = (data.employeeinfo.gsis_contribution) ? data.employeeinfo.gsis_contribution : 0.00;
					pagibigContAmount = (data.employeeinfo.pagibig_contribution) ? data.employeeinfo.pagibig_contribution : 0.00;
					pagibigPersonal = (data.employeeinfo.pagibig_personal) ? data.employeeinfo.pagibig_personal : 0;
					philhealthContAmount = (data.employeeinfo.philhealth_contribution) ? data.employeeinfo.philhealth_contribution : 0.00;
					pagibigTwoAmount = (data.employeeinfo.pagibig2) ? data.employeeinfo.pagibig2 : 0.00;

					pagibigContAmount = parseFloat(pagibigContAmount) + parseFloat(pagibigPersonal);

					totalTaxAmount = parseFloat(taxAmount) + parseFloat(taxAmountTwo);

					totalContAmount = (parseFloat(totalTaxAmount) + parseFloat(gsisContAmount) + parseFloat(pagibigContAmount) + parseFloat(philhealthContAmount) + parseFloat(pagibigTwoAmount));


					basic_salary = (basicSalary !== 0) ? commaSeparateNumber(parseFloat(basicSalary).toFixed(2)) : '';
					pera_amount = (peraAmount !== 0) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '';
					total_earnings = (totalEarnings !== 0) ? commaSeparateNumber(parseFloat(totalEarnings).toFixed(2)) : '';
					tax_amount = (totalTaxAmount !== 0) ? commaSeparateNumber(parseFloat(totalTaxAmount).toFixed(2)) : '';
					gsis_cont_amount = (gsisContAmount !== 0) ? commaSeparateNumber(parseFloat(gsisContAmount).toFixed(2)) : '';
					pagibig_cont_amount = (pagibigContAmount !== 0) ? commaSeparateNumber(parseFloat(pagibigContAmount).toFixed(2)) : '';
					philhealth_cont_amount = (philhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(philhealthContAmount).toFixed(2)) : '';
					total_cont_amount = (totalContAmount !== 0) ? commaSeparateNumber(parseFloat(totalContAmount).toFixed(2)) : '';
					pagibig_two_amount = (pagibigTwoAmount !== 0) ? commaSeparateNumber(parseFloat(pagibigTwoAmount).toFixed(2)) : '';

					$('#basic_salary').text(basic_salary);
					$('#pera').text(pera_amount);
					$('#totalearnings').text(total_earnings);
					$('#taxamount').text(tax_amount);
					$('#gsiscontribution').text(gsis_cont_amount);
					$('#pagibigcontribution').text(pagibig_cont_amount);
					$('#philhealtcontribution').text(philhealth_cont_amount);
					$('#totalcontribution').text(total_cont_amount);
					$('#pagibig_two_amount').text(pagibig_two_amount);

					if(pagibigTwoAmount == 0.00){
						$('.hdmf2').text('');
					}else{
						$('#hdmf2').text('HDMF 2');
					}

					codeOne = (codeOne) ? codeOne : '';
					codeTwo = (codeTwo) ? codeTwo : '';

					$('#for_month').text(months[_Month]+' '+_Year);
					$('#fullname').text(fullname);
					$('#empno').text(employee_number);
					$('#office').text(office);
					$('#position').text(position);
					$('#ao').text('AO-'+_Year+_Month+'-'+codeOne);
					$('#hrd').text('hrdd/'+codeTwo);
					$('#run_date').text(data.date_run);

					row = [];
					totalLoanAmount = 0;
					if(data.loaninfo.length !== 0){
						$.each(data.loaninfo,function(k,v){

							loanAmount = (v.amount) ? v.amount : 0;

							loan_amount = (loanAmount !== 0) ? commaSeparateNumber(parseFloat(loanAmount).toFixed(2)) : '';

							row += '<div class="row" style="margin-left: -5px;">';
							row +=	'<div class="col-md-3">';
							row +=		'<span style="padding-left: 30px;">'+v.loans.name+'</span>';
							row +=	'</div>';
							row +=	'<div class="col-md-3 text-right">';
							row +=		'<span>'+loan_amount+'</span>';
							row +=	'</div>';
							// row +=	'<div class="col-md-6 text-right">';
							// row +=		'<span>₱0.00</span>';
							// row +=	'</div>';
							row +=  '</div>';

							totalLoanAmount += parseFloat(loanAmount);
						});

						$('#loanlists').html(row);
					}

					eeAmount 	= 0;
					meAmount 	= 0;
					raAmount 	= 0;
					taAmount 	= 0;
					ceaAmount 	= 0;
					if(data.benefitinfo.length !== 0){
						$.each(data.benefitinfo,function(k,v){
							switch(v.benefits.code){
								case 'EE':
									eeAmount = (v.amount) ? v.amount : 0;
									break;
								case 'ME':
									meAmount = (v.amount) ? v.amount : 0;
									break;
								case 'REP':
									raAmount = (v.amount) ? v.amount : 0;
									break;
								case 'TRANSPO':
									taAmount = (v.amount) ? v.amount : 0;
									break;
								case 'CEA':
									ceaAmount = (v.amount) ? v.amount : 0;
									break;
							}
						})
					}

					emeAmount = parseFloat(eeAmount) + parseFloat(meAmount);

					cea_amount = (ceaAmount) ? commaSeparateNumber(parseFloat(ceaAmount).toFixed(2)) : '0.00';
					eme_amount = (emeAmount) ? commaSeparateNumber(parseFloat(emeAmount).toFixed(2)) : '0.00';
					ra_amount = (raAmount) ? commaSeparateNumber(parseFloat(raAmount).toFixed(2)) : '0.00';
					ta_amount = (taAmount) ? commaSeparateNumber(parseFloat(taAmount).toFixed(2)) : '0.00';

					total_loan_amount = (totalLoanAmount) ? commaSeparateNumber(parseFloat(totalLoanAmount).toFixed(2)) : '';

					$('#totalloans').text(total_loan_amount);

					$('#cea_amount').text(cea_amount);
					$('#eme_amount').text(eme_amount);
					$('#ra_amount').text(ra_amount);
					$('#ta_amount').text(ta_amount);

					otherDeductAmount = parseFloat(totalLoanAmount) + parseFloat(pagibigTwoAmount);
					other_deduct_amount = (otherDeductAmount !== 0) ? commaSeparateNumber(parseFloat(otherDeductAmount).toFixed(2)) : '';

					totalDeduction = parseFloat(totalContAmount) + parseFloat(totalLoanAmount) + parseFloat(pagibigTwoAmount);

					netPay = parseFloat(totalEarnings) - parseFloat(totalDeduction);

					total_deduction = (totalDeduction !== 0) ? commaSeparateNumber(parseFloat(totalDeduction).toFixed(2)) : '';
					net_pay = (netPay !== 0) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';

					dividedNetPay = parseFloat(netPay) / 2;

					getDecimal = parseFloat(dividedNetPay).toFixed(2).split('.');
					getDecimal = parseFloat(getDecimal[1])/100;
					secondHalfAmount = parseFloat(dividedNetPay) + getDecimal;

					secondHalfAmount = Math.round(secondHalfAmount * 100) /100;

					firsthalf_amount = (dividedNetPay !== 0) ? commaSeparateNumber(parseInt(dividedNetPay).toFixed(2)) : '';
					secondhalf_amount = (secondHalfAmount !== 0) ? commaSeparateNumber(secondHalfAmount.toFixed(2)) : '';
					$('.other_deduct_amount').text(other_deduct_amount);

					$('#total_deduction').text(total_deduction);
					$('#netpay').text(net_pay);
					$('#firsthalf_amount').text(firsthalf_amount);
					$('#secondhalf_amount').text(secondhalf_amount);

					$('#btnModal').trigger('click');

				}else{

					swal({
						  title: "No Record Found",
						  type: "warning",
						  showCancelButton: false,
						  confirmButtonClass: "btn-danger",
						  confirmButtonText: "Yes",
						  closeOnConfirm: false

					});
				}

			}
		})

	}


});

$(document).on('click','#print',function(){
	$('#reports').printThis();
})

})
</script>
@endsection