@extends('app-reports')

@section('reports-content')
<head>
<style type="text/css">
/*.table>tbody>tr>td{
	line-height: 0.428571;
}*/
.table2>thead>tr>td, .table2>tbody>tr>td{
	padding: 3px !important;
}
</style>
</head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table table2 borderless" id="loan-reports">
		<tr>
			<td >
				<div class="row"  style="margin-left: -5px;margin-right: -5px;">
					@include('payrolls.reports.includes._covered-date')
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<span>Sign Left</span>
						<div class="form-group" style="margin-top: 5px;">
							<select class="form-control font-style2 select2" id="sign_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<span>Sign Right</span>
						<div class="form-group" style="margin-top: 5px;">
							<select class="form-control font-style2 select2" id="sign_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<span>Sign Middle</span>
						<div class="form-group" style="margin-top: 5px;">
							<select class="form-control font-style2 select2" id="sign_middle">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_middle="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6" >
						<span>Document Tracking Number</span>
						<div class="form-group" style="margin-top: 5px;">
							<input type="text" name="code_1" id="code_1" class="form-control font-style2">
						</div>
					</div>
					<div class="col-md-6" >
						<span>Initial</span>
						<div class="form-group" style="margin-top: 5px;">
							<input type="text" name="code_2" id="code_2" class="form-control font-style2">
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:100%;width:100%;overflow-y:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid" id="reports">
	       		<table class="table" style="border: none;">
	       			<thead>
	       				<tr>
	       					<td colspan="15" style="border-left: none;border-right: none;border-top: none;">
	       						<div class="row" style="margin-left: -5px;margin-right: -5px;">
	       							<div class="col-md-12">
	       								<h5 style="font-weight: bold;">PHILIPPINE COMPETITION COMMISSION</h5>
	       								<h5 style="font-weight: bold;">PAYROLL ON OVERTIME SERVICES RENDERED</h5>
	       								<h5 style="font-weight: bold;">PERIOD: <span class="for_month"></span></h5>
	       							</div>
	       						</div>
	       					</td>
	       				</tr>
	       				<tr class="text-center" >
	       					<td rowspan="2" style="vertical-align: middle;">LBP S/A Number</td>
	       					<td rowspan="2" style="vertical-align: middle;">Employees Name</td>
	       					<td rowspan="2" style="vertical-align: middle;">Basic Salary</td>
	       					<td colspan="2">Rate Per Hour</td>
	       					<td colspan="2">Number of Hours</td>
	       					<td colspan="2">Total</td>
	       					<td rowspan="2" style="vertical-align: middle;">Total Gross</td>
	       					<td rowspan="2" style="vertical-align: middle;">Less Tax(15%)</td>
	       					<td rowspan="2" style="vertical-align: middle;">Computed OT Pay</td>
	       					<td rowspan="2" style="vertical-align: middle;">Allowed OT Pay</td>
	       					<td rowspan="2" style="vertical-align: middle;">OT Pay</td>
	       					<td rowspan="2">Balance to <br> be carried over</td>
	       				</tr>
	       				<tr class="text-center">
	       					<td>125%</td>
	       					<td>150%</td>
	       					<td>125%</td>
	       					<td>150%</td>
	       					<td>125%</td>
	       					<td>150%</td>
	       				</tr>
	       			</thead>
	       			<tbody id="tbl_body"></tbody>
	       		</table>
	       		<div class="row" style="margin-left: -5px;margin-right: -5px;font-size: 12px;margin-top: 2em;">
	       			<div class="col-md-4">
	       				<b>Certified:</b> Services rendered per submitted <br>
						Daily Time Records approved by their <br>
						Directors/Supervisors.
	       			</div>
	       			<div class="col-md-4">
	       				Approved for Payment
	       			</div>
	       			<div class="col-md-4">
	       				<b>Certified:</b> Supporting documents complete and <br>
						proper, and cash available in the amount of <br>
						Php _________________________.
	       			</div>
	       		</div>
	       		<div class="row" style="margin-left: -5px;margin-right: -5px;font-size: 12px;margin-top: 5em;">
	       			<div class="col-md-4">
	       				<span class="sign_left" style="font-weight: bold;"></span> <br>
	       				<span class="pos_left"></span>
	       			</div>
	       			<div class="col-md-4">
	       				<span class="sign_mid" style="font-weight: bold;"></span> <br>
	       				<span class="pos_mid"></span>
	       			</div>
	       			<div class="col-md-4">
	       				<span class="sign_right" style="font-weight: bold;"></span> <br>
	       				<span class="pos_right"></span>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-left: -5px;margin-right: -5px;font-size: 12px;margin-top: 5em;">
	       			<div class="col-md-4">
	       				<i>
	       					AO-<span class="month_code"></span>-<span class="code_1"></span> <br>
	       					hrdd/<span class="code_2"></span>
	       				</i>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>
<!-- 0.328571 -->
@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){

	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	});

	$('.select2').select2();

	$('#select_month').trigger('change');
	$('#select_year').trigger('change');

	var signLeft;
	var posLeft;
	$('#sign_left').change(function(){
		signLeft = "";
		posLeft  = "";
		signLeft = $(this).find(':selected').text();
		posLeft = $(this).find(':selected').data('position_left');
		$('.sign_left').text(signLeft)
		$('.pos_left').text(posLeft)

	});

	var signRight;
	var posRight;
	$('#sign_left').change(function(){
		signRight = "";
		posRight  = "";
		signRight = $(this).find(':selected').text();
		posRight = $(this).find(':selected').data('position_right');
		$('.sign_right').text(signRight)
		$('.pos_right').text(posRight)

	});

	var signMid;
	var posMid;
	$('#sign_middle').change(function(){
		signMid = "";
		posMid  = "";
		signMid = $(this).find(':selected').text();
		posMid = $(this).find(':selected').data('position_middle');
		$('.sign_mid').text(signMid)
		$('.pos_mid').text(posMid)

	});


	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();
		$('.code_1').text(codeOne);

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();
		$('.code_2').text(codeTwo);

	});

var months ={
	1:'January',
	2:'February',
	3:'March',
	4:'April',
	5:'May',
	6:'June',
	7:'July',
	8:'August',
	9:'September',
	10:'October',
	11:'November',
	12:'December',
}


$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	if(!year || !month){
		swal({
			  title: "Select Year and Month First!",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false

		});
	}else{
		$.ajax({
			url:base_url+module_prefix+module+'/getOvertimeReport',
			data:{
				'id':_empid,
				'year':year,
				'month':month,
				'emp_type':emp_type,
				'emp_status':emp_status,
				'category':category,
				'searchby':searchby,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data)
				if(data.overtimeinfo.length !== 0){

					body = [];

					var subRegularAmount 	= 0;
					var subHolidayAmount 	= 0;
					var subOtherAmount 		= 0;
					var subGrossAmount 		= 0;
					var subMaxAmount 		= 0;
					var subOvertimePay 		= 0;
					var subBalanceForward 	= 0;

					$.each(data.overtimeinfo,function(k,v){

						firstname = (v.employees.firstname) ? v.employees.firstname : '';
						lastname = (v.employees.lastname) ? v.employees.lastname : '';
						middlename = (v.employees.middlename) ? v.employees.middlename : '';
						fullname = lastname+' '+firstname+' '+middlename;
						monthlyRate = (v.employeeinfo.monthly_rate_amount) ? v.employeeinfo.monthly_rate_amount : 0;
						atm_no = (v.employeeinfo.atm_no) ? v.employeeinfo.atm_no : '';
						regular_rate = (v.regular_rate) ? v.regular_rate : 0;
						holiday_rate = (v.holiday_rate) ? v.holiday_rate : 0;
						actual_regular_rate = (v.actual_regular_rate) ? v.actual_regular_rate : 0;
						actual_holiday_rate = (v.actual_holiday_rate) ? v.actual_holiday_rate : 0;
						actualHolidayAmount = (v.actual_holiday_amount) ? v.actual_holiday_amount : 0;
						actualRegularAmount = (v.actual_regular_amount) ? v.actual_regular_amount : 0;
						actualOtherAmount = (v.actual_other_amount) ? v.actual_other_amount : 0;
						maxAmountToAvail = (v.max_amount_to_avail) ? v.max_amount_to_avail : 0;
						overtimePay = (v.overtime_pay_for_period) ? v.overtime_pay_for_period : 0;
						balanceForward = (v.balance_forwarded) ? v.balance_forwarded : 0;

						totalGross = parseFloat(actualRegularAmount) + parseFloat(actualHolidayAmount) + parseFloat(actualOtherAmount);

						subRegularAmount += parseFloat(actualRegularAmount);
						subHolidayAmount += parseFloat(actualHolidayAmount);
						subOtherAmount += parseFloat(actualOtherAmount);
						subGrossAmount += parseFloat(totalGross);
						subMaxAmount += parseFloat(maxAmountToAvail);
						subOvertimePay += parseFloat(overtimePay);
						subBalanceForward += parseFloat(balanceForward);

						monthly_rate_amount = (monthlyRate) ? commaSeparateNumber(parseFloat(monthlyRate).toFixed(2)) : '';
						actual_regular_amount = (actualRegularAmount) ? commaSeparateNumber(parseFloat(actualRegularAmount).toFixed(2)) : '';
						actual_holiday_amount = (actualHolidayAmount) ? commaSeparateNumber(parseFloat(actualHolidayAmount).toFixed(2)) : '';
						total_gross = (totalGross) ? commaSeparateNumber(parseFloat(totalGross).toFixed(2)) : '';
						max_amount = (maxAmountToAvail) ? commaSeparateNumber(parseFloat(maxAmountToAvail).toFixed(2)) : '';
						overtime_pay = (overtimePay) ? commaSeparateNumber(parseFloat(overtimePay).toFixed(2)) : '';
						balance_forward = (balanceForward) ? commaSeparateNumber(parseFloat(balanceForward).toFixed(2)) : '';

						body += '<tr class="text-right">';
						body += '<td class="text-center">'+atm_no+'</td>';
						body += '<td class="text-center">'+fullname+'</td>';
						body += '<td>'+monthly_rate_amount+'</td>';
						body += '<td>'+regular_rate+'</td>';
						body += '<td>'+holiday_rate+'</td>';
						body += '<td>'+actual_regular_rate+'</td>';
						body += '<td>'+actual_holiday_rate+'</td>';
						body += '<td>'+actual_regular_amount+'</td>';
						body += '<td>'+actual_holiday_amount+'</td>';
						body += '<td>'+total_gross+'</td>';
						body += '<td></td>'; // tax
						body += '<td>'+total_gross+'</td>';
						body += '<td>'+max_amount+'</td>';
						body += '<td>'+overtime_pay+'</td>';
						body += '<td>'+balance_forward+'</td>';
						body += '</tr>';
					});

					sub_regular_amount = (subRegularAmount) ? commaSeparateNumber(parseFloat(subRegularAmount).toFixed(2)) : '';
					sub_holiday_amount = (subHolidayAmount) ? commaSeparateNumber(parseFloat(subHolidayAmount).toFixed(2)) : '';
					sub_gross = (subGrossAmount) ? commaSeparateNumber(parseFloat(subGrossAmount).toFixed(2)) : '';
					sub_max_amount = (subMaxAmount) ? commaSeparateNumber(parseFloat(subMaxAmount).toFixed(2)) : '';
					sub_overtime_pay = (subOvertimePay) ? commaSeparateNumber(parseFloat(subOvertimePay).toFixed(2)) : '';
					sub_balance_forward = (subBalanceForward) ? commaSeparateNumber(parseFloat(subBalanceForward).toFixed(2)) : '';


					body += '<tr class="text-right" style="font-weight:bold;">';
					body += '<td></td>';
					body += '<td colspan="6" class="text-left" style="border-left:none;">TOTAL</td>';
					body += '<td>'+sub_regular_amount+'</td>';
					body += '<td>'+sub_holiday_amount+'</td>';
					body += '<td>'+sub_gross+'</td>';
					body += '<td></td>'; // tax
					body += '<td>'+sub_gross+'</td>';
					body += '<td>'+sub_max_amount+'</td>';
					body += '<td>'+sub_overtime_pay+'</td>';
					body += '<td>'+sub_balance_forward+'</td>';
					body += '</tr>';

					body += '<tr class="text-right" style="font-weight:bold;">';
					body += '<td colspan="15" class="text-left">*Following the provision on Financial cap per Item 10.1 of PCC Office Circular No. 2017-12-001</td>';
					body += '</tr>';

					$('#tbl_body').html(body);

					month_code = _Year+_Month;
					$('.month_code').text(month_code);
					$('.for_month').text(months[_Month]+' '+_Year)
					$('#btnModal').trigger('click');

				}else{
					swal({
						  title: "No Records Found!",
						  type: "warning",
						  showCancelButton: false,
						  confirmButtonClass: "btn-warning",
						  confirmButtonText: "Yes",
						  closeOnConfirm: false

					});
				}
			}
		})
	}


});

$(document).on('click','#print',function(){
	$('#reports').printThis();
})
})
</script>
@endsection