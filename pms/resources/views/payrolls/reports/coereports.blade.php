@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-12">
						<span>Employee Name</span>
						<div class="form-group">
							<select class="form-control select2" name="employee_id" id="employee_id" style="margin:auto;width: 100%;">
								<option value=""></option>
								@foreach($employee as $value)
								<option value="{{ $value->id }}">{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Date</span>
							<input type="text" name="print_date" id="print_date" class="form-control font-style2 datepicker">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory</span>
							<select class="form-control font-style2 select2" id="sign_top_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}, {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Degree</span>
							<input type="text" name="add_degree" id="add_degree" class="form-control font-style2">
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6" style="padding-top: 10px;">
						<div class="form-group">
							<span>Document Tracking Number</span>
							<input type="text" name="code_1" id="code_1" class="form-control font-style2">
						</div>
					</div>
					<div class="col-md-6" style="padding-top: 10px;">
						<div class="form-group">
							<span>Initial</span>
							<input type="text" name="code_2" id="code_2" class="form-control font-style2">
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div style="width: 670px;margin: auto;" id="reports">
		       <div class="row">
   					<div class="col-md-8">
   						<img src="{{ url('images/e2e_logo_header.png') }}" style="height: 50px;">
   						<i></i>
   					</div>
   					<div class="col-md-4 text-right" style="font-size: 12px;">
   						<span>25/F Vertis North Corporate Center 1</span> <br>
   						<span>North Avenue, Quezon City 1105</span>
   						<span>queries@phcc.gov.ph</span> <br>
   						<span>(+632) 7719-PCC (7719-722)</span>
   					</div>
		       </div>
		       <hr>
		       <div class="row">
		       		<div class="col-md-12 text-center">
		       			<span style="font-weight: bold;border-bottom: 2px solid;font-size: 16px;">CERTIFICATION</span>
		       		</div>
		       </div>
		       <div class="row" style="margin-top: 30px;">
		       		<div class="col-md-12">
		       			<p style="text-align: justify;color: #000;">
		       				This is to certify that <span id="employee_name" style="font-weight: bold;"></span> is a <span id="employeestatus"></span> employee of the Philippine Competition Commission (PCC). He/She has been with the Commission from  <span id="assumption_date"></span> to present, and is currently appointed as <span id="position"></span>. under the <span id="offices"></span>
		       			</p>
		       		</div>
		       </div>
		      <!--  <div class="row" style="margin-top: 30px;">
		       		<div class="col-md-12">
		       			<table class="table">
		       				<thead class="text-center" style="font-weight: bold;">
		       					<tr>
		       						<td>ITEM</td>
		       						<td>AMOUNT (in PHP)</td>
		       					</tr>
		       				</thead>
		       				<tbody id="tbl_tbody">
		       				</tbody>
		       			</table>
		       		</div>
		       </div> -->
		       <div class="row">
		       		<div class="col-md-12">
		       			<p style="text-indent: 50px;text-align: justify;color: #000;">
		       				This certification is being issued upon the request of Mr/Ms. <span id="surname"></span> for whatever legal purpose it may serve.
		       			</span>

		       			<p style="text-indent: 50px;text-align: justify;color: #000;">
		       				Issued this <span id="day"></span> day of <span id="month_year"></span>, in Pasig City, Philippines.
		       			</p>
		       		</div>
		       </div>
		       <div class="row" style="margin-top: 30px;">
		       		<div class="col-md-6">
		       			<span style="font-weight: bold;" id="sign_right"></span> <span id="degree"></span><br>
		       			<span id="pos_right"></span>
		       		</div>
		       		<div class="col-md-6"></div>
		       </div>
		       <div class="row" style="margin-top: 50px;">
		       		<div class="col-md-6">
		       			<span>
		       				<i style="font-size: 11px;">
		       					AO-<span id="yearCode"></span>-<span id="codeOne"></span> <br>
								hrrd/<span id="codeTwo"></span>
		       				</i>
						</span>
		       		</div>
		       </div>
		       <div class="row" style="margin-top: 50px;">
		       		<div class="col-md-6">
		       			<span>
		       				<i style="font-size: 11px;">
		       					For verification you may call HRDD <br>
								At(+632) 635-6734 or email at hrdd@phcc.gov.ph
		       				</i>
						</span>
		       		</div>
		       </div>
	    </div>
	 </div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$('.datepicker').datepicker({
		dateFormat:'yy-mm-dd'
	});

	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	var addDegree;
	$('#add_degree').on('change',function(){
		addDegree = $(this).val();
	})

	var printDate;
	$('#print_date').on('change',function(){
		printDate = $(this).val().split('-');
	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})

	var signTopRight;
	var positionTopRight;
	$('#sign_top_right').change(function(){
		signTopRight = "";
		positionTopRight  = "";
		signTopRight = $(this).find(':selected').text()
		positionTopRight = $(this).find(':selected').data('position_top_right')
	});


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});
var months = {
	1:'January',
	2:'February',
	3:'March',
    4:'April',
    5:'May',
    6:'June',
    7:'July',
    8:'August',
    9:'September',
    10:'October',
    11:'November',
    12:'December'
}

var months2 ={
		'January':1,
		'February':2,
		'March':3,
		'April':4,
		'May':5,
		'June':6,
		'July':7,
		'August':8,
		'September':9,
		'October':10,
		'November':11,
		'December':12,
	}

$(document).on('click','#preview',function(){
	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	$.ajax({
		url:base_url+module_prefix+module+'/getCOE',
		data:{
			'id':_empid,
			'year':year,
			'month':month,
			'emp_type':emp_type,
			'emp_status':emp_status,
			'category':category,
			'searchby':searchby,
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			if(data !== null){

				firstname = (data.employees.firstname) ? data.employees.firstname : '';
				lastname = (data.employees.lastname) ? data.employees.lastname : '';
				middlename = (data.employees.middlename) ? data.employees.middlename : '';
				position = (data.employeeinformation.positions) ? data.employeeinformation.positions.Name : '';
				assumptionDate = (data.employeeinformation.assumption_date) ? data.employeeinformation.assumption_date : '';
				employeestatus = (data.employeeinformation.employeestatus) ? data.employeeinformation.employeestatus.Name : '';
				offices = (data.employeeinformation.offices) ? data.employeeinformation.offices.Name : '';
				var d = new Date(assumptionDate)
				assumption_date = formatDate(d);

				_month = months[parseInt(printDate[1])];
				_year = printDate[0];

				day = ordinal_suffix_of(printDate[2]);

				fullname = firstname+' '+middlename+'. '+lastname;

				$('#employee_name').text(fullname.toUpperCase());
				$('#position').text(position)
				$('#offices').text(offices)
				$('#employeestatus').text(employeestatus.toLowerCase());
				$("#assumption_date").text(assumption_date);
				$('#surname').text(lastname);
				$('#day').text(day);
				$('#month_year').text(_month+' '+_year);

				basicAmount = (data.monthly_rate_amount) ? data.monthly_rate_amount : 0;

				body = [];

				basic_amount = (basicAmount) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';

				body += '<tr>';
				body += '<td>BASIC SALARY</td>';
				body += '<td class="text-right">'+basic_amount+'</td>';
				body += '</tr>';

				benefitAmount = 0;
				$.each(data.benefitinfo,function(k,v){

					amount = (v.benefit_amount) ? v.benefit_amount : 0;

					benefitAmount += parseFloat(amount);

					benefit_amount = (amount) ? commaSeparateNumber(parseFloat(amount).toFixed(2)) : '';

					body += '<tr>';
					body += '<td>'+v.benefits.name+'</td>';
					body += '<td class="text-right">'+benefit_amount+'</td>';
					body += '</tr>';
				});

				totalAmount = parseFloat(basicAmount) + parseFloat(benefitAmount);
				total_amount = (totalAmount) ? commaSeparateNumber(parseFloat(totalAmount).toFixed(2)) : '';
				body += '<tr style="font-weight:bold;">';
				body += '<td>TOTAL COMPENSATION</td>';
				body += '<td class="text-right">'+total_amount+'</td>';
				body += '</tr>';

				signTopRight = (signTopRight) ? signTopRight : '';
				positionTopRight = (positionTopRight) ? positionTopRight : '';
				addDegree = (addDegree) ? ', '+addDegree : '';
				codeOne = (codeOne) ? codeOne : '';
				codeTwo = (codeTwo) ? codeTwo : '';
				yearCode = _year+months2[_month]

				$('#yearCode').text(yearCode);
				$('#codeOne').text(codeOne);
				$('#codeTwo').text(codeTwo);

				$('#sign_right').text(signTopRight);
				$('#pos_right').text(positionTopRight);
				$('#degree').text(addDegree);

				$('#tbl_tbody').html(body);


				$('#btnModal').trigger('click');

			}else{

				swal({
					  title: "No Record Found",
					  type: "warning",
					  showCancelButton: false,
					  confirmButtonClass: "btn-danger",
					  confirmButtonText: "Yes",
					  closeOnConfirm: false

				});
			}

		}
	})

});

$(document).on('click','#print',function(){
	$('#reports').printThis();
})


function formatDate(date) {

  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return monthNames[monthIndex] +' '+day+ ' ' + year;
}

function ordinal_suffix_of(i) {
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

})
</script>
@endsection