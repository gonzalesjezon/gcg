@extends('app-othercompensations')


@section('othercompensations-content')

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0" style="width: 1240px;">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row">
	       			<div class="col-md-6">
	       				<img src="{{ url('images/new-logo.jpg') }}" style="height: 80px;">
	       				<h5><b>{{ $title }}</b></h5>
	       				<h6><b>Period Covered:</b> <span id="month_year"></span></h6>
	       			</div>
					<table class="table">
						<thead class="text-center">
							<tr >
								<td rowspan="2"><b >#</b></td>
								<td rowspan="2"><b >EMPLOYEE NAME</b></td>
								<td rowspan="2"><b >POSITION</b></td>
								<td rowspan="2"><b >STATUS OF APPOINTMENT</b></td>
								<td rowspan="2"><b >ASSUMPTION</b></td>
								<td rowspan="2"><b>CIVIL STATUS</b></td>
								<td rowspan="2"><b >JG</b></td>
								<td colspan="5">SALARY</td>
								<td rowspan="2"><b >NET PAY</b></td>
							</tr>
							<tr>
								<td><b>BASIC</b></td>
								<td><b>MID YEAR</b></td>
								<td><b>1/2 CASH GIFT</b></td>
								<td><b>GROSS PAY</b></td>
								<td><b>WTAX</b></td>
							</tr>
						</thead>
						<tfoot >
						 	<tr style="border-top:1px solid #c0c0c0;border-bottom:1px solid #c0c0c0;">
						 		<td style="border:none;" colspan="6"></td>
						 		<td style="border:none;" class="text-right"><b>Grand Total</b></td>
						 		<td style="border:none;font-weight: bold" class="text-right"><span class="net_basic_pay"></span></td>
						 		<td style="border:none;font-weight: bold" class="text-right"><span class="net_basic_pay"></span></td>
						 		<td style="border:none;font-weight: bold" class="text-right"><span id="net_cash_gift"></span></td>
						 		<td style="border:none;font-weight: bold" class="text-right"><span class="net_gross_pay"></span></td>
						 		<td style="border:none;font-weight: bold" class="text-right"></td>
						 		<td style="border:none;font-weight: bold" class="text-right"><span class="net_gross_pay"></span></td>
						 		<!-- <td style="border:none;font-weight: bold"><span id="net_er_pagibig_share"></span></td> -->
						 <!-- 		<td style="border:none;font-weight: bold;border-right:1px solid #c0c0c0;"><span id="net_pagibig"></span></td> -->
						 	</tr>
						 	<tr class="text-left" >
						 		<td style="border:none;" colspan="3">
							 		Certified Correct
							 		<br><br><br><br><br>
							 		<b>GWEN GRECIA-DE VERA</b>
							 		<br>
							 		<span>Executive Director</span>
						 		</td>
						 		<td  style="border:none;" colspan="5">
							 		Approved for Payment
							 		<br><br><br><br><br>
							 		<b>GWEN GRECIA-DE VERA</b>
							 		<br>
							 		<span>Executive Director</span>
						 		</td>
						 		<td style="border:none;border-right: 1px solid #c0c0c0;" colspan="5">
							 		<span><b>Certified:</b> Supporting documents complete <br>and </span>
							 		<span>proper, and cash available in the amount of</span><br>
							 		<span>Php _________________________.</span><br><br><br>
							 		<span><b>CAROLYN V. AQUINO</b></span><br>
							 		<span>Accountant III, FPMO</span>
						 		</td>
						 	</tr>
					 	</tfoot>
					 <tbody id="tbl_body">
					 </tbody>
					</table>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getEmployeeinfo',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				console.log(data);

					if(data.length !== 0){
						arr = [];
						ctr = 0;
						net_ee_amount = 0;
						net_me_amount = 0;
						net_total 	  = 0;
						net_basic_pay = 0;
						net_mid_year  = 0;
						net_cash_gift = 0;
						net_gross_pay = 0;
						net_w_tax 	  = 0;

						$.each(data,function(key,val){
							sub_total_basic 	= 0;
							sub_total_mid_year 	= 0;
							sub_total_cash_gift = 0;
							sub_total_gross_pay = 0;
							sub_total_wtax 		= 0;
							sub_net_pay 		= 0;

							arr += '<tr style="border:1px solid #c0c0c0;">';
							arr += '<td style="border-right:none;"></td>';
							arr += '<td  colspan="12" style="border-left:none;"><b>'+key+'</b></td>';
							arr += '</tr>';

							$.each(val,function(k,v){
								net_pay 	= 0;
								basic_pay 	= 0;
								mid_year 	= 0;
								cash_gift 	= 0;
								gross_pay 	= 0;
								w_tax 		= 0;


							arr += '<tr style="border:1px solid #c0c0c0;" class="text-center">';
								arr += '<td >'+(ctr+1)+'</td>';
								arr += '<td >&nbsp;'+v.employees.lastname+' '+v.employees.firstname+' '+v.employees.middlename+'</td>';
								if(v.positionitems.position_id !== null){
									arr += '<td >&nbsp;'+v.positionitems.positions.name+'</td>';
								}else{
									arr += '<td ></td>';
								}
								arr += '<td ></td>';
								arr += '<td >'+v.employeeinformation.assumption_date+'</td>';
								arr += '<td ></td>';
								arr += '<td >&nbsp;'+v.salaryinfo.jobgrade.job_grade+'</td>';



								$.each(v.employeeinfo.benefitinfo,function(k1,v1){

									switch(v1.benefits.code){
										case 'CG':
											cash_gift = (v1.benefit_amount) ? (v1.benefit_amount/2) : 0;
										break;
									}
								})

								basic_pay = (v.salaryinfo.salary_new_rate) ? v.salaryinfo.salary_new_rate : 0;
								gross_pay = (v.gross_pay) ? v.gross_pay : 0;
								sub_total_basic += parseFloat(basic_pay);
								sub_total_cash_gift += parseFloat(cash_gift);
								sub_total_gross_pay += parseFloat(gross_pay);


								basic_pay = (basic_pay == 0) ? '-' : commaSeparateNumber(parseFloat(basic_pay).toFixed(2));
								cash_gift = (cash_gift == 0) ? '-' : commaSeparateNumber(parseFloat(cash_gift).toFixed(2));
								gross_pay = (gross_pay == 0) ? '-' : commaSeparateNumber(parseFloat(gross_pay).toFixed(2));


								arr += '<td  class="text-right">'+basic_pay+'</td>';
								arr += '<td  class="text-right">'+basic_pay+'</td>';
								arr += '<td  class="text-right">'+cash_gift+'</td>';
								arr += '<td  class="text-right">'+gross_pay+'</td>';
								arr += '<td  class="text-right"></td>';
								arr += '<td  class="text-right">'+gross_pay+'</td>';

							arr += '</tr>';
							ctr++;
							});
							ctr = 0;
							arr += '<tr style="border:1px solid #c0c0c0;font-weight:bold;" class="text-center">';
							arr += '<td style="border:none" colspan="6"></td>';
							arr += '<td style="border:none" class="text-right"><b></b></td>';
							net_basic_pay += parseFloat(sub_total_basic);
							net_cash_gift += parseFloat(sub_total_cash_gift);
							net_gross_pay += parseFloat(sub_total_gross_pay);
							sub_total_basic = (sub_total_basic) ? sub_total_basic : 0;
							sub_total_cash_gift = (sub_total_cash_gift) ? sub_total_cash_gift : 0;
							sub_total_gross_pay = (sub_total_gross_pay) ? sub_total_gross_pay : 0;

							sub_total_basic = (sub_total_basic == 0) ? '-' :commaSeparateNumber(parseFloat(sub_total_basic).toFixed(2));
							sub_total_cash_gift = (sub_total_cash_gift == 0) ? '-' :commaSeparateNumber(parseFloat(sub_total_cash_gift).toFixed(2));
							sub_total_gross_pay = (sub_total_gross_pay == 0) ? '-' :commaSeparateNumber(parseFloat(sub_total_gross_pay).toFixed(2));

							arr += '<td  class="text-right" style="border:none">'+sub_total_basic+'</td>';
							arr += '<td  class="text-right" style="border:none">'+sub_total_basic+'</td>';
							arr += '<td  class="text-right" style="border:none">'+sub_total_cash_gift+'</td>';
							arr += '<td  class="text-right" style="border:none">'+sub_total_gross_pay+'</td>';
							arr += '<td  class="text-right" style="border:none"></td>';
							arr += '<td  class="text-right" style="border:none">'+sub_total_gross_pay+'</td>';
							arr += '</tr>';
						});

						net_basic_pay = (net_basic_pay) ? net_basic_pay : 0;
						net_cash_gift = (net_cash_gift) ? net_cash_gift : 0;
						net_gross_pay = (net_gross_pay) ? net_gross_pay : 0;
						net_basic_pay = (net_basic_pay == 0) ? '-' : commaSeparateNumber(parseFloat(net_basic_pay).toFixed(2));
						net_cash_gift = (net_cash_gift == 0) ? '-' : commaSeparateNumber(parseFloat(net_cash_gift).toFixed(2));
						net_gross_pay = (net_gross_pay == 0) ? '-' : commaSeparateNumber(parseFloat(net_gross_pay).toFixed(2));

						$('.net_basic_pay').text(net_basic_pay);
						$('#net_cash_gift').text(net_cash_gift);
						$('.net_gross_pay').text(net_gross_pay);

						$('#tbl_body').html(arr);

						$('#month_year').text(_Month+' '+_Year);

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}


	});

$(document).on('click','#print',function(){
	$('#reports').printThis();
})
})
</script>
@endsection