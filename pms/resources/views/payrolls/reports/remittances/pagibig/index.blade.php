@extends('app-remittances')


@section('remittances-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}" media="print">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes.covereddate')
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Left</span>
							<select class="form-control font-style2 select2" id="sign_top_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Mid</span>
							<select class="form-control font-style2 select2" id="sign_mid_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_mid_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Right</span>
							<select class="form-control font-style2 select2" id="sign_top_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid" id="reports">
	       		<div class="row">
	       			<div class="col-md-12" id="payroll_transfer">
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){

	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	});

	$("#select_year").trigger('change');
	$("#select_month").trigger('change');

	var signTopLeft;
	var posTopLeft;

	$('#sign_top_left').change(function(){
		signTopLeft = "";
		posTopLeft  = "";
		signTopLeft = $(this).find(':selected').text()
		posTopLeft = $(this).find(':selected').data('position_top_left')

	});

	var signTopMid;
	var posTopMid;

	$('#sign_mid_left').change(function(){
		signTopMid = "";
		posTopMid  = "";
		signTopMid = $(this).find(':selected').text()
		posTopMid = $(this).find(':selected').data('position_mid_left')


	});

	var signTopRight;
	var posTopRight;

	$('#sign_top_right').change(function(){
		signTopRight = "";
		posTopRight  = "";
		signTopRight = $(this).find(':selected').text()
		posTopRight = $(this).find(':selected').data('position_top_right')


	});

	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getEmployeeinfo',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						arr_pagibig = [];
						arr_subtotal = [];
						ctr 		= 1;
						netCtr 		= 0;
						own_loan_id = 0;
						pagibigLoanCount = (data.pagibigLoanCount) ? data.pagibigLoanCount : 0;

						count2 = (parseFloat(pagibigLoanCount) + 3);
						count1 = (parseFloat(pagibigLoanCount) + 6);
						count3 = (parseFloat(pagibigLoanCount) + 6);

						netPagibigEEContAmount 	= 0;
						netPagibigERContAmount 	= 0;
						netPSAmount  			= 0;
						netPagibigAmount  		= 0;
						netPagibig2Amount  		= 0;

						coveredPeriod = months[_Month]+' '+_Year;

						body += '<table class="table table2" style="border:none;">'
						body += '<thead class="text-center" style="font-weight:bold;">';
						body += '<tr>';
						body += '<td class="text-left" colspan="'+count1+'" style="border-left:none;border-right:none;">';
						body += '<img src="{{ url("images/e2e_logo_header.png") }}" style="height: 35px;"> <br>';
						body += '<b>PAG-IBIG PREMIUMS and LOANS</b> <br> BP NUMBER: 206673990006';
						body += '</td>';
						body += '<td colspan="2" style="border-left:none;border-right:none;" class="text-right">';
						body += '<i>HRDD COPY</i> <br> <i>ACCOUNTING COPY</i> <br> <i>CASHIERS COPY</i>'
						body += '</td>';
						body += '</tr>';
						body +=	'<tr>';
						body += '<td rowspan="2">#</td>';
						body += '<td rowspan="2" >EMPLOYEE NAME</td>';
						body += '<td colspan="'+count2+'">PERSONAL SHARE (PS)</td>';
						body += '<td rowspan="2">GOVERNMENT SHARE</td>';
						body += '<td rowspan="2">TOTAL PAGIBIG</td>';
						body +=	'</tr>';
						body += '<tr>';
						body += '<td>PREMIUM</td>';
						body += '<td>MP2</td>';
						$.each(data.pagibigLoanList,function(k,v){
							body += '<td>'+v.loans.name+'</td>';
							arr_pagibig[v.loan_id+'_id'] = v.loan_id;
							arr_subtotal[v.loan_id+'_id']  = 0;
						});
						body += '<td>TOTAL PS</td>';
						body += '</tr>';
						body += '</thead>';

						body += '<tbody>';
						$.each(data.transaction,function(key,val){


							body += '<tr>';
							body += '<td></td>'
							body += '<td style="font-weight:bold;" colspan="'+count3+'">'+key+'</td>'
							body += '</tr>';

							subPagibigEEContAmount  = 0;
							subPagibigERContAmount  = 0;
							subPagibigAmount 		= 0;
							subPagibig2Amount 		= 0;
							subPSAmount 			= 0;

							$.each(val,function(k,v){

								firstname = (v.employees.firstname) ? v.employees.firstname : '';
								lastname = (v.employees.lastname) ? v.employees.lastname : '';
								middlename = (v.employees.middlename) ? v.employees.middlename : '';

								fullname = lastname+' '+firstname+' '+middlename;
								pagibigEEContAmount = (v.employeeinfo.pagibig_contribution) ? v.employeeinfo.pagibig_contribution : 0;
								pagibigERContAmount = (v.employeeinfo.er_pagibig_share) ? v.employeeinfo.er_pagibig_share : 0;
								pagibigTwoAmount = (v.employeeinfo.pagibig2) ? v.employeeinfo.pagibig2 : 0;
								ee_share = (pagibigEEContAmount) ? commaSeparateNumber(parseFloat(pagibigEEContAmount).toFixed(2)) : '0.00';
								er_share = (pagibigERContAmount) ? commaSeparateNumber(parseFloat(pagibigERContAmount).toFixed(2)) : '0.00';
								pagibig2_amount = (pagibigTwoAmount) ? commaSeparateNumber(parseFloat(pagibigTwoAmount).toFixed(2)) : '0.00';

								body += '<tr class="text-right">';
								body += '<td class="text-center">'+ctr+'</td>';
								body += '<td class="text-left">'+fullname+'</td>';
								body += '<td >'+ee_share+'</td>';
								body += '<td >'+pagibig2_amount+'</td>';
								loanAmount = 0;
								if(data.pagibigLoanList.length !== 0){
									$.each(data.pagibigLoanList,function(k2,v2){
										loan = [];
										loan_dispay = [];
										loan_id = v2.loan_id;

										$.each(v.loaninfo_transaction,function(k3,v3){
											own_loan_id = v3.loan_id;
											loan_dispay['loan_amount_'+own_loan_id] = v3.amount;

										});
										if(loan_id){
											if (loan_dispay['loan_amount_'+loan_id]) {
												arr_subtotal[loan_id+'_id'] += parseFloat(loan_dispay['loan_amount_'+loan_id]);
											}
											loan_dispay['loan_amount_'+loan_id] = (loan_dispay['loan_amount_'+loan_id]) ? commaSeparateNumber(parseFloat(loan_dispay['loan_amount_'+loan_id]).toFixed(2)) : '';

											body += '<td class="sub_total text-right"  >'+loan_dispay['loan_amount_'+loan_id]+'</td>'
										}else{
											body += '<td class="text-right"></td>'
										}
									});
								}


								totalPSAmount = parseFloat(pagibigEEContAmount) + parseFloat(pagibigTwoAmount) + parseFloat(loanAmount);
								totalPagibigAmount = parseFloat(totalPSAmount) + parseFloat(pagibigERContAmount);

								total_ps_amount = (totalPSAmount) ? commaSeparateNumber(parseFloat(totalPSAmount).toFixed(2)) : '0.00';
								total_pagibig_amount = (totalPagibigAmount) ? commaSeparateNumber(parseFloat(totalPagibigAmount).toFixed(2)) : '0.00';

								body += '<td >'+total_ps_amount+'</td>';
								body += '<td >'+er_share+'</td>';
								body += '<td >'+total_pagibig_amount+'</td>';
								body += '</tr>';

								subPagibigEEContAmount += parseFloat(pagibigEEContAmount);
								subPagibigERContAmount += parseFloat(pagibigERContAmount);
								subPSAmount += parseFloat(totalPSAmount);
								subPagibigAmount += parseFloat(totalPagibigAmount);
								subPagibig2Amount += parseFloat(pagibigTwoAmount);

								ctr++;
							});
							ctr = 1;


							netPagibigEEContAmount += parseFloat(subPagibigEEContAmount);
							netPagibigERContAmount += parseFloat(subPagibigERContAmount);
							netPSAmount += parseFloat(subPSAmount);
							netPagibigAmount += parseFloat(subPagibigAmount);
							netPagibig2Amount += parseFloat(subPagibig2Amount);


							sub_ee_share = (subPagibigEEContAmount) ? commaSeparateNumber(parseFloat(subPagibigEEContAmount).toFixed(2)) : '0.00';
							sub_er_share = (subPagibigERContAmount) ? commaSeparateNumber(parseFloat(subPagibigERContAmount).toFixed(2)) : '0.00';
							sub_pagibig2_amount = (subPagibig2Amount) ? commaSeparateNumber(parseFloat(subPagibig2Amount).toFixed(2)) : '0.00';
							sub_ps_amount = (subPSAmount) ? commaSeparateNumber(parseFloat(subPSAmount).toFixed(2)) : '0.00';
							sub_pagibig_amount = (subPagibigAmount) ? commaSeparateNumber(parseFloat(subPagibigAmount).toFixed(2)) : '0.00';

							body += '<tr style="font-weight:bold;" class="text-right">';
							body += '<td class="text-center"></td>';
							body += '<td class="text-center">SUB TOTAL</td>';
							body += '<td >'+sub_ee_share+'</td>';
							body += '<td >'+sub_pagibig2_amount+'</td>';
							if(data.pagibigLoanList.length !== 0){
							for (var k in arr_pagibig){
								    if (typeof arr_pagibig[k] !== 'function') {
								    	arr_subtotal[arr_pagibig[k]+'_id'] = (arr_subtotal[arr_pagibig[k]+'_id']) ? commaSeparateNumber(parseFloat(arr_subtotal[arr_pagibig[k]+'_id']).toFixed(2)) : '0.00';
								        body += '<td  class="text-right">'+arr_subtotal[arr_pagibig[k]+'_id']+'</td>';
								        arr_subtotal[arr_pagibig[k]+'_id'] = 0;
								    }
								}
							}

							body += '<td >'+sub_ps_amount+'</td>';
							body += '<td >'+sub_er_share+'</td>';
							body += '<td >'+sub_pagibig_amount+'</td>';
							body += '</tr>';

						});


						net_ee_share = (netPagibigEEContAmount) ? commaSeparateNumber(parseFloat(netPagibigEEContAmount).toFixed(2)) : '0.00';
						net_er_share = (netPagibigERContAmount) ? commaSeparateNumber(parseFloat(netPagibigERContAmount).toFixed(2)) : '0.00';
						net_pagibig2_amount = (netPagibig2Amount) ? commaSeparateNumber(parseFloat(netPagibig2Amount).toFixed(2)) : '0.00';
						net_ps_amount = (netPSAmount) ? commaSeparateNumber(parseFloat(netPSAmount).toFixed(2)) : '0.00';
						net_pagibig_amount = (netPagibigAmount) ? commaSeparateNumber(parseFloat(netPagibigAmount).toFixed(2)) : '0.00';

						body += '<tr  style="font-weight:bold;" class="style-td text-right">';
						body += '<td class="text-center"></td>';
						body += '<td class="text-center">GRAND TOTAL</td>';
						body += '<td >'+net_ee_share+'</td>';
						body += '<td >'+net_pagibig2_amount+'</td>';
						if(data.pagibigLoanList.length !== 0){
							$.each(data.pagibigLoanList,function(k2,v2){
								loan = [];
								loan_dispay = [];
								loan_id = v2.loan_id;

								body += '<td class="text-right">'+commaSeparateNumber(parseFloat(v2.net_amount).toFixed(2))+'</td>';
							});
						}

						body += '<td >'+net_ps_amount+'</td>';
						body += '<td >'+net_er_share+'</td>';
						body += '<td >'+net_pagibig_amount+'</td>';
						body += '</tr>';

						signTopLeft = (signTopLeft) ? signTopLeft : '';
						posTopLeft = (posTopLeft) ? posTopLeft : '';
						signTopMid = (signTopMid) ? signTopMid : '';
						posTopMid = (posTopMid) ? posTopMid : '';
						signTopRight = (signTopRight) ? signTopRight : '';
						posTopRight = (posTopRight) ? posTopRight : '';

						body += '<tr class="text-left">';
						body += '<td colspan="6" style="border:none;">Certified Correct</td>'
						body += '<td colspan="2" style="border:none;">Approved for Payment</td>'
						body += '</tr>';
						body += '<tr class="text-left">';
						body += '<td colspan="3" style="border:none;padding-top:50px;">'+signTopLeft+' <br> '+posTopRight+'</td>'
						body += '<td colspan="3" style="border:none;padding-top:50px;">'+signTopMid+' <br> '+posTopMid+'</td>'
						body += '<td colspan="2" style="border:none;padding-top:50px;">'+signTopRight+' <br> '+posTopRight+'</td>'
						body += '</tr>';
						body += '<tr class="text-left">';

						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';

						body += '<td style="border:none;padding-top:50px;"  class="text-left" colspan="'+(parseInt(count3)+4)+'"><i>AO-'+_Year+_Month+'-'+codeOne+' <br> hrdd/'+codeTwo+' </i></td>';
						body += '</tr>';

						body += '</tbody></table>';
						$('#payroll_transfer').html(body);

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$(document).on('click','#print',function(){
	$('#reports').printThis();
});

})
</script>
@endsection