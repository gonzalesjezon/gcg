@extends('app-remittances')


@section('remittances-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}" media="print">

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes.covereddate')
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Left</span>
							<select class="form-control font-style2 select2" id="sign_top_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Mid</span>
							<select class="form-control font-style2 select2" id="sign_mid_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_mid_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Right</span>
							<select class="form-control font-style2 select2" id="sign_top_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid" id="reports">
	       		<div class="row">
	       			<div class="col-md-12" id="payroll_transfer">
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	});

	$('#select_year').trigger('change');
	$('#select_month').trigger('change')

	var signTopLeft;
	var posTopLeft;

	$('#sign_top_left').change(function(){
		signTopLeft = "";
		posTopLeft  = "";
		signTopLeft = $(this).find(':selected').text()
		posTopLeft = $(this).find(':selected').data('position_top_left')

	});

	var signTopMid;
	var posTopMid;

	$('#sign_mid_left').change(function(){
		signTopMid = "";
		posTopMid  = "";
		signTopMid = $(this).find(':selected').text()
		posTopMid = $(this).find(':selected').data('position_mid_left')


	});

	var signTopRight;
	var posTopRight;

	$('#sign_top_right').change(function(){
		signTopRight = "";
		posTopRight  = "";
		signTopRight = $(this).find(':selected').text()
		posTopRight = $(this).find(':selected').data('position_top_right')


	});

	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/getEmployeeinfo',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr 		= 1;
						netCtr 		= 0;
						own_loan_id = 0;

						netPhicEEContAmount 	= 0;
						netPhicERContAmount 	= 0;
						netPHICAmount 			= 0;

						coveredPeriod = months[_Month]+' '+_Year;

						body += '<table class="table table2" style="border:none;">'
						body += '<thead class="text-center" style="font-weight:bold;">';
						body += '<tr>';
						body += '<td class="text-left" colspan="4" style="border-left:none;border-right:none;">';
						body += '<img src="{{ url("images/e2e_logo_header.png") }}" style="height: 35px;"> <br>';
						body += '<b>PHILHEALTH PREMIUMS</b> <br> BP NUMBER: 001020006998';
						body += '</td>';
						body += '<td colspan="2" style="border-left:none;border-right:none;" class="text-right">';
						body += '<i>HRDD COPY</i> <br> <i>ACCOUNTING COPY</i> <br> <i>CASHIERS COPY</i>'
						body += '</td>';
						body += '</tr>';
						body +=	'<tr>';
						body += '<td>#</td>';
						body += '<td >EMPLOYEE NAME</td>';
						body += '<td >PERSONAL SHARE</td>';
						body += '<td>GOVERNMENT SHARE</td>';
						body += '<td>TOTAL PHIC</td>';
						body +=	'</tr>';
						body += '</thead>';

						body += '<tbody>';
						$.each(data.transaction,function(key,val){

							body += '<tr>';
							body += '<td></td>'
							body += '<td style="font-weight:bold;" colspan="5">'+key+'</td>'
							body += '</tr>';

							subPhicEEContAmount 	= 0;
							subPhicERContAmount 	= 0;
							subPHICAmount 			= 0;

							$.each(val,function(k,v){

								firstname = (v.employees.firstname) ? v.employees.firstname : '';
								lastname = (v.employees.lastname) ? v.employees.lastname : '';
								middlename = (v.employees.middlename) ? v.employees.middlename : '';

								fullname = lastname+' '+firstname+' '+middlename;
								philhealhtEEContAmount = (v.employeeinfo.philhealth_contribution) ? v.employeeinfo.philhealth_contribution : 0;
								philhealhtERContAmount = (v.employeeinfo.er_philhealth_share) ? v.employeeinfo.er_philhealth_share : 0;
								totalPHICAmount = parseFloat(philhealhtEEContAmount) + parseFloat(philhealhtERContAmount);

								ee_share = (philhealhtEEContAmount) ? commaSeparateNumber(parseFloat(philhealhtEEContAmount).toFixed(2)) : '';
								er_share = (philhealhtERContAmount) ? commaSeparateNumber(parseFloat(philhealhtERContAmount).toFixed(2)) : '';
								total_ps_amount = (totalPHICAmount) ? commaSeparateNumber(parseFloat(totalPHICAmount).toFixed(2)) : '';

								body += '<tr class="text-right">';
								body += '<td class="text-center">'+ctr+'</td>';
								body += '<td class="text-left">'+fullname+'</td>';
								body += '<td >'+ee_share+'</td>';
								body += '<td >'+er_share+'</td>';
								body += '<td >'+total_ps_amount+'</td>';
								body += '</tr>';

								subPhicEEContAmount += parseFloat(philhealhtEEContAmount);
								subPhicERContAmount += parseFloat(philhealhtERContAmount);
								subPHICAmount += parseFloat(totalPHICAmount);

								ctr++;
							});
							ctr = 1;


							netPhicEEContAmount += parseFloat(subPhicEEContAmount);
							netPhicERContAmount += parseFloat(subPhicERContAmount);
							netPHICAmount += parseFloat(subPHICAmount);

							sub_ee_share = (subPhicEEContAmount) ? commaSeparateNumber(parseFloat(subPhicEEContAmount).toFixed(2)) : '';
							sub_er_share = (subPhicERContAmount) ? commaSeparateNumber(parseFloat(subPhicERContAmount).toFixed(2)) : '';
							sub_phic_amount = (subPHICAmount) ? commaSeparateNumber(parseFloat(subPHICAmount).toFixed(2)) : '';

							body += '<tr style="font-weight:bold;" class="text-right">';
							body += '<td class="text-center"></td>';
							body += '<td class="text-center">SUB TOTAL</td>';
							body += '<td >'+sub_ee_share+'</td>';
							body += '<td >'+sub_er_share+'</td>';
							body += '<td >'+sub_phic_amount+'</td>';
							body += '</tr>';

						});


						net_ee_share = (netPhicEEContAmount) ? commaSeparateNumber(parseFloat(netPhicEEContAmount).toFixed(2)) : '';
						net_er_share = (netPhicERContAmount) ? commaSeparateNumber(parseFloat(netPhicERContAmount).toFixed(2)) : '';
						net_ps_amount = (netPHICAmount) ? commaSeparateNumber(parseFloat(netPHICAmount).toFixed(2)) : '';

						body += '<tr  style="font-weight:bold;" class="style-td text-right">';
						body += '<td class="text-center"></td>';
						body += '<td class="text-center">GRAND TOTAL</td>';
						body += '<td >'+net_ee_share+'</td>';
						body += '<td >'+net_er_share+'</td>';
						body += '<td >'+net_ps_amount+'</td>';
						body += '</tr>';

						signTopLeft = (signTopLeft) ? signTopLeft : '';
						posTopLeft = (posTopLeft) ? posTopLeft : '';
						signTopMid = (signTopMid) ? signTopMid : '';
						posTopMid = (posTopMid) ? posTopMid : '';
						signTopRight = (signTopRight) ? signTopRight : '';
						posTopRight = (posTopRight) ? posTopRight : '';

						body += '<tr class="text-left">';
						body += '<td colspan="4" style="border:none;">Certified Correct</td>'
						body += '<td colspan="2" style="border:none;">Approved for Payment</td>'
						body += '</tr>';
						body += '<tr class="text-left">';
						body += '<td colspan="2" style="border:none;padding-top:50px;">'+signTopLeft+' <br> '+posTopRight+'</td>'
						body += '<td colspan="2" style="border:none;padding-top:50px;">'+signTopMid+' <br> '+posTopMid+'</td>'
						body += '<td colspan="2" style="border:none;padding-top:50px;">'+signTopRight+' <br> '+posTopRight+'</td>'
						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';
						body += '</tr>';
						body += '<tr class="text-left">';
						body += '<td style="border:none;padding-top:50px;"  class="text-left" colspan="6"><i>AO-'+_Year+_Month+'-'+codeOne+' <br> hrdd/'+codeTwo+' </i></td>';
						body += '</tr>';

						body += '</tbody></table>';
						$('#payroll_transfer').html(body);

						$('#btnModal').trigger('click');

					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});
$(document).on('click','#print',function(){
	$('#reports').printThis();
});

})
</script>
@endsection