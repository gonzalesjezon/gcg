@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}" media="print">
<style type="text/css">
	.table>tbody>tr>td{
		border: none !important;
	}

</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					@include('payrolls.reports.includes._covered-date')
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-12">
						<div class="form-group">
							<span>Pay Period</span>
							<select class="form-control select2" id="select_pay_period" name="pay_period">
								<option value=""></option>
								<option value="firsthalf">1st HALF SALARY</option>
								<option value="secondhalf">2nd HALF SALARY</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6" >
						<div class="form-group">
							<span>Document Tracking Number</span>
							<input type="text" name="code_1" id="code_1" class="form-control font-style2">
						</div>
					</div>
					<div class="col-md-6" >
						<div class="form-group">
							<span>Initial</span>
							<input type="text" name="code_2" id="code_2" class="form-control font-style2">
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-4">
						<div class="form-group">
							<span>Signatory Top Left</span>
							<select class="form-control font-style2 select2" id="sign_top_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<span>Signatory Top Right</span>
							<select class="form-control font-style2 select2" id="sign_top_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<span>Signatory Bottom Left</span>
							<select class="form-control font-style2 select2" id="sign_bot_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_bot_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-4">
						<div class="form-group">
							<span>Signatory Bottom Right</span>
							<select class="form-control font-style2 select2" id="sign_bot_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_bot_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<span>Degree</span>
							<input type="text" name="add_degree" id="add_degree" class="form-control font-style2">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<span>Signatory Bottom</span>
							<select class="form-control font-style2 select2" id="sign_bottom">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_bottom="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->firstname }} {{ $value->middlename }}. {{ $value->lastname }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid">
	       		<div class="row" id="reports" style="margin: 960px;margin: auto;">
	       			<table class="table table2" style="margin-top:10px;border:none;">
						<thead style="font-weight: bold;">
					 	<tr>
					 	<td colspan="9"  style="border-left:none;border-right:none;">
					 		<div class="row" style="margin-left: -5px;margin-right: -5px;">
					 			<div class="col-md-12" >
					 				<img src="{{ url('images/e2e_logo_header.png') }}" style="height: 35px;"> <br>
					 				<span style="font-size: 12px;"><b>{{ $title }}</b></span> <br>
			       					<b>Covered Period:</b> <span id="covered_period"></span>
					 			</div>
					 		</div>
					 	</td>
					 	</tr>
						<tr class="text-center">
							<td rowspan="2" style="vertical-align:middle;">#</td>
							<td rowspan="2" style="vertical-align:middle;">NAME OF EMPLOYEE</td>
							<td rowspan="2" style="vertical-align:middle;">ACCOUNT NUMBER</td>
							<td rowspan="2" style="vertical-align:middle;">NET PAY BEFORE <br> ADJUSTMENT</td>
							<td rowspan="2" style="vertical-align:middle;">NET PAY <br> <span class="pay_period"></span></td>
							<td colspan="2" style="vertical-align:middle">ADJUSTMENT</td>
							<td rowspan="2" style="vertical-align: middle;">REMARKS</td>
							<td rowspan="2" style="vertical-align:middle;">TOTAL SALARY FOR <br> <span class="pay_period"></span></td>
						</tr>
						<tr class="text-center">
							<td>ADD</td>
							<td>LESS</td>
						</tr>
					 	</thead>
					 	<tbody id="tbl_body"></tbody>
					 </table>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){

// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	var _payPeriod;
	$('.select2').select2();
	$('#select_pay_period').select2({
		allowClear:true,
	    placeholder: "Pay Period",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$('#select_year').trigger('change');
	$('#select_month').trigger('change');


	var signTopLeft;
	var posTopLeft;
	$('#sign_top_left').change(function(){
		signTopLeft = "";
		posTopLeft  = "";
		signTopLeft = $(this).find(':selected').text()
		posTopLeft = $(this).find(':selected').data('position_top_left')

	});

	var signTopRight;
	var posTopRight;
	$('#sign_top_right').change(function(){
		signTopRight = "";
		posTopRight  = "";
		signTopRight = $(this).find(':selected').text()
		posTopRight = $(this).find(':selected').data('position_top_right')
	});

	// var signTopMid;
	// var posTopMid;
	// $('#sign_top_mid').change(function(){
	// 	signTopMid = "";
	// 	posTopMid  = "";
	// 	signTopMid = $(this).find(':selected').text()
	// 	posTopMid = $(this).find(':selected').data('position_top_mid')
	// });

	var signBotLeft;
	var posBotLeft;
	$('#sign_bot_left').change(function(){
		signBotLeft = "";
		posBotLeft  = "";
		signBotLeft = $(this).find(':selected').text()
		posBotLeft = $(this).find(':selected').data('position_bot_left')
	});

	var signBotRight;
	var posBotRight;
	$('#sign_bot_right').change(function(){
		signBotRight = "";
		posBotRight  = "";
		signBotRight = $(this).find(':selected').text()
		posBotRight = $(this).find(':selected').data('position_bot_right')
	});

	var signBottom;
	var posBottom;
	$('#sign_bottom').change(function(){
		signBottom = "";
		posBottom  = "";
		signBottom = $(this).find(':selected').text()
		posBottom = $(this).find(':selected').data('position_bottom')
	});

	var pay_period;
	$(document).on('change','#select_pay_period',function(){
		_payPeriod = $(this).find(':selected').val()
		pay_period = $(this).find(':selected').text();
		// $('#pay_period').text(_payPeriod);
	});

	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	var addDegree;
	$('#add_degree').on('change',function(){
		addDegree = $(this).val();
	})

	var months ={
		1:'January',
		2:'February',
		3:'March',
		4:'April',
		5:'May',
		6:'June',
		7:'July',
		8:'August',
		9:'September',
		10:'October',
		11:'November',
		12:'December',
	}

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});

		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year,
					'pay_period':_payPeriod
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						if(_payPeriod) {
						body = [];
						ctr = 1;
						netCtr = 0;
						count2 = 1;

						netTotalSalary 	= 0;
						netSalary 		= 0;
						netSalaryAmount	= 0;
						netPay 			= 0;
						netHalfSalary 	= 0;
						netHoldSalary 	= 0;
						netLessAdj 		= 0;

						coveredPeriod = months[_Month]+' '+_Year;

						holdCount = parseInt(data.hold_count) + 6;

						$.each(data.transaction,function(key,val){

							subSalary 		= 0;
							subPay 			= 0;
							subHalfSalary 	= 0;
							subSalaryAmount = 0;
							subTotalSalary 	= 0;
							subHoldPay 		= 0;
							subLessAdj 		= 0;

							body += '<tr>';
							body += '<td></td>';
							body += '<td style="font-weight:bold;" colspan="9"><span style="text-decoration:underline">'+key+'</span></td>';
							body += '</tr>';

							$.each(val,function(k,v){

								// EMPLOYEE
								lastname = (v.employees) ? v.employees.lastname : '';
								firstname = (v.employees) ? v.employees.firstname : '';
								middlename = (v.employees) ? v.employees.middlename : '';
								fullname = lastname+' '+firstname+' '+ middlename;
								atm_no = (v.employeeinfo.atm_no) ? v.employeeinfo.atm_no : '';
								additionalAdj = (v.additional_adj) ? v.additional_adj : 0;
								deductionAdj = (v.deduction_adj) ? v.deduction_adj : 0;
								payPeriodAdj = (v.pay_period) ? v.pay_period : '';
								remit_remarks = (v.remittance_remarks) ? v.remittance_remarks : '';
								totalPay = (v.net_pay) ? v.net_pay : 0;
								firstHalf = (v.net_first_half) ? v.net_first_half : 0;
								secondHalf = (v.net_second_half) ? v.net_second_half : 0;

								additionalAdj = (_payPeriod == payPeriodAdj) ? additionalAdj : 0;
								deductionAdj = (_payPeriod == payPeriodAdj) ? deductionAdj : 0;
								adjustmentAmount = parseFloat(additionalAdj) - parseFloat(deductionAdj);

								adjustmentAmount = (_payPeriod == payPeriodAdj) ? adjustmentAmount : 0;

								salaryAmount = parseFloat(totalPay) / 2;
								subPay += parseFloat(totalPay);

								body += '<tr >';
								body += '<td class="text-left">'+ctr+'</td>';
								body += '<td >&nbsp;'+fullname+'</td>';
								body += '<td class="text-center">'+atm_no+'</td>';

								additional_adj = (additionalAdj && payPeriodAdj == _payPeriod) ? commaSeparateNumber(parseFloat(additionalAdj).toFixed(2)) : '0.00';
								deduction_adj = (deductionAdj && payPeriodAdj == _payPeriod) ? commaSeparateNumber(parseFloat(deductionAdj).toFixed(2)) : '0.00';
								remit_remarks = (remit_remarks && payPeriodAdj == _payPeriod) ? remit_remarks : '';

								total_pay = (totalPay) ? commaSeparateNumber(parseFloat(totalPay).toFixed(2))  : '';

								if(_payPeriod === 'firsthalf'){
									salaryAmount = firstHalf;
								}else{
									salaryAmount = secondHalf;
								}
								salary_amount = (salaryAmount) ? commaSeparateNumber(salaryAmount) : '';
								body += '<td class="text-right">'+total_pay+'</td>';
								if(v.hold == 1){
									subSalary -= parseFloat(salaryAmount);
									body += '<td class="text-right">0.00</td>';
								}else{
									body += '<td class="text-right">'+salary_amount+'</td>';
								}
								// IF SALARY IS HOLD
								totalSalary = parseFloat(salaryAmount) + parseFloat(adjustmentAmount);
								if(v.hold == 1){
									totalSalary = 0;
								}

								holdTotalPay = 0;
								HoldPay = 0;
								if(v.transactions.length !== 0){
									//** HOLD SALARY
									$.each(v.transactions,function(k1,v1){
										holdFirstHalf = (v1.net_first_half) ? v1.net_first_half : 0;
										holdSecondHalf = (v1.net_second_half) ? v1.net_second_half : 0;
										holdNetPay = (v1.net_pay) ? v1.net_pay : 0;

										if(_payPeriod === 'firsthalf'){
											holdTotalPay = holdFirstHalf;
											if(v1.hold_status == 'both'){
												holdPay = parseInt(holdNetPay).toFixed(2);
											}else{
												holdPay = parseInt(holdFirstHalf).toFixed(2);
											}
										}else{
											holdTotalPay = holdSecondHalf;
										}

										hold_salary_amount = (holdTotalPay) ? commaSeparateNumber(parseFloat(holdTotalPay).toFixed(2)) : '0.00';

										holdPay = parseFloat(holdPay) + parseFloat(adjustmentAmount);
										totalSalaryAmount = parseFloat(salaryAmount) + parseFloat(holdPay);

										total_salary_amount = (holdPay) ? commaSeparateNumber(parseFloat(holdPay).toFixed(2)) : '0.00';
										if(v1.hold_status == 'firsthalf'){
											body += '<td class="text-right">'+hold_salary_amount+'</td>';
											body += '<td class="text-right">0.00</td>';
											body += '<td class="text-left">1st HALF '+months[v1.month]+'</td>';
											body += '<td class="text-right">'+total_salary_amount+'</td>';
										}

										if(v1.hold_status == 'secondhalf'){
											body += '<td class="text-right" colspan="6">'+hold_salary_amount+'</td>';
											body += '<td class="text-right">0.00</td>';
											body += '<td class="text-left">2nd HALF '+months[v1.month]+'</td>';
											body += '<td class="text-right">'+total_salary_amount+'</td>';
										}

										// HAVE HOLD BOTH
										if(v1.hold_status == 'both'){
											body += '<td class="text-right">'+hold_salary_amount+'</td>';
											body += '<td class="text-right">0.00</td>';
											body += '<td class="text-left">1st HALF '+months[v1.month]+'</td>';
											body += '<td class="text-right">'+total_salary_amount+'</td>';
											body += '<tr>';
											body += '<td class="text-right" colspan="6">'+hold_salary_amount+'</td>';
											body += '<td class="text-right">0.00</td>';
											body += '<td class="text-left">2nd HALF '+months[v1.month]+'</td>';
											body += '<td class="text-right"></td>';
											body += '</tr>';
										}

										// HAVE OVER REMITTANCE
										if(adjustmentAmount !== 0 ){
											body += '<tr>';;
											body += '<td class="text-right" colspan="6">'+additional_adj+'</td>';
											body += '<td class="text-right">'+deduction_adj+'</td>';
											body += '<td class="text-left"><div style = "width:80px; word-wrap: break-word">'+remit_remarks+'</div></td>';
											body += '<td class="text-right"></td>';
											body += '</tr>';
										}

										HoldPay += parseFloat(holdPay);

									});
									// ** end

								}else{
									body += '<td class="text-right">'+additional_adj+'</td>';
									body += '<td class="text-right">'+deduction_adj+'</td>';
									body += '<td class="text-left"><div style = "width:80px; word-wrap: break-word">'+remit_remarks+'</div></td>';
									total_salary = (totalSalary) ? commaSeparateNumber(parseFloat(totalSalary).toFixed(2)) : '0.00';
									body += '<td class="text-right">'+total_salary+'</td>';
								}
								body += '</tr>';

								subSalary += parseFloat(salaryAmount);
								subTotalSalary += parseFloat(totalSalary) + parseFloat(HoldPay) - parseFloat(holdTotalPay);
								subHoldPay += parseFloat(HoldPay) + parseFloat(additionalAdj);
								subLessAdj += parseFloat(deductionAdj);

								ctr++;
							});

							ctr = parseInt(ctr) - 1;
							netCtr += parseInt(ctr);
							netPay += parseFloat(subPay);
							netHalfSalary += parseFloat(subSalary);
							netHoldSalary += parseFloat(subHoldPay);
							netLessAdj += parseFloat(subLessAdj);
							netTotalSalary += parseFloat(subTotalSalary);
							sub_pay = (subPay) ? commaSeparateNumber(parseFloat(subPay).toFixed(2))  : '0.00';
							sub_half_salary= (subSalary) ? commaSeparateNumber(parseFloat(subSalary).toFixed(2))  : '0.00';
							sub_hold_salary= (subHoldPay) ? commaSeparateNumber(parseFloat(subHoldPay).toFixed(2))  : '0.00';
							sub_total_salary= (subTotalSalary) ? commaSeparateNumber(parseFloat(subTotalSalary).toFixed(2))  : '0.00';
							sub_less_adj = (subLessAdj) ? commaSeparateNumber(parseFloat(subLessAdj).toFixed(2))  : '0.00';

							body += '<tr style="font-weight:bold;">';
							body += '<td class="text-left" colspan="3"><span style="text-decoration:underline;">'+ctr+'</span></td>';
							body += '<td class="text-right" style="border-top:1px solid #000 !important;"><b>'+sub_pay+'</b></td>';
							body += '<td class="text-right" style="border-top:1px solid #000 !important;"><b>'+sub_half_salary+'</b></td>';
							body += '<td class="text-right" style="border-top:1px solid #000 !important;"><b>'+sub_hold_salary+'</b></td>';
							body += '<td class="text-right" style="border-top:1px solid #000 !important;"><b>'+sub_less_adj+'</b></td>'
							body += '<td class="text-right"></td>';
							body += '<td class="text-right" style="border-top:1px solid #000 !important;"><b>'+sub_total_salary+'</b></td>';
							body += '</tr>';

							ctr = 1;
						});


						net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2))  : '0.00';
						net_half_salary = (netHalfSalary) ? commaSeparateNumber(parseFloat(netHalfSalary).toFixed(2))  : '0.00';
						net_hold_salary = (netHoldSalary) ? commaSeparateNumber(parseFloat(netHoldSalary).toFixed(2))  : '0.00';
						net_less_adj = (netLessAdj) ? commaSeparateNumber(parseFloat(netLessAdj).toFixed(2))  : '0.00';
						net_total_salary = (netTotalSalary) ? commaSeparateNumber(parseFloat(netTotalSalary).toFixed(2))  : '0.00';

						signTopLeft = (signTopLeft) ? signTopLeft : '';
						posTopLeft = (posTopLeft) ? posTopLeft : '';
						signTopRight = (signTopRight) ? signTopRight : '';
						posTopRight = (posTopRight) ? posTopRight : '';
						signBotLeft = (signBotLeft) ? signBotLeft : '';
						posBotLeft = (posBotLeft) ? posBotLeft : '';
						signBotRight = (signBotRight) ? signBotRight : '';
						posBotRight = (posBotRight) ? posBotRight : '';
						signBottom = (signBottom) ? signBottom : '';
						posBottom = (posBottom) ? posBottom : '';

						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';
						addDegree = (addDegree) ? addDegree : '';

						body += '<tr style="font-weight: bold;">';
						body += '<td>'+netCtr+'</td>';
					 	body += '<td class="text-left" colspan="2"><b>GRAND TOTAL</b></td>';
					 	body += '<td class="text-right" style="border-top:2px solid #000 !important;" ><span class="net_salary" >'+net_pay+'</span></td>';
					 	body += '<td class="text-right" style="border-top:2px solid #000 !important;"><b>'+net_half_salary+'</b></td>';
						body += '<td class="text-right" style="border-top:2px solid #000 !important;"><b>'+net_hold_salary+'</b></td>';
						body += '<td class="text-right" style="border-top:2px solid #000 !important;"><b>'+net_less_adj+'</b></td>';
						body += '<td class="text-right"></td>';
						body += '<td class="text-right" style="border-top:2px solid #000 !important;"><b>'+net_total_salary+'</b></td>';
					 	body += '</tr>';

					 	body += '<tr class="text-left">';
					 	body +=	'<td colspan="9">';
					 	body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:50px;">';
					 	body += '<div class="col-md-12">';
					 	body += '<b>Certified:</b> Services rendered per submitted 	 <br>';
						body += 'Daily Time Records approved by their	 	<br>';
						body += 'Directors/Supervisors.';
					 	body += '</div>';
					 	body += '</div>';
					 	body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:30px;">';
					 	body += '<div class="col-md-4">';
					 	body +=	'<br><span style="font-weight: bold;" id="signatory_top_left">'+signTopLeft+'</span>';
					 	body +=	'<br><span id="pos_top_left">'+posTopLeft+'</span>';
					 	body += '</div>';
					 	body += '<div class="col-md-4">'
					 	// body +=	'<br><span style="font-weight: bold;" id="signatory_top_right">'+signTopMid+'</span>';
					 	// body +=	'<br><span id="pos_top_right">'+posTopMid+'</span>';
					 	body += '</div>';
					 	body += '<div class="col-md-4">'
					 	body +=	'<br><span style="font-weight: bold;" id="signatory_top_right">'+signTopRight+'</span>';
					 	body +=	'<br><span id="pos_top_right">'+posTopRight+'</span>';
					 	body += '</div>';
					 	body += '</div>'
					 	body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:30px;">';
					 	body += '<div class="col-md-4">';
					 	body +=	'<b>Certified:</b>Supporting documents complete and<br>';
					 	body +=	'proper, and cash available in the amount of<br>';
					 	body +=	'Php _________________________.';
					 	body += '</div>'
					 	body += '<div class="col-md-4"></div>';
					 	body += '<div class="col-md-4"> Approved for Payment:';
					 	body += '</div>';
					 	body += '</div>';
					 	body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:30px;">';
					 	body += '<div class="col-md-4">';
					 	body +=	'<br><span style="font-weight: bold;" id="signatory_top_left">'+signBotLeft+'</span>';
					 	body +=	'<br><span id="pos_top_left">'+posBotLeft+'</span>';
					 	body += '</div>';
					 	body += '<div class="col-md-4">';
					 	body += '</div>';
					 	body += '<div class="col-md-4">'
					 	body +=	'<br><span style="font-weight: bold;" id="signatory_top_right">'+signBotRight+' '+addDegree+'</span>';
					 	body +=	'<br><span id="pos_top_right">'+posBotRight+'</span>';
					 	body += '</div>';
					 	body += '</div>'
					 	body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:30px;">';
					 	body += '<div class="col-md-12">';
					 	body += '<span><b>Certified:</b> Each employee whose name appears <br>';
					 	body += 'on the payroll has been paid the amount as indicated opposite  his/her name.';
					 	body += '</div>';
					 	body += '</div>';
					 	body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:30px;">';
					 	body += '<div class="col-md-12">'
					 	body +=	'<br><span style="font-weight: bold;" id="signatory_top_right">'+signBottom+'</span>';
					 	body +=	'<br><span id="pos_top_right">'+posBottom+'</span>';
					 	body += '</div>';
					 	body += '</div>'
					 	body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:30px;">';
					 	body += '<div class="col-md-12">';
					 	body += '<i>AO-'+_Year+_Month+'-'+codeOne+' <br> hrdd/'+codeTwo+' </i>';
					 	body += '</div>';
					 	body += '</div>'
					 	body +=	'</td>';
					 	body += '</tr>';

						$('#tbl_body').html(body)
						$('.pay_period').text(pay_period.toUpperCase())
						$('#covered_period').text(coveredPeriod)
						$('#btnModal').trigger('click');

						}else{
							swal({
								title: "Select Pay Period First",
								type: "warning",
								showCancelButton: false,
								confirmButtonClass: "btn-danger",
								confirmButtonText: "Yes",
								closeOnConfirm: false

							});
						}
					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			});
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
});

})
</script>
@endsection