@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}" media="print">
<style type="text/css">
	.table>tbody>tr>td{
		border: none !important;
	}

</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;">Covered Date</span>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<select class="form-control select2" name="month" id="select_month">
							<option value=""></option>
						</select>
					</div>
					<div class="col-md-6">
						<select class="form-control select2" name="year" id="select_year">
							<option value=""></option>
						</select>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-12" style="padding-top: 10px;">
						<div class="form-group">
							<span>Pay Period</span>
							<select class="form-control select2" id="select_pay_period" name="pay_period">
								<option value=""></option>
								<option value="firsthalf">1st HALF SALARY</option>
								<option value="secondhalf">2nd HALF SALARY</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6" >
						<div class="form-group">
							<span>Document Tracking Number</span>
							<input type="text" name="code_1" id="code_1" class="form-control font-style2">
						</div>
					</div>
					<div class="col-md-6" >
						<div class="form-group">
							<span>Initial</span>
							<input type="text" name="code_2" id="code_2" class="form-control font-style2">
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Left</span>
							<select class="form-control font-style2 select2" id="sign_top_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Right</span>
							<select class="form-control font-style2 select2" id="sign_top_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Bottom Left</span>
							<select class="form-control font-style2 select2" id="sign_bot_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_bot_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Bottom Right</span>
							<select class="form-control font-style2 select2" id="sign_bot_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_bot_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid">
	       		<div class="row" id="reports" style="margin: 960px;margin: auto;">
	       			<div class="col-md-12" id="payroll_register"></div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	var _payPeriod;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});
	$('#select_pay_period').select2({
		allowClear:true,
	    placeholder: "Pay Period",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	var signTopLeft;
	var posTopLeft;
	$('#sign_top_left').change(function(){
		signTopLeft = "";
		posTopLeft  = "";
		signTopLeft = $(this).find(':selected').text()
		posTopLeft = $(this).find(':selected').data('position_top_left')

	});

	var signTopRight;
	var posTopRight;
	$('#sign_top_right').change(function(){
		signTopRight = "";
		posTopRight  = "";
		signTopRight = $(this).find(':selected').text()
		posTopRight = $(this).find(':selected').data('position_top_right')
	});

	var signBotLeft;
	var posBotLeft;
	$('#sign_bot_left').change(function(){
		signBotLeft = "";
		posBotLeft  = "";
		signBotLeft = $(this).find(':selected').text()
		posBotLeft = $(this).find(':selected').data('position_bot_left')
	});

	var signBotRight;
	var posBotRight;
	$('#sign_bot_right').change(function(){
		signBotRight = "";
		posBotRight  = "";
		signBotRight = $(this).find(':selected').text()
		posBotRight = $(this).find(':selected').data('position_bot_right')
	});

	var pay_period;
	$(document).on('change','#select_pay_period',function(){
		_payPeriod = $(this).find(':selected').val()
		pay_period = $(this).find(':selected').text();
		// $('#pay_period').text(_payPeriod);
	});

	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	var months ={
		'January':1,
		'February':2,
		'March':3,
		'April':4,
		'May':5,
		'June':6,
		'July':7,
		'August':8,
		'September':9,
		'October':10,
		'November':11,
		'December':12,
	}




	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});

		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{
					'month':_Month,
					'year':_Year,
					'pay_period':_payPeriod
				},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						if(_payPeriod) {
						body = [];
						ctr = 1;
						ctr2 = 0;
						netCtr = 0;
						count2 = 1;

						netTotalSalary 	= 0;
						netSalary 		= 0;
						netSalaryAmount	= 0;
						netPay 			= 0;

						coveredPeriod = _Month+' '+_Year;

						holdCount = parseInt(data.hold_count) + 6;
						totalPage = data.totalPage;
						$.each(data.transaction,function(pageNo,val){
							body += '<table class="table table2" style="margin-top:10px;border:none;">';
							body += '<thead style="font-weight: bold;">';
						 	body += '<tr>';
						 	body += '<td colspan="9"  style="border-left:none;border-right:none;">';
						 	body += '<div class="row" style="margin-left:-5px;">';
						 	body += '<div class="col-md-12" >';
						 	body += '<img src="{{ url("images/new-logo.jpg") }}" style="height: 60px;"> <br>';
						 	body += '<span style="font-size: 12px;"><b>{{ $title }}</b></span> <br>';
				       		body += '<b>Covered Period:</b> <span id="covered_period"></span>';
						 	body +=	'</div>';
						 	body += '</div>';
						 	body += '</td>';
						 	body += '</tr>';
							body += '<tr class="text-center">';
							body += '<td rowspan="2" style="vertical-align:middle;">#</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">NAME OF EMPLOYEE</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">ACCOUNT NUMBER</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">NET PAY BEFORE <br> ADJUSTMENT</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">NET PAY <br> <span class="pay_period"></span></td>';
							body += '<td colspan="2" style="vertical-align:middle">ADJUSTMENT</td>';
							body += '<td rowspan="2" style="vertical-align: middle;">REMARKS</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">TOTAL SALARY FOR <br> <span class="pay_period"></span></td>';
							body += '</tr>';
							body += '<tr class="text-center">';
								body += '<td>ADD</td>';
								body += '<td>LESS</td>';
							body += '</tr>';
						 	body += '</thead>';

						 	body += '<tfoot >';
							body += '<tr>';
							body += '<td colspan="9" class="text-right" style="border:none;height:50px;vertical-align:middle;">Page '+pageNo+' of '+totalPage+'</td>';
							body += '</tr>';
							body += '</tfoot>';

							body += '<tbody>';

							$.each(val,function(k1,v1){
								subSalary 		= 0;
								subPay 			= 0;
								subSalaryAmount = 0;
								body += '<tr>';
								body += '<td></td>';
								body += '<td style="font-weight:bold;" colspan="9"><span style="text-decoration:underline">'+k1.toUpperCase()+'</span></td>';
								body += '</tr>';

								$.each(v1,function(k,v){

									// EMPLOYEE
									lastname = (v.employees) ? v.employees.lastname : '';
									firstname = (v.employees) ? v.employees.firstname : '';
									middlename = (v.employees) ? v.employees.middlename : '';
									fullname = lastname+' '+firstname+' '+ middlename;
									atm_no = (v.employeeinfo.atm_no) ? v.employeeinfo.atm_no : '';
									totalPay = (v.net_pay) ? (v.net_pay) : 0;

									salaryAmount = parseFloat(totalPay) / 2;
									subPay += parseFloat(totalPay);

									body += '<tr >';
									body += '<td class="text-left">'+ctr+'</td>';
									body += '<td >&nbsp;'+fullname+'</td>';
									body += '<td class="text-center">'+atm_no+'</td>';

									total_pay = (totalPay) ? commaSeparateNumber(parseFloat(totalPay).toFixed(2))  : '';

									if(_payPeriod === 'firsthalf'){
										salaryAmount = parseInt(salaryAmount).toFixed(2);
										subSalary += parseFloat(salaryAmount);
										salary_amount = (salaryAmount) ? commaSeparateNumber(salaryAmount) : '';
									}else{
										getDecimal = parseFloat(salaryAmount).toFixed(2).split('.');
										getDecimal = parseFloat(getDecimal[1])/100;
										salaryAmount = (parseFloat(salaryAmount) + getDecimal);
										salaryAmount = Math.round(salaryAmount * 100) /100;
										subSalary += parseFloat(salaryAmount);
										salary_amount = (salaryAmount) ? commaSeparateNumber(parseFloat(salaryAmount).toFixed(2)) : '';
									}

									body += '<td class="text-right">'+total_pay+'</td>';
									body += '<td class="text-right">'+salary_amount+'</td>';
									holdSalaryAmount = 0;
									count = 0
									if(v.transactions.length !== 0){
										$.each(v.transactions,function(k1,v1){
											switch(v1.hold_status){
												case 'firsthalf':
													count++;
													break;
												case 'secondhalf':
													count++;
													break;
												default:
													count+=2;
													break;
											}
										});

										$.each(v.transactions,function(k1,v1){
											holdTotalPay = (v1.net_pay) ? v1.net_pay / 2 : 0;
											holdPay = (v1.net_pay) ? v1.net_pay : 0;

											if(_payPeriod === 'firsthalf'){
												holdTotalPay = parseInt(holdTotalPay).toFixed(2);
												if(v1.hold_status == 'both'){
													holdPay = parseInt(holdPay).toFixed(2);
												}else{
													holdPay = parseInt(holdTotalPay).toFixed(2);
												}
												hold_salary_amount = (holdTotalPay) ? commaSeparateNumber(holdTotalPay) : '';
											}else{
												holdDedcimal = parseFloat(holdTotalPay).toFixed(2).split('.');
												holdPayDec = parseFloat(holdPay).toFixed(2).split('.');
												holdDedcimal = parseInt(holdDedcimal[1])/100;
												holdPayDec = parseInt(holdPayDec[1])/100;
												holdTotalPay = (parseFloat(holdTotalPay) + holdDedcimal);
												holdPay = (parseFloat(holdPay) + holdPayDec);
												holdTotalPay = Math.round(holdTotalPay * 100) / 100;
												holdPay = Math.round(holdPay * 100) / 100;
												hold_salary_amount = (holdTotalPay) ? commaSeparateNumber(parseFloat(holdTotalPay).toFixed(2)) : '';
											}

											totalSalaryAmount = parseFloat(salaryAmount) + parseFloat(holdPay);
											total_salary_amount = (totalSalaryAmount) ? commaSeparateNumber(parseFloat(totalSalaryAmount).toFixed(2)) : '';

											if(count >= 1){
												body += '<td class="text-right">'+hold_salary_amount+'</td>';
												body += '<td class="text-right"></td>';
												if(v1.hold_status == 'both'){
													body += '<td class="text-center">1st HALF '+v1.month.substring(0,3).toUpperCase() +'</td>';
												}else{
													body += '<td class="text-center">1st HALF '+v1.month.substring(0,3).toUpperCase()+'</td>';
												}
												body += '<td class="text-right">'+total_salary_amount+'</td>';
												count = 0;
											}

											if(v1.hold_status == 'both'){
												body += '<tr>';
												body += '<td class="text-right" colspan="6">'+hold_salary_amount+'</td>';
												body += '<td class="text-right"></td>';
												body += '<td class="text-center">2nd HALF '+v1.month.substring(0,3).toUpperCase()+'</td>';
												body += '<td class="text-right"></td>';
												body += '</tr>';
											}

										});
									}else{
										body += '<td class="text-right"></td>';
										body += '<td class="text-right"></td>';
										body += '<td class="text-right"></td>';
										body += '<td class="text-right">'+salary_amount+'</td>';
									}
									body += '</tr>';

									ctr++;
									ctr2++;

									body += '<div style="page-break-after: always;display:block;"></div>';
								});

								ctr = parseInt(ctr) - 1;
								netPay += parseFloat(subPay);
								sub_pay = (subPay) ? commaSeparateNumber(parseFloat(subPay).toFixed(2))  : '';

								body += '<tr style="font-weight:bold;">';
								body += '<td class="text-left" colspan="3"><span style="text-decoration:underline;">'+ctr+'</span></td>';
								body += '<td class="text-right" style="border-top:1px solid #000 !important;"><b>'+sub_pay+'</b></td>';
								body += '</tr>';

								ctr = 1;

							});


						});

						net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2))  : '';

						signTopLeft = (signTopLeft) ? signTopLeft : '';
						posTopLeft = (posTopLeft) ? posTopLeft : '';
						signTopRight = (signTopRight) ? signTopRight : '';
						posTopRight = (posTopRight) ? posTopRight : '';
						signBotLeft = (signBotLeft) ? signBotLeft : '';
						posBotLeft = (posBotLeft) ? posBotLeft : '';
						signBotRight = (signBotRight) ? signBotRight : '';
						posBotRight = (posBotRight) ? posBotRight : '';

						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';

						body += '<tr style="font-weight: bold;">';
						body += '<td>'+ctr2+'</td>';
					 	body += '<td class="text-left" colspan="2"><b>GRAND TOTAL</b></td>';
					 	body += '<td class="text-right" style="border-top:1px solid #000 !important;" ><span class="net_salary" >'+net_pay+'</span></td>';
					 	body += '</tr>';

					 	body += '<tr class="text-left">';
					 	body +=	'<td colspan="9">';
					 	body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:50px;">';
					 	body += '<div class="col-md-12">';
					 	body += '<b>Certified:</b> Services rendered per submitted 	 <br>';
						body += 'Daily Time Records approved by their	 	<br>';
						body += 'Directors/Supervisors.';
					 	body += '</div>';
					 	body += '</div>';
					 	body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:50px;">';
					 	body += '<div class="col-md-6">';
					 	body +=	'<br><span style="font-weight: bold;" id="signatory_top_left">'+signTopLeft+'</span>';
					 	body +=	'<br><span id="pos_top_left">'+posTopLeft+'</span>';
					 	body += '</div>';
					 	body += '<div class="col-md-6">'
					 	body +=	'<br><span style="font-weight: bold;" id="signatory_top_right">'+signTopRight+'</span>';
					 	body +=	'<br><span id="pos_top_right">'+posTopRight+'</span>';
					 	body += '</div>';
					 	body += '</div>'
					 	body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:50px;">';
					 	body += '<div class="col-md-6">';
					 	body +=	'<b>Certified:</b>Supporting documents complete and<br>';
					 	body +=	'proper, and cash available in the amount of<br>';
					 	body +=	'Php _________________________.';
					 	body +=	'<br>';
					 	body +=	'<br><br><br>';
					 	body +=	'<span style="font-weight: bold;" id="signatory_bot_left">'+signBotLeft+'</span>';
					 	body +=	'<br>';
					 	body +=	'<span id="pos_bot_left">'+posBotLeft+'</span>';
					 	body += '</div>';
					 	body += '</div>'
					 	body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:50px;">';
					 	body += '<div class="col-md-12">';
					 	body += '<span><b>Certified:</b> Each employee whose name appears <br>';
					 	body += 'on the payroll has been paid the amount as indicated opposite  his/her name.';
					 	body += '</div>';
					 	body += '</div>'
					 	body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:50px;">';
					 	body += '<div class="col-md-12">';
					 	body += '<span style="font-weight: bold;" id="signatory_bot_right">'+signBotRight+'</span>';
					 	body += '<br>';
					 	body += '<span id="pos_bot_right">'+posBotRight+'</span>';
					 	body += '</div>';
					 	body += '</div>'
					 	body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:30px;">';
					 	body += '<div class="col-md-12">';
					 	body += '<i>AO-'+_Year+months[_Month]+'-'+codeOne+' <br> hrdd/'+codeTwo+' </i>';
					 	body += '</div>';
					 	body += '</div>'
					 	body +=	'</td>';
					 	body += '</tr>';
					 	body += '</tbody>';

					 	body += '</table>';

						$('#payroll_register').html(body)

						$('.pay_period').text(pay_period.toUpperCase())
						$('#covered_period').text(coveredPeriod)
						$('#btnModal').trigger('click');

						}else{
							swal({
								title: "Select Pay Period First",
								type: "warning",
								showCancelButton: false,
								confirmButtonClass: "btn-danger",
								confirmButtonText: "Yes",
								closeOnConfirm: false

							});
						}
					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			});
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
});

})
</script>
@endsection