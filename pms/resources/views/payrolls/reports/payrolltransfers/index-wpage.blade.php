@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscape.css')}}" media="print">
<style type="text/css">
	.table2>thead>tr>td, .table2>tbody>tr>td{
		padding: 3px !important;
	}
</style>
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				@include('payrolls.reports.includes.covereddate')
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Left</span>
							<select class="form-control font-style2 select2" id="sign_top_left">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_left="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Mid One</span>
							<select class="form-control font-style2 select2" id="sign_top_mid_one">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_mid_one="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row" style="margin-left: -5px;margin-right: -5px;">
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Mid Two</span>
							<select class="form-control font-style2 select2" id="sign_top_mid_two">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_mid_two="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<span>Signatory Top Right</span>
							<select class="form-control font-style2 select2" id="sign_top_right">
								<option></option>
								@foreach($employee as $key => $value)
								<option value="{{ $value->id }}" data-position_top_right="{{ @$value->employeeinformation->positions->Name }}" >{{ $value->lastname }}, {{ $value->firstname }} {{ $value->middlename }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:600px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid " id="reports">
	       		<div class="row" style="margin-right: -5px;margin-left: -5px;">
	       			<div class="col-md-12" id="payroll_transfer"></div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];

	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	var signTopLeft;
	var positionTopLeft;
	$('#sign_top_left').change(function(){
		signTopLeft = "";
		positionTopLeft  = "";
		signTopLeft = $(this).find(':selected').text()
		positionTopLeft = $(this).find(':selected').data('position_top_left')
	});

	var signTopMidOne;
	var positionTopMidOne;
	$('#sign_top_mid_one').change(function(){
		signTopMidOne = "";
		positionTopMidOne  = "";
		signTopMidOne = $(this).find(':selected').text()
		positionTopMidOne = $(this).find(':selected').data('position_top_mid_one')
	});

	var signTopMidTwo;
	var positionTopMidTwo;
	$('#sign_top_mid_two').change(function(){
		signTopMidTwo = "";
		positionTopMidTwo  = "";
		signTopMidTwo = $(this).find(':selected').text()
		positionTopMidTwo = $(this).find(':selected').data('position_top_mid_two')
	});

	var signTopRight;
	var positionTopRight;
	$('#sign_top_right').change(function(){
		signTopRight = "";
		positionTopRight  = "";
		signTopRight = $(this).find(':selected').text()
		positionTopRight = $(this).find(':selected').data('position_top_right')
	});

	var codeOne;
	var codeTwo;
	$(document).on('keyup','#code_1',function(){
		codeOne = "";
		codeOne = $(this).val();

	});

	$(document).on('keyup','#code_2',function(){
		codeTwo = "";
		codeTwo = $(this).val();

	});

	var months ={
		'January':1,
		'February':2,
		'March':3,
		'April':4,
		'May':5,
		'June':6,
		'July':7,
		'August':8,
		'September':9,
		'October':10,
		'November':11,
		'December':12,
	}

	var tagOffices = {
		1:'Administrative and Legal Office',
		2:'Policy Research and Knowledge Management Office',
		3:'Competition Enforcement Office',
		4:'Finance, Planning and Management Office',
		5:'Mergers and Acquisitions Office',
		6:'Office of the Chairman',
		7:'Office of the Executive Director'
	}

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false
			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					if(data.transaction.length !== 0){
						body = [];
						ctr = 1;
						pageNumber = 1;
						ctr2 = 0;
						netCtr = 0;
						gsisLoanCount = (data.gsisLoanCount) ? data.gsisLoanCount : 0;
						pagibigLoanCount = (data.pagibigLoanCount) ? data.pagibigLoanCount : 0;
						totalPage = data.totalPage;

						deductionCol = (parseFloat(gsisLoanCount) + parseInt(pagibigLoanCount) + 7);
						gsisCol = (parseFloat(gsisLoanCount) + 2);
						pagibigCol = (parseInt(pagibigLoanCount) + 3)
						totalCol = deductionCol + 15;

						tr_loan_total = 0;
						netBasicAmount 			= 0;
						netPeraAmount 			= 0;
						netGsisContAmount 		= 0;
						netPagibigContAmount 	= 0;
						netPhilhealthContAmount = 0;
						netPagibig2Amount 		= 0;
						netTaxContAmount 		= 0;
						netLoanAmount 			= 0;
						netLwopAmount 			= 0;
						netGrossPayAmount 		= 0;
						netTotalDeductionAmount = 0;
						NetAmount 				= 0;
						netTotalGsisAmount 		= 0;
						netLessGrossBasicAmount = 0;
						netTotalPagibigAmount 	= 0;
						netLWOPPeraAmount 		= 0;
						netLessGrossPeraAmount 	= 0;

						coveredPeriod = _Month+" "+_Year;



						$.each(data.transaction,function(pageNo,val){
							body += '<table class="table table2" style="border:none;margin-top:10px;">';
							body += '<thead>';
							body += '<tr>';
							body +=	'<td class="text-left" style="border-left:none;border-right:none;" colspan="'+totalCol+'">';
							body += '<img src="{{ url("images/new-logo.jpg") }}" style="height: 60px;">';
				       		body +=	'<h5><b>PAYROLL TRANSFER</b> <br> Covered Period: '+coveredPeriod+'</h5>';
				       		// body += '<img src="{{ url("images/new-logo.jpg") }}" style="height: 80px;">';
						 	body +=	'</td>';
							body += '</tr>';
							body += '<tr class="text-center" style="font-weight:bold">';
							body += '<td rowspan="3" ></td>';
							body += '<td rowspan="3"><br><br>EMPLOYEE NAME</td>';
							body += '<td rowspan="3" style="vertical-align:middle;" >POSITION</td>';
							body += '<td rowspan="3" style="vertical-align:middle;" >ASSUMPTION</td>';
							body += '<td rowspan="3" style="vertical-align:middle;">JG</td>';
							body += '<td colspan="2" style="vertical-align:middle;">SALARY</td>';
							body += '<td colspan="2" style="vertical-align:middle;">LWOP</td>';
							body += '<td colspan="2" style="vertical-align:middle;">GROSS LESS(LWOP)</td>';
							body += '<td rowspan="3"><br><br>ADJUSTMENT<br>to Contributes <br>(PHIC/GSIS)</td>';
							body += '<td rowspan="3" style="vertical-align:middle;">GROSS PAY</td>';
							body += '<td class="text-center" colspan="'+deductionCol+'" style="vertical-align:middle;">DEDUCTIONS</td>';
							body += '<td rowspan="3" style="vertical-align:middle;"><br>TOTAL <br> DEDUCTION</td>';
							// body += '<td rowspan="3" style="vertical-align:middle;"><br>NET <br> PAY <br>(BEFORE) <br>ADJUSTMENT</td>';
							// body += '<td rowspan="3"  style="vertical-align:middle;">ADJ</td>';
							body += '<td rowspan="3" class="text-center" style="vertical-align:middle;">NET PAY</td>';
							body += '</tr>';
							body += '<tr class="text-center" style="font-weight:bold">';
							body += '<td rowspan="2" style="vertical-align:middle;">BASIC</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">PERA</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">BASIC</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">PERA</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">BASIC</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">PERA</td>';
							body += '<td class="text-center" colspan="'+gsisCol+'">GSIS</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">PHILHEALTH</td>';
							body += '<td rowspan="2" style="vertical-align:middle;">W/TAX</td>';
							body += '<td colspan="'+pagibigCol+'" class="text-center">PAGIBIG</td>';
							body += '</tr>';
							body += '<tr class="text-center" style="font-weight:bold">';
							body += '<td style="vertical-align:middle;">PREMIUM</td>';
							if(data.gsisLoanList.length !== 0){
								$.each(data.gsisLoanList,function(k1,v1){
									body += '<td style="vertical-align:middle;">'+v1.loans.name+'</td>';
								})
							}
							body += '<td style="vertical-align:middle;">TOTAL GSIS</td>';
							body += '<td style="vertical-align:middle;">PAGIBIG CONT.</td>';
							body += '<td style="vertical-align:middle;">MP2</td>';
							if(data.pagibigLoanList.length !== 0){
								$.each(data.pagibigLoanList,function(k1,v1){
									body += '<td style="vertical-align:middle;">'+v1.loans.name+'</td>';
								})
							}
							body += '<td style="vertical-align:middle;">TOTAL PAGIBIG</td>';
							body += '</tr>';
							body += '</thead>';

							body += '<tfoot >';
							body += '<tr>';
							body += '<td colspan="'+totalCol+'" class="text-right" style="border:none;height:50px;vertical-align:middle;">Page '+pageNo+' of '+totalPage+'</td>';
							body += '</tr>';
							body += '</tfoot>';

							body += '<tbody>';




							$.each(val,function(k1,v1){
								subBasicAmount 			= 0;
								subPeraAmount 			= 0;
								subGsisContAmount 		= 0;
								subPagibigContAmount 	= 0;
								subPhilhealthContAmount = 0;
								subPagibig2Amount 		= 0;
								subTaxContAmount 		= 0;
								subLoanAmount 			= 0;
								subLwopAmount 			= 0;
								subGrossPayAmount 		= 0;
								subTotalDeductionAmount = 0;
								subNetAmount 			= 0;
								subTotalGsisAmount 		= 0;
								subLessGrossBasicAmount = 0;
								subTotalPagibigAmount 	= 0;
								own_loan_id 			= 0;
								subLWOPPeraAmount 		= 0;
								subLessGrossPeraAmount 	= 0;
								body += '<tr>';
								body += '<td style="font-weight:bold;" colspan="'+totalCol+'">'+k1+'</td>'
								body += '</tr>';
								// ======== BODY =======
								$.each(v1,function(k,v){

									var lwopPeraAmount = 0;
									var lwopAmount 	   = 0;
									var lessGrossBasicAmount = 0;
									var lessGrossPeraAmount = 0;

									lastname = (v.employees) ? v.employees.lastname : '';
									firstname = (v.employees) ? v.employees.firstname : '';
									middlename = (v.employees) ? v.employees.middlename : '';
									fullname = lastname+' '+firstname+' '+middlename;
									position = (v.positions) ? v.positions.Name : '';
									basicAmount = (v.salaryinfo) ? v.salaryinfo.salary_new_rate : 0;
									job_grade = (v.salaryinfo.jobgrade) ? v.salaryinfo.jobgrade.job_grade : 0;
									assumption_date = (v.employeeinformation) ? v.employeeinformation.assumption_date : '';
									peraAmount = (v.benefitinfo) ? v.benefitinfo.benefit_amount : 0;
									gsisContAmount = (v.gsis_ee_share) ? v.gsis_ee_share : 0;
									pagibigPersonal = (v.employeeinfo.pagibig_personal) ? v.employeeinfo.pagibig_personal : 0;
									pagibigContAmount = (v.employeeinfo.pagibig_contribution) ? v.employeeinfo.pagibig_contribution : 0;
									philhealthContAmount = (v.employeeinfo.philhealth_contribution) ? v.employeeinfo.philhealth_contribution : 0;
									pagibig2Amount = (v.employeeinfo.pagibig2) ? v.employeeinfo.pagibig2 : 0;
									taxContAmount = (v.tax_amount) ? v.tax_amount : 0;
									taxContAmountTwo = (v.additional_tax_amount) ? v.additional_tax_amount : 0;
									totalLoanAmount = (v.total_loan) ? v.total_loan : 0;
									pagibigLoanAmount = (v.pagibig_loan) ? v.pagibig_loan : 0;
									gsisLoanAmount = (v.gsis_loan) ? v.gsis_loan : 0;
									hasAbsent = (v.actual_absences) ? v.actual_absences : 0;

									totalTaxContAmount = parseFloat(taxContAmount) + parseFloat(taxContAmountTwo);

									// ===== COMPUTATION =====
									pagibigContAmount = parseFloat(pagibigContAmount) + parseFloat(pagibigPersonal);

									totalPagibig = parseFloat(pagibigContAmount) + parseFloat(pagibig2Amount);

									totalPagibigAmount = parseFloat(totalPagibig) + parseFloat(pagibigLoanAmount);

									totalGsisAmount = parseFloat(gsisContAmount) + parseFloat(gsisLoanAmount);

									if(hasAbsent){
										lwopPeraAmount = (v.benefit_transactions) ? v.benefit_transactions.amount : 0;
										lwopAmount = (v.total_absences_amount) ? v.total_absences_amount : 0;
										lessGrossBasicAmount = parseFloat(basicAmount) - parseFloat(lwopAmount);
										lessGrossPeraAmount = parseFloat(peraAmount) - parseFloat(lwopPeraAmount);
									}

									grossPayAmount = (parseFloat(basicAmount) + parseFloat(peraAmount) - parseFloat(lwopAmount));

									totalDeductionAmount = (parseFloat(totalGsisAmount) + parseFloat(totalPagibigAmount) + parseFloat(totalTaxContAmount) + parseFloat(philhealthContAmount));

									netAmount = (parseFloat(grossPayAmount) - parseFloat(totalDeductionAmount));

									// ===== COMPUTATION =====

									// ===== SUB TOTAL COMPUTATION =====

									subBasicAmount += parseFloat(basicAmount);
									subPeraAmount += parseFloat(peraAmount);
									subGsisContAmount += parseFloat(gsisContAmount);
									subPagibigContAmount += parseFloat(pagibigContAmount);
									subPhilhealthContAmount += parseFloat(philhealthContAmount);
									subPagibig2Amount += parseFloat(pagibig2Amount);
									subTaxContAmount += parseFloat(totalTaxContAmount);
									subLoanAmount += parseFloat(totalLoanAmount);
									subLwopAmount += parseFloat(lwopAmount);
									subGrossPayAmount += parseFloat(grossPayAmount);
									subTotalDeductionAmount += parseFloat(totalDeductionAmount);
									subTotalGsisAmount += parseFloat(totalGsisAmount);
									subNetAmount += parseFloat(netAmount);
									subLessGrossBasicAmount += parseFloat(lessGrossBasicAmount);
									subTotalPagibigAmount += parseFloat(totalPagibigAmount);
									subLWOPPeraAmount += parseFloat(lwopPeraAmount);
									subLessGrossPeraAmount += parseFloat(lessGrossPeraAmount);
									// ===== SUB TOTAL COMPUTATION =====

									// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====
									basic_salary_amount = (basicAmount !== 0) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
									pera_amount = (peraAmount !== 0) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '';
									gross_earned_amount = (grossPayAmount !== 0) ? commaSeparateNumber(parseFloat(grossPayAmount).toFixed(2)) : '';
									gsis_cont_amount = (gsisContAmount !== 0) ? commaSeparateNumber(parseFloat(gsisContAmount).toFixed(2)) : '';
									pagibig2_amount = (pagibig2Amount !== 0) ? commaSeparateNumber(parseFloat(pagibig2Amount).toFixed(2)) : '';
									tax_cont_amount = (totalTaxContAmount !== 0) ? commaSeparateNumber(parseFloat(totalTaxContAmount).toFixed(2)) : '';
									total_deduction_amount = (totalDeductionAmount !== 0) ? commaSeparateNumber(parseFloat(totalDeductionAmount).toFixed(2)) : '';
									net_amount = (netAmount !== 0) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '';
									lwop_amount = (lwopAmount !== 0) ? commaSeparateNumber(parseFloat(lwopAmount).toFixed(2)) : '';
									philhealth_cont_amount = (philhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(philhealthContAmount).toFixed(2)) : '';
									pagibig_cont_amount = (pagibigContAmount !== 0) ? commaSeparateNumber(parseFloat(pagibigContAmount).toFixed(2)) : '';
									total_gsis = (totalGsisAmount !== 0) ? commaSeparateNumber(parseFloat(totalGsisAmount).toFixed(2)) : '';
									total_pagibig = (totalPagibigAmount !== 0) ? commaSeparateNumber(parseFloat(totalPagibigAmount).toFixed(2)) : '';
									less_gross_basic = (lessGrossBasicAmount !== 0) ? commaSeparateNumber(parseFloat(lessGrossBasicAmount).toFixed(2)) : '';
									lwop_pera_amount = (lwopPeraAmount !== 0) ? commaSeparateNumber(parseFloat(lwopPeraAmount).toFixed(2)) : '';
									less_gross_pera_amount = (lessGrossPeraAmount !== 0) ? commaSeparateNumber(parseFloat(lessGrossPeraAmount).toFixed(2)) : '';
									// ===== CONVERT THE NUMBER TO COMMA SEPARATED =====

									body += '<tr>';
									body += '<td>'+ctr+'</td>'
									body += '<td>'+fullname+'</td>'
									body += '<td>'+position+'</td>'
									body += '<td>'+assumption_date+'</td>'
									body += '<td  class="text-center">'+job_grade+'</td>'
									body += '<td class="text-right">'+basic_salary_amount+'</td>'
									body += '<td  class="text-right">'+pera_amount+'</td>'
									body += '<td class="text-right">'+lwop_amount+'</td>' // LWOP BASIC
									body += '<td  class="text-right">'+less_gross_pera_amount+'</td>' //LWOP PERA
									body += '<td class="text-right">'+less_gross_basic+'</td>' // GROSS LESS BASIC
									body += '<td class="text-right">'+lwop_pera_amount+'</td>' // GROSS PERA
									body += '<td  class="text-right"></td>' // ADJUSTMENT
									body += '<td  class="text-right">'+gross_earned_amount+'</td>' // GROSS PAY
									body += '<td  class="text-right">'+gsis_cont_amount+'</td>' // GSIS CONT

									if(data.gsisLoanList.length !== 0){
										$.each(data.gsisLoanList,function(k2,v2){
											loan = [];
											loan_dispay = [];
											loan_id = v2.loan_id;

											$.each(v.loaninfo_transaction,function(k3,v3){
												own_loan_id = v3.loan_id;
												loan['loan_amount_'+own_loan_id] = v3.amount;
												loan_dispay['loan_amount_'+own_loan_id] = v3.amount;

											});

											if(loan_id){
												loan_dispay['loan_amount_'+loan_id] = (loan_dispay['loan_amount_'+loan_id]) ? commaSeparateNumber(parseFloat(loan_dispay['loan_amount_'+loan_id]).toFixed(2)) : '';

												body += '<td class="sub_total text-right" data-sub_total="'+loan['loan_amount_'+loan_id]+'" data-loan_id="'+loan_id+'" >'+loan_dispay['loan_amount_'+loan_id]+'</td>'
											}else{
												body += '<td class="text-right"></td>'
											}
										});
									}

									body += '<td class="text-right">'+total_gsis+'</td>'
									body += '<td class="text-right">'+philhealth_cont_amount+'</td>'
									body += '<td class="text-right">'+tax_cont_amount+'</td>'
									body += '<td class="text-right">'+pagibig_cont_amount+'</td>'
									body += '<td class="text-right">'+pagibig2_amount+'</td>'
									if(data.pagibigLoanList.length !== 0){
										$.each(data.pagibigLoanList,function(k2,v2){
											loan = [];
											loan_dispay = [];
											loan_id = v2.loan_id;

											$.each(v.loaninfo_transaction,function(k3,v3){
												own_loan_id = v3.loan_id;
												loan['loan_amount_'+own_loan_id] = v3.amount;
												loan_dispay['loan_amount_'+own_loan_id] = v3.amount;
											});


											if(loan_id){
												loan_dispay['loan_amount_'+loan_id] = (loan_dispay['loan_amount_'+loan_id]) ? commaSeparateNumber(parseFloat(loan_dispay['loan_amount_'+loan_id]).toFixed(2)) : '';

												body += '<td class="sub_total text-right" data-sub_total="'+loan['loan_amount_'+loan_id]+'" data-loan_id="'+loan_id+'" >'+loan_dispay['loan_amount_'+loan_id]+'</td>'
											}else{
												body += '<td class="text-right"></td>'
											}
										});
									}
									body += '<td class="text-right">'+total_pagibig+'</td>'
									body += '<td class="text-right">'+total_deduction_amount+'</td>'
									// body += '<td class="text-right"></td>'
									// body += '<td class="text-right"></td>'
									body += '<td class="text-right">'+net_amount+'</td>'
									body += '</tr>';

									ctr++;
									ctr2++;

									body += '<div style="page-break-after: always;display:block;"></div>';

								});
								// ======== BODY =======
								ctr = ctr - 1;
							// ===== COMPUTE NET AMOUNT  =====

							netBasicAmount += parseFloat(subBasicAmount);
							netPeraAmount += parseFloat(subPeraAmount);
							netGsisContAmount += parseFloat(subGsisContAmount);
							netPagibigContAmount += parseFloat(subPagibigContAmount);
							netPhilhealthContAmount += parseFloat(subPhilhealthContAmount);
							netPagibig2Amount += parseFloat(subPagibig2Amount);
							netTaxContAmount += parseFloat(subTaxContAmount);
							netLoanAmount += parseFloat(subLoanAmount);
							netLwopAmount += parseFloat(subLwopAmount);
							netGrossPayAmount += parseFloat(subGrossPayAmount);
							netTotalDeductionAmount += parseFloat(subTotalDeductionAmount);
							NetAmount += parseFloat(subNetAmount);
							netTotalGsisAmount += parseFloat(subTotalGsisAmount);
							netLessGrossBasicAmount += parseFloat(subLessGrossBasicAmount);
							netTotalPagibigAmount += parseFloat(subTotalPagibigAmount);
							netLWOPPeraAmount += parseFloat(subLWOPPeraAmount);
							netLessGrossPeraAmount += parseFloat(subLessGrossPeraAmount);

							// ===== COMPUTE NET AMOUNT =====

							sub_basic_salary_amount = (subBasicAmount !== 0) ? commaSeparateNumber(parseFloat(subBasicAmount).toFixed(2)) : '';
							sub_pera_amount = (subPeraAmount !== 0) ? commaSeparateNumber(parseFloat(subPeraAmount).toFixed(2)) : '';
							sub_gsis_cont_amount = (subGsisContAmount !== 0) ? commaSeparateNumber(parseFloat(subGsisContAmount).toFixed(2)) : '';
							sub_pagibig_cont_amount = (subPagibigContAmount !== 0) ? commaSeparateNumber(parseFloat(subPagibigContAmount).toFixed(2)) : '';
							sub_philhealth_cont_amount = (subPhilhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(subPhilhealthContAmount).toFixed(2)) : '';
							sub_pagibig2_amount = (subPagibig2Amount !== 0) ? commaSeparateNumber(parseFloat(subPagibig2Amount).toFixed(2)) : '';
							sub_tax_cont_amount = (subTaxContAmount !== 0) ? commaSeparateNumber(parseFloat(subTaxContAmount).toFixed(2)) : '';
							sub_lwop_amount = (subLwopAmount !== 0) ? commaSeparateNumber(parseFloat(subLwopAmount).toFixed(2)) : '';
							sub_gross_earned_amount = (subGrossPayAmount !== 0) ? commaSeparateNumber(parseFloat(subGrossPayAmount).toFixed(2)) : '';
							sub_total_deduction_amount = (subTotalDeductionAmount !== 0) ? commaSeparateNumber(parseFloat(subTotalDeductionAmount).toFixed(2)) : '';
							sub_total_gsis_amount = (subTotalGsisAmount !== 0) ? commaSeparateNumber(parseFloat(subTotalGsisAmount).toFixed(2)) : '';
							sub_net_amount = (subNetAmount !== 0) ? commaSeparateNumber(parseFloat(subNetAmount).toFixed(2)) : '';
							sub_total_pagibig_amount = (subTotalPagibigAmount !== 0) ? commaSeparateNumber(parseFloat(subTotalPagibigAmount).toFixed(2)) : '';
							sub_lwop_pera_amount = (subLWOPPeraAmount !== 0) ? commaSeparateNumber(parseFloat(subLWOPPeraAmount).toFixed(2)) : '';
							sub_less_gross_pera_amount = (subLessGrossPeraAmount !== 0) ? commaSeparateNumber(parseFloat(subLessGrossPeraAmount).toFixed(2)) : '';
							sub_less_gross_basic_amount = (subLessGrossBasicAmount !== 0) ? commaSeparateNumber(parseFloat(subLessGrossBasicAmount).toFixed(2)) : '';

							// ======= SUB TOTAL ======

							body += '<tr style="font-weight:bold;">';
							body += '<td class="text-center">'+ctr+'</td>'
							body += '<td></td>'
							body += '<td></td>'
							body += '<td class="text-center">SUB TOTAL</td>'
							body += '<td  class="text-center"></td>'
							body += '<td class="text-right">'+sub_basic_salary_amount+'</td>'
							body += '<td  class="text-right">'+sub_pera_amount+'</td>'
							body += '<td class="text-right">'+sub_lwop_amount+'</td>' // LWOP BASIC
							body += '<td  class="text-right">'+sub_less_gross_pera_amount+'</td>' //LWOP PERA
							body += '<td class="text-right">'+sub_less_gross_basic_amount+'</td>' // GROSS LESS BASIC
							body += '<td class="text-right">'+sub_lwop_pera_amount+'</td>' // GROSS PERA
							body += '<td  class="text-right"></td>' // ADJUSTMENT
							body += '<td  class="text-right">'+sub_gross_earned_amount+'</td>' // GROSS PAY
							body += '<td  class="text-right">'+sub_gsis_cont_amount+'</td>' // GSIS CONT

							loanId = 0;
							if(data.gsisLoanList.length !== 0){
								$.each(data.gsisLoanList,function(k,v){
									loan_id = v.loan_id;
									$('.sub_total').each(function(k1,v1){
										loanId = $(this).data('loan_id');
									});

									if(loan_id == loanId){
										body += '<td class="text-right"></td>';
									}else{
										body += '<td class="text-right"></td>';
									}
								});
							}

							body += '<td class="text-right">'+sub_total_gsis_amount+'</td>'
							body += '<td class="text-right">'+sub_philhealth_cont_amount+'</td>'
							body += '<td class="text-right">'+sub_tax_cont_amount+'</td>'
							body += '<td class="text-right">'+sub_pagibig_cont_amount+'</td>'
							body += '<td class="text-right">'+sub_pagibig2_amount+'</td>'
							if(data.pagibigLoanList.length !== 0){
								$.each(data.pagibigLoanList,function(k2,v2){
									loan = [];
									loan_dispay = [];
									loan_id = v2.loan_id;

									if(loan_id){
										body += '<td class="text-right"></td>'
									}else{
										body += '<td class="text-right"></td>'
									}
								});
							}
							body += '<td class="text-right">'+sub_total_pagibig_amount+'</td>'
							body += '<td class="text-right">'+sub_total_deduction_amount+'</td>'
							// body += '<td class="text-right"></td>'
							// body += '<td class="text-right"></td>'
							body += '<td class="text-right">'+sub_net_amount+'</td>'
							body += '</tr>';
							ctr = 1;
							});

							// ======= SUB TOTAL ======
						});

						net_basic_salary_amount = (netBasicAmount !== 0) ? commaSeparateNumber(parseFloat(netBasicAmount).toFixed(2)) : '';
						net_pera_amount = (netPeraAmount !== 0) ? commaSeparateNumber(parseFloat(netPeraAmount).toFixed(2)) : '';
						net_gsis_cont_amount = (netGsisContAmount !== 0) ? commaSeparateNumber(parseFloat(netGsisContAmount).toFixed(2)) : '';
						net_pagibig_cont_amount = (netPagibigContAmount !== 0) ? commaSeparateNumber(parseFloat(netPagibigContAmount).toFixed(2)) : '';
						net_philhealth_cont_amount = (netPhilhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(netPhilhealthContAmount).toFixed(2)) : '';
						net_pagibig2_amount = (netPagibig2Amount !== 0) ? commaSeparateNumber(parseFloat(netPagibig2Amount).toFixed(2)) : '';
						net_tax_cont_amount = (netTaxContAmount !== 0) ? commaSeparateNumber(parseFloat(netTaxContAmount).toFixed(2)) : '';
						net_lwop_amount = (netLwopAmount !== 0) ? commaSeparateNumber(parseFloat(netLwopAmount).toFixed(2)) : '';
						net_gross_earned_amount = (netGrossPayAmount !== 0) ? commaSeparateNumber(parseFloat(netGrossPayAmount).toFixed(2)) : '';
						net_total_deduction_amount = (netTotalDeductionAmount !== 0) ? commaSeparateNumber(parseFloat(netTotalDeductionAmount).toFixed(2)) : '';
						net_total_gsis_amount = (netTotalGsisAmount !== 0) ? commaSeparateNumber(parseFloat(netTotalGsisAmount).toFixed(2)) : '';
						net_net_amount = (NetAmount !== 0) ? commaSeparateNumber(parseFloat(NetAmount).toFixed(2)) : '';
						net_total_pagibig_amount = (netTotalPagibigAmount !== 0) ? commaSeparateNumber(parseFloat(netTotalPagibigAmount).toFixed(2)) : '';
						net_lwop_pera_amount = (netLWOPPeraAmount !== 0) ? commaSeparateNumber(parseFloat(netLWOPPeraAmount).toFixed(2)) : '';
						net_less_pera_amount = (netLessGrossPeraAmount !== 0) ? commaSeparateNumber(parseFloat(netLessGrossPeraAmount).toFixed(2)) : '';
						net_less_basic_amount = (netLessGrossBasicAmount !== 0) ? commaSeparateNumber(parseFloat(netLessGrossBasicAmount).toFixed(2)) : '';

						// // ======= GRAND TOTAL ======
						body += '<tr  style="font-weight:bold;" class="style-td">';
						body += '<td class="text-center">'+ctr2+'</td>'
						body += '<td>Total Employees</td>'
						body += '<td></td>'
						body += '<td class="text-center">GRAND TOTAL</td>'
						body += '<td  class="text-center"></td>'
						body += '<td class="text-right">'+net_basic_salary_amount+'</td>'
						body += '<td  class="text-right">'+net_pera_amount+'</td>'
						body += '<td class="text-right">'+net_lwop_amount+'</td>' // LWOP BASIC
						body += '<td  class="text-right">'+net_less_pera_amount+'</td>' //LWOP PERA
						body += '<td class="text-right">'+net_less_basic_amount+'</td>' // GROSS LESS BASIC
						body += '<td class="text-right">'+net_lwop_pera_amount+'</td>' // GROSS PERA
						body += '<td  class="text-right"></td>' // ADJUSTMENT
						body += '<td  class="text-right">'+net_gross_earned_amount+'</td>' // GROSS PAY
						body += '<td  class="text-right">'+net_gsis_cont_amount+'</td>' // GSIS CONT

						if(data.gsisLoanList.length !== 0){
							loanId = 0;
							$.each(data.gsisLoanList,function(k,v){
								loan_id = v.loan_id;
								$('.sub_total').each(function(k1,v1){
									loanId = $(this).data('loan_id');
								});

								if(loan_id == loanId){
									body += '<td class="text-right"></td>';
								}else{
									body += '<td class="text-right"></td>';
								}
							});
						}
						body += '<td class="text-right">'+net_total_gsis_amount+'</td>'
						body += '<td class="text-right">'+net_philhealth_cont_amount+'</td>'
						body += '<td class="text-right">'+net_tax_cont_amount+'</td>'
						body += '<td class="text-right">'+net_pagibig_cont_amount+'</td>'
						body += '<td class="text-right">'+net_pagibig2_amount+'</td>'
						if(data.pagibigLoanList.length !== 0){
							$.each(data.pagibigLoanList,function(k2,v2){
								loan = [];
								loan_dispay = [];
								loan_id = v2.loan_id;

								if(loan_id){
									body += '<td class="text-right"></td>'
								}else{
									body += '<td class="text-right"></td>'
								}
							});
						}
						body += '<td class="text-right">'+net_total_pagibig_amount+'</td>'
						body += '<td class="text-right">'+net_total_deduction_amount+'</td>'
						// body += '<td class="text-right"></td>'
						// body += '<td class="text-right"></td>'
						body += '<td class="text-right">'+net_net_amount+'</td>'
						body += '</tr>';
						// ======= GRAND TOTAL ======

						signTopLeft = (signTopLeft) ? signTopLeft : '';
						positionTopLeft = (positionTopLeft) ? positionTopLeft : '';
						signTopMidOne = (signTopMidOne) ? signTopMidOne : '';
						positionTopMidOne = (positionTopMidOne) ? positionTopMidOne : '';
						signTopMidTwo = (signTopMidTwo) ? signTopMidTwo : '';
						positionTopMidTwo = (positionTopMidTwo) ? positionTopMidTwo : '';
						signTopRight = (signTopRight) ? signTopRight : '';
						positionTopRight = (positionTopRight) ? positionTopRight : '';

						codeOne = (codeOne) ? codeOne : '';
						codeTwo = (codeTwo) ? codeTwo : '';

						body +=	'<tr class="text-left borderless" >';
						body +=	'<td style="border:none;" colspan="'+totalCol+'">'
						body += '<div class="row" style="margin-left:-5px;margin-right:-5px;margin-top:20px;">';
						body 	+= '<div class="col-md-4">';
						body 	+=	'<b>Certified:</b> Services rendered per submitted <br>';
						body 	+=	'Daily Time Records approved by their <br>';
						body 	+=	'Directors/Supervisors.';
						body 	+=	'<br><br>';
						body 	+=	'<b>'+signTopLeft+'</b>';
						body 	+=	'<br>';
						body 	+=	'<span>'+positionTopLeft+'</span>';
						body 	+= '</div>';
						body 	+= '<div class="col-md-4">';
						body 	+=	'<span><b>'+signTopMidOne+'</b><br>'+positionTopMidOne+' </span>';
						body 	+= '</div>';
						body 	+= '<div class="col-md-4">';
						body 	+=	'<span>Approved for Payment:</span><br><br>';
						body 	+=	'<span><b>'+signTopMidTwo+'</b><br>'+positionTopMidTwo+'</span>';
						body 	+= '</div>';
						body 	+= '<div class="col-md-4">';
						body 		+=	'<b>Certified:</b> Supporting documents complete and <br>';
						body 		+=	'proper, and cash available in the amount of<br>';
						body 		+=	'Php _________________________. <br><br><br>';
						body 		+=	'<b>'+signTopRight+'</b>';
						body 		+=	'<br>';
						body 		+=	'<span>'+positionTopRight+'</span>';
						body 	+= '</div>';
						body += '</div>';
						body += '<div class="row" style="margin-left:-5px;margin-right:-5px;">';
						body 	+= '<div class="col-md-12">';
						body 	+= '<i>AO-'+_Year+months[_Month]+'-'+codeOne+' <br> hrdd/'+codeTwo+' </i>';
						body 	+= '</div>';
						body += '</div>';
						body += '</td>';
						body += '</tr>';
						body += '</tbody></table>';
						$('#payroll_transfer').html(body);

						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection