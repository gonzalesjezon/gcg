<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'PMS') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <style type="text/css">
        .container .navbar-header h4{
            text-align: center;
        }
    </style>
</head>
<body style="background-color: #fff;">
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style=" background-color: #164c8a;">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
<!--                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button> -->

                    <!-- Branding Image -->
                    <!-- <a class="navbar-brand" href="{{ url('/') }}"> -->

                        <!-- {{ config('app.name', 'Laravel') }} -->
                    <!-- </a> -->

                    <!-- Title -->
                    <div class="col-md-12 col-md-offset-12">
                        <h4 class="navbar-text" style="position: relative;color: #fff;">PAYROLL MANAGEMENT SYSTEM</h4>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-left">
                        &nbsp;
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        <!-- @if (Auth::guest()) -->
<!--                             <li><a href="{{ route('login') }}" style="color:#fff;">Login</a></li>
                            <li><a href="{{ route('register') }}" style="color:#fff;">Register</a></li> -->
                        <!-- @else -->
          <!--                   <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif -->
                    </ul>
                </div>
            </div>
        </nav>
        <section class="main-content">
            <div class="container">
                 @yield('content')
            </div>
        </section>
        <section class="content-footer " style="background-color: #164c8a;color: #fff;position: fixed;left: 0; bottom: 0;width: 100%;">
            <div class="container text-center">
                @include('elements.footer')
            </div>
        </secttion>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
