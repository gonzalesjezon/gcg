-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for rms_gcg
DROP DATABASE IF EXISTS `rms_gcg`;
CREATE DATABASE IF NOT EXISTS `rms_gcg` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rms_gcg`;

-- Dumping structure for table rms_gcg.applicants
DROP TABLE IF EXISTS `applicants`;
CREATE TABLE IF NOT EXISTS `applicants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `reference_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publication` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` timestamp NULL DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_issued_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_id_date_issued` timestamp NULL DEFAULT NULL,
  `govt_id_valid_until` timestamp NULL DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_barangay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_letter_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pds_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employment_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tor_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coe_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_certificate_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `info_sheet_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `curriculum_vitae_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `qualified` int(11) DEFAULT '0',
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gwa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.applicants: ~5 rows (approximately)
/*!40000 ALTER TABLE `applicants` DISABLE KEYS */;
INSERT INTO `applicants` (`id`, `job_id`, `reference_no`, `first_name`, `middle_name`, `last_name`, `extension_name`, `nickname`, `email_address`, `mobile_number`, `contact_number`, `telephone_number`, `publication`, `birthday`, `birth_place`, `gender`, `civil_status`, `citizenship`, `filipino`, `naturalized`, `height`, `weight`, `blood_type`, `pagibig`, `gsis`, `philhealth`, `tin`, `sss`, `govt_issued_id`, `govt_id_issued_number`, `govt_id_issued_place`, `govt_id_date_issued`, `govt_id_valid_until`, `house_number`, `street`, `subdivision`, `barangay`, `city`, `province`, `country`, `permanent_house_number`, `permanent_street`, `permanent_subdivision`, `permanent_barangay`, `permanent_city`, `permanent_province`, `permanent_country`, `permanent_telephone_number`, `image_path`, `application_letter_path`, `pds_path`, `employment_certificate_path`, `tor_path`, `coe_path`, `training_certificate_path`, `info_sheet_path`, `curriculum_vitae_path`, `active`, `qualified`, `remarks`, `gwa`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(5, 5, '', 'RENN FRANCIS', 'BRAÑA', 'ALVIS', NULL, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', NULL, 'female', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '146143009000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, '2019-02-28 18:28:51', NULL, NULL),
	(15, 15, '', 'MARIA THERESA ESTELLA', '', 'ROBERT', NULL, NULL, NULL, NULL, NULL, NULL, '', '1975-04-26 00:00:00', NULL, 'female', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '196837416000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, '2019-02-28 18:28:51', NULL, NULL),
	(27, 27, '', 'MICHEAL', 'ESPINA', 'JAVIER', NULL, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', NULL, 'female', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '127968009000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, '2019-02-28 18:28:51', NULL, NULL),
	(67, 67, '', 'WARREN PAUL', 'CALAMBA', 'MENDOZA', NULL, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', NULL, 'female', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '165519058000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, '2019-02-28 18:28:51', NULL, NULL),
	(72, 78, '5c96086410206', 'Juan', 'Dela', 'Cruz', NULL, NULL, 'dela@gmail.com', '34567io', NULL, NULL, 'agency', '2019-03-14 00:00:00', 'Manila', 'male', 'single', 'PH', 0, 0, '32', '32', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Manila', 'Manila', 'PH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, 1, NULL, '2019-03-23 10:20:20', '2019-03-23 10:20:24', NULL);
/*!40000 ALTER TABLE `applicants` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.appointment_casual
DROP TABLE IF EXISTS `appointment_casual`;
CREATE TABLE IF NOT EXISTS `appointment_casual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `period_emp_from` date DEFAULT NULL,
  `period_emp_to` date DEFAULT NULL,
  `daily_wage` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.appointment_casual: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_casual` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_casual` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.appointment_checklist
DROP TABLE IF EXISTS `appointment_checklist`;
CREATE TABLE IF NOT EXISTS `appointment_checklist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `form33_hrmo` int(11) DEFAULT NULL,
  `form34b_hrmo` int(11) DEFAULT NULL,
  `form212_hrmo` int(11) DEFAULT NULL,
  `eligibility_hrmo` int(11) DEFAULT NULL,
  `form1_hrmo` int(11) DEFAULT NULL,
  `form32_hrmo` int(11) DEFAULT NULL,
  `form4_hrmo` int(11) DEFAULT NULL,
  `form33_cscfo` varchar(225) DEFAULT NULL,
  `form34b_cscfo` varchar(225) DEFAULT NULL,
  `form212_cscfo` varchar(225) DEFAULT NULL,
  `eligibility_cscfo` varchar(225) DEFAULT NULL,
  `form1_cscfo` varchar(225) DEFAULT NULL,
  `form32_cscfo` varchar(225) DEFAULT NULL,
  `form4_cscfo` varchar(225) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.appointment_checklist: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_checklist` DISABLE KEYS */;
INSERT INTO `appointment_checklist` (`id`, `applicant_id`, `form33_hrmo`, `form34b_hrmo`, `form212_hrmo`, `eligibility_hrmo`, `form1_hrmo`, `form32_hrmo`, `form4_hrmo`, `form33_cscfo`, `form34b_cscfo`, `form212_cscfo`, `eligibility_cscfo`, `form1_cscfo`, `form32_cscfo`, `form4_cscfo`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 72, 0, 0, 0, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-03-23 13:35:44', '2019-03-23 13:36:12', NULL);
/*!40000 ALTER TABLE `appointment_checklist` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.appointment_forms
DROP TABLE IF EXISTS `appointment_forms`;
CREATE TABLE IF NOT EXISTS `appointment_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `publication_assessment_date` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `date_issued` date DEFAULT NULL,
  `deliberation_date` date DEFAULT NULL,
  `assessment_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.appointment_forms: ~1 rows (approximately)
/*!40000 ALTER TABLE `appointment_forms` DISABLE KEYS */;
INSERT INTO `appointment_forms` (`id`, `applicant_id`, `employee_status`, `nature_of_appointment`, `appointing_officer`, `hrmo`, `chairperson`, `date_sign`, `publication_date_from`, `publication_date_to`, `publication_assessment_date`, `hrmo_date_sign`, `chairperson_date_sign`, `date_issued`, `deliberation_date`, `assessment_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 72, 1, 1, NULL, 'Juan Dela Cruz II', 'Juan Dela Cruz', NULL, NULL, NULL, '2019-03-12', NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-03-23 13:24:16', '2019-03-23 13:30:36', NULL);
/*!40000 ALTER TABLE `appointment_forms` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.appointment_issued
DROP TABLE IF EXISTS `appointment_issued`;
CREATE TABLE IF NOT EXISTS `appointment_issued` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL DEFAULT '0',
  `employee_status` int(11) NOT NULL DEFAULT '0',
  `date_issued` date DEFAULT NULL,
  `period_of_employment_from` date DEFAULT NULL,
  `period_of_employment_to` date DEFAULT NULL,
  `nature_of_appointment` int(11) NOT NULL DEFAULT '0',
  `appointing_officer` varchar(225) DEFAULT NULL,
  `publication_date_from` date DEFAULT NULL,
  `publication_date_to` date DEFAULT NULL,
  `hrmo` varchar(225) DEFAULT NULL,
  `date_sign` date DEFAULT NULL,
  `chairperson` varchar(225) DEFAULT NULL,
  `chairperson_date_sign` date DEFAULT NULL,
  `hrmo_date_sign` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.appointment_issued: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_issued` DISABLE KEYS */;
INSERT INTO `appointment_issued` (`id`, `applicant_id`, `employee_status`, `date_issued`, `period_of_employment_from`, `period_of_employment_to`, `nature_of_appointment`, `appointing_officer`, `publication_date_from`, `publication_date_to`, `hrmo`, `date_sign`, `chairperson`, `chairperson_date_sign`, `hrmo_date_sign`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 72, 1, NULL, NULL, NULL, 2, NULL, NULL, NULL, 'Juan Dela Cruz II', NULL, NULL, '2019-03-13', NULL, 1, 1, '2019-03-23 13:30:24', '2019-03-23 13:33:08', NULL);
/*!40000 ALTER TABLE `appointment_issued` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.appointment_processing
DROP TABLE IF EXISTS `appointment_processing`;
CREATE TABLE IF NOT EXISTS `appointment_processing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `educ_qualification` varchar(225) DEFAULT NULL,
  `educ_remarks` varchar(225) DEFAULT NULL,
  `educ_check` int(11) DEFAULT '0',
  `exp_qualification` varchar(225) DEFAULT NULL,
  `exp_remarks` varchar(225) DEFAULT NULL,
  `exp_check` int(11) DEFAULT '0',
  `training_qualification` varchar(225) DEFAULT NULL,
  `training_remarks` varchar(225) DEFAULT NULL,
  `training_check` int(11) DEFAULT '0',
  `eligibility_qualification` varchar(225) DEFAULT NULL,
  `eligibility_remarks` varchar(225) DEFAULT NULL,
  `eligibility_check` int(11) DEFAULT '0',
  `other_qualification` varchar(225) DEFAULT NULL,
  `other_remarks` varchar(225) DEFAULT NULL,
  `other_check` int(11) DEFAULT '0',
  `ra_form_33` varchar(225) DEFAULT NULL,
  `ra_employee_status` varchar(225) DEFAULT NULL,
  `ra_nature_appointment` varchar(225) DEFAULT NULL,
  `ra_appointing_authority` varchar(225) DEFAULT NULL,
  `ra_date_sign` date DEFAULT NULL,
  `ra_date_publication` date DEFAULT NULL,
  `ra_certification` varchar(225) DEFAULT NULL,
  `ra_pds` varchar(225) DEFAULT NULL,
  `ra_eligibility` varchar(225) DEFAULT NULL,
  `ra_position_description` varchar(225) DEFAULT NULL,
  `ar_01` varchar(225) DEFAULT NULL,
  `ar_02` varchar(225) DEFAULT NULL,
  `ar_03` varchar(225) DEFAULT NULL,
  `ar_04` varchar(225) DEFAULT NULL,
  `ar_05` varchar(225) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.appointment_processing: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_processing` DISABLE KEYS */;
INSERT INTO `appointment_processing` (`id`, `applicant_id`, `educ_qualification`, `educ_remarks`, `educ_check`, `exp_qualification`, `exp_remarks`, `exp_check`, `training_qualification`, `training_remarks`, `training_check`, `eligibility_qualification`, `eligibility_remarks`, `eligibility_check`, `other_qualification`, `other_remarks`, `other_check`, `ra_form_33`, `ra_employee_status`, `ra_nature_appointment`, `ra_appointing_authority`, `ra_date_sign`, `ra_date_publication`, `ra_certification`, `ra_pds`, `ra_eligibility`, `ra_position_description`, `ar_01`, `ar_02`, `ar_03`, `ar_04`, `ar_05`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 72, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, 0, NULL, NULL, 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2019-03-23 13:32:59', '2019-03-23 13:35:44', NULL);
/*!40000 ALTER TABLE `appointment_processing` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.appointment_requirements
DROP TABLE IF EXISTS `appointment_requirements`;
CREATE TABLE IF NOT EXISTS `appointment_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `pds_path` varchar(225) DEFAULT NULL,
  `saln_path` varchar(225) DEFAULT NULL,
  `eligibility_path` varchar(225) DEFAULT NULL,
  `training_path` varchar(225) DEFAULT NULL,
  `psa_path` varchar(225) DEFAULT NULL,
  `tor_path` varchar(225) DEFAULT NULL,
  `diploma_path` varchar(225) DEFAULT NULL,
  `philhealth_path` varchar(225) DEFAULT NULL,
  `nbi_path` varchar(225) DEFAULT NULL,
  `medical_path` varchar(225) DEFAULT NULL,
  `bir_path` varchar(225) DEFAULT NULL,
  `coe_path` varchar(225) DEFAULT NULL,
  `tin_number` varchar(225) DEFAULT NULL,
  `pagibig_number` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.appointment_requirements: ~0 rows (approximately)
/*!40000 ALTER TABLE `appointment_requirements` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment_requirements` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.assumptions
DROP TABLE IF EXISTS `assumptions`;
CREATE TABLE IF NOT EXISTS `assumptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `head_of_office` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attested_by` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assumption_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.assumptions: ~1 rows (approximately)
/*!40000 ALTER TABLE `assumptions` DISABLE KEYS */;
INSERT INTO `assumptions` (`id`, `applicant_id`, `head_of_office`, `attested_by`, `assumption_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 72, NULL, NULL, NULL, 1, 1, '2019-03-23 11:25:54', '2019-03-23 11:26:32', NULL);
/*!40000 ALTER TABLE `assumptions` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.attestations
DROP TABLE IF EXISTS `attestations`;
CREATE TABLE IF NOT EXISTS `attestations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `agency_receiving_offer` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_employee_status` int(11) DEFAULT NULL,
  `period_from` date DEFAULT NULL,
  `period_to` date DEFAULT NULL,
  `date_action` date DEFAULT NULL,
  `date_release` date DEFAULT NULL,
  `date_issuance` date DEFAULT NULL,
  `publication_from` date DEFAULT NULL,
  `publication_to` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.attestations: ~0 rows (approximately)
/*!40000 ALTER TABLE `attestations` DISABLE KEYS */;
/*!40000 ALTER TABLE `attestations` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.boarding_applicants
DROP TABLE IF EXISTS `boarding_applicants`;
CREATE TABLE IF NOT EXISTS `boarding_applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `board_status` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.boarding_applicants: ~0 rows (approximately)
/*!40000 ALTER TABLE `boarding_applicants` DISABLE KEYS */;
/*!40000 ALTER TABLE `boarding_applicants` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.config
DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.config: ~12 rows (approximately)
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` (`id`, `name`, `value`, `description`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
	(1, 'application_recipient_name', 'John Doe', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(2, 'application_recipient_title', 'Department Manager', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(3, 'application_recipient_department', 'Human Resources Department', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(4, 'application_recipient_organization', 'Organization Name', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(5, 'application_recipient_address', '123 street corner ABC avenue, Philippines', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(6, 'application_requirements', 'Letter of Application, Latest Personal Data Sheet', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(7, 'url_rms', 'http://rms-url-here', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(8, 'url_pis', 'http://pis-url-here', NULL, '2018-12-15 02:13:29', '2018-12-15 02:13:29', 1, NULL, NULL),
	(9, 'url_pms', 'http://pms-url-here', NULL, '2018-12-15 02:13:30', '2018-12-15 02:13:30', 1, NULL, NULL),
	(10, 'url_ams', 'http://ams-url-here', NULL, '2018-12-15 02:13:30', '2018-12-15 02:13:30', 1, NULL, NULL),
	(11, 'url_ldms', 'http://ldms-url-here', NULL, '2018-12-15 02:13:30', '2018-12-15 02:13:30', 1, NULL, NULL),
	(12, 'url_spms', 'http://spms-url-here', NULL, '2018-12-15 02:13:30', '2018-12-15 02:13:30', 1, NULL, NULL);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.countries: ~242 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `code`, `name`) VALUES
	(1, 'PH', 'Philippines'),
	(2, 'AF', 'Afghanistan'),
	(3, 'AL', 'Albania'),
	(4, 'DZ', 'Algeria'),
	(5, 'AS', 'American Samoa'),
	(6, 'AD', 'Andorra'),
	(7, 'AO', 'Angola'),
	(8, 'AI', 'Anguilla'),
	(9, 'AQ', 'Antarctica'),
	(10, 'AG', 'Antigua and/or Barbuda'),
	(11, 'AR', 'Argentina'),
	(12, 'AM', 'Armenia'),
	(13, 'AW', 'Aruba'),
	(14, 'AU', 'Australia'),
	(15, 'AT', 'Austria'),
	(16, 'AZ', 'Azerbaijan'),
	(17, 'BS', 'Bahamas'),
	(18, 'BH', 'Bahrain'),
	(19, 'BD', 'Bangladesh'),
	(20, 'BB', 'Barbados'),
	(21, 'BY', 'Belarus'),
	(22, 'BE', 'Belgium'),
	(23, 'BZ', 'Belize'),
	(24, 'BJ', 'Benin'),
	(25, 'BM', 'Bermuda'),
	(26, 'BT', 'Bhutan'),
	(27, 'BO', 'Bolivia'),
	(28, 'BA', 'Bosnia and Herzegovina'),
	(29, 'BW', 'Botswana'),
	(30, 'BV', 'Bouvet Island'),
	(31, 'BR', 'Brazil'),
	(32, 'IO', 'British lndian Ocean Territory'),
	(33, 'BN', 'Brunei Darussalam'),
	(34, 'BG', 'Bulgaria'),
	(35, 'BF', 'Burkina Faso'),
	(36, 'BI', 'Burundi'),
	(37, 'KH', 'Cambodia'),
	(38, 'CM', 'Cameroon'),
	(39, 'CA', 'Canada'),
	(40, 'CV', 'Cape Verde'),
	(41, 'KY', 'Cayman Islands'),
	(42, 'CF', 'Central African Republic'),
	(43, 'TD', 'Chad'),
	(44, 'CL', 'Chile'),
	(45, 'CN', 'China'),
	(46, 'CX', 'Christmas Island'),
	(47, 'CC', 'Cocos (Keeling) Islands'),
	(48, 'CO', 'Colombia'),
	(49, 'KM', 'Comoros'),
	(50, 'CG', 'Congo'),
	(51, 'CK', 'Cook Islands'),
	(52, 'CR', 'Costa Rica'),
	(53, 'HR', 'Croatia (Hrvatska)'),
	(54, 'CU', 'Cuba'),
	(55, 'CY', 'Cyprus'),
	(56, 'CZ', 'Czech Republic'),
	(57, 'CD', 'Democratic Republic of Congo'),
	(58, 'DK', 'Denmark'),
	(59, 'DJ', 'Djibouti'),
	(60, 'DM', 'Dominica'),
	(61, 'DO', 'Dominican Republic'),
	(62, 'TP', 'East Timor'),
	(63, 'EC', 'Ecudaor'),
	(64, 'EG', 'Egypt'),
	(65, 'SV', 'El Salvador'),
	(66, 'GQ', 'Equatorial Guinea'),
	(67, 'ER', 'Eritrea'),
	(68, 'EE', 'Estonia'),
	(69, 'ET', 'Ethiopia'),
	(70, 'FK', 'Falkland Islands (Malvinas)'),
	(71, 'FO', 'Faroe Islands'),
	(72, 'FJ', 'Fiji'),
	(73, 'FI', 'Finland'),
	(74, 'FR', 'France'),
	(75, 'FX', 'France, Metropolitan'),
	(76, 'GF', 'French Guiana'),
	(77, 'PF', 'French Polynesia'),
	(78, 'TF', 'French Southern Territories'),
	(79, 'GA', 'Gabon'),
	(80, 'GM', 'Gambia'),
	(81, 'GE', 'Georgia'),
	(82, 'DE', 'Germany'),
	(83, 'GH', 'Ghana'),
	(84, 'GI', 'Gibraltar'),
	(85, 'GR', 'Greece'),
	(86, 'GL', 'Greenland'),
	(87, 'GD', 'Grenada'),
	(88, 'GP', 'Guadeloupe'),
	(89, 'GU', 'Guam'),
	(90, 'GT', 'Guatemala'),
	(91, 'GN', 'Guinea'),
	(92, 'GW', 'Guinea-Bissau'),
	(93, 'GY', 'Guyana'),
	(94, 'HT', 'Haiti'),
	(95, 'HM', 'Heard and Mc Donald Islands'),
	(96, 'HN', 'Honduras'),
	(97, 'HK', 'Hong Kong'),
	(98, 'HU', 'Hungary'),
	(99, 'IS', 'Iceland'),
	(100, 'IN', 'India'),
	(101, 'ID', 'Indonesia'),
	(102, 'IR', 'Iran (Islamic Republic of)'),
	(103, 'IQ', 'Iraq'),
	(104, 'IE', 'Ireland'),
	(105, 'IL', 'Israel'),
	(106, 'IT', 'Italy'),
	(107, 'CI', 'Ivory Coast'),
	(108, 'JM', 'Jamaica'),
	(109, 'JP', 'Japan'),
	(110, 'JO', 'Jordan'),
	(111, 'KZ', 'Kazakhstan'),
	(112, 'KE', 'Kenya'),
	(113, 'KI', 'Kiribati'),
	(114, 'KP', 'Korea, Democratic People\'s Republic of'),
	(115, 'KR', 'Korea, Republic of'),
	(116, 'KW', 'Kuwait'),
	(117, 'KG', 'Kyrgyzstan'),
	(118, 'LA', 'Lao People\'s Democratic Republic'),
	(119, 'LV', 'Latvia'),
	(120, 'LB', 'Lebanon'),
	(121, 'LS', 'Lesotho'),
	(122, 'LR', 'Liberia'),
	(123, 'LY', 'Libyan Arab Jamahiriya'),
	(124, 'LI', 'Liechtenstein'),
	(125, 'LT', 'Lithuania'),
	(126, 'LU', 'Luxembourg'),
	(127, 'MO', 'Macau'),
	(128, 'MK', 'Macedonia'),
	(129, 'MG', 'Madagascar'),
	(130, 'MW', 'Malawi'),
	(131, 'MY', 'Malaysia'),
	(132, 'MV', 'Maldives'),
	(133, 'ML', 'Mali'),
	(134, 'MT', 'Malta'),
	(135, 'MH', 'Marshall Islands'),
	(136, 'MQ', 'Martinique'),
	(137, 'MR', 'Mauritania'),
	(138, 'MU', 'Mauritius'),
	(139, 'TY', 'Mayotte'),
	(140, 'MX', 'Mexico'),
	(141, 'FM', 'Micronesia, Federated States of'),
	(142, 'MD', 'Moldova, Republic of'),
	(143, 'MC', 'Monaco'),
	(144, 'MN', 'Mongolia'),
	(145, 'MS', 'Montserrat'),
	(146, 'MA', 'Morocco'),
	(147, 'MZ', 'Mozambique'),
	(148, 'MM', 'Myanmar'),
	(149, 'NA', 'Namibia'),
	(150, 'NR', 'Nauru'),
	(151, 'NP', 'Nepal'),
	(152, 'NL', 'Netherlands'),
	(153, 'AN', 'Netherlands Antilles'),
	(154, 'NC', 'New Caledonia'),
	(155, 'NZ', 'New Zealand'),
	(156, 'NI', 'Nicaragua'),
	(157, 'NE', 'Niger'),
	(158, 'NG', 'Nigeria'),
	(159, 'NU', 'Niue'),
	(160, 'NF', 'Norfork Island'),
	(161, 'MP', 'Northern Mariana Islands'),
	(162, 'NO', 'Norway'),
	(163, 'OM', 'Oman'),
	(164, 'PK', 'Pakistan'),
	(165, 'PW', 'Palau'),
	(166, 'PA', 'Panama'),
	(167, 'PG', 'Papua New Guinea'),
	(168, 'PY', 'Paraguay'),
	(169, 'PE', 'Peru'),
	(170, 'PN', 'Pitcairn'),
	(171, 'PL', 'Poland'),
	(172, 'PT', 'Portugal'),
	(173, 'PR', 'Puerto Rico'),
	(174, 'QA', 'Qatar'),
	(175, 'SS', 'Republic of South Sudan'),
	(176, 'RE', 'Reunion'),
	(177, 'RO', 'Romania'),
	(178, 'RU', 'Russian Federation'),
	(179, 'RW', 'Rwanda'),
	(180, 'KN', 'Saint Kitts and Nevis'),
	(181, 'LC', 'Saint Lucia'),
	(182, 'VC', 'Saint Vincent and the Grenadines'),
	(183, 'WS', 'Samoa'),
	(184, 'SM', 'San Marino'),
	(185, 'ST', 'Sao Tome and Principe'),
	(186, 'SA', 'Saudi Arabia'),
	(187, 'SN', 'Senegal'),
	(188, 'RS', 'Serbia'),
	(189, 'SC', 'Seychelles'),
	(190, 'SL', 'Sierra Leone'),
	(191, 'SG', 'Singapore'),
	(192, 'SK', 'Slovakia'),
	(193, 'SI', 'Slovenia'),
	(194, 'SB', 'Solomon Islands'),
	(195, 'SO', 'Somalia'),
	(196, 'ZA', 'South Africa'),
	(197, 'GS', 'South Georgia South Sandwich Islands'),
	(198, 'ES', 'Spain'),
	(199, 'LK', 'Sri Lanka'),
	(200, 'SH', 'St. Helena'),
	(201, 'PM', 'St. Pierre and Miquelon'),
	(202, 'SD', 'Sudan'),
	(203, 'SR', 'Suriname'),
	(204, 'SJ', 'Svalbarn and Jan Mayen Islands'),
	(205, 'SZ', 'Swaziland'),
	(206, 'SE', 'Sweden'),
	(207, 'CH', 'Switzerland'),
	(208, 'SY', 'Syrian Arab Republic'),
	(209, 'TW', 'Taiwan'),
	(210, 'TJ', 'Tajikistan'),
	(211, 'TZ', 'Tanzania, United Republic of'),
	(212, 'TH', 'Thailand'),
	(213, 'TG', 'Togo'),
	(214, 'TK', 'Tokelau'),
	(215, 'TO', 'Tonga'),
	(216, 'TT', 'Trinidad and Tobago'),
	(217, 'TN', 'Tunisia'),
	(218, 'TR', 'Turkey'),
	(219, 'TM', 'Turkmenistan'),
	(220, 'TC', 'Turks and Caicos Islands'),
	(221, 'TV', 'Tuvalu'),
	(222, 'US', 'United States'),
	(223, 'UG', 'Uganda'),
	(224, 'UA', 'Ukraine'),
	(225, 'AE', 'United Arab Emirates'),
	(226, 'GB', 'United Kingdom'),
	(227, 'UM', 'United States minor outlying islands'),
	(228, 'UY', 'Uruguay'),
	(229, 'UZ', 'Uzbekistan'),
	(230, 'VU', 'Vanuatu'),
	(231, 'VA', 'Vatican City State'),
	(232, 'VE', 'Venezuela'),
	(233, 'VN', 'Vietnam'),
	(234, 'VG', 'Virgin Islands (British)'),
	(235, 'VI', 'Virgin Islands (U.S.)'),
	(236, 'WF', 'Wallis and Futuna Islands'),
	(237, 'EH', 'Western Sahara'),
	(238, 'YE', 'Yemen'),
	(239, 'YU', 'Yugoslavia'),
	(240, 'ZR', 'Zaire'),
	(241, 'ZM', 'Zambia'),
	(242, 'ZW', 'Zimbabwe');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.departments
DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.departments: ~0 rows (approximately)
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'OTHER EXECUTIVE OFFICES', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.divisions
DROP TABLE IF EXISTS `divisions`;
CREATE TABLE IF NOT EXISTS `divisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.divisions: ~5 rows (approximately)
/*!40000 ALTER TABLE `divisions` DISABLE KEYS */;
INSERT INTO `divisions` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'ADMINISTRATIVE AND FINANCE DIVISION', NULL, NULL, NULL, NULL, NULL),
	(2, 'CORPORATE AFFAIRS AND INFORMATION RESOURCE MANAGEMENT DIVISION', NULL, NULL, NULL, NULL, NULL),
	(3, 'SECTORAL COORDINATION DIVISION', NULL, NULL, NULL, NULL, NULL),
	(4, 'POLICY DEVELOPMENT, PLANNING, MONITORING AND EVALUATION DIVISION', NULL, NULL, NULL, NULL, NULL),
	(5, 'TECHNICAL SERVICES AND REGIONAL DIVISION', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `divisions` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.educations
DROP TABLE IF EXISTS `educations`;
CREATE TABLE IF NOT EXISTS `educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `school_name` varchar(225) DEFAULT NULL,
  `course` varchar(225) DEFAULT NULL,
  `attendance_from` varchar(225) DEFAULT NULL,
  `attendance_to` varchar(225) DEFAULT NULL,
  `level` varchar(225) DEFAULT NULL,
  `graduated` varchar(225) DEFAULT NULL,
  `awards` varchar(225) DEFAULT NULL,
  `educ_level` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.educations: ~0 rows (approximately)
/*!40000 ALTER TABLE `educations` DISABLE KEYS */;
/*!40000 ALTER TABLE `educations` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.eligibilities
DROP TABLE IF EXISTS `eligibilities`;
CREATE TABLE IF NOT EXISTS `eligibilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `rating` varchar(225) DEFAULT NULL,
  `exam_place` varchar(225) DEFAULT NULL,
  `license_number` varchar(225) DEFAULT NULL,
  `license_validity` varchar(225) DEFAULT NULL,
  `exam_date` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.eligibilities: ~0 rows (approximately)
/*!40000 ALTER TABLE `eligibilities` DISABLE KEYS */;
/*!40000 ALTER TABLE `eligibilities` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.employees
DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship_id` int(11) DEFAULT NULL,
  `citizenship_country_id` int(11) DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` decimal(4,2) DEFAULT NULL,
  `weight` decimal(4,2) DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_date_issued` date DEFAULT NULL,
  `govt_issued_valid_until` date DEFAULT NULL,
  `agency_employee_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biometrics` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.employees: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.employee_information
DROP TABLE IF EXISTS `employee_information`;
CREATE TABLE IF NOT EXISTS `employee_information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `hired_date` timestamp NULL DEFAULT NULL,
  `assumption_date` timestamp NULL DEFAULT NULL,
  `resigned_date` timestamp NULL DEFAULT NULL,
  `rehired_date` timestamp NULL DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `position_item_id` int(11) NOT NULL,
  `employee_status_id` int(11) NOT NULL,
  `pay_period` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_rate_id` int(11) NOT NULL,
  `work_schedule_id` int(11) NOT NULL,
  `appointment_status_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.employee_information: ~0 rows (approximately)
/*!40000 ALTER TABLE `employee_information` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_information` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.erasure_alterations
DROP TABLE IF EXISTS `erasure_alterations`;
CREATE TABLE IF NOT EXISTS `erasure_alterations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `particulars` text,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.erasure_alterations: ~0 rows (approximately)
/*!40000 ALTER TABLE `erasure_alterations` DISABLE KEYS */;
/*!40000 ALTER TABLE `erasure_alterations` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.evaluations
DROP TABLE IF EXISTS `evaluations`;
CREATE TABLE IF NOT EXISTS `evaluations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `job_id` int(10) unsigned NOT NULL,
  `performance` int(10) unsigned DEFAULT NULL,
  `performance_divide` int(10) unsigned DEFAULT NULL,
  `performance_average` int(10) unsigned DEFAULT NULL,
  `performance_percent` int(10) unsigned DEFAULT NULL,
  `performance_score` int(10) unsigned DEFAULT NULL,
  `eligibility` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seminar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_education_points` int(10) unsigned DEFAULT NULL,
  `minimum_training_points` int(10) unsigned DEFAULT NULL,
  `education_points` int(10) unsigned DEFAULT NULL,
  `training_points` int(10) unsigned DEFAULT NULL,
  `education_training_total_points` int(10) unsigned DEFAULT NULL,
  `education_training_percent` int(10) unsigned DEFAULT NULL,
  `education_training_score` int(10) unsigned DEFAULT NULL,
  `relevant_positions_held` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minimum_experience_requirement` int(10) unsigned DEFAULT NULL,
  `additional_points` int(10) unsigned DEFAULT NULL,
  `experience_accomplishments_total_points` int(10) unsigned DEFAULT NULL,
  `experience_accomplishments_percent` int(10) unsigned DEFAULT NULL,
  `experience_accomplishments_score` int(10) unsigned DEFAULT NULL,
  `potential` int(10) unsigned DEFAULT NULL,
  `potential_average_rating` decimal(5,2) DEFAULT NULL,
  `potential_percentage_rating` int(10) unsigned DEFAULT NULL,
  `potential_percent` int(10) unsigned DEFAULT NULL,
  `potential_score` int(10) unsigned DEFAULT NULL,
  `psychosocial` int(10) unsigned DEFAULT NULL,
  `psychosocial_average_rating` int(10) unsigned DEFAULT NULL,
  `psychosocial_percentage_rating` int(10) unsigned DEFAULT NULL,
  `psychosocial_percent` int(10) unsigned DEFAULT NULL,
  `psychosocial_score` int(10) unsigned DEFAULT NULL,
  `total_percent` int(10) unsigned DEFAULT NULL,
  `total_score` decimal(5,2) DEFAULT NULL,
  `evaluated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviewed_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noted_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recommended` int(10) unsigned DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.evaluations: ~0 rows (approximately)
/*!40000 ALTER TABLE `evaluations` DISABLE KEYS */;
/*!40000 ALTER TABLE `evaluations` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.examinations
DROP TABLE IF EXISTS `examinations`;
CREATE TABLE IF NOT EXISTS `examinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `exam_date` date DEFAULT NULL,
  `exam_time` varchar(50) DEFAULT NULL,
  `exam_location` varchar(50) DEFAULT NULL,
  `resched_exam_date` date DEFAULT NULL,
  `resched_exam_time` varchar(50) DEFAULT NULL,
  `exam_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.examinations: ~1 rows (approximately)
/*!40000 ALTER TABLE `examinations` DISABLE KEYS */;
INSERT INTO `examinations` (`id`, `applicant_id`, `exam_date`, `exam_time`, `exam_location`, `resched_exam_date`, `resched_exam_time`, `exam_status`, `notify`, `confirmed`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 72, NULL, '232', '3232', NULL, NULL, 7, 1, 0, 1, NULL, '2019-03-23 10:22:54', '2019-03-23 10:22:54', NULL);
/*!40000 ALTER TABLE `examinations` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.interviews
DROP TABLE IF EXISTS `interviews`;
CREATE TABLE IF NOT EXISTS `interviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `interview_date` date DEFAULT NULL,
  `interview_time` varchar(50) DEFAULT NULL,
  `interview_location` varchar(50) DEFAULT NULL,
  `resched_interview_date` date DEFAULT NULL,
  `resched_interview_time` varchar(50) DEFAULT NULL,
  `interview_status` int(11) DEFAULT NULL,
  `notify` int(11) DEFAULT NULL,
  `noftiy_resched_interview` int(11) DEFAULT NULL,
  `confirmed` int(11) DEFAULT NULL,
  `psb_chairperson` varchar(225) DEFAULT NULL,
  `psb_secretariat` varchar(225) DEFAULT NULL,
  `psb_member` varchar(225) DEFAULT NULL,
  `psm_sweap_rep` varchar(225) DEFAULT NULL,
  `psb_end_user` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.interviews: ~1 rows (approximately)
/*!40000 ALTER TABLE `interviews` DISABLE KEYS */;
INSERT INTO `interviews` (`id`, `applicant_id`, `interview_date`, `interview_time`, `interview_location`, `resched_interview_date`, `resched_interview_time`, `interview_status`, `notify`, `noftiy_resched_interview`, `confirmed`, `psb_chairperson`, `psb_secretariat`, `psb_member`, `psm_sweap_rep`, `psb_end_user`, `created_by`, `updated_by`, `created_at`, `deleted_at`, `updated_at`) VALUES
	(1, 72, NULL, '232', '3232', NULL, NULL, 3, 0, NULL, NULL, 'sd', 'dsadsad', 'adsa', 'dsa', 'dsa', 1, 1, '2019-03-23 10:23:21', NULL, '2019-03-23 12:41:09');
/*!40000 ALTER TABLE `interviews` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.jobs
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `psipop_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `duties_responsibilities` text COLLATE utf8mb4_unicode_ci,
  `key_competencies` text COLLATE utf8mb4_unicode_ci,
  `monthly_basic_salary` decimal(10,3) DEFAULT '0.000',
  `daily_salary` decimal(12,3) DEFAULT '0.000',
  `pera_amount` decimal(12,3) DEFAULT '0.000',
  `clothing_amount` decimal(12,3) DEFAULT '0.000',
  `midyear_amount` decimal(12,3) DEFAULT '0.000',
  `yearend_amount` decimal(12,3) DEFAULT '0.000',
  `cashgift_amount` decimal(12,3) DEFAULT '0.000',
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `station` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reporting_line` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci,
  `compentency_1` text COLLATE utf8mb4_unicode_ci,
  `compentency_2` text COLLATE utf8mb4_unicode_ci,
  `compentency_3` text COLLATE utf8mb4_unicode_ci,
  `compentency_4` text COLLATE utf8mb4_unicode_ci,
  `compentency_5` text COLLATE utf8mb4_unicode_ci,
  `expires` timestamp NULL DEFAULT NULL,
  `deadline_date` date DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `publication_1` int(11) NOT NULL DEFAULT '0',
  `publication_2` int(11) NOT NULL DEFAULT '0',
  `publication_3` int(11) NOT NULL DEFAULT '0',
  `publication_4` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.jobs: ~11 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`id`, `psipop_id`, `description`, `education`, `experience`, `training`, `eligibility`, `duties_responsibilities`, `key_competencies`, `monthly_basic_salary`, `daily_salary`, `pera_amount`, `clothing_amount`, `midyear_amount`, `yearend_amount`, `cashgift_amount`, `status`, `station`, `reporting_line`, `requirements`, `compentency_1`, `compentency_2`, `compentency_3`, `compentency_4`, `compentency_5`, `expires`, `deadline_date`, `publish`, `publication_1`, `publication_2`, `publication_3`, `publication_4`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
	(5, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-02-28 17:52:31', NULL, 0, NULL, NULL),
	(15, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-02-28 17:52:31', NULL, 0, NULL, NULL),
	(27, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-02-28 17:52:31', NULL, 0, NULL, NULL),
	(67, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-02-28 17:52:31', NULL, 0, NULL, NULL),
	(72, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-02-28 17:52:31', NULL, 0, NULL, NULL),
	(73, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-02-28 17:52:31', NULL, 0, NULL, NULL),
	(74, 37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-02-28 17:52:31', NULL, 0, NULL, NULL),
	(75, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-02-28 17:52:31', NULL, 0, NULL, NULL),
	(76, 78, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-02-28 17:52:31', NULL, 0, NULL, NULL),
	(77, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, '2019-02-28 17:52:31', NULL, 0, NULL, NULL),
	(78, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 50000.000, 0.000, 2000.000, 1000.000, 50000.000, 50000.000, 5000.000, 'plantilla', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, '2019-03-23 10:18:37', '2019-03-23 10:18:37', 1, NULL, NULL);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.job_offers
DROP TABLE IF EXISTS `job_offers`;
CREATE TABLE IF NOT EXISTS `job_offers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `pera_amount` decimal(13,2) DEFAULT NULL,
  `clothing_allowance` decimal(13,2) DEFAULT NULL,
  `year_end_bonus` decimal(13,2) DEFAULT NULL,
  `cash_gift` decimal(13,2) DEFAULT NULL,
  `executive_director` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_offer_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.job_offers: ~0 rows (approximately)
/*!40000 ALTER TABLE `job_offers` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_offers` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.matrix_qualifications
DROP TABLE IF EXISTS `matrix_qualifications`;
CREATE TABLE IF NOT EXISTS `matrix_qualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `isc_chairperson` varchar(50) DEFAULT NULL,
  `isc_member_one` varchar(50) DEFAULT NULL,
  `isc_member_two` varchar(50) DEFAULT NULL,
  `ea_representative` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.matrix_qualifications: ~0 rows (approximately)
/*!40000 ALTER TABLE `matrix_qualifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `matrix_qualifications` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.migrations: ~21 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_07_13_041020_create_employees_table', 1),
	(4, '2018_07_13_042843_create_employee_information_table', 1),
	(5, '2018_08_14_084139_create_config_table', 1),
	(6, '2018_08_20_100207_create_jobs_table', 1),
	(7, '2018_09_04_093333_create_applicants_table', 1),
	(8, '2018_09_04_124945_create_countries_table', 1),
	(9, '2018_09_12_054802_add_reference_no_column_to_applicants_table', 1),
	(10, '2018_09_19_004331_create_evaluations_table', 1),
	(11, '2018_09_23_084430_alter_plantilla_item_number_column_to_varchar', 1),
	(12, '2018_09_25_031118_create_recommendations_table', 1),
	(13, '2018_09_26_081458_create_appointments_table', 1),
	(14, '2018_09_26_121939_create_job_offers_table', 1),
	(15, '2018_09_30_074257_create_assumptions_table', 1),
	(16, '2018_09_30_081027_create_attestations_table', 1),
	(17, '2018_10_06_050142_alter_columns_rating_data_evaluations_table', 1),
	(18, '2018_10_10_020636_add_column_publication_to_applicants_table', 1),
	(19, '2018_10_17_130131_alter_column_annual_basic_salary_on_jobs_table', 1),
	(20, '2018_10_18_044949_create_matrix_qualifications_table', 1),
	(21, '2018_10_31_090033_add_column_job_id_to_evaluations_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.oath_offices
DROP TABLE IF EXISTS `oath_offices`;
CREATE TABLE IF NOT EXISTS `oath_offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `person_administering` varchar(225) DEFAULT NULL,
  `oath_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.oath_offices: ~1 rows (approximately)
/*!40000 ALTER TABLE `oath_offices` DISABLE KEYS */;
INSERT INTO `oath_offices` (`id`, `applicant_id`, `person_administering`, `oath_date`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 72, NULL, NULL, 1, 1, '2019-03-23 11:25:54', '2019-03-23 11:26:32', NULL);
/*!40000 ALTER TABLE `oath_offices` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.offices
DROP TABLE IF EXISTS `offices`;
CREATE TABLE IF NOT EXISTS `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.offices: ~0 rows (approximately)
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.position_descriptions
DROP TABLE IF EXISTS `position_descriptions`;
CREATE TABLE IF NOT EXISTS `position_descriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `immediate_supervisor` varchar(225) DEFAULT NULL,
  `higher_supervisor` varchar(225) DEFAULT NULL,
  `item_number` varchar(225) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `used_tools` text,
  `managerial` int(11) DEFAULT NULL,
  `supervisor` int(11) DEFAULT NULL,
  `non_supervisor` int(11) DEFAULT NULL,
  `staff` int(11) DEFAULT NULL,
  `general_public` int(11) DEFAULT NULL,
  `other_agency` varchar(50) DEFAULT NULL,
  `other_contacts` varchar(50) DEFAULT NULL,
  `office_work` int(11) DEFAULT NULL,
  `field_work` int(11) DEFAULT NULL,
  `other_condition` varchar(50) DEFAULT NULL,
  `description_function_unit` text,
  `description_function_position` text,
  `compentency_1` text,
  `compentency_2` text,
  `percentage_work_time` varchar(50) DEFAULT NULL,
  `responsibilities` varchar(50) DEFAULT NULL,
  `supervisor_name` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.position_descriptions: ~0 rows (approximately)
/*!40000 ALTER TABLE `position_descriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `position_descriptions` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.preliminary_evaluation
DROP TABLE IF EXISTS `preliminary_evaluation`;
CREATE TABLE IF NOT EXISTS `preliminary_evaluation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `age` tinyint(3) unsigned NOT NULL,
  `education` text COLLATE utf8mb4_unicode_ci,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `eligibility` text COLLATE utf8mb4_unicode_ci,
  `training` text COLLATE utf8mb4_unicode_ci,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `isc_chairperson` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isc_member_one` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isc_member_two` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ea_representative` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.preliminary_evaluation: ~0 rows (approximately)
/*!40000 ALTER TABLE `preliminary_evaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `preliminary_evaluation` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.psipop
DROP TABLE IF EXISTS `psipop`;
CREATE TABLE IF NOT EXISTS `psipop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `employee_status` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `salary_grade` int(11) DEFAULT NULL,
  `step` int(11) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `item_number` varchar(225) DEFAULT NULL,
  `code` varchar(225) DEFAULT NULL,
  `level` varchar(225) DEFAULT NULL,
  `type` varchar(225) DEFAULT NULL,
  `annual_authorized_salary` varchar(50) DEFAULT NULL,
  `annual_actual_salary` decimal(10,2) DEFAULT NULL,
  `ppa_attribution` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.psipop: ~10 rows (approximately)
/*!40000 ALTER TABLE `psipop` DISABLE KEYS */;
INSERT INTO `psipop` (`id`, `applicant_id`, `employee_status`, `office_id`, `division_id`, `department_id`, `section_id`, `salary_grade`, `step`, `position_title`, `item_number`, `code`, `level`, `type`, `annual_authorized_salary`, `annual_actual_salary`, `ppa_attribution`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(4, 27, 1, NULL, 1, 1, NULL, 24, 1, 'CHIEF ADMINISTRATIVE OFFICER ', 'NCRFWB-CADOF-9-2004', '0', 'K', 'R', '879588', 879588.00, '100000100001000', 1, NULL, NULL, '2019-02-22 16:14:39', NULL, NULL),
	(5, NULL, 0, NULL, 1, 1, NULL, 18, 1, 'ADMINISTRATIVE OFFICER V', 'NCRFWB-ADOF5-3-2016', '0', 'T', 'R', '457020', 0.00, '100000100001000', 0, NULL, NULL, '2019-02-22 16:14:39', '2019-03-23 10:20:20', NULL),
	(6, 5, 1, NULL, 1, 1, NULL, 18, 8, 'ADMINISTRATIVE OFFICER V ', 'NCRFWB-ADOF5-10-2004', '0', 'A', 'R', '457020', 496956.00, '100000100001000', 1, NULL, NULL, '2019-02-22 16:14:39', NULL, NULL),
	(7, 67, 1, NULL, 1, 1, NULL, 18, 2, 'ADMINISTRATIVE OFFICER V ', 'NCRFWB-ADOF5-11-2004', '0', 'A', 'R', '457020', 462516.00, '100000100001000', 1, NULL, NULL, '2019-02-22 16:14:39', NULL, NULL),
	(8, 15, 1, NULL, 1, 1, NULL, 18, 2, 'ADMINISTRATIVE OFFICER V', 'NCRFWB-ADOF5-12-2004', '0', 'A', 'R', '457020', 462516.00, '100000100001000', 1, NULL, NULL, '2019-02-22 16:14:39', NULL, NULL),
	(14, NULL, 0, NULL, 1, 1, NULL, 11, 1, 'ADMINISTRATIVE ASSISTANT V,  EO 366', 'NCRFWB-ADAS5-5-2007', '0', 'A', 'R', '242148', 0.00, '100000100001000', 0, NULL, NULL, '2019-02-22 16:14:39', NULL, NULL),
	(21, NULL, 0, NULL, 1, 1, NULL, 4, 1, 'ADMINISTRATIVE AIDE IV', 'NCRFWB-ADA4-9-2004', '0', 'A', 'R', '152088', 0.00, '100000100001000', 0, NULL, NULL, '2019-02-22 16:14:39', NULL, NULL),
	(37, NULL, 0, NULL, 2, 1, NULL, 15, 1, 'STATISTICIAN II ', 'NCRFWB-STAT2-1-1998', '0', 'S', 'R', '348120', 0.00, '310100100001000', 0, NULL, NULL, '2019-02-22 16:14:39', NULL, NULL),
	(52, NULL, 0, NULL, 3, 1, NULL, 4, 1, 'ADMINISTRATIVE AIDE IV ', 'NCRFWB-ADA4-18-2004', '0', 'A', 'R', '152088', 0.00, '310100100004000', 0, NULL, NULL, '2019-02-22 16:14:39', NULL, NULL),
	(78, NULL, 0, NULL, 5, 1, NULL, 15, 1, 'GAD SPECIALIST II', 'NCRFWB-GAD2-10-2016', '0', 'T', 'R', '348120', 0.00, '310100100003000', 0, NULL, NULL, '2019-02-22 16:14:39', NULL, NULL);
/*!40000 ALTER TABLE `psipop` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.recommendations
DROP TABLE IF EXISTS `recommendations`;
CREATE TABLE IF NOT EXISTS `recommendations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `sign_one` varchar(225) DEFAULT NULL,
  `sign_two` varchar(225) DEFAULT NULL,
  `sign_three` varchar(225) DEFAULT NULL,
  `sign_four` varchar(225) DEFAULT NULL,
  `sign_five` varchar(225) DEFAULT NULL,
  `prepared_by` varchar(225) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.recommendations: ~0 rows (approximately)
/*!40000 ALTER TABLE `recommendations` DISABLE KEYS */;
/*!40000 ALTER TABLE `recommendations` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.resignation_acceptance
DROP TABLE IF EXISTS `resignation_acceptance`;
CREATE TABLE IF NOT EXISTS `resignation_acceptance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `letter_date` date DEFAULT NULL,
  `resignation_date` date DEFAULT NULL,
  `appointing_officer` text,
  `sign_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.resignation_acceptance: ~0 rows (approximately)
/*!40000 ALTER TABLE `resignation_acceptance` DISABLE KEYS */;
/*!40000 ALTER TABLE `resignation_acceptance` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.sections
DROP TABLE IF EXISTS `sections`;
CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.sections: ~0 rows (approximately)
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.selection_lineup
DROP TABLE IF EXISTS `selection_lineup`;
CREATE TABLE IF NOT EXISTS `selection_lineup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.selection_lineup: ~0 rows (approximately)
/*!40000 ALTER TABLE `selection_lineup` DISABLE KEYS */;
/*!40000 ALTER TABLE `selection_lineup` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.trainings
DROP TABLE IF EXISTS `trainings`;
CREATE TABLE IF NOT EXISTS `trainings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `title_learning_programs` varchar(225) DEFAULT NULL,
  `inclusive_date_from` varchar(225) DEFAULT NULL,
  `inclusive_date_to` varchar(225) DEFAULT NULL,
  `number_hours` varchar(225) DEFAULT NULL,
  `ld_type` varchar(225) DEFAULT NULL,
  `sponsored_by` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.trainings: ~0 rows (approximately)
/*!40000 ALTER TABLE `trainings` DISABLE KEYS */;
/*!40000 ALTER TABLE `trainings` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table rms_gcg.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'admin', NULL, '$2y$10$B.ZO79UiXJyLuY706Wepa.MVM3X8BjCE/ullOFJI7lhFLX1Fva.JG', 'lcxnKoHP5zYgpP1drzIdr53p3Pqh4voS1VMApNHCMEL9QaaLx1ngG9CMpCuW', '2019-01-26 07:32:58', '2019-01-26 07:32:58');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table rms_gcg.workexperiences
DROP TABLE IF EXISTS `workexperiences`;
CREATE TABLE IF NOT EXISTS `workexperiences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) DEFAULT NULL,
  `inclusive_date_from` varchar(50) DEFAULT NULL,
  `inclusive_date_to` varchar(50) DEFAULT NULL,
  `position_title` varchar(225) DEFAULT NULL,
  `department` varchar(225) DEFAULT NULL,
  `monthly_salary` varchar(225) DEFAULT NULL,
  `salary_grade` varchar(225) DEFAULT NULL,
  `status_of_appointment` varchar(225) DEFAULT NULL,
  `govt_service` varchar(225) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table rms_gcg.workexperiences: ~0 rows (approximately)
/*!40000 ALTER TABLE `workexperiences` DISABLE KEYS */;
/*!40000 ALTER TABLE `workexperiences` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
