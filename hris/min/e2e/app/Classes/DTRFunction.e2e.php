<?php
	function getDTRSummary($emprefid,$month,$year) {
		$NewMonth 					= $month;
		$NewYear 					= $year;
		$emprefid 					= $emprefid;
		include_once 'conn.e2e.php';
		include_once 'mdbcn.e2e.php';
		
	    
	    /*============================================================================================*/
		//SETTING VARIABLE TO NULL
		/*============================================================================================*/
	    $TimeIn 					= "";
	    $LunchOut 					= "";
	    $LunchIn 					= "";
	    $TimeOut 					= "";
	    $RegHour 					= "";
	    $ExcessHour 				= "";
	    $new_week 					= "";
	    $FLCount 					= 0;
	    $TardyEQ					= 0;
		$UndertimeEQ				= 0;
		$AbsentEQ					= 0;
		$week_count 				= 0;
		$userID 					= 0;
	    $WorkSchedule 				= 0;
	    $day_count_diff				= 0;
		$PresentDays				= 0;
		$VLCount 					= 0;
		$SLCount 					= 0;
		$TotalRegHour 				= 0;
		$TotalTardyHour 			= 0;
		$TotalUndertimeHour			= 0;
		$TotalExcessHour			= 0;
		$TotalAbsentCount			= 0;
		$TotalCOCHour 				= 0;
		$TotalTardyCount 			= 0;
		$TotalUndertimeCount		= 0;
		$TotalDeduct				= 0;
		$TotalOTHour 				= 0;
	    $arr 						= array();
	    $AbsentArr                  = array();
	    $RegHour_PerWeek 			= Array(0,0,0,0,0,0,0);
	    $TardyHour_PerWeek 			= Array(0,0,0,0,0,0,0);
	    $UndertimeHour_PerWeek 		= Array(0,0,0,0,0,0,0);
	    $COCHours_PerWeek 			= Array(0,0,0,0,0,0,0);
	    $ExcessHour_PerWeek 		= Array(0,0,0,0,0,0,0);
	    $OTHours_PerWeek 			= Array(0,0,0,0,0,0,0);
	    $UndertimeCount_PerWeek 	= Array(0,0,0,0,0,0,0);
	    $AbsentCount_PerWeek 		= Array(0,0,0,0,0,0,0);
	    $OTCount_PerWeek 			= Array(0,0,0,0,0,0,0);
	    $TardyCount_PerWeek 		= Array(0,0,0,0,0,0,0);
	    $WorkDayConversion 			= file_get_contents(json."WorkDayConversion.json");
		$WorkDayEQ   				= json_decode($WorkDayConversion, true);
	    /*==============================================================================================================*/
	    /*==============================================================================================================*/
		//SETTING OF DATE RANGE
		/*==============================================================================================================*/
		
		
		if ($NewMonth <= 9) {
			$NewMonth = "0".$NewMonth;
		}
		$curr_date					= date("Y-m-d",time());
		$num_of_days 				= cal_days_in_month(CAL_GREGORIAN,intval($NewMonth),$NewYear);
		$month_start 				= $NewYear."-".$NewMonth."-01";
		$month_end 					= $NewYear."-".$NewMonth."-".$num_of_days;
		if (strtotime($curr_date) <= strtotime($month_end)) {
			$month_end = $curr_date;
		} else {
			$month_end = $month_end;
		}
		$num_of_days 				= date("d",strtotime($month_end));
		$Period 					= $NewMonth."/01/".$NewYear." - ".$NewMonth."/".$num_of_days."/".$NewYear;
		for($d=1;$d<=$num_of_days;$d++) {	
			if ($d <= 9) $d = "0".$d;
			$y 		= $NewYear."-".$NewMonth."-".$d;
			$week 	= date("W",strtotime($y));
			if ($week != $new_week) {
				$week_count++;
			}
			$dtr = [
		   		"AttendanceDate" => "$y",
		   		"AttendanceTime" => "",
		   		"UTC" => "",
		   		"TimeIn" => "",
		   		"LunchOut" => "",
		   		"LunchIn" => "",
		   		"TimeOut" => "",
		   		"OBOut" => "",
		   		"OBIn" => "",
		   		"Day" => "$d",
		   		"Week" => "$week_count",
		   		"KEntry" => "",
		   		"Holiday" => "",
		   		"CTO"=>"",
		   		"Leave"=> "",
		   		"OffSus"=>"",
		   		"OffSusName"=>"",
		   		"HasOT"=>"",
		   		"OBName"=>""
		   	];
		   	$arr["ARR"][$y] = $dtr;
			$new_week = $week;
		}
		/*==============================================================================================================*/
		
	    /*==============================================================================================================*/
	    /*==============================================================================================================*/
		//START OF EMPLOYEE QUERY
		/*==============================================================================================================*/
		$where = "WHERE RefId = $emprefid";
	    //$row = FindFirst("employees",,"*");
	    $rs = SelectEach("employees",$where);
	    if ($rs) {
	    	while ($row = mysqli_fetch_assoc($rs)) {
	    		$emprefid 					= $row["RefId"];
		    	$EmployeesName				= $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];	
				$biometricsID 				= $row["BiometricsID"];
				$CompanyID 					= $row["CompanyRefId"];
				$BranchID					= $row["BranchRefId"];
				$KEntryContent 				= file_get_contents(json."Settings_".$CompanyID.".json");
		      	$KEntry_json   				= json_decode($KEntryContent, true);
		      	$OTMinTime 					= $KEntry_json["OTMinTime"];
		      	$COCMinTime 				= $KEntry_json["COCMinTime"];
				/*==============================================================================================================*/      	
		      	$Default_qry				= "WHERE CompanyRefId = ".$CompanyID." AND BranchRefId = ".$BranchID;
		      	/*==============================================================================================================*/
				$Company_row 				= FindFirst("company","WHERE RefId = $CompanyID","*");
				if ($Company_row) {
					$CompanyLogo 			= "<img src='../../../public/images/".$Company_row["RefId"]."/".$Company_row["SmallLogo"]."'
													style='height:60px;'
											>";
					$CompanyAddress 		= $Company_row["Address"];
				}
				/*==============================================================================================================*/
				$where_empinfo 				= $Default_qry." AND EmployeesRefId = $emprefid";
				$EmpInfo_row				= FindFirst("empinformation",$where_empinfo,"*");
				if ($EmpInfo_row) {
					$Division				= getRecord("division",$EmpInfo_row["DivisionRefId"],"Name");
					$Position 				= getRecord("position",$EmpInfo_row["PositionRefId"],"Name");
					$Office 				= getRecord("office",$EmpInfo_row["OfficeRefId"],"Name");
					$WorkSchedule 			= getRecord("workschedule",$EmpInfo_row["WorkScheduleRefId"],"Name");
					$WorkScheduleRefId      = $EmpInfo_row["WorkScheduleRefId"];
				}
				/*==============================================================================================================*/
				if ($WorkScheduleRefId > 0) {
					/*==========================================================================================================*/
					//GETTING OF ATTENDANCE IN EMPLOYEE ATTENDANCE
					/*==========================================================================================================*/
					$where_empAtt 			= $Default_qry;
					$where_empAtt 			.= " AND EmployeesRefId = '".$emprefid."'";
					$where_empAtt       	.= " AND AttendanceDate BETWEEN '".$month_start."' AND '".$month_end."'";
					$rs_empAtt 				= SelectEach("employeesattendance",$where_empAtt);
					if ($rs_empAtt) {
						while ($row = mysqli_fetch_assoc($rs_empAtt)) {
							$AttendanceDate 	= $row["AttendanceDate"];
							$AttendanceTime 	= $row["AttendanceTime"];
							$UTC 				= $row["CheckTime"];
							$KEntry         	= $row["KindOfEntry"];
							$fld 				= "";
							$val 				= "";
							switch ($KEntry) {
								case 1:
									$fld 		= "TimeIn";
									$val     	= get_today_minute($UTC);
									break;
								case 2:
									$fld 		= "LunchOut";
									$val     	= get_today_minute($UTC);
									break;
								case 3:
									$fld 		= "LunchIn";
									$val     	= get_today_minute($UTC);
									break;
								case 4:
									$fld 		= "TimeOut";
									$val     	= get_today_minute($UTC);
									break;
								case 7:
									$fld 		= "OBOut";
									$val     	= get_today_minute($UTC);
									break;
								case 8:
									$fld 		= "OBIn";
									$val     	= get_today_minute($UTC);
									break;
							}
							$arr["ARR"][$AttendanceDate][$fld] = $val;
							$arr["ARR"][$AttendanceDate]["UTC"] = $UTC;
						}
					}
					/*==========================================================================================================*/
					/*==========================================================================================================*/
					//GETTING ATTENDANCE IN BIOMETRICS
					/*==========================================================================================================*/
					switch ($CompanyID) {
						case '2':
							include_once 'bio_conn.e2e.php';
							include 'inc/inc_biometrics_2.e2e.php';
							break;
						case '28':
							include_once 'bio_conn.e2e_28.php';
							include 'inc/inc_biometrics_28.e2e.php';
							break;
						default:
							include 'inc/inc_bio_default.e2e.php';
							break;
					}
					/*if (isset($connection)) {

					    $query = "SELECT USERID, Badgenumber, Name FROM USERINFO";
					    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
					    if ($result) {
					        foreach($result as $row) {
					           	switch ($CompanyID) {
					              	case "1000":
					                	$mdb_field = "Badgenumber";
					              		break;
					              	case "2":
					                	$mdb_field = "Name";
					              		break;
					              	default:
					              		$mdb_field = "Badgenumber";
					              		break;
					           	}

					           	if ($row[$mdb_field] == $biometricsID) {
					              	$userID = $row["USERID"];
					              	break;
					           	}
					        }
					    }
					    $query = 'SELECT USERID, CHECKTIME, CHECKTYPE, VERIFYCODE, SENSORID, sn 
					    		  FROM CHECKINOUT 
					    		  WHERE USERID = '.$userID.' AND VERIFYCODE = 1';
					    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
					    foreach($result as $row) {
					       	$utc 				= strtotime($row["CHECKTIME"]);
					       	$d 					= date("Y-m-d",$utc);
					    	$mdbTime 			= date("H:i",$utc);
					    	$mdbDate 			= date("Y-m-d",$utc);
							$where 				= "WHERE AttendanceDate = '".$mdbDate."'";
							$fld 				= "";
							$val 				= "";
					    	switch ($row["CHECKTYPE"]) {
					          	case "I":
					          		$fld 		= $KEntry_json["FieldTimeIn"];
					          		$val 		= get_today_minute($utc);
					             	break;
					          	case "0":
					          		$fld 		= $KEntry_json["FieldLunchOut"];
					          		$val 		= get_today_minute($utc);
					             	break;
					          	case "1":
					          		$fld 		= $KEntry_json["FieldLunchIn"];
					          		$val 		= get_today_minute($utc);
					             	break;
					          	case "O":
					          		$fld 		= $KEntry_json["FieldTimeOut"];
					          		$val 		= get_today_minute($utc);
					            	break;
					    	}
					    	if (isset($arr["ARR"][$d]["AttendanceDate"])) {
					    		if ($arr["ARR"][$d]["AttendanceDate"] == $d) {
						    		if ($arr["ARR"][$d][$fld] == "") {
							    		$arr["ARR"][$d][$fld] = $val;
							    		$arr["ARR"][$d]["UTC"] = $utc;
							    	}	
						    	}	
					    	}
						}
					}*/
					/*==========================================================================================================*/
					/*==========================================================================================================*/
					//CHECKING HOLIDAY TABLE
					/*==========================================================================================================*/
					$holiday = SelectEach("holiday","");
				    if ($holiday) {
				        while ($row = mysqli_fetch_assoc($holiday)) {
				            $StartDate        = $row["StartDate"];
				            $EndDate          = $row["EndDate"];
				            $holiday_date_diff   = dateDifference($StartDate,$EndDate);
				            $Name             = $row["Name"];
				            $EveryYr          = $row["isApplyEveryYr"];
				            $Legal            = $row["isLegal"];
				            $temp_arr         = explode("-", $StartDate);
				            $temp_date        = $NewYear."-".$temp_arr[1]."-".$temp_arr[2];
				            //echo $holiday_date_diff."---->Holiday Count";
				            for ($H=0; $H <= $holiday_date_diff; $H++) { 
			                  	$holiday_NewDate = date('Y-m-d', strtotime($temp_date. ' + '.$H.' days'));
		                  		if ($EveryYr == 1) {
			                     	if(isset($arr["ARR"][$holiday_NewDate])) {
				                        $arr["ARR"][$holiday_NewDate]["Holiday"] = $Name;
			                    	}   
			                  	} else {
			                     	if(isset($arr["ARR"][$StartDate])) {
			                        	$arr["ARR"][$StartDate]["Holiday"] = $Name;
			                     	}
			                  	}
			               	}
				            /*if ($holiday_date_diff > 1) {
				               	for ($H=0; $H <= $holiday_date_diff-1; $H++) { 
				                  	$holiday_NewDate = date('Y-m-d', strtotime($temp_date. ' + '.$H.' days'));
			                  		if ($EveryYr == 1) {
				                     	if(isset($arr["ARR"][$holiday_NewDate])) {
					                        $arr["ARR"][$holiday_NewDate]["Holiday"] = $Name;
				                    	}   
				                  	} else {
				                     	if(isset($arr["ARR"][$holiday_NewDate])) {
				                        	$arr["ARR"][$holiday_NewDate]["Holiday"] = $Name;
				                     	}
				                  	}
				               	}
				            } else {
				               	if ($EveryYr == 1) {
				                  	if(isset($arr["ARR"][$temp_date])) {
				                     	$arr["ARR"][$temp_date]["Holiday"] = $Name;
				                  	}   
				               	} else {
				                  	if(isset($arr["ARR"][$StartDate])) {
				                     	$arr["ARR"][$StartDate]["Holiday"] = $Name;
				                  	}
				               	}
				            }*/               
				        }
				    }
				    /*==========================================================================================================*/
					/*==========================================================================================================*/
					//CHECKING OFFICE SUSPENSION TABLE
					/*==========================================================================================================*/
					$officesuspension = SelectEach("officesuspension","");
			      	if ($officesuspension) {
			         	while ($row = mysqli_fetch_assoc($officesuspension)) {
				            $StartDate        = $row["StartDate"];
			            	$EndDate          = $row["EndDate"];
			            	$OffSus_date_diff = dateDifference($StartDate,$EndDate);
			            	$Name             = $row["Name"];
			            	$OffSusTime       = $row["StartTime"];
			            	$temp_arr         = explode("-", $StartDate);
			            	for ($OS=0; $OS <= $OffSus_date_diff; $OS++) { 
		                  		$OffSus_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$OS.' days'));
		                  		if(isset($arr["ARR"][$OffSus_NewDate])) {
		                     		$arr["ARR"][$OffSus_NewDate]["OffSusName"] 	= $Name;
			                    	$arr["ARR"][$OffSus_NewDate]["OffSus"] 		= $OffSusTime;
		                  		}   
		               		}
			            	/*if ($OffSus_date_diff > 1) {
			               		for ($OS=0; $OS <= $OffSus_date_diff; $OS++) { 
			                  		$OffSus_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$OS.' days'));
			                  		if(isset($arr["ARR"][$OffSus_NewDate])) {
			                     		$arr["ARR"][$OffSus_NewDate]["OffSusName"] 	= $Name;
				                    	$arr["ARR"][$OffSus_NewDate]["OffSus"] 		= $OffSusTime;
			                  		}   
			               		}   
			            	} else {
			               		if(isset($arr["ARR"][$StartDate])) {
			                  		$arr["ARR"][$StartDate]["OffSusName"] 	= $Name;
				                    $arr["ARR"][$StartDate]["OffSus"] 		= $OffSusTime;
			               		}
			            	}*/
			         	}
			      	}
			      	/*==========================================================================================================*/
					/*==========================================================================================================*/
					//GETTING EMPLOYEE LEAVE
					/*==========================================================================================================*/
					$where_leave 		= $Default_qry;
					$where_leave		.= " AND EmployeesRefId = ".$emprefid;
					$where_leave		.= " AND ApplicationDateFrom BETWEEN '".$month_start."'";
					$where_leave  		.= " AND '".$month_end."'";
					$where_leave 		.= " AND Status = 'Approved'";
					$rs_leave			= SelectEach("employeesleave",$where_leave);
					if ($rs_leave) {
						while ($row = mysqli_fetch_assoc($rs_leave)) {
							$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
							for ($i=0; $i <= $diff ; $i++) { 
								$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
								if(isset($arr["ARR"][$temp_date])) {
			                     	$arr["ARR"][$temp_date]["Leave"] = $row["LeavesRefId"];
			                  	}
							}
							/*if ($diff == 0) {
								if(isset($arr["ARR"][$row["ApplicationDateFrom"]])) {
			                     	$arr["ARR"][$row["ApplicationDateFrom"]]["Leave"] = $row["LeavesRefId"];
			                  	}
							} else {
								for ($i=0; $i <= $diff ; $i++) { 
									$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
									if(isset($arr["ARR"][$temp_date])) {
				                     	$arr["ARR"][$temp_date]["Leave"] = $row["LeavesRefId"];
				                  	}
								}
							}*/
						}
					}
					/*==========================================================================================================*/
					/*==========================================================================================================*/
					//GETTING OFFICE AUTHORITY NAME
					/*==========================================================================================================*/
					$where_OB 			= $Default_qry;
					$where_OB			.= " AND EmployeesRefId = ".$emprefid;
					$where_OB			.= " AND ApplicationDateFrom BETWEEN '".$month_start."'";
					$where_OB  			.= " AND '".$month_end."'";
					$where_OB 			.= " AND Status = 'Approved'";
					$rs_OB				= SelectEach("employeesauthority",$where_OB);
					if ($rs_OB) {
						while ($row = mysqli_fetch_assoc($rs_OB)) {
							$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
							$OBName = getRecord("absences",$row["AbsencesRefId"],"Name");
							for ($i=0; $i <= $diff ; $i++) { 
								$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
								if(isset($arr["ARR"][$temp_date])) {
			                     	$arr["ARR"][$temp_date]["OBName"] = $OBName;
			                  	}
							}
						}
					}
					/*==========================================================================================================*/
					/*==========================================================================================================*/
					//GETTING EMPLOYEE CTO AVAILMENT
					/*==========================================================================================================*/
					$where_cto 		= $Default_qry;
					$where_cto		.= " AND EmployeesRefId = ".$emprefid;
					$where_cto		.= " AND ApplicationDateFrom BETWEEN '".$month_start."' AND '".$month_end."'";
					$where_cto 		.= " AND Status = 'Approved'";
					$rs_cto			= SelectEach("employeescto",$where_cto);
					if ($rs_cto) {
						while ($row = mysqli_fetch_assoc($rs_cto)) {
							$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
							for ($i=0; $i <= $diff; $i++) { 
								$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
								if(isset($arr["ARR"][$temp_date])) {
			                     	$arr["ARR"][$temp_date]["CTO"] = $row["Hours"]*60;
			                  	}
							}
						}
					}
					/*==========================================================================================================*/
					/*==========================================================================================================*/
					//GETTING EMPLOYEE OVERTIME REQUEST
					/*==========================================================================================================*/
					$where_OT    = $Default_qry;
					$where_OT   .= " AND EmployeesRefId = ".$emprefid;
					$where_OT   .= " AND StartDate BETWEEN '".$month_start."' AND '".$month_end."'";
					$where_OT   .= " AND Status = 'Approved'";
					$rs_OT 		 = SelectEach("overtime_request",$where_OT);
					if ($rs_OT) {
						while ($row = mysqli_fetch_assoc($rs_OT)) {
							$StartDate 		= $row["StartDate"];
							$EndDate 		= $row["EndDate"];
							$WithPay        = $row["WithPay"];
							$OT_DateDiff 	= dateDifference($StartDate,$EndDate);
							for ($O=0; $O <= $OT_DateDiff; $O++) { 
			               		$OT_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$O.' days'));
			               		if ($WithPay == 1) {
			               			$Pay = 1;
			               		} else {
			               			$Pay = 0;
			               		}
			               		if(isset($arr["ARR"][$OT_NewDate])) {
			                     	$arr["ARR"][$OT_NewDate]["HasOT"] = $Pay;
			                  	}
			               	}
						}
					}
					/*==============================================================================================================*/
					//START OF COMPUTATION
					/*==============================================================================================================*/
					$Emp_WorkSched 		= FindFirst("workschedule"," WHERE RefId = ".$WorkScheduleRefId,"*");
					foreach ($arr as $value) {
						foreach ($value as $key => $row) {
							$Remarks 					= "";
						    $TardyHour 					= "";
						    $UndertimeHour 				= "";
						    $arr_TI						= "";
							$arr_OBO					= "";
							$arr_TO						= "";
							$arr_OBI					= "";
							$arr_Holiday				= "";
							$arr_leave 					= "";
							$arr_COC					= "";
							$arr_OffSus					= "";
							$arr_HasOT 					= "";
							$arr_OBName 				= "";
							$day_name 					= date("D",strtotime($row['AttendanceDate']));
							if ($day_name != "") {
								switch ($day_name) {
									case 'Mon':
										$day_name = "Monday";
										break;
									case 'Tue':
										$day_name = "Tuesday";
										break;
									case 'Wed':
										$day_name = "Wednesday";
										break;
									case 'Thu':
										$day_name = "Thursday";
										break;
									case 'Fri':
										$day_name = "Friday";
										break;
									case 'Sat':
										$day_name = "Saturday";
										break;
									case 'Sun':
										$day_name = "Sunday";
										break;
								}
							}	
							$day 						= $row["Day"]; 
							$day_in             	   	= $day_name."In";
							$day_out            	   	= $day_name."Out";
							$day_flexi          	   	= $day_name."FlexiTime";
							$day_LBOut					= $day_name."LBOut";
							$day_LBIn					= $day_name."LBIn";
							$day_RestDay				= $day_name."isRestDay";
							$day_isflexi          	   	= $day_name."isFlexi";


							$data_flexi             	= $Emp_WorkSched[$day_flexi];
							$data_isflexi             	= $Emp_WorkSched[$day_isflexi];
							$data_timein            	= $Emp_WorkSched[$day_in];
							$data_timeout 				= $Emp_WorkSched[$day_out];
							$data_LunchOut 				= $Emp_WorkSched[$day_LBOut];
							$data_LunchIn 				= $Emp_WorkSched[$day_LBIn];
							$data_RestDay      			= $Emp_WorkSched[$day_RestDay];
							$day_work_hours_count   	= ($data_timeout - $data_timein) - ($data_LunchIn - $data_LunchOut);
							$Week 						= $row["Week"];	
							$AutoLB						= $Emp_WorkSched["AutoLB"];
							$PerDayHours                = $day_work_hours_count / 60;

							

							$arr_TI						= $row["TimeIn"];
							$arr_OBO					= $row["OBOut"];
							$arr_TO						= $row["TimeOut"];
							$arr_OBI					= $row["OBIn"];
							$arr_Holiday				= $row["Holiday"];
							$arr_leave 					= $row["Leave"];
							$arr_COC					= $row["CTO"];
							$arr_OffSus					= $row["OffSus"];
							$arr_HasOT 					= $row["HasOT"];
							$arr_OBName					= $row["OBName"];


							if ($arr_TI == "" && $arr_leave != "") {
								$arr_TI = $data_timein;
								$arr_TO = $data_timeout;
							}
							if ($arr_TI == "" && $arr_OBO != "") {
								$arr_TI = $arr_OBO;
							}
							if ($arr_TO == "" && $arr_OBI != "") {
								$arr_TO = $arr_OBI;
							}

							if ($arr_Holiday != "" && $arr_TI == "") {
								$arr_TI = $data_timein;
								$arr_TO = $data_timeout;
							}
							
							if ($arr_COC != "") {
								if ($arr_COC >= $day_work_hours_count) {
									$arr_TI = $data_timein;
									$arr_TO = $data_timeout;
								} else {
									if ($arr_TI == "") {
										$arr_TI = $data_timein;
									}
									if ($arr_TO == "") {
										$arr_TO = $data_timein + $arr_COC;
									} else {
										$arr_TO = $arr_TO + $arr_COC;
									}
								}	
							}

							if ($arr_OffSus != "") {
								if ($arr_TI == "") {
									$arr_TI = $arr_OffSus;
								}
								if ($arr_TO == "") {
									$arr_TO = $data_timeout;
								}
							}
							if ($arr_TI != "") {
								$PresentDays++;
							}
							/*===================================================================================================*/
							/*===================================================================================================*/
							//MAXIMUM TIME FROM FOR EMPLOYEE TO ENTER WITHOUT LATE
							/*===================================================================================================*/
							if ($Emp_WorkSched["ScheduleType"] == "Fi") {
								$data_flexi = $data_timein;
							} else {
								$data_flexi = $data_flexi;
							}
							/*===================================================================================================*/
							/*===================================================================================================*/
							//GETTING LATE
							/*===================================================================================================*/
							if ($data_RestDay != 1) {
								if ($arr_TI != "" || $arr_OBO != "") {
									if ($data_isflexi != 1) {
										if ($arr_TI >= $data_timein) {
											$TardyHour 					= "";
										} else {
											$TardyHour 					= ($arr_TI - $data_flexi);
											$TardyHour_PerWeek[$Week] 	= $TardyHour_PerWeek[$Week] + $TardyHour;
											$TardyCount_PerWeek[$Week]++;
										}
									} else {
										if ($arr_TI <= $data_flexi) {
											$TardyHour = "";
										} else {
											$TardyHour 					= ($arr_TI - $data_flexi);
											$TardyHour_PerWeek[$Week] 	= $TardyHour_PerWeek[$Week] + $TardyHour;
											$TardyCount_PerWeek[$Week]++;
										}
									}
								}
							}
							/*===================================================================================================*/
							/*===================================================================================================*/
							//GETTING THE WORKING AND EXCESS HOURS OF THE EMPLOYEE
							/*===================================================================================================*/
							if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
								if ($arr_TI <= $data_timein) {
									$arr_TI = $data_timein;
								}
								$consume_time = (($arr_TO - $arr_TI) - ($data_LunchIn - $data_LunchOut));
								$excess_time = $consume_time - $day_work_hours_count; 	
							} else {
								$excess_time = 0;
								$consume_time = 0;
							}
							/*===================================================================================================*/
							/*===================================================================================================*/
							//GETTING THE UNDERTIME AND EXCESS HOURS OF THE EMPLOYEE
							/*===================================================================================================*/
							if ($consume_time >= $day_work_hours_count) {
								if ($day_work_hours_count != "") {
									$RegHour_PerWeek[$Week] = $RegHour_PerWeek[$Week] + $day_work_hours_count;
								}
								$consume_time = $day_work_hours_count;
							} else {
								if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
									$excess_time 					= 0;
									$UndertimeHour 					= $day_work_hours_count - $consume_time;
									$UndertimeHour_PerWeek[$Week] 	= $UndertimeHour_PerWeek[$Week] + $UndertimeHour;
									$UndertimeCount_PerWeek[$Week]++;
								} else {
									$excess_time = 0;
								}
								if ($consume_time != 0) {
									$RegHour_PerWeek[$Week] = $RegHour_PerWeek[$Week] + $consume_time;
								}
								$consume_time = $consume_time;
							}
							/*===================================================================================================*/
							/*===================================================================================================*/
							//AUTO LUNCH BREAK
							/*===================================================================================================*/
							if ($AutoLB == 1) {
								if ($arr_TI != "") {
									$Lunch_Out = $data_LunchOut;
									$Lunch_In = $data_LunchIn;
								} else {
									$Lunch_Out = "";
									$Lunch_In = "";
								}
							} else {
								$Lunch_Out = $row["LunchOut"];
								$Lunch_In = $row["LunchIn"];
							}
							/*===============================================================================*/
							/*===============================================================================*/
							/*===============================================================================*/
							/*===============================================================================*/
							if ($TardyHour == "") {
								if ($arr_HasOT == "1") {
									if ($excess_time >= $OTMinTime) {
										$OTHours_PerWeek[$Week] 			= $OTHours_PerWeek[$Week] + $excess_time;
									} else {
										$excess_time 						= 0;
									}
								} else if ($arr_HasOT == "0") {
									if ($excess_time >= $COCMinTime) {
										$COCHours_PerWeek[$Week] 			= $COCHours_PerWeek[$Week] + $excess_time;
									} else {
										$excess_time 						= 0;
									}
								} else {
									$ExcessHour_PerWeek[$Week] 				= $ExcessHour_PerWeek[$Week] + $excess_time;
								}	
							} else {
								$ExcessHour_PerWeek[$Week] 					= intval($ExcessHour_PerWeek[$Week]) + intval($excess_time);
							}
							if ($arr_TI == "" && $arr_OBO == "" && $data_RestDay != 1) {
								$AbsentCount_PerWeek[$Week]++;
								$AbsentArr[] = date("d",strtotime($row["AttendanceDate"]));
							}
						}
					}
					$TotalRegHour   = $RegHour_PerWeek[1] + 
									  $RegHour_PerWeek[2] +
									  $RegHour_PerWeek[3] + 
									  $RegHour_PerWeek[4] + 
									  $RegHour_PerWeek[5] + 
									  $RegHour_PerWeek[6];
				    
					$TotalTardyHour = $TardyHour_PerWeek[1] + 
									  $TardyHour_PerWeek[2] + 
									  $TardyHour_PerWeek[3] + 
									  $TardyHour_PerWeek[4] + 
									  $TardyHour_PerWeek[5] +
									  $TardyHour_PerWeek[6];
					
					$TotalUndertimeHour = $UndertimeHour_PerWeek[1] + 
										  $UndertimeHour_PerWeek[2] + 
										  $UndertimeHour_PerWeek[3] + 
										  $UndertimeHour_PerWeek[4] + 
										  $UndertimeHour_PerWeek[5] +
										  $UndertimeHour_PerWeek[6];

					$TotalExcessHour    = $ExcessHour_PerWeek[1] + 
			                              $ExcessHour_PerWeek[2] + 
			                              $ExcessHour_PerWeek[3] + 
			                              $ExcessHour_PerWeek[4] +
			                              $ExcessHour_PerWeek[5] +
			                              $ExcessHour_PerWeek[6];
					
					
			        $TotalCOCHour       = $COCHours_PerWeek[1] + 
				                          $COCHours_PerWeek[2] + 
				                          $COCHours_PerWeek[3] + 
				                          $COCHours_PerWeek[4] +
				                          $COCHours_PerWeek[5] +
				                          $COCHours_PerWeek[6];
					
				    $TotalTardyCount    = $TardyCount_PerWeek[1] + 
				                          $TardyCount_PerWeek[2] + 
				                          $TardyCount_PerWeek[3] + 
				                          $TardyCount_PerWeek[4] +
				                          $TardyCount_PerWeek[5] +
				                          $TardyCount_PerWeek[6];
					 
				    $TotalUndertimeCount = 	$UndertimeCount_PerWeek[1] + 
					                        $UndertimeCount_PerWeek[2] + 
					                        $UndertimeCount_PerWeek[3] + 
					                        $UndertimeCount_PerWeek[4] +
					                        $UndertimeCount_PerWeek[5] +
					                        $UndertimeCount_PerWeek[6];

					$TotalAbsentCount  =  $AbsentCount_PerWeek[1] + 
				                          $AbsentCount_PerWeek[2] + 
				                          $AbsentCount_PerWeek[3] + 
				                          $AbsentCount_PerWeek[4] +
				                          $AbsentCount_PerWeek[5] +
				                          $AbsentCount_PerWeek[6];

				    $TotalOTHour       =  $OTHours_PerWeek[1] + 
				                          $OTHours_PerWeek[2] + 
				                          $OTHours_PerWeek[3] + 
				                          $OTHours_PerWeek[4] +
				                          $OTHours_PerWeek[5] +
				                          $OTHours_PerWeek[6];


		          	/*===================================================================================================*/
					/*===================================================================================================*/
					//GETTING THE EARNINGS
					/*===================================================================================================*/
					$TardyEQ 			= getEquivalent($TotalTardyHour,"workinghrsconversion");
				   	$UndertimeEQ		= getEquivalent($TotalUndertimeHour,"workinghrsconversion");
				   	$TotalHoursEQ       = getEquivalent(($TotalRegHour + $TotalExcessHour),"workinghrsconversion");
				   	$TotalDeduct 		= $TardyEQ + $UndertimeEQ + $TotalAbsentCount;
					$credits_arr        = getLeaveEarnings($emprefid,$CompanyID,$BranchID,$NewMonth);
					$Total_Days         = 31 - $TotalAbsentCount;
		          	$day_eq_vl 			= FindFirst("slvlearneddaily","WHERE NoOfDays =".$Total_Days,"VLEarned");
			    	$day_eq_sl 			= FindFirst("slvlearneddaily","WHERE NoOfDays =".$Total_Days,"SLEarned");
			    }
			    $arr_result = [
					"TotalRegHrs"=>$TotalRegHour,
					"TotalExcessHrs"=>$TotalExcessHour,
					"TotalHours"=>$TotalRegHour + $TotalExcessHour,
					"TotalLate"=>$TotalTardyHour,
					"TotalCOC"=>$TotalCOCHour,
					"TotalOT"=>$TotalOTHour,
					"TotalUnderTime"=>$TotalUndertimeHour,
					"LateCount"=>$TotalTardyCount,
					"UnderTimeCount"=>$TotalUndertimeCount,
					"AbsentCount"=>$TotalAbsentCount,
					"COCCount"=>$TotalCOCHour,
					"HoursEquivalent"=>$TotalHoursEQ,
					"VLEarned"=>$day_eq_vl,
					"SLEarned"=>$day_eq_sl,
					"UAL"=>$TotalDeduct,
					"AbsentArr"=>$AbsentArr,
					"FLCount"=>$FLCount
				];
				return $arr_result;
			}
		}
	}

	function getLeaveEarnings($emprefid,$CompanyID,$BranchID,$currMonth) {
		$whereSL 			= "WHERE CompanyRefId = $CompanyID AND BranchRefId = $BranchID"; 
		$whereSL           .= " AND EmployeesRefId = ".$emprefid." AND NameCredits = 'SL'";
		$whereVL 			= "WHERE CompanyRefId = $CompanyID AND BranchRefId = $BranchID"; 
		$whereVL           .= " AND EmployeesRefId = ".$emprefid." AND NameCredits = 'VL'";
		$whereOT 			= "WHERE CompanyRefId = $CompanyID AND BranchRefId = $BranchID"; 
		$whereOT           .= " AND EmployeesRefId = ".$emprefid." AND NameCredits = 'OT'";
		$row_sl				= FindOrderBy("employeescreditbalance",$whereSL,"*","BegBalAsOfDate");	
	    $row_vl				= FindOrderBy("employeescreditbalance",$whereVL,"*","BegBalAsOfDate");
	    $row_ot				= FindOrderBy("employeescreditbalance",$whereOT,"*","BegBalAsOfDate");
	    if ($row_sl) {
	    	$Earnings_sl = $row_sl["BeginningBalance"];
	    	$AsOf      = $row_sl["BegBalAsOfDate"];
	    } else {
	    	$Earnings_sl = 0;
	    	$AsOf        = date("Y",time())."-01-01";
	    }
	    if ($row_vl) {
	    	$Earnings_vl = $row_vl["BeginningBalance"];
	    	$AsOf      = $row_sl["BegBalAsOfDate"];
	    } else {
	    	$Earnings_vl = 0;
	    	$AsOf        = date("Y",time())."-01-01";
	    }
	    if ($row_ot) {
	    	$Earnings_ot = $row_ot["BeginningBalance"];
	    } else {
	    	$Earnings_ot = 0;
	    }
	    $date_arr 	  	= explode("-", $AsOf);
	    $Year           = date("Y",time());
	    $from         	= date("m",strtotime($AsOf)) + 1;
	    $to           	= $currMonth - 1;
	    if ($from <= $to) {
	    	for ($i=$from; $i<=$to; $i++) { 
		    	if ($i <= 9) $i = "0".$i;
		    	$num_of_days = cal_days_in_month(CAL_GREGORIAN,intval($i),$Year);
		    	$month_start = $Year."-".$i."-01";
		    	$month_end   = $Year."-".$i."-".$num_of_days;
		    	$dtr_Arr     = getDTRSummary($emprefid,intval($i),$Year);
		    	$VLEarned    = $dtr_Arr["VLEarned"];
		    	$SLEarned    = $dtr_Arr["SLEarned"];
		    	$OTEarned    = $dtr_Arr["TotalCOC"];
		    	$Earnings_vl = $Earnings_vl + $VLEarned;
		    	$Earnings_sl = $Earnings_sl + $SLEarned;
		    	$Earnings_ot = $Earnings_ot + $OTEarned;
		    }
	    }
	    
	    $arr = [
	    	"VLCredits"=>$Earnings_vl,
	    	"SLCredits"=>$Earnings_sl,
	    	"OTCredits"=>$Earnings_ot
	    ];
	    return $arr;
	}

	function ToHours($timeInMin,$default = "&nbsp;") {
      	if ($timeInMin > 0) {
          	$hr = explode(".",$timeInMin / 60)[0];
          	if ($hr <= 9) {
             	$hr = "0".$hr;
          	}
          	$mod_min = ($timeInMin % 60);
          	if ($mod_min <= 9) {
             	$mod_min = "0".$mod_min;
          	}
          	return $hr.":".$mod_min;   
      	} else {
      		return $default;	
     	}
  	}
  	function DTR_Summary($emprefid,$month,$year,$workschedule) {
		include_once 'conn.e2e.php';
		include_once 'constant.e2e.php';
		include_once '0620functions.e2e.php';
		/*==============================================================================================================*/
		//SETTING VARIABLE TO NULL
		/*==============================================================================================================*/
		/*==============================================================================================================*/
		$dtr_process        = [
			"EmployeesRefId"=>"",
			"WorkScheduleRefId"=>"",
			"Month"=>"",
			"Year"=>"",
			"Total_Regular_Hr"=>"",
			"Total_Excess_Hr"=>"",
			"Total_COC_Hr"=>"",
			"Total_OT_Hr"=>"",
			"Total_Tardy_Hr"=>"",
			"Total_Undertime_Hr"=>"",
			"Total_Absent_Count"=>"",
			"Total_Present_Count"=>"",
			"Total_Tardy_Count"=>"",
			"Total_Undertime_Count"=>"",
			"Days_Absent"=>"",
			"Days_Tardy"=>"",
			"Days_Undertime"=>"",
			"VL_Earned"=>"",
			"SL_Earned"=>"",
			"SPL_Used"=>"",
			"Tardy_Deduction_EQ"=>"",
			"Undertime_Deduction_EQ"=>"",
			"Regular_Hr_EQ"=>"",
			"Total_Days_EQ"=>"",
			"Total_Absent_EQ"=>"",
			"Processed"=>""
		];
		$TimeIn 					= "";
		$LunchOut 					= "";
		$LunchIn 					= "";
		$TimeOut 					= "";
		$RegHour 					= "";
		$ExcessHour 				= "";
		$TardyEQ					= "";
		$UndertimeEQ				= "";
		$AbsentEQ					= "";
		$Days_Absent				= "";
		$Days_Tardy					= "";
		$Days_Undertime				= "";
		$WorkSchedule 				= 0;
		$day_count_diff				= 0;
		$PresentDays				= 0;
		$VLCount 					= 0;
		$SLCount 					= 0;
		$TotalRegHour 				= 0;
		$TotalTardyHour 			= 0;
		$TotalUndertimeHour			= 0;
		$TotalExcessHour			= 0;
		$TotalAbsentCount			= 0;
		$TotalCOCHour 				= 0;
		$TotalTardyCount 			= 0;
		$TotalUndertimeCount		= 0;
		$TotalDeduct				= 0;
		$Total_Days_EQ				= 0;
		$Total_Absent_EQ			= 0;
		$arr 						= array();
		$RegHour_PerWeek 			= Array(0,0,0,0,0,0,0);
		$TardyHour_PerWeek 			= Array(0,0,0,0,0,0,0);
		$UndertimeHour_PerWeek 		= Array(0,0,0,0,0,0,0);
		$COCHours_PerWeek 			= Array(0,0,0,0,0,0,0);
		$ExcessHour_PerWeek 		= Array(0,0,0,0,0,0,0);
		$OTHours_PerWeek 			= Array(0,0,0,0,0,0,0);
		$UndertimeCount_PerWeek 	= Array(0,0,0,0,0,0,0);
		$AbsentCount_PerWeek 		= Array(0,0,0,0,0,0,0);
		$OTCount_PerWeek 			= Array(0,0,0,0,0,0,0);
		$TardyCount_PerWeek 		= Array(0,0,0,0,0,0,0);
		$WorkDayConversion 			= file_get_contents(json."WorkDayConversion.json");
		$WorkDayEQ   				= json_decode($WorkDayConversion, true);
		/*==============================================================================================================*/
		/*==============================================================================================================*/
		//SETTING OF DATE RANGE
		/*==============================================================================================================*/
		$new_week 					= "";
		$week_count 				= 0;
		$userID 					= 0;
		$NewMonth 					= intval($month);
		$NewYear 					= intval($year);
		if ($NewMonth <= 9) {
			$NewMonth = "0".$NewMonth;
		}
		$curr_date					= date("Y-m-d",time());
		$num_of_days 				= cal_days_in_month(CAL_GREGORIAN,$NewMonth,$NewYear);
		$exact_end_days 			= $num_of_days;
		$month_start 				= $NewYear."-".$NewMonth."-01";
		$month_end 					= $NewYear."-".$NewMonth."-".$num_of_days;
		if (strtotime($curr_date) <= strtotime($month_end)) {
			$month_end = $curr_date;
		} else {
			$month_end = $month_end;
		}
		$num_of_days 				= date("d",strtotime($month_end));

		for($d=1;$d<=$num_of_days;$d++) {
			$y = $NewYear."-".$NewMonth."-".$d;
			$day = date("D",strtotime($y));	
			if ($d <= 9) $d = "0".$d;
			$y = $NewYear."-".$NewMonth."-".$d;
			$week = date("W",strtotime($y));
			if ($week != $new_week) {
				$week_count++;
			}
			$dtr = [
		   		"AttendanceDate" => "$y",
		   		"AttendanceTime" => "",
		   		"UTC" => "",
		   		"TimeIn" => "",
		   		"LunchOut" => "",
		   		"LunchIn" => "",
		   		"TimeOut" => "",
		   		"OBOut" => "",
		   		"OBIn" => "",
		   		"Day" => "$d",
		   		"Week" => "$week_count",
		   		"KEntry" => "",
		   		"Holiday" => "",
		   		"CTO"=>"",
		   		"Leave"=> "",
		   		"OffSus"=>"",
		   		"OffSusName"=>"",
		   		"HasOT"=>"",
		   		"OBName"=>"",
		   		"OBTimeIn"=>"",
		   		"OBTimeOut"=>"",
		   		"OTTime"=>""
		   	];
		   	$arr["ARR"][$y] = $dtr;
			$new_week = $week;
		}
		/*==============================================================================================================*/
		$row = FindFirst("employees","WHERE RefId = $emprefid","*");
		if ($row){ 
			$emprefid 					= $row["RefId"];
			$biometricsID 				= $row["BiometricsID"];
			$CompanyID 					= $row["CompanyRefId"];
			$BranchID 					= $row["BranchRefId"];
			$KEntryContent 				= file_get_contents(json."Settings_".$CompanyID.".json");
		  	$KEntry_json   				= json_decode($KEntryContent, true);
		  	$OTMinTime 					= $KEntry_json["OTMinTime"];
		  	$COCMinTime 				= $KEntry_json["COCMinTime"];
			$Default_qry				= "WHERE CompanyRefId = ".$CompanyID." AND BranchRefId = ".$BranchID;
			/*==============================================================================================================*/
			if ($workschedule == "") {
				return false;
			} else {
				/*==========================================================================================================*/
				//GETTING OF ATTENDANCE IN EMPLOYEE ATTENDANCE
				/*==========================================================================================================*/
				$where_empAtt 			= $Default_qry;
				$where_empAtt 			.= " AND EmployeesRefId = '".$emprefid."'";
				$where_empAtt       	.= " AND AttendanceDate BETWEEN '".$month_start."' AND '".$month_end."'";
				$rs_empAtt 				= SelectEach("employeesattendance",$where_empAtt);
				if ($rs_empAtt) {
					while ($row = mysqli_fetch_assoc($rs_empAtt)) {
						$AttendanceDate 	= $row["AttendanceDate"];
						$AttendanceTime 	= $row["AttendanceTime"];
						$UTC 				= $row["CheckTime"];
						$KEntry         	= $row["KindOfEntry"];
						$fld 				= "";
						$val 				= "";
						switch ($KEntry) {
							case 1:
								$fld 		= "TimeIn";
								$val     	= get_today_minute($UTC);
								break;
							case 2:
								$fld 		= "LunchOut";
								$val     	= get_today_minute($UTC);
								break;
							case 3:
								$fld 		= "LunchIn";
								$val     	= get_today_minute($UTC);
								break;
							case 4:
								$fld 		= "TimeOut";
								$val     	= get_today_minute($UTC);
								break;
							/*case 7:
								$fld 		= "OBOut";
								$val     	= get_today_minute($UTC);
								break;
							case 8:
								$fld 		= "OBIn";
								$val     	= get_today_minute($UTC);
								break;*/
						}
						$arr["ARR"][$AttendanceDate][$fld] = $val;
						$arr["ARR"][$AttendanceDate]["UTC"] = $UTC;
					}
				}
				/*==========================================================================================================*/
				/*==========================================================================================================*/
				//GETTING ATTENDANCE IN BIOMETRICS
				/*==========================================================================================================*/
				switch ($CompanyID) {
					case '2':
						//include_once 'bio_conn.e2e.php';
						include_once 'inc/inc_biometrics_2.e2e.php';
						break;
					case '28':
						//include_once 'bio_conn_28.e2e.php';
						include_once 'inc/inc_biometrics_28.e2e.php';
						break;
					case '14':
						include_once 'inc/inc_biometrics_14.e2e.php';
						break;
					default:
						include_once 'inc/inc_bio_default.e2e.php';
						break;
				}
			}
			/*==========================================================================================================*/
			/*==========================================================================================================*/
			//CHECKING HOLIDAY TABLE
			/*==========================================================================================================*/
			$holiday = SelectEach("holiday","");
		    if ($holiday) {
		        while ($row = mysqli_fetch_assoc($holiday)) {
		            $StartDate        = $row["StartDate"];
		            $EndDate          = $row["EndDate"];
		            $holiday_date_diff   = dateDifference($StartDate,$EndDate);
		            $Name             = $row["Name"];
		            $EveryYr          = $row["isApplyEveryYr"];
		            $Legal            = $row["isLegal"];
		            $temp_arr         = explode("-", $StartDate);
		            $temp_date        = $NewYear."-".$temp_arr[1]."-".$temp_arr[2];
		            for ($H=0; $H <= $holiday_date_diff; $H++) { 
		              	$holiday_NewDate = date('Y-m-d', strtotime($temp_date. ' + '.$H.' days'));
		          		if ($EveryYr == 1) {
		                 	if(isset($arr["ARR"][$holiday_NewDate])) {
		                        $arr["ARR"][$holiday_NewDate]["Holiday"] = $Name;
		                	}   
		              	} else {
		                 	if(isset($arr["ARR"][$StartDate])) {
		                    	$arr["ARR"][$StartDate]["Holiday"] = $Name;
		                 	}
		              	}
		           	}
		        }
		    }
		    /*==========================================================================================================*/
			/*==========================================================================================================*/
			//CHECKING OFFICE SUSPENSION TABLE
			/*==========================================================================================================*/
			$officesuspension = SelectEach("officesuspension","");
		  	if ($officesuspension) {
		     	while ($row = mysqli_fetch_assoc($officesuspension)) {
		            $StartDate        = $row["StartDate"];
		        	$EndDate          = $row["EndDate"];
		        	$OffSus_date_diff = dateDifference($StartDate,$EndDate);
		        	$Name             = $row["Name"];
		        	$OffSusTime       = $row["StartTime"];
		        	$temp_arr         = explode("-", $StartDate);
		        	for ($OS=0; $OS <= $OffSus_date_diff; $OS++) { 
		          		$OffSus_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$OS.' days'));
		          		if(isset($arr["ARR"][$OffSus_NewDate])) {
		             		$arr["ARR"][$OffSus_NewDate]["OffSusName"] 	= $Name;
		                	$arr["ARR"][$OffSus_NewDate]["OffSus"] 		= $OffSusTime;
		          		}   
		       		}
		     	}
		  	}
		  	/*==========================================================================================================*/
			/*==========================================================================================================*/
			//GETTING EMPLOYEE LEAVE
			/*==========================================================================================================*/
			$where_leave 		= $Default_qry;
			$where_leave		.= " AND EmployeesRefId = ".$emprefid;
			$where_leave		.= " AND ApplicationDateFrom BETWEEN '".$month_start."'";
			$where_leave  		.= " AND '".$month_end."'";
			$where_leave 		.= " AND Status = 'Approved'";
			$rs_leave			= SelectEach("employeesleave",$where_leave);
			if ($rs_leave) {
				while ($row = mysqli_fetch_assoc($rs_leave)) {
					$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
					for ($i=0; $i <= $diff ; $i++) { 
						$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
						if(isset($arr["ARR"][$temp_date])) {
		                 	$arr["ARR"][$temp_date]["Leave"] = $row["LeavesRefId"];
		              	}
					}
				}
			}
			/*==========================================================================================================*/
			/*==========================================================================================================*/
			//GETTING OFFICE AUTHORITY NAME
			/*==========================================================================================================*/
			$where_OB 			= $Default_qry;
			$where_OB			.= " AND EmployeesRefId = ".$emprefid;
			$where_OB			.= " AND ApplicationDateFrom BETWEEN '".$month_start."'";
			$where_OB  			.= " AND '".$month_end."'";
			$where_OB 			.= " AND Status = 'Approved'";
			$rs_OB				= SelectEach("employeesauthority",$where_OB);
			if ($rs_OB) {
				while ($row = mysqli_fetch_assoc($rs_OB)) {
					$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
					$OBName = getRecord("absences",$row["AbsencesRefId"],"Name");
					$whole = $row["Whole"];
					$time_in = $row["FromTime"];
					$time_out = $row["ToTime"];
					for ($i=0; $i <= $diff ; $i++) { 
						$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
						if(isset($arr["ARR"][$temp_date])) {
	                     	$arr["ARR"][$temp_date]["OBName"] = $OBName;
	                     	if ($whole == 1) {
	                     		$arr["ARR"][$temp_date]["OBTimeIn"] = "Whole";
	                     		$arr["ARR"][$temp_date]["OBTimeOut"] = "Whole";	
	                     	} else {
	                     		$arr["ARR"][$temp_date]["OBTimeIn"] = $time_in;
	                     		$arr["ARR"][$temp_date]["OBTimeOut"] = $time_out;	
	                     	}
	                     	
	                  	}
					}
				}
			}
			/*==========================================================================================================*/
			/*==========================================================================================================*/
			//GETTING EMPLOYEE CTO AVAILMENT
			/*==========================================================================================================*/
			$where_cto 		= $Default_qry;
			$where_cto		.= " AND EmployeesRefId = ".$emprefid;
			$where_cto		.= " AND ApplicationDateFrom BETWEEN '".$month_start."' AND '".$month_end."'";
			$where_cto 		.= " AND Status = 'Approved'";
			$rs_cto			= SelectEach("employeescto",$where_cto);
			if ($rs_cto) {
				while ($row = mysqli_fetch_assoc($rs_cto)) {
					$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
					for ($i=0; $i <= $diff; $i++) { 
						$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
						if(isset($arr["ARR"][$temp_date])) {
		                 	$arr["ARR"][$temp_date]["CTO"] = $row["Hours"]*60;
		              	}
					}
				}
			}
			/*==========================================================================================================*/
			/*==========================================================================================================*/
			//GETTING EMPLOYEE OVERTIME REQUEST
			/*==========================================================================================================*/
			$where_OT    = $Default_qry;
			$where_OT   .= " AND EmployeesRefId = ".$emprefid;
			$where_OT   .= " AND StartDate BETWEEN '".$month_start."' AND '".$month_end."'";
			$where_OT   .= " AND Status = 'Approved'";
			$rs_OT 		 = SelectEach("overtime_request",$where_OT);
			if ($rs_OT) {
				while ($row = mysqli_fetch_assoc($rs_OT)) {
					$StartDate 		= $row["StartDate"];
					$EndDate 		= $row["EndDate"];
					$WithPay        = $row["WithPay"];
					$OTFrom			= $row["FromTime"];
					$OTTo 			= $row["ToTime"];
					$OTClass 		= $row["OTClass"];
					if ($OTClass == 2) {
						if ($OTTo > $OTFrom) {
							$OTTime = $OTTo - $OTFrom;
							for ($y=60; $y <= $OTTime ; $y+=60) {
								if ($y == 180) {
									$dummy = $OTTime - 180;
									if ($dummy > 60) {
										$OTTime -= 60;	
									} else {
										$OTTime -= $dummy;
									}
									
								}
								/*if ($y >= 180 && $y <= 240) {
									$OTTime = $OTTime - ($OTTime -180);
								} else {
									$modulus_y = $y % 240;
									if ($modulus_y == 0) {
										$OTTime = $OTTime - 60;
									}		
								}*/
							}
						} else {
							$OTTime = (1440 - $OTFrom) + $OTTo;
							for ($x=60; $x < $OTTime ; $x+=60) { 
								$modulus = $x % 240;
								if ($modulus == 0) {
									$OTTime = $OTTime - 60;
								}
							}
						}
					} else {
						$OTTime = "";
					}
					$OT_DateDiff 	= dateDifference($StartDate,$EndDate);
					for ($O=0; $O <= $OT_DateDiff; $O++) { 
	               		$OT_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$O.' days'));
	               		if ($WithPay == 1) {
	               			$Pay = 1;
	               		} else {
	               			$Pay = 0;
	               		}
	               		if(isset($arr["ARR"][$OT_NewDate])) {
	                     	$arr["ARR"][$OT_NewDate]["HasOT"] = $Pay;
	                     	$arr["ARR"][$OT_NewDate]["OTTime"] = $OTTime;
	                  	}
	               	}
				}
			}
			/*==============================================================================================================*/
			/*==============================================================================================================*/
			/*==============================================================================================================*/
			/*==============================================================================================================*/
			/*==============================================================================================================*/
			/*==============================================================================================================*/
			/*==============================================================================================================*/
			/*==============================================================================================================*/
			//START OF COMPUTATION AND SHOOTING
			/*==============================================================================================================*/

			$Emp_WorkSched 		= FindFirst("workschedule"," WHERE RefId = ".$workschedule,"*");
			foreach ($arr as $value) {
				foreach ($value as $key => $row) {
					$Remarks 					= "";
				    $TardyHour 					= "";
				    $UndertimeHour 				= "";
				    $arr_TI						= "";
					$arr_OBO					= "";
					$arr_TO						= "";
					$arr_OBI					= "";
					$arr_Holiday				= "";
					$arr_leave 					= "";
					$arr_COC					= "";
					$arr_OffSus					= "";
					$arr_HasOT 					= "";
					$arr_OBName 				= "";
					$invisible_time 			= 0;
					$day_name 					= date("D",strtotime($row['AttendanceDate']));
					if ($day_name != "") {
						switch ($day_name) {
							case 'Mon':
								$day_name = "Monday";
								break;
							case 'Tue':
								$day_name = "Tuesday";
								break;
							case 'Wed':
								$day_name = "Wednesday";
								break;
							case 'Thu':
								$day_name = "Thursday";
								break;
							case 'Fri':
								$day_name = "Friday";
								break;
							case 'Sat':
								$day_name = "Saturday";
								break;
							case 'Sun':
								$day_name = "Sunday";
								break;
						}
					}	
					$day 						= $row["Day"]; 
					$day_in             	   	= $day_name."In";
					$day_out            	   	= $day_name."Out";
					$day_flexi          	   	= $day_name."FlexiTime";
					$day_LBOut					= $day_name."LBOut";
					$day_LBIn					= $day_name."LBIn";
					$day_RestDay				= $day_name."isRestDay";
					$day_isflexi          	   	= $day_name."isFlexi";


					$data_flexi             	= $Emp_WorkSched[$day_flexi];
					$data_isflexi             	= $Emp_WorkSched[$day_isflexi];
					$data_timein            	= $Emp_WorkSched[$day_in];
					$data_timeout 				= $Emp_WorkSched[$day_out];
					$data_LunchOut 				= $Emp_WorkSched[$day_LBOut];
					$data_LunchIn 				= $Emp_WorkSched[$day_LBIn];
					$data_RestDay      			= $Emp_WorkSched[$day_RestDay];
					$day_work_hours_count   	= ($data_timeout - $data_timein) - ($data_LunchIn - $data_LunchOut);
					$Week 						= $row["Week"];	
					$AutoLB						= $Emp_WorkSched["AutoLB"];
					$PerDayHours                = $day_work_hours_count / 60;
					$core_time 					= $data_flexi + ($data_timeout - $data_timein);

					

					$arr_TI						= $row["TimeIn"];
					$arr_OBO					= $row["OBOut"];
					$arr_TO						= $row["TimeOut"];
					$arr_OBI					= $row["OBIn"];
					$arr_Holiday				= $row["Holiday"];
					$arr_leave 					= $row["Leave"];
					$arr_COC					= $row["CTO"];
					$arr_OffSus					= $row["OffSus"];
					$arr_HasOT 					= $row["HasOT"];
					$arr_OBName					= $row["OBName"];
					$arr_OT						= $row["OTTime"];


					if ($arr_TI == "" && $arr_leave != "") {
						$arr_TI = $data_timein;
						$arr_TO = $data_timeout;
					}
					// if ($arr_TI == "" && $arr_OBO != "") {
					// 	$arr_TI = $arr_OBO;
					// }
					// if ($arr_TO == "" && $arr_OBI != "") {
					// 	$arr_TO = $arr_OBI;
					// }

					if ($arr_Holiday != "" && $arr_TI == "") {
						$arr_TI = $data_timein;
						$arr_TO = $data_timeout;
					}
					
					if ($arr_COC != "") {
						if ($arr_TI < $data_flexi) {
							if ($arr_COC >= $day_work_hours_count) {
								$arr_TI = $data_timein;
								$arr_TO = $data_timeout;
							} else {
								if ($arr_TI == "") {
									$arr_TI = $data_timein;
									$Lunch_Out = $data_LunchOut;
								}
								if ($arr_TO == "") {
									//$arr_TO = $data_timein + $arr_COC;
									$arr_TO = $data_LunchIn + $arr_COC;
								} else {
									$arr_TO = $arr_TO + $arr_COC;
									
								}
							}
						} else {
							$arr_TI = $data_timein;
							$arr_TO = $data_timeout;
						}		
					}

					/*if ($arr_OffSus != "") {
						if ($arr_TI == "") {
							$arr_TI = $arr_OffSus;
						}
						if ($arr_TO == "") {
							$arr_TO = $data_timeout;
						}
					}*/
					/*if ($arr_OffSus != "") {
						$arr_TI = $data_timein;
						$arr_TO = $data_timeout;
					}*/
					if ($arr_TI != "") {
						$PresentDays++;
					}
					/*===================================================================================================*/
					/*===================================================================================================*/
					//MAXIMUM TIME FROM FOR EMPLOYEE TO ENTER WITHOUT LATE
					/*===================================================================================================*/
					if ($Emp_WorkSched["ScheduleType"] == "Fi") {
						$data_flexi = $data_timein;
					} else {
						$data_flexi = $data_flexi;
					}
					/*===================================================================================================*/
					/*===================================================================================================*/
					//GETTING LATE
					/*===================================================================================================*/
					if ($data_RestDay != 1) {
						if ($arr_TI != "" || $arr_OBO != "") {
							if ($data_isflexi != 1) {
								if ($arr_TI >= $data_timein) {
									$TardyHour 					= "";
								} else {
									$TardyHour 					= ($arr_TI - $data_flexi);
									$TardyHour_PerWeek[$Week] 	= $TardyHour_PerWeek[$Week] + $TardyHour;
									$TardyCount_PerWeek[$Week]++;
									$Days_Tardy .= $day."|";
								}
							} else {
								if ($arr_TI <= $data_flexi) {
									$TardyHour = "";
								} else {
									$TardyHour 					= ($arr_TI - $data_flexi);
									$TardyHour_PerWeek[$Week] 	= $TardyHour_PerWeek[$Week] + $TardyHour;
									$TardyCount_PerWeek[$Week]++;
									$Days_Tardy .= $day."|";
								}
							}
						}	
					}
					if ($arr_OffSus != "") {
						if ($TardyHour != "") {
							$arr_TO = $core_time;
						} else {
							$arr_TI = $data_timein;
							$arr_TO = $data_timeout;
						}
						
					}
					if ($arr_OBName != "") {
						if ($TardyHour != "") {
							$arr_TO = $data_timeout;
						} else {
							$arr_TI = $data_timein;
							$arr_TO = $data_timeout;
						}
					}
					/*===================================================================================================*/
					/*===================================================================================================*/
					//GETTING THE WORKING AND EXCESS HOURS OF THE EMPLOYEE
					/*===================================================================================================*/
					if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
						if ($arr_TI <= $data_timein) {
							$arr_TI = $data_timein;
						}
						if ($TardyHour != "") {
							if ($arr_TO > $core_time) {
								$arr_TO = $core_time;
							}
						}
						$consume_time = (($arr_TO - $arr_TI) - ($data_LunchIn - $data_LunchOut));
						$excess_time = $consume_time - $day_work_hours_count; 	
					} else {
						$excess_time = 0;
						$consume_time = 0;
					}
					/*===================================================================================================*/
					/*===================================================================================================*/
					//GETTING THE UNDERTIME AND EXCESS HOURS OF THE EMPLOYEE
					/*===================================================================================================*/
					if ($consume_time >= $day_work_hours_count) {
						if ($day_work_hours_count != "") {
							$RegHour_PerWeek[$Week] = $RegHour_PerWeek[$Week] + $day_work_hours_count;
						}
						$consume_time = $day_work_hours_count;
					} else {
						if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
							$excess_time 					= 0;
							if ($TardyHour != "") {
								$UndertimeHour = $core_time - $arr_TO;
							} else {
								$UndertimeHour = $day_work_hours_count - $consume_time;	
							}
							$UndertimeHour_PerWeek[$Week] 	= $UndertimeHour_PerWeek[$Week] + $UndertimeHour;
							$UndertimeCount_PerWeek[$Week]++;
							$Days_Undertime .= $day."|";
						} else {
							$excess_time = 0;
						}
						if ($consume_time != 0) {
							$RegHour_PerWeek[$Week] = $RegHour_PerWeek[$Week] + $consume_time;
						}
						$consume_time = $consume_time;
					}
					/*===================================================================================================*/
					/*===================================================================================================*/
					//AUTO LUNCH BREAK
					/*===================================================================================================*/
					if ($AutoLB == 1) {
						if ($arr_TI != "") {
							$Lunch_Out = $data_LunchOut;
							$Lunch_In = $data_LunchIn;
						} else {
							$Lunch_Out = "";
							$Lunch_In = "";
						}
					} else {
						$Lunch_Out = $row["LunchOut"];
						$Lunch_In = $row["LunchIn"];
					}
					/*===================================================================================================*/
					/*===================================================================================================*/
					/*===================================================================================================*/
					/*===================================================================================================*/
					/*===================================================================================================*/
					/*===================================================================================================*/
					//MAPPING
					/*===================================================================================================*/
					if ($TardyHour == "") {						
						if ($arr_HasOT == "1") {
							$excess_time = $excess_time + $invisible_time;
							if ($excess_time >= $OTMinTime) {
								$OTHours_PerWeek[$Week] 			= $OTHours_PerWeek[$Week] + $excess_time;
							} else {
								$excess_time 						= 0;
							}
						} else if ($arr_HasOT == "0") {
							$excess_time = $excess_time + $invisible_time;
							if ($excess_time >= $COCMinTime) {
								$COCHours_PerWeek[$Week] 			= $COCHours_PerWeek[$Week] + $excess_time;
							} else {
								$excess_time 						= 0;
							}
						} else {
							$ExcessHour_PerWeek[$Week] 				= $ExcessHour_PerWeek[$Week] + $excess_time;
						}	
					} else {
						$ExcessHour_PerWeek[$Week] 				= intval($ExcessHour_PerWeek[$Week]) + intval($excess_time);
					}

					if ($arr_TI == "" && $arr_TO == "" && $arr_OBO == "" && $data_RestDay != 1) {
						$AbsentCount_PerWeek[$Week]++;
						$Days_Absent .= $day."|";
					}
					if ($row["Leave"] != "") {
		              	$leave = getRecord("leaves",$row["Leave"],"Name");
		            }
				}
			}
			$TotalRegHour   = $RegHour_PerWeek[1] + 
							  $RegHour_PerWeek[2] +
							  $RegHour_PerWeek[3] + 
							  $RegHour_PerWeek[4] + 
							  $RegHour_PerWeek[5] + 
							  $RegHour_PerWeek[6];
		    
			$TotalTardyHour = $TardyHour_PerWeek[1] + 
							  $TardyHour_PerWeek[2] + 
							  $TardyHour_PerWeek[3] + 
							  $TardyHour_PerWeek[4] + 
							  $TardyHour_PerWeek[5] +
							  $TardyHour_PerWeek[6];
			
			$TotalUndertimeHour = $UndertimeHour_PerWeek[1] + 
								  $UndertimeHour_PerWeek[2] + 
								  $UndertimeHour_PerWeek[3] + 
								  $UndertimeHour_PerWeek[4] + 
								  $UndertimeHour_PerWeek[5] +
								  $UndertimeHour_PerWeek[6];

			$TotalExcessHour    = $ExcessHour_PerWeek[1] + 
		                          $ExcessHour_PerWeek[2] + 
		                          $ExcessHour_PerWeek[3] + 
		                          $ExcessHour_PerWeek[4] +
		                          $ExcessHour_PerWeek[5] +
		                          $ExcessHour_PerWeek[6];
		    if ($CompanyID == "1000") {
		    	$TotalRegHour = $TotalRegHour + $TotalExcessHour;
		    }
			
			
		    $TotalCOCHour       = $COCHours_PerWeek[1] + 
		                          $COCHours_PerWeek[2] + 
		                          $COCHours_PerWeek[3] + 
		                          $COCHours_PerWeek[4] +
		                          $COCHours_PerWeek[5] +
		                          $COCHours_PerWeek[6];
			
		    $TotalTardyCount    = $TardyCount_PerWeek[1] + 
		                          $TardyCount_PerWeek[2] + 
		                          $TardyCount_PerWeek[3] + 
		                          $TardyCount_PerWeek[4] +
		                          $TardyCount_PerWeek[5] +
		                          $TardyCount_PerWeek[6];
			 
		    $TotalUndertimeCount = 	$UndertimeCount_PerWeek[1] + 
			                        $UndertimeCount_PerWeek[2] + 
			                        $UndertimeCount_PerWeek[3] + 
			                        $UndertimeCount_PerWeek[4] +
			                        $UndertimeCount_PerWeek[5] +
			                        $UndertimeCount_PerWeek[6];

			$TotalAbsentCount  =  $AbsentCount_PerWeek[1] + 
		                          $AbsentCount_PerWeek[2] + 
		                          $AbsentCount_PerWeek[3] + 
		                          $AbsentCount_PerWeek[4] +
		                          $AbsentCount_PerWeek[5] +
		                          $AbsentCount_PerWeek[6];

		    $TotalOTHour       =  $OTHours_PerWeek[1] + 
		                          $OTHours_PerWeek[2] + 
		                          $OTHours_PerWeek[3] + 
		                          $OTHours_PerWeek[4] +
		                          $OTHours_PerWeek[5] +
		                          $OTHours_PerWeek[6];
		  	/*===================================================================================================*/
			/*===================================================================================================*/
			//GETTING THE EARNINGS
			/*===================================================================================================*/
			
			$TardyEQ 			= getEquivalent($TotalTardyCount,"workinghrsconversion");
		   	$UndertimeEQ		= getEquivalent($TotalUndertimeCount,"workinghrsconversion");
		   	$TotalRegEQ			= getEquivalent($TotalRegHour,"workinghrsconversion");
		   	
			//$credits_arr        = getLeaveEarnings($emprefid,$CompanyID,$BranchID,$NewMonth);
			$Total_Days         = 31 - $TotalAbsentCount;
		  	$day_eq_vl 			= FindFirst("slvlearneddaily","WHERE NoOfDays =".$Total_Days,"VLEarned");
			$day_eq_sl 			= FindFirst("slvlearneddaily","WHERE NoOfDays =".$Total_Days,"SLEarned");

			
			$Setting_json        = file_get_contents(json."Settings_".$CompanyID.".json");
		    $Setting_arr         = json_decode($Setting_json, true);
		    $PerDayHours         = $Setting_arr["WorkingHrs"];
		    if (!isset($PerDayHours)) $PerDayHours = 8;
			if (isset($PerDayHours)) {
	            for ($i=1; $i <= $Total_Days; $i++) { 
	               $Total_Days_EQ = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $Total_Days_EQ;
	            }
	        }
	        if ($PerDayHours != "") {
	            for ($i=1; $i <= $TotalAbsentCount; $i++) { 
	               $Total_Absent_EQ = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $Total_Absent_EQ;
	            }
	        }



			$dtr_process        = [
				"EmployeesRefId"=>$emprefid,
				"WorkScheduleRefId"=>$workschedule,
				"Month"=>$month,
				"Year"=>$year,
				"Total_Regular_Hr"=>$TotalRegHour,
				"Total_Excess_Hr"=>$TotalExcessHour,
				"Total_COC_Hr"=>$TotalCOCHour,
				"Total_OT_Hr"=>$TotalOTHour,
				"Total_Tardy_Hr"=>$TotalTardyHour,
				"Total_Undertime_Hr"=>$TotalUndertimeHour,
				"Total_Absent_Count"=>$TotalAbsentCount,
				"Total_Present_Count"=>$PresentDays,
				"Total_Tardy_Count"=>$TotalTardyCount,
				"Total_Undertime_Count"=>$TotalUndertimeCount,
				"Days_Absent"=>$Days_Absent,
				"Days_Tardy"=>$Days_Tardy,
				"Days_Undertime"=>$Days_Undertime,
				"VL_Earned"=>floatval($day_eq_vl),
				"SL_Earned"=>floatval($day_eq_sl),
				"SPL_Used"=>"0",
				"Tardy_Deduction_EQ"=>$TardyEQ,
				"Undertime_Deduction_EQ"=>$UndertimeEQ,
				"Regular_Hr_EQ"=>$TotalRegEQ,
				"Total_Days_EQ"=>$Total_Days_EQ,
				"Total_Absent_EQ"=>$Total_Absent_EQ,
				"Processed"=>"1"
			];
		}
		return $dtr_process;
	}
	function dtr_process($emprefid = "",$month = "", $year = ""){
		$curr_date		= date("Y-m-d",time());
		$curr_month     = date("m",time());
		$curr_year		= date("Y",time());
		$start_date		= $curr_year."-".$curr_month."-01";	
		$previous_date  = date('Y-m-d', strtotime($start_date.' - 1 day'));
		$previous_month = date('m', strtotime($previous_date));
		$previous_year  = date('Y', strtotime($previous_date));
		if ($emprefid == "") {
			if ($curr_date == $previous_date) {
				$rs_emp		= SelectEach("employees","");
				if ($rs_emp) {
					while ($row_emp = mysqli_fetch_assoc($rs_emp)) {
						$emprefid 			= $row_emp["RefId"];
						$workschedule 		= FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","WorkScheduleRefId");
						$where 				= "WHERE EmployeesRefId = $emprefid AND Month = '$curr_month' AND Year = '$curr_year'";
						$check_dtr_process 	= FindFirst("dtr_process",$where,"RefId");
						if (!is_numeric($check_dtr_process)) {
							if ($workschedule != "") {
								$dtr_array 		= DTR_Summary($emprefid,$previous_month,$previous_year,$workschedule);
								$flds 			= "";
								$vals 			= "";	
								$table 			= "";
								foreach ($dtr_array as $key => $value) {
									$flds .= "`$key`,";
									$vals .= "'$value', ";
								}
								$save_dtr_process = f_SaveRecord("NEWSAVE","dtr_process",$flds,$vals);	
								if (!is_numeric($save_dtr_process)) {
									echo $save_dtr_process;
									return false;
								}
							}
						}
					}
				}
			}	
		} else {
			$workschedule 	= FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","WorkScheduleRefId");
			$where 			= "WHERE EmployeesRefId = $emprefid AND Month = '$month' AND Year = '$year'";
			$dtr_refid	 	= FindFirst("dtr_process",$where,"RefId");
			$dtr_array 		= DTR_Summary($emprefid,$month,$year,$workschedule);
			$fldnval 		= "";
			foreach ($dtr_array as $key => $value) {
				$fldnval .= "`$key` = '$value', ";
			}
			$edit_dtr_process = f_SaveRecord("EDITSAVE","dtr_process",$fldnval,$dtr_refid);	
			if ($edit_dtr_process != "") {
				echo $edit_dtr_process;
				return false;
			} else {
				return $dtr_refid;
			}
		}
	}
?>