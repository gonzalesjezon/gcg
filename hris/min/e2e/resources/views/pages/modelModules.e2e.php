<?php
   require_once "constant.e2e.php";
   require_once "inc_model.e2e.php";
   require_once "incUtilitiesJS.e2e.php";
?>
   <div class="container" id="EntryScrn">
      <div class="row" id="EntryScrn">
         <div class="col-xs-6">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } ?>
            <input type="hidden" class="form-input saveFields-- mandatory" id="char_ScrnId" name="char_ScrnId" value="<?php echo strtotime("Now"); ?>">
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">CODE:</label><br>
                  <input class="form-input saveFields-- uCase-- mandatory" type="text" placeholder="Code"
                     id="inputs" name="char_Code" style='width:75%' autofocus>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">NAME:</label><br>
                  <input class="form-input saveFields-- mandatory"
                         type="text" placeholder="name"
                         id="inputs" name="char_Name"
                         style='width:75%'>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">ROUTE:</label><br>
                  <input class="form-input saveFields-- mandatory"
                         type="text" placeholder="Route"
                         id="inputs" name="char_Route"
                         style='width:75%'>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">PROGRAM FILE:</label><br>
                  <input class="form-input saveFields-- mandatory"
                         type="text" placeholder="Program File"
                         id="inputs" name="char_Filename"
                         style='width:75%'>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">ICONS:</label><br>
                  <input class="form-input saveFields--"
                         type="text" placeholder="Icons"
                         id="inputs" name="char_Icons"
                         style='width:75%'>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">ORDINAL:</label><br>
                  <input class="form-input saveFields-- mandatory number--"
                         type="text" placeholder="name"
                         id="inputs" name="sint_Ordinal"
                         style='width:75%'>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label">System:</label><br>
                  <select class="form-input saveFields-- mandatory" id="sint_SystemRefId" name="sint_SystemRefId">
                     <option value="">SELECT SYSTEM</option>
                     <?php
                        $rs = SelectEach("system","");
                        if ($rs) {
                           while ($sys = mysqli_fetch_assoc($rs)) {
                              echo '<option value="'.$sys["RefId"].'">'.($sys["Code"]." - ".$sys["Name"]).'</option>';
                           }
                        }
                     ?>
                  </select>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">DESCRIPTION:</label>
                  <textarea class="form-input saveFields--" rows="5" name="char_Description"></textarea>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">REMARKS:</label>
                  <textarea class="form-input saveFields--" rows="5" name="char_Remarks"
                  placeholder="remarks"><?php echo $remarks; ?></textarea>
               </div>
            </div>
         </div>
      </div>
   </div>