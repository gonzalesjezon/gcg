<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_approval") ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php
               doTitleBar("For Aprroval > Leave Cancellation");
               spacer(5);

            ?>
            <div class="row">
               <div class="col-xs-12">
                  <?php
                     $EmpRefId = getvalue("txtRefId");
                     $attr = ["empRefId"=>getvalue("txtRefId"),
                              "empLName"=>getvalue("txtLName"),
                              "empFName"=>getvalue("txtFName"),
                              "empMName"=>getvalue("txtMidName")];
                     $EmpRefId = EmployeesSearch($attr);
                     bar();
                     /*$sql = "SELECT *,fl_cancellation_request.RefId as asRefId FROM employees INNER JOIN fl_cancellation_request
                       WHERE employees.CompanyRefId = ".$CompanyId."
                         AND employees.BranchRefId = ".$BranchId;

                     if (getvalue("txtLName") != "") {
                        $sql .= " AND employees.LastName LIKE '".getvalue("txtLName")."%'";
                     }
                     if (getvalue("txtFName") != "") {
                        $sql .= " AND employees.FirstName LIKE '".getvalue("txtFName")."%'";
                     }
                     if (getvalue("txtMidName") != "") {
                        $sql .= " AND employees.MiddleName LIKE '".getvalue("txtMidName")."%'";
                     }
                     if (getvalue("txtRefId") != "") {
                        $sql .= " AND fl_cancellation_request.EmployeesRefId = '".getvalue("txtRefId")."'";
                     } else {
                        $sql .= " AND employees.RefId = fl_cancellation_request.EmployeesRefId";
                     }
                     $sql .= " and (fl_cancellation_request.Status IS NULL OR fl_cancellation_request.Status = '')";
                     $sql .= " ORDER BY FiledDate LIMIT 20";*/
                     $sql = "SELECT *,fl_cancellation_request.RefId as asRefId FROM fl_cancellation_request
                     INNER JOIN employees 
                     ON fl_cancellation_request.CompanyRefId = employees.CompanyRefId
                     AND fl_cancellation_request.BranchRefId = employees.BranchRefId
                     AND fl_cancellation_request.EmployeesRefId = employees.RefId";

                     if (getvalue("txtLName") != "") {
                        $sql .= " AND employees.LastName LIKE '".getvalue("txtLName")."%'";
                     }
                     if (getvalue("txtFName") != "") {
                        $sql .= " AND employees.FirstName LIKE '".getvalue("txtFName")."%'";
                     }
                     if (getvalue("txtMidName") != "") {
                        $sql .= " AND employees.MiddleName LIKE '".getvalue("txtMidName")."%'";
                     }
                     if (getvalue("txtRefId") != "") {
                        $sql .= " AND fl_cancellation_request.EmployeesRefId = '".getvalue("txtRefId")."'";
                     } 
                     $sql .= " AND fl_cancellation_request.Status IS NULL ORDER BY FiledDate LIMIT 100";
                     $rs = mysqli_query($conn,$sql) or die(mysqli_error($conn));

                  ?>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-1"></div>
               <div class="col-xs-10 padd5">
                  <?php
                     if ($rs) {
                        $recordNum = mysqli_num_rows($rs);
                        while ($row = mysqli_fetch_array($rs)) {
                           $refid = $row["asRefId"];
                           $rsEmp = FFirstRefId("employees",$row["EmployeesRefId"],"*");
                           $where = "WHERE EmployeesRefId = ".$rsEmp["RefId"];
                           $leave = FindFirst("employeesleave","WHERE RefId = ".$row["EmployeesLeaveRefId"],"*");
                           $empinformation = FindFirst('empinformation',$where,"*");
                           if ($empinformation) {
                              $info = array_merge($rsEmp,$empinformation);

                  ?>
                           <div class="mypanel pull-left padd5" style="margin:5px;width:40%;">
                              <div class="panel-top">REF ID:&nbsp;<?php echo $row['RefId']; ?></div>
                              <div class="panel-mid">
                                 <div class="row txt-right" style="margin-right:10px;">
                                    <label>DATE FILE:</label><span style="margin-left:15px;"><?php echo $row["FiledDate"];?></span>
                                 </div>
                                 <?php
                                    echo '
                                    <div class="row margin-top padd5">
                                       <div class="row margin-top">
                                          <div class="col-sm-4 txt-center">
                                             <div class="border" style="height:1.5in;width:1.3in;">
                                                <img src="'.img($rsEmp['CompanyRefId']."/EmployeesPhoto/".$rsEmp['PicFilename']).'" style="width:100%;height:100%;">
                                             </div>
                                          </div>
                                          <div class="col-sm-8 txt-center padd5">';
                                             $templ->btn_apprvReject(2,$refid);
                                          echo    
                                          '</div>
                                       </div>
                                       <div class="row margin-top">   
                                          <div class="col-sm-12">';
                                             $templ->doEmployeeInfo($info);
                                    echo       
                                          '</div>   
                                       </div>   
                                    </div>';
                                    bar();  
                                 ?>
                                 <div class="row">
                                    <div class="col-xs-12">
                                       <label>Leave Date Requested:</label>
                                       <span style="margin-left:15px;">
                                          <?php echo getRecord("leaves",$leave["LeavesRefId"],"Name");?>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-xs-6">
                                       <label>Start Date:</label>
                                       <span style="margin-left:15px;">
                                          <?php echo $leave["ApplicationDateFrom"];?>
                                       </span>
                                    </div>
                                    <div class="col-xs-6">
                                       <label>End Date:</label>
                                       <span style="margin-left:15px;">
                                          <?php echo $leave["ApplicationDateTo"];?>
                                       </span>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       <label>REMARKS:</label>
                                       <br>
                                       <span style="margin-left:15px;">
                                          <?php 
                                             echo $row["Reason"];
                                          ?>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                        <?php
                           }
                        }
                     }

                  ?>

               </div>
               <div class="col-xs-1"></div>
            </div>
            <?php
               footer();
               $table = "fl_cancellation_request";
               modalReject();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>