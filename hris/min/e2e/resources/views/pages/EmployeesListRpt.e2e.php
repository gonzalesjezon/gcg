<?php
   //session_start();
   include 'colors.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $p_TSDfrom            = getvalue("txtSTrainingFrom");
   $p_TSDto              = getvalue("txtSTrainingTo");
   $p_TEDfrom            = getvalue("txtETrainingFrom");
   $p_TEDto              = getvalue("txtETrainingTo");
   $p_WSDfrom            = getvalue("txtWorkSFrom");
   $p_WSDto              = getvalue("txtWorkSTo");
   $p_WEDfrom            = getvalue("txtWorkEFrom");
   $p_WEDto              = getvalue("txtWorkETo");
   $p_VolWSDfrom         = getvalue("txtVWorkSFrom");
   $p_VolWSDto           = getvalue("txtVWorkSTo");
   $p_VolWEDfrom         = getvalue("txtVWorkEFrom");
   $p_VolWEDto           = getvalue("txtVWorkETo");
   $p_EligExamDateFrom   = getvalue("txtEligiExamFrom");
   $p_EligExamDateTo     = getvalue("txtEligiExamTo");
   $p_EducLevel          = getvalue("drpEducLvl");
   /*=============================================*/
   include 'incRptQryString.e2e.php';
   if ($p_TSDfrom != "" && $p_TSDto != "") {
      if ($searchCriteria == "")
         $searchCriteria .= "TRAINING START BETWEEN '$p_TSDfrom' AND '$p_TSDto'";
      else
         $searchCriteria .= "|TRAINING START BETWEEN '$p_TSDfrom' AND '$p_TSDto'";
   }

   if ($p_TEDfrom != "" && $p_TEDto != "") {
      if ($searchCriteria == "")
         $searchCriteria .= "TRAINING END BETWEEN '$p_TEDfrom' AND '$p_TEDto'";
      else
         $searchCriteria .= "|TRAINING END BETWEEN '$p_TEDfrom' AND '$p_TEDto'";
   }

   if ($p_WSDfrom != "" && $p_WSDto != "") {
      if ($searchCriteria == "")
         $searchCriteria .= "WORK START BETWEEN '$p_WSDfrom' AND '$p_WSDto'";
      else
         $searchCriteria .= "|WORK START BETWEEN '$p_WSDfrom' AND '$p_WSDto'";
   }

/*
   if ($p_WEDfrom != "" && $p_WEDto != "") {
      $whereClause .= " and WORK END DATE BETWEEN
         '$p_WEDfrom' AND '$p_WEDto'";
      if ($searchCriteria == "")
         $searchCriteria .= "WORK END BETWEEN
         '$p_WEDfrom' AND '$p_WEDto'";
      else
         $searchCriteria .= "|WORK END BETWEEN
         '$p_WEDfrom' AND '$p_WEDto'";
   }
   if ($p_VolWSDfrom != "" && $p_VolWSDto != "") {
      $whereClause .= " and Vol. WORK START DATE BETWEEN
         '$p_VolWSDfrom' AND '$p_VolWSDto'";
      if ($searchCriteria == "")
         $searchCriteria .= "Vol. WORK START BETWEEN
         '$p_VolWSDfrom' AND '$p_VolWSDto'";
      else
         $searchCriteria .= "|Vol. WORK START BETWEEN
         '$p_VolWSDfrom' AND '$p_VolWSDto'";
   }
   if ($p_VolWEDfrom != "" && $p_VolWEDto != "") {
      $whereClause .= " and WORK END DATE BETWEEN
         '$p_VolWEDfrom' AND '$p_VolWEDto'";
      if ($searchCriteria == "")
         $searchCriteria .= "WORK END BETWEEN
         '$p_VolWEDfrom' AND '$p_VolWEDto'";
      else
         $searchCriteria .= "|WORK END BETWEEN
         '$p_VolWEDfrom' AND '$p_VolWEDto'";
   }
   if ($p_EligExamDateFrom != "" && $p_EligExamDateTo != "") {
      $whereClause .= " and Eligibility Exam DATE BETWEEN
         '$p_EligExamDateFrom' AND '$p_EligExamDateTo'";
      if ($searchCriteria == "")
         $searchCriteria .= "Eligibility Exam BETWEEN
         '$p_EligExamDateFrom' AND '$p_EligExamDateTo'";
      else
         $searchCriteria .= "|Eligibility Exam BETWEEN
         '$p_EligExamDateFrom' AND '$p_EligExamDateTo'";
   }*/
   include "incRptSortBy.e2e.php";
   $rs = SelectEach($table,$whereClause);
   if ($rs) $rowcount = mysqli_num_rows($rs);

   if ($dbg) {
      echo $whereClause;
   }

   function doColumn($row,$s) {
      echo '<tr>';
         if (getvalue("chkEmpRefId") == 'true') {
            if ($s == 0) {
               echo '<th nowrap style="width:50px;padding:5px;" title="Employees Ref. ID">RefId</th>';
            } else {
               echo '<td class="center--">'.$row["RefId"].'</td>';
            }
         }

         if ($s == 0) {
            echo '<th nowrap>Employees Name</th>';
         } else {
            echo '<td nowrap style="padding-left:5px;">'.$row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"].'</td>';
         }

         if (getvalue ("chkCStatus") == 'true') {
            if ($s == 0) {
               echo '<th nowrap>Civil Status</th>';
            } else {
               switch ($row["CivilStatus"]) {
                  case "Si":
                     $CivilStatus = "SINGLE";
                  break;
                  case "Ma":
                     $CivilStatus = "MARRIED";
                  break;
                  case "An":
                     $CivilStatus = "ANNULLED";
                  break;
                  case "Wi":
                     $CivilStatus = "WIDOWED";
                  break;
                  case "Se":
                     $CivilStatus = "SEPARATED";
                  break;
                  case "Ot":
                     $CivilStatus = "Others, Please Specify";
                  break;
                  default:
                     $CivilStatus = "";
                  break;
               }
               echo '<td nowrap class="center--">'.$CivilStatus.'</td>';
            }
         }
         if (getvalue ("chkSex") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Sex</th>';
            } else {
               echo '<td nowrap class="center--">'.$row["Sex"].'</td>';
            }
         }
         if (getvalue ("chkCitizenship") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Citizenship</th>';
            } else {
               echo '<td nowrap>'.$row["CitizenshipRefId"].'</td>';
            }
         }
         if (getvalue ("chkBirth") == 'true') {
            if ($s==0) {
               echo '<th nowrap>DOB</th>';
            } else {
               echo '<td nowrap class="center--">'.$row["BirthDate"].'</td>';
            }
         }
         if (getvalue ("chkHeight") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Height(m)</th>';
            } else {
               echo '<td nowrap class="center--">'.$row["Height"].'</td>';
            }

         }
         if (getvalue ("chkWeight") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Weight(lbs.)</th>';
            } else {
               echo '<td nowrap class="center--">'.$row["Weight"].'</td>';
            }
         }
         if (getvalue ("chkBlood") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Blood</th>';
            } else {
               echo '<td nowrap class="center--">'.getRecord("bloodtype",$row["BloodTypeRefId"],"Name").'</td>';
            }
         }
         if (getvalue ("chkResAddress") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Res. Address</th>';
            } else {
               echo '<td nowrap></td>';
            }
         }
         if (getvalue ("chkResZipCode") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Res. Zip Code</th>';
            } else {
               echo '<td nowrap class="center--"></td>';
            }
         }
         if (getvalue ("chkResTelNo") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Res. Tel No.</th>';
            } else {
               echo '<td nowrap class="center--">'.$row["ResidentTelNo"].'</td>';
            }
         }
         if (getvalue ("chkPermAddress") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Perm. Address</th>';
            } else {
               echo '<td nowrap>NOT YET</td>';
            }
         }
         if (getvalue ("chkPermZipCode") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Perm. Zip Code</th>';
            } else {
               echo '<td nowrap>NOT YET</td>';
            }
         }
         if (getvalue ("chkPermTelNo") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Perm. Tel No.</th>';
            } else {
               echo '<td nowrap class="center--">'.$row["TelNo"].'</td>';
            }
         }
         if (getvalue ("chkEmail") == 'true') {
            if ($s==0) {
               echo '<th nowrap>E-Mail Address</th>';
            } else {
               echo '<td nowrap>'.$row["EmailAdd"].'</td>';
            }
         }
         if (getvalue ("chkMobileNo") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Mobile No.</th>';
            } else {
               echo '<td nowrap class="center--">'.$row["MobileNo"].'</td>';
            }
         }
         if (getvalue ("chkAgencyEmpNo") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Agency Employee No.</th>';
            } else {
               echo '<td nowrap class="center--">'.$row["AgencyId"].'</td>';
            }
         }
         if (getvalue ("chkTinNo") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Tin. No.</th>';
            } else {
               echo '<td nowrap class="center--">'.$row["TIN"].'</td>';
            }
         }
          if (getvalue ("chkPHILHEALTHNo") == 'true') {
            if ($s==0) {
               echo '<th nowrap>PHILHEALTH No.</th>';
            } else {
               echo '<td nowrap class="center--">NOT YET</td>';
            }
         }
         if (getvalue ("chkGSIS") == 'true') {
            if ($s==0) {
               echo '<th nowrap>GSIS</th>';
            } else {
               echo '<td nowrap class="center--">'.$row["GSIS"].'</td>';
            }
         }
         if (getvalue ("chkPAGIBIG") == 'true') {
            if ($s==0) {
               echo '<th nowrap>PAGIBIG</th>';
            } else {
               echo '<td nowrap class="center--">'.$row["PAGIBIG"].'</td>';
            }
         }

         if (getvalue ("chkEducLvl") == 'true'){
            if ($s==0) {
               echo '<th nowrap>EDUCATION LEVEL</th>';
            } else {
               echo '<td nowrap class="center--">'.$GLOBALS["EducLevelInfo"].'</td>';
            }
         }


         if (getvalue ("chkTrainStartDate") == 'true'){
            if ($s==0) {
               echo '<th nowrap>Start Training Date</th>';
            } else {
               echo '<td nowrap class="center--">'.$GLOBALS["dtStrTraining"].'</td>';
            }
         }
         if (getvalue ("chkTrainEndDate") == 'true'){
            if ($s==0) {
               echo '<th nowrap>End Training Date</th>';
            } else {
               echo '<td nowrap class="center--">'.$GLOBALS["dtEndTraining"].'</td>';
            }
         }

         if (getvalue ("chkStrWorkDate") == 'true') {
            if ($s==0) {
               echo '<th nowrap>Start Work Date</th>';
            } else {
               echo '<td nowrap class="center--">'.$GLOBALS["dtStrWork"].'</td>';
            }
         }

         if (getvalue ("chkStrWorkDate") == 'true') {
            if ($s==0) {
               echo '<th nowrap>End Work Date</th>';
            } else {
               echo '<td nowrap class="center--">'.$GLOBALS["dtEndWork"].'</td>';
            }
         }

      echo '</tr>';
   }
   /*Start Here Date Validation --*/
   $errmsg = "";
   chkDateRange($p_TSDfrom,$p_TSDto);
   chkDateRange($p_TEDfrom,$p_TEDto);
   chkDateRange($p_WSDfrom,$p_WSDto);
   chkDateRange($p_WSDfrom,$p_WSDto);
   chkDateRange($p_VolWSDfrom,$p_VolWSDto);
   chkDateRange($p_VolWEDfrom,$p_VolWEDto);
   chkDateRange($p_EligExamDateFrom,$p_EligExamDateTo);
   /*End Here - Date Validation*/
   $recordsCount = 0;
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style>
         td {vertical-align:top;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Employees List");
            if ($rs && $errmsg == "")
            {

         ?>
            <table border="1">
               <tr>
               <?php
                  doColumn("",0);
               ?>
               </tr>
               <?php
                  $empRefid = "";
                  $_singleM = $_singleF = $_marriedM = $_marriedF = $_annulledM = $_annulledF = $_widowedM = 0;
                  $_widowedF = $_separatedM = $_separatedF = $_othersM = $_othersF = $_undef = $_elemM = $_elemF = 0;
                  $_secondM = $_secondF = $_vocM = $_vocF = $_collegeM = $_collegeF = $_college_gradM = $_college_gradF = 0;
                  $_educ_undef = 0;

                  while ($row = mysqli_fetch_assoc($rs))
                  {
                     $empRefid = $row['RefId'];
                     $EducLevelInfo = "";
                     /* FILTER */

                     $isProceed = true;
                     $dtStrTraining = $dtEndTraining = "";
                     $dtStrWork = $dtEndWork = "";
                     $dtVolStrWork = $dtVolEndWork = $dtEligExamDate = "" ;


                     $rsRow = chk_get_row($empRefid,"EmployeesTraining",$p_TSDfrom,$p_TSDto,"StartDate");
                     if ($rsRow)
                        $dtStrTraining = $rsRow["StartDate"];
                     else
                        $isProceed = false;

                     $rsRow = chk_get_row($empRefid,"EmployeesTraining",$p_TEDfrom,$p_TEDto,"EndDate");
                     if ($rsRow)
                        $dtEndTraining = $rsRow["EndDate"];
                     else
                        $isProceed = false;

                     $rsRow = chk_get_row($empRefid,"EmployeesWorkExperience",$p_WSDfrom,$p_WSDto,"WorkStartDate");
                     if ($rsRow)
                        $dtStrWork = $rsRow["WorkStartDate"];
                     else
                        $isProceed = false;

                     $rsRow = chk_get_row($empRefid,"EmployeesWorkExperience",$p_WEDfrom,$p_WEDto,"WorkEndDate");
                     if ($rsRow)
                        $dtEndWork = $rsRow["WorkEndDate"];
                     else
                        $isProceed = false;

                     $rsRow = chk_get_row($empRefid,"EmployeesVoluntary",$p_VolWSDfrom,$p_VolWSDto,"StartDate");
                     if ($rsRow)
                        $dtVolStrWork = $rsRow["StartDate"];
                     else
                        $isProceed = false;

                     $rsRow = chk_get_row($empRefid,"EmployeesVoluntary",$p_VolWEDfrom,$p_VolWEDto,"EndDate");
                     if ($rsRow)
                        $dtVolEndWork = $rsRow["EndDate"];
                     else
                        $isProceed = false;

                     $rsRow = chk_get_row($empRefid,"EmployeesElegibility",$p_EligExamDateFrom,$p_EligExamDateTo,"ExamDate");
                     if ($rsRow)
                        $dtEligExamDate = $rsRow["ExamDate"];
                     else
                        $isProceed = false;

                     /*maghnap ng educ level sa table na employeeseducBG depende sa pinili ni user $p_EducLevel*/
                     if (
                           $p_EducLevel != "" ||
                           getvalue("chkEducLvl") == "true"
                        ) {
                           if ($p_EducLevel > 0)
                              $crit = " WHERE EmployeesRefId = $empRefid AND LevelType = $p_EducLevel";
                           else
                              $crit = " WHERE EmployeesRefId = $empRefid";


                           $rs_EducLevel = f_Find("EmployeesEduc",$crit);
                           if ($rs_EducLevel) {
                              $empEducLevelHad = "";
                              while ($row_EducLevel = mysqli_fetch_assoc($rs_EducLevel)) {
                                 $SchoolName = getRecord("Schools",$row_EducLevel["SchoolsRefId"],"Name");
                                 $EducLevelInfo .= "<div>[".$row_EducLevel["LevelType"]."] ".$SchoolName."</div>";
                                 $empEducLevelHad = "|".$empEducLevelHad.$row_EducLevel["LevelType"];
                              }
                              //------Count EducLevel-----------------------------------------------------
                                 if ($row["Sex"] == "M"){
                                    if (stripos($empEducLevelHad,"1") > 0) $_elemM++;
                                    if (stripos($empEducLevelHad,"2") > 0) $_secondM++;
                                    if (stripos($empEducLevelHad,"3") > 0) $_vocM++;
                                    if (stripos($empEducLevelHad,"4") > 0) $_collegeM++;
                                    if (stripos($empEducLevelHad,"5") > 0) $_college_gradM++;
                                 }
                                 else if ($row["Sex"] == "F"){
                                    if (stripos($empEducLevelHad,"1") > 0) $_elemF++;
                                    if (stripos($empEducLevelHad,"2") > 0) $_secondF++;
                                    if (stripos($empEducLevelHad,"3") > 0) $_vocF++;
                                    if (stripos($empEducLevelHad,"4") > 0) $_collegeF++;
                                    if (stripos($empEducLevelHad,"5") > 0) $_college_gradF++;

                                 } else $_educ_undef++;
                                 $total_elem          = $_elemM + $_elemF;
                                 $total_second        = $_secondM + $_secondF;
                                 $total_voc           = $_vocM + $_vocF;
                                 $total_college       = $_collegeM + $_collegeF;
                                 $total_college_grad  = $_college_gradM + $_college_gradF;

                           //----------------------------------------------------------------------
                           }
                           else {
                              if ($p_EducLevel > 0) $isProceed = false;
                              $_educ_undef++;
                           }
                        }
                     if ($isProceed) {
                        $recordsCount++;
                        if ($row["Sex"] == "M") $_male++;
                        else if ($row["Sex"] == "F") $_female++;
                        else $_sex_undef++;
                        $total_sex = $_male + $_female + $_sex_undef;



                        if ($row["CivilStatus"] == "Si" && $row["Sex"] == "M") $_singleM++;
                        else if ($row["CivilStatus"] == "Si" && $row["Sex"] == "F") $_singleF++;
                        else if ($row["CivilStatus"] == "Ma" && $row["Sex"] == "M") $_marriedM++;
                        else if ($row["CivilStatus"] == "Ma" && $row["Sex"] == "F") $_marriedF++;
                        else if ($row["CivilStatus"] == "An" && $row["Sex"] == "M") $_annulledM++;
                        else if ($row["CivilStatus"] == "An" && $row["Sex"] == "F") $_annulledF++;
                        else if ($row["CivilStatus"] == "Wi" && $row["Sex"] == "M") $_widowedM++;
                        else if ($row["CivilStatus"] == "Wi" && $row["Sex"] == "F") $_widowedF++;
                        else if ($row["CivilStatus"] == "Se" && $row["Sex"] == "M") $_separatedM++;
                        else if ($row["CivilStatus"] == "Se" && $row["Sex"] == "F") $_separatedF++;
                        else if ($row["CivilStatus"] == "Ot" && $row["Sex"] == "M") $_othersM++;
                        else if ($row["CivilStatus"] == "Ot" && $row["Sex"] == "F") $_othersF++;
                        else $_undef++;
                        $total_si = $_singleM + $_singleF ;
                        $total_ma = $_marriedM + $_marriedF ;
                        $total_an = $_annulledM + $_annulledF ;
                        $total_wi = $_widowedM + $_widowedF ;
                        $total_se = $_separatedM + $_separatedF ;
                        $total_ot = $_othersM + $_othersF ;
                        $total_civil = $total_si +
                                       $total_ma +
                                       $total_an +
                                       $total_wi +
                                       $total_se +
                                       $total_ot +
                                       $_undef;
                        doColumn($row,1);
                     }
                  }

            echo '</table>';
                  echo "RECORD COUNT : ".$recordsCount;
                  if (getvalue("chkRptSummary") == 'true') {
                     spacer(5);
               ?>
                     <table style="width:30%;" border="1">
                        <tr align="center">
                           <th colspan="6">GENDER</th>
                        </tr>
                        <tr align="center">
                           <th>MALE</th>
                           <th>FEMALE</th>
                           <th>UNDEFINED</th>
                           <th>TOTAL</th>
                        </tr>
                        <tr align="center">
                           <td><?php echo $_male;?></td>
                           <td><?php echo $_female;?></td>
                           <td><?php echo $_sex_undef;?></td>
                           <td><?php echo $total_sex;?></td>
                        </tr>
                     </table><br>


                     <table style="width:30%;" border="1">
                        <tr>
                           <th>EDUCATION LEVEL</th>
                           <th>MALE</th>
                           <th>FEMALE</th>
                           <th>TOTAL</th>
                        </tr>
                        <tr>
                           <td>ELEMENTARY</td>
                           <td><?php echo $_elemM;?></td>
                           <td><?php echo $_elemF;?></td>
                           <td><?php echo $total_elem;?></td>
                        </tr>
                        <tr>
                           <td>SECONDARY</td>
                           <td><?php echo $_secondM;?></td>
                           <td><?php echo $_secondF;?></td>
                           <td><?php echo $total_second;?></td>
                        </tr>
                        <tr>
                           <td>VOCATIONAL</td>
                           <td><?php echo $_vocM;?></td>
                           <td><?php echo $_vocF;?></td>
                           <td><?php echo $total_voc;?></td>
                        </tr>
                        <tr>
                           <td>COLLEGE</td>
                           <td><?php echo $_collegeM;?></td>
                           <td><?php echo $_collegeF;?></td>
                           <td><?php echo $total_college;?></td>
                        </tr>
                        <tr>
                           <td>COLLEGE GRADUATE</td>
                           <td><?php echo $_college_gradM;?></td>
                           <td><?php echo $_college_gradF;?></td>
                           <td><?php echo $total_college_grad;?></td>
                        </tr>
                        <tr>
                           <td>UNDEFINED</td>
                           <td colspan="2"><?php echo $_educ_undef;?></td>
                           <td><?php echo $_educ_undef;?></td>
                        </tr>
                     </table><br>




                     <table style="width:30%;" border="1">
                        <tr>
                           <th>CIVIL STATUS</th>
                           <th>MALE</th>
                           <th>FEMALE</th>
                           <th>TOTAL</th>
                        </tr>
                        <tr>
                           <td>SINGLE</td>
                           <td><?php echo $_singleM;?></td>
                           <td><?php echo $_singleF;?></td>
                           <td><?php echo $total_si;?></td>
                        </tr>
                        <tr>
                           <td>MARRIED</td>
                           <td><?php echo $_marriedM;?></td>
                           <td><?php echo $_marriedF;?></td>
                           <td><?php echo $total_ma;?></td>
                        </tr>
                        <tr>
                           <td>ANNULLED</td>
                           <td><?php echo $_annulledM;?></td>
                           <td><?php echo $_annulledF;?></td>
                           <td><?php echo $total_an;?></td>
                        </tr>
                        <tr>
                           <td>WIDOWED</td>
                           <td><?php echo $_widowedM;?></td>
                           <td><?php echo $_widowedF;?></td>
                           <td><?php echo $total_wi;?></td>
                        </tr>
                        <tr>
                           <td>SEPARATED</td>
                           <td><?php echo $_separatedM;?></td>
                           <td><?php echo $_separatedF;?></td>
                           <td><?php echo $total_se;?></td>
                        </tr>
                        <tr>
                           <td>OTHERS</td>
                           <td><?php echo $_othersM;?></td>
                           <td><?php echo $_othersF;?></td>
                           <td><?php echo $total_ot;?></td>
                        </tr>
                        <tr>
                           <td>UNDEFINED</td>
                           <td colspan="2"><?php echo $_undef;?></td>
                           <td><?php echo $_undef;?></td>
                        </tr>
                        <tr>
                           <td colspan="3"></td>
                           <td><?php echo $total_civil;?></td>
                        </tr>
                     </table>
         <?php
                  }
                  echo
                  '<div>SEARCH CRITERIA:</div>';
                  if ($searchCriteria == "") {
                     echo "<li>ALL RECORDS</li>";
                  } else {
                     $crit_Arr = explode("|",$searchCriteria);
                     for ($j=0;$j<count($crit_Arr);$j++) {
                        echo "<li>".$crit_Arr[$j]."</li>";
                     }
                  }
            } else {
               echo '<div>NO RECORD QUERIED base on your criteria!!!</div>';
               echo '<div>'.$errmsg.'</div>';
            }
            rptFooter();
         ?>
      </div>
   </body>
</html>