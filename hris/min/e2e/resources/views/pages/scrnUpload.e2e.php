<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once 'constant.e2e.php';
   require_once 'conn.e2e.php';
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
   </head>
   <body>
      <form name="xForm" action="<?php echo $fileAction; ?>"
            method="post"
            enctype="multipart/form-data">
         <?php doHEADER(); ?>
         <div class="container-fluid" id="mainScreen">
         <?php
            $target_dir = path."uploads/";
            $new_target_dir = path."images/EmployeesPhoto/";
            $new_target_dir_olds = path."images/EmployeesPhoto/olds/";
            $uploadOk = 1;
            $msg = "";

            // Check if image file is a actual image or fake image
            if(isset($_POST["submit"]) && $_POST["submit"] == "Upload") {
               
               $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
               $old_filename = basename($_FILES["fileToUpload"]["name"]);
               $imageFileType = pathinfo($target_dir.$old_filename,PATHINFO_EXTENSION);
               $new_filename = getvalue("hUser").".".$imageFileType;
               $target_file = $new_target_dir . $new_filename;
               if($check !== false) {
                  $uploadOk = 1;

                  // Check file size
                  if ($_FILES["fileToUpload"]["size"] > 1000000) {
                      $msg .= "<br>Sorry, your file is too large.";
                      $uploadOk = 0;
                  }
                  // Allow certain file formats
                  if(
                        strtolower($imageFileType) != "jpg" &&
                        strtolower($imageFileType) != "png" &&
                        strtolower($imageFileType) != "jpeg" &&
                        strtolower($imageFileType) != "gif"
                     )
                  {
                      $msg .= "<br>Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                      $uploadOk = 0;
                  }
               }
               else {
                  $msg .= "File is not an image.";
                  $uploadOk = 0;
               }

               // Check if $uploadOk is set to 0 by an error
               if ($uploadOk == 0) {
                   $msg .= "<br>Sorry, your file was not uploaded.";
               // if everything is ok, try to upload file
               } else {
                  if (file_exists($target_file)) {
                     $timestamp = time();
                     $date_today = date("Ymd",$timestamp);
                     $curr_time  = date("his",$timestamp);
                     $source = $target_file;
                     $dest = $new_target_dir_olds.getvalue("hUser")."_".$date_today."_".$curr_time.".".$imageFileType;
                     copy($source, $dest);
                  }
                  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                     $msg .= "<br>The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                     
                     // UPDATE 201 File, Pic 
                     
                     $qry_update = "UPDATE `employees` SET PicFilename = '".$new_filename."' WHERE RefId = ".getvalue("hEmpRefId");
                     if ($conn->query($qry_update) === TRUE) {
                     } else {
                        $msg .= "<br>with error : ".$conn->error;
                     }
                  } else {
                     $msg .= "<br>Sorry, there was an error uploading your file.";
                  }
               }
            }
         ?>
         <?php doTitleBar(getvalue("paramTitle")); ?>
         <div class="container-fluid margin-top10">
            <div class="row">
               <div class="col-xs-12" id="div_CONTENT">
                  <?php
                     if ($msg !== "") {
                        alert("Information",$msg);
                     }
                  ?>
                     <div class="mypanel">
                        <div class="panel-top">Select Image to Upload:</div>
                        <div class="panel-mid-litebg">
                           <div class="border2 padd15 margin-top">
                              <label>PROFILE PICTURE:</label>
                              <input type="file" name="fileToUpload" id="fileToUpload">
                           </div>
                        </div>
                        <div class="panel-bottom">
                           <div class="padd10">
                              <button type="submit"
                                      value="Upload"
                                      class="btn-cls4-sea trnbtn"
                                      name="submit">
                                 <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;
                                 SUBMIT
                              </button>
                              <button type="button"
                                      class="btn-cls4-red trnbtn"
                                      id="btnCLOSE">
                                 <i class="fa fa-times" aria-hidden="true"></i>&nbsp;
                                 CLOSE
                              </button>
                           </div>
                        </div>
                     </div>

               </div>
            </div>
         </div>
         <?php
            modalEmpLkUp();
            footer();
            $mode = "VIEW";
            $ModId = 1001;
            include "varHidden.e2e.php";
         ?>
         </div>
      </form>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Upload"); ?>"></script>
   </body>
</html>







