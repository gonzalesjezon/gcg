<?php
	error_reporting(E_ALL);
   ini_set('display_errors', 1);
	session_start();
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   $user = getvalue("hUser");

   $funcname = getvalue("fn");
   $params   = getvalue("params");
   if (!empty($funcname)) {
      $funcname($params);
   } else {
      echo 'alert("Error... No Function defined");';
   }

   
   function getIPAR() {
   	$refid 	= getvalue("refid");
   	$str 		= "";

   	$ipar_row = FindFirst("spms_ipar","WHERE RefId = '$refid'","*");
   	if ($ipar_row) {
   		echo '$("#sint_Quarter").val("'.$ipar_row["Quarter"].'");'."\n";
   		echo '$("#sint_Year").val("'.$ipar_row["Year"].'");'."\n";
   		echo '$("#sint_AverageNumerical").val("'.$ipar_row["AverageNumerical"].'");'."\n";
   		echo '$("#sint_AverageAdjectival").val("'.$ipar_row["AverageAdjectival"].'");'."\n";
   	} 

   	$sp_rs  	= SelectEach("ipar_strategic_priorities","WHERE spms_ipar_id = '$refid'");
   	if ($sp_rs) {
   		$sp_count = 0;
   		while ($sp_row = mysqli_fetch_assoc($sp_rs)) {
   			$sp_count++;
   			echo '$("#sp_refid_'.$sp_count.'").val("'.$sp_row["RefId"].'");'."\n";
	   		echo '$("#sp_objectives_'.$sp_count.'").val("'.$sp_row["objective"].'");'."\n";
	   		echo '$("#sp_percent_'.$sp_count.'").val("'.$sp_row["percent"].'");'."\n";
	   		echo '$("#sp_si_'.$sp_count.'").val("'.$sp_row["success_indicator"].'");'."\n";
	   		echo '$("#sp_sitype_'.$sp_count.'").val("'.$sp_row["sitype"].'");'."\n";
	   		echo '$("#sp_accomplishment_'.$sp_count.'").val("'.$sp_row["accomplishment"].'");'."\n";
	   		echo '$("#sp_numerical_'.$sp_count.'").val("'.$sp_row["numerical"].'");'."\n";
	   		echo '$("#sp_adjectival_'.$sp_count.'").val("'.$sp_row["adjectival"].'");'."\n";
	   		echo '$("#sp_remarks_'.$sp_count.'").val("'.$sp_row["remarks"].'");'."\n";
   		}
   	}

   	$cf_rs  = SelectEach("ipar_core_functions","WHERE spms_ipar_id = '$refid'");
   	if ($cf_rs) {
   		$cf_count = 0;
   		while ($cf_row = mysqli_fetch_assoc($cf_rs)) {
   			$cf_count++;
   			echo '$("#cf_refid_'.$cf_count.'").val("'.$cf_row["RefId"].'");'."\n";
	   		echo '$("#cf_objectives_'.$cf_count.'").val("'.$cf_row["objective"].'");'."\n";
	   		echo '$("#cf_percent_'.$cf_count.'").val("'.$cf_row["percent"].'");'."\n";
	   		echo '$("#cf_si_'.$cf_count.'").val("'.$cf_row["success_indicator"].'");'."\n";
	   		echo '$("#cf_sitype_'.$cf_count.'").val("'.$cf_row["sitype"].'");'."\n";
	   		echo '$("#cf_accomplishment_'.$cf_count.'").val("'.$cf_row["accomplishment"].'");'."\n";
	   		echo '$("#cf_numerical_'.$cf_count.'").val("'.$cf_row["numerical"].'");'."\n";
	   		echo '$("#cf_adjectival_'.$cf_count.'").val("'.$cf_row["adjectival"].'");'."\n";
	   		echo '$("#cf_remarks_'.$cf_count.'").val("'.$cf_row["remarks"].'");'."\n";
   		}
   	}

   	$sf_rs  = SelectEach("ipar_support_functions","WHERE spms_ipar_id = '$refid'");
   	if ($sf_rs) {
   		$sf_count = 0;
   		while ($sf_row = mysqli_fetch_assoc($sf_rs)) {
   			$sf_count++;
   			echo '$("#sf_refid_'.$sf_count.'").val("'.$sf_row["RefId"].'");'."\n";
	   		echo '$("#sf_objectives_'.$sf_count.'").val("'.$sf_row["objective"].'");'."\n";
	   		echo '$("#sf_percent_'.$sf_count.'").val("'.$sf_row["percent"].'");'."\n";
	   		echo '$("#sf_si_'.$sf_count.'").val("'.$sf_row["success_indicator"].'");'."\n";
	   		echo '$("#sf_sitype_'.$sf_count.'").val("'.$sf_row["sitype"].'");'."\n";
	   		echo '$("#sf_accomplishment_'.$sf_count.'").val("'.$sf_row["accomplishment"].'");'."\n";
	   		echo '$("#sf_numerical_'.$sf_count.'").val("'.$sf_row["numerical"].'");'."\n";
	   		echo '$("#sf_adjectival_'.$sf_count.'").val("'.$sf_row["adjectival"].'");'."\n";
	   		echo '$("#sf_remarks_'.$sf_count.'").val("'.$sf_row["remarks"].'");'."\n";
   		}
   	}
   }
   function saveIPAR(){
   	$emprefid 	= getvalue("sint_EmployeesRefId");
   	$AgencyId 	= FindFirst("employees","WHERE RefId = '$emprefid'","AgencyId");
   	$count_sp 	= getvalue("count_sp");
   	$count_cf 	= getvalue("count_cf");
   	$count_sf 	= getvalue("count_sf");
   	$quarter  	= getvalue("sint_Quarter");
   	$year 	 	= getvalue("sint_Year");
   	$AverageNumerical 	 	= getvalue("sint_AverageNumerical");
   	$AverageAdjectival 	 	= getvalue("sint_AverageAdjectival");
   	$error 	 	= 0;
   	$ipar_refid = getvalue("sint_IPARRefId");

   	if (intval($ipar_refid) == 0) {
   		$ipar_fld = "";
	   	$ipar_val = "";
	   	$ipar_fld .= "`EmployeesRefId`, `AgencyId`, `Quarter`, `Year`, `AverageNumerical`, `AverageAdjectival`,";
	   	$ipar_val .= "'$emprefid', '$AgencyId', '$quarter', '$year', '$AverageNumerical', '$AverageAdjectival',";
	   	$spms_ipar_id = f_SaveRecord("NEWSAVE","spms_ipar",$ipar_fld,$ipar_val);
	   	if (is_numeric($spms_ipar_id)) {
	   		for ($a=1; $a <= $count_sp; $a++) { 
		   		$objective = realEscape($_POST["sp_objectives_".$a]);
		   		if ($objective != "") {
		   			$ipar_sp_fld 	= "";
		   			$ipar_sp_val 	= "";
		   			$sp_objectives = realEscape($_POST["sp_objectives_".$a]);
					   $sp_percent 	= realEscape($_POST["sp_percent_".$a]);
					   $sp_si 			= realEscape($_POST["sp_si_".$a]);
					   $sp_sitype 		= realEscape($_POST["sp_sitype_".$a]);
					   $sp_accomplishment 	= realEscape($_POST["sp_accomplishment_".$a]);
					   $sp_numerical 			= realEscape($_POST["sp_numerical_".$a]);
					   $sp_adjectival 		= realEscape($_POST["sp_adjectival_".$a]);
					   $sp_remarks 	= realEscape($_POST["sp_remarks_".$a]);

					   $ipar_sp_fld   .= "`spms_ipar_id`, `objective`, `percent`, `success_indicator`, ";
					   $ipar_sp_fld 	.= "`sitype`, `accomplishment`, `remarks`, `adjectival` , `numerical`,";
					   $ipar_sp_val	.= "'$spms_ipar_id', '$sp_objectives', '$sp_percent', '$sp_si', ";
					   $ipar_sp_val   .= "'$sp_sitype', '$sp_accomplishment', '$sp_remarks', '$sp_adjectival', '$sp_numerical',";
					   $save_sp = f_SaveRecord("NEWSAVE","ipar_strategic_priorities",$ipar_sp_fld,$ipar_sp_val);
					   if (!is_numeric($save_sp)) {
					   	$error++;
					   }
		   		}
		   	}


		   	for ($b=1; $b <= $count_cf; $b++) { 
		   		$objective = realEscape($_POST["cf_objectives_".$b]);
		   		if ($objective != "") {
		   			$ipar_cf_fld 	= "";
		   			$ipar_cf_val 	= "";
		   			$cf_objectives = realEscape($_POST["cf_objectives_".$b]);
					   $cf_percent 	= realEscape($_POST["cf_percent_".$b]);
					   $cf_si 			= realEscape($_POST["cf_si_".$b]);
					   $cf_sitype 		= realEscape($_POST["cf_sitype_".$b]);
					   $cf_accomplishment 		= realEscape($_POST["cf_accomplishment_".$b]);
					   $cf_adjectival 		= realEscape($_POST["cf_adjectival_".$b]);
					   $cf_numerical 		= realEscape($_POST["cf_numerical_".$b]);
					   $cf_remarks 	= realEscape($_POST["cf_remarks_".$b]);

					   $ipar_cf_fld   = "`spms_ipar_id`, `objective`, `percent`, `success_indicator`, ";
					   $ipar_cf_fld 	.= "`sitype`, `accomplishment`, `remarks`, `adjectival`, `numerical`,";
					   $ipar_cf_val	.= "'$spms_ipar_id', '$cf_objectives', '$cf_percent', '$cf_si', ";
					   $ipar_cf_val   .= "'$cf_sitype', '$cf_accomplishment', '$cf_remarks', '$cf_adjectival', '$cf_numerical',";
					   $save_cf = f_SaveRecord("NEWSAVE","ipar_core_functions",$ipar_cf_fld,$ipar_cf_val);
					   if (!is_numeric($save_cf)) {
					   	$error++;
					   }
		   		}
		   	}


		   	for ($c=1; $c <= $count_sf; $c++) { 
		   		$objective = realEscape($_POST["sf_objectives_".$c]);
		   		if ($objective != "") {
		   			$ipar_sf_fld 	= "";
		   			$ipar_sf_val 	= "";
		   			$sf_objectives = realEscape($_POST["sf_objectives_".$c]);
					   $sf_percent 	= realEscape($_POST["sf_percent_".$c]);
					   $sf_si 			= realEscape($_POST["sf_si_".$c]);
					   $sf_sitype 		= realEscape($_POST["sf_sitype_".$c]);
					   $sf_accomplishment 	= realEscape($_POST["sf_accomplishment_".$c]);
					   $sf_adjectival 		= realEscape($_POST["sf_adjectival_".$c]);
					   $sf_numerical 			= realEscape($_POST["sf_numerical_".$c]);
					   $sf_remarks 	= realEscape($_POST["sf_remarks_".$c]);

					   $ipar_sf_fld   = "`spms_ipar_id`, `objective`, `percent`, `success_indicator`, ";
					   $ipar_sf_fld 	.= "`sitype`, `accomplishment`, `remarks`, `adjectival`, `numerical`,";
					   $ipar_sf_val	.= "'$spms_ipar_id', '$sf_objectives', '$sf_percent', '$sf_si', ";
					   $ipar_sf_val   .= "'$sf_sitype', '$sf_accomplishment', '$sf_remarks', '$sf_adjectival', '$sf_numerical',";
					   $save_sf = f_SaveRecord("NEWSAVE","ipar_support_functions",$ipar_sf_fld,$ipar_sf_val);
					   if (!is_numeric($save_sf)) {
					   	$error++;
					   }
		   		}
		   	}	
	   	} else {
		   	$error++;
		   }
		   if ($error == 0) {
		   	echo 'afterSave();';
		   } else {
		   	echo "Error";
		   }	
   	} else {
   		$ipar_fldnval 	= "Quarter = '$quarter', Year = '$year',";
   		$ipar_fldnval 	.= "AverageAdjectival = '$AverageAdjectival', AverageNumerical = '$AverageNumerical',";
   		$update_ipar 	= f_SaveRecord("EDITSAVE","spms_ipar",$ipar_fldnval,$ipar_refid);
   		if ($update_ipar == "") {
   			for ($a=1; $a <= $count_sp; $a++) { 
   				$sp_refid = realEscape($_POST["sp_refid_".$a]);
   				if (intval($sp_refid) > 0) {
   					$ipar_sp_fldnval 		= "";
   					$sp_objectives 		= realEscape($_POST["sp_objectives_".$a]);
					   $sp_percent 			= realEscape($_POST["sp_percent_".$a]);
					   $sp_si 					= realEscape($_POST["sp_si_".$a]);
					   $sp_sitype 				= realEscape($_POST["sp_sitype_".$a]);
					   $sp_accomplishment 	= realEscape($_POST["sp_accomplishment_".$a]);
					   $sp_adjectival 		= realEscape($_POST["sp_adjectival_".$a]);
					   $sp_numerical 			= realEscape($_POST["sp_numerical_".$a]);
					   $sp_remarks 			= realEscape($_POST["sp_remarks_".$a]);
					   $ipar_sp_fldnval  	.= "`objective` = '$sp_objectives', `percent` = '$sp_percent', ";
					   $ipar_sp_fldnval		.= "`success_indicator` = '$sp_si', ";
					   $ipar_sp_fldnval		.= "`sitype` = '$sp_sitype', `accomplishment` = '$sp_accomplishment', ";
					   $ipar_sp_fldnval		.= "`remarks` = '$sp_remarks', `adjectival` = '$sp_adjectival',";
					   $ipar_sp_fldnval 		.= "`numerical` = '$sp_numerical', ";
					   $update_sp 	= f_SaveRecord("EDITSAVE","ipar_strategic_priorities",$ipar_sp_fldnval,$sp_refid);
					   if ($update_sp != "") {
					   	$error++;
					   }
   				}
   			}


   			for ($b=1; $b <= $count_cf; $b++) { 
   				$cf_refid = realEscape($_POST["cf_refid_".$b]);
   				if (intval($cf_refid) > 0) {
   					$ipar_cf_fldnval 		= "";
   					$cf_objectives 		= realEscape($_POST["cf_objectives_".$b]);
					   $cf_percent 			= realEscape($_POST["cf_percent_".$b]);
					   $cf_si 					= realEscape($_POST["cf_si_".$b]);
					   $cf_sitype 				= realEscape($_POST["cf_sitype_".$b]);
					   $sp_accomplishment 	= realEscape($_POST["sp_accomplishment_".$a]);
					   $sp_adjectival 		= realEscape($_POST["sp_adjectival_".$a]);
					   $sp_numerical 			= realEscape($_POST["sp_numerical_".$a]);
					   $cf_remarks 			= realEscape($_POST["cf_remarks_".$b]);
					   $ipar_cf_fldnval  	.= "`objective` = '$cf_objectives', `percent` = '$cf_percent', ";
					   $ipar_cf_fldnval		.= "`success_indicator` = '$cf_si', ";
					   $ipar_cf_fldnval		.= "`sitype` = '$cf_sitype', `accomplishment` = '$cf_accomplishment', ";
					   $ipar_cf_fldnval		.= "`remarks` = '$cf_remarks', `adjectival` = '$cf_adjectival',";
					   $ipar_cf_fldnval 		.= "`numerical` = '$cf_numerical', ";
					   $update_cf 	= f_SaveRecord("EDITSAVE","ipar_core_functions",$ipar_cf_fldnval,$cf_refid);
					   if ($update_cf != "") {
					   	$error++;
					   }
   				}   				
   			}


   			for ($c=1; $c <= $count_sf; $c++) { 
   				$sf_refid = realEscape($_POST["sf_refid_".$c]);
   				if (intval($sf_refid) > 0) {
   					$ipar_sf_fldnval 		= "";
   					$sf_objectives 		= realEscape($_POST["sf_objectives_".$b]);
					   $sf_percent 			= realEscape($_POST["sf_percent_".$b]);
					   $sf_si 					= realEscape($_POST["sf_si_".$b]);
					   $sf_sitype 				= realEscape($_POST["sf_sitype_".$b]);
					   $sp_accomplishment 	= realEscape($_POST["sp_accomplishment_".$a]);
					   $sp_adjectival 		= realEscape($_POST["sp_adjectival_".$a]);
					   $sp_numerical 			= realEscape($_POST["sp_numerical_".$a]);
					   $sf_remarks 			= realEscape($_POST["sf_remarks_".$b]);
					   $ipar_sf_fldnval  	.= "`objective` = '$sf_objectives', `percent` = '$sf_percent', ";
					   $ipar_sf_fldnval		.= "`success_indicator` = '$sf_si', ";
					   $ipar_sf_fldnval		.= "`sitype` = '$sf_sitype', `accomplishment` = '$sf_accomplishment', ";
					   $ipar_sf_fldnval		.= "`remarks` = '$sf_remarks', `adjectival` = '$sf_adjectival',";
					   $ipar_sf_fldnval 		.= "`numerical` = '$sf_numerical', ";
					   $update_sf 	= f_SaveRecord("EDITSAVE","ipar_strategic_priorities",$ipar_sf_fldnval,$sf_refid);
					   if ($update_sf != "") {
					   	$error++;
					   }
   				}
   			}
   		} else {
   			$error++;
   		}
   		if ($error == 0) {
		   	echo 'afterEdit();';
		   } else {
		   	echo "Error";
		   }
   	}   	
   }




   function getIPCR() {
   	$refid 	= getvalue("refid");
   	$str 		= "";

   	$ipcr_row = FindFirst("spms_ipcr","WHERE RefId = '$refid'","*");
   	if ($ipcr_row) {
   		echo '$("#sint_Quarter").val("'.$ipcr_row["Quarter"].'");'."\n";
   		echo '$("#sint_Year").val("'.$ipcr_row["Year"].'");'."\n";
   	} 

   	$sp_rs  	= SelectEach("ipcr_strategic_priorities","WHERE spms_ipcr_id = '$refid'");
   	if ($sp_rs) {
   		$sp_count = 0;
   		while ($sp_row = mysqli_fetch_assoc($sp_rs)) {
   			$sp_count++;
   			echo '$("#sp_refid_'.$sp_count.'").val("'.$sp_row["RefId"].'");'."\n";
	   		echo '$("#sp_objectives_'.$sp_count.'").val("'.$sp_row["objective"].'");'."\n";
	   		echo '$("#sp_percent_'.$sp_count.'").val("'.$sp_row["percent"].'");'."\n";
	   		echo '$("#sp_si_'.$sp_count.'").val("'.$sp_row["success_indicator"].'");'."\n";
	   		echo '$("#sp_sitype_'.$sp_count.'").val("'.$sp_row["sitype"].'");'."\n";
	   		echo '$("#sp_rating_'.$sp_count.'").val("'.$sp_row["rating"].'");'."\n";
	   		echo '$("#sp_remarks_'.$sp_count.'").val("'.$sp_row["remarks"].'");'."\n";
	   		echo '$("#sp_q1_'.$sp_count.'").val("'.$sp_row["q1"].'");'."\n";
	   		echo '$("#sp_q2_'.$sp_count.'").val("'.$sp_row["q2"].'");'."\n";
	   		echo '$("#sp_q3_'.$sp_count.'").val("'.$sp_row["q3"].'");'."\n";
	   		echo '$("#sp_q4_'.$sp_count.'").val("'.$sp_row["q4"].'");'."\n";

	   		if ($sp_row["q1"] == 1) {
	   			echo '$("#sp_q1_'.$sp_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($sp_row["q2"] == 1) {
	   			echo '$("#sp_q2_'.$sp_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($sp_row["q3"] == 1) {
	   			echo '$("#sp_q3_'.$sp_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($sp_row["q4"] == 1) {
	   			echo '$("#sp_q4_'.$sp_count.'").prop("checked",true);'."\n";	
	   		}
   		}
   	}

   	$cf_rs  = SelectEach("ipcr_core_functions","WHERE spms_ipcr_id = '$refid'");
   	if ($cf_rs) {
   		$cf_count = 0;
   		while ($cf_row = mysqli_fetch_assoc($cf_rs)) {
   			$cf_count++;
   			echo '$("#cf_refid_'.$cf_count.'").val("'.$cf_row["RefId"].'");'."\n";
	   		echo '$("#cf_objectives_'.$cf_count.'").val("'.$cf_row["objective"].'");'."\n";
	   		echo '$("#cf_percent_'.$cf_count.'").val("'.$cf_row["percent"].'");'."\n";
	   		echo '$("#cf_si_'.$cf_count.'").val("'.$cf_row["success_indicator"].'");'."\n";
	   		echo '$("#cf_sitype_'.$cf_count.'").val("'.$cf_row["sitype"].'");'."\n";
	   		echo '$("#cf_rating_'.$cf_count.'").val("'.$cf_row["rating"].'");'."\n";
	   		echo '$("#cf_remarks_'.$cf_count.'").val("'.$cf_row["remarks"].'");'."\n";
	   		echo '$("#cf_q1_'.$cf_count.'").val("'.$cf_row["q1"].'");'."\n";
	   		echo '$("#cf_q2_'.$cf_count.'").val("'.$cf_row["q2"].'");'."\n";
	   		echo '$("#cf_q3_'.$cf_count.'").val("'.$cf_row["q3"].'");'."\n";
	   		echo '$("#cf_q4_'.$cf_count.'").val("'.$cf_row["q4"].'");'."\n";

	   		if ($cf_row["q1"] == 1) {
	   			echo '$("#cf_q1_'.$cf_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($cf_row["q2"] == 1) {
	   			echo '$("#cf_q2_'.$cf_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($cf_row["q3"] == 1) {
	   			echo '$("#cf_q3_'.$cf_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($cf_row["q4"] == 1) {
	   			echo '$("#cf_q4_'.$cf_count.'").prop("checked",true);'."\n";	
	   		}
   		}
   	}

   	$sf_rs  = SelectEach("ipcr_support_functions","WHERE spms_ipcr_id = '$refid'");
   	if ($sf_rs) {
   		$sf_count = 0;
   		while ($sf_row = mysqli_fetch_assoc($sf_rs)) {
   			$sf_count++;
   			echo '$("#sf_refid_'.$sf_count.'").val("'.$sf_row["RefId"].'");'."\n";
	   		echo '$("#sf_objectives_'.$sf_count.'").val("'.$sf_row["objective"].'");'."\n";
	   		echo '$("#sf_percent_'.$sf_count.'").val("'.$sf_row["percent"].'");'."\n";
	   		echo '$("#sf_si_'.$sf_count.'").val("'.$sf_row["success_indicator"].'");'."\n";
	   		echo '$("#sf_sitype_'.$sf_count.'").val("'.$sf_row["sitype"].'");'."\n";
	   		echo '$("#sf_rating_'.$sf_count.'").val("'.$sf_row["rating"].'");'."\n";
	   		echo '$("#sf_remarks_'.$sf_count.'").val("'.$sf_row["remarks"].'");'."\n";
	   		echo '$("#sf_q1_'.$sf_count.'").val("'.$sf_row["q1"].'");'."\n";
	   		echo '$("#sf_q2_'.$sf_count.'").val("'.$sf_row["q2"].'");'."\n";
	   		echo '$("#sf_q3_'.$sf_count.'").val("'.$sf_row["q3"].'");'."\n";
	   		echo '$("#sf_q4_'.$sf_count.'").val("'.$sf_row["q4"].'");'."\n";

	   		if ($sf_row["q1"] == 1) {
	   			echo '$("#sf_q1_'.$sf_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($sf_row["q2"] == 1) {
	   			echo '$("#sf_q2_'.$sf_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($sf_row["q3"] == 1) {
	   			echo '$("#sf_q3_'.$sf_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($sf_row["q4"] == 1) {
	   			echo '$("#sf_q4_'.$sf_count.'").prop("checked",true);'."\n";	
	   		}
	   		
   		}
   	}
   }
   function saveIPCR(){
   	$emprefid 	= getvalue("sint_EmployeesRefId");
   	$AgencyId 	= FindFirst("employees","WHERE RefId = '$emprefid'","AgencyId");
   	$count_sp 	= getvalue("count_sp");
   	$count_cf 	= getvalue("count_cf");
   	$count_sf 	= getvalue("count_sf");
   	$quarter  	= getvalue("sint_Quarter");
   	$year 	 	= getvalue("sint_Year");
   	$error 	 	= 0;
   	$ipcr_refid = getvalue("sint_IPCRRefId");

   	if (intval($ipcr_refid) == 0) {
   		$ipcr_fld = "";
	   	$ipcr_val = "";
	   	$ipcr_fld .= "`EmployeesRefId`, `AgencyId`, `Quarter`, `Year`,";
	   	$ipcr_val .= "'$emprefid', '$AgencyId', '$quarter', '$year', ";
	   	$spms_ipcr_id = f_SaveRecord("NEWSAVE","spms_ipcr",$ipcr_fld,$ipcr_val);
	   	if (is_numeric($spms_ipcr_id)) {
	   		for ($a=1; $a <= $count_sp; $a++) { 
		   		$objective = realEscape($_POST["sp_objectives_".$a]);
		   		if ($objective != "") {
		   			$ipcr_sp_fld 	= "";
		   			$ipcr_sp_val 	= "";
		   			$sp_objectives = realEscape($_POST["sp_objectives_".$a]);
					   $sp_percent 	= realEscape($_POST["sp_percent_".$a]);
					   $sp_si 			= realEscape($_POST["sp_si_".$a]);
					   $sp_sitype 		= realEscape($_POST["sp_sitype_".$a]);
					   $sp_rating 		= realEscape($_POST["sp_rating_".$a]);
					   $sp_remarks 	= realEscape($_POST["sp_remarks_".$a]);

					   $ipcr_sp_fld   .= "`spms_ipcr_id`, `objective`, `percent`, `success_indicator`, `sitype`, `rating`, `remarks`, ";
					   $ipcr_sp_val	.= "'$spms_ipcr_id', '$sp_objectives', '$sp_percent', '$sp_si', ";
					   $ipcr_sp_val   .= "'$sp_sitype', '$sp_rating', '$sp_remarks',";
					   if (isset($_POST["sp_q1_".$a])) {
					   	$sp_q1 = realEscape($_POST["sp_q1_".$a]);
					   	$ipcr_sp_fld .= "`q1`,";
					   	$ipcr_sp_val .= "'$sp_q1',";
					   }
					   if (isset($_POST["sp_q2_".$a])) {
					   	$sp_q2 = realEscape($_POST["sp_q2_".$a]);
					   	$ipcr_sp_fld .= "`q2`,";
					   	$ipcr_sp_val .= "'$sp_q2',";
					   }
					   if (isset($_POST["sp_q3_".$a])) {
					   	$sp_q3 = realEscape($_POST["sp_q3_".$a]);
					   	$ipcr_sp_fld .= "`q3`,";
					   	$ipcr_sp_val .= "'$sp_q3',";
					   }
					   if (isset($_POST["sp_q4_".$a])) {
					   	$sp_q4 = realEscape($_POST["sp_q4_".$a]);
					   	$ipcr_sp_fld .= "`q4`,";
					   	$ipcr_sp_val .= "'$sp_q4',";
					   }
					   $save_sp = f_SaveRecord("NEWSAVE","ipcr_strategic_priorities",$ipcr_sp_fld,$ipcr_sp_val);
					   if (!is_numeric($save_sp)) {
					   	$error++;
					   }
		   		}
		   	}


		   	for ($b=1; $b <= $count_cf; $b++) { 
		   		$objective = realEscape($_POST["cf_objectives_".$b]);
		   		if ($objective != "") {
		   			$ipcr_cf_fld 	= "";
		   			$ipcr_cf_val 	= "";
		   			$cf_objectives = realEscape($_POST["cf_objectives_".$b]);
					   $cf_percent 	= realEscape($_POST["cf_percent_".$b]);
					   $cf_si 			= realEscape($_POST["cf_si_".$b]);
					   $cf_sitype 		= realEscape($_POST["cf_sitype_".$b]);
					   $cf_rating 		= realEscape($_POST["cf_rating_".$b]);
					   $cf_remarks 	= realEscape($_POST["cf_remarks_".$b]);

					   $ipcr_cf_fld   = "`spms_ipcr_id`, `objective`, `percent`, `success_indicator`, `sitype`, `rating`, `remarks`, ";
					   $ipcr_cf_val	.= "'$spms_ipcr_id', '$cf_objectives', '$cf_percent', '$cf_si', ";
					   $ipcr_cf_val   .= "'$cf_sitype', '$cf_rating', '$cf_remarks',";
					   if (isset($_POST["cf_q1_".$b])) {
					   	$cf_q1 = realEscape($_POST["cf_q1_".$b]);
					   	$ipcr_cf_fld .= "`q1`,";
					   	$ipcr_cf_val .= "'$cf_q1',";
					   }
					   if (isset($_POST["cf_q2_".$b])) {
					   	$cf_q2 = realEscape($_POST["cf_q2_".$b]);
					   	$ipcr_cf_fld .= "`q2`,";
					   	$ipcr_cf_val .= "'$cf_q2',";
					   }
					   if (isset($_POST["cf_q3_".$b])) {
					   	$cf_q3 = realEscape($_POST["cf_q3_".$b]);
					   	$ipcr_cf_fld .= "`q3`,";
					   	$ipcr_cf_val .= "'$cf_q3',";
					   }
					   if (isset($_POST["cf_q4_".$b])) {
					   	$cf_q4 = realEscape($_POST["cf_q4_".$b]);
					   	$ipcr_cf_fld .= "`q4`,";
					   	$ipcr_cf_val .= "'$cf_q4',";
					   }
					   $save_cf = f_SaveRecord("NEWSAVE","ipcr_core_functions",$ipcr_cf_fld,$ipcr_cf_val);
					   if (!is_numeric($save_cf)) {
					   	$error++;
					   }
		   		}
		   	}


		   	for ($c=1; $c <= $count_sf; $c++) { 
		   		$objective = realEscape($_POST["sf_objectives_".$c]);
		   		if ($objective != "") {
		   			$ipcr_sf_fld 	= "";
		   			$ipcr_sf_val 	= "";
		   			$sf_objectives = realEscape($_POST["sf_objectives_".$c]);
					   $sf_percent 	= realEscape($_POST["sf_percent_".$c]);
					   $sf_si 			= realEscape($_POST["sf_si_".$c]);
					   $sf_sitype 		= realEscape($_POST["sf_sitype_".$c]);
					   $sf_rating 		= realEscape($_POST["sf_rating_".$c]);
					   $sf_remarks 	= realEscape($_POST["sf_remarks_".$c]);

					   $ipcr_sf_fld   = "`spms_ipcr_id`, `objective`, `percent`, `success_indicator`, `sitype`, `rating`, `remarks`, ";
					   $ipcr_sf_val	.= "'$spms_ipcr_id', '$sf_objectives', '$sf_percent', '$sf_si', ";
					   $ipcr_sf_val   .= "'$sf_sitype', '$sf_rating', '$sf_remarks',";
					   if (isset($_POST["sf_q1_".$c])) {
					   	$sf_q1 = realEscape($_POST["sf_q1_".$c]);
					   	$ipcr_sf_fld .= "`q1`,";
					   	$ipcr_sf_val .= "'$sf_q1',";
					   }
					   if (isset($_POST["sf_q2_".$c])) {
					   	$sf_q2 = realEscape($_POST["sf_q2_".$c]);
					   	$ipcr_sf_fld .= "`q2`,";
					   	$ipcr_sf_val .= "'$sf_q2',";
					   }
					   if (isset($_POST["sf_q3_".$c])) {
					   	$sf_q3 = realEscape($_POST["sf_q3_".$c]);
					   	$ipcr_sf_fld .= "`q3`,";
					   	$ipcr_sf_val .= "'$sf_q3',";
					   }
					   if (isset($_POST["sf_q4_".$c])) {
					   	$sf_q4 = realEscape($_POST["sf_q4_".$c]);
					   	$ipcr_sf_fld .= "`q4`,";
					   	$ipcr_sf_val .= "'$sf_q4',";
					   }
					   $save_sf = f_SaveRecord("NEWSAVE","ipcr_support_functions",$ipcr_sf_fld,$ipcr_sf_val);
					   if (!is_numeric($save_sf)) {
					   	$error++;
					   }
		   		}
		   	}	
	   	} else {
		   	$error++;
		}
	   	if ($error == 0) {
	   		echo 'afterSave();';
	   	} else {
	   		echo "Error";
	   	}	
   	} else {
   		$ipcr_fldnval 	= "Quarter = '$quarter', Year = '$year',";
   		$update_ipcr 	= f_SaveRecord("EDITSAVE","spms_ipcr",$ipcr_fldnval,$ipcr_refid);
   		if ($update_ipcr == "") {
   			for ($a=1; $a <= $count_sp; $a++) { 
   				$sp_refid = realEscape($_POST["sp_refid_".$a]);
   				if (intval($sp_refid) > 0) {
   					$ipcr_sp_fldnval = "";
   					$sp_objectives = realEscape($_POST["sp_objectives_".$a]);
					   $sp_percent 	= realEscape($_POST["sp_percent_".$a]);
					   $sp_si 			= realEscape($_POST["sp_si_".$a]);
					   $sp_sitype 		= realEscape($_POST["sp_sitype_".$a]);
					   $sp_rating 		= realEscape($_POST["sp_rating_".$a]);
					   $sp_remarks 	= realEscape($_POST["sp_remarks_".$a]);
					   $ipcr_sp_fldnval  .= "`objective` = '$sp_objectives', `percent` = '$sp_percent', ";
					   $ipcr_sp_fldnval	.= "`success_indicator` = '$sp_si', ";
					   $ipcr_sp_fldnval	.= "`sitype` = '$sp_sitype', `rating` = '$sp_rating', `remarks` = '$sp_remarks', ";
					   if (isset($_POST["sp_q1_".$a])) {
					   	$sp_q1 = realEscape($_POST["sp_q1_".$a]);
					   	$ipcr_sp_fldnval .= "`q1` = '$sp_q1',";
					   } else {
					   	$ipcr_sp_fldnval .= "`q1` = '0',";
					   }
					   if (isset($_POST["sp_q2_".$a])) {
					   	$sp_q2 = realEscape($_POST["sp_q2_".$a]);
					   	$ipcr_sp_fldnval .= "`q2` = '$sp_q2',";
					   } else {
					   	$ipcr_sp_fldnval .= "`q2` = '0',";
					   }
					   if (isset($_POST["sp_q3_".$a])) {
					   	$sp_q3 = realEscape($_POST["sp_q3_".$a]);
					   	$ipcr_sp_fldnval .= "`q3` = '$sp_q3',";
					   } else {
					   	$ipcr_sp_fldnval .= "`q3` = '0',";
					   }
					   if (isset($_POST["sp_q4_".$a])) {
					   	$sp_q4 = realEscape($_POST["sp_q4_".$a]);
					   	$ipcr_sp_fldnval .= "`q4` = '$sp_q4',";
					   } else {
					   	$ipcr_sp_fldnval .= "`q4` = '0',";
					   }
					   $update_sp 	= f_SaveRecord("EDITSAVE","ipcr_strategic_priorities",$ipcr_sp_fldnval,$sp_refid);
					   if ($update_sp != "") {
					   	$error++;
					   }
   				}
   			}


   			for ($b=1; $b <= $count_cf; $b++) { 
   				$cf_refid = realEscape($_POST["cf_refid_".$b]);
   				if (intval($cf_refid) > 0) {
   					$cf_objectives = realEscape($_POST["cf_objectives_".$b]);
					   $cf_percent 	= realEscape($_POST["cf_percent_".$b]);
					   $cf_si 			= realEscape($_POST["cf_si_".$b]);
					   $cf_sitype 		= realEscape($_POST["cf_sitype_".$b]);
					   $cf_rating 		= realEscape($_POST["cf_rating_".$b]);
					   $cf_remarks 	= realEscape($_POST["cf_remarks_".$b]);
					   $ipcr_cf_fldnval   = "`objective` = '$cf_objectives', `percent` = '$cf_percent', ";
					   $ipcr_cf_fldnval	.= "`success_indicator` = '$cf_si', ";
					   $ipcr_cf_fldnval	.= "`sitype` = '$cf_sitype', `rating` = '$cf_rating', `remarks` = '$cf_remarks', ";
					   if (isset($_POST["cf_q1_".$b])) {
					   	$cf_q1 = realEscape($_POST["cf_q1_".$b]);
					   	$ipcr_cf_fldnval .= "`q1` = '$cf_q1',";
					   } else {
					   	$ipcr_cf_fldnval .= "`q1` = '0',";
					   }
					   if (isset($_POST["cf_q2_".$b])) {
					   	$cf_q2 = realEscape($_POST["cf_q2_".$b]);
					   	$ipcr_cf_fldnval .= "`q2` = '$cf_q2',";
					   } else {
					   	$ipcr_cf_fldnval .= "`q2` = '0',";
					   }
					   if (isset($_POST["cf_q3_".$b])) {
					   	$cf_q3 = realEscape($_POST["cf_q3_".$b]);
					   	$ipcr_cf_fldnval .= "`q3` = '$cf_q3',";
					   } else {
					   	$ipcr_cf_fldnval .= "`q3` = '0',";
					   }
					   if (isset($_POST["cf_q4_".$b])) {
					   	$cf_q4 = realEscape($_POST["cf_q4_".$b]);
					   	$ipcr_cf_fldnval .= "`q4` = '$cf_q4',";
					   } else {
					   	$ipcr_cf_fldnval .= "`q4` = '0',";
					   }
					   $update_cf 	= f_SaveRecord("EDITSAVE","ipcr_core_functions",$ipcr_cf_fldnval,$cf_refid);
					   if ($update_cf != "") {
					   	$error++;
					   }
   				}   				
   			}


   			for ($c=1; $c <= $count_sf; $c++) { 
   				$sf_refid = realEscape($_POST["sf_refid_".$c]);
   				if (intval($sf_refid) > 0) {
   					$sf_objectives = realEscape($_POST["sf_objectives_".$c]);
					   $sf_percent 	= realEscape($_POST["sf_percent_".$c]);
					   $sf_si 			= realEscape($_POST["sf_si_".$c]);
					   $sf_sitype 		= realEscape($_POST["sf_sitype_".$c]);
					   $sf_rating 		= realEscape($_POST["sf_rating_".$c]);
					   $sf_remarks 	= realEscape($_POST["sf_remarks_".$c]);
					   $ipcr_sf_fldnval   = "`objective` = '$sf_objectives', `percent` = '$sf_percent', ";
					   $ipcr_sf_fldnval	.= "`success_indicator` = '$sf_si', ";
					   $ipcr_sf_fldnval	.= "`sitype` = '$sf_sitype', `rating` = '$sf_rating', `remarks` = '$sf_remarks', ";
					   if (isset($_POST["sf_q1_".$c])) {
					   	$sf_q1 = realEscape($_POST["sf_q1_".$c]);
					   	$ipcr_sf_fldnval .= "`q1` = '$sf_q1',";
					   } else {
					   	$ipcr_sf_fldnval .= "`q1` = '0',";
					   }
					   if (isset($_POST["sf_q2_".$c])) {
					   	$sf_q2 = realEscape($_POST["sf_q2_".$c]);
					   	$ipcr_sf_fldnval .= "`q2` = '$sf_q2',";
					   } else {
					   	$ipcr_sf_fldnval .= "`q2` = '0',";
					   }
					   if (isset($_POST["sf_q3_".$c])) {
					   	$sf_q3 = realEscape($_POST["sf_q3_".$c]);
					   	$ipcr_sf_fldnval .= "`q3` = '$sf_q3',";
					   } else {
					   	$ipcr_sf_fldnval .= "`q3` = '0',";
					   }
					   if (isset($_POST["sf_q4_".$c])) {
					   	$sf_q4 = realEscape($_POST["sf_q4_".$c]);
					   	$ipcr_sf_fldnval .= "`q4` = '$sf_q4',";
					   } else {
					   	$ipcr_sf_fldnval .= "`q4` = '0',";
					   }
					   $update_sf 	= f_SaveRecord("EDITSAVE","ipcr_strategic_priorities",$ipcr_sf_fldnval,$sf_refid);
					   if ($update_sf != "") {
					   	$error++;
					   }
   				}
   			}
   		} else {
   			$error++;
   		}
   		if ($error == 0) {
	   		echo 'afterEdit();';
	   	} else {
	   		echo "Error";
	   	}
   	}   	
   }

   function getDPCR() {
   	$refid 	= getvalue("refid");
   	$str 		= "";

   	$ipcr_row = FindFirst("spms_dpcr","WHERE RefId = '$refid'","*");
   	if ($ipcr_row) {
   		echo '$("#sint_Quarter").val("'.$ipcr_row["Quarter"].'");'."\n";
   		echo '$("#sint_Year").val("'.$ipcr_row["Year"].'");'."\n";
   	} 

   	$sp_rs  	= SelectEach("dpcr_strategic_priorities","WHERE spms_dpcr_id = '$refid'");
   	if ($sp_rs) {
   		$sp_count = 0;
   		while ($sp_row = mysqli_fetch_assoc($sp_rs)) {
   			$sp_count++;
   			echo '$("#sp_refid_'.$sp_count.'").val("'.$sp_row["RefId"].'");'."\n";
	   		echo '$("#sp_objectives_'.$sp_count.'").val("'.$sp_row["objective"].'");'."\n";
	   		echo '$("#sp_percent_'.$sp_count.'").val("'.$sp_row["percent"].'");'."\n";
	   		echo '$("#sp_si_'.$sp_count.'").val("'.$sp_row["success_indicator"].'");'."\n";
	   		echo '$("#sp_sitype_'.$sp_count.'").val("'.$sp_row["sitype"].'");'."\n";
	   		echo '$("#sp_rating_'.$sp_count.'").val("'.$sp_row["rating"].'");'."\n";
	   		echo '$("#sp_budget_'.$sp_count.'").val("'.$sp_row["budget"].'");'."\n";
	   		echo '$("#sp_remarks_'.$sp_count.'").val("'.$sp_row["remarks"].'");'."\n";
	   		echo '$("#sp_q1_'.$sp_count.'").val("'.$sp_row["q1"].'");'."\n";
	   		echo '$("#sp_q2_'.$sp_count.'").val("'.$sp_row["q2"].'");'."\n";
	   		echo '$("#sp_q3_'.$sp_count.'").val("'.$sp_row["q3"].'");'."\n";
	   		echo '$("#sp_q4_'.$sp_count.'").val("'.$sp_row["q4"].'");'."\n";

	   		if ($sp_row["q1"] == 1) {
	   			echo '$("#sp_q1_'.$sp_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($sp_row["q2"] == 1) {
	   			echo '$("#sp_q2_'.$sp_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($sp_row["q3"] == 1) {
	   			echo '$("#sp_q3_'.$sp_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($sp_row["q4"] == 1) {
	   			echo '$("#sp_q4_'.$sp_count.'").prop("checked",true);'."\n";	
	   		}
   		}
   	}

   	$cf_rs  = SelectEach("dpcr_core_functions","WHERE spms_dpcr_id = '$refid'");
   	if ($cf_rs) {
   		$cf_count = 0;
   		while ($cf_row = mysqli_fetch_assoc($cf_rs)) {
   			$cf_count++;
   			echo '$("#cf_refid_'.$cf_count.'").val("'.$cf_row["RefId"].'");'."\n";
	   		echo '$("#cf_objectives_'.$cf_count.'").val("'.$cf_row["objective"].'");'."\n";
	   		echo '$("#cf_percent_'.$cf_count.'").val("'.$cf_row["percent"].'");'."\n";
	   		echo '$("#cf_si_'.$cf_count.'").val("'.$cf_row["success_indicator"].'");'."\n";
	   		echo '$("#cf_sitype_'.$cf_count.'").val("'.$cf_row["sitype"].'");'."\n";
	   		echo '$("#cf_rating_'.$cf_count.'").val("'.$cf_row["rating"].'");'."\n";
	   		echo '$("#cf_budget_'.$cf_count.'").val("'.$cf_row["budget"].'");'."\n";
	   		echo '$("#cf_remarks_'.$cf_count.'").val("'.$cf_row["remarks"].'");'."\n";
	   		echo '$("#cf_q1_'.$cf_count.'").val("'.$cf_row["q1"].'");'."\n";
	   		echo '$("#cf_q2_'.$cf_count.'").val("'.$cf_row["q2"].'");'."\n";
	   		echo '$("#cf_q3_'.$cf_count.'").val("'.$cf_row["q3"].'");'."\n";
	   		echo '$("#cf_q4_'.$cf_count.'").val("'.$cf_row["q4"].'");'."\n";

	   		if ($cf_row["q1"] == 1) {
	   			echo '$("#cf_q1_'.$cf_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($cf_row["q2"] == 1) {
	   			echo '$("#cf_q2_'.$cf_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($cf_row["q3"] == 1) {
	   			echo '$("#cf_q3_'.$cf_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($cf_row["q4"] == 1) {
	   			echo '$("#cf_q4_'.$cf_count.'").prop("checked",true);'."\n";	
	   		}
   		}
   	}

   	$sf_rs  = SelectEach("dpcr_support_functions","WHERE spms_dpcr_id = '$refid'");
   	if ($sf_rs) {
   		$sf_count = 0;
   		while ($sf_row = mysqli_fetch_assoc($sf_rs)) {
   			$sf_count++;
   			echo '$("#sf_refid_'.$sf_count.'").val("'.$sf_row["RefId"].'");'."\n";
	   		echo '$("#sf_objectives_'.$sf_count.'").val("'.$sf_row["objective"].'");'."\n";
	   		echo '$("#sf_percent_'.$sf_count.'").val("'.$sf_row["percent"].'");'."\n";
	   		echo '$("#sf_si_'.$sf_count.'").val("'.$sf_row["success_indicator"].'");'."\n";
	   		echo '$("#sf_sitype_'.$sf_count.'").val("'.$sf_row["sitype"].'");'."\n";
	   		echo '$("#sf_rating_'.$sf_count.'").val("'.$sf_row["rating"].'");'."\n";
	   		echo '$("#sf_budget_'.$sf_count.'").val("'.$sf_row["budget"].'");'."\n";
	   		echo '$("#sf_remarks_'.$sf_count.'").val("'.$sf_row["remarks"].'");'."\n";
	   		echo '$("#sf_q1_'.$sf_count.'").val("'.$sf_row["q1"].'");'."\n";
	   		echo '$("#sf_q2_'.$sf_count.'").val("'.$sf_row["q2"].'");'."\n";
	   		echo '$("#sf_q3_'.$sf_count.'").val("'.$sf_row["q3"].'");'."\n";
	   		echo '$("#sf_q4_'.$sf_count.'").val("'.$sf_row["q4"].'");'."\n";

	   		if ($sf_row["q1"] == 1) {
	   			echo '$("#sf_q1_'.$sf_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($sf_row["q2"] == 1) {
	   			echo '$("#sf_q2_'.$sf_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($sf_row["q3"] == 1) {
	   			echo '$("#sf_q3_'.$sf_count.'").prop("checked",true);'."\n";	
	   		}
	   		if ($sf_row["q4"] == 1) {
	   			echo '$("#sf_q4_'.$sf_count.'").prop("checked",true);'."\n";	
	   		}
	   		
   		}
   	}
   }
   function saveDPCR(){
   	$emprefid 	= getvalue("sint_EmployeesRefId");
   	$AgencyId 	= FindFirst("employees","WHERE RefId = '$emprefid'","AgencyId");
   	$count_sp 	= getvalue("count_sp");
   	$count_cf 	= getvalue("count_cf");
   	$count_sf 	= getvalue("count_sf");
   	$quarter  	= getvalue("sint_Quarter");
   	$year 	 	= getvalue("sint_Year");
   	$error 	 	= 0;
   	$dpcr_refid = getvalue("sint_DPCRRefId");

   	if (intval($dpcr_refid) == 0) {
   		$dpcr_fld = "";
	   	$dpcr_val = "";
	   	$dpcr_fld .= "`EmployeesRefId`, `AgencyId`, `Quarter`, `Year`,";
	   	$dpcr_val .= "'$emprefid', '$AgencyId', '$quarter', '$year', ";
	   	$spms_dpcr_id = f_SaveRecord("NEWSAVE","spms_dpcr",$dpcr_fld,$dpcr_val);
	   	if (is_numeric($spms_dpcr_id)) {
	   		for ($a=1; $a <= $count_sp; $a++) { 
		   		$objective = realEscape($_POST["sp_objectives_".$a]);
		   		if ($objective != "") {
		   			$dpcr_sp_fld 	= "";
		   			$dpcr_sp_val 	= "";
		   			$sp_objectives = realEscape($_POST["sp_objectives_".$a]);
					   $sp_percent 	= realEscape($_POST["sp_percent_".$a]);
					   $sp_si 			= realEscape($_POST["sp_si_".$a]);
					   $sp_sitype 		= realEscape($_POST["sp_sitype_".$a]);
					   $sp_rating 		= realEscape($_POST["sp_rating_".$a]);
					   $sp_budget 		= realEscape($_POST["sp_budget_".$a]);
					   $sp_remarks 	= realEscape($_POST["sp_remarks_".$a]);

					   $dpcr_sp_fld   .= "`spms_dpcr_id`, `objective`, `percent`, `success_indicator`, `sitype`, ";
					   $dpcr_sp_fld 	.= "`rating`, `remarks`, `budget`,";
					   $dpcr_sp_val	.= "'$spms_dpcr_id', '$sp_objectives', '$sp_percent', '$sp_si', ";
					   $dpcr_sp_val   .= "'$sp_sitype', '$sp_rating', '$sp_remarks', '$sp_budget',";
					   if (isset($_POST["sp_q1_".$a])) {
					   	$sp_q1 = realEscape($_POST["sp_q1_".$a]);
					   	$dpcr_sp_fld .= "`q1`,";
					   	$dpcr_sp_val .= "'$sp_q1',";
					   }
					   if (isset($_POST["sp_q2_".$a])) {
					   	$sp_q2 = realEscape($_POST["sp_q2_".$a]);
					   	$dpcr_sp_fld .= "`q2`,";
					   	$dpcr_sp_val .= "'$sp_q2',";
					   }
					   if (isset($_POST["sp_q3_".$a])) {
					   	$sp_q3 = realEscape($_POST["sp_q3_".$a]);
					   	$dpcr_sp_fld .= "`q3`,";
					   	$dpcr_sp_val .= "'$sp_q3',";
					   }
					   if (isset($_POST["sp_q4_".$a])) {
					   	$sp_q4 = realEscape($_POST["sp_q4_".$a]);
					   	$dpcr_sp_fld .= "`q4`,";
					   	$dpcr_sp_val .= "'$sp_q4',";
					   }
					   $save_sp = f_SaveRecord("NEWSAVE","dpcr_strategic_priorities",$dpcr_sp_fld,$dpcr_sp_val);
					   if (!is_numeric($save_sp)) {
					   	$error++;
					   }
		   		}
		   	}


		   	for ($b=1; $b <= $count_cf; $b++) { 
		   		$objective = realEscape($_POST["cf_objectives_".$b]);
		   		if ($objective != "") {
		   			$dpcr_cf_fld 	= "";
		   			$dpcr_cf_val 	= "";
		   			$cf_objectives = realEscape($_POST["cf_objectives_".$b]);
					   $cf_percent 	= realEscape($_POST["cf_percent_".$b]);
					   $cf_si 			= realEscape($_POST["cf_si_".$b]);
					   $cf_sitype 		= realEscape($_POST["cf_sitype_".$b]);
					   $cf_rating 		= realEscape($_POST["cf_rating_".$b]);
					   $cf_remarks 	= realEscape($_POST["cf_remarks_".$b]);
					   $cf_budget 		= realEscape($_POST["cf_budget_".$b]);

					   $dpcr_cf_fld   = "`spms_dpcr_id`, `objective`, `percent`, `success_indicator`, `sitype`, ";
					   $dpcr_cf_fld 	.= "`rating`, `remarks`, `budget`,";
					   $dpcr_cf_val	.= "'$spms_dpcr_id', '$cf_objectives', '$cf_percent', '$cf_si', ";
					   $dpcr_cf_val   .= "'$cf_sitype', '$cf_rating', '$cf_remarks', '$cf_budget',";
					   if (isset($_POST["cf_q1_".$b])) {
					   	$cf_q1 = realEscape($_POST["cf_q1_".$b]);
					   	$dpcr_cf_fld .= "`q1`,";
					   	$dpcr_cf_val .= "'$cf_q1',";
					   }
					   if (isset($_POST["cf_q2_".$b])) {
					   	$cf_q2 = realEscape($_POST["cf_q2_".$b]);
					   	$dpcr_cf_fld .= "`q2`,";
					   	$dpcr_cf_val .= "'$cf_q2',";
					   }
					   if (isset($_POST["cf_q3_".$b])) {
					   	$cf_q3 = realEscape($_POST["cf_q3_".$b]);
					   	$dpcr_cf_fld .= "`q3`,";
					   	$dpcr_cf_val .= "'$cf_q3',";
					   }
					   if (isset($_POST["cf_q4_".$b])) {
					   	$cf_q4 = realEscape($_POST["cf_q4_".$b]);
					   	$dpcr_cf_fld .= "`q4`,";
					   	$dpcr_cf_val .= "'$cf_q4',";
					   }
					   $save_cf = f_SaveRecord("NEWSAVE","dpcr_core_functions",$dpcr_cf_fld,$dpcr_cf_val);
					   if (!is_numeric($save_cf)) {
					   	$error++;
					   }
		   		}
		   	}


		   	for ($c=1; $c <= $count_sf; $c++) { 
		   		$objective = realEscape($_POST["sf_objectives_".$c]);
		   		if ($objective != "") {
		   			$dpcr_sf_fld 	= "";
		   			$dpcr_sf_val 	= "";
		   			$sf_objectives = realEscape($_POST["sf_objectives_".$c]);
					   $sf_percent 	= realEscape($_POST["sf_percent_".$c]);
					   $sf_si 			= realEscape($_POST["sf_si_".$c]);
					   $sf_sitype 		= realEscape($_POST["sf_sitype_".$c]);
					   $sf_rating 		= realEscape($_POST["sf_rating_".$c]);
					   $sf_remarks 	= realEscape($_POST["sf_remarks_".$c]);
					   $sf_budget 		= realEscape($_POST["sf_budget_".$c]);

					   $dpcr_sf_fld   = "`spms_dpcr_id`, `objective`, `percent`, `success_indicator`, `sitype`, ";
					   $dpcr_sf_fld 	.= "`rating`, `remarks`, `budget`,";
					   $dpcr_sf_val	.= "'$spms_dpcr_id', '$sf_objectives', '$sf_percent', '$sf_si', ";
					   $dpcr_sf_val   .= "'$sf_sitype', '$sf_rating', '$sf_remarks', '$sf_budget',";
					   if (isset($_POST["sf_q1_".$c])) {
					   	$sf_q1 = realEscape($_POST["sf_q1_".$c]);
					   	$dpcr_sf_fld .= "`q1`,";
					   	$dpcr_sf_val .= "'$sf_q1',";
					   }
					   if (isset($_POST["sf_q2_".$c])) {
					   	$sf_q2 = realEscape($_POST["sf_q2_".$c]);
					   	$dpcr_sf_fld .= "`q2`,";
					   	$dpcr_sf_val .= "'$sf_q2',";
					   }
					   if (isset($_POST["sf_q3_".$c])) {
					   	$sf_q3 = realEscape($_POST["sf_q3_".$c]);
					   	$dpcr_sf_fld .= "`q3`,";
					   	$dpcr_sf_val .= "'$sf_q3',";
					   }
					   if (isset($_POST["sf_q4_".$c])) {
					   	$sf_q4 = realEscape($_POST["sf_q4_".$c]);
					   	$dpcr_sf_fld .= "`q4`,";
					   	$dpcr_sf_val .= "'$sf_q4',";
					   }
					   $save_sf = f_SaveRecord("NEWSAVE","dpcr_support_functions",$dpcr_sf_fld,$dpcr_sf_val);
					   if (!is_numeric($save_sf)) {
					   	$error++;
					   }
		   		}
		   	}	
	   	} else {
		   	$error++;
		   }
		   if ($error == 0) {
		   	echo 'afterSave();';
		   } else {
		   	echo "Error";
		   }	
   	} else {
   		$dpcr_fldnval 	= "Quarter = '$quarter', Year = '$year',";
   		$update_dpcr 	= f_SaveRecord("EDITSAVE","spms_dpcr",$dpcr_fldnval,$dpcr_refid);
   		if ($update_dpcr == "") {
   			for ($a=1; $a <= $count_sp; $a++) { 
   				$sp_refid = realEscape($_POST["sp_refid_".$a]);
   				if (intval($sp_refid) > 0) {
   					$dpcr_sp_fldnval = "";
   					$sp_objectives = realEscape($_POST["sp_objectives_".$a]);
					   $sp_percent 	= realEscape($_POST["sp_percent_".$a]);
					   $sp_si 			= realEscape($_POST["sp_si_".$a]);
					   $sp_sitype 		= realEscape($_POST["sp_sitype_".$a]);
					   $sp_rating 		= realEscape($_POST["sp_rating_".$a]);
					   $sp_remarks 	= realEscape($_POST["sp_remarks_".$a]);
					   $sp_budget 		= realEscape($_POST["sp_budget_".$a]);
					   $dpcr_sp_fldnval  .= "`objective` = '$sp_objectives', `percent` = '$sp_percent', ";
					   $dpcr_sp_fldnval	.= "`success_indicator` = '$sp_si', `budget` = '$sp_budget',";
					   $dpcr_sp_fldnval	.= "`sitype` = '$sp_sitype', `rating` = '$sp_rating', `remarks` = '$sp_remarks', ";
					   if (isset($_POST["sp_q1_".$a])) {
					   	$sp_q1 = realEscape($_POST["sp_q1_".$a]);
					   	$dpcr_sp_fldnval .= "`q1` = '$sp_q1',";
					   } else {
					   	$dpcr_sp_fldnval .= "`q1` = '0',";
					   }
					   if (isset($_POST["sp_q2_".$a])) {
					   	$sp_q2 = realEscape($_POST["sp_q2_".$a]);
					   	$dpcr_sp_fldnval .= "`q2` = '$sp_q2',";
					   } else {
					   	$dpcr_sp_fldnval .= "`q2` = '0',";
					   }
					   if (isset($_POST["sp_q3_".$a])) {
					   	$sp_q3 = realEscape($_POST["sp_q3_".$a]);
					   	$dpcr_sp_fldnval .= "`q3` = '$sp_q3',";
					   } else {
					   	$dpcr_sp_fldnval .= "`q3` = '0',";
					   }
					   if (isset($_POST["sp_q4_".$a])) {
					   	$sp_q4 = realEscape($_POST["sp_q4_".$a]);
					   	$dpcr_sp_fldnval .= "`q4` = '$sp_q4',";
					   } else {
					   	$dpcr_sp_fldnval .= "`q4` = '0',";
					   }
					   $update_sp 	= f_SaveRecord("EDITSAVE","dpcr_strategic_priorities",$dpcr_sp_fldnval,$sp_refid);
					   if ($update_sp != "") {
					   	$error++;
					   }
   				}
   			}


   			for ($b=1; $b <= $count_cf; $b++) { 
   				$cf_refid = realEscape($_POST["cf_refid_".$b]);
   				if (intval($cf_refid) > 0) {
   					$cf_objectives = realEscape($_POST["cf_objectives_".$b]);
					   $cf_percent 	= realEscape($_POST["cf_percent_".$b]);
					   $cf_si 			= realEscape($_POST["cf_si_".$b]);
					   $cf_sitype 		= realEscape($_POST["cf_sitype_".$b]);
					   $cf_rating 		= realEscape($_POST["cf_rating_".$b]);
					   $cf_remarks 	= realEscape($_POST["cf_remarks_".$b]);
					   $cf_budget 		= realEscape($_POST["cf_budget_".$b]);
					   $dpcr_cf_fldnval   = "`objective` = '$cf_objectives', `percent` = '$cf_percent', ";
					   $dpcr_cf_fldnval	.= "`success_indicator` = '$cf_si', `budget` = '$cf_budget',";
					   $dpcr_cf_fldnval	.= "`sitype` = '$cf_sitype', `rating` = '$cf_rating', `remarks` = '$cf_remarks', ";
					   if (isset($_POST["cf_q1_".$b])) {
					   	$cf_q1 = realEscape($_POST["cf_q1_".$b]);
					   	$dpcr_cf_fldnval .= "`q1` = '$cf_q1',";
					   } else {
					   	$dpcr_cf_fldnval .= "`q1` = '0',";
					   }
					   if (isset($_POST["cf_q2_".$b])) {
					   	$cf_q2 = realEscape($_POST["cf_q2_".$b]);
					   	$dpcr_cf_fldnval .= "`q2` = '$cf_q2',";
					   } else {
					   	$dpcr_cf_fldnval .= "`q2` = '0',";
					   }
					   if (isset($_POST["cf_q3_".$b])) {
					   	$cf_q3 = realEscape($_POST["cf_q3_".$b]);
					   	$dpcr_cf_fldnval .= "`q3` = '$cf_q3',";
					   } else {
					   	$dpcr_cf_fldnval .= "`q3` = '0',";
					   }
					   if (isset($_POST["cf_q4_".$b])) {
					   	$cf_q4 = realEscape($_POST["cf_q4_".$b]);
					   	$dpcr_cf_fldnval .= "`q4` = '$cf_q4',";
					   } else {
					   	$dpcr_cf_fldnval .= "`q4` = '0',";
					   }
					   $update_cf 	= f_SaveRecord("EDITSAVE","dpcr_core_functions",$dpcr_cf_fldnval,$cf_refid);
					   if ($update_cf != "") {
					   	$error++;
					   }
   				}   				
   			}


   			for ($c=1; $c <= $count_sf; $c++) { 
   				$sf_refid = realEscape($_POST["sf_refid_".$c]);
   				if (intval($sf_refid) > 0) {
   					$sf_objectives = realEscape($_POST["sf_objectives_".$c]);
					   $sf_percent 	= realEscape($_POST["sf_percent_".$c]);
					   $sf_si 			= realEscape($_POST["sf_si_".$c]);
					   $sf_sitype 		= realEscape($_POST["sf_sitype_".$c]);
					   $sf_rating 		= realEscape($_POST["sf_rating_".$c]);
					   $sf_remarks 	= realEscape($_POST["sf_remarks_".$c]);
					   $sf_budget 		= realEscape($_POST["sf_budget_".$c]);
					   $dpcr_sf_fldnval   = "`objective` = '$sf_objectives', `percent` = '$sf_percent', ";
					   $dpcr_sf_fldnval	.= "`success_indicator` = '$sf_si', `budget` = '$sf_budget',";
					   $dpcr_sf_fldnval	.= "`sitype` = '$sf_sitype', `rating` = '$sf_rating', `remarks` = '$sf_remarks', ";
					   if (isset($_POST["sf_q1_".$c])) {
					   	$sf_q1 = realEscape($_POST["sf_q1_".$c]);
					   	$dpcr_sf_fldnval .= "`q1` = '$sf_q1',";
					   } else {
					   	$dpcr_sf_fldnval .= "`q1` = '0',";
					   }
					   if (isset($_POST["sf_q2_".$c])) {
					   	$sf_q2 = realEscape($_POST["sf_q2_".$c]);
					   	$dpcr_sf_fldnval .= "`q2` = '$sf_q2',";
					   } else {
					   	$dpcr_sf_fldnval .= "`q2` = '0',";
					   }
					   if (isset($_POST["sf_q3_".$c])) {
					   	$sf_q3 = realEscape($_POST["sf_q3_".$c]);
					   	$dpcr_sf_fldnval .= "`q3` = '$sf_q3',";
					   } else {
					   	$dpcr_sf_fldnval .= "`q3` = '0',";
					   }
					   if (isset($_POST["sf_q4_".$c])) {
					   	$sf_q4 = realEscape($_POST["sf_q4_".$c]);
					   	$dpcr_sf_fldnval .= "`q4` = '$sf_q4',";
					   } else {
					   	$dpcr_sf_fldnval .= "`q4` = '0',";
					   }
					   $update_sf 	= f_SaveRecord("EDITSAVE","dpcr_strategic_priorities",$dpcr_sf_fldnval,$sf_refid);
					   if ($update_sf != "") {
					   	$error++;
					   }
   				}
   			}
   		} else {
   			$error++;
   		}
   		if ($error == 0) {
		   	//echo 'afterEdit();';
		   } else {
		   	echo "Error";
		   }
   	}   	
   }
?>