<?php
   require_once 'constant.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   //include 'pageHead.e2e.php';
   $pdsProp = file_get_contents(json.'pds.json');
   $pds = json_decode($pdsProp, true);
   $split = json_decode(getvalue("split"), true);
   /*RUNNING*/
   $diag = 0;
   $empRefId = getvalue("empRefId");
   $MasterFile = true;
   $rsPersonInfo = FindFirst("employees","WHERE RefId = $empRefId","*");
   $rsEmpFamily = FindFirst("employeesfamily","WHERE EmployeesRefId = $empRefId","*");
   $rsEmpChild = SelectEach("employeeschild","WHERE EmployeesRefId = $empRefId ORDER BY BirthDate DESC");
   $rsEmpEducElem = FindFirst("employeeseduc","WHERE LevelType = 1 AND EmployeesRefId = $empRefId","*");
   $rsEmpEducSeco = FindFirst("employeeseduc","WHERE LevelType = 2 AND EmployeesRefId = $empRefId","*");
   $rsEmpEducVoca = SelectEach("employeeseduc","WHERE LevelType = 3 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC");
   $rsEmpEducColl = SelectEach("employeeseduc","WHERE LevelType = 4 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC");
   $rsEmpEducGrad = SelectEach("employeeseduc","WHERE LevelType = 5 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC");
   $rsEmpEduc = SelectEach("employeeseduc","WHERE LevelType >= 3 AND LevelType <= 5 AND EmployeesRefId = $empRefId ORDER BY LevelType, DateFrom DESC");
   $rsEmpEligibility = SelectEach("employeeselegibility","WHERE EmployeesRefId = $empRefId ORDER BY ExamDate DESC");
   $rsEmpWorkExp = SelectEach("employeesworkexperience","WHERE EmployeesRefId = $empRefId AND (isServiceRecord = 0 OR isServiceRecord IS NULL) ORDER BY WorkStartDate DESC");
   $rsEmpVoluntary = SelectEach("employeesvoluntary","WHERE EmployeesRefId = $empRefId ORDER BY StartDate DESC");
   $rsEmpTraining = SelectEach("employeestraining","WHERE EmployeesRefId = $empRefId ORDER BY StartDate DESC");
   $rsEmpOtherInfo = SelectEach("employeesotherinfo","WHERE EmployeesRefId = $empRefId ORDER BY RefId");
   $rsEmpReference = SelectEach("employeesreference","WHERE EmployeesRefId = $empRefId ORDER BY RefId");
   $rsPDSQ = FindFirst("employeespdsq","WHERE EmployeesRefId = $empRefId","*");
   $EmpChild_split = false;
   if ($rsEmpChild) {
      $rowCount_EmpChild = mysqli_num_rows($rsEmpChild);
      if ($rowCount_EmpChild > $pds["rowChildren"]) {
         $EmpChild_split = true;
      }
   }
   function isSplit($tab,$rowNumber,$split) {
      if (isset($split[$tab])) {
         $val = $split[$tab];
         if ($tab == "Educ") {
            return ($rowNumber == $val);
         } else {
            $val_Array = explode(".",$val);
            if (array_search($rowNumber,$val_Array))
               return true;
            else
               return false;
         }
      } else return false;
   }
   function educTHEAD() {
      echo '<thead class="table_header">
               <tr align="center" class="ruler">
                  <td width="20px">A</td>
                  <td width="120px">B</td>
                  <td width="50px">C</td>
                  <td width="50px">D</td>
                  <td width="50px">E</td>
                  <td width="50px">F</td>
                  <td width="60px">G</td>
                  <td width="60px">H</td>
                  <td width="*px">I</td>
                  <td width="50px">J</td>
                  <td width="50px">K</td>
                  <td width="50px">L</td>
                  <td width="50px">M</td>
                  <td width="50px">N</td>
               </tr>
               <tr>
                  <td colspan="14" class="bgGray tabTitle_td">
                     <span class="tabTitle">
                        <i>III. EDUCATIONAL BACKGROUND</i>
                     </span>
                  </td>
               </tr>
               <tr style="border:1px solid #000;">
                  <td rowspan="2" class="bgGrayLabel" align="center">26.</td>
                  <td rowspan="2" class="bgGrayLabel" align="center" style="border-left:1px solid gray;" valign>LEVEL</td>
                  <td rowspan="2" colspan="4" class="bgGrayLabel" align="center">
                     NAME OF SCHOOL
                     <br>
                     (Write in full)
                  </td>
                  <td rowspan="2" colspan="3" class="bgGrayLabel" align="center">
                     BASIC EDUCATION/DEGREE/COURSE
                     <br>
                     (Write in full)
                  </td>
                  <td colspan="2" class="bgGrayLabel" align="center">
                     PERIOD OF
                     <br>
                     ATTENDANCE
                  </td>
                  <td rowspan="2" class="bgGrayLabel" align="center">
                     HIGHEST
                     <br>
                     LEVEL/
                     <br>
                     UNITS EARNED
                     <br>
                     (if not graduated)
                  </td>
                  <td rowspan="2" class="bgGrayLabel" align="center">
                     YEAR
                     <br>
                     GRADUATED
                  </td>
                  <td rowspan="2" class="bgGrayLabel" align="center">
                     SCHOLARSHIP/
                     <br>
                     ACADEMIC
                     <br>
                     HONORS
                     <br>
                     RECEIVED
                  </td>
               </tr>
               <tr style="border:1px solid #000;">
                  <td class="bgGrayLabel" align="center">From</td>
                  <td class="bgGrayLabel" align="center">To</td>
               </tr>
            </thead>
      ';
   }
   function eligibTHEAD() {
      echo
      '<thead>
               <tr align="center" class="ruler">
                  <td width="20px">A</td>
                  <td width="7%">B</td>
                  <td width="7%">C</td>
                  <td width="7%">D</td>
                  <td width="7%">E</td>
                  <td width="7%">F</td>
                  <td width="7%">G</td>
                  <td width="7%">H</td>
                  <td width="*%">I</td>
                  <td width="7%">J</td>
                  <td width="7%">K</td>
                  <td width="7%">L</td>
                  <td width="100px">M</td>
               </tr>
               <tr>
                  <th colspan="13" class="bgGray tabTitle_td" style="text-align:left;">
                     <span class="tabTitle">
                        <i>IV. CIVIL SERVICE ELIGIBILITY</i>
                     </span>
                  </th>
               </tr>
               <tr>
                  <th class="bgGrayLabel" align="center" valign="center" width="20px" rowspan="2">27.</th>
                  <th class="bgGrayLabel" align="center" rowspan=2 colspan=4>
                     CAREER SERVICE/ RA 1080 (BOARD/ BAR) <br>UNDER SPECIAL LAWS/ CES/ CSEE<br>
                     BARANGAY ELIGIBILITY / DRIVER\'S LICENSE
                  </th>
                  <th class="bgGrayLabel" align="center" valign="center" rowspan=2 >
                     RATING<br>(If Applicable)
                  </th>
                  <th class="bgGrayLabel" align="center" valign="center" rowspan=2 colspan=2 style="width:100px;">
                     DATE OF EXAMINATION / <br>CONFERMENT
                  </th>
                  <th class="bgGrayLabel" align="center" valign="center" rowspan=2 colspan=3>
                     PLACE OF EXAMINATION / <br>CONFERMENT
                  </th>
                  <th class="bgGrayLabel" align="center" valign="center" colspan=2>
                     LICENSE (if applicable)
                  </th>
               </tr>
               <tr>
                  <th class="bgGrayLabel" align="center" valign="center" rowspan=2 style="width:100px;">NUMBER</th>
                  <th class="bgGrayLabel" align="center" valign="center" rowspan=2 style="width:100px;">Date of Validatiy</th>
               </tr>
            </thead>';
   }
   function workexpTHEAD() {
      echo
      '<thead class="table_header">
         <tr align="center" class="ruler">
            <td width="20px">A</td>
            <td width="7%">B</td>
            <td width="7%">C</td>
            <td width="7%">D</td>
            <td width="7%">E</td>
            <td width="7%">F</td>
            <td width="7%">G</td>
            <td width="7%">H</td>
            <td width="*%">I</td>
            <td width="7%">J</td>
            <td width="10%">K</td>
            <td width="10%">L</td>
            <td width="7%">M</td>
         </tr>
         <tr>
            <th colspan="13" class="bgGray tabTitle_td" style="text-align:left;">
               <span class="tabTitle">
                  <i>V. WORK EXPERIENCE</i>
               </span><br>
               <span class="tabTitle" style="font-size:10px;">
                  <i>
                     (Include private employment.  Start from your recent work) Description of duties should be indicated in the attached Work Experience sheet.
                  </i>
               </span>
            </th>
         </tr>
         <tr>
            <th class="bgGrayLabel" rowspan=1 colspan=3>
               28. INCLUSIVE DATES<br>(mm/dd/yyyy)
            </th>
            <th class="bgGrayLabel" rowspan=3 colspan=3>
               POSITION TITLE<br>(Write in full/do not abbreviate)
            </th>
            <th class="bgGrayLabel" rowspan=3 colspan=3>
               DEPARTMENT / AGENCY / OFFICE / COMPANY<br>
               (Write in full/Do not abbreviate)
            </th>
            <th class="bgGrayLabel" rowspan=3>
               MONTHLY SALARY
            </th>
            <th class="bgGrayLabel" rowspan=3>
               SALARY/ JOB/ PAY GRADE <br>
               (if applicable) & STEP (Format "00-0")/ <br>
               INCREMENT
            </th>
            <th class="bgGrayLabel" rowspan=3>
               STATUS OF APPOINTMENT
            </th>
            <th class="bgGrayLabel" rowspan=3>
               GOV\'T SERVICE<br>(Y/N)
            </th>
         </tr>
         <tr>
            <th class="bgGrayLabel" colspan=3>
               <table style="height:100%">
                  <tr>
                     <td width="100px" align="center">From</td>
                     <td width="100px" align="center">To</td>
                  </tr>
               </table>
            </th>
         </tr>
      </thead>';
   }
   function volworkTHEAD() {
      echo
      '<thead>
            <tr align="center" class="ruler">
               <td width="26px">A</td>
               <td width="219px">B</td>
               <td width="26px">C</td>
               <td width="132px">D</td>
               <td width="72px">E</td>
               <td width="70px">F</td>
               <td width="67px">G</td>
               <td width="72px">H</td>
               <td width="25px">I</td>
               <td width="175px">J</td>
            </tr>
            <tr>
               <td colspan="10" class="bgGray tabTitle_td">
                  <span class="tabTitle">
                     <i>VI. VOLUNTARY WORK OR INVOLVEMENT IN CIVIC/ NON-GOVERNMENT/
                     PEOPLE/ VOLUNTARY ORGANIZATION/ S</i>
                  </span>
               </td>
            </tr>
            <tr>
               <td rowspan="2" class="bgGrayLabel" align="center">29.</td>
               <td colspan="3" rowspan="2" class="bgGrayLabel" align="center">
                  NAME & ADDRESS OF ORGANIZATION
                  <br>
                  (Write in full)
               </td>
               <td colspan="2" class="bgGrayLabel" align="center">
                  INCLUSIVE DATES
                  <br>
                  (mm/dd/yyyy)
               </td>
               <td rowspan="2" class="bgGrayLabel" align="center">
                  NUMBER OF
                  <br>
                  HOURS
               </td>
               <td colspan="3" rowspan="2" class="bgGrayLabel" align="center">
                  POSITION/ NATURE OF WORK
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center">
                  From
               </td>
               <td class="bgGrayLabel" align="center">
                  To
               </td>
            </tr>
         </thead>';

   }
   function learningTHEAD() {
      echo '
      <thead>
            <tr align="center" class="ruler">
               <td width="26px">A</td>
               <td width="219px">B</td>
               <td width="26px">C</td>
               <td width="132px">D</td>
               <td width="72px">E</td>
               <td width="70px">F</td>
               <td width="67px">G</td>
               <td width="72px">H</td>
               <td width="25px">I</td>
               <td width="175px">J</td>
            </tr>
            <tr>
               <td colspan="10" class="bgGray tabTitle_td">
                  <span class="tabTitle">
                     <i>VII. LEARNING AND DEVELOPMENT (L&D) INTERVENTIONS/ TRAINING PROGRAMS ATTENDED </i>
                  </span>
               </td>
            </tr>
            <tr>
               <td colspan="10" class="bgGray" style="font-size:8pt;">
                  <span class="">
                     <i>(Start from the most recent L&D /training program and include only the relevant L&D/training taken for the last five (5) years for Division Chief/Executive/Managerial positions)</i>
                  </span>
               </td>
            </tr>
            <tr>
               <td rowspan="2" class="bgGrayLabel" align="center">30.</td>
               <td colspan="3" rowspan="2" class="bgGrayLabel" align="center">
                  TITLE OF LEARNING AND DEVELOPMENT INTERVENTIONS/TRAINING PROGRAMS
                  <br>
                  (Write in full)
               </td>
               <td colspan="2" class="bgGrayLabel" align="center">
                  INCLUSIVE DATES OF
                  <br>
                  ATTENDANCE
                  <br>
                  (mm/dd/yyyy)
               </td>
               <td rowspan="2" class="bgGrayLabel" align="center">
                  NUMBER OF
                  <br>
                  HOURS
               </td>
               <td rowspan="2" class="bgGrayLabel" align="center">
                  Type of LD
                  <br>
                  (Managerial/
                  <br>
                  Supervisory/
                  <br>
                  Technical/etc)
               </td>
               <td colspan="2" rowspan="2" class="bgGrayLabel" align="center">
                   CONDUCTED/ SPONSORED BY
                   <br>
                   (Write in full)
               </td>
            </tr>
            <tr>
               <td class="bgGrayLabel" align="center">
                  From
               </td>
               <td class="bgGrayLabel" align="center">
                  To
               </td>
            </tr>
         </thead>
      ';

   }
   function otherinfoTHEAD() {
      echo '<thead>
            <tr align="center" class="ruler">
               <td width="20px">A</td>
               <td width="175px">B</td>
               <td width="50px">C</td>
               <td width="50px">D</td>

               <td width="20px">E</td>
               <td width="175px">F</td>
               <td width="50px">G</td>
               <td width="50px">H</td>

               <td width="20px">I</td>
               <td width="175px">J</td>
            </tr>
            <tr>
               <td colspan="10" class="bgGray tabTitle_td">
                  <span class="tabTitle">
                     <i>VIII. OTHER INFORMATION</i>
                  </span>
               </td>
            </tr>
            <tr>
               <td colspan="3" class="bgGrayLabel" align="center">
                  31. SPECIAL SKILLS / HOBBIES:
               </td>
               <td colspan="3" class="bgGrayLabel" align="center">
                  32. NON-ACADEMIC DISTINCTIONS / RECOGNITION:
                  <br>
                  (Write in full)
               </td>
               <td class="bgGrayLabel" align="center">33.</td>
               <td colspan="4" class="bgGrayLabel" align="center">
                  MEMBERSHIP IN ASSOCIATION/ORGANIZATION
                  <br>
                  (Write in full)
               </td>
            </tr>
         </thead>   ';

   }
   function pdsFooter($showContinuation,$colspan) {
      if ($showContinuation) {
         rowContinuation($colspan);
      }
   }
   function rowContinuation($colspan) {
      echo
      '<tr><td colspan="'.$colspan.'" class="bgGrayLabel" align="center" style="color:#FF0000;border-top:1px solid #000;border-bottom:1px solid #000;">
         <i>(Continue on separate sheet if necessary)</i>
      </td></tr>';
   }
   function footer_signdate($bottom) {
      echo
      '<div class="table_footer" ';
      if ($bottom) {
         echo 'style="position:absolute;bottom:0px;left:0px;width:100%;`"';
      }
      echo
      '>
         <table width="100%">
            <tr>
               <td class="bgGrayLabel Sign" style="border:1px solid black;"><i>SIGNATURE</i></td>
               <td style="width:*px;border:1px solid black;"></td>
               <td class="bgGrayLabel Date" style="border:1px solid black;"><i>DATE</i></td>
               <td style="width:90px;border:1px solid black;" colspan="2"></td>
            </tr>
            <tr>
               <td colspan="4">&nbsp;</td>
               <td align="right" class="paging" style="border: none;">
                  <i>CS FORM 212 (Revised 2017),
                     <span class="page-number"></span>
                     <span class="page-total"></span>
                  </i>&nbsp;
               </td>
            </tr>
         </table>
      </div>';
   }
   function setValue($rs,$fields) {
      if (empty($rs[$fields])) {
         echo '<span style="color:blue;font-weight:600;">N/A</span>';
      } else {
         if ($fields == "EmailAdd") {
            echo '<span style="color:blue;font-weight:600;">'.$rs[$fields].'</span>';
         } else {
            if ($rs[$fields] == "1901-01-01") {
               echo '<span style="color:blue;font-weight:600;">N/A</span>';
            } else {
               echo '<span style="color:blue;font-weight:600;">'.strtoupper($rs[$fields]).'</span>';
            }
         }
      }
   }
   function validateDate($date, $format = 'Y-m-d'){
      $d = DateTime::createFromFormat($format, $date);
      return $d && $d->format($format) == $date;
   }
   function dispValue($str) {
      if (trim($str) == "") {
         echo "N/A";
      } else {
         if (trim($str) == "1901-01-01") {
            echo "N/A";
         } else {
            echo '<span style="color:blue;font-weight:600;">'.strtoupper($str).'</span>';
         }
      }

   }
   function dispVal($str) {
      if (trim($str) == "" || empty($str)) {
         return "N/A";
      } else {
         if (validateDate($str)) {
            if (trim($str) == "1901-01-01") {
               $str = "N/A";
            } else {
               $str_arr = explode("-", $str);
               $str = $str_arr[1]."/".$str_arr[2]."/".$str_arr[0];
            }
            return strtoupper($str);
         } else {
            return strtoupper($str);
         }
      }
   }
   function paging() {
      echo
      '<div class="noPrint">
         <a href="javascript:void(0);" id="pg1" >Page 1</a> | <a href="javascript:void(0);"id="pg2">Page 2</a> | <a href="javascript:void(0);" id="pg3">Page 3</a> | <a href="javascript:void(0);" id="pg4">Page 4</a>
      </div>';
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <script type="text/javascript" src="<?php echo path("jquery/jquery.js") ?>"></script>
      <script type="text/javascript">
         $(document).ready(function () {
            var totalHeight = 0;
            $(".table_footer .page-total").html($(".page-total").length);
            $(".ruler--").html("");
            /*$("#page1 table").each(function () {
               totalHeight += $(this).height();
            });
            $("#page1_adjuster").height((12.2 * 96 - totalHeight) + "px");*/
         });
      </script>
      <link rel="stylesheet" type="text/css" href="<?php echo path."css/tableless.css"; ?>">
      <style type="text/css">
         

         .ruler td {
            border: none;
            padding: 0 !important;
         }

         body {
            font-family:helvetica, Arial;
            font-size: 8.3px;
         }
         #PDSbody
         {
            counter-reset:page;
            font-family:helvetica, Arial;
            font-size:8.3px;
         }
         .table_footer {
            page-break-after: always;
         }
         .nextpage:before {
            counter-increment:page;
         }
         .table_footer .page-number:before {
            counter-increment:page;
            content:"Page " counter(page) " of ";
         }

         table {
               width:100%;
               border-collapse:collapse;
         }

         .tabTitle {
            font-family:Arial;font-size:11px;font-weight:bold;
         }
         .tabTitle_td {
            border-top:2px solid #000;border-bottom:2px solid #000;
         }
         .bgGray {
            background:silver;color:white;font-size:10pt;font-family:Arial;
         }
         .bgGrayLabel {
            background:silver;color:black;vertical-align:middle;font-size:8.3px;font-family:Arial;
         }
         .answer {
            font-family:Arial;font-size:9pt;font-weight:600;color:blue;padding-left: 3px;
         }
         .Sign {
            border-right:1px solid #000;width:150px;font-family:Arial;font-size:8pt;font-weight:bold;color:black;text-align:center
            }
         .Date {
            border-right:1px solid #000;border-left:1px solid #000;text-align:center;
            width:90px;font-family:Arial;font-size:8pt;font-weight:bold;color:black;
         }
         .b-left  {
            border-left:1px solid #000
         }
         .b-right {
            border-right:1px solid #000
         }
         .b-top {
            border-top:1px solid #000
         }
         .b-bottom {
            border-bottom:1px solid #000
         }
         .bgBlueLabel{
            background:#b3d1ff;color:black;vertical-align:top;
         }
         .paging {
            border-left:1px solid #000;width:170px;font-size:7px;
         }

         <?php
            if (empty($split["rowHeight"])) {

            } else {
               echo ".trHeight { height:".$split["rowHeight"]."px;}";
            }
         ?>
         .trHeight {
            height:<?php echo $pds["trHeight"]; ?>
         }
         .pdsq_exp {
            padding-bottom: 15px;
         }
         thead {
             font-weight : 600;
         }
         <?php echo $pds["cssTD"]; ?>
         .page-- {
            border:1px solid #000 !important;
            padding-bottom:16px;
         }
         tr.ruler {
            height:0px !important;
            visibility:hidden !important;
            background-color: black;
            padding: 0 !important;
            border: 0 !important;
            margin: 0 !important;
            font-size: 0 !important;
            line-height: 0 !important;
            height: 0 !important;
            white-space: normal;
            word-break: break-all !important;
            word-wrap: break-word !important;
         }
         .page1 td { 
            border-bottom:1px solid black; 
            border-left:1px solid black;
         }
         @media print {
            .noPrint, .noPrint *{display: none !important;}
            .nextpage    {
                page-break-after:always !important;
            }
            .page-- {
               border:1px solid #000 !important;
               position:relative !important;
               padding-bottom:0px;
            }
            .lastpage    {page-break-after:avoid !important;}
            #PDSbody
            {
               font-family:Arial;
               font-size:8pt;
               color:black;
            }
            .answer {font-family:Arial;font-size:9px !important;font-weight:600;}
            @page {
               size:8.5in 13in !important;
               margin-top: 0.4in !important;
               margin-bottom: 0.4in !important;
               margin-left: 0.4in !important;
               margin-right: 0.4in !important;
            }
            @page rotated {size:portrait;}
         }
      </style>
   </head>
   <body>
      <div id="PDSbody" class="PDSbody">
         <?php require_once("pds_Page1.e2e.php"); ?>
         <?php require_once("pds_Page2.e2e.php"); ?>
         <?php require_once("pds_Page3.e2e.php"); ?>
         <div class="page-- lastpage" id="page4" style="position:relative;">
            <?php require_once("pds_Page4.e2e.php"); ?>
         </div>
      </div>
      <script type="text/javascript">
         <?php
            echo
            '$(document).ready(function() {
               $(".ruler td").html("");
               $(".CHILDREN").html("N/A");
            ';
               if ($rsEmpChild) {
                  if ($EmpChild_split) {
                     $rsEmpChild = SelectEach("employeeschild","WHERE EmployeesRefId = $empRefId ORDER BY BirthDate DESC LIMIT 0,".$pds["rowChildren"]);
                  } else {
                     $rsEmpChild = SelectEach("employeeschild","WHERE EmployeesRefId = $empRefId ORDER BY BirthDate DESC");
                  }
                  $idx=32;
                  while ($row = mysqli_fetch_assoc($rsEmpChild)) {
                     $child_fullname = $row["FullName"];
                     $child_bday = $row["BirthDate"];
                     /*if (validateDate($child_bday)) {
                        echo 'console.log("Bday to '.$child_bday.'");';
                     }*/
                     if ($child_fullname == "") $child_fullname = "N/A";
                     if ($child_bday == "") $child_bday = "N/A";
                     echo '$("#page1_I'.$idx.'").html("'.strtoupper(dispVal($child_fullname)).'");'."\n";
                     echo '$("#page1_M'.$idx.'").html("'.dispVal($child_bday).'");'."\n";
                     $idx++;
                     /*if ($idx > 43) {
                        break;
                        console.log("PDS children : Overflow !!!");
                     }*/
                  }
               }

               echo '$(".ELEMENTARY").html("N/A");';
               if ($rsEmpEducElem) {
                  $elem_dateto = $rsEmpEducElem["DateTo"];
                  $elem_school = getRecord("schools",$rsEmpEducElem["SchoolsRefId"],"Name");
                  $elem_course = getRecord("course",$rsEmpEducElem["CourseRefId"],"Name");
                  $elem_datefrom = $rsEmpEducElem["DateFrom"];
                  $elem_highest = $rsEmpEducElem["HighestGrade"];
                  $elem_year = $rsEmpEducElem["YearGraduated"];
                  $elem_honor = $rsEmpEducElem["Honors"];
                  //if ($elem_dateto == "2999-12-31" || $rsEmpEducElem["Present"] == 1) $elem_dateto = "PRESENT";
                  if ($elem_dateto == 9999) {
                     $elem_dateto = "ON GOING";
                  }
                  echo '
                     $("#page1_C51").html("'.dispVal($elem_school).'");
                     $("#page1_F51").html("'.dispVal($elem_course).'");
                     $("#page1_J51").html("'.dispVal($elem_datefrom).'");
                     $("#page1_K51").html("'.dispVal($elem_dateto).'");
                     $("#page1_L51").html("'.dispVal($elem_highest).'");
                     $("#page1_M51").html("'.dispVal($elem_year).'");
                     $("#page1_N51").html("'.dispVal($elem_honor).'");
                  '."\n";
               }
               echo '$(".SECONDARY").html("N/A");';
               if ($rsEmpEducSeco) {
                  $secondary_school = getRecord("schools",$rsEmpEducSeco["SchoolsRefId"],"Name");
                  $secondary_course = getRecord("course",$rsEmpEducSeco["CourseRefId"],"Name");
                  $secondary_datefrom = $rsEmpEducSeco["DateFrom"];
                  $secondary_dateto = $rsEmpEducSeco["DateTo"];
                  $secondary_highest = $rsEmpEducSeco["HighestGrade"];
                  $secondary_year = $rsEmpEducSeco["YearGraduated"];
                  $secondary_honor = $rsEmpEducSeco["Honors"];

                  //if ($secondary_dateto == "2999-12-31" || $rsEmpEducSeco["Present"] == 1) $secondary_dateto = "PRESENT";
                  if ($secondary_dateto == 9999) {
                     $secondary_dateto = "ON GOING";
                  }
                  echo '
                     $("#page1_C53").html("'.dispVal($secondary_school).'");
                     $("#page1_F53").html("'.dispVal($secondary_course).'");
                     $("#page1_J53").html("'.dispVal($secondary_datefrom).'");
                     $("#page1_K53").html("'.dispVal($secondary_dateto).'");
                     $("#page1_L53").html("'.dispVal($secondary_highest).'");
                     $("#page1_M53").html("'.dispVal($secondary_year).'");
                     $("#page1_N53").html("'.dispVal($secondary_honor).'");
                  '."\n";
               }
               /*
               echo '$(".VOCATIONAL").html("N/A");';
               if ($rsEmpEducVoca) {
                  $dbtable = "employeeseduc";
                  $sql = "WHERE LevelType = 3 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC, RefId DESC";
                  if ($EmpEducVoca_split) {
                     $sql = "WHERE LevelType = 3 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC, RefId DESC LIMIT 0,".$pds["rowVocational"];
                  } 
                  $rsEmpEducVoca = SelectEach($dbtable,$sql);
                  $voc = 0;
                  while ($row = mysqli_fetch_assoc($rsEmpEducVoca)) {
                     $voc++;
                     $vocational_dateto = $row["DateTo"];
                     //if ($vocational_dateto == "2999-12-31" || $row["Present"] == 1) $vocational_dateto = "PRESENT";
                     if ($vocational_dateto == 9999) {
                        $vocational_dateto = "ON GOING";
                     }
                     echo '
                        $("#page1_voc_row'.$voc.'_col1").html("'.dispVal(getRecord("schools",$row["SchoolsRefId"],"Name")).'");
                        $("#page1_voc_row'.$voc.'_col2").html("'.dispVal(getRecord("course",$row["CourseRefId"],"Name")).'");
                        $("#page1_voc_row'.$voc.'_col3").html("'.dispVal($row["DateFrom"]).'");
                        $("#page1_voc_row'.$voc.'_col4").html("'.dispVal($vocational_dateto).'");
                        $("#page1_voc_row'.$voc.'_col5").html("'.dispVal($row["HighestGrade"]).'");
                        $("#page1_voc_row'.$voc.'_col6").html("'.dispVal($row["YearGraduated"]).'");
                        $("#page1_voc_row'.$voc.'_col7").html("'.dispVal($row["Honors"]).'");
                     ';
                  }
                  if ($EmpEducVoca_split) {
                     $sql = "WHERE LevelType = 3 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC, RefId DESC LIMIT ".$pds["rowVocational"].", ".($rowCount_EmpEducVoca - $pds["rowVocational"]);
                     $rsEmpEducVoca = SelectEach($dbtable,$sql);
                     $voc = 0;
                     while ($row = mysqli_fetch_assoc($rsEmpEducVoca)) {
                        $voc++;
                        $vocational_dateto = $row["DateTo"];
                        //if ($vocational_dateto == "2999-12-31" || $row["Present"] == 1) $vocational_dateto = "PRESENT";
                        if ($vocational_dateto == 9999) {
                           $vocational_dateto = "ON GOING";
                        }
                        echo '
                           $("#page1_1_voc_row'.$voc.'_col1").html("'.dispVal(getRecord("schools",$row["SchoolsRefId"],"Name")).'");
                           $("#page1_1_voc_row'.$voc.'_col2").html("'.dispVal(getRecord("course",$row["CourseRefId"],"Name")).'");
                           $("#page1_1_voc_row'.$voc.'_col3").html("'.dispVal($row["DateFrom"]).'");
                           $("#page1_1_voc_row'.$voc.'_col4").html("'.dispVal($vocational_dateto).'");
                           $("#page1_1_voc_row'.$voc.'_col5").html("'.dispVal($row["HighestGrade"]).'");
                           $("#page1_1_voc_row'.$voc.'_col6").html("'.dispVal($row["YearGraduated"]).'");
                           $("#page1_1_voc_row'.$voc.'_col7").html("'.dispVal($row["Honors"]).'");
                        ';
                     }
                  } 
               }

               echo '$(".COLLEGE").html("N/A");';
               if ($rsEmpEducColl) {
                  $dbtable = "employeeseduc";
                  $sql = "WHERE LevelType = 4 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC, RefId DESC";
                  if ($EmpEducColl_split) {
                     $sql = "WHERE LevelType = 4 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC, RefId DESC LIMIT 0,".$pds["rowCollege"];
                  } 
                  $rsEmpEducColl = SelectEach($dbtable,$sql);
                  $coll = 0;
                  while ($row = mysqli_fetch_assoc($rsEmpEducColl)) {
                     $coll++;
                     $college_dateto = $row["DateTo"];
                     //if ($college_dateto == "2999-12-31" || $row["Present"] == 1) $college_dateto = "PRESENT";
                     if ($college_dateto == 9999) {
                        $college_dateto = "ON GOING";
                     }
                     echo '
                        $("#page1_coll_row'.$coll.'_col1").html("'.dispVal(getRecord("schools",$row["SchoolsRefId"],"Name")).'");
                        $("#page1_coll_row'.$coll.'_col2").html("'.dispVal(getRecord("course",$row["CourseRefId"],"Name")).'");
                        $("#page1_coll_row'.$coll.'_col3").html("'.dispVal($row["DateFrom"]).'");
                        $("#page1_coll_row'.$coll.'_col4").html("'.dispVal($college_dateto).'");
                        $("#page1_coll_row'.$coll.'_col5").html("'.dispVal($row["HighestGrade"]).'");
                        $("#page1_coll_row'.$coll.'_col6").html("'.dispVal($row["YearGraduated"]).'");
                        $("#page1_coll_row'.$coll.'_col7").html("'.dispVal($row["Honors"]).'");
                     ';
                  }
                  if ($EmpEducColl_split) {
                     $sql = "WHERE LevelType = 4 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC, RefId DESC LIMIT ".$pds["rowCollege"].", ".($rowCount_EmpEducColl - $pds["rowCollege"]);
                     $rsEmpEducColl = SelectEach($dbtable,$sql);
                     $coll = 0;
                     while ($row = mysqli_fetch_assoc($rsEmpEducColl)) {
                        $coll++;
                        $college_dateto = $row["DateTo"];
                        //if ($college_dateto == "2999-12-31" || $row["Present"] == 1) $college_dateto = "PRESENT";
                        if ($college_dateto == 9999) {
                           $college_dateto = "ON GOING";
                        }
                        echo '
                           $("#page1_1_coll_row'.$coll.'_col1").html("'.dispVal(getRecord("schools",$row["SchoolsRefId"],"Name")).'");
                           $("#page1_1_coll_row'.$coll.'_col2").html("'.dispVal(getRecord("course",$row["CourseRefId"],"Name")).'");
                           $("#page1_1_coll_row'.$coll.'_col3").html("'.dispVal($row["DateFrom"]).'");
                           $("#page1_1_coll_row'.$coll.'_col4").html("'.dispVal($college_dateto).'");
                           $("#page1_1_coll_row'.$coll.'_col5").html("'.dispVal($row["HighestGrade"]).'");
                           $("#page1_1_coll_row'.$coll.'_col6").html("'.dispVal($row["YearGraduated"]).'");
                           $("#page1_1_coll_row'.$coll.'_col7").html("'.dispVal($row["Honors"]).'");
                        ';
                     }
                  }
               }
               echo '$(".GRADUATE").html("N/A");';
               if ($rsEmpEducGrad) {
                  $dbtable = "employeeseduc";
                  $sql = "WHERE LevelType = 5 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC, RefId DESC";
                  if ($EmpEducGrad_split) {
                     $sql = "WHERE LevelType = 5 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC, RefId DESC LIMIT 0,".$pds["rowGradStud"];
                  } 
                  $rsEmpEducGrad = SelectEach($dbtable,$sql);
                  $grad = 0;
                  while ($row = mysqli_fetch_assoc($rsEmpEducGrad)) {
                     $grad++;
                     $graduate_dateto = $row["DateTo"];
                     //if ($graduate_dateto == "2999-12-31" || $row["Present"] == 1) $graduate_dateto = "PRESENT";
                     if ($graduate_dateto == 9999) {
                        $graduate_dateto = "ON GOING";
                     }
                     echo '
                        $("#page1_grad_row'.$grad.'_col1").html("'.dispVal(getRecord("schools",$row["SchoolsRefId"],"Name")).'");
                        $("#page1_grad_row'.$grad.'_col2").html("'.dispVal(getRecord("course",$row["CourseRefId"],"Name")).'");
                        $("#page1_grad_row'.$grad.'_col3").html("'.dispVal($row["DateFrom"]).'");
                        $("#page1_grad_row'.$grad.'_col4").html("'.dispVal($graduate_dateto).'");
                        $("#page1_grad_row'.$grad.'_col5").html("'.dispVal($row["HighestGrade"]).'");
                        $("#page1_grad_row'.$grad.'_col6").html("'.dispVal($row["YearGraduated"]).'");
                        $("#page1_grad_row'.$grad.'_col7").html("'.dispVal($row["Honors"]).'");
                     ';
                  }
                  if ($EmpEducGrad_split) {
                     $sql = "WHERE LevelType = 5 AND EmployeesRefId = $empRefId ORDER BY DateFrom DESC, RefId DESC LIMIT ".$pds["rowCollege"].", ".($rowCount_EmpEducGrad - $pds["rowGradStud"]);
                     $rsEmpEducGrad = SelectEach($dbtable,$sql);
                     $grad = 0;
                     while ($row = mysqli_fetch_assoc($rsEmpEducGrad)) {
                        $grad++;
                        $graduate_dateto = $row["DateTo"];
                        //if ($graduate_dateto == "2999-12-31" || $row["Present"] == 1) $graduate_dateto = "PRESENT";
                        if ($graduate_dateto == 9999) {
                           $graduate_dateto = "ON GOING";
                        }
                        echo '
                           $("#page1_1_grad_row'.$grad.'_col1").html("'.dispVal(getRecord("schools",$row["SchoolsRefId"],"Name")).'");
                           $("#page1_1_grad_row'.$grad.'_col2").html("'.dispVal(getRecord("course",$row["CourseRefId"],"Name")).'");
                           $("#page1_1_grad_row'.$grad.'_col3").html("'.dispVal($row["DateFrom"]).'");
                           $("#page1_1_grad_row'.$grad.'_col4").html("'.dispVal($graduate_dateto).'");
                           $("#page1_1_grad_row'.$grad.'_col5").html("'.dispVal($row["HighestGrade"]).'");
                           $("#page1_1_grad_row'.$grad.'_col6").html("'.dispVal($row["YearGraduated"]).'");
                           $("#page1_1_grad_row'.$grad.'_col7").html("'.dispVal($row["Honors"]).'");
                        ';
                     }
                  }
               }
               */

               // Eligibility
               echo '$(".ELIGIBILITY").html("N/A");'."\n";
               if ($rsEmpEligibility) {
                  $dbtable = "employeeselegibility";
                  if ($EmpEligibility_split) {
                     $rsEmpEligibility = SelectEach($dbtable,"WHERE EmployeesRefId = $empRefId ORDER BY ExamDate DESC, RefId DESC LIMIT 0,".$pds["rowEligibility"]);
                  } else {
                     $rsEmpEligibility = SelectEach($dbtable,"WHERE EmployeesRefId = $empRefId ORDER BY ExamDate DESC, RefId DESC");
                  }
                  $j = 0;
                  while ($row = mysqli_fetch_array($rsEmpEligibility)) {
                     $j++;
                     echo '
                     $("#page2_A'.($j + 3).'").html("'.dispVal(getRecord("CareerService",$row["CareerServiceRefId"],"Name")).'");
                     $("#page2_F'.($j + 3).'").html("'.dispVal($row["Rating"]).'");
                     $("#page2_G'.($j + 3).'").html("'.dispVal($row["ExamDate"]).'");
                     $("#page2_I'.($j + 3).'").html("'.dispVal($row["ExamPlace"]).'");
                     $("#page2_L'.($j + 3).'").html("'.dispVal(strtoupper($row["LicenseNo"])).'");
                     $("#page2_M'.($j + 3).'").html("'.dispVal($row["LicenseReleasedDate"]).'");
                     '."\n";
                  }
                  if ($EmpEligibility_split) {
                     $sql = "WHERE EmployeesRefId = $empRefId ORDER BY ExamDate DESC, RefId DESC LIMIT ".$pds["rowEligibility"].",".($rowCount_EmpEligibility - $pds["rowEligibility"]);
                     $rsEmpEligibility = SelectEach($dbtable,$sql);
                     $j = 0;
                     while ($row = mysqli_fetch_array($rsEmpEligibility)) {
                        $j++;
                        echo '
                        $("#page2_1_A'.($j + 3).'").html("'.dispVal(getRecord("CareerService",$row["CareerServiceRefId"],"Name")).'");
                        $("#page2_1_F'.($j + 3).'").html("'.dispVal($row["Rating"]).'");
                        $("#page2_1_G'.($j + 3).'").html("'.dispVal($row["ExamDate"]).'");
                        $("#page2_1_I'.($j + 3).'").html("'.dispVal($row["ExamPlace"]).'");
                        $("#page2_1_L'.($j + 3).'").html("'.dispVal(strtoupper($row["LicenseNo"])).'");
                        $("#page2_1_M'.($j + 3).'").html("'.dispVal($row["LicenseReleasedDate"]).'");
                        '."\n";
                     }
                  }
               }

               // Work Experience
               echo '$(".WORKEXPERIENCE").html("N/A");'."\n";
               if ($rsEmpWorkExp) {
                  $dbtable = "employeesworkexperience";
                  if ($EmpVoluntary_split) {
                     $rsEmpWorkExp = SelectEach($dbtable,"WHERE EmployeesRefId = $empRefId AND (isServiceRecord = 0 OR isServiceRecord IS NULL) ORDER BY WorkStartDate DESC, RefId DESC LIMIT 0, ".$pds["rowWorkExperience"]);
                  } else {
                     $rsEmpWorkExp = SelectEach($dbtable,"WHERE EmployeesRefId = $empRefId AND (isServiceRecord = 0 OR isServiceRecord IS NULL) ORDER BY WorkStartDate DESC, RefId DESC");
                  }
                  $j = 0;
                  while ($row = mysqli_fetch_array($rsEmpWorkExp)) {
                     $j++;
                     if (is_numeric($row["SalaryAmount"])) {
                        $salary = number_format($row["SalaryAmount"],2);
                     } else {
                        $salary = $row["SalaryAmount"];
                     }
                     if ($row["Present"] == 1) {
                        $work_end_date = "PRESENT";
                     } else {
                        $work_end_date = $row["WorkEndDate"];
                     }
                     echo '
                     $("#page2_O'.($j + 5).'").html("'.dispVal($row["WorkStartDate"]).'");
                     $("#page2_Q'.($j + 5).'").html("'.dispVal($work_end_date).'");
                     $("#page2_R'.($j + 5).'").html("'.dispVal(getRecord("Position",$row["PositionRefId"],"Name")).'");
                     $("#page2_U'.($j + 5).'").html("'.dispVal(getRecord("Agency",$row["AgencyRefId"],"Name")).'");
                     $("#page2_X'.($j + 5).'").html("'.dispVal($salary).'");
                     $("#page2_Z'.($j + 5).'").html("'.dispVal(getRecord("EmpStatus",$row["EmpStatusRefId"],"Name")).'");
                     '."\n";

                     $disp = "";
                     if ($row["SalaryGradeRefId"] > 0) {
                        $disp = dispVal(getRecord("SalaryGrade",$row["SalaryGradeRefId"],"Name"))." - ".dispVal(getRecord("StepIncrement",$row["StepIncrementRefId"],"Name"));
                        echo '$("#page2_Y'.($j + 5).'").html("'.$disp.'");'."\n";
                     } else {
                        $disp = dispVal(getRecord("JobGrade",$row["JobGradeRefId"],"Name"))." - ".dispVal(getRecord("StepIncrement",$row["StepIncrementRefId"],"Name"));
                        echo '$("#page2_Y'.($j + 5).'").html("'.$disp.'");'."\n";
                     }

                     if ($row["isGovtService"])
                        echo '$("#page2_AA'.($j + 5).'").html("YES");'."\n";
                     else
                        echo '$("#page2_AA'.($j + 5).'").html("NO");'."\n";
                  }
                  if ($EmpWorkExp_split) {
                     $sql = "WHERE EmployeesRefId = $empRefId AND (isServiceRecord = 0 OR isServiceRecord IS NULL) ORDER BY WorkStartDate DESC, RefId DESC LIMIT ".$pds["rowWorkExperience"].",".($rowCount_EmpWorkExp - $pds["rowWorkExperience"]);
                     $rsEmpWorkExp = SelectEach($dbtable,$sql);
                     $j = 0;
                     while ($row = mysqli_fetch_array($rsEmpWorkExp)) {
                        $j++;
                        if (is_numeric($row["SalaryAmount"])) {
                           $salary = number_format($row["SalaryAmount"],2);
                        } else {
                           $salary = $row["SalaryAmount"];
                        }
                        if ($row["Present"] == 1) {
                           $work_end_date = "PRESENT";
                        } else {
                           $work_end_date = $row["WorkEndDate"];
                        }
                        echo '
                        $("#page2_1_O'.($j + 5).'").html("'.dispVal($row["WorkStartDate"]).'");
                        $("#page2_1_Q'.($j + 5).'").html("'.dispVal($work_end_date).'");
                        $("#page2_1_R'.($j + 5).'").html("'.dispVal(getRecord("Position",$row["PositionRefId"],"Name")).'");
                        $("#page2_1_U'.($j + 5).'").html("'.dispVal(getRecord("Agency",$row["AgencyRefId"],"Name")).'");
                        $("#page2_1_X'.($j + 5).'").html("'.dispVal($salary).'");
                        $("#page2_1_Z'.($j + 5).'").html("'.dispVal(getRecord("EmpStatus",$row["EmpStatusRefId"],"Name")).'");
                        '."\n";

                        $disp = "";
                        if ($row["SalaryGradeRefId"] > 0) {
                           $disp = dispVal(getRecord("SalaryGrade",$row["SalaryGradeRefId"],"Name"))." - ".dispVal(getRecord("StepIncrement",$row["StepIncrementRefId"],"Name"));
                           echo '$("#page2_1_Y'.($j + 5).'").html("'.$disp.'");'."\n";
                        } else {
                           $disp = dispVal(getRecord("JobGrade",$row["JobGradeRefId"],"Name"))." - ".dispVal(getRecord("StepIncrement",$row["StepIncrementRefId"],"Name"));
                           echo '$("#page2_1_Y'.($j + 5).'").html("'.$disp.'");'."\n";
                        }

                        if ($row["isGovtService"])
                           echo '$("#page2_1_AA'.($j + 5).'").html("YES");'."\n";
                        else
                           echo '$("#page2_1_AA'.($j + 5).'").html("NO");'."\n";
                     }
                  }
               }

               // VOLUNTARY
               echo '$(".VOLUNTARY").html("N/A");'."\n";
               if ($rsEmpVoluntary) {
                  $dbtable = "employeesvoluntary";
                  if ($EmpVoluntary_split) {
                     $rsEmpVoluntary = SelectEach($dbtable,"WHERE EmployeesRefId = $empRefId ORDER BY StartDate DESC, RefId DESC LIMIT 0, ".$pds["rowVoluntary"]);
                  } else {
                     $rsEmpVoluntary = SelectEach($dbtable,"WHERE EmployeesRefId = $empRefId ORDER BY StartDate DESC, RefId DESC");
                  }
                  $j = 0;
                  while ($row = mysqli_fetch_array($rsEmpVoluntary)) {
                     $j++;
                     $vol_date_to = $row["EndDate"];
                     if ($vol_date_to == "2999-12-31" || $row["Present"] == 1) {
                         $vol_date_to = "PRESENT";
                     }
                     echo '
                     $("#page3_A'.($j + 4).'").html("'.dispVal(getRecord("Organization",$row["OrganizationRefId"],"Name")).'");
                     $("#page3_E'.($j + 4).'").html("'.dispVal($row["StartDate"]).'");
                     $("#page3_F'.($j + 4).'").html("'.dispVal($vol_date_to).'");
                     $("#page3_G'.($j + 4).'").html("'.dispVal($row["NumofHrs"]).'");
                     $("#page3_H'.($j + 4).'").html("'.dispVal($row["WorksNature"]).'");';
                  }
                  if ($EmpVoluntary_split) {
                     $sql = "WHERE EmployeesRefId = $empRefId ORDER BY StartDate DESC, RefId DESC LIMIT ".$pds["rowVoluntary"].", ".($rowCount_EmpVoluntary - $pds["rowVoluntary"]);
                     $rsEmpVoluntary = SelectEach($dbtable,$sql);
                     $j = 0;
                     while ($row = mysqli_fetch_array($rsEmpVoluntary)) {
                        $j++;
                        $vol_date_to = $row["EndDate"];
                        if ($vol_date_to == "2999-12-31" || $row["Present"] == 1) {
                           $vol_date_to = "PRESENT";
                        }
                        echo '
                        $("#page3_1_A'.($j + 4).'").html("'.dispVal(getRecord("Organization",$row["OrganizationRefId"],"Name")).'");
                        $("#page3_1_E'.($j + 4).'").html("'.dispVal($row["StartDate"]).'");
                        $("#page3_1_F'.($j + 4).'").html("'.dispVal($vol_date_to).'");
                        $("#page3_1_G'.($j + 4).'").html("'.dispVal($row["NumofHrs"]).'");
                        $("#page3_1_H'.($j + 4).'").html("'.dispVal($row["WorksNature"]).'");'."\n";
                     }
                  }
               }

               echo '$(".TRAININGPROG").html("N/A");'."\n";
               if ($rsEmpTraining) {
                  $dbtable = "employeestraining";
                  if ($EmpTraining_split) {
                     $rsEmpTraining = SelectEach($dbtable,"WHERE EmployeesRefId = $empRefId ORDER BY StartDate DESC, RefId DESC LIMIT 0,".$pds["rowLearning"]);
                  } else {
                     $rsEmpTraining = SelectEach($dbtable,"WHERE EmployeesRefId = $empRefId ORDER BY StartDate DESC, RefId DESC");
                  }
                  $j = 0;
                  while ($row = mysqli_fetch_array($rsEmpTraining)) {
                     $j++;
                     $string3_L = trim(preg_replace('/\s+/', ' ',dispVal(getRecord("Seminars",$row["SeminarsRefId"],"Name"))));
                     $string3_U = trim(preg_replace('/\s+/', ' ',dispVal(getRecord("Sponsor",$row["SponsorRefId"],"Name"))));
                     $tp_date_to = $row["EndDate"];
                     if ($tp_date_to == "2999-12-31" || $row["Present"] == 1) {
                         $tp_date_to = "PRESENT";
                      }
                     echo '
                     $("#page3_L'.($j + 6).'").html("'.dispVal($string3_L).'");
                     $("#page3_Q'.($j + 6).'").html("'.dispVal($row["StartDate"]).'");
                     $("#page3_R'.($j + 6).'").html("'.dispVal($tp_date_to).'");
                     $("#page3_S'.($j + 6).'").html("'.dispVal($row["NumofHrs"]).'");
                     $("#page3_T'.($j + 6).'").html("'.dispVal(getRecord("seminartype",$row["SeminarTypeRefId"],'Name')).'");
                     $("#page3_U'.($j + 6).'").html("'.dispVal($string3_U).'");
                     '."\n";
                  }
                  if ($EmpTraining_split) {
                     $sql = "WHERE EmployeesRefId = $empRefId ORDER BY StartDate DESC, RefId DESC LIMIT ".$pds["rowLearning"].", ".($rowCount_EmpTraining - $pds["rowLearning"]);
                     $rsEmpTraining = SelectEach($dbtable,$sql);
                     $j = 0;
                     while ($row = mysqli_fetch_array($rsEmpTraining)) {
                        $j++;
                        $string3_L = trim(preg_replace('/\s+/', ' ',dispVal(getRecord("Seminars",$row["SeminarsRefId"],"Name"))));
                        $string3_U = trim(preg_replace('/\s+/', ' ',dispVal(getRecord("Sponsor",$row["SponsorRefId"],"Name"))));
                        $tp_date_to = $row["EndDate"];
                        if ($tp_date_to == "2999-12-31" || $row["Present"] == 1) {
                           $tp_date_to = "PRESENT";
                        }
                        echo '
                        $("#page3_1_L'.($j + 6).'").html("'.dispVal($string3_L).'");
                        $("#page3_1_Q'.($j + 6).'").html("'.dispVal($row["StartDate"]).'");
                        $("#page3_1_R'.($j + 6).'").html("'.dispVal($tp_date_to).'");
                        $("#page3_1_S'.($j + 6).'").html("'.dispVal($row["NumofHrs"]).'");
                        $("#page3_1_T'.($j + 6).'").html("'.dispVal(getRecord("seminarclass",$row["SeminarClassRefId"],'Name')).'");
                        $("#page3_1_U'.($j + 6).'").html("'.dispVal($string3_U).'");
                        '."\n";
                     }
                  }
               }

               echo '$(".OTHERINFO").html("N/A");'."\n";
               if ($rsEmpOtherInfo) {
                  $dbtable = "employeesotherinfo";
                  if ($EmpOtherInfo_split) {
                     $rsEmpOtherInfo = SelectEach($dbtable,"WHERE EmployeesRefId = $empRefId ORDER BY RefId LIMIT 0,".$pds["rowOtherInfo"]);
                  } else {
                     $rsEmpOtherInfo = SelectEach($dbtable,"WHERE EmployeesRefId = $empRefId ORDER BY RefId");
                  }
                  $j = 0;
                  while ($row = mysqli_fetch_array($rsEmpOtherInfo)) {
                     $j++;
                     echo '
                     $("#page3_X'.($j + 6).'").html("'.dispVal($row["Skills"]).'");
                     $("#page3_Y'.($j + 6).'").html("'.dispVal($row["Recognition"]).'");
                     $("#page3_Z'.($j + 6).'").html("'.dispVal($row["Affiliates"]).'");';
                  }
                  if ($EmpOtherInfo_split) {
                     $rsEmpOtherInfo = SelectEach($dbtable,"WHERE EmployeesRefId = $empRefId ORDER BY RefId LIMIT ".$pds["rowOtherInfo"].", ".($rowCount_EmpOtherInfo - $pds["rowOtherInfo"]));      
                     $j = 0;
                     while ($row = mysqli_fetch_array($rsEmpOtherInfo)) {
                        $j++;
                        echo '
                        $("#page3_1_X'.($j + 6).'").html("'.dispVal($row["Skills"]).'");
                        $("#page3_1_Y'.($j + 6).'").html("'.dispVal($row["Recognition"]).'");
                        $("#page3_1_Z'.($j + 6).'").html("'.dispVal($row["Affiliates"]).'");';
                     }
                  }
               }

               echo '$(".REFERENCES").html("N/A");'."\n";
               if ($rsEmpReference) {
                  $j = 0;
                  while ($row = mysqli_fetch_array($rsEmpReference)) {
                     $j++;
                     echo '
                     $("#page4_A'.($j + 53).'").html("'.dispVal($row["Name"]).'");
                     $("#page4_E'.($j + 53).'").html("'.dispVal($row["Address"]).'");
                     $("#page4_F'.($j + 53).'").html("'.dispVal($row["ContactNo"]).'");'."\n";
                  }
               }
            echo '});';
         ?>
      </script>
   </body>
</html>

