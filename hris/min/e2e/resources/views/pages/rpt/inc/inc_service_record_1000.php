<div style="page-break-after: always;">
   <table style="width: 100%;">
      <?php
         include 'header_service_record_1000.php';
      ?>
      <tbody>
         <?php
            if ($emp_movement) {
               while ($emp_movement_row = mysqli_fetch_assoc($emp_movement)) {
                  $count++;
         ?>
            <tr>
               <td class="text-center" valign="top" style="height: 65px;">
                  <?php 
                     if ($emp_movement_row["EffectivityDate"] != "") {
                        echo rptDefaultValue(date("m/d/Y",strtotime($emp_movement_row["EffectivityDate"])));   
                     }
                  ?>
               </td>
               <td class="text-center" valign="top">
                  <?php 
                     if ($emp_movement_row["ExpiryDate"] != "") {
                        echo rptDefaultValue(date("m/d/Y",strtotime($emp_movement_row["ExpiryDate"]))); 
                     } else if ($emp_movement_row["ExpiryDate"] == "") {
                        echo "PRESENT";
                     }
                  ?>
               </td>
               <td valign="top">
                  <?php echo rptDefaultValue($emp_movement_row["PositionRefId"],"position"); ?>
               </td>
               <td class="text-center" valign="top">
                  <?php echo rptDefaultValue($emp_movement_row["EmpStatusRefId"],"empstatus"); ?>
               </td>
               <td class="text-right" valign="top">
                  <?php
                     $SalaryAmount = intval($emp_movement_row["SalaryAmount"] * 12);
                     if ($SalaryAmount != "") {
                        echo "P ".number_format($SalaryAmount,2);
                     } else {
                        echo "P ".number_format(0,2);
                     }
                  ?>
               </td>
               <td valign="top">
                  <?php echo rptDefaultValue($emp_movement_row["AgencyRefId"],"agency"); ?>
               </td>
               <td class="text-center" valign="top">
                  <?php
                     $LWOP = intval($emp_movement_row["LWOP"]);
                     if ($LWOP > 0) {
                        echo $LWOP;   
                     }
                  ?>
               </td>
               <td valign="top">
                  <?php 
                     if ($emp_movement_row["Remarks"] != "") {
                        echo rptDefaultValue($emp_movement_row["Remarks"]);    
                     } else {
                        echo rptDefaultValue($emp_movement_row["Cause"]); 
                     }
                  ?>
               </td>
            </tr>

         <?php
               }
               echo '
                  <tr>
                     <td class="text-center" colspan="8"><b>* * *NOTHING FOLLOWS* * *</b></td>
                  </tr>
               ';
            } else {
               echo '
                  <tr>
                     <td class="text-center" colspan="8"><b>* * *NO SERVICE RECORD* * *</b></td>
                  </tr>
               ';
            }
         ?>
      </tbody>
   </table>
   <div class="row margin-top">
      <div class="col-xs-12">
         <br>
         Issued in compliance with Executive Order No. 54 dated August 10, 1954, and in accordance with Circular No. 58, dated August 10,1954 of the System.
      </div>
   </div>
   <br><br>
   <div class="row margin-top">
      <div class="col-xs-6">
         DATE: <b><?php echo date("m/d/Y",time()); ?></b>
      </div>
      <div class="col-xs-3">
         CERTIFIED CORRECT:
      </div>
      <div class="col-xs-3 text-center">
         <u>
            <?php echo $Signatory_FullName; ?>
         </u>
         <br>
         <?php echo $Signatory_Position; ?>
      </div>
   </div>
</div>