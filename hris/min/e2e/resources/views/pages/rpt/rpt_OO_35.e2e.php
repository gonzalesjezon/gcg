<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid = getvalue("refid");
   $row = FindFirst("employeesauthority","WHERE RefId = $refid","*");
   if ($row) {
         $Signatory1       = $row["Signatory1"];
         $Signatory2       = $row["Signatory2"];
         $Signatory3       = $row["Signatory3"];

         $s1_row           = FindFirst("signatories","WHERE RefId = '$Signatory1'","*");
         $s2_row           = FindFirst("signatories","WHERE RefId = '$Signatory2'","*");

         $s1_name          = $s1_row["Name"];
         $s1_position      = getRecord("position",$s1_row["PositionRefId"],"Name");

         $s2_name          = $s2_row["Name"];
         $s2_position      = getRecord("position",$s2_row["PositionRefId"],"Name");


   		$authority 		= getRecord("absences",$row["AbsencesRefId"],"Name");
   		$authority 		= strtoupper($authority);
   		$Purpose 		= $row["Remarks"];
   		$Remarks 		= $authority." - ".$row["Remarks"];
   		$Destination 	= $row["Destination"];
   		$emprefid 		= $row["EmployeesRefId"];
   		$FiledDate 		= $row["FiledDate"];
   		$TO_no			= "OO-".date("Y",strtotime($FiledDate))."".date("m",strtotime($FiledDate))."-".str_pad($refid, 3, '0',STR_PAD_LEFT);
   		$employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   		if ($employees) {
   			$FirstName = $employees["FirstName"];
   			$LastName = $employees["LastName"];
   			$MiddleName = $employees["MiddleName"];
   			$ExtName = $employees["ExtName"];
   			$FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
   		} else {
   			$FullName = "&nbsp;";
   		}
   		$empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   		if ($empinformation) {
   			$OfficeRefId = getRecord("office",$empinformation["OfficeRefId"],"Name");
   			$DivisionRefId = getRecord("division",$empinformation["DivisionRefId"],"Name");
   			$PositionRefId = getRecord("position",$empinformation["PositionRefId"],"Name");
   		} else {
   			$OfficeRefId = $DivisionRefId = $PositionRefId = "&nbsp;";
   		}
   		if ($row["Whole"] == 1) {
   			$start_time = $end_time = "Whole Day";
   		} else {
   			$start_time = convertToHoursMins($row["FromTime"]);
   			$end_time = convertToHoursMins($row["ToTime"]);
   		}
   		$from = $row["ApplicationDateFrom"];
   		$to = $row["ApplicationDateTo"];
   		if ($from == $to) {
   			$date = date("F d, Y",strtotime($from));
   		} else {
   			$date = date("F d, Y",strtotime($from))." to ".date("F d, Y",strtotime($to));;
   		}   		
   }
	$user      = getvalue("hUserRefId");
	$user_row  = FindFirst("employees","WHERE RefId = '$user'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
	if ($user_row) {
      $user_LastName   = $user_row["LastName"];
      $user_FirstName  = $user_row["FirstName"];
	} else {
      $user_FirstName = $user_LastName = "";
	}
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td, th {
			border: 2px solid black;
			vertical-align: top;
			padding: 5px;
			font-size: 9pt;
		}
      th {text-align: center;}
		.data {
			font-size: 10pt;
			text-transform: uppercase;
		}
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div style="page-break-after: always;">
	        <?php
	            rptHeader("Office Order","HRDMS-T-003");
	        ?>
	        <div class="row margin-top">
	        	<div class="col-xs-12">
	        		<b>OFFICE ORDER NO: <?php echo $TO_no; ?></b>
               <br>
               Series of <?php echo date("Y",strtotime($FiledDate)); ?>
               <br>
               <br>
               This office order authorizes the following personnel to attend/participate/conduct the specified activity
               <br>
               <br>
               <b>WHAT </b>
               <u><?php echo $Purpose; ?></u>
               <br>
               <br>
               <b>NATURE OF ACTIVITY </b>________________________________
               <br>
               <br>
               <b>SPONSORED/CONDUCTED/REQUESTED BY: </b>________________________________
               
	        	</div>
	        </div>
	        <br>
	        <div class="row margin-top">
	        	<div class="col-xs-12">
	        		<table width="100%" border="1">
                  <thead>
                     <tr>
                        <th style="width: 50%;">NAME OF PERSONNEL</th>
                        <th style="width: 50%;">
                           PURPOSE OF ATTENDANCE
                           <br>
                           <i>
                              (specify if participant, resource speaker, observer, facilitator, secretariat,documenter, TA provider, coordinator, etc)
                           </i>
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>
                           <?php echo $FullName; ?>
                        </td>
                        <td>
                           
                        </td>
                     </tr>
                  </tbody>
               </table>
	        	</div>
	        </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <b>WHEN:</b> _________________________________________________
                  <br>
                  <b>WHERE:</b> _________________________________________________
                  <br>
                  <b>TIME:</b> _________________________________________________
               </div>
            </div>
            <br>
            <br>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <p>
                     This further allows the above-named personnel to charge the following expense/s, subject to the availability of ______________ funds and usual accounting and auditing requirements:
                  </p>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-1">
               </div>
               <div class="col-xs-11">
                  <input type="checkbox" name="">&nbsp;Registration fee (Amount _________)
                  <br>
                  <input type="checkbox" name="">&nbsp;Transportation expense/allowance
                  <br>
                  <input type="checkbox" name="">&nbsp;Representation allowance
                  <br>
                  <input type="checkbox" name="">&nbsp;Others (please specify e.g. Food allowance, etc)
               </div>
            </div>
            <br>
            <br>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <p>All Office Orders previously issued which are inconsistent with the above are deemed superseded by this Order.</p>
               </div>
            </div>
            <br>
            <br>
	        <div class="row margin-top">
	        	<div class="col-xs-6">
	        		Recommended by:
	        		<br><br><br>
	        		<b>
                  <?php echo $s1_name; ?>
               </b>
               <br>
               <?php echo $s1_position; ?>
	        	</div>
	        	<div class="col-xs-6">
	        		Approved by:
	        		<br><br><br>
	        		<b>
                  <?php echo $s2_name; ?>
               </b>
               <br>
               <?php echo $s2_position; ?>
	        	</div>
	        </div>

	        <?php spacer(40); ?>
         	<div class="row margin-top">
            	<div class="col-xs-1"></div>
            	<div class="col-xs-10" style="border: 1px solid black; padding: 5px;">
               		The only CONTROLLED copy of this document is the online version maintained in the HRIS. The user must ensure that this or any other copy of a controlled document is current and complete prior to use. The MASTER copy of this document is with the AFD-HRMD Section. This document is UNCONTROLLED when downloaded and printed.
	               	<br>
	               	<br>
	               	Printed on <?php echo date("F d,Y",time()); ?>
	               	<br>
	               	Printed by <?php echo $user_LastName.", ".$user_FirstName; ?>
            	</div>
         	</div>
	    </div>
    </div>
</body>
</html>