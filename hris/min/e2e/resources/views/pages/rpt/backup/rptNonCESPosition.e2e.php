<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u> </p>

         <table border="1">
            <tr>
               <th class="padd5" rowspan=2 style="width:*%">AGENCY/POSITION</th>
               <th class="padd5" rowspan=2 style="width:10%">SALARY GRADE</th>
               <th class="padd5" colspan=4 style="width:40%">APPOINTEE</th>
               <th class="padd5" colspan=2 style="width:20%">OCCUPANT</th>
            </tr>
            <tr>
               <th class="padd5" >Name</th>
               <th class="padd5" >Status of Appointment</th>
               <th class="padd5" >Date of Effectivity</th>
               <th class="padd5" >Present Assignment</th>
               <th class="padd5" >Name</th>
               <th class="padd5" >Position Per Appointment Issued</th>
            </tr>
            <?php for ($j=1;$j<=15;$j++) {?>
               <tr>
                  <td>&nbsp;</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
               <tr>
            <?php } ?>
         </table>
         <p> * Not Covered in the Career Executive Services per Letter of Exec. Director D. Juiridico date October 10, 1995 and per CSC Resolution No. 08-1213 dated June 30, 2008</p>
         <p> ** All appointments are made by the head of the agency</p>
         <br><br>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Certified correct / Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>