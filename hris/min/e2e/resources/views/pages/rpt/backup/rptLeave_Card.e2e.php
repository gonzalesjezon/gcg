<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <div class="row" style="border:1px solid black;padding:5px;">
            Generate Individual Leave Ledger
            <br>
            Quarter / Year 1st Quarter / 2017
         </div>
         <?php spacer(20);?>
         <table border="1" width="100%">
            <tr align="center">
               <td rowspan="2">Period Covered</td>
               <td rowspan="2">Particular</td>
               <td colspan="4">Vacation Leave</td>
               <td colspan="4">Sick Leave</td>
               <td rowspan="2">Remarks</td>
            </tr>
            <tr align="center">
               <td>Earned</td>
               <td>Absences Undertime With Pay</td>
               <td>Balance</td>
               <td>Absences Undertime W/O Pay</td>
               <td>Earned</td>
               <td>Absences Undertime With Pay</td>
               <td>Balance</td>
               <td>Absences Undertime W/O Pay</td>
              
            </tr>
            <?php for($i = 1;$i <11;$i++) {?>
               <tr>
                  <td>&nbsp;</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
               </tr>
            <?php }?>
         </table>
      </div>
      <?php rptFooter(); ?>
   </body>
</html>