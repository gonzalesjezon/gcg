<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName LIMIT 10";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            #Content {
               font-size: 12pt;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
               while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                  $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                  $emp_info   = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                  //if ($emp_info) {
                     $date_hired = $emp_info["HiredDate"];
                     if ($date_hired != "") {
                        $date_hired = date("m/d/Y",strtotime($date_hired));
                     } else {
                        $date_hired = "(NO HIRED DATE)";
                     }
                     $position   = rptDefaultValue($emp_info["PositionRefId"],"position");
         ?>
         <div class="row" style="page-break-after: always;" id="Content">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                     <br><br>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <p>To Whom It May Concern:</p>

                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <p style="text-indent: 5%;">
                        This is to certify that <b><?php echo strtoupper($FullName); ?></b> is an employee of the Metals Industry Research and Development Center (MIRDC) since <b><?php echo $date_hired; ?></b>. He holds a permanent position of <b><?php echo $position; ?></b>.
                     </p>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <p style="text-indent: 5%;">
                        This is to certify further that the names <b>(name)</b> and <b>(name)</b> refer to one and the same person.
                     </p>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <p style="text-indent: 5%;">
                        This certification is being issued for updating (his/her) <b>(purpose)</b>.
                     </p>
                  </div>
               </div>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12 text-right">
                     Certified Correct:
                     <br>
                     <b>JELLY N. ORTIZ, DPA</b>
                     <br>
                     Chief, FAD-AGSS
                     <br>
                     Finance and Administrative Division
                  </div>
               </div>
            </div>
         </div>
         <?php
                 // }
               }
            }
         ?>
      </div>
   </body>
</html>