<?php
   $file = "Request for Authority To Render Overtime Work";
   
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid = getvalue("refid");
   $row = FindFirst("overtime_request","WHERE RefId = '$refid'","*");
   $FiledDate = date("F d, Y",strtotime($row["FiledDate"]));
   if ($row) {
      $emprefid = $row["EmployeesRefId"];
      $emp_row = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
      if ($emp_row) {
         $LastName = $emp_row["LastName"];
         $FirstName = $emp_row["FirstName"];
         $MiddleName = $emp_row["MiddleName"];
         $ExtName = $emp_row["ExtName"];
         $FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
      } else {
         $FullName = "";
      }
      $empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
      if ($empinfo_row) {
         $Office     = getRecord("office",$empinfo_row["OfficeRefId"],"Name");
         $Position   = getRecord("position",$empinfo_row["PositionRefId"],"Name");
         $Division   = getRecord("Division",$empinfo_row["DivisionRefId"],"Name");
      } else {
         $Office     = "";
         $Position   = "";
         $Division   = "";
      }
      $From             = $row["FromTime"];
      $To               = $row["ToTime"];
      $StartDate        = date("F d, Y",strtotime($row["StartDate"]));
      $EndDate          = date("F d, Y",strtotime($row["EndDate"]));
      if ($StartDate != $EndDate) {
         $Inclusive_Date   = $StartDate." to ".$EndDate;
      } else {
         $Inclusive_Date = $StartDate;
      }
      if (intval($From) > 0) {
         $Inclusive_Date .= " ".convertToHoursMins($From)." - ".convertToHoursMins($To);
      }
      $stat = $row["WithPay"];
      if (intval($stat) > 0) {
         $stat = "OT Pay";
      } else {
         $stat = "CTO";
      }
   }
   $user = getvalue("hUserRefId");
   $user_row = FindFirst("employees","WHERE RefId = '$user'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   if ($user_row) {
      $user_LastName   = $user_row["LastName"];
      $user_FirstName  = $user_row["FirstName"];
   } else {
      $user_FirstName = $user_LastName = "";
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php
         include_once 'pageHEAD.e2e.php';
      ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style>
         /*.bottomline {border-bottom:2px solid black;}*/
         .bold {font-size:600;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Request Authority To Render Overtime");
         ?>
         <div class="row">
            <div class="col-xs-12">
               <b>(1)</b> For: <b>Deputy Administrator</b> ___________________________
            </div>
         </div>
         <br>
         <div class="row">
            <div class="col-xs-12">
               Pursuant to Joint DBM-CSC Circular No. 1, series 2015 the (2) <b><?php echo $Division; ?></b> requests the Overtime (OT) services of the following employees
            </div>
         </div>
         <br>
         <div class="row margin-top">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
               <table border="1">
                  <thead>
                     <tr>
                        <th>
                           (3) Name
                        </th>
                        <th>
                           (4) Position/SG
                        </th>
                        <th>
                           (5) Signature (Conforme)
                        </th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>
                           <?php echo $FullName; ?>
                        </td>
                        <td>
                           <?php echo $Position; ?>
                        </td>
                        <td>
                           <?php echo ""; ?>
                        </td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
         <br>
         <div class="row margin-top">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
               <b>(6)Scope of Work:</b>
               <br>
               <table border="1">
                  <?php
                     for ($i=1; $i <= 3 ; $i++) { 
                  ?>
                  <tr>
                     <td>&nbsp;</td>
                  </tr>
                  <?php
                     }
                  ?>
               </table>
            </div>
         </div>
         <br>
         <div class="row margin-top">
            <div class="col-xs-6">
               <b>(7) Inclusive Date/s and Time</b>
               <br>
               <?php echo $Inclusive_Date; ?>
            </div>
            <div class="col-xs-6">
               <b>(8) Place:</b>&nbsp;&nbsp;&nbsp;Office____&nbsp;&nbsp;&nbsp;Field_____
            </div>
         </div>
         <br>
         <div class="row margin-top">
            <div class="col-xs-12">
               <b>(9) <u>DARRELL JOHN S. MAGSAMBOL</u></b>
               <br>
               Head Technical Assistant
            </div>
         </div>
         <br>         
         <div style="border: 2px solid black;">
         </div>
         <br>
         <div class="row margin-top">
            <div class="col-xs-12">
               For: <b>The Chief Regulator</b>
               <br>
               The OT services requested are related to the priority activities under Joint DBM-CSC Circular No. 1, series 2015.
               <br>
               <br>
               <b>(10) ______________________________</b>
               <br>
               Deputy Administrator/ Date
            </div>
         </div>
         <br>         
         <div style="border: 2px solid black;">
         </div>
         <br>
         <div class="row margin-top">
            <div class="col-xs-12 text-center"><b>OCR Action:</b><br></div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               <b>(11)To: Deputy Administrator for Administration and Legal Affairs</b>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
               ___ Please process payment, subject to limitations of overtime pay
               <br>
               ___ Please process CTO
               <br>
               ___ Disapproved
            </div>
         </div>
         <br><br>
         <div class="row margin-top">
            <b>(11) PATRICK LESTER N. TY</b>
            <br>
            Chief Regulator / Date
         </div>
         <?php spacer(70); ?>
         <?php
            rpt_footer(14);
         ?>
      </div>
   </body>
</html>