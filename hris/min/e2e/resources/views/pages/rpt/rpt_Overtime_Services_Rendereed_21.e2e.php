<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName LIMIT 10";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
         <div class="row" style="page-break-after: always;" id="Content">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                     <br><br>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     This is to certify that I have rendered the OT services specified in my approved Authority to Render Overtime Services dated __________________ .
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12 text-center">
                     <b>DETAILS OF OT SERVICES RENDERED</b>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-1"></div>
                  <div class="col-xs-11">
                     OT PERIOD _________________________________
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-1"></div>
                  <div class="col-xs-11">
                     ACCOMPLISHMENTS:
                     <br>
                     _________________________________
                     <br>
                     _________________________________
                     <br>
                     _________________________________
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     Prepared by:
                  </div>
                  <div class="col-xs-6">
                     Certified Correct by:
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     ________________________________
                  </div>
                  <div class="col-xs-6">
                     ________________________________
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     Print Name and Signature
                  </div>
                  <div class="col-xs-6">
                     Division Head
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     Attachment:<i><b>Approved Authority to Render Overtime Services</b></i>
                  </div>
               </div>
            </div>
         </div>
         <?php
            }
         ?>
      </div>
   </body>
</html>