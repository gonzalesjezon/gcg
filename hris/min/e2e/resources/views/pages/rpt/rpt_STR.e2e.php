<?php
	require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $whereClause .= " ORDER BY LastName";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);

   $month = getvalue("txtAttendanceMonth");
   $months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<?php include_once $files["inc"]["pageHEAD"]; ?>
   <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   <style type="text/css">
   	th, td {
   		font-size: 7pt;
   	}
   </style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 text-center">
						<?php rptHeader("SUMMARY TIME REPORT"); ?>
						<br>
						For the Month of <u><?php echo $months[$month - 1]; ?></u>
					</div>
				</div>
				<div class="row margin-top">
					<div class="col-xs-12">
						<table width="100%">
							<thead>
								<tr class="colHEADER">
									<th rowspan="3">Employee Name</th>
									<th rowspan="3">SL<br>DAYS</th>
									<th rowspan="3">VL<br>DAYS</th>
									<th colspan="2">UNDERTIME</th>
									<th colspan="3">01</th>
									<th colspan="3">02</th>
									<th colspan="3">03</th>
									<th colspan="3">04</th>
									<th colspan="3">05</th>
									<th colspan="3">06</th>
									<th colspan="3">07</th>
									<th colspan="3">08</th>
									<th colspan="3">09</th>
									<th colspan="3">10</th>
									<th colspan="3">11</th>
									<th colspan="3">12</th>
									<th colspan="3">13</th>
									<th colspan="3">14</th>
									<th colspan="3">15</th>
									<th colspan="3">16</th>
									<th colspan="3">17</th>
									<th colspan="3">18</th>
									<th colspan="3">19</th>
									<th colspan="3">20</th>
									<th colspan="3">21</th>
									<th colspan="3">22</th>
									<th colspan="3">23</th>
									<th colspan="3">24</th>
									<th colspan="3">25</th>
									<th colspan="3">26</th>
									<th colspan="3">27</th>
									<th colspan="3">28</th>
									<th colspan="3">29</th>
									<th colspan="3">30</th>
									<th colspan="3">31</th>
								</tr>
								<tr class="colHEADER">
									<th>HRS</th>
									<th>MIN</th>
									<?php
										for ($a=1; $a <= 31; $a++) { 
									?>
									<th>SL/VL</th>
									<th>H</th>
									<th>M</th>
									<?php
										}
									?>
								</tr>
							</thead>
							<tbody>
								<?php
									if ($rsEmployees) {
										while ($row = mysqli_fetch_assoc($rsEmployees)) {
											$SL_Count   = "";
											$VL_Count   = "";
											$UT_Hrs 		= "";
											$UT_Min 		= "";
											$LastName 	= $row["LastName"];
											$FirstName 	= $row["FirstName"];
											$MiddleName = $row["MiddleName"];
											$ExtName 	= $row["ExtName"];
											$emprefid 	= $row["RefId"];
											$FullName 	= "$LastName, $FirstName $ExtName $MiddleName";
											$where 		= "WHERE EmployeesRefId = '$emprefid' AND Month = '$month'";
											$dtr 			= FindFirst("dtr_process",$where,"*");
											if ($dtr) {
												$VL_Count 		= $dtr["VL_Used"];
												$SL_Count 		= $dtr["SL_Used"];
												$SPL_Count 		= $dtr["SPL_Used"];
												$Total_UT 		= $dtr["Total_Undertime_Hr"];
												$military_time = convertToHoursMins($Total_UT);
										    	$time_arr 		= explode(":", $military_time);
										    	if (isset($time_arr[0])) {
										    		$UT_Hrs 			= $time_arr[0];	
										    	}
										    	if (isset($time_arr[1])) {
										    		$UT_Min 			= $time_arr[1];	
										    	}
										    	
											}
								?>
									<tr>
										<td><?php echo $FullName; ?></td>
										<td><?php echo $SL_Count; ?></td>
										<td><?php echo $VL_Count; ?></td>
										<td><?php echo $UT_Hrs; ?></td>
										<td><?php echo $UT_Min; ?></td>
									<?php
										if ($dtr) {
											for ($b=1; $b <= 31; $b++) { 
												if ($b <= 9) $b = "0".$b;
												$dummy_time 		= convertToHoursMins($dtr[$b."_UT"]);
												if ($dummy_time != "") {
													$time_arr 		= explode(":", $dummy_time);
											    	$td_Hrs 			= $time_arr[0];
											    	$td_Min 			= $time_arr[1];	
												} else {
													$td_Hrs = $td_Min = "&nbsp;";
												}
										    	
												echo '
													<td></td>
													<td>'.$td_Hrs.'</td>
													<td>'.$td_Min.'</td>
												';
											}
										} else {
											for ($c=1; $c <= 31 ; $c++) { 
												echo '
													<td>&nbsp;</td>
													<td></td>
													<td></td>
												';
											}
										}
										
									?>
									</tr>
								<?php
										}
									}else {
										echo '<tr><td class="text-center">No Record Found</td></tr>';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>