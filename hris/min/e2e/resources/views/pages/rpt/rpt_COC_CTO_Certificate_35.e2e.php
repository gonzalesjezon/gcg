<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            table {
               font-size: 6pt !important;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                        $prev_month = date("F Y",strtotime(date("F Y")." - 1 months"));
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12 text-center">
                     As of <?php echo date("F Y",time()); ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table width="100%">
                        <thead>
                           <tr class="colHEADER">
                              <th rowspan="2">Name of Employee</th>
                              <th rowspan="2">COC Available as<br>of <?php echo $prev_month; ?></th>
                              <th colspan="2">CTO Utilization</th>
                              <th rowspan="2">COC Available</th>
                           </tr>
                           <tr class="colHEADER">
                              <th>Inclusive Date/s</th>
                              <th>Hour/s</th>
                           </tr>
                        </thead>
                        <tbody>
                     <?php
                        if ($rsEmployees) {
                           while ($row = mysqli_fetch_assoc($rsEmployees)) {
                              $emprefid   = $row["RefId"];
                              $LastName   = $row["LastName"];
                              $FirstName  = $row["FirstName"];
                              $MiddleName = $row["MiddleName"];
                              $ExtName    = $row["ExtName"];
                              $FullName   = $LastName.", ".$FirstName." ".$ExtName." ".$MiddleName;
                              $AgencyId   = $row["AgencyId"];
                              echo '
                                 <tr>
                                    <td>'.$FullName.'</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                 </tr>
                              ';
                     ?>
                     <?php
                           }
                        }
                     ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>