<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
          <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p style="text-indent: 30px;">
                This is to certify that (NAME, ALL CAPS, BOLD), Commissioner of the Office of the Chairman, has an accumulated Leave Credit Balance of 8.250 days as of 31 May 2017, broken down as follows: 
               </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-3"></div>
            <div class="col-xs-2">
              <div class="row">Vacation Leave</div>
              <div class="row margin-top">Sick Leave</div>
              <div class="row margin-top">
                <label>TOTAL</label>
              </div>
            </div>
            <div class="col-xs-1">
              <div class="row">:</div>
              <div class="row margin-top">:</div>
              <div class="row margin-top">:</div>
            </div>
            <div class="col-xs-2">
              <div class="row">2.625</div>
              <div class="row margin-top">5.625</div>
              <div class="row margin-top">
                <label>8.250</label>
              </div>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p>
                This certification is being issued upon the request of <b>Ms. Asuncion</b> for whatever legal purpose it may serve.
              </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p>
                Issued this 16th day of November 2017, in Pasig City, Philippines.
              </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-8"></div>
            <div class="col-xs-4">
              <p>
                <label>KENNETH V. TANATE</label><br>
                Director IV<br>
                Administrative Office
              </p>
            </div>
          </div>



      </div>
      <?php rptFooter(); ?>
   </body>
</html>