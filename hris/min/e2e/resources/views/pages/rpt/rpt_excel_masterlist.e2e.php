<?php
	// We'll be outputting an excel file
	header('Content-type: application/vnd.ms-excel');

	// It will be called file.xls
	header('Content-Disposition: attachment; filename="file.xls"');

	function getRecord($table,$RefId,$fields){
     	include '../conn.e2e.php';
     	if ($RefId) {
        	$sql = "SELECT `$fields` FROM `".strtolower($table)."` where RefId = $RefId";
        	$result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
        	$row = mysqli_fetch_assoc($result);
        	mysqli_close($conn);
        	$numrow = mysqli_num_rows($result);
        	if ($numrow <= 0) {
           	return "";
        	} else {
           	if ($fields == "*") return $row;
           	else {
              	$value = str_replace("(","&#40;",$row[$fields]);
              	$value = str_replace(")","&#41;",$value);
              	$value = str_replace('"',"&#34;",$value);
              	return $value;
           	}
        	}
     	} else return "";
   }

   function FindFirst($table,$whereClause,$fld) {
      include '../conn.e2e.php';
      if ($fld == "*") {
         $sql = "SELECT * FROM `".strtolower($table)."` ".$whereClause." order by refid limit 1";
      } else if (stripos($fld,",") > 0) {
         $sql = "SELECT $fld FROM `".strtolower($table)."` ".$whereClause." order by refid limit 1";
      } else {
         $sql = "SELECT $fld FROM `".strtolower($table)."` ".$whereClause." order by refid limit 1";
      }
      $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
      $row = mysqli_fetch_assoc($result);
      $numrow = mysqli_num_rows($result);
      mysqli_close($conn);
      if ($numrow <= 0) {
         return false;
      } else {
         if ($fld == "*" || stripos($fld,",") > 0) {
            return $row;
         } else {
            return $row[$fld];
         }
      }
   }
   function computeAge($bday,$param = "today"){
      return date_diff(date_create($bday), date_create($param))->y;
   }

   function SelectEach($table,$whereClause) {
      include '../conn.e2e.php';
      $sql = "SELECT * FROM `".strtolower($table)."` ".$whereClause;
      $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
      mysqli_close($conn);
      if (mysqli_num_rows($result) > 0) return $result;
      else return false;
   }

	$where = "WHERE employees.CompanyRefId = 35";
	$where .= " AND employees.BranchRefId = 1";
	$where .= " AND (Inactive != 1 OR Inactive IS NULL)";
	$where .= " ORDER BY LastName";
	$table = "employees";
	$rsEmployees = SelectEach($table,$where);
?>
<!DOCTYPE html>
<html>
   <head>
      <style type="text/css">
         @media print {
            table {
               font-size: 6pt !important;
            }
         }
         th {
            text-align: center; 
            background: gray;
            border: 1px solid black;
         }
         td {
            padding: 5px;
            vertical-align: top;
         }
         .text-center {
         	text-align: center;
         }
      </style>
   </head>
   <body>
      <table width="1000%" border="1">
         <thead>
            <tr>
               <th>#</th>
               <th>DIVISION</th>
               <th>SECTION</th>
               <th>POSITION TITLE</th>
               <th>ITEM NUMBER </th>
               <th>
                  DATE POSITION WAS CREATED
                  <br>
                  (If not able to indicate past creations just include since 2013 or 2014 up-to-date)
               </th>
               <th>SG</th>
               <th>MONTHLY SALARY OR COST OF SERVICES (Per SG)</th>
               <th>DESIGNATION (AS APPROPRIATE)</th>
               <th>DATE OF DESIGNATION</th>
               <th>SPECIAL ORDER NO.</th>
               <th>Email Address</th>
               <th>STATUS OF EMPLOYMENT</th>
               <th>MODE OF ACCESSION</th>
               <th>DATE FILLED UP</th>
               <th>INCUMBENT</th>
               <th>LAST NAME</th>
               <th>FIRST NAME</th>
               <th>MIDDLE NAME</th>
               <th>EXT.</th>
               <th>DATE OF ORIGINAL APPOINTMENT</th>
               <th>DATE OF LAST PROMOTION</th>
               <th>ENTRY DATE IN PCW (First day in service)</th>
               <th>ELIGIBILITY (Specify RA1080 if SW,CPA, etc.)</th>
               <th>HIGHEST EDUCATION COMPLETED</th>
               <th>DEGREE AND COURSE (Specify)</th>
               <th>MASTERS OR DOCTORAL DEGREE (Specify)</th>
               <th>DATE OF BIRTH (MM/DD/YYYY)</th>
               <th>AGE</th>
               <th>SEX</th>
               <th>CIVIL STATUS</th>
               <th>BIR TIN NO.</th>
               <th>RESIDENTIAL ADDRESS</th>
               <th>PERMANENT ADDRESS</th>
               <th>INDICATE WHETHER "SOLO PARENT"</th>
               <th>INDICATE WHETHER "SENIOR CITIZEN"</th>
               <th>INDICATE WHETHER "PWD"</th>
               <th>INDICATE IF MEMBER OF ANY INDIGENOUS GROUP</th>
               <th>CITIZESHIP</th>
               <th>CONTACT NOS.</th>
               <th>EMAIL ADDRESS</th>
               <th>FORMER INCUMBENT</th>
               <th>MODE OF SEPARATION</th>
               <th>DATE VACATED</th>
               <th>REMARKS / STATUS OF VACANT POSITION</th>
            </tr>
         </thead>
         <tbody>
            <?php
               if ($rsEmployees) {
                  $count = 0;
                  while ($row = mysqli_fetch_assoc($rsEmployees)) {
                     $count++;
                     $emprefid                     = $row["RefId"];
                     $LastName                     = $row["LastName"];
                     $FirstName                    = $row["FirstName"];
                     $MiddleName                   = $row["MiddleName"];
                     $ExtName                      = $row["ExtName"];
                     $Sex                          = $row["Sex"];
                     $CivilStatus                  = $row["CivilStatus"];
                     $TIN                          = $row["TIN"];
                     $BirthDate                    = $row["BirthDate"];
                     $EmailAdd                     = $row["EmailAdd"];
                     $ContactNo                    = $row["ContactNo"];
                     $ResiAddCityRefId             = getRecord("city",$row["ResiAddCityRefId"],"Name");
                     $ResiAddProvinceRefId         = getRecord("province",$row["ResiAddProvinceRefId"],"Name");
                     $ResiHouseNo                  = $row["ResiHouseNo"];
                     $ResiStreet                   = $row["ResiStreet"];
                     $ResiSubd                     = $row["ResiSubd"];
                     $ResiBrgy                     = $row["ResiBrgy"];
                     $PermanentHouseNo             = $row["PermanentHouseNo"];
                     $PermanentStreet              = $row["PermanentStreet"];
                     $PermanentSubd                = $row["PermanentSubd"];
                     $PermanentBrgy                = $row["PermanentBrgy"];
                     $PermanentAddCityRefId        = getRecord("city",$row["PermanentAddCityRefId"],"Name");
                     $PermanentAddProvinceRefId    = getRecord("province",$row["PermanentAddProvinceRefId"],"Name");

                     switch ($CivilStatus) {
                        case 'Ma':
                           $CivilStatus = "Married";
                           break;
                        case 'Si':
                           $CivilStatus = "Single";
                           break;
                     }

                     $where                        = "WHERE EmployeesRefId = '$emprefid'";
                     $empinfo                      = FindFirst("empinformation",$where,"*");
                     if ($empinfo) {
                        $HiredDate                 = $empinfo["HiredDate"];
                        $AgencyRefId               = getRecord("agency",$empinfo["AgencyRefId"],"Name");
                        $PositionItemRefId         = getRecord("positionitem",$empinfo["PositionItemRefId"],"Name");
                        $PositionRefId             = getRecord("position",$empinfo["PositionRefId"],"Name");
                        $SalaryGradeRefId          = getRecord("salarygrade",$empinfo["SalaryGradeRefId"],"Name");
                        $EmpStatusRefId            = getRecord("empstatus",$empinfo["EmpStatusRefId"],"Name");
                        $OfficeRefId               = getRecord("office",$empinfo["OfficeRefId"],"Name");
                        $DepartmentRefId           = getRecord("department",$empinfo["DepartmentRefId"],"Name");
                        $DivisionRefId             = getRecord("division",$empinfo["DivisionRefId"],"Name");
                        $SalaryAmount              = number_format(floatval($empinfo["SalaryAmount"]),2);
                     }

            ?>
            <tr>
               <td class="text-center">
                  <?php echo $count; ?>
               </td>
               <td>
                  <?php echo $DivisionRefId; ?>
               </td>
               <td>
                  
               </td>
               <td>
                  <?php echo $PositionRefId; ?>
               </td>
               <td class="text-center">
                  <?php echo $PositionItemRefId; ?>
               </td>
               <td>
                  &nbsp;
               </td>
               <td class="text-center">
                  <?php echo $SalaryGradeRefId; ?>
               </td>
               <td class="text-center">
                  <?php echo $SalaryAmount; ?>
               </td>
               <td>
                  <?php echo $PositionRefId; ?>
               </td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>
                  <?php echo $EmailAdd; ?>
               </td>
               <td>
                  <?php echo $EmpStatusRefId; ?>
               </td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>
                  <?php echo $LastName; ?>
               </td>
               <td>
                  <?php echo $FirstName; ?>
               </td>
               <td>
                  <?php echo $MiddleName; ?>
               </td>
               <td>
                  <?php echo $ExtName; ?>
               </td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>
                  <?php
                     $eligibility_rs = SelectEach("employeeselegibility",$where);
                     if ($eligibility_rs) {
                        while ($eligibility_row = mysqli_fetch_assoc($eligibility_rs)) {
                           echo "<li>".getRecord("careerservice",$eligibility_row["CareerServiceRefId"],"Name")."</li>";
                        }
                     }
                  ?>
               </td>
               <td>
                  
               </td>
               <td></td>
               <td>
                  <?php
                     $masters_rs = SelectEach("employeeseduc",$where." AND LevelType = '5'");
                     if ($masters_rs) {
                        while ($masters_row = mysqli_fetch_assoc($masters_rs)) {
                           echo "<li>".getRecord("course",$masters_row["CourseRefId"],"Name")."</li>";
                        }
                     }
                  ?>
               </td>
               <td class="text-center">
                  <?php echo date("m/d/Y",strtotime($BirthDate)); ?>
               </td>
               <td class="text-center">
                  <?php echo computeAge($BirthDate,$param = "today"); ?>
               </td>
               <td class="text-center">
                  <?php echo $Sex; ?>
               </td>
               <td class="text-center">
                  <?php echo $CivilStatus; ?>
               </td>
               <td>
                  <?php echo $TIN; ?>
               </td>
               <td>
                  <?php echo $ResiBrgy." ".$ResiAddCityRefId." ".$ResiAddProvinceRefId; ?>
               </td>
               <td>
                  <?php echo $PermanentBrgy." ".$PermanentAddCityRefId." ".$PermanentAddProvinceRefId; ?>
               </td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td>FILIPINO</td>
               <td>
                  <?php echo $ContactNo; ?>
               </td>
               <td>
                  <?php echo $EmailAdd; ?>
               </td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td></td>
            </tr>
            <?php
                  }
               } 
            ?>
         </tbody>
      </table>
   </body>
</html>