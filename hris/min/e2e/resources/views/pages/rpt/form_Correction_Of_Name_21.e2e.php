<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName LIMIT 10";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            #Content {;
               font-size: 12pt;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
               while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                  $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
         ?>
         <div class="row" style="page-break-after: always;" id="Content">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                     <br><br>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <p>To Whom It May Concern:</p>

                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <p style="text-indent: 5%;">
                        This is to certify that Mr/Mrs. <b><?php echo strtoupper($FullName); ?></b> is one and the same person who was employed by the Metals Industry Research and Development Center (MIRDC).
                     </p>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <p style="text-indent: 5%;">
                        That the true and correct name of the above is:
                     </p>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-1"></div>
                  <div class="col-xs-3 text-center">
                     First Name
                  </div>
                  <div class="col-xs-3 text-center">
                     Middle Name
                  </div>
                  <div class="col-xs-3 text-center">
                     Last Name
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-1"></div>
                  <div class="col-xs-3 text-center">
                     ----
                  </div>
                  <div class="col-xs-3 text-center">
                     ----
                  </div>
                  <div class="col-xs-3 text-center">
                     ----
                  </div>
               </div>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-4">
                     Certified Correct:
                     <br>
                     <b>JELLY N. ORTIZ, DPA</b>
                     <br>
                     Chief, FAD-AGSS
                  </div>
                  <div class="col-xs-4">
                     Conforme:
                     <br>
                     <b>JOEL B. NARVAEZ</b>
                     <br>
                     Pag-IBIG No: 1210-2819-4970
                  </div>
                  <div class="col-xs-4">
                     Noted:
                     <br>
                     <b>ROBERT O. DIZON</b>
                     <br>
                     Executive Director
                  </div>
               </div>
            </div>
         </div>
         <?php
               }
            }
         ?>
      </div>
   </body>
</html>