<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName LIMIT 10";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      	<style type="text/css">
      	.container {
      		page-break-after: auto;
      	}
  		</style>
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
   		<?php
   			while($row = mysqli_fetch_assoc($rsEmployees)) {
   		?>
      	<div class="container">
      		<div class="row">
      			<div class="col-xs-12">
      				<?php
                    	rptHeader(getvalue("RptName"));
                    	spacer(20);
                 	?>
  				</div>
      		</div>
      		<div class="row">
    			<div class="col-xs-12">
    				<?php bar(); spacer(20); ?>
    				<div class="row text-center">
						<div class="col-xs-12">
							<label>INSTRUCTION</label>
						</div>
    				</div>
    				<div class="row margin-top">
    					<div class="col-xs-1"></div>
						<div class="col-xs-11">
							a. This medical certificate should be accomplished by a licensed government physician.
						</div>
    				</div>
    				<div class="row margin-top">
    					<div class="col-xs-1"></div>
						<div class="col-xs-11">
							b. Attach this certificate to original appointment, transfer and reemployment.
						</div>
    				</div>
    				<div class="row margin-top">
    					<div class="col-xs-1"></div>
						<div class="col-xs-11">
							c. The results of the following pre-employment medical/physical/mental examinations must be attached to this form:
						</div>
    				</div>
    				<div class="row margin-top">
    					<div class="col-xs-2"></div>
						<div class="col-xs-10">
							<input type="checkbox">&nbsp;&nbsp;&nbsp;Blood Test
							<br>
							<input type="checkbox">&nbsp;&nbsp;&nbsp;Urinalysis
							<br>
							<input type="checkbox">&nbsp;&nbsp;&nbsp;Chest X-Ray
							<br>
							<input type="checkbox">&nbsp;&nbsp;&nbsp;Drug Test
							<br>
							<input type="checkbox">&nbsp;&nbsp;&nbsp;Psychological Test
							<br>
							<input type="checkbox">&nbsp;&nbsp;&nbsp;Neuro-Psychiatric Examination (if applicable)
						</div>
    				</div>
    			</div>
    		</div>
    		<?php bar(); spacer(20); ?>
    		<div class="row">
				<div class="col-xs-12">
					<div class="row text-center">
						<div class="col-xs-12">
							<label>FOR THE PROPOSED APPOINTEE</label>
						</div>
					</div>
					<div class="row margin-top">
						<div class="col-xs-12">
							<table>
								<tbody>
									<tr style="background: #999999;">
										<td colspan="3">NAME (Last Name, First Name, Name Extension (if any) and Middle Name)</td>
										<td class="text-center">AGENCY / ADDRESS</td>
									</tr>
									<tr>
										<td colspan="3"><?php echo $row["LastName"].", ".$row["FirstName"]; ?></td>
										<td rowspan="3">&nbsp;</td>
									</tr>
									<tr style="background: #999999;">
										<td colspan="3">ADDRESS</td>
									</tr>
									<tr>
										<td colspan="3">
											<?php 
												$street     = $row["PermanentStreet"];
												$brgy       = $row["PermanentBrgy"];
												$province   = getRecord("province",$row["PermanentAddProvinceRefId"],"Name");
												$city       = getRecord("city",$row["PermanentAddCityRefId"],"Name");
												if ($province != "" || $city != "") {
													echo $street." ".$brgy.", ".$city.", ".$province;	
												} else {
													echo "&nbsp;";
												}
												
											?>
										</td>
									</tr>
									<tr style="background: #999999;">
										<td>AGE</td>
										<td>SEX</td>
										<td>CIVIL STATUS</td>
										<td>PROPOSED POSITION</td>
									</tr>
									<tr>
										<td>
											<?php
												if ($row["BirthDate"] == "") {
													echo "";
												} else {
													$birthday = date_create($row["BirthDate"])->diff(date_create('today'))->y;
													echo $birthday." Years Old";	
												}
												
											?>
										</td>
										<td>
											<?php
												$sex = $row["Sex"];
												if ($sex == "M") {
													$sex = "Male";
												} else if ("F") {
													$sex = "Female";
												} else {
													$sex = "Other";
												}
												echo $sex;
											?>
										</td>
										<td>
											<?php
												$cvlstat = $row["CivilStatus"];
												switch ($cvlstat) {
													case "Ma":
														$cvlstat = "Married";
														break;
													case "Si":
														$cvlstat = "Single";
														break;
													case "An":
														$cvlstat = "Annuled";
														break;
													case "Wi":
														$cvlstat = "Widowed";
														break;
													case "Se":
														$cvlstat = "Separated";
														break;
													case "Ot":
														$cvlstat = "Other";
														break;
												}
												echo $cvlstat;
											?>
										</td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
    		</div>
    		<?php spacer(30); ?>
    		<div class="row">
				<div class="col-xs-12">
					<div class="row text-center">
						<div class="col-xs-12">
							<label>FOR THE LICENSED GOVERNMENT PHYSICIAN</label>
						</div>
					</div>
					<div class="row margin-top">
						<div class="col-xs-12">
							<table>
								<tbody>
									<tr>
										<td colspan="4">
									        <i>I hereby certify that I have reviewed and evaluated the attached examination results, personally examined the above named individual and found him/her to be physically and medically <b><input type="checkbox">&nbsp;FIT / <input type="checkbox">&nbsp;UNFIT</b> for employment.</i>
										</td>
									</tr>
									<tr>
										<td colspan="3">SIGNATURE over PRINTED NAME OF LICENSED GOVERNMENT PHYSICIAN:</td>
										<td class="text-center">OTHER INFORMATION ABOUT THE PROPOSED APPOINTEE</td>
									</tr>
									<tr>
										<td colspan="3">&nbsp;</td>
										<td rowspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="3">AGENCY/Affiliation of Licensed Government Physician: </td>
									</tr>
									<tr>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="3">
											<table>
												<tr>
													<td>LICENSE NO.</td>
												</tr>
												<tr>
													<td>&nbsp;</td>
												</tr>
											</table>
										</td>
										<td>
											<table border="0">
												<tr class="text-center">
													<td>HEIGHT (M)</td>
													<td>WEIGHT (KG)</td>
													<td>BLOOD TYPE</td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td colspan="3">OFFICIAL DESIGNATION</td>
										<td>DATE EXAMINED</td>
									</tr>
									<tr>
										<td colspan="3"></td>
										<td>&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
    		</div>
      	</div>
      	<?php } ?>
   	</body>
</html>
