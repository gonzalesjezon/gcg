<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $period     = getvalue("period");
   $division   = getvalue("division");
   $department = getvalue("department");
   $year       = getvalue("year");
   $where = "WHERE RefId > 0";
   if ($period != "") {
      $where .= " AND Semester = '$period'";
   }
   if (intval($division) > 0) {
      $where .= " AND DivisionRefId = '$division'";
   }
   if (intval($department) > 0) {
      $where .= " AND DepartmentRefId = '$department'";
   }
   if (intval($year) > 0) {
      $where .= " AND YearPerformed = '$year'";
   }
   $count = 0;
   $where .= " LIMIT 2";
   $rs = SelectEach("employeesperformance",$where);
   $nrs = SelectEach("employeesperformance",$where);
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         .rotate {
            transform: rotate(90deg);
         }
         .semi_rotate {
            transform: rotate(-55deg);
            font-size: 8pt;
         }
         .average {
            height: 75px;
            font-size: 12pt;
         }
         .data_td {
             height: 375px;
             padding: 5px;
         }
         .box_data {
            background: #e77543;
            max-width: 40px;
            box-shadow: 5px 0px 0px 2px gray;
            border: 1px solid black;
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("SPMS Report");
         ?>
         <br>
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-1 text-center">
                     <div class="average">
                        <b>5</b>
                        <br>
                        -
                        <br>
                        -
                     </div>
                     <div class="average">
                        <b>4</b>
                        <br>
                        -
                        <br>
                        -
                     </div>
                     <div class="average">
                        <b>3</b>
                        <br>
                        -
                        <br>
                        -
                     </div>
                     <div class="average">
                        <b>2</b>
                        <br>
                        -
                        <br>
                        -
                     </div>
                     <div class="average">
                        <b>1</b>
                        <br>
                     </div>
                     <div style="height: 25px; font-size: 12pt;" class="rotate">
                        <b>Average</b>
                     </div>
                  </div>
                  <div class="col-xs-11">
                     <?php
                        if ($rs) {
                     ?>
                     <table style="min-width: 10%;">
                        <tr valign="bottom" style="height: 375px;">
                           <?php
                              while ($row = mysqli_fetch_assoc($rs)) {
                                 $emprefid = $row["EmployeesRefId"];
                                 $Average = $row["OverallScore"];
                                 $emp_row = FindFirst("employees","WHERE RefId = '$emprefid'","*");
                                 if ($emp_row) {
                                    echo '<th style="padding: 5px; border-bottom:2px solid black;"">';
                                       echo '
                                          <div style="height:'.($Average * 75).'px;" class="box_data">
                                          &nbsp;
                                          </div>
                                       ';
                                    echo '</th>';
                                 }
                              }
                           ?>
                        </tr>
                        <tr>
                           <?php
                              while ($nrow = mysqli_fetch_assoc($nrs)) {
                                 $emprefid   = $nrow["EmployeesRefId"];
                                 $Average    = $nrow["OverallScore"];
                                 $nemp_row   = FindFirst("employees","WHERE RefId = '$emprefid'","*");
                                 if ($nemp_row) {
                                    $LastName   = $nemp_row["LastName"];
                                    $FirstName  = $nemp_row["FirstName"];
                                    $MiddleName = $nemp_row["MiddleName"];
                                    $ExtName    = $nemp_row["ExtName"];
                                    $FullName   = $LastName.", ".$FirstName;
                                 echo '<th style="padding: 5px; border-top:1px solid black;"><br>';
                                    echo '
                                       <div class="semi_rotate" style="width: 50px;">'.$LastName.', '.$FirstName.'</div>
                                    ';
                                 echo '</th>';
                                 }
                              }
                           ?>
                        </tr>
                     </table>
                     <?php
                                 
                        } else {
                           echo '<h3>No Record Found</h3>';
                        }
                     ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
