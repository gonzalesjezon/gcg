<?php
	
?>
<!DOCTYPE html>
<html>
   	<head>
      	<?php include_once $files["inc"]["pageHEAD"]; ?>
      	<link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      	<style>
         	td {vertical-align:top;}
      	</style>
   	</head>
   	<body>
      	<div class="container-fluid rptBody">
      		<div class="row">
      			<div class="col-xs-12">
      				<?php
			            rptHeader("NEWLY HIRED EMPLOYEES");
			         ?>
      				<table style="width: 100%" border="1">
      					<thead>
      						<tr class="colHEADER">
      							<th>#</th>
	      						<th>Employee ID</th>
	      						<th>Employee Name</th>
	      						<th>Date Hired</th>	
      						</tr>
      						
      					</thead>
      					<tbody>
      						<?php
      							$count = 0;
      							$past_month = date("Y",time())."-01-01";
	                            $newly_hired_rs = SelectEach("empinformation","WHERE HiredDate > '$past_month'");
	                            if ($newly_hired_rs) {
	                                while ($newly_hired_row = mysqli_fetch_assoc($newly_hired_rs)) {
	                                    $emprefid = $newly_hired_row["EmployeesRefId"];
	                                    $hired_date = $newly_hired_row["HiredDate"];
	                                    $fld = "`LastName`,`FirstName`,`MiddleName`,`AgencyId`";
	                                    $row_emp = FindFirst("employees","WHERE RefId = '$emprefid'",$fld);
	                                    if ($row_emp) {
	                                    	$count++;
	                                    	$AgencyId = $row_emp["AgencyId"];
	                                    	$FullName = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
	                                    	echo '<tr>';
	                                    		echo '
	                                    			<td class="text-center">'.$count.'</td>
	                                    			<td class="text-center">'.$AgencyId.'</td>
	                                    			<td>'.$FullName.'</td>
	                                    			<td class="text-center">'.date("d M Y",strtotime($hired_date)).'</td>
	                                    		';
	                                    	echo '</tr>';
	                                    }
	                                }
	                            }
      						?>
      					</tbody>
      				</table>
      			</div>
      		</div>
      	</div>
    </body>
</html>