<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            table {
               font-size: 6pt !important;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            if ($rsEmployees) {
               $check = 0;
               while ($row = mysqli_fetch_assoc($rsEmployees)) {
                  $emprefid   = $row["RefId"];
                  $LastName   = $row["LastName"];
                  $FirstName  = $row["FirstName"];
                  $MiddleName = $row["MiddleName"];
                  $ExtName    = $row["ExtName"];
                  $FullName   = $FirstName." ".$ExtName." ".$MiddleName." ".$LastName;
                  $AgencyId   = $row["AgencyId"];
                  $empinfo    = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
                  if ($empinfo) {
                     $Division      = getRecord("division",$empinfo["DivisionRefId"],"Name");
                  } else {
                     $Division = "";
                  }
         ?>
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     This is entitles Ms./Mr. <b><u><?php echo $FullName; ?></u></b> to <b><u>19.55</u></b> of Compensatory Overtime Credits as  of 20 October 2015.
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table width="100%">
                        <thead>
                           <tr class="colHEADER">
                              <th colspan="5">COMPENSATORY OVERTIME CREDIT</th>
                              <th rowspan="2" colspan="3">COMPENSATORY TIME OFF</th>
                           </tr>
                           <tr class="colHEADER">
                              <th rowspan="2">Date</th>
                              <th>Weekdays</th>
                              <th colspan="2">Weekends/Holidays<br>(Multiply by 1.5 days)</th>
                              <th rowspan="2">COC<br>Earned</th>
                              
                           </tr>
                           <tr class="colHEADER">
                              <th>No. of<br>Hours<br>Earned</th>
                              <th>No. of Hours<br>Earned(Actual)</th>
                              <th>
                                 Conversion<br>of COCs on<br>(Weekends)
                              </th>
                              <th>Date of CTO</th>
                              <th>Used COCs</th>
                              <th>COC<br>Remaining<br>Balance</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              for ($i=1; $i <= 10 ; $i++) { 
                                 echo '
                                    <tr>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
                                    </tr>
                                 ';
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     Valid until: 31 December <?php echo date("Y",time()); ?>
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     Certified By:
                     <br>
                     <br>
                     <b><u>Jess C. Santos</u></b>
                     <br>
                     HR Personnel
                     <br>
                     Date Signed:
                  </div>
                  <div class="col-xs-6">
                     Noted By:
                     <br>
                     <br>
                     <b><u>Marie E. Castro</u></b>
                     <br>
                     HR Section Head
                     <br>
                     Date Signed:
                  </div>
               </div>
               <br>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <sup>1</sup> Employee’s Name
                     <br>
                     <sup>2</sup> No. of hours COC remaining balance as of date of report generation
                     <br>
                     <sup>3</sup> Date of Report Generation
                     <br>
                     <sup>4</sup> Formula: Previous COC remaining balance + COC earned – Used COC
                     <br>
                     <sup>5</sup> Year preceding current year of COC
                     <br>
                     <sup>6</sup> Facility for changing the names of signatories
                  </div>
               </div>
            </div>
         </div>
         <?php
               }
            }
         ?>
      </div>
   </body>
</html>