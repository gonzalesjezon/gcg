<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid = getvalue("refid");
   $row = FindFirst("employeesauthority","WHERE RefId = $refid","*");
   if ($row) {
   		$authority = getRecord("absences",$row["AbsencesRefId"],"Name");
   		$authority = strtoupper($authority);
   		$Remarks = $authority." - ".$row["Remarks"];
   		$Destination = $row["Destination"];
   		$emprefid = $row["EmployeesRefId"];
   		$employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   		if ($employees) {
   			$FirstName = $employees["FirstName"];
   			$LastName = $employees["LastName"];
   			$MiddleName = $employees["MiddleName"];
   			$ExtName = $employees["ExtName"];
   			$FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
   		} else {
   			$FullName = "&nbsp;";
   		}
   		$empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   		if ($empinformation) {
   			$OfficeRefId = getRecord("office",$empinformation["OfficeRefId"],"Name");
   			$DivisionRefId = getRecord("division",$empinformation["DivisionRefId"],"Name");
   			$PositionRefId = getRecord("position",$empinformation["PositionRefId"],"Name");
   		} else {
   			$OfficeRefId = $DivisionRefId = $PositionRefId = "&nbsp;";
   		}
   		if ($row["Whole"] == 1) {
   			$start_time = $end_time = "Whole Day";
   		} else {
   			$start_time = convertToHoursMins($row["FromTime"]);
   			$end_time = convertToHoursMins($row["ToTime"]);
   		}
   		$from = $row["ApplicationDateFrom"];
   		$to = $row["ApplicationDateTo"];
   		if ($from == $to) {
   			$date = date("F d, Y",strtotime($from));
   		} else {
   			$date = date("F d, Y",strtotime($from))." to ".date("F d, Y",strtotime($to));;
   		}

   		
   }
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td {
			border: 2px solid black;
			vertical-align: top;
			padding: 5px;
			font-size: 9pt;
		}
		.data {
			font-size: 10pt;
			text-transform: uppercase;
			font-weight: 600;
		}
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div style="page-break-after: always;">
	        <div class="row">
	         	<div class="col-xs-12">
	         		<center>
	         			<h3>AUTHORITY TO LEAVE THE OFFICE</h3>
	         		</center>
	         		<table width="100%">
	         			<tr>
	         				<td style="width: 50%;">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Date:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $date;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td style="width: 25%;">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Time Of Departure
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $start_time;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td style="width: 25%;">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Expect Time of Return:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $end_time;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Printed Name and Signature of <br>
	         							Official/Employee:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $FullName;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Position Title:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $PositionRefId;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Office/Division:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $OfficeRefId."<br>".$DivisionRefId;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td colspan="2">
	         					Destination and Purpose of Official Business
	         				</td>
	         				<td rowspan="2">
	         					Approved:
	         					<?php spacer(100); ?>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td rowspan="3" colspan="2" valign="top" style="min-height: 90px;">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Destination:
	         							<br>
	         							<?php
	         								if ($Destination != "") {
	         									echo "<b>".$Destination."</b>";
	         								} else {
	         									spacer(90);
	         								}
	         							?>
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-12">
	         							Purpose:
	         							<br>
	         							<?php echo "<b>".$Remarks."</b>"; ?>
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td class="text-center">Concerned DM/DA</td>
	         			</tr>
	         			<tr>
	         				<td>
	         					<li>
	         						Please ensure Check out on the biometrics upon leaving office.
	         					</li>
	         					<li>Check in on the biometrics upon arrival.</li>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td colspan="2" rowspan="2" valign="top">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Transport:
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-1"></div>
	         						<div class="col-xs-11">
	         							[ ] Requested
	         							<br>
	         							[ ] Not Requested
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-2"></div>
	         						<div class="col-xs-10">
	         							<li>
	         								Coordinate with Admin dept. for vehicle assignment
	         							</li>
	         						</div>
	         					</div>
	         				</td>
	         				<td valign="top">
	         					<li>
	         						Submit to HR upon approval of concern DM/DA
	         					</li>
	         					<?php spacer(40); ?>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td>
	         					Recorded by HR
	         					<?php spacer(40); ?>
	         				</td>
	         			</tr>
	         		</table>
	         	</div>
	        </div>
	    </div>
    </div>
</body>
</html>