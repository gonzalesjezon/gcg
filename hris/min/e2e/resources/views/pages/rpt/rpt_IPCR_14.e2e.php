<?php
	include_once 'constant.e2e.php';
	require_once pathClass.'0620functions.e2e.php';
	$ipcr_refid   = getvalue("refid");
	$row = FindFirst("spms_ipcr","WHERE RefId = '$ipcr_refid'","*");
	if ($row) {
		$Semester   = $row["Semester"];
		$Year 		= $row["Year"];
		$emprefid 	= $row["EmployeesRefId"];
		$emp_row 	= FindFirst("employees","WHERE RefId = '$emprefid'","*");
		$LastName 	= $emp_row["LastName"];
		$FirstName 	= $emp_row["FirstName"];
		$MiddleName = $emp_row["MiddleName"];
		$ExtName 	= $emp_row["ExtName"];
		$FullName 	= $FirstName." ".$MiddleName." ".$LastName." ".$ExtName; 
		$FullName 	= strtoupper($FullName);

		$empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
		if ($empinfo_row) {
			$Division 	= strtoupper(getRecord("division",$empinfo_row["DivisionRefId"],"Name"));
			$Position 	= strtoupper(getRecord("Position",$empinfo_row["PositionRefId"],"Name"));
			$Office 	= strtoupper(getRecord("Office",$empinfo_row["OfficeRefId"],"Name"));
		} else {
			$Division = $Position = $Office = "";
		}
		switch ($Semester) {
			case '1':
				$Semester = "January - June ".$Year;
				break;
			case '2':
				$Semester = "July - December ".$Year;
				break;
			default:
				$Semester = "";
				break;
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<?php
		include_once 'pageHEAD.e2e.php';
	?>
	<title></title>
	<style type="text/css">
		thead {
			text-transform: uppercase;
		}
		.border-l-b-r {
			border-bottom: 2px solid black;
			border-left: 2px solid black;
			border-right: 2px solid black;
		}
		td {
			padding: 2px;
			border: 1px solid black;
		}
		body {
			font-size: 8pt;
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12">
						<table width="100%">
							<tr>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
							</tr>
							<tr>
								<td colspan="12" style="padding: 10px;">
									<div class="row">
										<div class="col-xs-12 text-center">
											<b>
												INDIVIDUAL PERFORMANCE COMMITMENT AND REVIEW (IPCR)
											</b>
											<br><br>
										</div>
									</div>
									<div class="row margin-top">
										<div class="col-xs-12" style="text-indent: 5%;">
											I <u><?php echo $FullName; ?></u>, of the <u><?php echo $Office; ?></u>, commit to deliver and agree to be rated on the attainment of the following targets in accordance with the indicated measures for the period <?php echo $Semester; ?>
											<br><br>
										</div>
									</div>
									<div class="row margin-top">
										<div class="col-xs-7"></div>
										<div class="col-xs-4 text-center">
											<u><?php echo $FullName; ?></u>
											<br>
											Ratee
										</div>
									</div>
									<div class="row margin-top">
										<div class="col-xs-7"></div>
										<div class="col-xs-4 text-center">
											Date:
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="5">
									Reviewed by:
								</td>
								<td class="text-center">
									Date:
								</td>
								<td colspan="5">
									Approved by:
								</td>
								<td class="text-center">
									Date:
								</td>
							</tr>
							<tr>
								<td colspan="5">
									<?php spacer(80); ?>
								</td>
								<td rowspan="2">
									&nbsp;
								</td>
								<td colspan="5">
									<?php spacer(80); ?>
								</td>
								<td rowspan="2">
									&nbsp;
								</td>
							</tr>
							<tr>
								<td colspan="5" class="text-center">
									Immediate Supervisor
								</td>
								<td colspan="5" class="text-center">
									Head of Office
								</td>
							</tr>
						</table>
					</div>
				</div>
				<br><br>
				<div class="row">
					<div class="col-xs-12">
						<table width="100%">
							<tr>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
								<td style="width: 8.33%; padding: 0px; border: none;"></td>
							</tr>
							<tr class="text-center">
								<td colspan="2" rowspan="2">
									Output
								</td>
								<td colspan="2" rowspan="2">
									Success Indicator
									<br>
									(Targets + Measures)
								</td>
								<td colspan="2" rowspan="2">
									Actual Accomplishments
								</td>
								<td colspan="4">
									Rating
								</td>
								<td colspan="2" rowspan="2">
									Remarks
								</td>
							</tr>
							<tr class="text-center">
								<td>Q<sup>1</sup></td>
								<td>E<sup>2</sup></td>
								<td>T<sup>3</sup></td>
								<td>A<sup>4</sup></td>
							</tr>
							<?php
								$average 	= 0;
								$count 		= 0;
								$rs = SelectEach("ipcr_details","WHERE spms_ipcr_id = '$ipcr_refid'");
								if ($rs) {
									$count  = mysqli_num_rows($rs);
									while ($ipcr_row = mysqli_fetch_assoc($rs)) {
										$output 			= $ipcr_row["output"];
										$success_indicator 	= $ipcr_row["success_indicator"];
										$accomplishment 	= $ipcr_row["accomplishment"];
										$q1 				= $ipcr_row["q1"];
										$e2 				= $ipcr_row["e2"];
										$t3 				= $ipcr_row["t3"];
										$a4 				= $ipcr_row["a4"];
										$total = intval($q1) + intval($e2) + intval($t3) + intval($a4);
										$total = $total / 4;
										$average += $total;
										$remarks = $ipcr_row["remarks"];
										echo '<tr>';
										echo '<td colspan="2">'.$output.'</td>';
										echo '<td colspan="2">'.$success_indicator.'</td>';
										echo '<td colspan="2">'.$accomplishment.'</td>';
										echo '<td class="text-center">'.$q1.'</td>';
										echo '<td class="text-center">'.$e2.'</td>';
										echo '<td class="text-center">'.$t3.'</td>';
										echo '<td class="text-center">'.$a4.'</td>';
										echo '<td colspan="2">'.$remarks.'</td>';
										echo '</tr>';
									}
								} else {
									echo '<tr><td colspan="12">No IPCR Found</td></tr>';
								}
							?>
							<tr>
								<td colspan="6">
									Final Average Rating
								</td>
								<td colspan="4" class="text-right">
									<?php
										if ($count > 0) {
											echo number_format(($average / $count),2);	
										}
									?>
								</td>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td colspan="12">
									Comments and recommendations for development purposes
									<br><br>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									Discussed with:
								</td>
								<td class="text-center">
									Date:
								</td>
								<td colspan="3">
									Assessed by:
								</td>
								<td class="text-center">
									Date:
								</td>
								<td colspan="3">
									Final Rating By:
								</td>
								<td class="text-center">
									Date:
								</td>
							</tr>
							<tr>
								<td colspan="3" valign="bottom" class="text-center">
									<?php echo $FullName; ?>
								</td>
								<td rowspan="2">
									&nbsp;
								</td>
								<td colspan="3">
									<?php spacer(80); ?>
								</td>
								<td rowspan="2">
									&nbsp;
								</td>
								<td colspan="3">
									<?php spacer(80); ?>
								</td>
								<td rowspan="2">
									&nbsp;
								</td>
							</tr>
							<tr>
								<td colspan="3" class="text-center">
									<?php echo $Position; ?>
								</td>
								<td colspan="3" class="text-center">
									Supervisor
								</td>
								<td colspan="3" class="text-center">
									Chief
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>