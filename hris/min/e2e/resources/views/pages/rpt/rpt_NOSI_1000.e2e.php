<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   include 'incSignatory.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName LIMIT 5";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         @media print {
            .header-space {margin-top: 25%;}
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <div class="col-xs-12">
               <?php
                  if ($rsEmployees) {
                     while ($emp_row = mysqli_fetch_assoc($rsEmployees)) {
                        $emprefid      = $emp_row["RefId"];
                        $FirstName     = $emp_row["FirstName"];
                        $LastName      = $emp_row["LastName"];
                        $MiddleName    = $emp_row["MiddleName"];
                        $ExtName       = $emp_row["ExtName"];
                        $MiddleInitial = substr($MiddleName, 0,1);
                        $FullName      = $FirstName." ".$MiddleInitial.". ".$LastName;
                        $emp_info      = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
                        if ($emp_info) {
                           $Position      = rptDefaultValue($emp_info["PositionRefId"],"position");
                           $SalaryGrade   = rptDefaultValue($emp_info["SalaryGradeRefId"],"SalaryGrade");
                           $StepIncrement = rptDefaultValue($emp_info["StepIncrementRefId"],"StepIncrement");
                           $ApptStatus    = rptDefaultValue($emp_info["ApptStatusRefId"],"apptstatus");
                           $SalaryAmount  = number_format($emp_info["SalaryAmount"],2);
                           $Assumption    = date("m/d/Y",strtotime($emp_info["AssumptionDate"]));
                           $default_salary = $emp_info["SalaryAmount"];
                        } else {
                           $Position      = "";
                           $ApptStatus    = "";
                           $Assumption    = "";
                           $SalaryAmount  = "0.00";
                        }
                        $where_empmovement   = "WHERE EmployeesRefId = '$emprefid' AND ExpiryDate IS NOT NULL";
                        $empmovement         = SelectEach("employeesmovement",$where_empmovement);
                        if ($empmovement) {
                           while($empmovement_row = mysqli_fetch_assoc($empmovement)) {
                              $appt       = $empmovement_row["ApptStatusRefId"];
                              $appt_name  = getRecord("apptstatus",$appt,"Name");
                              $old_sg     = rptDefaultValue($empmovement_row["SalaryGradeRefId"],"SalaryGrade");
                              $old_si     = rptDefaultValue($empmovement_row["StepIncrementRefId"],"StepIncrement");
                              $start_date = date("F d, Y",strtotime($empmovement_row["ExpiryDate"]." + 1 Day"));
                              $Fld        = "Step".$old_si;
                              if (strtolower($appt_name) == "step increment") {
                                 $where_old_salary    = "WHERE RefId = '".$empmovement_row["SalaryGradeRefId"]."'";
                                 $old_salary          = FindFirst("salarygrade",$where_old_salary,$Fld);
                                 $default_old_salary  = $old_salary;
                                 if (intval($old_salary) > 0) $old_salary = number_format($old_salary,2);

                                 $salary_diff = $default_salary - $default_old_salary;
                                 $salary_diff = number_format($salary_diff,2);
               ?>
               <div style="page-break-after: always;">
                  <div class="row header-space">
                     <div class="col-xs-12 text-center">
                        <h3><b>NOTICE OF STEP INCREMENT</b></h3>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12 text-right">
                        <?php echo date("F d, Y",time()); ?>
                     </div>
                  </div>   
                  <br><br>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <?php
                           echo "<b>".$FullName."</b><br>".$Position;
                        ?>
                        <br><br>
                        Sir/Madam:
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <p style="text-indent: 5%;">
                           Pursuant to Joint Civil Service Commission (CSC) and Department of Budget and Management (DBM) Circular No. 1 s. 1990 implementing Section 13 © of RA No. 6758,your salary as DIVISION MANAGER A is hereby adjusted effective <?php echo $start_date; ?> as shown below:
                        </p>
                     </div>
                  </div>
                  <div class="row margin-top">
                     <div class="col-xs-1"></div>
                     <div class="col-xs-5">
                        1. Basic Monthly Salary as of <br>&nbsp;&nbsp;&nbsp; <?php echo $start_date; ?> : <?php echo $SalaryGrade; ?>, Step <?php echo $StepIncrement; ?>
                     </div>
                     <div class="col-xs-5 text-right">
                        &nbsp;<br>
                        P <?php echo $SalaryAmount; ?>
                     </div>
                  </div>
                  <br>
                  <div class="row margin-top">
                     <div class="col-xs-1"></div>
                     <div class="col-xs-5">
                        2. Basic Monthly Salary as of <br>&nbsp;&nbsp;&nbsp; <?php echo date("F d, Y",strtotime($empmovement_row["ExpiryDate"])); ?> : <?php echo $old_sg; ?>, Step <?php echo $old_si; ?>
                     </div>
                     <div class="col-xs-5 text-right">
                        &nbsp;<br>
                        P <?php echo $old_salary; ?>
                     </div>
                  </div>
                  <br>
                  <div class="row margin-top">
                     <div class="col-xs-1"></div>
                     <div class="col-xs-5">
                        3. Salary Increase per month effective <br>&nbsp;&nbsp;&nbsp; <?php echo $start_date; ?>
                     </div>
                     <div class="col-xs-5 text-right">
                        &nbsp;<br>
                        P <?php echo $salary_diff; ?>
                     </div>
                  </div>
                  <br>
                  <div class="row margin-top">
                     <div class="col-xs-1"></div>
                     <div class="col-xs-5">
                        4. Adjusted Salary effective <br>&nbsp;&nbsp;&nbsp; <?php echo $start_date; ?>
                     </div>
                     <div class="col-xs-5 text-right">
                        &nbsp;<br>
                        P <b><u><?php echo $SalaryAmount; ?></u></b>
                     </div>
                  </div>
                  <br><br>
                  <div class="row margin-top">
                     <div class="col-xs-12">
                        <p style="text-indent: 5%;">
                           This step is subject to review and post-audit by the Department of Budget and Management and subject to re-adjustment and refund if found not in order.
                        </p>
                     </div>
                  </div>
                  <br>
                  <div class="row margin-top">
                     <div class="col-xs-12 text-right">
                        Very truly yours,
                     </div>
                  </div>   
               </div>
               <?php
                              }
                           }
                        }
                     }
                  }
               ?>
            </div>
         </div>
      </div>
   </body>
</html>