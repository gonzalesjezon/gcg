<?php
   //session_start();
   include 'colors.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   include 'incRptSortBy.e2e.php';
   $rsEmployee = SelectEach($table,$whereClause);
   if ($rsEmployee) $rowcount = mysqli_num_rows($rsEmployee);
   if ($dbg) { echo "DBG >> ".$whereClause; }

   /*Start Here Date Validation --*/
   $errmsg = "";
   /*End Here - Date Validation*/
   $recordsCount = 0;

   function dispNumFormat($val) {
      if ($val == "") {
         echo "0.00";
      } else {
         echo number_format($val,2);
      }
   }

   function dispBlank($val) {
      if ($val == "") {
         echo "---";
      } else {
         echo $val;
      }
   }

?>

<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody page--">
         <?php
            //rptHeader("Notice of Salary Adjustment");

            $empRefid = "";
            while ($row = mysqli_fetch_assoc($rsEmployee))
            {
               $childCount = 0;
               $empRefid = $row['RefId'];
               $sql = "SELECT * FROM `employeesmovement` WHERE CompanyRefId = $CompanyId ";
               $sql .= "AND BranchRefId = $BranchId ";
               $sql .= "AND EmployeesRefId = $empRefid ORDER BY EffectivityDate DESC";
               $rsServiceRecords = mysqli_query($conn,$sql) or die(mysqli_error($conn));
               $numrow = mysqli_num_rows($rsServiceRecords);
               if ($numrow > 0) {
                  $EmployeesName = $row["FirstName"]." ".$row["MiddleName"].". ".$row["LastName"];
                  rptHeader("SERVICE RECORD");

                  include "inc/incServiceRecords.e2e.php";
         ?>
                  
         <?php
                  $recordsCount++;
               }
               echo '<div class="nextpage"></div>';
            }
            echo
            '<div class="lastpage">';
            rptHeader("SERVICE RECORD");
            echo
            '<span>RECORD COUNT : '.$recordsCount.'</span>
            <div>SEARCH CRITERIA:</div>';
            if ($searchCriteria == "") echo "<li>ALL RECORDS</li>";
            else {
               $crit_Arr = explode("|",$searchCriteria);
               for ($j=0;$j<count($crit_Arr);$j++) {
                  echo "<li>".$crit_Arr[$j]."</li>";
               }
            }
            echo
            '</div>';
         ?>
      </div>
   </body>
</html>