<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $refid = getvalue("refid");
   $row = FindFirst("attendance_request","WHERE RefId = $refid","*");
   if ($row) {
   		$type = $row["Type"];
   		$emprefid = $row["EmployeesRefId"];
   		$employees = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   		if ($employees) {
   			$FirstName 	= $employees["FirstName"];
   			$LastName 	= $employees["LastName"];
   			$MiddleName = $employees["MiddleName"];
   			$ExtName 	= $employees["ExtName"];
   			$FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
   		} else {
   			$FullName = "&nbsp;";
   		}
   		$empinformation = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   		if ($empinformation) {
   			$OfficeRefId = getRecord("office",$empinformation["OfficeRefId"],"Name");
   			$DivisionRefId = getRecord("division",$empinformation["DivisionRefId"],"Name");
   			$PositionRefId = getRecord("position",$empinformation["PositionRefId"],"Name");
   		} else {
   			$OfficeRefId = $DivisionRefId = $PositionRefId = "&nbsp;";
   		}
   		$filed_date = date("F d, Y",strtotime($row["FiledDate"]));
   		$applied_date = date("F d, Y",strtotime($row["AppliedDateFor"]));
   }
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td {
			border: 2px solid black;
			vertical-align: top;
			padding: 5px;
			font-size: 9pt;
		}
		.data {
			font-size: 10pt;
			text-transform: uppercase;
			font-weight: 600;
		}
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div style="page-break-after: always;">
	        <?php
	            rptHeader("Request for Time Capture Registration/Correction");
	        ?>
	        <div class="row">
	         	<div class="col-xs-12">
	         		<table width="100%">
	         			<tr>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Name of Employee:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $FullName;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Position Title:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $PositionRefId;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Office/Division:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $OfficeRefId."<br>".$DivisionRefId;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							DATE FILED:
	         							<br>
	         							<span class="data">
		         							<?php
		         								echo $filed_date;
		         							?>
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td colspan="4">
	         					<div class="row">
	         						<div class="col-xs-12 text-center">
	         							<span class="data">
		         							Details of the Request
	         							</span>
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td class="text-center" style="width: 30%">
	         					Date of Attendance to be Corrected
	         				</td>
	         				<td class="text-center" style="width: 20%">
	         					PM Break Out
	         				</td>
	         				<td class="text-center" style="width: 20%">
	         					PM Break In
	         				</td>
	         				<td class="text-center" style="width: 30%">
	         					Reason for the Request
	         				</td>
	         			</tr>
	         			<tr>
	         				<td class="text-center">
	         					<span class="data">
         							<?php
         								echo $applied_date;
         							?>
     							</span>
	         				</td>
	         				<td class="text-center">
	         					&nbsp;
	         				</td>
	         				<td class="text-center">
	         					&nbsp;
	         				</td>
	         				<td>
	         					&nbsp;
	         				</td>
	         			</tr>
	         		</table>
	         	</div>
	        </div>
	        <div class="row">
	        	<div class="col-xs-12">
	        		<?php spacer(40); ?>
	        		Recommended by:
	        		<br><br>
	        		<b>HONEY M. CASTRO</b>
	        		<br>
	        		Information Officer V
	        	</div>
	        </div>
	    </div>
    </div>
</body>
</html>