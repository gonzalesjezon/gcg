<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $refid = getvalue("refid");
   $row = FindFirst("employeescto","WHERE RefId = '$refid'","*");
   $FiledDate = date("F d, Y",strtotime($row["FiledDate"]));
   $emprefid = $row["EmployeesRefId"];
   $emp_row = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`,`ExtName`");
   if ($emp_row) {
      $LastName = $emp_row["LastName"];
      $FirstName = $emp_row["FirstName"];
      $MiddleName = $emp_row["MiddleName"];
      $ExtName = $emp_row["ExtName"];
      $FullName = $LastName.", ".$FirstName." $ExtName ".$MiddleName;
   } else {
      $FullName = "";
   }
   $empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
   if ($empinfo_row) {
      $Office = getRecord("office",$empinfo_row["OfficeRefId"],"Name");
      $Division = getRecord("Division",$empinfo_row["DivisionRefId"],"Name");
      $Position = getRecord("position",$empinfo_row["PositionRefId"],"Name");
   } else {
      $Office = "";
      $Position = "";
      $Division = "";
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </div>
               </div>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-6"></div>
                  <div class="col-xs-6 text-right">
                     DATE OF FILING: <?php echo date("m/d/Y",strtotime($FiledDate)) ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     NAME: <?php echo rptDefaultValue($FullName); ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     POSITION: <?php echo rptDefaultValue($Position); ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     DIVISION: <?php echo rptDefaultValue($Division); ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     SCHEDULE OF CTO: 
                  </div>
                  <div class="col-xs-6">
                     NUMBER OF HOURS: <?php echo $row["Hours"]; ?>
                  </div>
               </div>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-4">
                     Requested by:
                  </div>
                  <div class="col-xs-4">
                     Recommending Approval
                  </div>
                  <div class="col-xs-4">
                     Approved By: 
                  </div>
               </div>
               <br>
               <br>
               <div class="row margin-top">
                  <div class="col-xs-4 text-center">
                     ___________________________
                     <br>
                     Print Name and Signature
                  </div>
                  <div class="col-xs-4 text-center">
                     ___________________________
                     <br>
                     Division Head
                  </div>
                  <div class="col-xs-4 text-center">
                     ___________________________
                     <br>
                     Executive Director /
                     <br>
                     Authorized Official
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     Attachment: <b><i>Certificate of Overtime Credits</i></b>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>