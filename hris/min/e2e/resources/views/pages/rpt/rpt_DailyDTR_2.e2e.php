<?php
	require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $selected_date_from = getvalue("attendance_date_from");
   $selected_date_to = getvalue("attendance_date_to");
   $from 			= $selected_date_from." 00:00:01";
   $to 				= $selected_date_to." 23:59:59";
   $rsEmployees 	= SelectEach($table,$whereClause);
   if ($selected_date_from == $selected_date_to) {
   	$selected_date = date("F d, Y",strtotime($selected_date_from));
   } else {
   	$selected_date = date("F d, Y",strtotime($selected_date_from))." to ".date("F d, Y",strtotime($selected_date_to));;
   }
   if ($selected_date_from == "" || $selected_date_to == "") {
   	echo "<h3>Invalid Date Range</h3>";
   	return false;
   }
?>
<html>
   <head>
   	<?php include_once $files["inc"]["pageHEAD"]; ?>
   	<link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   	<script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
		<div class="container-fluid">
			<?php
				if ($selected_date == "") {
					echo "<h3>No Selected Date</h3>";
					return false;
				}
				if ($rsEmployees) {
					while ($row = mysqli_fetch_assoc($rsEmployees)) {
						$bio_id = $row["BiometricsID"];
						$LastName = $row["LastName"];
						$FirstName = $row["FirstName"];
						$MiddleName = $row["MiddleName"];
						$ExtName = $row["ExtName"];
						$FullName = $LastName.", ".$FirstName." ".$ExtName." ".$MiddleName;
 			?>

			<div class="row margin-top" style="margin-bottom: 50px; page-break-after: always;">
				<div class="col-xs-6">
					<div class="row margin-top">
						<div class="col-xs-12">
							<?php
				            rptHeader("ATTENDANCE LOGS");
				        	?>
						</div>
					</div>
					<div class="row margin-top">
						<div class="col-xs-4">
							Employee Name: <br><b><?php echo $FullName; ?></b>
						</div>
						<div class="col-xs-6">
							LOG TIME FOR <br><b><?php echo $selected_date; ?></b>
						</div>
					</div>
					<div class="row margin-top">
						<div class="col-xs-12">
							<table border="1" style="width: 100%">
								<tbody>
									<?php
										$biometricsID 	= strtolower($bio_id);
										$sql 				= "SELECT * FROM USERINFO where NAME = '$biometricsID'";
										$new_rs 			= mysqli_query($bio_conn,$sql);
										$date_check 	= "";
										$dummy_check 	= "";
										if (mysqli_num_rows($new_rs) > 0) {
											while ($bio_row = mysqli_fetch_assoc($new_rs)) {
												$userID = $bio_row["USERID"];
												$where_zk = "WHERE USERID = '$userID' AND CHECKTIME >= '$from' AND CHECKTIME <= '$to'";
												$zk_sql = "SELECT * FROM CHECKINOUT ".$where_zk;
												$zk_rs = mysqli_query($bio_conn,$zk_sql);
												if (mysqli_num_rows($zk_rs) > 0) {
													while ($zk_row = mysqli_fetch_assoc($zk_rs)) {
														$time = $zk_row["CHECKTIME"];
														$date = date("F d, Y",strtotime($time));
														$time = date("h:i A",strtotime($time));

														if ($date_check == "") {
															$date_check = $date;
															echo '
																<tr>
																	<th>'.$date.'</th>
																</tr>
															';
														}

														if ($date_check != $date) {
															echo '
																<tr>
																	<th>'.$date.'</th>
																</tr>
															';
														}
														
														echo '<tr>
																<td style="padding:5px;">'.$time.'</td>
															</tr>';
														$date_check = $date;

													}
												} else {
													echo '<tr><td>No Logs Found</td></tr>';
												}
											}
										} else {
											echo '<tr><td>Incorrect Biometrics ID</td></tr>';
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<?php
					}
				}
			?>
		</div>   	
   </body>
</html>
