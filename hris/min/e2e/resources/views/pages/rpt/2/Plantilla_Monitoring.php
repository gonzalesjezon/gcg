<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="bootstrap.min.css">
	<style type="text/css">
		table {
			width: 100%;
		}
		td {
			padding: 2px;
		}
		#myTable th {
			text-align: center;
		}
	</style>
</head>
	<body>
		<table id="myTable" border="1" width="100%" style="font-size: 9px;">
			<thead>
				<tr>
					<th style="text-align: left;">FULL NAME</th>
					<th style="text-align: left;">POSITION</th>
					<th style="text-align: left;">DIVISION</th>
					<th style="text-align: left;">OFFICE</th>
					<th style="text-align: center;">PLANTILLA ITEM NO.</th>
					<th style="text-align: center;">JOB<br>GRADE</th>
					<th style="text-align: center;">MONTHLY<br>SALARY</th>
					<th style="text-align: center;">DATE OF<br>ASSUMPTION TO<br>DUTY</th>
					<th style="text-align: center;">NATURE OF <br> APPOINTMENT</th>
					<th style="text-align: center;">STATUS OF <br> APPOINTMENT</th>
					<th style="text-align: center;">PREVIOUS <br> EMPLOYER</th>
					<th style="text-align: center;">EMAIL ADDRESS</th>
					<th style="text-align: center;">RESIDENTAL <br> ADDRESS</th>
					<th style="text-align: center;">MOBILE <br> NUMBER</th>
					<th style="text-align: center;">BIRTHDATE</th>
					<th style="text-align: left;">REMARKS</th>
					<th style="text-align: center;">HIRING <br> STATUS</th>
					<th style="text-align: center;">Perm/Co-<br>Term</th>
					<th style="text-align: center;">EMPLOYEE<br>ORIENTATION</th>
				</tr>
			</thead>
			<tbody>
				<?php
					for ($i=1; $i <=10 ; $i++) { 
						echo '
							<tr>
								<td>'.$i.'</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						';
					}
				?>
			</tbody>
		</table>
	</body>
</html>