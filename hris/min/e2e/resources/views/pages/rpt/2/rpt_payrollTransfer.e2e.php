<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <!--<table>
            <thead>
               <tr>
                  <th>
                  </th>
               </tr>   
            </thead>
            <tbody>
               <tr>
                  <td></td>
               </tr>   
            </tbody>
            <tfoot>
               <tr>
                  <td></td>
               </tr>   
            </tfoot>
         </table>-->   
         <table>
            <thead>
               <tr>
                  <th colspan="22" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                     <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
                  </th>
               </tr>   
               <tr class="colHEADER">
                  <th rowspan="3">ID</th>
                  <th rowspan="3">EMPLOYEE NAME</th>
                  <th rowspan="3">POSITION</th>
                  <th colspan="3">SALARY</th>
                  <th rowspan="3">(GROSS LESS<br>LWOP)</th>
                  <th colspan="13">DEDUCTIONS</th>
                  <th rowspan="3">TOTAL DED</th>
                  <th rowspan="3">NET PAY</th>
               </tr>
               <tr class="colHEADER">
                  <th rowspan="2">BASIC</th>
                  <th rowspan="2">PERA</th>
                  <th rowspan="2">GROSS PAY</th>
                  <th colspan="7">GSIS</th>
                  <th rowspan="2">PhilHealth</th>
                  <th rowspan="2">W/ Tax</th>
                  <th colspan="4">PAG-IBIG</th>
               </tr>
               <tr class="colHEADER">
                  <th>PREMIUM</th>
                  <th>CONSO</th>
                  <th>POLICY</th>
                  <th>Optional<br>Life</th>
                  <th>Educ<br>Loan</th>
                  <th>Emergency/<br>Calamity</th>
                  <th>Total GSIS</th>
                  <th>Cont</th>
                  <th>MPL/CAL</th>
                  <th>MP2</th>
                  <th>Total<br>Pag-ibig</th>
               </tr>
            </thead>
            <tbody>
                  <?php
                     $rs = SelectEach("empinformation","GROUP BY RefId LIMIT 60");
                     $i = 0;
                     if ($rs){
                        while($row = mysqli_fetch_assoc($rs)){
                     $i++;
                  ?>
                     <tr>
                        <td><?php echo $i;?></td>
                        <td>
                           <?php
                              $result = mysqli_query($conn,"SELECT * FROM employees WHERE RefId =".$row["EmployeesRefId"]);
                              if (mysqli_num_rows($result) > 0 ){
                                while($data = mysqli_fetch_assoc($result)){
                                  echo $data["LastName"].", ".$data["FirstName"];
                                }
                              }
                            ?>
                        </td>
                        <td><?php echo getRecord("position",$row["PositionRefId"],"Name"); ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                     </tr>
                     <?php 
                           } 
                        } else {
                           for ($a=0;$a<60;$a++){
                     ?>
                        <tr>
                           <td><?php echo $a;?></td>
                           <td>
                              JUAN DELA CRUZ
                           </td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                        </tr>
                  <?php 
                     }
                  }
                  ?>

            </tbody>
            <tfoot>
               <tr>
                  <td colspan="22">
                     <?php rptFooter(); ?>
                  </td>
               </tr>   
            </tfoot>
         </table>   
      </div>
   </body>
</html>