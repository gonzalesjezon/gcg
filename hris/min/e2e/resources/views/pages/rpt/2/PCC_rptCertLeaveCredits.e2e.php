<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include_once 'incRptQryString.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         
        <?php
            /*$sql = "SELECT * FROM employees ".$whereClause;
            $rs = mysqli_query($conn,$sql);*/
            $rs = SelectEach("employees",$whereClause);
            if (mysqli_num_rows($rs) > 0) {
               while ($row = mysqli_fetch_assoc($rs)) {
                  rptHeader(getRptName(getvalue("drpReportKind")));
                  $EmployeesRefId = $row["RefId"];
                  $CompanyRefId   = $row["CompanyRefId"];
                  $BranchRefId    = $row["BranchRefId"];
                  $LastName       = $row["LastName"];
                  $FirstName      = $row["MiddleName"];
                  $MiddleName     = $row["FirstName"];
                  $FullName       = $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];
                  $where          = "WHERE CompanyRefId = $CompanyRefId";
                  $where         .= " AND BranchRefId = $BranchRefId";
                  $where         .= " AND EmployeesRefId = $EmployeesRefId";
                  $wherevl        = $where." AND NameCredits = 'VL'";
                  $wherevl       .= " AND EffectivityYear = ".date("Y",time());
                  $wheresl        = $where." AND NameCredits = 'SL'";
                  $wheresl       .= " AND EffectivityYear = ".date("Y",time());

                  $vl_row         = FindFirst("employeescreditbalance",$wherevl,"*");
                  if ($vl_row) {
                     if ($vl_row["OutstandingBalance"] != "") {
                        $vl = $vl_row["OutstandingBalance"];
                     } else {
                        $vl = $vl_row["BeginningBalance"];
                     }
                  } else {
                     $vl = 0;
                  }


                  $sl_row         = FindFirst("employeescreditbalance",$wheresl,"*");
                  if ($sl_row) {
                     if ($sl_row["OutstandingBalance"] != "") {
                        $sl = $sl_row["OutstandingBalance"];
                     } else {
                        $sl = $sl_row["BeginningBalance"];
                     }
                  } else {
                     $sl = 0;
                  }
         ?>

          <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p style="text-indent: 30px;">
                This is to certify that <span style="text-transform: uppercase; "><b><?php echo ("$FullName");?></b></span>, Commissioner of the Office of the Chairman, has an accumulated Leave Credit Balance of <?php echo $vl+$sl; ?> days as of <?php echo date("d F Y",time()); ?>, broken down as follows: 
               </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-3"></div>
            <div class="col-xs-2">
              <div class="row">Vacation Leave</div>
              <div class="row margin-top">Sick Leave</div>
              <div class="row margin-top">
                <label>TOTAL</label>
              </div>
            </div>
            <div class="col-xs-1">
              <div class="row">:</div>
              <div class="row margin-top">:</div>
              <div class="row margin-top">:</div>
            </div>
            <div class="col-xs-2">
              <div class="row">
                <?php echo $vl; ?>
              </div>
              <div class="row margin-top">
                <?php echo $sl; ?>
              </div>
              <div class="row margin-top">
                <label><?php echo $vl+$sl; ?></label>
              </div>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p>
                This certification is being issued upon the request of <b>Ms. Asuncion</b> for whatever legal purpose it may serve.
              </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p>
                Issued this 16th day of November 2017, in Pasig City, Philippines.
              </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-8"></div>
            <div class="col-xs-4" align="center">
              <p>
                <label>KENNETH V. TANATE</label><br>
                Director IV<br>
                Administrative Office
              </p>
            </div>
          </div>

          <?php
              }
            }
          ?>

      </div>
   </body>
</html>