<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <!--<table>
            <thead>
               <tr>
                  <th>
                  </th>
               </tr>   
            </thead>
            <tbody>
               <tr>
                  <td></td>
               </tr>   
            </tbody>
            <tfoot>
               <tr>
                  <td></td>
               </tr>   
            </tfoot>
         </table>-->   
         <table>
            <thead>
               <tr>
                  <th colspan="13" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                     <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
                  </th>
                  
               </tr>   
               <tr class="colHEADER">
                  <th class="padd5">ID</th>
                  <th class="padd5">EMPLOYEE NAME</th>
                  <th class="padd5">POSITION</th>
                  <th class="padd5">ASSUMPTION</th>
                  <th class="padd5">PEI AMOUNT</th>
                  <th class="padd5">NET PAY</th>
               </tr>
            </thead>
            <tbody>
            <?php
               $rs = SelectEach("empinformation","GROUP BY RefId LIMIT 60");
               $i = 0;
               if ($rs){
                  while($row = mysqli_fetch_assoc($rs)){
               $i++;
                
            ?>
               <tr>
                  <td><?php echo $i;?></td>
                  <td>
                     <?php
                        $result = mysqli_query($conn,"SELECT * FROM employees WHERE RefId =".$row["EmployeesRefId"]);
                        if (mysqli_num_rows($result) > 0 ){
                          while($data = mysqli_fetch_assoc($result)){
                            echo $data["LastName"].", ".$data["FirstName"];
                          }
                        }
                      ?>
                  </td>
                  <td><?php echo getRecord("position",$row["PositionRefId"],"Name"); ?></td>
                  <td></td>
                  <td></td>
                  <td></td>
               </tr>
            <?php 
                  } 
               } else {
                  for ($a=1;$a<15;$a++){
            ?>
               <tr>
                  <td><?php echo $a;?></td>
                  <td>
                     JUAN DELA CRUZ
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
               </tr>
            <?php
                  }
               }
            ?>
               <tr>
                  <td></td>
                  <td colspan="3">Total No. Of Employees</td>
                  <td></td>
                  <td></td>
               </tr>
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="13">
                     <?php rptFooter(); ?>
                  </td>
               </tr>   
            </tfoot>
         </table>   
      </div>
   </body>
</html>