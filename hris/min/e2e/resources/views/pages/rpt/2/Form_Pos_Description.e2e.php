<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         .gray {
            background: #d9d9d9;
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <table>
            <thead>
               <tr>
                  <th colspan="12">
                     <?php rptHeader(getRptName(getvalue("drpReportKind"))); ?>
                  </th>
               </tr>
            </thead>
            <tbody>
               <tr>
                  <td rowspan="2" class="text-center" colspan="6">
                     <b>POSITION DESCRIPTION FORM DBM-CSC Form No. 1</b>
                     <br>
                     (Revised Version No. 1 ,s.2017)
                     <?php spacer(40); ?>
                  </td>
                  <td colspan="6" class="gray">
                     <b>1.  POSITION TITLE (as approved  by authorized agency) <br> with parenthetical title</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="6">&nbsp;</td>
               </tr>
               <tr>
                  <td colspan="6" class="gray">
                     <b>2. ITEM NUMBER </b>
                  </td>
                  <td colspan="6" class="gray">
                     <b>3. SALARY GRADE</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="6">
                     &nbsp;
                  </td>
                  <td colspan="6">
                     
                  </td>
               </tr>
               <tr>
                  <td colspan="12" class="gray">
                     <b>4. FOR LOCAL GOVERNMENT POSITION, ENUMERATE GOVERNMENTAL UNIT AND CLASS</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="12">
                     <div class="row">
                        <div class="col-xs-4" style="padding-left: 10px;">
                           <input type="checkbox">&nbsp;Province
                           <br>
                           <input type="checkbox">&nbsp;City
                           <br>
                           <input type="checkbox">&nbsp;Municipality
                        </div>
                        <div class="col-xs-4" style="padding-left: 10px;">
                           <input type="checkbox">&nbsp;1st Class
                           <br>
                           <input type="checkbox">&nbsp;2nd Class
                           <br>
                           <input type="checkbox">&nbsp;3rd Class
                           <br>
                           <input type="checkbox">&nbsp;4th Class
                        </div>
                        <div class="col-xs-4" style="padding-left: 10px;">
                           <input type="checkbox">&nbsp;5th Class
                           <br>
                           <input type="checkbox">&nbsp;6th Class
                           <br>
                           <input type="checkbox">&nbsp;Special
                        </div>
                     </div>
                  </td>
               </tr>
               <tr>
                  <td colspan="6" class="gray">
                     <b>5. DEPARTMENT, CORPORATION OR AGENCY/LOCAL GOVERNMENT</b>
                  </td>
                  <td colspan="6" class="gray">
                     <b>6. BUREAU OR OFFICE</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="6">
                     &nbsp;
                  </td>
                  <td colspan="6">
                     
                  </td>
               </tr>
               <tr>
                  <td colspan="6" class="gray">
                     <b>7. DEPARTMENT / BRANCH / DIVISION</b>
                  </td>
                  <td colspan="6" class="gray">
                     <b>8. WORKSTATION / PLACE OF WORK</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="6">
                     &nbsp;
                  </td>
                  <td colspan="6">
                     
                  </td>
               </tr>
               <tr>
                  <td colspan="3" class="gray">
                     <b>9. PRESENT APPROP ACT</b>
                  </td>
                  <td colspan="3" class="gray">
                     <b>10. PREVIOUS APPROP ACT</b>
                  </td>
                  <td colspan="3" class="gray">
                     <b>11.  SALARY AUTHORIZED</b>
                  </td>
                  <td colspan="3" class="gray">
                     <b>12.  OTHER COMPENSATION </b>
                  </td>
               </tr>
               <tr>
                  <td colspan="3">&nbsp;</td>
                  <td colspan="3">&nbsp;</td>
                  <td colspan="3">&nbsp;</td>
                  <td colspan="3">&nbsp;</td>
               </tr>
               <tr>
                  <td colspan="6" class="gray">
                     <b>13. POSITION TITLE OF IMMEDIATE SUPERVISOR</b>
                  </td>
                  <td colspan="6" class="gray">
                     <b>14. POSITION TITLE OF NEXT HIGHER SUPERVISOR</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="6">
                     &nbsp;
                  </td>
                  <td colspan="6">
                     
                  </td>
               </tr>
               <tr>
                  <td colspan="12" class="gray">
                     <b>15. POSITION TITLE, AND ITEM OF THOSE DIRECTLY SUPERVISED</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="12" class="text-center">
                     <i>(if more than seven (7) list only by their item numbers and titles)</i>
                  </td>
               </tr>
               <tr>
                  <td colspan="6" class="text-center">
                     <b>POSITION TITLE</b>
                  </td>
                  <td colspan="6" class="text-center">
                     <b>ITEM NUMBER</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="6">
                     &nbsp;
                  </td>
                  <td colspan="6">
                     
                  </td>
               </tr>
               <tr>
                  <td colspan="12" class="gray">
                     <b>16. MACHINE, EQUIPMENT, TOOLS, ETC., USED REGULARLY IN PERFORMANCE OF WORK</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="12">
                     &nbsp;
                  </td>
               </tr>
               <tr>
                  <td colspan="12" class="gray">
                     <b>17.  CONTACTS / CLIENTS / STAKEHOLDERS</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="2" class="gray">
                     <b>17a. Internal</b>
                  </td>
                  <td colspan="2" class="gray">
                     <b>Occasional</b>
                  </td>
                  <td colspan="2" class="gray">
                     <b>Frequent</b>
                  </td>
                  <td colspan="2" class="gray">
                     <b>17b. External</b>
                  </td>
                  <td colspan="2" class="gray">
                     <b>Occasional</b>
                  </td>
                  <td colspan="2" class="gray">
                     <b>Frequent</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="2">
                     <b>Executive/Managerial</b>
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="2">
                     <b>General Public</b>
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
               </tr>
               <tr>
                  <td colspan="2">
                     <b>Supervisor</b>
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="2">
                     <b>Other Agencies</b>
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
               </tr>
               <tr>
                  <td colspan="2">
                     <b>Non-Supervisor</b>
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="2" style="font-size:7pt;">
                     <b>Others (Please Specify)</b>
                  </td>
                  <td colspan="4">
                     &nbsp;
                  </td>
               </tr>
               <tr>
                  <td colspan="2">
                     <b>Staff</b>
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="6">
                     &nbsp;
                  </td>
               </tr>
               <tr>
                  <td colspan="12" class="gray">
                     <b>18. WORKING CONDITION</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="2">
                     <b>Office Work</b>
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="2" style="font-size:7pt;">
                     <b>Others (Please Specify)</b>
                  </td>
                  <td colspan="4">
                     &nbsp;
                  </td>
               </tr>
               <tr>
                  <td colspan="2">
                     <b>Field Work</b>
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="2" class="text-center">
                     <input type="checkbox">
                  </td>
                  <td colspan="6">
                     &nbsp;
                  </td>
               </tr>
               <tr>
                  <td colspan="12">
                     &nbsp;
                  </td>
               </tr>
               <tr>
                  <td colspan="12" class="gray">
                     <b>19.  BRIEF DESCRIPTION OF THE GENERAL FUNCTION OF THE UNIT OR SECTION</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="12">
                     <?php spacer(30); ?>
                  </td>
               </tr>
               <tr>
                  <td colspan="12" class="gray">
                     <b>20.  BRIEF DESCRIPTION OF THE GENERAL FUNCTION OF THE POSITION (Job Summary)</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="12">
                     <?php spacer(50); ?>
                  </td>
               </tr>
               <tr>
                  <td colspan="12" class="gray">
                     <b>21.  QUALIFICATION STANDARDS</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="3" class="text-center gray">
                     <b>21a. Education</b>
                  </td>
                  <td colspan="3" class="text-center gray">
                     <b>21b. Experience</b>
                  </td>
                  <td colspan="3" class="text-center gray">
                     <b>21c. Training</b>
                  </td>
                  <td colspan="3" class="text-center gray">
                     <b>21d. Eligibility</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="3">
                     <?php spacer(30); ?>
                  </td>
                  <td colspan="3">
                     <?php spacer(30); ?>
                  </td>
                  <td colspan="3">
                     <?php spacer(30); ?>
                  </td>
                  <td colspan="3">
                     <?php spacer(30); ?>
                  </td>
               </tr>
               <tr>
                  <td colspan="9" class="gray">
                     <b>21e. Core Competencies</b>
                  </td>
                  <td colspan="3" class="text-center gray">
                     <b>Competency Level</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="9" style="padding-top: 15pt; padding-bottom: 15pt; text-align: center;">
                     (Indicate the required Core Competencies here)
                  </td>
                  <td colspan="3" style="padding-top: 15pt; padding-bottom: 15pt; text-align: center;">
                     (Indicate the required Competency Level here)
                  </td>
               </tr>
               <tr>
                  <td colspan="9" class="gray">
                     <b>21f. Leadership Competencies</b>
                  </td>
                  <td colspan="3" class="text-center gray">
                     <b>Competency Level</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="9" style="padding-top: 15pt; padding-bottom: 15pt; text-align: center;">
                     (Indicate the required Leadership Competencies here)
                  </td>
                  <td colspan="3" style="padding-top: 15pt; padding-bottom: 15pt; text-align: center;">
                     (Indicate the required Competency Level here)
                  </td>
               </tr>
               <tr>
                  <td colspan="9" class="gray">
                     <b>22. STATEMENT OF DUTIES AND RESPONSIBILITIES (Technical Competencies)</b>
                  </td>
                  <td colspan="3" class="text-center gray">
                     <b>Competency Level</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="3" class="text-center">
                     Percentage of Working Time
                  </td>
                  <td colspan="6" class="text-center">
                     (State the duties and responsibilities here)
                  </td>
                  <td colspan="3" rowspan="2" style="padding-top: 15pt; padding-bottom: 15pt; text-align: center;">
                     (Indicate the required Competency Level here)
                  </td>
               </tr>
               <tr>
                  <td colspan="3">
                     <?php spacer(20); ?>
                  </td>
                  <td colspan="6">
                     <?php spacer(20); ?>
                  </td>
               </tr>
               <tr>
                  <td colspan="12" class="gray">
                     <b>23.  ACKNOWLEDGMENT AND ACCEPTANCE:</b>
                  </td>
               </tr>
               <tr>
                  <td colspan="12">
                     <p>I have received a copy of this position description. It has been discussed with me and I have freely chosen to comply with the performance and behavior/conduct expectations contained herein.</p>
                  </td>
               </tr>
               <tr>
                  <td colspan="6">
                     <?php spacer(40); ?>
                  </td>
                  <td colspan="6">
                     <?php spacer(40); ?>
                  </td>
               </tr>
               <tr>
                  <td colspan="6" class="text-center">
                     <b>Employee's Name, Date and Signature</b>
                  </td>
                  <td colspan="6" class="text-center">
                     <b>Supervisor's Name, Date and Signature</b>
                  </td>
               </tr>
            </tbody>
         </table>
      </div>
   </body>
</html>