<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1) ?></u> 2017</p>
         
         <div class="row" style="margin-left:100px;margin-right:10px;">
            <div class="col-xs-2">
            </div>
            <div class="col-xs-8">
               (date)________________<br>
                  <br>
                  <br>
               To Whom it may Concern<br>
                  <br>
                  <br>
               This is to certify that ________________________ is a Philhealth member with MID ____________<br>
                  <br>
               This further certifies that he/she has the following contributions to wit:
            </div>
            <div class="col-xs-2">
            </div>
         </div>
         <?php spacer(40);?>
         <div class="row" style="text-align: center;">
            <div class="col-xs-3">
               <labeL>Month</label>
            </div>
            <div class="col-xs-2">
               <labeL>EE Share</label>
            </div>
            <div class="col-xs-2">
               <labeL>ER Share</label>
            </div>
            <div class="col-xs-2">
               <labeL>OR NO.#</label>
            </div>
            <div class="col-xs-3">
               <labeL>Date of Payment</label>
            </div>
         </div>
         <?php spacer(200);?>
         <div class="row" style="margin-left:100px;margin-right:10px;">
            <div class="col-xs-2">
            </div>
            <div class="col-xs-8">
               This certification is issued upon the request of __________________ valid for whater legal<br>
               purpose it may serve.<br>
               <br>
               Done this (day)_____ of (month)_____ at (company name and address)________________________________<br>
                         
               <br>
               <br>
               <br>
               <br>
               <br>
               <br>
               Sign by:____________
               <br>
               <br>
               <br>
               Name<br>
               Authorized Signatories
            <div class="col-xs-2">
            </div>
         </div>
         <?php spacer(50);?>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>