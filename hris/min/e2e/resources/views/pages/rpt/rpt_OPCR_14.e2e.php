<?php
	include_once 'constant.e2e.php';
	require_once pathClass.'0620functions.e2e.php';
	$refid   = getvalue("refid");
	$row = FindFirst("spms_opcr","WHERE RefId = '$refid'","*");
	if ($row) {
		$Semester   		= $row["Semester"];
		$Year 				= $row["Year"];
		$Adjectival_Rating 	= $row["Adjectival_Rating"];
		$Overall_Rating 	= $row["Overall_Rating"];
		$Final_Rating 		= $row["Final_Rating"];
		$Office 			= strtoupper(getRecord("Office",$row["OfficeRefId"],"Name"));
		switch ($Semester) {
			case '1':
				$Semester = "January - June ".$Year;
				break;
			case '2':
				$Semester = "July - December ".$Year;
				break;
			default:
				$Semester = "";
				break;
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<?php
		include_once 'pageHEAD.e2e.php';
	?>
	<title></title>
	<style type="text/css">
		thead {
			text-transform: uppercase;
		}
		.border-l-b-r {
			border-bottom: 2px solid black;
			border-left: 2px solid black;
			border-right: 2px solid black;
		}
		td {
			padding: 2px;
			border: 1px solid black;
		}
		body {
			font-size: 8pt;
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 text-center">
						<i>
							<b>
								OFFICE PERFORMANCE COMMITMENT AND REVIEW FORM
							</b>
						</i>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-xs-12">
						<i>
							I ______________________________________ of the, <u><?php echo $Office; ?></u> commit to deliver and agree to be rated on the attainment of the following targets in accordance with the indicated measures for the period <u><?php echo $Semester; ?></u>;
						</i>
					</div>
				</div>
				<br>
				<br>
				<br>
				<div class="row margin-top">
					<div class="col-xs-6"></div>
					<div class="col-xs-5 text-center">
						______________________________________
						<br>
						Ratee
						<br>
						______________________________________
						<br>
						Date
					</div>
				</div>
				<br>
				<br>
				<div class="row margin-top">
					<div class="col-xs-12">
						<table border="1" width="100%">

							<tr>
								<td colspan="8" rowspan="3">
									<div class="row">
										<div class="col-xs-12">
											Approved by:
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 text-center">
											<br><br><br>
											PATRICK LESTER TY
										</div>
									</div>
								</td>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2" class="text-center">Date</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="8" class="text-center">
									Head of Agency
								</td>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="5">&nbsp;</td>
								<td class="text-center">
									5<br>4<br>3<br>2<br>1
								</td>
								<td colspan="4">
									Outstanding
									<br>
									Very Satisfactiory
									<br>
									Satisfactory
									<br>
									Unsatisfactory
									<br>
									Poor
								</td>
							</tr>
							<tr align="center">
								<td style="width: 20%;" rowspan="2">
									Output
								</td>
								<td style="width: 20%;" rowspan="2">
									Success Indicator
									<br>
									(Target + Measure)
								</td>
								<td style="width: 10%;" rowspan="2">
									Allotted Budget
								</td>
								<td style="width: 10%;" rowspan="2">
									Division/Individuals
									<br>
									Accountable
								</td>
								<td style="width: 10%;" rowspan="2">
									Actual
									<br>
									Accomplishments
								</td>
								<td colspan="4">
									Rating
								</td>
								<td style="width: 10%;" rowspan="2">
									Remarks
								</td>
							</tr>
							<tr align="center">
								<td style="width: 5%;">Q<sup>1</sup></td>
								<td style="width: 5%;">E<sup>2</sup></td>
								<td style="width: 5%;">T<sup>3</sup></td>
								<td style="width: 5%;">A<sup>4</sup></td>
							</tr>
							<?php
								$rs = SelectEach("opcr_details","WHERE spms_opcr_id = '$refid'");
								if ($rs) {
									while ($opcr_row = mysqli_fetch_assoc($rs)) {
										$output 			= htmlentities($opcr_row["output"]);
										$success_indicator 	= htmlentities($opcr_row["success_indicator"]);
										$accomplishment 	= htmlentities($opcr_row["accomplishment"]);
										$allotted_budget 	= htmlentities($opcr_row["allotted_budget"]);
										$accountable 		= htmlentities($opcr_row["accountable"]);
										$q1 				= htmlentities($opcr_row["q1"]);
										$e2 				= htmlentities($opcr_row["e2"]);
										$t3 				= htmlentities($opcr_row["t3"]);
										$a4 				= htmlentities($opcr_row["a4"]);
										$remarks 			= htmlentities($opcr_row["remarks"]);
										echo '<tr valign="top">';
											echo '<td>'.$output.'</td>';
											echo '<td>'.$success_indicator.'</td>';
											echo '<td>'.$allotted_budget.'</td>';
											echo '<td>'.$accountable.'</td>';
											echo '<td>'.$accomplishment.'</td>';
											echo '<td class="text-center">'.$q1.'</td>';
											echo '<td class="text-center">'.$e2.'</td>';
											echo '<td class="text-center">'.$t3.'</td>';
											echo '<td class="text-center">'.$a4.'</td>';
											echo '<td>'.$remarks.'</td>';
										echo '</tr>';
									}
								} else {
									echo '<tr><td colspan="10">No Record Found</td></tr>';
								}
							?>
							<tr>
								<td colspan="2">
									Total Average Rating
									<br>
									Final Average Rating
									<br>
									Adjectival Rating
								</td>
								<td></td>
								<td></td>
								<td></td>
								<td rowspan="5" colspan="5"></td>
							</tr>
							<tr>
								<td colspan="2">
									Assessed by:
								</td>
								<td></td>
								<td class="text-center">
									Final Rating by:
								</td>
								<td>
									Date
								</td>
							</tr>
							<tr>
								<td></td>
								<td rowspan="2" valign="top">
									Date
									
								</td>
								<td valign="top">
									Date
									<br><br><br>
								</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="text-center">
									Planning Office
								</td>
								<td class="text-center">
									PMT
								</td>
								<td class="text-center">
									Head Of Agency
								</td>
								<td></td>
							</tr>
							<tr>
								<td colspan="5">
									Legend: 
									1-Quality&nbsp;&nbsp;&nbsp;
									2-Efficiency&nbsp;&nbsp;&nbsp;
									3-Timeliness&nbsp;&nbsp;&nbsp; 
									4-Average&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>