<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $count = 0;
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Status of Return Service Agreement");
         ?>
         <div class="row">
            <div class="col-xs-12 text-center">
               Quarter:______, Year:_________
            </div>
         </div>
         <br><br>
         <div class="row">
            <div class="col-xs-12">
               <table style="width: 100%;">
                  <thead>
                     <tr class="colHEADER">
                        <th>Name</th>
                        <th>Title of<br>Training/Seminar/Scholarship</th>
                        <th>Start Date of Return Service</th>
                        <th>Date Return Service will be <br>Served</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $rs = SelectEach("employees","WHERE RefId > 0 AND (Inactive != 1 OR Inactive IS NULL)");
                        if ($rs) {
                           while ($row = mysqli_fetch_assoc($rs)) {
                              $check_name    = "";
                              $emprefid      = $row["RefId"];
                              $LastName      = $row["LastName"];
                              $FirstName     = $row["FirstName"];
                              $MiddleName    = $row["MiddleName"];
                              $MiddleInitial = substr($MiddleName, 0,1);
                              $FullName      = $FirstName." ".$MiddleInitial.". ".$LastName;
                              $where_competency = "WHERE EmployeesRefId = '$emprefid'";
                              $competency    = SelectEach("ldmsreturnobligation",$where_competency);
                              if ($competency) {
                                 $row_count  = mysqli_num_rows($competency);
                                 while ($competency_row = mysqli_fetch_assoc($competency)) {
                                    $LDMSLNDInterventionRefId  = $competency_row["LDMSLNDInterventionRefId"];
                                    $Rating                    = $competency_row["Rating"];
                                    $Equivalent                = $competency_row["Equivalent"];
                                    $Name                      = $competency_row["Name"];
                                    $ServiceStartDate          = $competency_row["ServiceStartDate"];
                                    $InterventionStartDate     = $competency_row["InterventionStartDate"];
                                    $InterventionEndDate       = $competency_row["InterventionEndDate"];
                                    $ServedStartDate           = $competency_row["ServedStartDate"];
                                    $ReturnService             = $competency_row["ReturnService"];
                                    $where_intervention        = "WHERE RefId = '$LDMSLNDInterventionRefId'";
                                    $comp_row            = FindFirst("ldmslndintervention",$where_intervention,"*");
                                    $Name                = $comp_row["Name"];
                                    echo '<tr>';
                                       if ($check_name == $FullName) {
                                          echo '<td>&nbsp;</td>';
                                       } else {
                                          echo '<td>'.$FullName.'</td>';
                                          $check_name = $FullName;
                                       }
                                       echo '<td>'.$Name.'</td>';
                                       echo '<td class="text-center">'.date("F d, Y",strtotime($ServiceStartDate)).'</td>';
                                       echo '<td class="text-center">'.date("F d, Y",strtotime($ServedStartDate)).'</td>';
                                    echo '</tr>';
                                 }
                              }
                           }
                        }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </body>
</html>
