<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
               while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                  $FullName = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                  $FullName = rptDefaultValue($FullName);
                  $emp_info = FindFirst("empinformation","WHERE EmployeesRefId = ".$row_emp["RefId"],"*");
                  if ($emp_info) {
                     $Position   = rptDefaultValue($emp_info["PositionRefId"],"position");
                     $Division   = rptDefaultValue($emp_info["DivisionRefId"],"division");
                     $ApptStatus = rptDefaultValue($emp_info["ApptStatusRefId"],"apptstatus");
                  } else {
                     $Position   = "";
                     $Division   = "";
                     $ApptStatus = "";
                  }
         ?>
         <div class="row" style="page-break-after: always;" id="Content">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                     <br><br>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     This is to certify that per records as of date, <b><?php echo $FullName; ?></b>, <b><?php echo $Position; ?></b>  of the <b><?php echo $Division; ?></b>, has incurred Leave without Pay on the following dates:
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table style="width: 100%;">
                        <thead>
                           <tr class="colHEADER">
                              <th>Date</th>
                              <th>Appointment Status</th>
                              <th>No of Incurred LWOP (day/s)</th>
                           </tr>   
                        </thead>
                        <tbody>
                           <?php
                              $emp_leave = SelectEach("employeesleave","WHERE EmployeesRefId = ".$row_emp["RefId"]." AND WithPay = 0");
                              if ($emp_leave) {
                                 $lwop_count = 0;
                                 while ($leave_row = mysqli_fetch_assoc($emp_leave)) {
                                    $lwop_count++;
                                    echo '
                                       <tr>
                                          <td>'.$leave_row["ApplicationDateFrom"].' - '.$leave_row["ApplicationDateTo"].'</td>
                                          <td>'.$ApptStatus.'</td>
                                          <td>'.(dateDifference($leave_row["ApplicationDateFrom"],$leave_row["ApplicationDateTo"]) + 1).'</td>
                                       </tr>
                                    ';
                                 }
                                 echo '
                                    <tr>
                                       <td class="text-center" colspan="2">TOTAL</td>
                                       <td class="text-center">'.$lwop_count.'</td>
                                    </tr>
                                 ';
                              } else {
                                 echo '
                                    <tr>
                                       <td class="text-center" colspan="3">NO LWOP</td>
                                    </tr>
                                 ';
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <?php
               }
            }
         ?>
      </div>
   </body>
</html>