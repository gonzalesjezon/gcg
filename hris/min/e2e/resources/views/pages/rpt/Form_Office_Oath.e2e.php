<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            table {
               font-size: 6pt !important;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            if ($rsEmployees) {
               while ($row = mysqli_fetch_assoc($rsEmployees)) {
                  $LastName   = $row["LastName"];
                  $FirstName  = $row["FirstName"];
                  $FullName   = $LastName.", ".$FirstName;
         ?>
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-12">
                     <b><i>CS FORM No. 32</i></b>
                     <br>
                     Revised 2017
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <p style="text-indent: 10px;">
                        I, <u><?php echo $FullName; ?></u> of ______________(Address of the Appointee)__________________ having been appointed to the position of ______________(Position Title)____________ hereby solemnly swear, that I will faithfully discharge to the best of my ability, the duties of my present position and of all others that I may hereafter hold under the Republic of the Philippines; that I will bear true faith and allegiance to the same; that I will obey the laws, legal orders, and decrees promulgated by the duly constituted authorities of the Republic of the Philippines; and that I impose this obligation upon myself voluntarily, without mental reservation or purpose of evasion.   
                     </p>
                     <p style="text-indent: 10px;">
                       SO HELP ME GOD 
                     </p>
                     <br>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12 text-right">
                     _________________________________________
                     <br>
                     (Signature over Printed Name of the Appointee)
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     Government ID: _______________________
                     ID Number: _______________________
                     Date Issued: _______________________
                  </div>
               </div>
               <?php bar(); ?>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     Subscribed and sworn to before me this _______ day of ___________________, 20___ in __________________________________, Philippines.
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12 text-right">
                     _________________________________________
                     <br>
                     (Signature over Printed Name of the Appointing Officer/Authority/ Head of Office)
                  </div>
               </div>
            </div>
         </div>

         <?php
               }
            }
         ?>
         
      </div>
   </body>
</html>