<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $count = 0;
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <style type="text/css">
         
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Summary of L&D Interventions");
         ?>
         <div class="row">
            <div class="col-xs-12 text-center">
               Quarter:______, Year:_________
            </div>
         </div>
         <br><br>
         <div class="row">
            <div class="col-xs-12">
               Part I: Summary
            </div>
         </div>
         <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-11">
               A. Total Number of Active Employees in the Quarter:_____
               <br>
               B. Total Number of Active Employees in the Quarter with at least 1 L&D Intervention:_____
               <br>
               C. Total Cost Involved:____
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12">
               Part II: Data
               <br><br>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12">
               <table style="width: 100%;">
                  <thead>
                     <tr class="colHEADER">
                        <th>Name</th>
                        <th>Number of L&D <br>Interventions</th>
                        <th>Provider</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Cost</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $rs = SelectEach("employees","WHERE RefId > 0 AND (Inactive != 1 OR Inactive IS NULL)");
                        if ($rs) {
                           while ($row = mysqli_fetch_assoc($rs)) {
                              $check_name    = "";
                              $emprefid      = $row["RefId"];
                              $LastName      = $row["LastName"];
                              $FirstName     = $row["FirstName"];
                              $MiddleName    = $row["MiddleName"];
                              $MiddleInitial = substr($MiddleName, 0,1);
                              $FullName      = $FirstName." ".$MiddleInitial.". ".$LastName;
                              $where         = "WHERE EmployeesRefId = '$emprefid'";
                              $new_rs        = SelectEach("ldmslndintervention",$where);
                              if ($new_rs) {
                                 $row_count  = mysqli_num_rows($new_rs);
                                 while ($new_row = mysqli_fetch_assoc($new_rs)) {
                                    $Name       = $new_row["Name"];
                                    $StartDate  = $new_row["StartDate"];
                                    $EndDate    = $new_row["EndDate"];
                                    $Provider   = $new_row["Provider"];
                                    $Cost       = $new_row["Cost"];
                                    echo '<tr>';
                                       if ($check_name == $FullName) {
                                          echo '<td>&nbsp;</td>';
                                       } else {
                                          echo '<td>'.$FullName.'</td>';
                                          $check_name = $FullName;
                                       }
                                       echo '<td>'.$Name.'</td>';
                                       echo '<td class="text-center">'.$Provider.'</td>';
                                       echo '<td class="text-center">'.date("F d, Y",strtotime($StartDate)).'</td>';
                                       echo '<td class="text-center">'.date("F d, Y",strtotime($EndDate)).'</td>';
                                       echo '<td class="text-center">'.number_format(intval($Cost),2).'</td>';
                                    echo '</tr>';
                                 }
                              }
                           }
                        }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </body>
</html>
