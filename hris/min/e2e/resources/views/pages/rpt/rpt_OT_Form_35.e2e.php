<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         @media print {
            table {
               font-size: 6pt !important;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row" style="page-break-after: always;">
            <div class="col-xs-12">
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <?php
                        rptHeader(getvalue("RptName"),"HRMDS -T- 003");
                     ?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <table width="100%">
                        <thead>
                           <tr class="colHEADER">
                              <th>
                                 Date and No. of Hours Required
                              </th>
                              <th>
                                 Employee Name
                              </th>
                              <th>
                                 Work to be Accomplished
                              </th>
                              <th>
                                 Reason/Justification
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                              if ($rsEmployees) {
                                 $check = 0;
                                 while ($row = mysqli_fetch_assoc($rsEmployees)) {
                                    $emprefid   = $row["RefId"];
                                    $LastName   = $row["LastName"];
                                    $FirstName  = $row["FirstName"];
                                    $MiddleName = $row["MiddleName"];
                                    $ExtName    = $row["ExtName"];
                                    $FullName   = $FirstName." ".$ExtName." ".$MiddleName." ".$LastName;
                                    $AgencyId   = $row["AgencyId"];
                                    $rs_overtime = SelectEach("overtime_request","WHERE EmployeesRefId = '$emprefid'");
                                    if ($rs_overtime) {
                                       while ($row_overtime = mysqli_fetch_assoc($rs_overtime)) {
                                          $HoursReq         = $row_overtime["HoursReq"];
                                          $Accomplishment   = $row_overtime["Accomplishment"];
                                          $Justification    = $row_overtime["Justification"];
                                          $StartDate        = $row_overtime["StartDate"];
                                          $EndDate          = $row_overtime["EndDate"];
                                          if ($StartDate == $EndDate) {
                                             $covered_date = date("F d, Y",strtotime($StartDate));
                                          } else {
                                             $covered_date = date("F d, Y",strtotime($StartDate))." to ".date("F d, Y",strtotime($EndDate));;
                                          }
                                          echo '<tr>';
                                          echo '<td class="text-center">'.$covered_date.',  '.intval($HoursReq).' Hours</td>';
                                          echo '<td>'.$FullName.'</td>';
                                          echo '<td>'.$Accomplishment.'</td>';
                                          echo '<td>'.$Justification.'</td>';
                                          echo '</tr>';
                                       }
                                    }

                           ?>
                           <?php
                                 }
                              }
                           ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>