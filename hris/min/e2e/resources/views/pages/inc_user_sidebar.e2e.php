<?php
   if (getvalue("hCompanyID") == 35) {
      echo
      '<div>
         <div class="sideMenu" route="Request" pre="ams" id="amsReqs">
            <img src="' . img("request.png") . '" class="pngIcons">
            <span style="margin-left:10px;">APPLICATION</span>
         </div>
         <div id="submenuReq">';
            if (getvalue("hCompanyID") != 1000) {
               $menu = [
                  array("route"=>"ReqChangeShift","id"=>"amsChangeShift","label"=>"Change Shift"),
                  array("route"=>"ReqOvertime","id"=>"amsOvertime","label"=>"Overtime"),
                  array("route"=>"ReqForceLeave","id"=>"amsForceLeave","label"=>"Cancellation of Leave"),
                  array("route"=>"AttendanceRequest","id"=>"amsAttendanceReq","label"=>"Attendance Registration")
               ];
            } else {
               $menu = [
                  array("route"=>"ReqOvertime","id"=>"amsOvertime","label"=>"Overtime"),
                  array("route"=>"ReqForceLeave","id"=>"amsForceLeave","label"=>"Cancellation of Leave")
               ];
            }
            foreach($menu as $attr) {
               echo
               '<div class="subMenu" route="'.$attr["route"].'" pre="ams" id="'.$attr["id"].'" memof="amsReqs">
                  <span style="margin-left:30px;">'.$attr["label"].'</span>
               </div>'."\n";
            }
            $menu = [
               array("route"=>"AvailLeave","id"=>"amsLeave","label"=>"Leave"),
               array("route"=>"AvailCTO","id"=>"amsCTO","label"=>"CTO"),
               array("route"=>"AvailAuthority","id"=>"amsAuthority","label"=>"Office Authority"),
               array("route"=>"AvailLeaveMonitization","id"=>"amsLeaveMonitize","label"=>"Leave Monetization")
            ];
            foreach($menu as $attr) {
               echo
               '<div class="subMenu" route="'.$attr["route"].'" pre="ams" id="'.$attr["id"].'" memof="amsAvail">
                  <span style="margin-left:30px;">'.$attr["label"].'</span>
               </div>'."\n";
            }
         echo
         '</div>
      </div>';
   } else {
      echo
      '<div>
         <div class="sideMenu" route="Request" pre="ams" id="amsReqs">
            <img src="' . img("request.png") . '" class="pngIcons">
            <span style="margin-left:10px;">REQUEST</span>
         </div>
         <div id="submenuReq">';
            if (getvalue("hCompanyID") != 1000) {
               $menu = [
                  array("route"=>"ReqChangeShift","id"=>"amsChangeShift","label"=>"Change Shift"),
                  array("route"=>"ReqOvertime","id"=>"amsOvertime","label"=>"Overtime"),
                  array("route"=>"ReqForceLeave","id"=>"amsForceLeave","label"=>"Cancellation of Leave"),
                  array("route"=>"AttendanceRequest","id"=>"amsAttendanceReq","label"=>"Attendance Registration")
               ];
            } else {
               $menu = [
                  array("route"=>"ReqOvertime","id"=>"amsOvertime","label"=>"Overtime"),
                  array("route"=>"ReqForceLeave","id"=>"amsForceLeave","label"=>"Cancellation of Leave")
               ];
            }
            foreach($menu as $attr) {
               echo
               '<div class="subMenu" route="'.$attr["route"].'" pre="ams" id="'.$attr["id"].'" memof="amsReqs">
                  <span style="margin-left:30px;">'.$attr["label"].'</span>
               </div>'."\n";
            }
         echo
         '</div>
      </div>
      <div>
         <div class="sideMenu" route="Availment" pre="ams" id="amsAvail">
            <img src="' . img("availability.png") . '" class="pngIcons">
            <span style="margin-left:10px;">AVAILMENT</span>
         </div>
         <div id="submenuAvail">';
            $menu = [
               array("route"=>"AvailLeave","id"=>"amsLeave","label"=>"Leave"),
               array("route"=>"AvailCTO","id"=>"amsCTO","label"=>"CTO"),
               array("route"=>"AvailAuthority","id"=>"amsAuthority","label"=>"Office Authority"),
               array("route"=>"AvailLeaveMonitization","id"=>"amsLeaveMonitize","label"=>"Leave Monetization")
            ];
            foreach($menu as $attr) {
               echo
               '<div class="subMenu" route="'.$attr["route"].'" pre="ams" id="'.$attr["id"].'" memof="amsAvail">
                  <span style="margin-left:30px;">'.$attr["label"].'</span>
               </div>'."\n";
            }
         echo
         '</div>
      </div>';   
   }
   
   
   
?>
