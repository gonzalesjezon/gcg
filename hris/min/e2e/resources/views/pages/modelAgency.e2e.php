<?php
   require_once "constant.e2e.php";
   require_once "inc_model.e2e.php";
   require_once "incUtilitiesJS.e2e.php";
?>
   <div class="container" id="EntryScrn">
      <div class="row">
         <div class="col-xs-6">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } ?>
            <!--
            <div class="row margin-top10">
               <div class="form-group">
                  <label class="control-label" for="inputs">CODE:</label><br>
                  <input class="form-input saveFields-- uCase-- mandatory" type="text" placeholder="Code" <?php if ($ScrnMode == 2) { echo "disabled"; } else { echo $disabled; }  ?>
                     id="inputs" name="char_Code" style='width:75%' value="<?php echo $code; ?>" autofocus>
               </div>
            </div>
            -->
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">NAME:</label><br>
                  <input class="form-input saveFields-- mandatory uCase--" type="text" placeholder="name" <?php echo $disabled; ?>
                     id="inputs" name="char_Name" style='width:75%' value="<?php echo $name; ?>">
               </div>
            </div>
            <div class="row" style="display:none">
               <input type="checkbox" class="saveFields--" value=0 id="sint_IsPrivate" name="sint_IsPrivate">
               <label class="control-label" for="sint_IsPrivate">Private Agency?</label>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">REMARKS:</label>
                  <textarea class="form-input saveFields--" rows="5" name="char_Remarks" <?php echo $disabled; ?>
                  placeholder="remarks"><?php echo $remarks; ?></textarea>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script type="text/javascript">
      $(document).ready(function () {
         if ($("#isPOP").val() == "yes") {
            var idx = $("[name='dropobj']").val().split("_")[2];
            var isGovt = opener.$("[name='sint_isGovtService_"+idx+"']").val();
            if (isGovt == 1) {
               $("#sint_IsPrivate").val(0);
               $("#sint_IsPrivate").prop("checked",false);
            } else {
               $("#sint_IsPrivate").val(1);
               $("#sint_IsPrivate").prop("checked",true);
               //$("#sint_IsPrivate").prop("disabled",true);
            } 
            $("#sint_IsPrivate").click(function () {
               if (isGovt == 1) {
                  $("#sint_IsPrivate").val(0);
                  $.notify("You are already adding in government agency");
                  $("#sint_IsPrivate").prop("checked",false);
               } else {
                  $("#sint_IsPrivate").val(1);
                  $.notify("You are already adding in private agency");
                  $("#sint_IsPrivate").prop("checked",true);
               }
            });
         }
      });
   </script>