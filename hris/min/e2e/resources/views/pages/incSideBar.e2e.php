<div class="margin-top">
   <div>
      <div id="menuPIS">
         <div class="sideMenu">
            <img src="<?php echo img("empinfo.png"); ?>" class="pngIcons">&nbsp;PIS
         </div>
      </div>
      <div id="submenuPIS">
         <div>
            <?php doRSideBar(""); ?>
         </div>
      </div>
   </div>

   <div>
      <div id="menuRMS">
         <div class="sideMenu">
            <img src="<?php echo img("RMS.png"); ?>" class="pngIcons">&nbsp;RMS
         </div>
      </div>
      <div id="submenuRMS">
         <div>
            <?php doRMS_SideBar(); ?>
         </div>
      </div>
   </div>

   <div>
      <div id="menuSPMS">
         <div class="sideMenu">
            <img src="<?php echo img("SPMS.png"); ?>" class="pngIcons">&nbsp;SPMS
         </div>
      </div>
      <div id="submenuSPMS">
         <div>
            <?php doSPMS_SideBar(); ?>
         </div>
      </div>
   </div>
</div>