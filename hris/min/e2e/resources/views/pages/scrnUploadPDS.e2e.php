<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require 'conn.e2e.php';
   $isUploaded = 0;
   $msg = "";
?>

<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_Upload"); ?>"></script>
   </head>
   <body>
      <form name="xForm" id="xForm" action="<?php echo $fileAction; ?>"
            method="post"
            enctype="multipart/form-data">
         <?php $sys->SysHdr($sys,'pis') ?>
         <div class="container-fluid" id="mainScreen">
         <?php
            $path = $_SESSION['path'];
            $target_dir = $path."uploads/";
            $new_target_dir = $path."doc/PDS/";
            $new_target_dir_olds = $path."doc/PDS/olds/";
            $uploadOk = 1;
            if(isset($_POST["hSubmit"]) && $_POST["hSubmit"] == "YES") {

               //if ($_FILES["fileToUpload"]["type"] == "application/vnd.ms-excel") {

                  $old_filename = basename($_FILES["fileToUpload"]["name"]);
                  $docFileType = pathinfo($target_dir.$old_filename,PATHINFO_EXTENSION);
                  $date_today = date("Ymd",time());
                  $curr_time  = date("his",time());
                  $fname = $date_today."_".$curr_time."_PDS";
                  $new_filename = $fname.".".$docFileType;
                  $target_file = $new_target_dir . $new_filename;
                  $uploadOk = 1;

                  // Check file size
                  if ($_FILES["fileToUpload"]["size"] > 1000000) {
                      $msg .= "<br>Sorry, your file is too large.";
                      $uploadOk = 0;
                  }
                  // Allow certain file formats
                  if(strtolower($docFileType) != "xls" &&
                     strtolower($docFileType) != "xlsx")
                  {
                      $msg .= "<br>Sorry, only .xls or xlsx are allowed.";
                      $uploadOk = 0;
                  }
                  if ($uploadOk == 0) {
                      $msg .= "<br>Sorry, your file was not uploaded.";
                  } else {
                     if (file_exists($target_file)) {
                        $timestamp = time();
                        $date_today = date("Ymd",$timestamp);
                        $curr_time  = date("his",$timestamp);
                        $source = $target_file;
                        $dest = $new_target_dir_olds.$fname."_".$date_today."_".$curr_time.".".$docFileType;
                        copy($source, $dest);
                        //unlink($source);
                     }
                     if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                        //$msg .= "<br>The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                        $isUploaded = 1;
                        $msg = '<h4>PDS Successfully Uploaded</span></h4>';
                        $msg .= '<h5>To view & save <a href="javascript:void(0);"
                                     onclick="popPDS(\'ReadPDS\',\'&xls='.$new_filename.'\');"> click here</a>
                                </h5>';
                     } else {
                        $msg .= "<br>Sorry, there was an error uploading your file.";
                     }
                  }
               //} else {
               //   echo "ERR : not application/vnd.ms-excel > ".$_FILES["fileToUpload"]["type"];
               //}

            }
         ?>
         <?php doTitleBar($modTitle); ?>
         <div class="container-fluid margin-top10">
            <?php
               //EmployeesSearch();
            ?>
            <div class="row">
               <div class="col-xs-12" id="div_CONTENT">
                  <?php
                     if ($msg !== "") {
                        alert("Information",$msg);
                     }
                  ?>
                     <div class="mypanel">
                        <div class="panel-top">
                           <a href="javascript:void(0)" class="mbar" title="Slide Up this" id="minPDSUpload">
                              <i class="fa fa-minus-square" aria-hidden="true"></i>
                           </a>
                           <a href="javascript:void(0)" class="mbar" title="Open PDS" id="openPDS">
                              <i class="fa fa-openid" aria-hidden="true"></i>
                           </a>
                           SELECT FILE TO UPLOAD:
                        </div>
                        <div class="panel-mid" id="PDSUpload">
                           <div class="border2 padd15 margin-top">
                              <label>PDS FILE:</label>
                              <input type="file" name="fileToUpload" id="fileToUpload">
                           </div>
                           <div class="padd10 margin-top10">
                              <input type="hidden" id="hSubmit" name="hSubmit" value="">
                              <button type="button"
                                      value="Upload"
                                      class="btn-cls4-sea trnbtn"
                                      name="btnSUBMIT" onclick="submitUploadForm()">
                                 <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;
                                 SUBMIT
                              </button>
                              <button type="button"
                                      class="btn-cls4-tree trnbtn"
                                      id="btnCLOSE">
                                 <i class="fa fa-times" aria-hidden="true"></i>&nbsp;
                                 CLOSE
                              </button>
                           </div>
                        </div>
                     </div>
               </div>
            </div>
         </div>
         <input type="hidden" id="hCaller" value="<?php echo getvalue("hCaller"); ?>">
         <?php
            footer();
            include "varHidden.e2e.php";
         ?>
         </div>
      </form>
   </body>
</html>







