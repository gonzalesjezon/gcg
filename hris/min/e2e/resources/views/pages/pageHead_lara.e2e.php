<?php
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>e2e</title>
<script type="text/javascript" src="<?php echo $sys->host('js/angular1.4.8.js'); ?>"></script>
<link href="<?php echo $sys->host('css/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo $sys->host('jquery/jquery.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo $sys->host('bs/css/bootstrap.min.css"'); ?>" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="<?php echo $sys->host('bs/js/bootstrap.min.js'); ?>" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="stylesheet" href="<?php echo $sys->host('jqueryui/jquery-ui.css'); ?>">
<script type="text/javascript" src="<?php echo $sys->host('jqueryui/jquery-ui.js'); ?>"></script>
<link href="<?php echo $sys->host('datatables/jquery.dataTables.min.css'); ?>" rel="stylesheet">
<script type="text/javascript" src="<?php echo $sys->host('datatables/jquery.dataTables.min.js'); ?>"></script>

<script type="text/javascript" src="<?php echo $sys->host('js/0620utilities.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $sys->host('js/0620functions.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $sys->host('js/0620sys.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $sys->host('js/toolTip.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $sys->host('js/jquery_utilities.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo $sys->host('css/'.$mynewfile); ?>">
<link rel="stylesheet" href="<?php echo $sys->host('css/tip.css'); ?>">
