<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_AfterTrn"); ?>"></script>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_IPAR"); ?>"></script>
      <style type="text/css">
         .td-input {
            padding: 2px;
         }
      </style>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("INDIVIDUAL PERFORMANCE ACCOMPLISHMENT AND REVIEW [IPAR]"); ?>
            <div class="container-fluid margin-top10">
               <div class="row" id="spmsList">
                  <?php
                     if (getvalue("hUserGroup") != "COMPEMP") {
                  ?>
                  <div class="col-xs-3">
                     <?php
                        employeeSelector();
                     ?>
                  </div>
                  <div class="col-xs-9">
                  <?php
                     } else {
                        echo '<div class="col-xs-12">';   
                     }
                  ?>
                     <div class="panel-top">
                        LIST OF PERFORMANCE MANAGEMENT OF 
                        <span id="selectedEmployees">&nbsp;</span>
                     </div>
                     <div class="panel-mid">
                        <div id="spGridTable">
                        </div>
                     </div>
                     <div class="panel-bottom">
                        <button type="button"
                             class="btn-cls4-sea trnbtn"
                             id="btnINSERTSPMS" name="btnINSERTSPMS">
                           <i class="fa fa-file" aria-hidden="true"></i>
                           &nbsp;Insert New
                        </button>
                     </div>
                  </div>
               </div>
               <div class="row" id="insert_spms">
                  <div class="col-xs-12">
                     <div class="panel-top">ADD NEW</div>
                     <div class="panel-mid">
                        <div class="row">
                           <div class="col-xs-4">
                              <label>Quarter</label>
                              <select class="form-input" name="sint_Quarter" id="sint_Quarter">
                                 <option value="">Select Quarter</option>
                                 <option value="1">1st Quarter</option>
                                 <option value="2">2nd Quarter</option>
                                 <option value="3">3rd Quarter</option>
                                 <option value="4">4th Quarter</option>
                              </select>
                           </div>
                           <div class="col-xs-4">
                              <label>Year</label>
                              <select class="form-input" name="sint_Year" id="sint_Year">
                                 <?php
                                    $past_year = date("Y",time()) - 2;
                                    $curr_year = date("Y",time());
                                    $future_year = date("Y",time()) + 2;
                                    for ($i=$past_year; $i <= $future_year ; $i++) { 
                                       if ($i == $curr_year) {
                                          echo '<option value="'.$i.'" selected>'.$i.'</option>';   
                                       } else {
                                          echo '<option value="'.$i.'">'.$i.'</option>';
                                       }
                                       
                                    }
                                 ?>
                              </select>
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-12">
                              <table style="width: 100%" border="1">
                                 <thead>
                                    <tr>
                                       <th class="text-center" rowspan="2" style="width: 15%;">
                                          Objective
                                       </th>
                                       <th class="text-center" rowspan="2" style="width: 5%;">
                                          %
                                       </th>
                                       <th class="text-center" style="width: 20%;">
                                          Success Indicator<br>(SI)
                                       </th>
                                       <th class="text-center" rowspan="2" style="width: 10%;">
                                          SI TYPE<br>(Qt Qi T)
                                       </th>
                                       <th class="text-center" rowspan="2">
                                          Actual<br>Accomplishment
                                       </th>
                                       <th class="text-center" rowspan="2" style="width: 10%;">
                                          Numerical<br>Rating
                                       </th>
                                       <th class="text-center" rowspan="2" style="width: 10%;">
                                          Adjectival<br>Rating
                                       </th>
                                       <th class="text-center" rowspan="2" style="width: 10%;">
                                          Remarks
                                       </th>
                                    </tr>
                                    <tr>
                                       <th class="text-center">for an equivalent rating of 3</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td colspan="8" class="panel-top">
                                          <b>Strategic Priorities (If Applicable)</b>
                                       </td>
                                    </tr>
                                    <?php
                                       for ($a=1; $a <= 5; $a++) { 
                                          echo '<tr id="sprow_'.$a.'">';
                                          echo '
                                          <td class="td-input">
                                             <input type="hidden"
                                                    name="sp_refid_'.$a.'"
                                                    id="sp_refid_'.$a.'"
                                                    value="0">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sp_objectives_'.$a.'" 
                                                    id="sp_objectives_'.$a.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sp_percent_'.$a.'" 
                                                    id="sp_percent_'.$a.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sp_si_'.$a.'" 
                                                    id="sp_si_'.$a.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sp_sitype_'.$a.'" 
                                                    id="sp_sitype_'.$a.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sp_accomplishment_'.$a.'" 
                                                    id="sp_accomplishment_'.$a.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sp_numerical_'.$a.'" 
                                                    id="sp_numerical_'.$a.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sp_adjectival_'.$a.'" 
                                                    id="sp_adjectival_'.$a.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sp_remarks_'.$a.'" 
                                                    id="sp_remarks_'.$a.'">
                                          </td>
                                          ';
                                          echo '</tr>';
                                       }
                                    ?>
                                    <tr>
                                       <td colspan="8" class="td-input">
                                          <button type="button" class="btn-cls4-tree" id="addrow_sp">
                                             Add row
                                          </button>
                                          <button type="button" class="btn-cls4-red" id="delete_sp">
                                             Delete last row
                                          </button>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="8" class="panel-top">
                                          <b>Core Functions</b>
                                       </td>
                                    </tr>
                                    <?php
                                       for ($b=1; $b <= 5; $b++) { 
                                          echo '<tr id="cfrow_'.$b.'">';
                                          echo '
                                          <td class="td-input">
                                             <input type="hidden"
                                                    name="cf_refid_'.$b.'"
                                                    id="cf_refid_'.$b.'"
                                                    value="0">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="cf_objectives_'.$b.'" 
                                                    id="cf_objectives_'.$b.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="cf_percent_'.$b.'" 
                                                    id="cf_percent_'.$b.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="cf_si_'.$b.'" 
                                                    id="cf_si_'.$b.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="cf_sitype_'.$b.'" 
                                                    id="cf_sitype_'.$b.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="cf_accomplishment_'.$b.'" 
                                                    id="cf_accomplishment_'.$b.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="cf_numerical_'.$b.'" 
                                                    id="cf_numerical_'.$b.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="cf_adjectival_'.$b.'" 
                                                    id="cf_adjectival_'.$b.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="cf_remarks_'.$b.'" 
                                                    id="cf_remarks_'.$b.'">
                                          </td>
                                          ';
                                          echo '</tr>';
                                       }
                                    ?>
                                    <tr>
                                       <td colspan="8" class="td-input">
                                          <button type="button" class="btn-cls4-tree" id="addrow_cf">
                                             Add row
                                          </button>
                                          <button type="button" class="btn-cls4-red" id="delete_cf">
                                             Delete last row
                                          </button>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="8" class="panel-top">
                                          <b>Support Functions (If Applicable)</b>
                                       </td>
                                    </tr>
                                    <?php
                                       for ($c=1; $c <= 5; $c++) { 
                                          echo '<tr id="sfrow_'.$c.'">';
                                          echo '
                                          <td class="td-input">
                                             <input type="hidden"
                                                    name="sf_refid_'.$c.'"
                                                    id="sf_refid_'.$c.'"
                                                    value="0">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sf_objectives_'.$c.'" 
                                                    id="sf_objectives_'.$c.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sf_percent_'.$c.'" 
                                                    id="sf_percent_'.$c.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sf_si_'.$c.'" 
                                                    id="sf_si_'.$c.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sf_sitype_'.$c.'" 
                                                    id="sf_sitype_'.$c.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sf_accomplishment_'.$c.'" 
                                                    id="sf_accomplishment_'.$c.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sf_numerical_'.$c.'" 
                                                    id="sf_numerical_'.$c.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sf_adjectival_'.$c.'" 
                                                    id="sf_adjectival_'.$c.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="sf_remarks_'.$c.'" 
                                                    id="sf_remarks_'.$c.'">
                                          </td>
                                          ';
                                          echo '</tr>';
                                       }
                                    ?>
                                    <tr>
                                       <td colspan="8" class="td-input">
                                          <button type="button" class="btn-cls4-tree" id="addrow_sf">
                                             Add row
                                          </button>
                                          <button type="button" class="btn-cls4-red" id="delete_sf">
                                             Delete last row
                                          </button>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="5" class="text-center td-input">
                                          <label>Average Rating for the Period</label>
                                       </td>
                                       <td class="td-input">
                                          <input type="text" 
                                                 class="form-input" 
                                                 name="sint_AverageNumerical" 
                                                 id="sint_AverageNumerical">
                                       </td>
                                       <td class="td-input">
                                          <input type="text" 
                                                 class="form-input" 
                                                 name="sint_AverageAdjectival" 
                                                 id="sint_AverageAdjectival">
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>  
                     </div>
                     <div class="panel-bottom">
                        <input type="hidden" name="sint_IPARRefId" id="sint_IPARRefId" value="0">
                        <input type="hidden" name="sint_EmployeesRefId" id="sint_EmployeesRefId" value="0">
                        <input type="hidden" name="count_sp" id="count_sp">
                        <input type="hidden" name="count_cf" id="count_cf">
                        <input type="hidden" name="count_sf" id="count_sf">
                        <input type="hidden" name="fn" id="fn" value="saveIPAR">
                        <button type="button" id="btnSAVEIPCR" class="btn-cls4-sea">
                           <i class="fa fa-save"></i>&nbsp;SAVE
                        </button>
                        <button type="button" id="btnCANCELIPCR" class="btn-cls4-red">
                           <i class="fa fa-times"></i>&nbsp;CANCEL
                        </button>
                        <button type="button" id="btnEDITIPCR" class="btn-cls4-sea">
                           <i class="fa fa-save"></i>&nbsp;UPDATE
                        </button>
                        <button type="button" id="btnBACKIPCR" class="btn-cls4-red">
                           <i class="fa fa-backward"></i>&nbsp;BACK
                        </button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "spms_ipcr";
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
</html>