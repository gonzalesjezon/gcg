<?php
   $disabled = "disabled";
   $mode = "";
   $user = getvalue("hUser");
   $mode = "VIEW";
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo $path."/css/sideBar.css"; ?>">
   </head>
   <body onload="loadRecord(1);">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <nav class="navbar navbar-fixed-top">
            <div class="sysNameHolder sysBG" style="border-bottom:3px solid #fff;">
               <?php
                  $TRNBTN = 0;
                  $title = "";
                  $Logout = true;
                  include_once $files["inc"]["hdr"];
               ?>
               <span class="sysName">Human Resources And Information System</span>
            </div>
         </nav>
         <div style="margin-top:60px;">
            <?php doSideBarMain(); ?>
            <div class="container-fluid" id="mainScreen">
               <?php doTitleBar("Company Profile"); ?>
               <div class="container-fluid margin-top">
                     <div class="row">
                        <div class="col-xs-12">
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <div class="row">
                                    <div class="col-xs-12">
                                       <span id="spADDEDITPRINT">
                                          <?php if (!$rs) {?>
                                          <button type="button"
                                                  class="btn-cls4-sea trnbtn"
                                                  id="btnADD" name="btnADD">
                                             <i class="fa fa-plus-circle" aria-hidden="true"></i>ADD
                                          </button>
                                          <?php }?>
                                          <button type="button"
                                                  class="btn-cls4-sea trnbtn"
                                                  id="btnEDIT" name="btnEDIT">
                                             <i class="fa fa-pencil-square-o" aria-hidden="true"></i>EDIT
                                          </button>
                                          <button type="button"
                                                  class="btn-cls4-lemon trnbtn"
                                                  id="btnPRINT" name="btnPRINT">
                                             <i class="fa fa-print" aria-hidden="true"></i>PRINT
                                          </button>
                                          <button type="button"
                                                  class="btn-cls4-red trnbtn"
                                                  id="btnEXIT" name="btnEXIT">
                                             <i class="fa fa-times" aria-hidden="true"></i>EXIT
                                          </button>
                                       </span>
                                       <span id="spSAVECANCEL" style="display:none;">
                                          <button type="button"
                                                  class="btn-cls4-sea trnbtn"
                                                  id="btnSAVE" name="btnSAVE">
                                             <i class="fa fa-floppy-o" aria-hidden="true"></i>SAVE
                                          </button>
                                          <button type="button"
                                                  class="btn-cls4-red trnbtn"
                                                  id="btnCANCEL" name="btnCANCEL">
                                             <i class="fa fa-undo" aria-hidden="true"></i>CANCEL
                                          </button>
                                       </span>
                                    </div>
                                 </div>
                                 <?php bar(); ?>
                                 <div class="row">
                                    <div class="col-xs-2">
                                       <span class="label">Company Code</span>
                                    </div>
                                    <div class="col-xs-10">
                                       <input type="text" class="form-input mandatory--" id="Code" name="char_Code" style="width:30%;"/>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-2">
                                       <span class="label">Company Name</span>
                                    </div>
                                    <div class="col-xs-10">
                                       <input type="text" class="form-input mandatory--" name="char_Name" autofocus>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-3"></div>
                                    <div class="col-xs-3">
                                       <span class="label">Main&nbsp;</span>
                                       <input type="checkbox" class="form-input" name="sint_isMain" style="width:30%;">
                                    </div>

                                    <div class="col-xs-6">
                                       <input type="Hidden" class="HiddenEntry flds" name="char_CompanyType" value="1">
                                       <span class="label">COMPANY TYPE:&nbsp;</span>
                                       <input type="radio" id="rad_Govt" name="compType" checked>
                                       <span class="label">&nbsp;Government</span>
                                       &nbsp;
                                       <input type="radio" id="rad_Private" name="compType">
                                       <span class="label">&nbsp;Private</span>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-xs-4">
                                       <span><h4>Contact Info</h4></span>
                                    </div>
                                 </div>
                                 <?php bar(); ?>
                                 <div class="row" >
                                    <div class="col-xs-2 txt-right">
                                       <span class="label">Address</span>
                                    </div>
                                    <div class="col-xs-10">
                                       <input type="text" class="form-input" name="char_Address">
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-2 txt-right">
                                       <span class="label">Country</span>
                                    </div>
                                    <div class="col-xs-2">
                                       <?php
                                          createSelect("Country",
                                                       "sint_CountryRefId",
                                                       "",100,"Name","Select Country","");
                                       ?>
                                    </div>
                                    <div class="col-xs-2 txt-right">
                                       <span class="label">Province</span>
                                    </div>
                                    <div class="col-xs-2">
                                       <?php
                                          createSelect("Province",
                                                       "sint_ProvinceRefId",
                                                       "",100,"Name","Select Province","");
                                       ?>
                                    </div>
                                    <div class="col-xs-2 txt-right">
                                       <span class="label">City/Municipality</span>
                                    </div>
                                    <div class="col-xs-2">
                                       <?php
                                          createSelect("City",
                                                       "sint_CityRefId",
                                                       "",100,"Name","Select City","");
                                       ?>

                                    </div>
                                 </div>


                                 <div class="row margin-top">
                                    <div class="col-xs-2 txt-right">
                                       <span class="label">Zip Code</span>
                                    </div>
                                    <div class="col-xs-6">
                                       <input type="text" class="form-input" name="char_ZipCode" />
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-2 txt-right">
                                       <span class="label">Telephone No.</span>
                                    </div>
                                    <div class="col-xs-2">
                                       <input type="text" class="form-input" name="char_TelNo"/>
                                    </div>
                                    <div class="col-xs-2 txt-right">
                                       <span class="label">E-mail Add</span>
                                    </div>
                                    <div class="col-xs-2">
                                       <input type="text" class="form-input" name="char_EmailAdd"/>
                                    </div>
                                    <div class="col-xs-1 txt-right">
                                       <span class="label">Website</span>
                                    </div>
                                    <div class="col-xs-3">
                                       <input type="text" class="form-input" name="char_Website"/>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-xs-4">
                                       <span class=""><h4>Signatory Info</h4></span>
                                    </div>
                                 </div>
                                 <?php bar(); ?>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Label 1</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_Label1"/>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Name 1</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_Name1"/>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Position 1</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_Position1"/>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="col-xs-6 txt-right">
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Label 2</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_Label2"/>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Name 2</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_Name2"/>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Position 2</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_Position2"/>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Label 3</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_Label3"/>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Name 3</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_Name3"/>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Position 3</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_Position3"/>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-xs-6 txt-right">
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Label 4</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_Label4"/>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Name 4</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_Name4"/>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Position 4</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_Position4"/>
                                          </div>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="row margin-top">
                                    <div class="col-xs-6">
                                       <div class="row">
                                          <div class="col-xs-12">
                                             <span class=""><h4>LOGO</h4></span>
                                          </div>
                                       </div>
                                       <?php bar();?>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 txt-right">
                                             <span class="label">Logo</span>
                                          </div>
                                          <div class="col-xs-8">
                                             <input type="text" class="form-input" name="char_Logo"/>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 txt-right">
                                             <span class="label">Big Logo</span>
                                          </div>
                                          <div class="col-xs-8">
                                             <input type="text" class="form-input" name="char_BigLogo"/>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 txt-right">
                                             <span class="label">Small Logo</span>
                                          </div>
                                          <div class="col-xs-8">
                                             <input type="text" class="form-input" name="char_SmallLogo"/>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-4 txt-right">
                                             <span class="label">Background Pattern</span>
                                          </div>
                                          <div class="col-xs-8">
                                             <input type="text" class="form-input" name="char_PatternLogo"/>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-xs-6">
                                       <div class="row">
                                          <div class="col-xs-12">
                                             <span class=""><h4>COMPANY COLOR</h4></span>
                                          </div>
                                       </div>
                                       <?php bar();?>
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Color 1</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_color1"/>
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Color 2</span>
                                          </div>
                                          <div class="col-xs-9">
                                             <input type="text" class="form-input" name="char_color2"/>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-xs-12">
                                       <span class=""><h4>Remarks</h4></span>
                                    </div>
                                 </div>
                                 <?php bar();?>
                                 <div class="row margin-top">
                                    <div class="col-xs-2">
                                       <span class="label"></span>
                                    </div>
                                    <div class="col-xs-4">
                                       <textarea name="char_Remarks" class="form-input" row="5"></textarea>
                                    </div>
                                 </div>
                              </div>
                              <!--<div class="col-xs-2">
                              <img src="<?php //echo img("nopic.png")?>" style="margin-bottom:3px;border:1px solid #aaa;width:1.2in;height:1.5in;"/><br>
                                 <button type="button"
                                         class="btn-cls4-sea trnbtn"
                                         id="btnUPLOADPIC" name="btnUPLOADPIC"
                                         title="Upload Picture"
                                         >
                                          <i class="fa fa-upload"></i>&nbsp;Upload Logo
                                 </button>
                              </div>-->
                           </div>
                        </div>
                     </div>
               </div>
               <input type="hidden" name="hNewToken" value="">
               <input type="hidden" name="char_hNewReToken" class="HiddenEntry" value="">
               <?php
                  footer();
                  $table = "company";
                  include "varHidden.e2e.php";
               ?>
            </div>
         </div>
         <script language="JavaScript" src="<?php echo jsCtrl("ctrl_CompanyProfile"); ?>"></script>
      </form>
   </body>
</html>


