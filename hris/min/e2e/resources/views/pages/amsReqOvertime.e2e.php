<?php 
   $module = module("ReqOvertime"); 
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            $("#special_box").hide();
            $("#btnPRINT").click(function () {
               popPDS("Overtime_2","");
            });
            $("#sint_OTClass").change(function () {
               var value = $(this).val();
               if (value > 1) {
                  $("#special_box").show();
               } else {
                  $("#special_box").hide();
               }
            });
            $("#LocSAVE").click(function () {
               if (saveProceed() == 0) {
                  $(this).attr("type","submit"); 
               }
            });
         });
         
         function afterDelete() {
            alert("Successfully Deleted");
            gotoscrn($("#hProg").val(),"");
         }
         function selectMe(refid){
            $("#rptContent").attr("src","blank.htm");
            var cid = $("#hCompanyID").val();
            var rptFile = "rpt_Overtime_" + cid;
            var url = "ReportCaller.e2e.php?file=" + rptFile;
            url += "&refid=" + refid;
            url += "&" + $("[name='hgParam']").val();
            $("#prnModal").modal();
            $("#rptContent").attr("src",url);
         }
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="postMultiple.e2e.php">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                        <div id="divList">
                           <div class="mypanel">
                              <div class="panel-top">List of Overtime Request</div>
                              <div class="panel-mid">
                                 <span id="spGridTable">
                                    <?php
                                       $sizeCol = "col-xs-6";
                                       if ($UserCode == "COMPEMP") {
                                          $sizeCol = "col-xs-12";
                                          $gridTableHdr_arr = ["File Date", "Start Date", "End Date", "With Pay", "Status"];
                                          $gridTableFld_arr = ["FiledDate", "StartDate", "EndDate", "WithPay", "Status"];
                                          $sql = "SELECT * FROM $table WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId ORDER BY StartDate DESC";
                                          $Action = [false,true,false,true];
                                       }
                                       doGridTable($table,
                                                   $gridTableHdr_arr,
                                                   $gridTableFld_arr,
                                                   $sql,
                                                   $Action,
                                                   "gridTable");
                                    ?>
                                 </span>
                              </div>
                              <div class="panel-bottom">
                                 <?php
                                    btnINRECLO([true,true,false]);
                                    echo '
                                       <!--<button type="button"
                                            class="btn-cls4-lemon trnbtn"
                                            id="btnPRINT" name="btnPRINT">
                                          <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                          PRINT
                                       </button>-->
                                    ';
                                 ?>
                              </div>
                           </div>
                        </div>
                        <div id="divView">
                           <?php
                              if ($UserCode == "COMPEMP") {
                           ?>
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="panel-top">
                                    Overtime Reminders:
                                 </div>
                                 <div class="panel-mid" style="padding: 10px;">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          REGULAR OT - (In excess of regular hours)
                                          <br>
                                          SPECIAL OT - (For overtime during weekends, special and non-special holidays and those rendered beyond 12:00 midnight)
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    &nbsp;
                                 </div>
                              </div>
                           </div>
                           <br>
                           <?php
                              }
                           ?>
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="mypanel">
                                    <div class="panel-top">
                                       <span id="ScreenMode">REQUESTING</span> <?php echo strtoupper($ScreenName); ?>

                                    </div>
                                    <div class="panel-mid">
                                       <div id="EntryScrn">
                                          <div class="row margin-top">
                                             <div class="<?php echo $sizeCol; ?>">
                                                <div class="row" id="badgeRefId">
                                                   <div class="col-xs-6">
                                                      <ul class="nav nav-pills">
                                                         <li class="active" style="font-size:12pt;font-weight:600;">
                                                            <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                            </span></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                                <?php
                                                   $attr = ["br"=>true,
                                                            "type"=>"text",
                                                            "row"=>true,
                                                            "name"=>"date_FiledDate",
                                                            "col"=>"4",
                                                            "id"=>"FiledDate",
                                                            "label"=>"Date File",
                                                            "class"=>"saveFields-- mandatory date--",
                                                            "style"=>"",
                                                            "other"=>""];
                                                   $form->eform($attr);
                                                   echo
                                                   '<div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <div class="row">';
                                                            $attr = ["br"=>true,
                                                                     "type"=>"text",
                                                                     "row"=>false,
                                                                     "name"=>"date_StartDate",
                                                                     "col"=>"6",
                                                                     "id"=>"StartDate",
                                                                     "label"=>"Start Date",
                                                                     "class"=>"saveFields-- mandatory date--",
                                                                     "style"=>"",
                                                                     "other"=>""];
                                                            $form->eform($attr);
                                                            $attr = ["br"=>true,
                                                                     "type"=>"text",
                                                                     "row"=>false,
                                                                     "name"=>"date_EndDate",
                                                                     "col"=>"6",
                                                                     "id"=>"EndDate",
                                                                     "label"=>"End Date",
                                                                     "class"=>"saveFields-- mandatory date--",
                                                                     "style"=>"",
                                                                     "other"=>""];
                                                            $form->eform($attr);
                                                   echo
                                                   '
                                                         </div>
                                                      </div>
                                                   </div>';
                                                ?>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <label>Overtime Classification</label>
                                                      <select name="sint_OTClass" class="saveFields-- form-input mandatory--" id="sint_OTClass">
                                                         <option value="">Select Overtime Classification</option>
                                                         <option value="1">Regular Overtime</option>
                                                         <option value="2">Special Overtime</option>
                                                      </select>
                                                   </div>
                                                </div>
                                                <div id="special_box">
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <label>Overtime Location</label>
                                                         <select name="sint_OTLocation" class="saveFields-- form-input" id="sint_OTLocation">
                                                            <option value="">Select Overtime Location</option>
                                                            <option value="1">On Site</option>
                                                            <option value="2">Off Site</option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <label>Time Coverage</label>
                                                      </div>
                                                   </div>
                                                   <div class="row">
                                                      <div class="col-xs-6">
                                                         <label>From</label>
                                                         <?php
                                                            drpTime("sint_FromTime",8,30,0);
                                                         ?>
                                                      </div>
                                                      <div class="col-xs-6">
                                                         <label>To</label>
                                                         <?php
                                                            drpTime("sint_ToTime",8,30,0);
                                                         ?>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <label>Overtime Type</label>
                                                      <select name="sint_WithPay" class="saveFields-- form-input" id="WithPay">
                                                         <option value="">Select Overtime Type</option>
                                                         <option value="0">CTO</option>
                                                         <?php
                                                            if ($CompanyId != "35") {
                                                               echo '<option value="1">OT Pay</option>';
                                                            }
                                                         ?>
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <label>Number of Hours Required</label>
                                                      <input type="text" name="sint_HoursReq" id="sint_HoursReq" class="number-- form-input saveFields--">
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <label class="control-label" for="inputs">Work To Be Accomplished:</label>
                                                      <textarea class="form-input saveFields--" 
                                                                rows="3" 
                                                                name="char_Accomplishment" 
                                                                id="char_Accomplishment" 
                                                                placeholder="Work To Be Accomplished"></textarea>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-xs-12">
                                                      <label class="control-label" for="inputs">Reason/Justification:</label>
                                                      <textarea class="form-input saveFields--" 
                                                                rows="3" 
                                                                name="char_Justification" 
                                                                id="char_Justification" 
                                                                placeholder="Reason/Justification"></textarea>
                                                   </div>
                                                </div>
                                                <?php
                                                   if ($UserCode == "COMPEMP") {
                                                ?>
                                                <div class="row">
                                                   <div class="col-xs-12">
                                                      <label class="control-label" for="inputs">Recommended By:</label>
                                                      <?php
                                                         createSelect("signatories",
                                                                      "sint_Signatory1",
                                                                      "",100,"Name","Select Signatory","",false);
                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-xs-12">
                                                      <label class="control-label" for="inputs">Approved By:</label>
                                                      <?php
                                                         createSelect("signatories",
                                                                      "sint_Signatory2",
                                                                      "",100,"Name","Select Signatory","",false);
                                                      ?>
                                                   </div>
                                                </div>
                                                <?php
                                                   }
                                                ?>
                                                <!-- <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <div class="form-group">
                                                         <label class="control-label" for="inputs">Remarks:</label>
                                                         <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="remarks"></textarea>
                                                      </div>
                                                   </div>
                                                </div> -->
                                             </div>
                                             <div class="col-xs-6 padd5 adminUse--">
                                                <label>Employees Selected</label>
                                                <select multiple id="EmpSelected" name="EmpSelected" class="form-input" style="height:250px;">
                                                </select>
                                                <p>Double Click the items to remove in the List</p>
                                                <input type="hidden" class="saveFields--" name="hEmpSelected">
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          spacer(10);
                                          echo
                                          '<button type="button" class="btn-cls4-sea"
                                                   name="btnLocSAVE" id="LocSAVE">
                                             <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                             &nbsp;Save
                                          </button>';
                                          btnSACABA([false,true,true]);
                                       ?>
                                    </div>
                                    <div class="panel-bottom"></div>
                                 </div>
                              </div>

                              <div class="col-xs-6 adminUse--">
                                 <div class="mypanel">
                                    <div class="panel-top">Employees List</div>
                                    <div class="panel-mid">
                                       <?php include_once "incEmpListSearchCriteria.e2e.php" ?>
                                       <!-- <a href="javascript:void(0);" title="Search Employees">
                                          <i class="fa fa-search" aria-hidden="true"></i>
                                       </a>-->
                                       <div id="empList" style="max-height:300px;overflow:auto;">
                                          <h4>No Employees Selected</h4>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <a href="javascript:void(0);" id="clearEmpSelected">Clear Employees Selected</a>&nbsp;
                                    </div>
                                 </div>
                              </div>

                           </div>
                        </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varJSON.e2e.php";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>