<?php
   session_start();
   require "conn.e2e.php";
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
?>
<script language="JavaScript" src="<?php echo jsCtrl("ctrl_SysAccount"); ?>"></script>
<?php
   $sysgrouprefid = getvalue("SysGroupRefid");
   if (getvalue("task") != "") {
      $task = getvalue("task");
   }
   if ($task == "list") {
      $criteria  = " WHERE SysGroupRefId = $sysgrouprefid";
      $recordSet = f_Find("sysgroupaccess",$criteria);
      if ($recordSet) {
         $rowcount = mysqli_num_rows($recordSet);
         $YES = "<strong><u>YES</u></strong>";
         $NO = "<span>NO</span>";

         echo
         '<table border=1 style="width:100%;">
            <tr class="panel-top">
               <th class="txt-center  style="width:*%;" padd5">MODULE NAME</th>
               <th class="txt-center" style="width:12%;">ADD</th>
               <th class="txt-center" style="width:12%;">EDIT</th>
               <th class="txt-center" style="width:12%;">DELETE</th>
               <th class="txt-center" style="width:12%;">PRINT</th>
               <th class="txt-center" style="width:12%;">VIEW</th>
               <th class="txt-center" style="width:12%;"></th>
            <tr>';
         while ($SysGroupAccess = mysqli_fetch_assoc($recordSet)) {
            $refid = $SysGroupAccess["RefId"];
            echo
            '<tr class="panel-mid">
               <td class="txt-center padd5">';
               $where = "WHERE RefId = ".$SysGroupAccess['SysModulesRefId'];
                   echo FindFirst("SysModules",$where,"Name");
               echo '</td>
               <td class="txt-center">';
                  if ($SysGroupAccess['CanAdd']) echo $YES; else echo $NO;
               echo '</td>
               <td class="txt-center">';
                  if ($SysGroupAccess['CanEdit']) echo $YES; else echo $NO;
               echo '</td>
               <td class="txt-center">';
                  if ($SysGroupAccess['CanDelete']) echo $YES; else echo $NO;
               echo '</td>
               <td class="txt-center">';
                  if ($SysGroupAccess['CanPrint']) echo $YES; else echo $NO;
               echo '</td>
               <td class="txt-center">';
                  if ($SysGroupAccess['CanView']) echo $YES; else echo $NO;
               echo '</td>
               <td class="txt-center">
                  <a style="text-decoration:none;cursor:pointer;margin-right:10px"
                     title="Edit This Record" onclick="EditGroupAccess('.$refid.');">
                     <img src="'.img("edit.png").'">
                  </a>
                  <a style="text-decoration:none;cursor:pointer"
                     title="Delete This Record" onclick="DelGroupAccess('.$refid.');">
                     <img src="'.img("delete.png").'">
                  </a>
               </td>
            <tr>';
         }
         echo '<table>';
      } else {
         echo "No Record Available";
      }

   }
   else if ($task == "modal") {
      $mode = getvalue("mode");
      $title = $CanAdd0 = $CanEdit0 = $CanDelete0 = $CanPrint0 = $CanView0 = "";
      $CanAdd = $CanEdit = $CanDelete = $CanPrint = $CanView = "";
      if ($mode == "add") {
         $title = 'INSERTING NEW ACCESS UNDER '.getvalue("group");
         $disabled = "";
      } else {
         $title = 'EDITING ACCESS UNDER '.getvalue("group");
         $disabled = "disabled";
         $rs_SysGroupAccess = getRecordSet("sysgroupaccess",getvalue("RefId"));
         $CanAdd = $rs_SysGroupAccess["CanAdd"];
         $CanEdit = $rs_SysGroupAccess["CanEdit"];
         $CanDelete = $rs_SysGroupAccess["CanDelete"];
         $CanPrint = $rs_SysGroupAccess["CanPrint"];
         $CanView = $rs_SysGroupAccess["CanView"];
      }
?>
               <div class="mypanel">
                  <div class="panel-top">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title" id=""><?php echo $title; ?></h4>
                  </div>
                  <div class="panel-mid">
                     <div class="row margin-top">
                        <div class="col-xs-2">MODULES:</div>
                        <div class="col-xs-8">
                           <select name="sint_SysModulesRefId"
                                   class="form-input saveFields-- rptCriteria--">
                              <option value=0>--Select System Modules--</option>
                              <?php
                                 $sql = "SELECT RefId, Name FROM `sysmodules` ORDER BY RefId";
                                 $result = $conn->query($sql);

                                 while($row = $result->fetch_assoc()) {
                                    $whereClause = "WHERE SysGroupRefId = $sysgrouprefid AND SysModulesRefId = ".$row["RefId"];
                                    $rs = FindFirst("SysGroupAccess",$whereClause,"RefId");
                                    if (!($rs)) {
                                       echo '<option value="'.$row["RefId"].'">['.$row["RefId"].'] '.$row["Name"].'</option>'."\n";
                                    }
                                 }
                              ?>
                           </select>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-2">CAN ADD?</div>
                        <input type="hidden" name="hCanAdd" id="hCanAdd" value="0">
                        <div class="col-xs-2">
                           <label for="radCanAddYes">YES&nbsp;&nbsp;</label>
                           <input type="radio" id="radCanAddYes" name="radCanAdd">
                        </div>
                        <div class="col-xs-2">
                           <label for="radCanAddNo">NO&nbsp;&nbsp;</label>
                           <input type="radio" id="radCanAddNo" name="radCanAdd">
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-2">CAN EDIT?</div>
                        <input type="hidden" name="hCanEdit" id="hCanEdit" value="0">
                        <div class="col-xs-2">
                           <label for="radCanEditYes">YES&nbsp;&nbsp;</label>
                           <input type="radio" id="radCanEditYes" name="radCanEdit">
                        </div>
                        <div class="col-xs-2">
                           <label for="radCanEditNo">NO&nbsp;&nbsp;</label>
                           <input type="radio" id="radCanEditNo" name="radCanEdit" checked>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-2">CAN DELETE?</div>
                        <input type="hidden" name="hCanDelete" id="hCanDelete" value="0">
                        <div class="col-xs-2">
                           <label for="radCanDeleteYes">YES&nbsp;&nbsp;</label>
                           <input type="radio" id="radCanDeleteYes" name="radCanDelete">
                        </div>
                        <div class="col-xs-2">
                           <label for="radCanDeleteNo">NO&nbsp;&nbsp;</label>
                           <input type="radio" id="radCanDeleteNo" name="radCanDelete" checked>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-2">CAN PRINT?</div>
                        <input type="hidden" name="hCanPrint" id="hCanPrint" value="0">
                        <div class="col-xs-2">
                           <label for="radCanPrintYes">YES&nbsp;&nbsp;</label>
                           <input type="radio" id="radCanPrintYes" name="radCanPrint">
                        </div>
                        <div class="col-xs-2">
                           <label for="radCanPrintNo">NO&nbsp;&nbsp;</label>
                           <input type="radio" id="radCanPrintNo" name="radCanPrint" checked>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-2">CAN VIEW?</div>
                        <input type="hidden" name="hCanView" id="hCanView" value="1">
                        <div class="col-xs-2">
                           <label for="radCanViewYes">YES&nbsp;&nbsp;</label>
                           <input type="radio" id="radCanViewYes" name="radCanView" checked>
                        </div>
                        <div class="col-xs-2">
                           <label for="radCanViewNo">NO&nbsp;&nbsp;</label>
                           <input type="radio" id="radCanViewNo" name="radCanView">
                        </div>
                     </div>
                  </div>
                  <div class="panel-bottom txt-center">
                     <button type="button" class="btn-cls4-sea trnbtn" id="btnSAVE" name="btnSAVE">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;
                        Save
                     </button>
                     <button type="button" class="btn-cls4-red trnbtn" id="btnCLOSE" name="btnCLOSE" data-dismiss="modal">
                        <i class="fa fa-undo" aria-hidden="true"></i>&nbsp;
                        Close
                     </button>
                  </div>
               </div>

                  <script language="JavaScript">
                     <?php
                        if ($mode == "add") {
                           echo '
                              $("#hCanAdd").val(0);
                              $("#hCanEdit").val(0);
                              $("#hCanDelete").val(0);
                              $("#hCanPrint").val(0);
                              $("#hCanView").val(1);
                              $("#radCanAddNo").prop("checked",true);
                              $("#radCanEditNo").prop("checked",true);
                              $("#radCanDeleteNo").prop("checked",true);
                              $("#radCanPrintNo").prop("checked",true);
                              $("#radCanViewYes").prop("checked",true);
                           ';
                        } else {
                           echo "\nvar CanAdd = ".$rs_SysGroupAccess["CanAdd"].";";
                           echo "\nvar CanEdit = ".$rs_SysGroupAccess["CanEdit"].";";
                           echo "\nvar CanDelete = ".$rs_SysGroupAccess["CanDelete"].";";
                           echo "\nvar CanPrint = ".$rs_SysGroupAccess["CanPrint"].";";
                           echo "\nvar CanView = ".$rs_SysGroupAccess["CanView"].";";
                           echo '
                              $("[name=\'sint_SysModulesRefId\']").val('.$rs_SysGroupAccess["SysModulesRefId"].');
                              $("#hCanAdd").val(CanAdd);
                              $("#hCanEdit").val(CanEdit);
                              $("#hCanDelete").val(CanDelete);
                              $("#hCanPrint").val(CanPrint);
                              $("#hCanView").val(CanView);
                              if (CanAdd) {
                                 $("#radCanAddYes").prop("checked",true);
                              } else {
                                 $("#radCanAddNo").prop("checked",true);
                              }
                              if (CanEdit) {
                                 $("#radCanEditYes").prop("checked",true);
                              } else {
                                 $("#radCanEditNo").prop("checked",true);
                              }
                              if (CanDelete) {
                                 $("#radCanDeleteYes").prop("checked",true);
                              } else {
                                 $("#radCanDeleteNo").prop("checked",true);
                              }
                              if (CanPrint) {
                                 $("#radCanPrintYes").prop("checked",true);
                              } else {
                                 $("#radCanPrintNo").prop("checked",true);
                              }
                              if (CanView) {
                                 $("#radCanViewYes").prop("checked",true);
                              } else {
                                 $("#radCanViewNo").prop("checked",true);
                              }
                           ';
                        }
                     ?>
                  </script>

<?php
   }
   $conn = null;
?>




