<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_AfterTrn"); ?>"></script>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_IPCR_14"); ?>"></script>
      <style type="text/css">
         .td-input {
            padding: 2px;
         }
      </style>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("INDIVIDUAL PERFORMANCE COMMITMENT AND REVIEW [IPCR]"); ?>
            <div class="container-fluid margin-top10">
               <div class="row" id="spmsList">
                  <?php
                     if (getvalue("hUserGroup") != "COMPEMP") {
                  ?>
                  <div class="col-xs-3">
                     <?php
                        employeeSelector();
                     ?>
                  </div>
                  <div class="col-xs-9">
                  <?php
                     } else {
                        echo '<div class="col-xs-12">';   
                     }
                  ?>
                     <div class="panel-top">
                        LIST OF PERFORMANCE MANAGEMENT OF 
                        <span id="selectedEmployees">&nbsp;</span>
                     </div>
                     <div class="panel-mid">
                        <div id="spGridTable">
                        </div>
                     </div>
                     <div class="panel-bottom">
                        <button type="button"
                             class="btn-cls4-sea trnbtn"
                             id="btnINSERTSPMS" name="btnINSERTSPMS">
                           <i class="fa fa-file" aria-hidden="true"></i>
                           &nbsp;Insert New
                        </button>
                     </div>
                  </div>
               </div>
               <div class="row" id="insert_spms">
                  <div class="col-xs-12">
                     <div class="panel-top">ADD NEW</div>
                     <div class="panel-mid" id="contentIPCR">
                        <div class="row">
                           <div class="col-xs-4">
                              <label>Semester</label>
                              <select class="form-input" name="sint_Semester" id="sint_Semester">
                                 <option value="">Select Semester</option>
                                 <option value="1">1st Half</option>
                                 <option value="2">2nd Half</option>
                              </select>
                           </div>
                           <div class="col-xs-4">
                              <label>Year</label>
                              <select class="form-input" name="sint_Year" id="sint_Year">
                                 <?php
                                    $past_year = date("Y",time()) - 2;
                                    $curr_year = date("Y",time());
                                    $future_year = date("Y",time()) + 2;
                                    for ($i=$past_year; $i <= $future_year ; $i++) { 
                                       if ($i == $curr_year) {
                                          echo '<option value="'.$i.'" selected>'.$i.'</option>';   
                                       } else {
                                          echo '<option value="'.$i.'">'.$i.'</option>';
                                       }
                                       
                                    }
                                 ?>
                              </select>
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-4">
                              <label>Supervisor Rating:</label>
                              <input type="text" class="form-input" name="Supervisor_Rating" id="Supervisor_Rating">
                           </div>
                           <div class="col-xs-4">
                              <label>Final Rating:</label>
                              <input type="text" class="form-input" name="Final_Rating" id="Final_Rating">
                           </div>
                        </div>
                        <br>
                        <div id="canvas">
                           <?php
                              for ($x=1; $x <=3; $x++) { 
                           ?>
                           <div id="EntryIPCR_<?php echo $x; ?>" class="entry201">
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <label>IPCR #<?php echo $x; ?> </label>
                                    <input type="hidden" name="refid_<?php echo $x; ?>" id="refid_<?php echo $x; ?>">
                                 </div>
                              </div>
                              <?php bar(); ?>
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <label>OUTPUT</label>
                                    <textarea class="form-input" rows="4" name="output_<?php echo $x; ?>" id="output_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-6">
                                    <label>SUCCESS INDICATORS (TARGETS+MEASURES)</label>
                                    <textarea class="form-input" rows="4" name="si_<?php echo $x; ?>" id="si_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <label>ACTUAL ACCOMPLISHMENTS</label>
                                    <textarea class="form-input" rows="6" name="accomplishment_<?php echo $x; ?>" id="accomplishment_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-3">
                                    <div class="row">
                                       <div class="col-xs-4"></div>
                                       <div class="col-xs-8 text-center">
                                          <label>Employee Rating</label>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-center">
                                          <label>Q<sup>1</sup></label>
                                       </div>
                                       <div class="col-xs-8">
                                          <input type="text" class="form-input" name="q1_<?php echo $x; ?>" id="q1_<?php echo $x; ?>">
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-center">
                                          <label>E<sup>2</sup></label>
                                       </div>
                                       <div class="col-xs-8">
                                          <input type="text" class="form-input" name="e2_<?php echo $x; ?>" id="e2_<?php echo $x; ?>">
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-center">
                                          <label>T<sup>3</sup></label>
                                       </div>
                                       <div class="col-xs-8">
                                          <input type="text" class="form-input" name="t3_<?php echo $x; ?>" id="t3_<?php echo $x; ?>">
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-center">
                                          <label>A<sup>4</sup></label>
                                       </div>
                                       <div class="col-xs-8">
                                          <input type="text" class="form-input" name="a4_<?php echo $x; ?>" id="a4_<?php echo $x; ?>">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-xs-3">
                                    <div class="row">
                                       <div class="col-xs-4"></div>
                                       <div class="col-xs-8 text-center">
                                          <label>Supervisor Rating</label>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-center">
                                          <label>Q<sup>1</sup></label>
                                       </div>
                                       <div class="col-xs-8">
                                          <input type="text" class="form-input" name="<?php echo $x; ?>" id="<?php echo $x; ?>">
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-center">
                                          <label>E<sup>2</sup></label>
                                       </div>
                                       <div class="col-xs-8">
                                          <input type="text" class="form-input" name="<?php echo $x; ?>" id="<?php echo $x; ?>">
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-center">
                                          <label>T<sup>3</sup></label>
                                       </div>
                                       <div class="col-xs-8">
                                          <input type="text" class="form-input" name="<?php echo $x; ?>" id="<?php echo $x; ?>">
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4 text-center">
                                          <label>A<sup>4</sup></label>
                                       </div>
                                       <div class="col-xs-8">
                                          <input type="text" class="form-input" name="<?php echo $x; ?>" id="<?php echo $x; ?>">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <label>REMARKS</label>
                                    <textarea class="form-input" rows="5" name="remarks_<?php echo $x; ?>" id="remarks_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                           </div>
                           <br>
                           <?php
                              }
                           ?>   
                        </div>
                        
                     </div>
                     <div class="panel-bottom">
                        <button type="button" id="addIPCR" class="btn-cls4-tree">
                           <i class="fa fa-plus"></i>&nbsp;ADD ROW
                        </button>
                        <input type="hidden" name="sint_IPCRRefId" id="sint_IPCRRefId" value="0">
                        <input type="hidden" name="sint_EmployeesRefId" id="sint_EmployeesRefId" value="0">
                        <input type="hidden" name="count" id="count">
                        <input type="hidden" name="fn" id="fn" value="saveIPCR">
                        <button type="button" id="btnSAVEIPCR" class="btn-cls4-sea">
                           <i class="fa fa-save"></i>&nbsp;SAVE
                        </button>
                        <button type="button" id="btnCANCELIPCR" class="btn-cls4-red">
                           <i class="fa fa-times"></i>&nbsp;CANCEL
                        </button>
                        <button type="button" id="btnEDITIPCR" class="btn-cls4-sea">
                           <i class="fa fa-save"></i>&nbsp;UPDATE
                        </button>
                        <button type="button" id="btnBACKIPCR" class="btn-cls4-red">
                           <i class="fa fa-backward"></i>&nbsp;BACK
                        </button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "spms_ipcr";
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         $("#addIPCR").click(function() {
            AddRows("EntryIPCR_","contentIPCR","refid_");
         });
      });
      function AddRows(parentId,gparentId,refid) {
         // $.notify(parentId + "1");
         // $.notify(gparentId + "2");
         // $.notify(refid + "3");
         var rowHTML    = "";
         var newAppends = "";
         var appends    = "";
         var rowLength  = $("[id*='"+parentId+"']").length;
         appends        = $("#"+parentId+rowLength).html();
         
         newAppends = appends.split("_"+rowLength).join("_"+(rowLength+1));
         newAppends = newAppends.split("#"+rowLength).join("#"+(rowLength+1));
         newAppends = newAppends.split('idx="' + rowLength + '"').join('idx="'+ (rowLength + 1) +'"');
         newAppends = newAppends.split('unclick="CancelAddRow"').join('onClick=removeRow("'+parentId+(rowLength+1)+'");');
         newAppends = newAppends.split('focus="validateGrade"').join('onChange=validateGrade2($(this).attr(\'partner\')); onFocus=validateGrade2($(this).attr(\'partner\'));');
         newAppends = newAppends.split('click="HighestGrade_validation"').join('onClick=HighestGrade_validation($(this).attr(\'name\'));');
         rowHTML += '<div id="'+parentId+(rowLength+1)+'" class="entry201">';
         rowHTML += newAppends;
         rowHTML += '</div>';
         $("#"+gparentId).append(rowHTML);
         $("[name='"+refid+(rowLength + 1)+"']").val("");
         if (rowHTML.indexOf("date--") > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='date--']").each(function () {
               $(this).keypress(fnDateClass($(this).attr("name")));
            });
         }
         if (rowHTML.indexOf("alpha--") > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='alpha--']").each(function () {
               var e = arguments[0];
               fnAlphaClass($(this).attr("name"),e);
            });
         }
         if (rowHTML.indexOf("number--") > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='number--']").each(function () {
               var e = arguments[0];
               fnNumberClass($(this).attr("name"),e);
            });
         }
         if (rowHTML.indexOf("valDate--") > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='valDate--']").each(function () {
               $(this).keypress(fnValDate($(this).attr("name")));
            });
         }
         if (rowHTML.indexOf('rowid="') > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='enabler--']").each(function () {
               $(this).attr("refid","");
               $("[name='"+$(this).attr("rowid")+"']").val("");
            });
         }
         if (rowHTML.indexOf('presentbox--') > 0) {
            $("#" + parentId+(rowLength+1) + " input[class*='presentbox--']").each(function () {
               $(this).attr("checked",false);
               $(this).click(function () {
                  sint_Present_Click($(this).attr("id"));
               });
            });
         }
         $("#refid_"+(rowLength+1)).val("");
         $("#output_"+(rowLength+1)).val("");
         $("#si_"+(rowLength+1)).val("");
         $("#accomplishment_"+(rowLength+1)).val("");
         $("#remarks_"+(rowLength+1)).val("");
         $("#q1_"+(rowLength+1)).val("");
         $("#e2_"+(rowLength+1)).val("");
         $("#t3_"+(rowLength+1)).val("");
         $("#a4_"+(rowLength+1)).val("");
         // After Add Row
         //$("#"+parentId+(rowLength+1)).append(btnCan);
         $("#" +parentId+(rowLength+1)+ " .saveFields--").prop("disabled",false);
         $("#" +parentId+(rowLength+1)+ "> .enabler--").prop("checked",true);
         showBtnUpd();
      }
   </script>
</html>