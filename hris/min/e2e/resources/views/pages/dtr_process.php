<?php
	include 'conn.e2e.php';
	include 'constant.e2e.php';
	include pathClass.'0620functions.e2e.php';
	include pathClass.'DTRFunction.e2e.php';

	$curr_date		= date("Y-m-d",time());
	$curr_month     = date("m",time());
	$curr_year		= date("Y",time());
	$start_date		= $curr_year."-".$curr_month."-01";
	$rs = SelectEach("employees","");
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$emprefid 		= $row["RefId"];
			$LastName       = $row["LastName"];
			$FirstName      = $row["FirstName"];
			$Name 			= strtoupper($FirstName." ".$LastName);
			$workschedule 	= FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","WorkScheduleRefId");		
			$AsOf 			= FindLast("employeescreditbalance","WHERE EmployeesRefId = $emprefid","BegBalAsOfDate");
			if (!$AsOf) {
				$AsOf = $start_date;
			}
			$Start_AsOf = date('Y-m-d', strtotime($AsOf.' + 1 day'));
			$AsOf_month = date("m",strtotime($Start_AsOf));
			$AsOf_year  = date("Y",strtotime($Start_AsOf));
			if ($workschedule != "") {
				for ($i=1; $i <= intval($curr_month) - 1; $i++) { 
					if ($i < 10) $i = "0".$i;
					$new_date = $AsOf_year."-".$i."-01";
					$dtr_array 		= DTR_Summary ($emprefid,$i,$AsOf_year,$workschedule);
					$flds 			= "";
					$vals 			= "";	
					$table 			= "dtr_process";
					foreach ($dtr_array as $key => $value) {
						$flds .= "`$key`,";
						$vals .= "'$value', ";
						
					}
					//echo $flds." => ".$vals."<br>";
					$save_dtr_process = f_SaveRecord("NEWSAVE",$table,$flds,$vals);
					if (is_numeric($save_dtr_process)) {
						echo "Successfully processed $Name's DTR for the month of ".date("F",strtotime($new_date))."<br>";
					} 
				}
			}
		}
	}
?>