<?php $module = module("AvailmentAuthority"); ?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            $("[name='sint_AbsencesRefId']").addClass("mandatory");
            $("#btnPRINT").click(function () {
               popPDS("OB_2","");
            });
            $("#LocSAVE").click(function () {
               if (saveProceed() == 0) {
                  $(this).attr("type","submit"); 
               }
            });
            remIconDL();
            //#destination
            $("#time_canvas").hide();
            $("[name='sint_AbsencesRefId']").change(function () {
               var value = $("[name='sint_AbsencesRefId'] option:selected").text().toLowerCase();
               if (
                     value == "official business" ||
                     value == "travel order" ||
                     value == "office order" ||
                     value == "foreign travel order"
                  ) {
                  $("#destination").show();
               } else {
                  $("#destination").hide();
               }
            }); 
            $("[name='sint_Whole']").change(function () {
               var value = $(this).val();
               if (value > 0) {
                  $("#time_canvas").hide();
               } else {
                  $("#time_canvas").show();
               }
            });
            <?php
               if (isset($_GET["errmsg"])) {
                  echo '$.notify("'.$_GET["errmsg"].'");';
               }
            ?>
         });
         function afterDelete() {
            alert("Successfully Deleted");
            gotoscrn($("#hProg").val(),"");
         }
         function selectMe(refid) {
            $("#rptContent").attr("src","blank.htm");
            var rptFile = "rpt_OB_2";
            var url = "ReportCaller.e2e.php?file=" + rptFile;
            url += "&refid=" + refid;
            url += "&diff_report=1";
            url += "&" + $("[name='hgParam']").val();
            $("#prnModal").modal();
            $("#rptContent").attr("src",url);
         }
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="postMultiple.e2e.php">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-sm-12" id="div_CONTENT">
                        <div id="divList">
                           <div class="mypanel">
                              <div class="panel-top">List of Office Authority Application</div>
                              <div class="panel-mid">
                                 <span id="spGridTable">
                                    <?php
                                          $sizeCol = "col-sm-6";
                                          if ($UserCode == "COMPEMP") {
                                             $sizeCol = "col-sm-12";
                                             $gridTableHdr_arr = ["Office Authority","File Date", "Application Date From", "Application Date To", "Status"];
                                             $gridTableFld_arr = ["AbsencesRefId","FiledDate", "ApplicationDateFrom", "ApplicationDateTo", "Status"];
                                             $sql = "SELECT * FROM $table WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId ORDER BY FiledDate DESC";
                                             $Action = [false,true,false,true];
                                          }
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      $Action,
                                                      "gridTable");
                                    ?>
                                 </span>
                                 <?php
                                       btnINRECLO([true,true,false]);
                                       /*echo '
                                          <button type="button"
                                               class="btn-cls4-lemon trnbtn"
                                               id="btnPRINT" name="btnPRINT">
                                             <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                                             PRINT
                                          </button>
                                       ';*/
                                 ?>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                        </div>
                        <div id="divView">
                           <div class="row">
                              <div class="col-sm-6">
                                 <div class="mypanel">
                                    <div class="panel-top">
                                       <span id="ScreenMode">AVAILING</span> <?php echo strtoupper($ScreenName); ?>
                                    </div>
                                    <div class="panel-mid">
                                       <div id="EntryScrn">
                                          <div class="row margin-top">
                                             <div class="<?php echo $sizeCol; ?>">
                                                   <div class="row" id="badgeRefId">
                                                      <div class="col-sm-6">
                                                         <ul class="nav nav-pills">
                                                            <li class="active" style="font-size:12pt;font-weight:600;">
                                                               <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                               </span></a>
                                                            </li>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                   <?php
                                                      $attr = ["br"=>true,
                                                               "type"=>"text",
                                                               "row"=>true,
                                                               "name"=>"date_FiledDate",
                                                               "col"=>"6",
                                                               "id"=>"FiledDate",
                                                               "label"=>"Date of Filing",
                                                               "class"=>"saveFields-- mandatory date--",
                                                               "style"=>"",
                                                               "other"=>""];
                                                      $form->eform($attr);
                                                      spacer(10);
                                                      /*$attr = array(
                                                         "row"=>true,
                                                         "col"=>"12",
                                                         "label"=>"Office Authority",
                                                         "table"=>"Absences",
                                                      );
                                                      createFormFK($attr);*/  
                                                      echo '
                                                         <div class="row margin-top">
                                                            <div class="col-xs-12">
                                                      ';
                                                      createSelect("Absences",
                                                                   "sint_AbsencesRefId",
                                                                   "",100,"Name","Select Office Authority","required",false);
                                                      echo '
                                                            </div>
                                                         </div>
                                                      ';
                                                      ?>
                                                      <div class="row margin-top">
                                                         <div class="col-xs-12">
                                                            <div class="row">
                                                               <div class="col-xs-12">
                                                                  <label>Inclusive Date</label>
                                                               </div>
                                                            </div>
                                                            <div class="row">
                                                               <div class="col-xs-6">
                                                                  <label>From</label>
                                                                  <br>
                                                                  <input type="text" class="form-input mandatory saveFields-- date--" name="date_ApplicationDateFrom" id="date_ApplicationDateFrom" required="true">
                                                               </div>
                                                               <div class="col-xs-6">
                                                                  <label>To</label>
                                                                  <br>
                                                                  <input type="text" class="form-input mandatory saveFields-- date--" name="date_ApplicationDateTo" id="date_ApplicationDateTo" required="true">
                                                               </div>
                                                            </div>

                                                         </div>
                                                      </div>
                                                      <div class="row margin-top">
                                                         <div class="col-xs-6">
                                                            <label>Is Whole Day?</label>
                                                            <select class="form-input saveFields--" name="sint_Whole" id="sint_Whole">
                                                               <option value="">Select --</option>
                                                               <option value="0">NO</option>
                                                               <option value="1">YES</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                      <?php
                                                      echo
                                                      '
                                                      <div id="time_canvas">
                                                         <label>Time</label><br>
                                                         <div class="row margin-top">
                                                            <div class="col-sm-12">
                                                               <div class="col-sm-6">
                                                               <label>From Time</label>
                                                               ';
                                                                  //timePicker("FromTime","From Time");
                                                               drpTime("FromTime",8,1,0);
                                                               echo
                                                               '</div>
                                                               <div class="col-sm-6">
                                                               <label>To Time</label>
                                                               ';
                                                                  //timePicker("ToTime","To Time");
                                                               drpTime("ToTime",8,1,0);
                                                               echo
                                                               '</div>';

                                                         echo
                                                         '
                                                            </div>
                                                         </div>
                                                      </div>
                                                      ';

                                                   ?>
                                                   
                                                   <div class="row margin-top" id="destination">
                                                      <div class="col-sm-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="inputs">Destination:</label>
                                                            <textarea class="form-input saveFields--" rows="2" name="char_Destination" placeholder="Destination"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="row">
                                                      <div class="col-sm-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="inputs">Purpose:</label>
                                                            <textarea class="form-input saveFields--" rows="5" name="char_Remarks" placeholder="Purpose"></textarea>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <?php
                                                      if ($UserCode == "COMPEMP") {
                                                   ?>
                                                   <div class="row">
                                                      <div class="col-xs-12">
                                                         <label class="control-label" for="inputs">Recommended By:</label>
                                                         <?php
                                                            createSelect("signatories",
                                                                         "sint_Signatory1",
                                                                         "",100,"Name","Select Signatory","",false);
                                                         ?>
                                                      </div>
                                                   </div>
                                                   <div class="row">
                                                      <div class="col-xs-12">
                                                         <label class="control-label" for="inputs">Approved By:</label>
                                                         <?php
                                                            createSelect("signatories",
                                                                         "sint_Signatory2",
                                                                         "",100,"Name","Select Signatory","",false);
                                                         ?>
                                                      </div>
                                                   </div>
                                                   <?php
                                                      }
                                                   ?>
                                             </div>
                                             <div class="col-sm-6 padd5 adminUse--">
                                                <label>Employees Selected</label>
                                                <select multiple id="EmpSelected" name="EmpSelected" class="form-input" style="height:250px;">
                                                </select>
                                                <p>Double Click the items to remove in the List</p>
                                                <input type="hidden" class="saveFields--" name="hEmpSelected">
                                             </div>
                                          </div>
                                       </div>
                                       <?php
                                          spacer(10);
                                          echo
                                          '<button type="button" class="btn-cls4-sea"
                                                   name="btnLocSAVE" id="LocSAVE">
                                             <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                             &nbsp;Save
                                          </button>';
                                          btnSACABA([false,true,true]);
                                       ?>
                                    </div>
                                    <div class="panel-bottom"></div>
                                 </div>
                              </div>

                              <div class="col-sm-6 adminUse--">
                                 <div class="mypanel">
                                    <div class="panel-top">Employees List</div>
                                    <div class="panel-mid">
                                       <?php include_once "incEmpListSearchCriteria.e2e.php" ?>
                                       <!-- <a href="javascript:void(0);" title="Search Employees">
                                          <i class="fa fa-search" aria-hidden="true"></i>
                                       </a>-->
                                       <div id="empList" style="max-height:300px;overflow:auto;">
                                          <h4>No Employees Selected</h4>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <a href="javascript:void(0);" id="clearEmpSelected">Clear Employees Selected</a>&nbsp;
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varJSON.e2e.php";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>