<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>

   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("Learning And Development Intervention"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row">
                     <div class="col-sm-3">
                        <?php
                           employeeSelector();
                        ?>
                     </div>
                     <div class="col-xs-9" id="div_CONTENT">
                        <div class="margin-top">
                           <div id="divList">
                              <div class="mypanel panel panel-default">
                                 <div class="panel-top">
                                    <span id="ScreenMode">INSERTING NEW COMPETENCY ASSESSMENT
                                 </div>
                                 <div class="panel-mid">
                                    <span id="spGridTable">
                                       <?php
                                          $table = "ldmslndintervention";
                                          $gridTableHdr_arr = ["Employee Name","Title","Start Date","End Date"];
                                          $gridTableFld_arr = ["EmployeesRefId","Name","StartDate","EndDate"];
                                          $sql = "SELECT * FROM ldmslndintervention";
                                          $Action = [true,true,true,false];
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      $Action,
                                                      $_SESSION["module_gridTable_ID"]);
                                       ?>
                                    </span>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php
                                       btnINRECLO([true,false,false]);
                                    ?>
                                 </div>
                              </div>
                           </div>
                           <div id="divView">
                              <div class="mypanel panel panel-default">
                                 <div class="panel-top">
                                    <span id="ScreenMode">INSERTING NEW LEARNING AND DEVELOPMENT INTERVENTION
                                 </div>
                                 <div class="panel-mid-litebg" id="EntryScrn">
                                    <div class="container" id="EntryScrn">
                                       <div class="row">
                                          <div class="col-xs-4">
                                             <label class="control-label" for="inputs">Current Position:</label><br>
                                             <?php
                                                createSelect("Position",
                                                             "sint_PositionRefId",
                                                             "",100,"Name","Select Position","");
                                             ?>
                                          </div>
                                          <div class="col-xs-4">
                                             <label class="control-label" for="inputs">Current Department:</label><br>
                                             <?php
                                                createSelect("Department",
                                                             "sint_DepartmentRefId",
                                                             "",100,"Name","Select Department","");
                                             ?>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-xs-4">
                                             <input type="hidden" name="sint_EmployeesRefId" id="sint_EmployeesRefId" class="saveFields-- mandatory">
                                             <label class="control-label" for="inputs">Employee Name:</label><br>
                                             <input class="form-input" 
                                                    type="text"
                                                    name="employeename"
                                                    id="employeename">
                                          </div>
                                          <div class="col-xs-4">
                                             <label class="control-label" for="inputs">Year In Position:</label><br>
                                             <input class="form-input date--" 
                                                    type="text"
                                                    name="employeename"
                                                    id="employeename">
                                          </div>
                                       </div>
                                       <br><br><br>
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">
                                                      Title of Training/Seminar/Scholarship:
                                                   </label><br>
                                                   <input class="form-input saveFields-- mandatory uCase--" 
                                                          type="text"
                                                          name="char_Name"
                                                          id="char_Name">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">Start Date:</label><br>
                                                   <input class="form-input saveFields-- mandatory date--" 
                                                          type="text"
                                                          name="date_StartDate"
                                                          id="date_StartDate">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">End Date:</label><br>
                                                   <input class="form-input saveFields-- mandatory date--" 
                                                          type="text"
                                                          name="date_EndDate"
                                                          id="date_EndDate">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">
                                                      Provider:
                                                   </label><br>
                                                   <input class="form-input saveFields-- mandatory uCase--" 
                                                          type="text"
                                                          name="char_Provider"
                                                          id="char_Provider">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">
                                                      Cost:
                                                   </label><br>
                                                   <input class="form-input saveFields-- mandatory uCase--" 
                                                          type="text"
                                                          name="char_Cost"
                                                          id="char_Cost">
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="form-group">
                                                   <label class="control-label" for="inputs">REMARKS:</label>
                                                   <textarea class="form-input saveFields--" 
                                                             rows="5" 
                                                             name="char_Remarks"
                                                             placeholder="remarks"></textarea>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php btnSACABA([true,true,true]); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "ldmslndintervention";
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script language="JavaScript">
      $(document).ready(function () {
         remIconDL();
      });
      function afterNewSave() {
         alert("Successfully Saved");
         gotoscrn("ldms_learning_and_development_intervention","");
      }
      function selectMe(emprefid) {
         $("[name='sint_EmployeesRefId']").val(emprefid);
         $.get("ldmsEmpDetail.e2e.php",
         {
            EmpRefId:emprefid,
            hCompanyID:$("#hCompanyID").val(),
            hBranchID:$("#hBranchID").val(),
            hEmpRefId:$("#hEmpRefId").val(),
            hUserRefId:$("#hUserRefId").val()
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);
               } catch (e) {
                   if (e instanceof SyntaxError) {
                       alert(e.message);
                   }
               }
            }
         });
      }
   </script>
</html>



