<?php
   require_once "constant.e2e.php";
   $ScrnMode = $code = $name = $remarks = $disabled = $msg = $posrefid = $label = $divrefid ="";
   $mainTable = "";
   if (isset($_GET["hScrnMode"]))
      $ScrnMode = $_GET["hScrnMode"];
   else if (isset($_POST["hScrnMode"]))
      $ScrnMode = $_POST["hScrnMode"];

   if ($ScrnMode == 1) {
      $refid = "";
   } else if ($ScrnMode == 3 || $ScrnMode == 2) {
      session_start();
      require_once $_SESSION['Classes'].'0620functions.e2e.php';
      require_once "conn.e2e.php";
      $msg = getvalue("msg");
      $mainTable = getvalue("hTable");
      if ($ScrnMode == 3) $disabled = "disabled";
      $refid = getvalue("hRefId");
      if ($refid) {
         $criteria  = " WHERE RefId = $refid";
         $criteria .= " LIMIT 1";
         $recordSet = f_Find($mainTable,$criteria);
         $rowcount = mysqli_num_rows($recordSet);
         $row = array();
         $row = mysqli_fetch_assoc($recordSet);
         if ($rowcount) {
            $name = $row["Name"];
            $label = $row["Label"];
            $posrefid = $row["PositionRefId"];
            $remarks = $row["Remarks"];
         }
      }
   }
   require_once "incUtilitiesJS.e2e.php";
?>
   <div class="container" id="EntryScrn">
      <div class="row">
         <div class="col-xs-6">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } ?>
            <!-- <div class="row margin-top">
               <div class="form-group">
                  <label class="control-label" for="inputs">Label</label><br>
                  <input type="text" class="form-input saveFields--" name="char_Label" <?php echo $disabled; ?> value="<?php echo $label; ?>" autofocus>
               </div>
            </div> -->
            <div class="row margin-top">
               <div class="col-xs-12">
                  <label class="control-label" for="inputs">Employee Name</label><br>
                  <input type="text" class="form-input saveFields-- mandatory" id="char_Name" name="char_Name" <?php echo $name; ?>>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <label class="control-label" for="inputs">POSITION:</label><br>
                  <?php
                     createSelect("Position",
                                  "sint_PositionRefId",
                                  $posrefid,100,"Name","Select Position",$disabled);
                  ?>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-12">
                  <label class="control-label" for="inputs">DIVISION:</label><br>
                  <?php
                     createSelect("Division",
                                  "sint_DivisionRefId",
                                  $divrefid,100,"Name","Select Division",$disabled);
                  ?>
               </div>
            </div>
            <!-- <div class="row margin-top">
               <div class="col-xs-6">
                  <label class="control-label">SIGNATORY FOR</label>
                  <select class="form-input saveFields--" name="char_ForLabel" id="char_ForLabel">
                     <option value="leave">Leave Application</option>
                     <option value="ob">Office Authority</option>
                  </select>
               </div>
               <div class="col-xs-6">
                  <label class="control-label">SIGNATORY LABEL</label>
                  <select class="form-input saveFields--" name="char_SignatoryLabel" id="char_SignatoryLabel"></select>
               </div>
            </div> -->
            <div class="row margin-top">
               <div class="col-xs-12">
                  <label class="control-label" for="inputs">REMARKS:</label>
                  <textarea class="form-input saveFields--" rows="5" name="char_Remarks" <?php echo $disabled; ?>
                  placeholder="remarks"><?php echo $remarks; ?></textarea>
               </div>
            </div>
         </div>
      </div>
   </div>
   <link rel="stylesheet" href="<?php echo path("js/autocomplete/css/jquery-ui.css"); ?>" type="text/css" />
   <script type="text/javascript" src="<?php echo path("js/autocomplete/jquery-ui.js") ?>"></script>
   <script type="text/javascript">
      $(document).ready(function () {
         remIconDL();
         EmployeeAutoComplete("employees","char_Name");
         $("#char_Name").blur(function () {
            var value = $(this).val();
            arr = value.split("-");
            getPosition(arr[0]);
            getDivision(arr[0]);
            $(this).val(arr[1]);
         });
         $("#char_ForLabel").change(function () {
            var value = $(this).val();
            $("#char_SignatoryLabel").empty();
            getSignatoryLabel(value);
         });
      });
   </script>