<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_SummRating"); ?>"></script>
      <script type="text/javascript">
         $(document).ready(function () {



         });
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($modTitle); ?>
            <div class="container-fluid margin-top10">
               <div class="row">
                  <div class="col-xs-3">
                     <?php
                        employeeSelector();
                     ?>
                  </div>
                  <div class="col-xs-9">
                     <div id="divList">
                        <div class="panel-top">
                           LIST OF PERFORMANCE MANAGEMENT
                        </div>
                        <div class="panel-mid-litebg">
                           <div class="row">
                              <div class="col-xs-12">
                                 <div style="overflow:auto;max-height:500px;">
                                    <div id="spGridTable">
                                       <?php
                                          $table = "employeesperformance";
                                          $gridTableHdr = "Employee Name|Semester|Year|Average|Adjectival";
                                          $gridTableFld = "EmployeesRefId|Semester|YearPerformed|OverallScore|Adjectival";
                                          $gridTableHdr_arr = explode("|",$gridTableHdr);
                                          $gridTableFld_arr = explode("|",$gridTableFld);
                                          $sql = "SELECT * FROM `$table` ORDER BY RefId Desc LIMIT 10";
                                          $_SESSION["module_table"] = $table;
                                          $_SESSION["module_gridTableHdr_arr"] = $gridTableHdr_arr;
                                          $_SESSION["module_gridTableFld_arr"] = $gridTableFld_arr;
                                          $_SESSION["module_sql"] = $sql;
                                          $_SESSION["module_gridTable_ID"] = "gridTable";
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      [true,true,true,false],
                                                      $_SESSION["module_gridTable_ID"]);
                                       ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                           <?php
                              btnINRECLO([true,false,false]);
                           ?>
                        </div>
                     </div>
                        <div id="divView">
                           <div class="mypanel panel panel-default">
                              <div class="panel-top">
                                 <span id="ScreenMode">INSERTING NEW COMPETENCY ASSESSMENT
                              </div>
                              <div class="panel-mid-litebg" id="EntryScrn">
                                 <div class="container" id="EntryScrn">
                                    <div class="row">
                                       <div class="col-xs-4">
                                          <input type="hidden" name="sint_EmployeesRefId" id="sint_EmployeesRefId" class="saveFields-- mandatory">
                                          <label class="control-label" for="inputs">Employee Name:</label><br>
                                          <input class="form-input" 
                                                 type="text"
                                                 name="employeename"
                                                 id="employeename">
                                       </div>
                                       <div class="col-xs-4">
                                          <label class="control-label" for="inputs">Position:</label><br>
                                          <?php
                                             createSelect("Position",
                                                          "sint_PositionRefId",
                                                          "",100,"Name","Select Position","");
                                          ?>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-xs-4">
                                          <label class="control-label" for="inputs">Department:</label><br>
                                          <?php
                                             createSelect("Department",
                                                          "sint_DepartmentRefId",
                                                          "",100,"Name","Select Department","");
                                          ?>
                                       </div>
                                       <div class="col-xs-4">
                                          <label class="control-label" for="inputs">Division:</label><br>
                                          <?php
                                             createSelect("Division",
                                                          "sint_DivisionRefId",
                                                          "",100,"Name","Select Division","");
                                          ?>
                                       </div>
                                    </div>
                                    <br><br><br>
                                    <div class="row">
                                       <div class="col-xs-6">
                                          <div class="row margin-top">
                                             <div class="col-xs-3">
                                                <label>Semester:</label>
                                             </div>
                                             <div class="col-xs-3">
                                                <select class="form-input saveFields--" name="char_Semester">
                                                   <option value="January - June">January - June</option>
                                                   <option value="July - December">July - December</option>
                                                </select>
                                             </div>
                                             <div class="col-xs-3">
                                                <label>Year:</label>
                                             </div>
                                             <div class="col-xs-3">
                                                <select class="form-input saveFields-- mandatory" 
                                                       name="date_YearPerformed"
                                                       id="date_YearPerformed">
                                                       <?php
                                                         for ($i=(date("Y",time()) - 5); $i <= (date("Y",time()) + 5); $i++) { 
                                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                                         }
                                                       ?>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3">
                                                <label>Average:</label>
                                             </div>
                                             <div class="col-xs-3">
                                                <input type="text" class="form-input saveFields-- number--" name="bint_OverallScore" placeholder="LIMIT IS 5">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3">
                                                <label>Rating:</label>
                                             </div>
                                             <div class="col-xs-3">
                                                <input type="text" class="form-input saveFields-- number--" name="bint_NumericalRating" readonly>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3">
                                                <label>Adjectival:</label>
                                             </div>
                                             <div class="col-xs-3">
                                                <select class="form-input saveFields-- mandatory" name="char_Adjectival" readonly>
                                                   <option value="5">Outstanding</option>
                                                   <option value="4">Very Satisfactory</option>
                                                   <option value="3">Satisfactory</option>
                                                   <option value="2">Unsatisfactory</option>
                                                   <option value="1">Poor</option>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-bottom">
                                 <?php btnSACABA([true,true,true]); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("EmpRefIdRequire","YES","");
               $table = "employeesperformance";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      function selectMe(emprefid) {
         $("[name='sint_EmployeesRefId']").val(emprefid);
         $.get("ldmsEmpDetail.e2e.php",
         {
            EmpRefId:emprefid,
            hCompanyID:$("#hCompanyID").val(),
            hBranchID:$("#hBranchID").val(),
            hEmpRefId:$("#hEmpRefId").val(),
            hUserRefId:$("#hUserRefId").val()
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);
               } catch (e) {
                   if (e instanceof SyntaxError) {
                       alert(e.message);
                   }
               }
            }
         });
      }
   </script>
</html>