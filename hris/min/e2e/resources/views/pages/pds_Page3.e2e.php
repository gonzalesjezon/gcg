<?php
/*
------------------------------------------------------------------------
    File        : pds_Page3.php
    Purpose     :
    Syntax      : PHP / HTML
    Description :
    Author(s)   : Erwin Mendoza / emendoza0620@gmail.com
    Created     :
    Notes       :
------------------------------------------------------------------------
Developer   Date     Changes
--------------------------------------------------------------------------

--------------------------------------------------------------------------*/
   function split_volwork() {
      pdsFooter(true,10);
      echo
      '</tbody>';
      echo    
      '</table>';
      footer_signdate(false);
      echo
      '</div>
      <div class="page-- nextpage" id="page3" style="position:relative;">
      <table border=1 cellspacing=0 cellpadding=0>';
      volworkTHEAD();
      echo 
      '<tbody>';
   }

   function split_learning() {
      pdsFooter(true,10);
      echo
      '</tbody>';
      echo    
      '</table>';
      footer_signdate(false);
      echo
      '</div>
      <div class="page-- nextpage" id="page3" style="position:relative;">
      <table border=1 cellspacing=0 cellpadding=0>';
      learningTHEAD();
      echo 
      '<tbody>';
   }

   function split_otherinfo() {
      pdsFooter(true,10);
      echo
      '</tbody>';
      echo    
      '</table>';
      footer_signdate(false);
      echo
      '</div>
      <div class="page-- nextpage" id="page3" style="position:relative;">
      <table border=1 cellspacing=0 cellpadding=0>';
      ontherinfoTHEAD();
      echo 
      '<tbody>';

   }

   $EmpVoluntary_split = false;
   $EmpTraining_split = false;
   $EmpOtherInfo_split = false;
?>
   <div class="page-- nextpage" id="page3" style="position:relative;">
      <table border=1 cellspacing=0 cellpadding=1>
         <?php volworkTHEAD(); ?>   
         <tbody>
            <?php
               $lineCount = 0;
               $rowCount_EmpVoluntary = 0;
               $tailCount[1] = 0;
               if ($rsEmpVoluntary) {
                  $rowCount_EmpVoluntary = mysqli_num_rows($rsEmpVoluntary); 
                     if ($split["VolWork"] != ".") {
                        $pds["rowVoluntary"] = explode(".",$split["VolWork"])[1];
                        $EmpVoluntary_split = true;
                        if ($rowCount_EmpVoluntary - $pds["rowVoluntary"] <= 0) {
                           $EmpTraining_split = false;
                        }
                     } else {
                        if ($rowCount_EmpVoluntary > $pds["rowVoluntary"]) {
                           $EmpVoluntary_split = true;
                        }
                     }
               } else {
                  if ($split["VolWork"] != ".") {
                     $pds["rowVoluntary"] = explode(".",$split["VolWork"])[1];
                  }
               }

               for ($j=1;$j<=$pds["rowVoluntary"];$j++) {
            ?>
                  <tr class="trHeight">
                     <td align="center" colspan="4">
                        <span class="answer VOLUNTARY" id="page3_A<?php echo ($j + 4); ?>"></span>
                     </td>
                     <td align="center">
                        <span class="answer VOLUNTARY" id="page3_E<?php echo ($j + 4); ?>"></span>
                     </td>
                     <td align="center">
                        <span class="answer VOLUNTARY" id="page3_F<?php echo ($j + 4); ?>"></span>
                     </td>
                     <td align="center">
                        <span class="answer VOLUNTARY" id="page3_G<?php echo ($j + 4); ?>"></span>
                     </td align="center">
                     <td align="center" colspan="3">
                        <span class="answer VOLUNTARY" id="page3_H<?php echo ($j + 4); ?>"></span>
                     </td>
                  </tr>
            <?php
               }
               rowContinuation(10);
            ?>
         </tbody>
         <!--<tfoot>
            <tr style="border-top:2px solid #000;border-bottom:2px solid #000;">
               <td colspan="10" class="bgGrayLabel" align="center" style="color:#FF0000;">
                  <i>(Continue on separate sheet if necessary)</i>
               </td>
            </tr>   
         </tfoot> -->
      </table> 
      <table border=1 cellspacing=0 cellpadding=1>
         <?php learningTHEAD(); ?>   
         <tbody>
            <?php
               $rowCount_EmpTraining = 0;
               if ($rsEmpTraining) {
                  $rowCount_EmpTraining = mysqli_num_rows($rsEmpTraining);
                  if ($split["Learning"] != ".") {
                     $EmpTraining_split = true;
                     $pds["rowLearning"] = explode(".",$split["Learning"])[1];
                     if ($rowCount_EmpTraining - $pds["rowLearning"] <= 0) {
                        $EmpTraining_split = false;
                     }
                  } else {
                     if ($rowCount_EmpTraining > $pds["rowLearning"]) {
                        $EmpTraining_split = true;
                     }
                  }
               } else {
                  if ($split["Learning"] != ".") $pds["rowLearning"] = explode(".",$split["Learning"])[1];
               }

               for ($j=1;$j<=$pds["rowLearning"];$j++) {
            ?>
                  <tr class="trHeight">
                     <td colspan="4">
                        <span class="answer TRAININGPROG" id="page3_L<?php echo ($j + 6); ?>"></span>
                     </td>
                     <td align="center">
                        <span class="answer TRAININGPROG" id="page3_Q<?php echo ($j + 6); ?>"></span>
                     </td>
                     <td align="center">
                        <span class="answer TRAININGPROG" id="page3_R<?php echo ($j + 6); ?>"></span>
                     </td>
                     <td align="center">
                        <span class="answer TRAININGPROG" id="page3_S<?php echo ($j + 6); ?>"></span>
                     </td>
                     <td align="center">
                        <span class="answer TRAININGPROG" id="page3_T<?php echo ($j + 6); ?>"></span>
                     </td>
                     <td colspan="2">
                        <span class="answer TRAININGPROG" id="page3_U<?php echo ($j + 6); ?>"></span>
                     </td>
                  </tr>
            <?php
               }
               rowContinuation(10);
            ?>
         </tbody>
         <!--<tfoot>
            <tr style="border-top:2px solid #000;border-bottom:2px solid #000;">
               <td colspan="10" class="bgGrayLabel" align="center" style="color:#FF0000;">
                  <i>(Continue on separate sheet if necessary)</i>
               </td>
            </tr>   
         </tfoot>   
         -->
      </table>
      <table border=1 cellspacing=0 cellpadding=0>
         <?php otherinfoTHEAD(); ?>
         <tbody>
            <?php
               if ($rsEmpOtherInfo) {
                  $rowCount_EmpOtherInfo = mysqli_num_rows($rsEmpOtherInfo);
                  if ($split["OtherInfo"] != ".") {
                     $pds["rowOtherInfo"] = explode(".",$split["OtherInfo"])[1];
                     $EmpOtherInfo_split = true;
                     if ($rowCount_EmpOtherInfo - $pds["rowOtherInfo"] <= 0) {
                        $EmpOtherInfo_split = false;
                     }
                  } else {
                     if ($rowCount_EmpOtherInfo > $pds["rowOtherInfo"]) {
                        $EmpOtherInfo_split = true;
                     }
                  }
               } else {
                  if ($split["OtherInfo"] != ".") {
                     $pds["rowOtherInfo"] = explode(".",$split["OtherInfo"])[1];
                  }
               }

               for ($j=1;$j<=$pds["rowOtherInfo"];$j++) {
            ?>
                  <tr class="trHeight">
                     <td colspan="4" align="center">
                        <span class="answer OTHERINFO" id="page3_X<?php echo ($j + 6); ?>"></span>
                     </td>
                     <td colspan="4" align="center">
                        <span class="answer OTHERINFO" id="page3_Y<?php echo ($j + 6); ?>"></span>
                     </td>
                     <td colspan="4" align="center">
                        <span class="answer OTHERINFO" id="page3_Z<?php echo ($j + 6); ?>"></span>
                     </td>
                  </tr>
            <?php
               }
               rowContinuation(12);
            ?>
         </tbody>   
      </table>
      <?php footer_signdate(false) ?>
   </div>      

<?php 
   if ($EmpVoluntary_split || $EmpTraining_split || $EmpOtherInfo_split) {
?>    
      <div class="page-- nextpage" id="page3_1" style="position:relative;">
      <?php 
         $rows_Count = 0;
         if ($EmpVoluntary_split) { 
            echo '<table border=1 cellspacing=0 cellpadding=1>';
            volworkTHEAD();
            echo '<tbody>';
            for ($j=1;$j<=$rowCount_EmpVoluntary - $pds["rowVoluntary"];$j++) {
               $rows_Count++;
               if ($split["VolWork"] != ".") {
                  if (count(explode(".",$split["VolWork"])) == 3) {
                     $splitRowPerPage = explode(".",$split["VolWork"])[2];
                  } 
                  if ($rows_Count > $splitRowPerPage) {
                     $rows_Count = 1; 
                     split_volwork();
                  }  
               }
               else {
                  if ($rows_Count > $pds["rowPerPage"]) {
                     $rows_Count = 1; 
                     split_volwork();
                  }  
               }
      ?>
               <tr class="trHeight">
                  <td align="center" colspan="4">
                     <span class="answer VOLUNTARY" id="page3_1_A<?php echo ($j + 4); ?>"></span>
                  </td>
                  <td align="center">
                     <span class="answer VOLUNTARY" id="page3_1_E<?php echo ($j + 4); ?>"></span>
                  </td>
                  <td align="center">
                     <span class="answer VOLUNTARY" id="page3_1_F<?php echo ($j + 4); ?>"></span>
                  </td>
                  <td align="center">
                     <span class="answer VOLUNTARY" id="page3_1_G<?php echo ($j + 4); ?>"></span>
                  </td align="center">
                  <td align="center" colspan="3">
                     <span class="answer VOLUNTARY" id="page3_1_H<?php echo ($j + 4); ?>"></span>
                  </td>
               </tr>
      <?php 
               
            } 
            echo '</tbody></table>';
         }
         if ($EmpTraining_split) { 
            //$tabPageCount = 2;
            echo '<table border=1 cellspacing=0 cellpadding=1>';
            learningTHEAD();
            echo '<tbody>';
            for ($j=1;$j<=$rowCount_EmpTraining - $pds["rowLearning"];$j++) {
               $rows_Count++;
               if ($split["Learning"] != ".") {
                  if (count(explode(".",$split["Learning"])) == 3) {
                     $splitRowPerPage = explode(".",$split["Learning"])[2];
                  } 
                  if ($rows_Count > $splitRowPerPage) {
                     $rows_Count = 1; 
                     $tabPageCount++;
                     split_learning();
                  }  
                  
               } else {
                  if ($rows_Count > $pds["rowPerPage"]) {
                     $rows_Count = 1; 
                     split_learning();
                  }  
               }

               
      ?>    
               <tr class="trHeight">
                  <td colspan="4">
                     <span class="answer TRAININGPROG" id="page3_1_L<?php echo ($j + 6); ?>"></span>
                  </td>
                  <td align="center">
                     <span class="answer TRAININGPROG" id="page3_1_Q<?php echo ($j + 6); ?>"></span>
                  </td>
                  <td align="center">
                     <span class="answer TRAININGPROG" id="page3_1_R<?php echo ($j + 6); ?>"></span>
                  </td>
                  <td align="center">
                     <span class="answer TRAININGPROG" id="page3_1_S<?php echo ($j + 6); ?>"></span>
                  </td>
                  <td align="center">
                     <span class="answer TRAININGPROG" id="page3_1_T<?php echo ($j + 6); ?>"></span>
                  </td>
                  <td colspan="2">
                     <span class="answer TRAININGPROG" id="page3_1_U<?php echo ($j + 6); ?>"></span>
                  </td>
               </tr>     
      <?php
               
            }
            echo '</tbody></table>';
         }
         if ($EmpOtherInfo_split) { 
            echo 
            '<table border=1 cellspacing=0 cellpadding=0>';
            otherinfoTHEAD();
            echo '<tbody>';
            for ($j=1;$j<=$rowCount_EmpOtherInfo - $pds["rowOtherInfo"];$j++) {
               $rows_Count++;
               if ($split["OtherInfo"] != ".") {
                  if (count(explode(".",$split["OtherInfo"])) == 3) {
                     $splitRowPerPage = explode(".",$split["OtherInfo"])[2];
                  } 
                  if ($rows_Count > $splitRowPerPage) {
                     $rows_Count = 1; 
                     split_otherinfo();
                  }  
               } else {
                  if ($rows_Count > $pds["rowPerPage"]) {
                     $rows_Count = 1;
                     split_otherinfo();
                  }
               }
      ?>
               <tr class="trHeight">
                  <td colspan="4" align="center">
                     <span class="answer OTHERINFO" id="page3_1_X<?php echo ($j + 6); ?>"></span>
                  </td>
                  <td colspan="4" align="center">
                     <span class="answer OTHERINFO" id="page3_1_Y<?php echo ($j + 6); ?>"></span>
                  </td>
                  <td colspan="4" align="center">
                     <span class="answer OTHERINFO" id="page3_1_Z<?php echo ($j + 6); ?>"></span>
                  </td>
               </tr>
      <?php         
            }
            echo '</tbody></table>';
         }
         footer_signdate(false);
      ?>
      </div>
<?php   
   }
?>


      
        


   
