<?php
   //session_start();
   include 'colors.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   include 'incRptSortBy.e2e.php';
   $rs = SelectEach($table,$whereClause);
   if ($rs) $rowcount = mysqli_num_rows($rs);
   if ($dbg) { echo "DBG >> ".$whereClause; }

   /*Start Here Date Validation --*/
   $errmsg = "";
   /*End Here - Date Validation*/
   $recordsCount = 0;

   function dispNumFormat($val) {
      if ($val == "") {
         echo "0.00";
      } else {
         echo number_format($val,2);
      }
   }

   function dispBlank($val) {
      if ($val == "") {
         echo "---";
      } else {
         echo $val;
      }
   }

?>

<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody page--">
         <?php
            //rptHeader("Notice of Salary Adjustment");

            $empRefid = "";
            while ($row = mysqli_fetch_assoc($rs))
            {
               $childCount = 0;
               $empRefid = $row['RefId'];

               $sql = "SELECT * FROM `employeesworkexperience` WHERE CompanyRefId = $CompanyId ";
               $sql .= "AND BranchRefId = $BranchId ";
               $sql .= "AND EmployeesRefId = $empRefid AND isServiceRecord = 1 ORDER BY WorkStartDate DESC";

               $rsWORKEXP = mysqli_query($conn,$sql) or die(mysqli_error($conn));
               $numrow = mysqli_num_rows($rsWORKEXP);
               if ($numrow) {
                  $EmployeesName = $row["FirstName"]." ".$row["MiddleName"].". ".$row["LastName"];
                  rptHeader("SERVICE RECORD");
                  echo "<div class='txt-center'><span class='rptFooter'>(To be accomplished by Employer)</span></div>";


         ?>
                  <div class="container-fluid">
                     <div>
                        <p>
                           <div class="row">
                              <div class="col-xs-2 txt-right">NAME:</div>
                              <div class="col-xs-2 txt-center">
                                 <div style="border-bottom:2px solid #000"><b><?php echo $row["LastName"]; ?></b></div>
                                 <span class='rptFooter'>(Surname)</span>
                              </div>
                              <div class="col-xs-2 txt-center">
                                 <div style="border-bottom:2px solid #000"><b><?php echo $row["FirstName"]; ?></b></div>
                                 <span class='rptFooter'>(Given Name)</span>
                              </div>
                              <div class="col-xs-2 txt-center">
                                 <div style="border-bottom:2px solid #000"><b><?php echo $row["MiddleName"]; ?></b></div>
                                 <span class='rptFooter'>(Middle Name)</span>
                              </div>
                              <div class="col-xs-4">
                                 <span class='rptFooter'>
                                 (If married woman,give also full maiden name)
                                 </span>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-2 txt-right">BIRTH:</div>
                              <div class="col-xs-3 txt-center">
                                 <div style="border-bottom:2px solid #000"><b><?php echo $row["BirthDate"]; ?></b></div>
                                 <span class='rptFooter'>(Date)</span>
                              </div>
                              <div class="col-xs-3 txt-center">
                                 <div style="border-bottom:2px solid #000"><b><?php echo $row["BirthPlace"]; ?></b></div>
                                 <span class='rptFooter'>(Place)</span>
                              </div>
                              <div class="col-xs-4">
                                 <span class='rptFooter'>
                                    Date herein should be checked from birth or baptismal certificate or some other reliable documents.
                                 </span>
                              </div>
                           </div>
                           <br>
                           <div class="row">
                              <div class="col-xs-12 txt-center">
                                 This is to certify that the employee named herein above actually rendered services in this Office as shown by the service record below, each line of which is supported by appointment and other papers actually issued by this Office and approved by the authorities concerned.
                              </div>
                           </div>
                           <br>

                           <table class="rptBody" border=1>
                              <tr class="txt-center">
                                 <th colspan=2 rowspan=2 class="txt-center">
                                    SERVICE<br>
                                    <span class='rptFooter'>
                                       (Includsive Dates)
                                    </span>
                                 </th>
                                 <th colspan=3>
                                    RECORDS OF APPOINTMENT
                                 </th>
                                 <th>OFFICE/ENTITY</th>
                                 <th rowspan=3>L/V<br>ABS<br>w/o Pay</th>
                                 <th>SEPARATION</th>
                              </tr>
                              <tr class="txt-center">
                                 <th rowspan=2>DESIGNATIONS</th>
                                 <th rowspan=2>STATUS<br>(1)</th>
                                 <th rowspan=2>SALARY<br>(2)</th>
                                 <th rowspan=2>Station/Place<br>of Assignment</th>
                                 <th>(4)</th>
                              </tr>
                              <tr class="txt-center">
                                 <th>FROM</th>
                                 <th>TO</th>
                                 <th>DATE / CAUSE</th>
                              </tr>
                           <?php
                           while ($row_WorkExp = mysqli_fetch_assoc($rsWORKEXP)) {
                           ?>
                              <tr>
                                 <td class="txt-center"><?php echo $row_WorkExp["WorkStartDate"]; ?></td>
                                 <td class="txt-center"><?php echo $row_WorkExp["WorkEndDate"]; ?></td>
                                 <td><?php dispBlank(getRecord("Position",$row_WorkExp["PositionRefId"],"Name")); ?></td>
                                 <td><?php dispBlank(getRecord("EmpStatus",$row_WorkExp["EmpStatusRefId"],"Name")); ?></td>
                                 <td class="txt-right"><?php dispNumFormat($row_WorkExp["SalaryAmount"]); ?></td>
                                 <td><?php dispBlank(""); ?></td>
                                 <td><?php $row_WorkExp["LWOP"]; ?></td>
                                 <td><?php
                                    echo $row_WorkExp["SeparatedDate"]." / ";
                                    dispBlank($row_WorkExp["Reason"]); ?>
                                 </td>
                              </tr>
                           <?php
                           }
                           ?>
                           <tr class="txt-center">
                                 <td colspan=8>--- NOTHING FOLLOWS --- NOTHING FOLLOWS --- NOTHING FOLLOWS ---</td>
                           </tr>
                           </table><br>

                           <div class="row">
                              <div class="col-xs-12 txt-center">
                                 Issued in compliance with Executive Order No. 54 dated August 10, 1984, and in accordance with Circular, No. 68, dated August 10, 1954 of the System.
                              </div>
                           </div>
                           <br><br>
                           <div class="row">
                              <div class="col-xs-2 txt-right">DATE:</div>
                              <div class="col-xs-2" style="border-bottom:2px solid #000"><?php echo date("Y-m-d",time()); ?></div>
                              <div class="col-xs-2">&nbsp;</div>
                              <div class="col-xs-3 txt-right">CERTIFIED CORRECT:</div>
                              <div class="col-xs-2" style="border-bottom:2px solid #000">&nbsp;</div>
                              <div class="col-xs-1 txt-center"></div>
                           </div>
                        </p>
                     </div>

                     <hr>
                     <?php rptFooter(); ?>
                  </div>
         <?php
                  $recordsCount++;
               }
               echo '<div class="nextpage"></div>';
            }
            echo
            '<div class="lastpage">';
            rptHeader("SERVICE RECORD");
            echo
            '<span>RECORD COUNT : '.$recordsCount.'</span>
            <div>SEARCH CRITERIA:</div>';
            if ($searchCriteria == "") echo "<li>ALL RECORDS</li>";
            else {
               $crit_Arr = explode("|",$searchCriteria);
               for ($j=0;$j<count($crit_Arr);$j++) {
                  echo "<li>".$crit_Arr[$j]."</li>";
               }
            }
            rptFooter();
            echo
            '</div>';
         ?>
      </div>
   </body>
</html>