<?php
	include 'conn.e2e.php';
	include 'constant.e2e.php';
	include pathClass.'0620functions.e2e.php';
	include pathClass.'DTRFunction.e2e.php';

	
	/*2017-12-31*/
	/*2018-01-01*/
	/*$CompanyID		= FindFirst("employees","WHERE RefId = $emprefid","CompanyRefId");
	$Setting_json	= file_get_contents(json."Settings_".$CompanyID.".json");
	$Setting_arr   	= json_decode($Setting_json, true);*/
	/*function dtr_process($emprefid = "",$month = "", $year = ""){
		$curr_date		= date("Y-m-d",time());
		$curr_month     = date("m",time());
		$curr_year		= date("Y",time());
		$start_date		= $curr_year."-".$curr_month."-01";	
		$previous_date  = date('Y-m-d', strtotime($start_date.' - 1 day'));
		$previous_month = date('m', strtotime($previous_date));
		$previous_year  = date('Y', strtotime($previous_date));
		if ($emprefid == "") {
			if ($start_date == $curr_date) {
				$rs_emp		= SelectEach("employees","");
				if ($rs_emp) {
					while ($row_emp = mysqli_fetch_assoc($rs_emp)) {
						$emprefid 			= $row_emp["RefId"];
						$workschedule 		= FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","WorkScheduleRefId");
						$where 				= "WHERE EmployeesRefId = $emprefid AND Month = '$curr_month' AND Year = '$curr_year'";
						$check_dtr_process 	= FindFirst("dtr_process",$where,"RefId");
						if (!is_numeric($check_dtr_process)) {
							if ($workschedule != "") {
								$dtr_array 		= DTR_Summary($emprefid,$previous_month,$previous_year,$workschedule);
								$flds 			= "";
								$vals 			= "";	
								$table 			= "";
								foreach ($dtr_array as $key => $value) {
									$flds .= "`$key`,";
									$vals .= "'$value', ";
								}
								$save_dtr_process = f_SaveRecord("NEWSAVE","dtr_process",$flds,$vals);	
								if (!is_numeric($save_dtr_process)) {
									echo $save_dtr_process;
									return false;
								}
							}
							
						}
					}
				}
			}	
		} else {
			$workschedule 	= FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","WorkScheduleRefId");
			$where 			= "WHERE EmployeesRefId = $emprefid AND Month = '$curr_month' AND Year = '$curr_year'";
			$dtr_refid	 	= FindFirst("dtr_process",$where,"RefId");
			$dtr_array 		= DTR_Summary($emprefid,$month,$year,$workschedule);
			$fldnval 		= "";
			foreach ($dtr_array as $key => $value) {
				$fldnval .= "`$key` = '$value', ";
			}
			$edit_dtr_process = f_SaveRecord("EDITSAVE","dtr_process",$fldnval,$dtr_refid);	
			if ($edit_dtr_process != "") {
				echo $edit_dtr_process;
				return false;
			}
		}
		
	}*/
	mysqli_query($conn,"TRUNCATE dtr_process");
	$curr_date		= date("Y-m-d",time());
	$curr_month     = date("m",time());
	$curr_year		= date("Y",time());
	$start_date		= $curr_year."-".$curr_month."-01";
	$rs = SelectEach("employees","WHERE RefId = 4");
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$emprefid 		= $row["RefId"];
			$LastName       = $row["LastName"];
			$FirstName      = $row["FirstName"];
			$Name 			= strtoupper($FirstName." ".$LastName);
			$workschedule 	= FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","WorkScheduleRefId");		
			$AsOf 			= FindLast("employeescreditbalance","WHERE EmployeesRefId = $emprefid","BegBalAsOfDate");
			if (!$AsOf) {
				$AsOf = $start_date;
			}
			$Start_AsOf = date('Y-m-d', strtotime($AsOf.' + 1 day'));
			$AsOf_month = date("m",strtotime($Start_AsOf));
			$AsOf_year  = date("Y",strtotime($Start_AsOf));
			if ($workschedule != "") {
				for ($i=1; $i <= intval($curr_month) - 1; $i++) { 
					if ($i < 10) $i = "0".$i;
					$new_date = $AsOf_year."-".$i."-01";
					$dtr_array 		= DTR_Summary ($emprefid,$i,$AsOf_year,$workschedule);
					$flds 			= "";
					$vals 			= "";	
					$table 			= "dtr_process";
					foreach ($dtr_array as $key => $value) {
						$flds .= "`$key`,";
						$vals .= "'$value', ";
						echo $key." => ".$value."<br>"; 
						
					}
					echo $flds." => ".$vals."<br>";
					$save_dtr_process = f_SaveRecord("NEWSAVE",$table,$flds,$vals);
					if (is_numeric($save_dtr_process)) {
						echo "Successfully processed $Name's DTR for the month of ".date("F",strtotime($new_date))."<br>";
					} 
				}
			}
		}
	}
?>