<?php
   require_once "constant.e2e.php";
   require_once "conn.e2e.php";
   require_once pathClass.'0620functions.e2e.php';
   $REFID = getvalue("id");
   $path = $_SESSION['path'];
   $css = ["armyBlue1","armyBlue2","bgPIS"];
   $mynewfile = $sys->css_create($css);
   $SaveSuccessfull = 0;
?>

<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Issues"); ?>"></script>

      <style>
      /* MODAL START */
.clsmodal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 9; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%;
    height: 100%; /* Full height */
    overflow:auto; /* Enable scroll if needed */
    background-color: rgb(5,5,5); /* Fallback color */
    background-color: rgba(5,5,5,0.9---------------------); /* Black w/ opacity */
    text-align: center;
}

/* Modal Content */
div.ModalContent {
   margin: auto;
   margin-top:25px;
   height:95%;
   width:auto;
   max-height:calc(100% - 50px);
   max-width:1000px;
   transition:1s ease;
   position:relative;
   overflow:hidden;
   border:2px solid #fff;
   background-color:#111;
}
#bigIMG {
   width:auto;
   height:auto;
}
/* The Close Button */
.ModalClose {
   color: #aaa;
   float: right;
   font-size: 20px;
   font-weight: bold;
   width:40px;
   text-align:center;
   margin:0px;
   padding:3px;
}
.ModalClose:hover {
   background-color:red;
}
#FullScrnImg {
   position:absolute;
   right:10px;
   top:calc(100% - 30px);
   z-index:2;
   color:silver;
   font-size:20px;
}
#FullScrnImg:hover {
   color:#fff;
   cursor:pointer;
}
      </style>
   </head>
   <body style="background:#fff;" onload="selectMe(<?php echo $REFID ?>);">
   	   <div class="container margin-top10">
            <div class="row">
               <h4 class="pull-left">SYSTEM ISSUES</h4>
               <label class="pull-right" style="padding:5px;border:1px solid #888;" id="BackHome" onmouseover="this.style.cursor='pointer';">
                  <i class="fa fa-home" aria-hidden="true"></i>
               </label>
            </div>
            <div class="row bgLightGray border1 padd5">
               <div class="col-xs-12">
                  <label>Search:</label>
                  <input type="text" name="tID" placeholder="ID" value="<?php echo getvalue("tID") ?>">
                  <input type="text" name="tStatus" placeholder="Status" value="<?php echo getvalue("tStatus") ?>">
                  <input type="text" name="tCreated" placeholder="Created By" value="<?php echo getvalue("tCreated") ?>">
                  <input type="text" name="tAssigned" placeholder="Assigned To" value="<?php echo getvalue("tAssigned") ?>">
                  <input type="text" name="tWorkingBy" placeholder="Currently Working By" value="<?php echo getvalue("tWorkingBy") ?>">
                  <input type="button" value="Filter" name="btnFilter">
               </div>
            </div>
            <div class="row bgLightGray border1 padd5 margin-top">
               <div class="col-xs-12">
                  <form action="postComment.e2e.php" method="post" enctype="multipart/form-data" id="NewIssue">
                  <div id="divList">
                     <div id="spGridTable">
                        <?php
                           createButton("Post New Issue","btnINSERT","btn-cls4-sea","fa fa-file","");
                           bar();
                           /*$rs = SelectEach("issues","ORDER BY RefId Desc LIMIT 100");
                           if ($rs) {
                              while ($row = mysqli_fetch_assoc($rs)) {
                                 echo
                                 '<div class="border1" style="background:#fafafa;padding:10px;border-radius:5px;margin-top:5px;">
                                    <div>
                                       <h4><b>Title/Issues : </b><span class="fontB14">['.$row["RefId"].'] '.$row["Issue"].'</span></h4>
                                    </div>
                                    <div>
                                       <h5><b>Description : </b>'.$row["Description"].'</h5>
                                    </div>
                                    <div>
                                       <b>Created By : </b>
                                       '.strtoupper($row["CreatedBy"]).'
                                       <b style="margin-left:10px;">Posting Date : </b>
                                       '.($row["LastUpdateDate"]." ".$row["LastUpdateTime"]).'
                                    </div>
                                    <div>';
                                       if ($row["CreatedBy"] == $_SESSION['sess_user']) {
                                          echo
                                          '<div class="dropdown">
                                             <b>Status : </b><span id="issuesStatus">'.strtoupper($row["Status"]).'</span>&nbsp;
                                             <a href="#" class="dropdown-toggle link" data-toggle="dropdown">
                                                <i class="fa fa-chevron-down" aria-hidden="true" style="font-size:9pt;"></i>
                                             </a>
                                             <ul class="dropdown-menu">
                                                <li class="dropdown-header">Change Status</li>
                                                <li><a href="#" class="chnStatus" id="Close">Close</a></li>
                                                <li><a href="#" class="chnStatus" id="Cancel">Cancel</a></li>
                                                <li><a href="#" class="chnStatus" id="Resolve">Resolve</a></li>
                                                <li><a href="#" class="chnStatus" id="In-Progress">In Progress</a></li>
                                                <li><a href="#" class="chnStatus" id="Re-Open">Re-Open</a></li>
                                             </ul>
                                          </div>';
                                       } else {
                                          echo
                                          '<div><b>Status : </b>'.strtoupper($row["Status"]).'</div>';
                                       }
                                       echo
                                       '<div><b>Currently Working By : </b><span id="WorkingBy">'.strtoupper($row["CurrentlyWorkingBy"]).'</span></div>';
                                    echo
                                    '</div>';
                                    if (!empty($row["ImageFile"])) {
                                       echo
                                       '<div class="row padd5">
                                          <img src="'.pathPublic.'images/Comments/'.$row["ImageFile"].'" style="width:100%;">
                                       </div>';
                                    }

                                    $issuesRefId = $row["RefId"];
                                    $criteria  = " WHERE issuesRefId = $issuesRefId ORDER BY RefId Desc";
                                    $recordSet = f_Find("issuescomments",$criteria);

                                    bar();
                                    echo
                                    '<div class="row padd5" id="Comments">
                                       <p id="message"></p>
                                       <div class="form-group">
                                          <textarea class="form-input saveFields--" rows="2" name="char_Comments" placeholder="Reply Here"></textarea>
                                       </div>
                                       <div>
                                          <label>Forward To:</label>';
                                          $where = "where companyrefid = 1000 and branchrefid = 1 and sysgrouprefid = 2";
                                          createSelectSysUser("drpForward",$where);
                                       echo
                                       '</div>
                                       <input type="hidden" class="saveFields--" name="char_CommentBy" value="'.$_SESSION['sess_user'].'">
                                       <input type="hidden" class="saveFields--" name="sint_issuesRefId" value="'.$issuesRefId.'">
                                       <input type="hidden" class="saveFields--" name="hPostNewComment" value="new">
                                       <input type="hidden" class="form-input saveFields--" name="fn" value="PostNewComment">
                                       <div style="text-align:right;margin-top:10px;">
                                          <label for="imageComment" style="padding:5px;border:1px solid #888;">
                                             <i class="fa fa-picture-o" aria-hidden="true" title="Attach Image" onmouseover="this.style.cursor=\'pointer\';"></i>
                                          </label>
                                          <input type="file" name="imageComment" id="imageComment" style="display:none;" accept="image/*">
                                          <a href="javascript:void(0);" id="aSEND" style="margin-left:3px;padding:5px;border:1px solid #888"><i class="fa fa-paper-plane" aria-hidden="true" title="Send Reply"></i></a>
                                       </div>
                                    </div>';

                                    if ($recordSet) {
                                       echo '<div style="padding:10px;">';
                                       $rowcount = mysqli_num_rows($recordSet);
                                       if ($rowcount > 0) {
                                          echo '<h5>'.$rowcount.' Comment(s)</h5>';
                                          while ($rows = mysqli_fetch_assoc($recordSet)) {
                                             echo
                                             '<div class="padd5" id="Comments_'.$rows["RefId"].'"
                                                   style="border-left:5px solid #777;margin-top:5px;border-bottom:1px solid #777">
                                                <div><b>'.$rows["CommentBy"].':</b>&nbsp;&nbsp;'.$rows["Comments"].'</div>
                                                <div>'.($rows["LastUpdateDate"]." ".$rows["LastUpdateTime"]).'</div>';
                                                if (!empty($rows["PictureComments"])) {
                                                   echo
                                                   '<div style="margin-top:5px;">
                                                      <a href="javascript:void(0);"><img src="'.pathPublic.'images/Comments/'.$rows["PictureComments"].'" style="width:100%;" class="imgComments"></a>
                                                   </div>';
                                                }
                                             echo
                                             '</div>';

                                          }
                                       }
                                       echo '</div>';
                                    }
                                 echo
                                 '</div>';
                              }
                           }*/
                           $sql = "select * from issues where CompanyRefId = ".$CompanyId." AND BranchRefId = ".$BranchId;
                           if (getvalue("tStatus") != "")
                              $sql .= " and status like '".getvalue("tStatus")."%'";
                           if (getvalue("tCreated") != "")
                              $sql .= " and CreatedBy like '".getvalue("tCreated")."%'";
                           if (getvalue("tAssigned") != "")
                              $sql .= " and AssignedTo like '".getvalue("tAssigned")."%'";
                           if (getvalue("tWorkingBy") != "")
                              $sql .= " and CurrentlyWorkingBy like '".getvalue("tWorkingBy")."%'";
                           if (getvalue("tID") != "")
                              $sql .= " and RefId = ".getvalue("tID");
                           //echo $sql;
                           doGridTable("issues",
                                       ["Issue","Created By","Status","Assigned To","Currently Working By"],
                                       ["Issue","CreatedBy","Status","AssignedTo","CurrentlyWorkingBy"],
                                       $sql,
                                       [false,false,false,true],
                                       "gridTable");
                        ?>
                     </div>
                  </div>
                  <div id="divView">
                     <div id="EntryScrn">
                        <div class="row">
                           <div class="col-xs-6">
                           <?php
                              $list = [
                                       array("row"=>true,
                                             "name"=>"char_Issue",
                                             "col"=>"12",
                                             "id"=>"Issue",
                                             "label"=>"Issues Title",
                                             "class"=>"mandatory",
                                             "style"=>"")
                                    ];
                              createInput($list);
                              entryAlert("Issue","Issue is mandatory");
                           ?>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-6">
                              <div class="form-group">
                                 <label class="control-label" for="inputs">Description:</label>
                                 <textarea class="form-input saveFields-- mandatory" rows="5" id="Desc" name="char_Description"></textarea>
                              </div>
                              <?php entryAlert("Desc","Description is mandatory") ?>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-6">
                              <div class="form-group">
                                 <label class="control-label" for="inputs">Assigned To:</label>
                                 <?php
                                    $where = "where CompanyRefId = ".$CompanyId." AND BranchRefId = ".$BranchId." AND sysgrouprefid = 2";
                                    createSelectSysUser("drpAssigned",$where);
                                    echo "<br><br>";
                                    entryAlert("drpAssigned","Assigned is mandatory")
                                 ?>
                              </div>
                           </div>
                        </div>
                        <script language="JavaScript">
                           $(document).ready(function () {
                              $("#Issue").attr('maxlength', '50');
                              $("#drpAssigned").addClass('mandatory');
                           });
                        </script>
                        <input type="hidden" class="form-input saveFields--" name="char_CreatedBy" value="<?php echo $_SESSION['sess_user']; ?>">
                        <input type="hidden" class="form-input saveFields--" name="char_Status" value="open">
                        <input type="hidden" class="form-input saveFields--" name="hPostNewIssue" value="new">
                        <input type="hidden" class="form-input saveFields--" name="fn" value="PostNewIssues">
                        <div style="text-align:left">
                           <label for="postImage" style="padding:5px;border:1px solid #888;">
                              <i class="fa fa-picture-o" aria-hidden="true" title="Attach Image" onmouseover="this.style.cursor=\'pointer\';"></i>
                           </label>
                           <input type="file" name="postImage" id="postImage" style="display:none;" accept="image/*">
                           <button type="button" id="PostIssue" name="btnPostIssue" value="PostNewIssue" style="margin-left:20px;padding:5px;color:#222">
                              <i class="fa fa-paper-plane" aria-hidden="true" title="Post Now"></i>
                           </button>
                           <button type="button" id="CancelPost" style="margin-left:3px;padding:5px;color:#222">
                              <i class="fa fa-undo" aria-hidden="true" title="Cancel Post"></i>
                           </button>
                        </div>


                     </div>
                  </div>
                     <input type="hidden" id="hIssuesRefId" value="<?php echo $REFID; ?>">
                     <?php
                        $refid = "0";
                        $progFile = "scrnIssues";
                        $table = "issues";
                        include_once ("varHidden.e2e.php");
                     ?>
                  </form>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-2"></div>
               <div class="col-xs-8">
                  <div id="divComments">

                  </div>
               </div>
               <div class="col-xs-2"></div>
            </div>
         </div>

      <!-- The Modal -->
      <div id="ebmModal" class="clsmodal">
         <!-- Modal content -->
         <div class="ModalClose">
            <i class="fa fa-times" aria-hidden="true"></i>
         </div>
         <div class="ModalContent">
            <img src="" id="bigIMG" />
            <span class="glyphicon glyphicon-resize-full" aria-hidden="true" id="FullScrnImg"></span>
         </div>
      </div>

	</body>
</html>