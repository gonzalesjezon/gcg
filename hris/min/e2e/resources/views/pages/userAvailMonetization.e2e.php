<?php
   require_once "conn.e2e.php";
   require_once "constant.e2e.php";
   require_once pathClass.'0620RptFunctions.e2e.php';
   $table = "employees";
   $EmployeesRefId = getvalue("hEmpRefId");
   $cid = getvalue("hCompanyID");
   if ($cid != 2) {
      $defaul_monetize = "6";
   } else {
      $defaul_monetize = "10";
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      
      <style type="text/css">

      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("LEAVE MONETIZATION"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="container-fluid rptBody">
                        <div>
                           <div class="row" id="divList">
                              <div class="col-xs-12">
                                 <div class="panel-top">
                                    List of Leave Monetization Applications
                                 </div>
                                 <div class="panel-mid">
                                    <span id="spGridTable">
                                       <?php
                                          $table = "employeesleavemonetization";
                                          $gridTableHdr_arr = ["File Date", "Value(VL)", "Value(SL)", "Status"];
                                          $gridTableFld_arr = ["FiledDate", "VLValue", "SLValue", "Status"];
                                          $sql = "SELECT * FROM $table WHERE EmployeesRefId = $EmployeesRefId ORDER BY FiledDate";
                                          $Action = [false,true,false,true];
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      $Action,
                                                      "gridTable");
                                       ?>
                                    </span>
                                 </div>
                                 <div class="panel-bottom">
                                    <button type="button" class="btn-cls4-sea" id="btnINSERTNEW">
                                       <i class="fa fa-file"></i>&nbsp;&nbsp;&nbsp;Insert New
                                    </button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="divView">
                           <div id="">
                              <div class="row margin-top" id="EntryScrn">
                                 <div class="row">
                                    <div class="mypanel">
                                       <div class="panel-top">
                                          LEAVE MONETIZATION
                                       </div>
                                       <div class="panel-mid">
                                          <div id="EntryScrn">
                                             <div class="padd10">
                                                <input type="hidden" value="<?php echo $EmployeesRefId; ?>" class="form-input saveFields--" name="sint_EmployeesRefId">
                                                <div class="row">
                                                   <div class="col-xs-4">
                                                      <?php label("Date:",""); ?><br>
                                                      <input type="text" class="form-input date-- saveFields--" name="date_FiledDate" value="<?php today(); ?>">
                                                   </div>
                                                   <div class="col-xs-8">
                                                      <div class="row">
                                                         <div class="col-xs-12 text-center">
                                                            <label>
                                                               Leave Credit Balances
                                                            </label>
                                                         </div>
                                                      </div>
                                                      <div class="row margin-top">
                                                         <div class="col-xs-6 text-center">
                                                            <input type="text" class="form-disp saveFields--" disabled name="deci_VLBalance">
                                                            <br>
                                                            VL
                                                         </div>
                                                         <div class="col-xs-6 text-center">
                                                            <input type="text" class="form-disp saveFields--" disabled name="deci_SLBalance">
                                                            <br>
                                                            SL
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-4">
                                                      <label>
                                                         Type of Monetization
                                                      </label>
                                                      <br>
                                                      <select name="sint_IsHalf" id="sint_IsHalf" class="form-input saveFields--">
                                                         <option value="0">Regular Monetization</option>
                                                         <option value="1">Special Monetization</option>
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="row margin-top" id="regular_monetization">
                                                   <div class="col-xs-6">
                                                      <div class="row">
                                                         <div class="col-xs-12 text-center" style="margin-top: 20px;">
                                                            <label>No. Of Days to Monetize</label>
                                                         </div>
                                                      </div>
                                                      <div class="row margin-top">
                                                         <div class="col-xs-6 text-center">
                                                            <select name="deci_VLValue" id="deci_VLValue" class="form-input saveFields--">
                                                               <option value=""></option>
                                                               <?php
                                                                  for ($a=$defaul_monetize; $a <= 30 ; $a++) { 
                                                                     echo '<option value="'.number_format($a,3).'">'.$a.'</option>'."\n";
                                                                  }
                                                               ?>
                                                            </select>
                                                            <br>
                                                            <label>VL</label>
                                                         </div>
                                                         <div class="col-xs-6 text-center">
                                                            <select name="deci_SLValue" id="deci_SLValue" class="form-input saveFields--" disabled>
                                                               <option value=""></option>
                                                               <?php
                                                                  for ($b=$defaul_monetize; $b <= 30 ; $b++) { 
                                                                     echo '<option value="'.number_format($b,3).'">'.$b.'</option>'."\n";
                                                                  }
                                                               ?>
                                                            </select>
                                                            <br>
                                                            <label>SL</label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="col-xs-6" style="display: none;">
                                                      <div class="row">
                                                         <div class="col-xs-12">
                                                            <label>CHECKING</label>
                                                         </div>
                                                      </div>
                                                      <div class="row margin-top">
                                                         <div class="col-xs-6">
                                                            <label>Salary:</label>
                                                            <input type="text" name="hSalary" id="hSalary" class="form-input" readonly>
                                                         </div>
                                                      </div>
                                                      <div class="row margin-top">
                                                         <div class="col-xs-6 text-center">
                                                            <input type="text" name="deci_VLAmount" id="deci_VLAmount" class="form-input saveFields--" readonly>
                                                            <br>
                                                            <label>VL Amount</label>
                                                         </div>
                                                         <div class="col-xs-6 text-center">
                                                            <input type="text" name="deci_SLAmount" id="deci_SLAmount" class="form-input saveFields--" readonly>
                                                            <br>
                                                            <label>SL Amount</label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row margin-top" id="fifty_monetization">
                                                   <div class="col-xs-6">
                                                      <div class="form-group">
                                                         <label>Reason For Monetization</label>
                                                         <textarea class="form-input saveFields--" rows="5" name="char_Remarks" id="char_Remarks" placeholder="remarks"></textarea>
                                                      </div>
                                                   </div>   
                                                </div>
                                             </div>
                                          </div>
                                          <?php
                                             spacer(10);
                                             btnSACABA([true,true,true]);
                                          ?>
                                       </div>

                                       <div class="panel-bottom"></div>
                                    </div>
                                 </div>
                              </div>   
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "employeesleavemonetization";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>

   <script type="text/javascript">
      $(document).ready(function () {
         $("#fifty_monetization").hide();
         $("#btnINSERTNEW").click(function () {
            
            var cid = $("#hCompanyID").val();
            var bid = $("#hBranchID").val();
            var emprefid = $("[name='sint_EmployeesRefId']").val();
            getCreditBalance(emprefid,cid,bid);
            getSalary(emprefid,"hSalary",cid,bid);            
            $("#divList").slideUp();
            $("#divView").show(500);
         });
         $("#sint_IsHalf").change(function () {
            var value = $(this).val();
            if (value == 0) {
               $("#char_Remarks").val("");
               $("#regular_monetization").show();
               $("#fifty_monetization").hide();
               $("[name='deci_SLValue']").prop("disabled",true);
            } else {
               $("#deci_VLValue, #deci_SLValue, #deci_VLAmount, #deci_SLAmount").val("");
               $("#regular_monetization").hide();
               $("#fifty_monetization").show();
               $("[name='deci_SLValue']").prop("disabled",false);
            }
         });
         $("#deci_VLValue").change(function () {
            var value = $(this).val();
            var salary = $("#hSalary").val();
            if (value != "") {
               compute(value,"deci_VLAmount",salary);      
            }
         });
         $("#deci_SLValue").change(function () {
            var value = $(this).val();
            var salary = $("#hSalary").val();
            if (value != "") {
               compute(value,"deci_SLAmount",salary);      
            }
         });
      });
      function selectMe(refid) {
         printAttachment(refid);  
      }
      function printAttachment(refid){
         $("#rptContent").attr("src","blank.htm");
         var rptFile = "rpt_Leave_Availment_Monetization";
         var url = "ReportCaller.e2e.php?file=" + rptFile;
         url += "&refid=" + refid;
         url += "&" + $("[name='hgParam']").val();
         $("#prnModal").modal();
         $("#rptContent").attr("src",url);
      }
      function afterEditSave(refid) {
         alert("Record Updated");
         gotoscrn($("#hProg").val(),"");
      }
      function afterNewSave(newRefId) {
         alert("Successfully Avail. Wait for the HR Approval");
         gotoscrn($("#hProg").val(),"");
      }
      function getCreditBalance(emprefid,CompanyId,BranchId) {
         $.get("trn.e2e.php",
         {
            fn:"getCreditBalance",
            emprefid:emprefid,
            CompanyId:CompanyId,
            BranchId:BranchId,
            vlobj:"deci_VLBalance",
            slobj:"deci_SLBalance"

         },
         function(data,status){
            if (status == 'success') {
               eval(data);
            } else {
               console.log(status + " - " + data);
            }
         });
      }
      function getSalary(emprefid,obj,CompanyId,BranchId) {
         $.get("trn.e2e.php",
         {
            fn:"getSalary",
            emprefid:emprefid,
            CompanyId:CompanyId,
            BranchId:BranchId,
            obj:obj

         },
         function(data,status){
            if (status == 'success') {
               eval(data);
            } else {
               console.log(status + " - " + data);
            }
         });
      }
   </script>
</html>



