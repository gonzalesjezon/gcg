<?php
	error_reporting(E_ALL);
   	ini_set('display_errors', 1);
	session_start();
   	include_once "constant.e2e.php";
   	include_once pathClass.'0620functions.e2e.php';
   	$user = getvalue("hUser");

   	$funcname = getvalue("fn");
   	$params   = getvalue("params");
   	if (!empty($funcname)) {
      	$funcname($params);
   	} else {
      	echo 'alert("Error... No Function defined");';
   	}

   	function getIPCR() {
	   	$refid 	= getvalue("refid");
	   	$ipcr_row = FindFirst("spms_ipcr","WHERE RefId = '$refid'","*");
	   	if ($ipcr_row) {
	   		echo '$("#sint_Semester").val("'.$ipcr_row["Semester"].'");'."\n";
	   		echo '$("#sint_Year").val("'.$ipcr_row["Year"].'");'."\n";
	   		echo '$("#Supervisor_Rating").val("'.$ipcr_row["Supervisor_Rating"].'");'."\n";
	   		echo '$("#Final_Rating").val("'.$ipcr_row["Final_Rating"].'");'."\n";
	   	} 
	}
	function getOPCR() {
	   	$refid 	= getvalue("refid");
	   	$ipcr_row = FindFirst("spms_opcr","WHERE RefId = '$refid'","*");
	   	if ($ipcr_row) {
	   		echo '$("#sint_Semester").val("'.$ipcr_row["Semester"].'");'."\n";
	   		echo '$("#sint_Year").val("'.$ipcr_row["Year"].'");'."\n";
	   		echo '$("#Adjectival_Rating").val("'.$ipcr_row["Adjectival_Rating"].'");'."\n";
	   		echo '$("#Final_Rating").val("'.$ipcr_row["Final_Rating"].'");'."\n";
	   		echo '$("#Overall_Rating").val("'.$ipcr_row["Overall_Rating"].'");'."\n";
	   		echo '$("#sint_OfficeRefId").val("'.$ipcr_row["OfficeRefId"].'");'."\n";
	   	} 
	}
	function getOPCR_detail() {
	   	$refid 	= getvalue("refid");
	   	$rs  	= SelectEach("opcr_details","WHERE spms_opcr_id = '$refid'");
	   	if ($rs) {
	   		$count = 0;
	   		$str   = "";
	   		while ($row = mysqli_fetch_assoc($rs)) {
	   			$count++;
	   			$str .= '
	   				<div id="EntryIPCR_'.$count.'" class="entry201">
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>OPCR #'.$count.' </label>
                             <input type="hidden" name="refid_'.$count.'" id="refid_'.$count.'" value="'.$row["RefId"].'">
                          </div>
                       </div>
                       <?php bar(); ?>
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>OUTPUT</label>
                             <textarea class="form-input" rows="4" name="output_'.$count.'" id="output_'.$count.'">'.$row["output"].'</textarea>
                          </div>
                          <div class="col-xs-6">
                             <label>SUCCESS INDICATORS (TARGETS+MEASURES)</label>
                             <textarea class="form-input" rows="4" name="si_'.$count.'" id="si_'.$count.'">'.htmlentities($row["success_indicator"]).'</textarea>
                          </div>
                       </div>
                       <div class="row margin-top">
                         <div class="col-xs-6">
                            <label>ALLOTTED BUDGET</label>
                            <textarea class="form-input" rows="4" name="budget_'.$count.'" id="budget_'.$count.'">'.htmlentities($row["allotted_budget"]).'</textarea>
                         </div>
                         <div class="col-xs-6">
                            <label>Division/Individuals Accountable</label>
                            <textarea class="form-input" rows="4" name="accountable_'.$count.'" id="accountable_'.$count.'">'.htmlentities($row["accountable"]).'</textarea>
                         </div>
                      </div>
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>ACTUAL ACCOMPLISHMENTS</label>
                             <textarea class="form-input" rows="6" name="accomplishment_'.$count.'" id="accomplishment_'.$count.'">'.htmlentities($row["accomplishment"]).'</textarea>
                          </div>
                          <div class="col-xs-3">
                             <div class="row">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-8 text-center">
                                   <label>Rating</label>
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>Q<sup>1</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="q1_'.$count.'" id="q1_'.$count.'" value="'.$row["q1"].'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>E<sup>2</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="e2_'.$count.'" id="e2_'.$count.'" value="'.$row["e2"].'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>T<sup>3</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="t3_'.$count.'" id="t3_'.$count.'" value="'.$row["t3"].'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>A<sup>4</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="a4_'.$count.'" id="a4_'.$count.'" value="'.$row["a4"].'">
                                </div>
                             </div>
                          </div>
                       </div>
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>REMARKS</label>
                             <textarea class="form-input" rows="5" name="remarks_'.$count.'" id="remarks_'.$count.'">'.htmlentities($row["remarks"]).'</textarea>
                          </div>
                       </div>
                    </div>
                    <br>
	   			';
	   		}
	   	}
	   	echo $str;
	}
	function getIPCR_detail() {
	   	$refid 	= getvalue("refid");
	   	$rs  	= SelectEach("ipcr_details","WHERE spms_ipcr_id = '$refid'");
	   	if ($rs) {
	   		$count = 0;
	   		$str   = "";
	   		while ($row = mysqli_fetch_assoc($rs)) {
	   			$count++;
	   			$str .= '
	   				<div id="EntryIPCR_'.$count.'" class="entry201">
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>IPCR #'.$count.' </label>
                             <input type="hidden" name="refid_'.$count.'" id="refid_'.$count.'" value="'.$row["RefId"].'">
                          </div>
                       </div>
                       <?php bar(); ?>
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>OUTPUT</label>
                             <textarea class="form-input" rows="4" name="output_'.$count.'" id="output_'.$count.'">'.$row["output"].'</textarea>
                          </div>
                          <div class="col-xs-6">
                             <label>SUCCESS INDICATORS (TARGETS+MEASURES)</label>
                             <textarea class="form-input" rows="4" name="si_'.$count.'" id="si_'.$count.'">'.htmlentities($row["success_indicator"]).'</textarea>
                          </div>
                       </div>
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>ACTUAL ACCOMPLISHMENTS</label>
                             <textarea class="form-input" rows="6" name="accomplishment_'.$count.'" id="accomplishment_'.$count.'">'.htmlentities($row["accomplishment"]).'</textarea>
                          </div>
                          <div class="col-xs-3">
                             <div class="row">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-8 text-center">
                                   <label>Employee Rating</label>
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>Q<sup>1</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="q1_'.$count.'" id="q1_'.$count.'" value="'.$row["q1"].'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>E<sup>2</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="e2_'.$count.'" id="e2_'.$count.'" value="'.$row["e2"].'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>T<sup>3</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="t3_'.$count.'" id="t3_'.$count.'" value="'.$row["t3"].'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>A<sup>4</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="a4_'.$count.'" id="a4_'.$count.'" value="'.$row["a4"].'">
                                </div>
                             </div>
                          </div>
                          <div class="col-xs-3">
                             <div class="row">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-8 text-center">
                                   <label>Supervisor Rating</label>
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>Q<sup>1</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="'.$count.'" id="'.$count.'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>E<sup>2</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="'.$count.'" id="'.$count.'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>T<sup>3</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="'.$count.'" id="'.$count.'">
                                </div>
                             </div>
                             <div class="row margin-top">
                                <div class="col-xs-4 text-center">
                                   <label>A<sup>4</sup></label>
                                </div>
                                <div class="col-xs-8">
                                   <input type="text" class="form-input" name="'.$count.'" id="'.$count.'">
                                </div>
                             </div>
                          </div>
                       </div>
                       <div class="row margin-top">
                          <div class="col-xs-6">
                             <label>REMARKS</label>
                             <textarea class="form-input" rows="5" name="remarks_'.$count.'" id="remarks_'.$count.'">'.htmlentities($row["remarks"]).'</textarea>
                          </div>
                       </div>
                    </div>
                    <br>
	   			';
	   		}
	   	}
	   	echo $str;
	}
	function saveIPCR(){
	   	$emprefid 			= getvalue("sint_EmployeesRefId");
	   	$Division 			= FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","DivisionRefId");
	   	$Division 			= intval($Division);
	   	$count 				= getvalue("count");
	   	$quarter  			= getvalue("sint_Semester");
	   	$year 	 			= getvalue("sint_Year");
	   	$Final_Rating 		= getvalue("Final_Rating");
	   	$Supervisor_Rating 	= getvalue("Supervisor_Rating");
	   	$error 	 			= 0;
	   	$ipcr_refid 		= getvalue("sint_IPCRRefId");
	   	$Final_Rating 		= intval($Final_Rating);
	   	$Supervisor_Rating 	= intval($Supervisor_Rating);
	   	// var_dump($_POST);
	   	// return false;
	   	if (intval($ipcr_refid) == 0) {
	   		$ipcr_fld = "";
		   	$ipcr_val = "";
		   	$ipcr_fld .= "`EmployeesRefId`, `DivisionRefId`, `Semester`, `Year`, `Final_Rating`, `Supervisor_Rating`, ";
		   	$ipcr_val .= "'$emprefid', '$Division', '$quarter', '$year', '$Final_Rating', '$Supervisor_Rating',";
		   	$spms_ipcr_id = f_SaveRecord("NEWSAVE","spms_ipcr",$ipcr_fld,$ipcr_val);
		   	if (is_numeric($spms_ipcr_id)) {
		   		for ($a=1; $a <= $count; $a++) { 
		   			$output 	= realEscape($_POST["output_".$a]); 
		   			if ($output != "") {
		   				$ipcr_detail_fld 	= "";
			   			$ipcr_detail_val 	= "";
			   			$si 				= realEscape($_POST["si_".$a]);
			   			$accomplishment 	= realEscape($_POST["accomplishment_".$a]);
			   			$success_indicator 	= realEscape($_POST["si_".$a]);
			   			$q1 				= realEscape($_POST["q1_".$a]);
			   			$e2 				= realEscape($_POST["e2_".$a]);
			   			$t3 				= realEscape($_POST["t3_".$a]);
			   			$a4 				= realEscape($_POST["a4_".$a]);
			   			$remarks 			= realEscape($_POST["remarks_".$a]);
			   			$ipcr_detail_fld 	.= "spms_ipcr_id, output, accomplishment, q1, e2, t3, a4, remarks,";
			   			$ipcr_detail_fld 	.= "success_indicator, ";
			   			$ipcr_detail_val 	.= "'$spms_ipcr_id', '$output', '$accomplishment', '$q1', '$e2',";
			   			$ipcr_detail_val 	.= "'$t3', '$a4', '$remarks', '$success_indicator',";
			   			$save_sp 			= f_SaveRecord("NEWSAVE",
			   								  			   "ipcr_details",
			   								  			   $ipcr_detail_fld,
			   								  			   $ipcr_detail_val);
					   	if (!is_numeric($save_sp)) {
					   		$error++;
					   	}
		   			}
			   	}	
		   	} else {
		   		$error++;
		   	}
		   	if ($error == 0) {
		   		echo 'afterSave();';
		   	} else {
		   		echo "Error";
		   	}	
	   	} else {
	   		$ipcr_fldnval 	= "Semester = '$quarter', Year = '$year',";
	   		$ipcr_fldnval   .= "Supervisor_Rating = '$Supervisor_Rating', ";
	   		$ipcr_fldnval   .= "Final_Rating = '$Final_Rating', ";
	   		$update_ipcr 	= f_SaveRecord("EDITSAVE","spms_ipcr",$ipcr_fldnval,$ipcr_refid);
	   		if ($update_ipcr == "") {
	   			for ($a=1; $a <= $count; $a++) { 
	   				$refid 		= realEscape($_POST["refid_".$a]); 
		   			$output 	= realEscape($_POST["output_".$a]); 
		   			if ($refid != "") {
		   				if ($output != "") {
			   				$ipcr_detail_fldnval = "";
				   			$si 				= realEscape($_POST["si_".$a]);
				   			$accomplishment 	= realEscape($_POST["accomplishment_".$a]);
				   			$success_indicator 	= realEscape($_POST["si_".$a]);
				   			$q1 				= realEscape($_POST["q1_".$a]);
				   			$e2 				= realEscape($_POST["e2_".$a]);
				   			$t3 				= realEscape($_POST["t3_".$a]);
				   			$a4 				= realEscape($_POST["a4_".$a]);
				   			$remarks 			= realEscape($_POST["remarks_".$a]);
				   			$ipcr_detail_fldnval = "output = '$output', accomplishment = '$accomplishment', ";
				   			$ipcr_detail_fldnval .= "q1 = '$q1', e2 = '$e2', t3 = '$t3', a4 = '$a4', ";
				   			$ipcr_detail_fldnval .= "remarks = '$remarks', ";
				   			$ipcr_detail_fldnval .= "success_indicator = '$success_indicator',";
				   			$save_sp 			= f_SaveRecord("EDITSAVE",
				   								  			   "ipcr_details",
				   								  			   $ipcr_detail_fldnval,
				   								  			   $refid);
						   	if ($save_sp != "") {
						   		$error++;
						   	}
			   			}	
		   			} else {
		   				if ($output != "") {
			   				$ipcr_detail_fld 	= "";
				   			$ipcr_detail_val 	= "";
				   			$si 				= realEscape($_POST["si_".$a]);
				   			$accomplishment 	= realEscape($_POST["accomplishment_".$a]);
				   			$success_indicator 	= realEscape($_POST["si_".$a]);
				   			$q1 				= realEscape($_POST["q1_".$a]);
				   			$e2 				= realEscape($_POST["e2_".$a]);
				   			$t3 				= realEscape($_POST["t3_".$a]);
				   			$a4 				= realEscape($_POST["a4_".$a]);
				   			$remarks 			= realEscape($_POST["remarks_".$a]);
				   			$ipcr_detail_fld 	.= "spms_ipcr_id, output, accomplishment, q1, e2, t3, a4, remarks,";
				   			$ipcr_detail_fld 	.= "success_indicator, ";
				   			$ipcr_detail_val 	.= "'$ipcr_refid', '$output', '$accomplishment', '$q1', '$e2',";
				   			$ipcr_detail_val 	.= "'$t3', '$a4', '$remarks', '$success_indicator',";
				   			$save_sp 			= f_SaveRecord("NEWSAVE",
				   								  			   "ipcr_details",
				   								  			   $ipcr_detail_fld,
				   								  			   $ipcr_detail_val);
						   	if (!is_numeric($save_sp)) {
						   		$error++;
						   	}
			   			}
		   			}
			   	}	
	   		} else {
	   			$error++;
	   		}
	   		if ($error == 0) {
		   		echo 'afterEdit();';
		   	} else {
		   		echo "Error";
		   	}
	   	}   	
	}
	function saveOPCR(){
	   	$Office 			= getvalue("sint_OfficeRefId");
	   	$count 				= getvalue("count");
	   	$quarter  			= getvalue("sint_Semester");
	   	$year 	 			= getvalue("sint_Year");
	   	$Final_Rating 		= getvalue("Final_Rating");
	   	$Adjectival_Rating 	= getvalue("Adjectival_Rating");
	   	$Overall_Rating 	= getvalue("Overall_Rating");
	   	$error 	 			= 0;
	   	$ipcr_refid 		= getvalue("sint_IPCRRefId");
	   	$Final_Rating 		= intval($Final_Rating);
	   	$Adjectival_Rating 	= intval($Adjectival_Rating);
	   	$Overall_Rating 	= intval($Overall_Rating);
	   	// var_dump($_POST);
	   	// return false;
	   	if (intval($ipcr_refid) == 0) {
	   		$ipcr_fld = "";
		   	$ipcr_val = "";
		   	$ipcr_fld .= "`OfficeRefId`, `Semester`, `Year`, `Final_Rating`, `Adjectival_Rating`, `Overall_Rating`, ";
		   	$ipcr_val .= "'$Office', '$quarter', '$year', '$Final_Rating', '$Adjectival_Rating', '$Overall_Rating',";
		   	$spms_ipcr_id = f_SaveRecord("NEWSAVE","spms_opcr",$ipcr_fld,$ipcr_val);
		   	if (is_numeric($spms_ipcr_id)) {
		   		for ($a=1; $a <= $count; $a++) { 
		   			$output 	= realEscape($_POST["output_".$a]); 
		   			if ($output != "") {
		   				$ipcr_detail_fld 	= "";
			   			$ipcr_detail_val 	= "";
			   			$si 				= realEscape($_POST["si_".$a]);
			   			$accomplishment 	= realEscape($_POST["accomplishment_".$a]);
			   			$accountable 		= realEscape($_POST["accountable_".$a]);
			   			$allotted_budget 	= realEscape($_POST["budget_".$a]);
			   			$success_indicator 	= realEscape($_POST["si_".$a]);
			   			$q1 				= realEscape($_POST["q1_".$a]);
			   			$e2 				= realEscape($_POST["e2_".$a]);
			   			$t3 				= realEscape($_POST["t3_".$a]);
			   			$a4 				= realEscape($_POST["a4_".$a]);
			   			$remarks 			= realEscape($_POST["remarks_".$a]);
			   			$ipcr_detail_fld 	.= "spms_opcr_id, output, accomplishment, q1, e2, t3, a4, remarks,";
			   			$ipcr_detail_fld 	.= "success_indicator, allotted_budget, accountable, ";
			   			$ipcr_detail_val 	.= "'$spms_ipcr_id', '$output', '$accomplishment', '$q1', '$e2',";
			   			$ipcr_detail_val 	.= "'$t3', '$a4', '$remarks', '$success_indicator',";
			   			$ipcr_detail_val 	.= "'$allotted_budget', '$accountable',";
			   			$save_sp 			= f_SaveRecord("NEWSAVE",
			   								  			   "opcr_details",
			   								  			   $ipcr_detail_fld,
			   								  			   $ipcr_detail_val);
					   	if (!is_numeric($save_sp)) {
					   		$error++;
					   	}
		   			}
			   	}	
		   	} else {
		   		$error++;
		   	}
		   	if ($error == 0) {
		   		echo 'afterSave();';
		   	} else {
		   		echo "Error";
		   	}	
	   	} else {
	   		$ipcr_fldnval 	= "Semester = '$quarter', Year = '$year',";
	   		$ipcr_fldnval   .= "Adjectival_Rating = '$Adjectival_Rating', ";
	   		$ipcr_fldnval   .= "Overall_Rating = '$Overall_Rating', ";
	   		$ipcr_fldnval   .= "Final_Rating = '$Final_Rating', ";
	   		$update_ipcr 	= f_SaveRecord("EDITSAVE","spms_opcr",$ipcr_fldnval,$ipcr_refid);
	   		if ($update_ipcr == "") {
	   			for ($a=1; $a <= $count; $a++) { 
	   				$refid 		= realEscape($_POST["refid_".$a]); 
		   			$output 	= realEscape($_POST["output_".$a]); 
		   			if ($refid != "") {
		   				if ($output != "") {
			   				$ipcr_detail_fldnval = "";
				   			$si 				= realEscape($_POST["si_".$a]);
				   			$accomplishment 	= realEscape($_POST["accomplishment_".$a]);
				   			$accountable 		= realEscape($_POST["accountable_".$a]);
				   			$allotted_budget 	= realEscape($_POST["budget_".$a]);
				   			$success_indicator 	= realEscape($_POST["si_".$a]);
				   			$q1 				= realEscape($_POST["q1_".$a]);
				   			$e2 				= realEscape($_POST["e2_".$a]);
				   			$t3 				= realEscape($_POST["t3_".$a]);
				   			$a4 				= realEscape($_POST["a4_".$a]);
				   			$remarks 			= realEscape($_POST["remarks_".$a]);
				   			$ipcr_detail_fldnval = "output = '$output', accomplishment = '$accomplishment', ";
				   			$ipcr_detail_fldnval .= "q1 = '$q1', e2 = '$e2', t3 = '$t3', a4 = '$a4', ";
				   			$ipcr_detail_fldnval .= "remarks = '$remarks', ";
				   			$ipcr_detail_fldnval .= "allotted_budget = '$allotted_budget', ";
				   			$ipcr_detail_fldnval .= "accountable = '$accountable', ";
				   			$ipcr_detail_fldnval .= "success_indicator = '$success_indicator',";
				   			$save_sp 			= f_SaveRecord("EDITSAVE",
				   								  			   "ipcr_details",
				   								  			   $ipcr_detail_fldnval,
				   								  			   $refid);
						   	if ($save_sp != "") {
						   		$error++;
						   	}
			   			}	
		   			} else {
		   				if ($output != "") {
			   				$ipcr_detail_fld 	= "";
				   			$ipcr_detail_val 	= "";
				   			$si 				= realEscape($_POST["si_".$a]);
				   			$accomplishment 	= realEscape($_POST["accomplishment_".$a]);
				   			$accountable 		= realEscape($_POST["accountable_".$a]);
				   			$allotted_budget 	= realEscape($_POST["budget_".$a]);
				   			$success_indicator 	= realEscape($_POST["si_".$a]);
				   			$q1 				= realEscape($_POST["q1_".$a]);
				   			$e2 				= realEscape($_POST["e2_".$a]);
				   			$t3 				= realEscape($_POST["t3_".$a]);
				   			$a4 				= realEscape($_POST["a4_".$a]);
				   			$remarks 			= realEscape($_POST["remarks_".$a]);
				   			$ipcr_detail_fld 	.= "spms_opcr_id, output, accomplishment, q1, e2, t3, a4, remarks,";
				   			$ipcr_detail_fld 	.= "success_indicator, allotted_budget, accountable, ";
				   			$ipcr_detail_val 	.= "'$ipcr_refid', '$output', '$accomplishment', '$q1', '$e2',";
				   			$ipcr_detail_val 	.= "'$t3', '$a4', '$remarks', '$success_indicator',";
				   			$ipcr_detail_val 	.= "'$allotted_budget', '$accountable',";
				   			$save_sp 			= f_SaveRecord("NEWSAVE",
				   								  			   "opcr_details",
				   								  			   $ipcr_detail_fld,
				   								  			   $ipcr_detail_val);
						   	if (!is_numeric($save_sp)) {
						   		$error++;
						   	}
			   			}
		   			}
			   	}	
	   		} else {
	   			$error++;
	   		}
	   		if ($error == 0) {
		   		echo 'afterEdit();';
		   	} else {
		   		echo "Error";
		   	}
	   	}   	
	}
?>