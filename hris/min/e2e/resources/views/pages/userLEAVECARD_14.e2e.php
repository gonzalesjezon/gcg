<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $whereClause = "WHERE RefId = ".getvalue('hUserRefId');
   $year = date("Y",time());
?>
<!DOCTYPE html>
<html>
   <head>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript">
         $(document).ready(function () {
            $("#btnPrint").click(function () {
               var head = $("head").html();
               printDiv('div_CONTENT',head);
            });
         });
      </script>
      <style type="text/css">
         th {
            text-align: center;
         }
         @page { size: landscape; }
      </style>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis") ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("Leave Card"); ?>
            <div class="container-fluid margin-top">
               <button type="button" id="btnPrint" class="btn-cls4-lemon">PRINT</button>
               <div class="row">
                  <div class="col-sm-11" id="div_CONTENT">
                     <?php
                        $rsEmployees = SelectEach("employees",$whereClause);
                        if ($rsEmployees) {
                           while ($row = mysqli_fetch_assoc($rsEmployees)) {
                              $vl_earned_carried = 0; 
                              $sl_earned_carried = 0; 
                              $EmployeesRefId = $row["RefId"];
                              $CompanyRefId   = $row["CompanyRefId"];
                              $BranchRefId    = $row["BranchRefId"];
                              $emp_row = FindFirst("empinformation","WHERE EmployeesRefId = ".$row["RefId"],"*");
                              if ($emp_row) {
                                 $appt    = $emp_row["ApptStatusRefId"];
                                 $appt    = getRecord("apptstatus",$appt,"Name");
                                 $div     = getRecord("division",$emp_row["DivisionRefId"],"Name");
                                 $hired   = date("d F Y",strtotime($emp_row["HiredDate"]));
                                 $worksched = $emp_row["WorkScheduleRefId"];
                              } else {
                                 $appt    = "";
                                 $div     = "";
                                 $hired   = "";
                                 $worksched = "";
                              }
                              if ($worksched != "") {
                                 rptHeader(getRptName("rptLeave_Card"));
                     ?>               
                              <div class="row" style="padding:10px;">
                                 <div class="col-xs-6">
                                    <?php 
                                       echo "NAME : ".$row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];  
                                    ?>
                                 </div>
                                 <div class="col-xs-6">
                                    Department
                                 </div>
                              </div>
                              <table border="1" width="100%">
                                 <thead>
                                    <tr class="colHEADER">
                                       <th rowspan="3" style="width: 15%;">Period Covered</th>
                                       <th rowspan="3" style="width: 15%;">Particular</th>
                                       <th colspan="6">Vacation Leave</th>
                                       <th colspan="4">Sick Leave</th>
                                       <th rowspan="3">AVAILABLE<br>SPL</th>
                                       <th rowspan="3">Remarks</th>
                                    </tr>
                                    <tr class="colHEADER">
                                       <th rowspan="2">EARNED</th>
                                       <th rowspan="2">ABSENCE</th>
                                       <th colspan="2">UT W/Pay</th>
                                       <th rowspan="2">BALANCE</th>
                                       <th rowspan="2">ABSENCE<br>UT W/O PAY</th>
                                       <th rowspan="2">EARNED</th>
                                       <th rowspan="2">ABSENCE<br>UT W/PAY</th>
                                       <th rowspan="2">BALANCE</th>
                                       <th rowspan="2">ABSENCE<br>UT W/O PAY</th>
                                    </tr>
                                    <tr class="colHEADER">
                                       <th>HOURS</th>
                                       <th>MINS</th>
                                    </tr>
                                 </thead>   
                                 <tbody>
                                    <?php
                                       //return false;
                                       $whereVL         = "WHERE CompanyRefId = ".$row['CompanyRefId']; 
                                       $whereVL         .= " AND BranchRefId = ".$row["BranchRefId"];
                                       $whereVL         .= " AND EmployeesRefId = ".$row['RefId']." AND NameCredits = 'VL'";
                                       $row_vl          = FindOrderBy("employeescreditbalance",$whereVL,"*","BegBalAsOfDate");
                                       if ($row_vl) {
                                          $vl = $row_vl["BeginningBalance"];
                                          $AsOf = $row_vl["BegBalAsOfDate"];
                                          $start_date = date("Y-m-d",strtotime( "$AsOf + 1 day" ));
                                       } else {
                                          $vl = 0;
                                          $AsOf = date("Y-m",time())."-01";
                                          $start_date = date("Y-m-d",strtotime( "$AsOf - 1 day" ));
                                       }
                                       ${"VLBal_".$EmployeesRefId} = $vl;
                                       $whereSL         = "WHERE CompanyRefId = ".$row['CompanyRefId']; 
                                       $whereSL         .= " AND BranchRefId = ".$row["BranchRefId"];
                                       $whereSL         .= " AND EmployeesRefId = ".$row['RefId']." AND NameCredits = 'SL'";
                                       $row_sl          = FindOrderBy("employeescreditbalance",$whereSL,"*","BegBalAsOfDate");  
                                       if ($row_sl) {
                                          $sl = $row_sl["BeginningBalance"];
                                       } else {
                                          $sl = 0;
                                       }
                                       ${"SLBal_".$EmployeesRefId} = $sl;

                                       ${"arr_leave_".$EmployeesRefId} = [
                                          "01"=>"",
                                          "02"=>"",
                                          "03"=>"",
                                          "04"=>"",
                                          "05"=>"",
                                          "06"=>"",
                                          "07"=>"",
                                          "08"=>"",
                                          "09"=>"",
                                          "10"=>"",
                                          "11"=>"",
                                          "12"=>"",
                                       ];
                                       $where = "where EmployeesRefId = ".$EmployeesRefId." AND Status = 'Approved'";
                                       $where .= " AND ApplicationDateFrom BETWEEN '".$year."-01-01"."' AND '".date("Y-m-d",time())."'";
                                       $where .= " ORDER BY ApplicationDateFrom";
                                       $rsLeave = SelectEach("employeesleave",$where);
                                       if ($rsLeave) {
                                          while ($row = mysqli_fetch_assoc($rsLeave)) {
                                             $dfrom   = date("d",strtotime($row["ApplicationDateFrom"]));
                                             $dto     = date("d",strtotime($row["ApplicationDateTo"]));
                                             $type    = getRecord("leaves",$row["LeavesRefId"],"Code");
                                             //echo $type;
                                             $month   = date("m",strtotime($row["ApplicationDateFrom"]));
                                             $date    = $row["ApplicationDateFrom"];
                                             $arr     = [$type,$row["ApplicationDateFrom"],$row["ApplicationDateTo"]];
                                             ${"arr_leave_".$EmployeesRefId}[intval($month)][$date] = $arr;
                                          }
                                       }
                                       $arr_month =[
                                         "January",
                                         "February",
                                         "March",
                                         "April",
                                         "May",
                                         "June",
                                         "July",
                                         "August",
                                         "September",
                                         "October",
                                         "November",
                                         "December"
                                       ];
                                       for ($a=1; $a <=12 ; $a++) { 
                                          if ($a <= 9) $a = "0".$a;
                                          ${"VL_count_".$a} = 0;
                                          ${"SL_count_".$a} = 0;
                                       }
                                       echo '
                                          <tr>
                                             <td colspan="6">Beginning Balance As Of '.date("F d Y",strtotime($AsOf)).'</td>
                                             <td class="text-center">'.$vl.'</td>
                                             <td colspan="3"></td>
                                             <td class="text-center">'.$sl.'</td>
                                             <td colspan="3"></td>
                                          </tr>
                                       ';
                                       $from          = date("m",strtotime($start_date));
                                       for ($i=intval($from)-1; $i <= date("m",time()) - 2 ; $i++) {
                                          $idx = $i+1;
                                          if ($idx <= 9) $idx = "0".$idx;
                                          $month      = $idx;
                                          $where_dtr  = "WHERE EmployeesRefId = $EmployeesRefId AND Month = '$month' AND Year = '$year'";
                                          $arr_empDTR = FindFirst("dtr_process",$where_dtr,"*");
                                          if ($arr_empDTR) {
                                             echo '
                                                <tr>
                                                   <td class="text-center">'.$arr_month[$i].'</td>';
                                             echo '<td>';
                                                   if (isset(${"arr_leave_".$EmployeesRefId}[intval($idx)])) {
                                                      $sl_type = "";
                                                      $vl_type = "";
                                                      foreach (${"arr_leave_".$EmployeesRefId}[intval($idx)] as $key => $value) {
                                                         $type = $value[0];
                                                         //echo $type;
                                                         $from = intval(date("d",strtotime($value[1])));
                                                         $to   = intval(date("d",strtotime($value[2])));
                                                         
                                                         if ($from == $to) {
                                                            //echo $from." (f ".$type.")<br> ";
                                                            
                                                            switch ($type) {
                                                               case 'VL':
                                                                  ${"VL_count_".$idx}++;
                                                                  $vl_type .= $from.","; 
                                                                  break;
                                                               case 'SL':
                                                                  ${"SL_count_".$idx}++;
                                                                  $sl_type .= $from.","; 
                                                                  break;
                                                            }
                                                         } else {
                                                            for ($x=$from; $x <= $to; $x++) { 
                                                               switch ($type) {
                                                                  case 'VL':
                                                                     ${"VL_count_".$idx}++;
                                                                     $vl_type .= $x.",";
                                                                     break;
                                                                  case 'SL':
                                                                     ${"SL_count_".$idx}++;
                                                                     $sl_type .= $x.","; 
                                                                     break;
                                                               }
                                                            }
                                                            //echo $count." (c ".$type.") ";
                                                         }
                                                      }
                                                      if ($vl_type != "") echo $vl_type."(VL)";
                                                      if ($sl_type != "") echo $sl_type."(SL)";
                                                   }
                                                   $sl_earned     = $arr_empDTR["SL_Earned"];
                                                   $sl_deduction  = ${"SL_count_".$idx};
                                                   $newSLBal = ${"SLBal_".$EmployeesRefId} + $sl_earned - $sl_deduction;
                                                   ${"SLBal_".$EmployeesRefId} = $newSLBal;
                                                   if ($newSLBal < 0) {
                                                      $sl_wop     = str_replace("-", "", $newSLBal);
                                                      $newSLBal   = 0;
                                                      $sl_balance = 0;
                                                      ${"SLBal_".$EmployeesRefId} = 0;
                                                   } else {
                                                      $sl_wop     = 0; 
                                                      $sl_balance = $newSLBal;
                                                   }
                                                   




                                                   $vl_earned     = $arr_empDTR["VL_Earned"];
                                                   $deduction     = $arr_empDTR["Tardy_Deduction_EQ"] + 
                                                                    $arr_empDTR["Undertime_Deduction_EQ"] + 
                                                                    $arr_empDTR["Total_Absent_EQ"];

                                                   $vl_deduction  = $deduction + ${"VL_count_".$idx} + $sl_wop;
                                                   $newVLBal      = ${"VLBal_".$EmployeesRefId} + $vl_earned - $vl_deduction;
                                                   $where_wop     = "WHERE NoOfDaysLeaveWOP <= '$vl_deduction'";
                                                   $get_vl_wop    = FindLast("leavecreditsearnedwopay",$where_wop,"LeaveCreditsEarned");
                                                   ${"VLBal_".$EmployeesRefId} = $newVLBal;
                                                   if ($newVLBal < 0) {
                                                      $vl_wop     = $get_vl_wop;
                                                      $newVLBal   = 0;
                                                      $vl_balance = 0;
                                                      ${"VLBal_".$EmployeesRefId} = 0;
                                                   } else {
                                                      $vl_wop     = 0; 
                                                      $vl_balance = $newVLBal;  
                                                   }
                                                   $TotalUT = ToHours($arr_empDTR["Total_Undertime_Hr"]);
                                                   $TotalTardy = ToHours($arr_empDTR["Total_Tardy_Hr"]);
                                                   $TotalDeduct = ToHours($arr_empDTR["Total_Undertime_Hr"] + $arr_empDTR["Total_Tardy_Hr"]);

                                                   if ($TotalDeduct == 0 || $TotalDeduct == "") {
                                                      $Hrs = "";
                                                      $Mins = "";
                                                   } else {
                                                      $arr_TotalDeduct = explode(":", $TotalDeduct);
                                                      $Hrs = $arr_TotalDeduct[0];
                                                      $Mins = $arr_TotalDeduct[1];
                                                      $Hrs  = getEquivalent(($Hrs * 60),"workinghrsconversion");
                                                      $Mins = getEquivalent($Mins,"workinghrsconversion");
                                                   }

                                             echo '</td>';      
                                             echo '<td class="text-center">'.$vl_earned.'</td>';
                                             echo '<td class="text-center">'.$arr_empDTR["Total_Absent_Count"].'</td>';
                                             echo '<td class="text-center">'.$Hrs.'</td>';
                                             echo '<td class="text-center">'.$Mins.'</td>';
                                             echo '<td class= "text-center">'.$vl_balance.'</td>';
                                             echo '<td class="text-center">'.$vl_wop.'</td>';      
                                             echo '<td class="text-center">'.$sl_earned.'</td>';      
                                             echo '<td class="text-center">'.$sl_deduction.'</td>';      
                                             echo '<td class="text-center">'.$sl_balance.'</td>';      
                                             echo '<td class="text-center">&nbsp;</td>';
                                             echo '<td class="text-center">&nbsp;</td>';
                                             echo '<td style="padding-left:5px;">';
                                                   $remAbsent = "";
                                                   $Absent_arr = explode("|", $arr_empDTR["Days_Absent"]);
                                                   foreach ($Absent_arr as $key => $value) {
                                                      if ($value != "") {
                                                         $remAbsent = $remAbsent.$value.", ";
                                                      }
                                                   }
                                                   if ($remAbsent != "") {
                                                      echo $remAbsent." No File";
                                                   }
                                                   if ($sl_wop != "") {
                                                      echo "( ".$sl_wop." charged to VL)";
                                                   }
                                             echo '</td>';      
                                             echo '
                                                </tr>
                                             ';
                                             $vl_earned_carried = $vl_earned_carried + $vl_earned;
                                             $sl_earned_carried = $sl_earned_carried + $sl_earned;
                                          }
                                       }
                                    ?>
                                    <tr>
                                       <td>&nbsp;</td>
                                       <td class="text-center">BALANCE CARRIED FORWARD</td>
                                       <td class="text-center"><?php echo $vl_earned_carried; ?></td>
                                       <td colspan="5"></td>
                                       <td class="text-center"><?php echo $sl_earned_carried; ?></td>
                                       <td colspan="5"></td>
                                    </tr>
                                 </tbody>   
                              </table>
                              <p>
                                 This is a system generated report. Signature is not required.
                              </p>
                        <?php 
                              }
                           }
                        }
                        ?>
                  </div>
               </div>
            </div>
            <?php
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>