<?php
   $file = "Authorization To Go On Field Work";
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>.
      
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("Field Work");
         ?>

         <table border="1">
            <tr>
               <td rowspan=2>Is Granted To:</td>
            </tr>
            <tr>
               <td colspan=3>Name:</td>
            </tr>
            <tr>
               <td colspan=3>Staff/Project:</td>
            </tr>
            <tr>
               <td colspan=4>Destination:</td>
            </tr>
            <tr>
               <td colspan=4>Purpose:</td>
            </tr>
            <tr>
               <td colspan=2>Inclusive Date(s):</td>
               <td colspan=2>
                  <div class="row">
                     <div class="col-xs-4">TIME:</div>
                     <div class="col-xs-4">From</div>
                     <div class="col-xs-4">To</div>
                  </div>
               </td>
            </tr>
            <tr style="height:3in;padding:20px">
               <td colspan=2 align="center" class="padd10">
                  Requested By:
                  <?php spacer(250); ?>
                  ___________________________________________<br>
                  Signature Over Printed Name
               </td>
               <td colspan=2 align="center" class="padd10">
                  Approved By:
                  <?php spacer(250); ?>
                  ___________________________________________<br>
                  Signature Over Printed Name
               </td>
            </tr>
            <tr>
               <td colspan=4 class="padd10">
                  <p>
                   Note : This form shall be accomplished in duplicate copies. The original copy be forwarded to the HRMO II for the records purposes while the duplicate shall be atteched to the request for reimbursement of transportation expenses.
                  </p>
               </td>
            </tr>

         </table>


      </div>
      <?php rptFooter(); ?>
   </body>
</html>