CREATE TABLE IF NOT EXISTS `spms_ipcr` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `DivisionRefId` bigint(50) DEFAULT NULL,
  `AgencyId` varchar(50) DEFAULT NULL,
  `Semester` int(1) DEFAULT NULL,
  `Year` int(4) DEFAULT NULL,
  `Supervisor_Rating` int(4) DEFAULT NULL,
  `Final_Rating` int(4) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `ipcr_details` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `spms_ipcr_id` bigint(50) DEFAULT NULL,
  `output` varchar(300) DEFAULT NULL,
  `success_indicator` varchar(300) DEFAULT NULL,
  `accomplishment` varchar(300) DEFAULT NULL,
  `q1` int(1) DEFAULT NULL,
  `e2` int(1) DEFAULT NULL,
  `t3` int(1) DEFAULT NULL,
  `a4` int(1) DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;