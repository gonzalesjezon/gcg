<?php
	include 'FnUpload.php';
	include '../conn.e2e.php';
	$file 				= "csv/empinfo_2.csv";
	$myFile 			= fopen($file, "r");
	while(!feof($myFile)) {
		$Flds = $Vals = "";
		$obj = fgets($myFile);
		if ($obj != "") {
			$obj_arr 				= explode(",", $obj);
			$empid 					= $obj_arr[0];
			$emprefid 				= FindFirst("employees","WHERE AgencyId ='$empid'","RefId",$conn);
			if ($emprefid > 0) {
				$empinfo_refid 			= FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","RefId",$conn);
				$PositionItemRefId 		= clean($obj_arr[1]);
				$PositionRefId 			= clean($obj_arr[2]);
				$DivisionRefId			= clean($obj_arr[3]);
				$OfficeRefId			= clean($obj_arr[4]);
				$InterimPositionRefId 	= clean($obj_arr[5]);
				$InterimDivisionRefId 	= clean($obj_arr[6]);
				$InterimOfficeRefId 	= clean($obj_arr[7]);
				$JobGradeRefId 			= clean($obj_arr[8]);
				$SalaryAmount 			= clean($obj_arr[9]);
				$ApptStatusRefId 		= clean($obj_arr[10]);
				$EmpStatusRefId 		= clean($obj_arr[11]);
				$date 					= clean($obj_arr[12]);
				$date_arr 				= explode("/", $date);
				$date  					= $date_arr[2]."-".$date_arr[1]."-".$date_arr[0];
				$HiredDate 				= $date;
				$AssumptionDate 		= $date;
				$Err = 0;

				if ($PositionRefId != "" ) {
					$PositionRefId = saveFM("position","`Name`, ","'$PositionRefId', ",$PositionRefId);
					$Flds .= "`PositionRefId`, ";
					$Vals .= "'$PositionRefId', ";
				}
				if ($DivisionRefId != "" ) {
					$DivisionRefId = saveFM("division","`Name`, ","'$DivisionRefId', ",$DivisionRefId);
					$Flds .= "`DivisionRefId`, ";
					$Vals .= "'$DivisionRefId', ";
				}
				if ($OfficeRefId != "" ) {
					$OfficeRefId = saveFM("office","`Name`, ","'$OfficeRefId', ",$OfficeRefId);
					$Flds .= "`OfficeRefId`, ";
					$Vals .= "'$OfficeRefId', ";
				}
				if ($InterimPositionRefId != "" ) {
					$InterimPositionRefId = saveFM("interimposition","`Name`, ","'$InterimPositionRefId', ",$InterimPositionRefId);
					$Flds .= "`InterimPositionRefId`, ";
					$Vals .= "'$InterimPositionRefId', ";
				}
				if ($InterimDivisionRefId != "" ) {
					$InterimDivisionRefId = saveFM("interimdivision","`Name`, ","'$InterimDivisionRefId', ",$InterimDivisionRefId);
					$Flds .= "`InterimDivisionRefId`, ";
					$Vals .= "'$InterimDivisionRefId', ";
				}
				if ($InterimOfficeRefId != "" ) {
					$InterimOfficeRefId = saveFM("interimoffice","`Name`, ","'$InterimOfficeRefId', ",$InterimOfficeRefId);
					$Flds .= "`InterimOfficeRefId`, ";
					$Vals .= "'$InterimOfficeRefId', ";
				}
				if ($JobGradeRefId != "" ) {
					$JobGradeRefId = saveFM("jobgrade","`Name`, ","'$JobGradeRefId', ",$JobGradeRefId);
					$Flds .= "`JobGradeRefId`, ";
					$Vals .= "'$JobGradeRefId', ";
				}
				if ($ApptStatusRefId != "" ) {
					$ApptStatusRefId = saveFM("apptstatus","`Name`, ","'$ApptStatusRefId', ",$ApptStatusRefId);
					$Flds .= "`ApptStatusRefId`, ";
					$Vals .= "'$ApptStatusRefId', ";
				}
				if ($EmpStatusRefId != "" ) {
					$EmpStatusRefId = saveFM("empstatus","`Name`, ","'$EmpStatusRefId', ",$EmpStatusRefId);
					$Flds .= "`EmpStatusRefId`, ";
					$Vals .= "'$EmpStatusRefId', ";
				}
				$PositionRefId = intval($PositionRefId);
				$DivisionRefId = intval($DivisionRefId);
				$OfficeRefId = intval($OfficeRefId);
				$InterimOfficeRefId = intval($InterimOfficeRefId);
				$InterimDivisionRefId = intval($InterimDivisionRefId);
				$InterimOfficeRefId = intval($InterimOfficeRefId);
				$JobGradeRefId = intval($JobGradeRefId);
				$ApptStatusRefId = intval($ApptStatusRefId);
				$EmpStatusRefId = intval($EmpStatusRefId);
				$SalaryAmount = intval($SalaryAmount);
				$plantilla_fld = "`Code`, `Name`, `PositionRefId`, `OfficeRefId`, `DivisionRefId`, `JobGradeRefId`, `StepIncrementRefId`, ";
				$plantilla_fld .= "`SalaryAmount`,";
				$plantilla_val = "'$PositionItemRefId','$PositionItemRefId', '$PositionRefId', '$OfficeRefId', '$DivisionRefId', '$JobGradeRefId', '1', '$SalaryAmount',";
				$plantilla_fldnval = "`Code` = '$PositionItemRefId',";
				$plantilla_fldnval .= "`Name` = '$PositionItemRefId',";
				$plantilla_fldnval .= "`PositionRefId` = '$PositionRefId', ";
				$plantilla_fldnval .= "`OfficeRefId` = '$OfficeRefId', ";
				$plantilla_fldnval .= "`DivisionRefId` = '$DivisionRefId', ";
				$plantilla_fldnval .= "`JobGradeRefId` = '$JobGradeRefId', ";
				$plantilla_fldnval .= "`StepIncrementRefId` = '1', ";
				$plantilla_fldnval .= "`SalaryAmount` = '$SalaryAmount', ";

				$check = FindFirst("positionitem","WHERE Name = '$PositionItemRefId'","RefId",$conn);
				if ($check > 0) {
					$PositionItemRefId = $check;
					$update = update("positionitem",$plantilla_fldnval,$PositionItemRefId);
					if ($update != "") {
						$Err++;
					}
				} else {
					$save_plantilla = save("positionitem",$plantilla_fld,$plantilla_val);
					if (is_numeric($save_plantilla)) {
						$PositionItemRefId = $save_plantilla;		
					} else {
						$Err++;
					}
				}
				if ($Err == 0) {
					$empinfo_fldnval = "`HiredDate` = '$date',";
					$empinfo_fldnval .= "`AssumptionDate` = '$date',";
					$empinfo_fldnval .= "`PositionRefId` = '$PositionRefId', ";
					$empinfo_fldnval .= "`OfficeRefId` = '$OfficeRefId', ";
					$empinfo_fldnval .= "`DivisionRefId` = '$DivisionRefId', ";
					$empinfo_fldnval .= "`InterimPositionRefId` = '$InterimPositionRefId', ";
					$empinfo_fldnval .= "`InterimOfficeRefId` = '$InterimOfficeRefId', ";
					$empinfo_fldnval .= "`InterimDivisionRefId` = '$InterimDivisionRefId', ";
					$empinfo_fldnval .= "`JobGradeRefId` = '$JobGradeRefId', ";
					$empinfo_fldnval .= "`ApptStatusRefId` = '$ApptStatusRefId', ";
					$empinfo_fldnval .= "`EmpStatusRefId` = '$EmpStatusRefId', ";
					$empinfo_fldnval .= "`PositionItemRefId` = '$PositionItemRefId', ";
					$empinfo_fldnval .= "`StepIncrementRefId` = '1', ";
					$empinfo_fldnval .= "`SalaryAmount` = '$SalaryAmount', ";
					$empinfo_update = update("empinformation",$empinfo_fldnval,$empinfo_refid);
					if ($empinfo_update == "") {
						echo "Successfully Saved Employee Info.<br>";
					} else {
						echo "Error in updating employee info.<br>";
					}	
				}
				
			}
		}	
	}
?>