<?php
	include '../conn.e2e.php';


	function SelectEach($table,$where) {
		include '../conn.e2e.php';
		$sql = "SELECT * FROM $table ".$where;
		$rs = mysqli_query($conn,$sql) or die(mysqli_error($conn));
		if ($rs) {
			return $rs;
		} else {
			echo "No Record Found";
		}
	}

	function save($table,$flds,$vals) {
		include '../conn.e2e.php';
		$table = strtolower($table);
		$date_today    	= date("Y-m-d",time());
        $curr_time     	= date("H:i:s",time());
        $trackingA_fld 	= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
        $trackingA_val 	= "'$date_today', '$curr_time', 'Admin', 'A'";
        $flds   	   	= $flds.$trackingA_fld;
        $vals   		= $vals.$trackingA_val;
        $sql 			= "INSERT INTO $table ($flds) VALUES ($vals)";
        $rs 			= mysqli_query($conn,$sql) or die(mysqli_error($conn));
        if ($rs) {
        	return mysqli_insert_id($conn);
        } else {
        	echo $rs -> $sql;
        }
        mysqli_close($conn);
	}
	function savePMS($table,$flds,$vals,$createdby = 0) {
        include '../conn.e2e.php';
        $newFld = $flds."`created_by`, `created_at`";
        $newVal = $vals."'$createdby', '".date("Y-m-d H:i",time())."'";
        $sql = "INSERT INTO $table ($newFld) VALUES ($newVal)";
        $result = mysqli_query($conn,$sql);
        if ($result) {
           return mysqli_insert_id($conn);
        } else {
           return false;
        }
        mysqli_close($conn);
     }
	function saveFM($table,$flds,$vals,$name,$where = "") {
		include '../conn.e2e.php';
		$table 			= strtolower($table);
		$where 			= "WHERE Name = '$name' ".$where;
		$check_FM 		= FindFirst($table,$where,"RefId",$conn);
		if (is_numeric($check_FM)) {
			return $check_FM;
		} else {
			$date_today    	= date("Y-m-d",time());
	        $curr_time     	= date("H:i:s",time());
	        $trackingA_fld 	= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
	        $trackingA_val 	= "'$date_today', '$curr_time', 'Admin', 'A'";
	        $flds   	   	= $flds.$trackingA_fld;
	        $vals   		= $vals.$trackingA_val;
	        $sql 			= "INSERT INTO $table ($flds) VALUES ($vals)";
	        $rs 			= mysqli_query($conn,$sql) or die(mysqli_error($conn));
	        if ($rs) {
	        	return mysqli_insert_id($conn);
	        } else {
	        	echo $rs."<br>$sql";
	        }	
		}
		mysqli_close($conn);
			
	}
	function update($table,$fldnval,$refid) {
		include '../conn.e2e.php';
		$table 				= strtolower($table);
		$date_today    		= date("Y-m-d",time());
        $curr_time     		= date("H:i:s",time());
        $trackingA_fldnval 	= "LastUpdateBy = 'Admin', LastUpdateTime = '$curr_time',";
        $trackingA_fldnval .= "LastUpdateDate = '$date_today', Data = 'E'";
        $fldnval 		   .= $fldnval.$trackingA_fldnval;
        $sql                = "UPDATE $table SET $fldnval WHERE RefId = $refid";
        $rs 				= mysqli_query($conn,$sql) or die(mysqli_error($conn));
        if ($rs) {
        	return "";
        } else {
        	return $sql;
        }
        mysqli_close($conn);
	}

	function FindFirst($table,$where,$fld,$conn) {
		$table = strtolower($table);
		if ($fld == "*") {
			$sql	= "SELECT * FROM $table ".$where." LIMIT 1";
		} else {
			$sql	= "SELECT `$fld` FROM $table ".$where." LIMIT 1";
		}
		$rs 		= mysqli_query($conn,$sql) or die(mysqli_error($conn));
		if ($rs) {
			if ($fld == "*") {
				$row = mysqli_fetch_assoc($rs);
				return $row;
			} else {
				$row = mysqli_fetch_assoc($rs);
				return $row[$fld];
			}
		} else {
			echo $sql;
		}
		mysqli_close($conn);
	}
  	function clean($value) {
  		include '../conn.e2e.php';
  		if ($value == "N/A") {
  			return "";
  		} else {
  			return mysqli_real_escape_string($conn,trim($value));	
  		}
  		mysqli_close($conn);
  	}
  	function validateDate($date, $format = 'Y-m-d'){
      	$d = DateTime::createFromFormat($format, $date);
      	return $d && $d->format($format) == $date;
   	}

?>