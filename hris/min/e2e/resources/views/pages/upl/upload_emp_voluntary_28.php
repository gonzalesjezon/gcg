<?php
	include 'FnUpload.php';
	mysqli_query($conn,"TRUNCATE employeesvoluntary");
	$EmpVoluntary = fopen("csv/35/voluntary.csv", "r");
	while(!feof($EmpVoluntary)) {
		$Fld = "CompanyRefId, BranchRefId, ";
		$Val = "35, 1, ";
		$voluntary_row 		= explode(",", fgets($EmpVoluntary));
		$EmpAgencyID 		= clean($voluntary_row[0]);
		$OrganizationRefId 	= clean($voluntary_row[1]);
		$StartDate			= clean($voluntary_row[2]);
		$EndDate			= clean($voluntary_row[3]);
		$NumofHrs			= intval(clean($voluntary_row[4]));
		$WorksNature		= clean($voluntary_row[5]);
		
		


		$emprefid = FindFirst("employees","WHERE AgencyId = '$EmpAgencyID'","RefId",$conn);
		if (is_numeric($emprefid)) {
			if ($OrganizationRefId != "") {
				$OrganizationRefId = saveFM("Organization","Name, ","'$OrganizationRefId', ",$OrganizationRefId);
				$Fld .= "OrganizationRefId , ";
				$Val .= "'$OrganizationRefId',";
			}
			if ($StartDate != "") {
				$Fld .= "StartDate, ";
				$Val .= "'$StartDate',";
			}

			if ($EndDate != "") {
				if (strtolower($EndDate) == "present") {
					$Fld .= "Present, ";
					$Val .= "'1',";
				} else {
					$Fld .= "EndDate, ";
					$Val .= "'$EndDate',";
				}
				
			}

			if ($NumofHrs != 0) {
				$Fld .= "NumofHrs, ";
				$Val .= "'$NumofHrs',";
			}
			$Fld .= "EmployeesRefId, WorksNature, ";
			$Val .= "$emprefid, '$WorksNature',";
			$save_voluntary = save("employeesvoluntary",$Fld,$Val);
			if (is_numeric($save_voluntary)) {
				echo "$emprefid Voluntary Saved.<br>";
			}
		}
	}
?>