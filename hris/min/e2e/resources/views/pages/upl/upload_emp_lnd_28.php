<?php
	include 'FnUpload.php';
	mysqli_query($conn,"TRUNCATE employeestraining");
	$EmpLND = fopen("csv/35/lnd.csv", "r");
	while(!feof($EmpLND)) {
		$lnd_row = explode(",", fgets($EmpLND));
		$Fld = "CompanyRefId, BranchRefId, ";
		$Val = "35, 1, ";
		$EmpAgencyID 		= clean($lnd_row[0]);
		$SeminarsRefId 		= clean($lnd_row[1]);
		$StartDate			= clean($lnd_row[2]);
		$EndDate			= clean($lnd_row[3]);
		$NumofHrs			= intval(clean($lnd_row[4]));
		$SeminarClassRefId  = clean($lnd_row[5]);
		$SponsorRefId		= clean($lnd_row[6]);
		
		


		$emprefid = FindFirst("employees","WHERE AgencyId = '$EmpAgencyID'","RefId",$conn);
		if (is_numeric($emprefid)) {
			if ($SeminarClassRefId != "") {
				$SeminarClassRefId = saveFM("seminarclass","Name, ","'$SeminarClassRefId', ",$SeminarClassRefId);
				$Fld .= "SeminarClassRefId , ";
				$Val .= "'$SeminarClassRefId',";
			}
			if ($SeminarsRefId != "") {
				$SeminarsRefId = saveFM("seminars","Name, ","'$SeminarsRefId', ",$SeminarsRefId);
				$Fld .= "SeminarsRefId , ";
				$Val .= "'$SeminarsRefId',";
			}
			if ($SponsorRefId != "") {
				$SponsorRefId = saveFM("sponsor","Name, ","'$SponsorRefId', ",$SponsorRefId);
				$Fld .= "SponsorRefId , ";
				$Val .= "'$SponsorRefId',";
			}
			if ($StartDate != "") {
				$Fld .= "StartDate, ";
				$Val .= "'$StartDate',";
			}

			if ($EndDate != "") {
				if (strtolower($EndDate) == "present") {
					$Fld .= "Present, ";
					$Val .= "'1',";
				} else {
					$Fld .= "EndDate, ";
					$Val .= "'$EndDate',";
				}
				
			}

			if ($NumofHrs != 0) {
				$Fld .= "NumofHrs, ";
				$Val .= "'$NumofHrs',";
			}
			$Fld .= "EmployeesRefId,";
			$Val .= "$emprefid,";
			$save_training = save("employeestraining",$Fld,$Val);
			if (is_numeric($save_training)) {
				echo "$emprefid Training Saved.<br>";
			}
		}
	}
?>