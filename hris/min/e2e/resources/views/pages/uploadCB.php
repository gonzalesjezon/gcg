<?php
include 'conn.e2e.php';
include 'constant.e2e.php';
include pathClass.'0620functions.e2e.php';
include_once $files["inc"]["pageHEAD"];

$file = fopen(textFile."PCC_BegBal.csv","r");
$file_new = fopen(textFile."noVLSL.txt","a+");


$date_today    = date("Y-m-d",time());
$curr_time     = date("H:i:s",time());
$trackingflds = "`LastUpdateBy`, `LastUpdateDate`, `LastUpdateTime`, `Data`";
$trackingvals = "'SYSTEM', '$date_today', '$curr_time', 'A'";
$j = 0;
//TRUNCATE FIRST THE CREDIT BALANCE TABLE
mysqli_query($conn,"TRUNCATE employeescreditbalance");

while(! feof($file)) {
	$j++;
  	$filex = fgets($file);
  	$file_arr = explode(",", $filex);
  	$agencyID = $file_arr[0];
  	$vl = trim($file_arr[1]);
  	$sl = trim($file_arr[2]);

  	if ($vl == "") $vl = "0.000";
  	if ($sl == "") $sl = "0.000";
  	
  	$sql = "SELECT * FROM `employees` WHERE AgencyId = '$agencyID' LIMIT 1";
  	$rs = mysqli_query($conn,$sql);
  	if (mysqli_num_rows($rs) > 0) {
  		$row = mysqli_fetch_assoc($rs);
  		$emprefid = $row["RefId"];
  		$companyrefid = $row["CompanyRefId"];
  		$branchrefid = $row["BranchRefId"];
  		$exist = true;
  	} else {
  		$exist = false;
  	}
  	if ($exist) {
  		$vl_flds = "`CompanyRefId`, `BranchRefId`, `EmployeesRefId`, `NameCredits`,";
	  	$vl_flds .= "`EffectivityYear`, `BegBalAsOfDate`, `BeginningBalance`, ";
	  	$vl_flds = $vl_flds.$trackingflds;
	  	$vl_vals = "'$companyrefid', '$branchrefid', '$emprefid', 'VL', '2018', '2018-01-01', '$vl', ";
	  	$vl_vals = $vl_vals.$trackingvals;
	  	$vl_sql = "INSERT INTO `employeescreditbalance` ($vl_flds) VALUES ($vl_vals)";
	  	//UNCOMMENT THE QUERY TO SAVE AND COMMENT THE LINE THAT NEXT TO IT
	  	//echo "<li> -- ".$vl_sql;
	  	$vl_rs = mysqli_query($conn,$vl_sql);
	  	//$vl_rs = true;
	  	if ($vl_rs) {
	  		$sl_flds = "`CompanyRefId`, `BranchRefId`, `EmployeesRefId`, `NameCredits`,";
		  	$sl_flds .= "`EffectivityYear`, `BegBalAsOfDate`, `BeginningBalance`, ";
		  	$sl_flds = $sl_flds.$trackingflds;
		  	$sl_vals = "'$companyrefid', '$branchrefid', '$emprefid', 'SL', '2018', '2018-01-01', '$sl', ";
		  	$sl_vals = $sl_vals.$trackingvals;

		  	$sl_sql = "INSERT INTO `employeescreditbalance` ($sl_flds) VALUES ($sl_vals)";
		  	//UNCOMMENT THE QUERY TO SAVE AND COMMENT THE LINE THAT NEXT TO IT
		  	$sl_rs = mysqli_query($conn,$sl_sql);
		  	//$sl_rs = true;
		  	if (!$sl_rs) {
		  		echo "Error in Saving in employeescreditbalance SL ".$sl_sql;
		  	} 
	  	} else {
	  		echo "Error in Saving in employeescreditbalance VL ".$vl_sql;
	  	}
	} else {
  		echo "<li>NO EMPLOYEES RECORD ".$agencyID;
  	}
}
fclose($file);
?> 