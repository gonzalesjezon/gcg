<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo $path."/css/sideBar.css"; ?>">
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_SysUserAccess"); ?>"></script>
      <script language="JavaScript">
         $(document).ready(function () {

         });
         function openNav_adm() {
             document.getElementById("mySidenav").style.width = "250px";
         }

         function closeNav_adm() {
             document.getElementById("mySidenav").style.width = "0";
         }
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <nav class="navbar navbar-fixed-top">
            <div class="sysNameHolder sysBG" style="border-bottom:3px solid #fff;">
               <?php
                  $TRNBTN = 0;
                  $title = "";
                  $Logout = true;
                  include $files["inc"]["hdr"];
               ?>
               <span class="sysName">Human Resources And Information System</span>
            </div>
         </nav>
         <div style="margin-top:60px;">
            <?php doSideBarMain(); ?>
            <div class="container-fluid" id="mainScreen">
               <?php doTitleBar("System User Access"); ?>
               <?php spacer(5) ?>
               <div class="row">
                  <div class="col-sm-3">
                     <div class="mypanel">
                        <div class="panel-top">SYSTEM USER</div>
                        <div class="panel-mid-litebg" style="max-height:600px;overflow:auto;">
                           <div class="list-group">
                                 <?php
                                    $rs = f_Find("sysuser","order by UserName");
                                    while ($row = mysqli_fetch_assoc($rs)) {

                                          if ($row["UserName"]!= "") {
                                             echo
                                             '<a href="#" onclick="selectSysUser(\''.$row["UserName"].'\','.$row["RefId"].');" id="'.$row["UserName"].'" class="list-group-item">
                                                ['.$row["RefId"].'] - '.$row["UserName"].'
                                             </a>';
                                          }
                                    }
                                 ?>
                           </div>
                        </div>
                        <div class="panel-bottom"></div>
                     </div>
                  </div>
                  <div class="col-sm-9">
                     <div class="mypanel">
                        <div class="panel-top">
                           SYSTEM(S) ACCESED OF THE USER
                           <p id="idSysUserName"></p>
                        </div>
                        <div class="panel-mid" id="divSysAccess">
                        </div>
                        <div class="panel-bottom">
                           <?php createButton("Update","btnSysAccessUpdate","btn-cls4-sea","fa-floppy-o","disabled"); ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("fn","UpdateSysAccess","");
               doHidden("hSelectedSysUSer","","");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



