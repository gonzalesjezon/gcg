<div class="row">
   <div class="col-xs-12">
      <div class="row margin-top">
         <div class="col-xs-3">
            <label class="control-label" for="inputs">GROUP LEAVE POLICY:</label>
            <?php createSelect("leavepolicygroup","drpLeavePolicyGroupRefId","",100,"Name","",""); ?>
         </div>
      </div>
      <br>
      <div id="spGridTable_lp">
         <?php
            $table = "leavepolicy";
            $sql = "SELECT * FROM `$table` ORDER BY RefId Desc LIMIT 100";
            doGridTable($table,
                        ["Name","Leave","Accumulating","Value"],
                        ["LeavePolicyGroupRefId","LeavesRefId","Accumulating","Value"],
                        $sql,
                        [true,true,true,false],
                        "gridTable_leavepolicy");
         ?>
      </div>
      <button type="button" class="btn-cls4-sea"
           id="btnINSERTLP" name="btnINSERTLP">
         <i class="fa fa-file" aria-hidden="true"></i>
         &nbsp;Insert New
      </button>
   </div>
</div>
<!-- Modal -->
<div class="modal fade modalFieldEntry--" id="modalFieldEntry_LeavePolicy" role="dialog">
   <div class="modal-dialog" style="width:75%;">
      <div class="mypanel" style="height:100%;">
         <div class="panel-top bgSea">
            <span id="modalTitle_LeavePolicy" style="font-size:11pt;">Inserting New Leave Policy</span>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="panel-mid">
            <div class="row" id="EntryScrn_LeavePolicy">
               <div class="col-xs-12">
                  <div class="row margin-top">
                     <div class="col-xs-3 label">
                        <label class="control-label" for="inputs">GROUP LEAVE POLICY:</label>
                     </div>
                     <div class="col-xs-4">
                        <?php createSelect("leavepolicygroup","sint_LeavePolicyGroupRefId","",100,"Name","",""); ?>

                     </div>
                  </div>
                  <!--
                  <div class="row margin-top">
                     <div class="col-xs-3 label">
                        <label class="control-label" for="inputs">CODE:</label>
                     </div>
                     <div class="col-xs-4">
                        <input class="form-input saveFields--" type="text" id="LPCode" name="char_LPCode">
                     </div>
                  </div>

                  <div class="row margin-top">
                     <div class="col-xs-3 label">
                        <label class="control-label" for="inputs">LEAVE POLICY NAME:</label>
                     </div>
                     <div class="col-xs-4">
                        <input class="form-input saveFields-- mandatory--" type="text" id="LPName" name="char_LPName">
                     </div>
                  </div>
                  -->
                  <?php spacer(20); ?>
                  <div class="row">
                     <div class="col-xs-12">
                        <div class="row margin-top">
                           <div class="col-xs-1"></div>
                           <!--
                           <div class="col-xs-3">
                              <input type="radio" name="radAccumulating" id="Accumulating" class="saveFields--">&nbsp;&nbsp;<label for="Accumulating">ACCUMULATING</label>
                           </div>
                           <div class="col-xs-3">
                              <input type="radio" name="radAccumulating" id="ResetYearly" class="saveFields--">&nbsp;&nbsp;<label for="ResetYearly">RESET YEARLY</label>
                           </div>
                           <input type="hidden" class="saveFields--" name="sint_Accumulating" value="1">
                           -->
                           <div class="col-xs-3"></div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-2 label">
                              <label class="control-label" for="inputs">LEAVE:</label>
                           </div>
                           <div class="col-xs-4">
                              <?php createSelect("leaves","sint_LeavesRefId","",100,"Name","Select Leave",""); ?>
                           </div>
                           <div class="col-xs-4">
                              <select class="form-input saveFields--" name="sint_Accumulating">
                                 <option value="1">Accumulating</option>
                                 <option value="0">Reset Yearly</option>
                              </select>
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-1"></div>
                           <div class="col-xs-4">
                              <div class="row margin-top">
                                 <input type="checkbox" id="OffsetLate" name="sint_OffsetLate" class="saveFields--" value=0>&nbsp;&nbsp;
                                 <label for="OffsetLate">Offset late</label>
                              </div>
                              <div class="row margin-top">
                                 <input type="checkbox" id="OffsetUndertime" name="sint_OffsetUT" class="saveFields--" value=0>&nbsp;&nbsp;
                                 <label for="OffsetUndertime">Offset Undertime</label>
                              </div>
                              <div class="row margin-top">
                                 <input type="checkbox" id="OffsetNoFile" name="sint_OffsetIfNoFile" class="saveFields--" value=0>&nbsp;&nbsp;
                                 <label for="OffsetNoFile">Offset if No File</label>
                              </div>
                              <div class="row margin-top">
                                 <input type="checkbox" id="ForceLeave" name="sint_ForceLeaveApplied" class="saveFields--" value=0>&nbsp;&nbsp;
                                 <label for="ForceLeave">Force Leave Applied</label>
                              </div>
                              <div class="row margin-top">
                                 <input type="checkbox" id="AffectedAbsences" name="sint_AffectedByAbs" class="saveFields--" value=0>&nbsp;&nbsp;
                                 <label for="AffectedAbsences">Affected By Absences</label>
                              </div>
                           </div>
                           <div class="col-xs-4">
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <label class="control-label" for="inputs">VALUE:</label>
                                 </div>
                                 <div class="col-xs-6">
                                    <input class="form-input number-- saveFields--" type="text" id="" name="deci_Value" maxlength="4">
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <label class="control-label saveFields--" for="inputs">MAX FORCE LEAVE:</label>
                                 </div>
                                 <div class="col-xs-6">
                                    <input class="form-input saveFields--" type="text" id="char_MaxForceLeave" name="char_MaxForceLeave">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="panel-bottom">
            <div class="row">
               <div class="col-xs-12 txt-center">
                  <?php createButton("Save","btnLocSave","btn-cls4-sea","fa-floppy-o",""); ?>
                  <?php createButton("Cancel","btnLocCancel","btn-cls4-red","fa-undo",""); ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>