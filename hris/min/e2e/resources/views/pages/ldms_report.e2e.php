<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <style type="text/css">
         .rpt_row:hover {
            padding-left: 10px;
            transition: 0.5s;
            font-weight: 500;
         }
         .rpt_row {
            padding: 10px;
            margin-left: 20px;
         }
      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("LDMS Reports"); ?>
            <div class="container-fluid margin-top">
               <div class="mypanel">
                  <div class="row">
                     <div class="col-xs-8">
                        <div class="panel-top">Reports</div>
                        <div class="panel-mid-litebg" style="padding: 10px;">
                           <div class="row">
                              <div class="col-xs-6">
                                 <label>Select Report</label>
                                 <select class="form-input" id="report_type" name="report_type">
                                    <option value="">Select Report</option>
                                    <option value="rpt_competency_assessment">
                                       Summary of Competency Assessment
                                    </option>
                                    <option value="rpt_return_service">
                                       Status of Return Service Agreement
                                    </option>
                                    <option value="rpt_lnd_intervention">
                                       Summary of L&D Interventions
                                    </option>
                                    <option value="rpt_training_effectiveness">
                                       Summary of Training Effectiveness Rating
                                    </option>
                                 </select>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <br>
                                 <button type="button" class="btn-cls4-tree" id="generate">GENERATE REPORT</button>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal fade border0" id="modalLDMS" role="dialog">
               <div class="modal-dialog border0" style="padding:0px;width:90%;height:92%;">
                  <div class="mypanel border0" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                              <i class="fa fa-print" aria-hidden="true"></i>
                           </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script language="JavaScript">
      $(document).ready(function () {
         $("#generate").click(function () {
            var file       = $("#report_type").val();
            var url        = "ReportCaller.e2e.php?file=" + file;   
            url            += "&" + $("[name='hgParam']").val();
            $("#rptContent").attr("src",url);
            $("#modalLDMS").modal();
         });
      });
   </script>
</html>



