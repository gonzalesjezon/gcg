<?php
   session_start();
   require "conn.e2e.php";
   require 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   $fContent      = file_get_contents(json."file.json");
   $files         = json_decode($fContent, true);

   $CompanyId     = 0;
   $BranchId      = 0;
   $CompanyId     = getvalue("hCompanyID");
   $BranchId      = getvalue("hBranchID");
   $CompanyCode   = getvalue("ccode");
   $Owner         = getvalue("hOwner");
   $file_name     = getvalue("file");
   if (isset($_GET["diff_report"])) {
      $diff_report = 1;
   } else {
      $diff_report = 0;
   }
   if ($diff_report == 1) {
      $ob_refid = FindFirst("employeesauthority","WHERE RefId = '".getvalue("refid")."'","AbsencesRefId");
      $ob_code = getRecord("absences",$ob_refid,"Code");
   } else {
      $ob_code = "";
   }
   if (getvalue("ext") == "") {
      $ext = "php";
   } else {
      $ext = getvalue("ext");
   }
   if ($CompanyId == "35") {
      if ($ob_code != "") {
         switch ($ob_code) {
            case 'TO':
               $file_name = "rpt_TO_35";
               break;
            case 'OO':
               $file_name = "rpt_OO_35";
               break;
            case 'PT':
               $file_name = "rpt_PT_35";
               break;
            default:
               $file_name = "rpt_OB_2";
               break;
         }
      }
   }
   if ($CompanyId == "14") {
      if ($ob_code != "") {
         switch ($ob_code) {
            case 'OB':
               $file_name = "rpt_OB_14";
               break;
            default:
               $file_name = "rpt_OB_2";
               break;
         }
      }   
   }
   $agencyCode = FindFirst("company","","Code");
   $rptFile    = "./rpt/".$file_name;

   if (file_exists($rptFile)) {
      if ($Owner == "E2E" || $Owner == "") {
         $rptFile = "./rpt/".$file_name;
         if (!file_exists($rptFile.".e2e.".$ext)) {
            echo "No Report Yet.";
         } else {
            require $rptFile.".e2e.".$ext;   
         }
      } else {
         $rptFile = "./rpt/$CompanyId/".$file_name;
         if (!file_exists($rptFile.".e2e.".$ext)) {
            echo "No Report Yet.";
         } else {
            require $rptFile.".e2e.".$ext;   
         }
      }   
   } else {
      if (!file_exists("rpt/".$file_name.".e2e.".$ext)) {
         echo "No Report Yet.";
      } else {
         require "rpt/".$file_name.".e2e.".$ext;
      }
      
   }
   

   $conn->close();
?>