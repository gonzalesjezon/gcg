$(document).ready(function(){
   remIconDL();
   $("#transfer").hide();
   var selected = $("#SelectedEMP").val();
   if (selected != "" && selected != undefined) {
      local_tr_Click(selected);      
   } else {
      $(".alert, #divView, #DataEntry, #EntryEmpInformation").hide();
      $("#btnSAVE").prop("disabled",true);   
   }
   $("#btnSAVE").click(function(){
      var emprefid = $("#hEmpRefid").val();
      var inactive = 0;
      if (emprefid > 0) {
         $proceed = 0;
         /*Validation Before Save*/
         if ($("[name='sint_EmpStatusRefId']").find(":selected").text() == "PERMANENT") {
            if (getValueByName("date_StartDate") != "" || 
                getValueByName("date_EndDate") != "") {
               $("[name='date_StartDate']").notify("Not Allowed with Date Value when Emp. Status is PERMANENT !!!");
               $("[name='date_EndDate']").notify("Not Allowed with Date Value when Emp. Status is PERMANENT !!!");
               if (getValueByName("date_StartDate") != "") { 
                  $("[name='date_StartDate']").focus(); 
               }
               if (getValueByName("date_EndDate") != "") { 
                  $("[name='date_EndDate']").focus(); 
               }
               $proceed++;
               return false;
            } 
         }
         if (saveProceed() == 0 && $proceed == 0) {
            fldnval_add = getFieldEntry("EntryEmpInformation", "ADD");
            if ($("#hRefId").val() == 0) {
               fldnval_add += "hidden|sint_EmployeesRefId|" + $("#hEmpRefid").val() + "!";  // Add Purposes;
            }
            if (confirm("Change the Employment Information?")) {
               if ($("[name='date_ResignedDate']").val() != "") {
                  inactive = 1;
                  $("[name='sint_Inactive']").val(inactive);
               } else {
                  inactive = $("[name='sint_Inactive']").val();
               }
               updateEmpActiveness(emprefid, inactive);
               gSaveRecord(fldnval_add,"empinformation");
               
            }
         }
      } else {
         alert("Oooops!!! No Emp. Ref ID.");
      }
   });

   $("#btnPRINT").click(function(){
      $("#prnModal").modal();
      $("#rptContent").attr("src","ReportCaller.e2e.php?file=rptEmploymentInfo");
   });
   $("#btnUpdate").click(function () {
      var emprefid = $("#hEmpRefid").val();
      if (confirm("Change the Employment Information?")) {
         if ($("[name='date_ResignedDate']").val() != "") {
            inactive = 1;
            $("[name='sint_Inactive']").val(inactive);
         } else {
            inactive = $("[name='sint_Inactive']").val();
         }
         updateEmpActiveness(emprefid, inactive);
         updateEmpInfo();
         
      }
      
   });
   $("[name='sint_SalaryGradeRefId']").change(function () {
      var sg = $(this).val();
      var jg = $("[name='sint_JobGradeRefId']").val(); 
      var si = $("[name='sint_StepIncrementRefId']").val(); 
      getSalary(sg,jg,si);
   });
   $("[name='sint_StepIncrementRefId']").change(function () {
      var si = $(this).val();
      var jg = $("[name='sint_JobGradeRefId']").val(); 
      var sg = $("[name='sint_SalaryGradeRefId']").val(); 
      getSalary(sg,jg,si);
   });
   $("[name='sint_JobGradeRefId']").change(function () {
      var jg = $(this).val();
      var sg = $("[name='sint_SalaryGradeRefId']").val(); 
      var si = $("[name='sint_StepIncrementRefId']").val(); 
      getSalary(sg,jg,si);
   });
   $("[name='sint_Inactive']").change(function () {
      var val = $(this).val();
      if (val > 0) {
         $("#transfer").show();
      } else {
         $("#transfer").hide();
      }
   });
   
});

function updateEmpInfo() {
   $.ajax({
      url: "trn.e2e.php",
      type: "POST",
      data: new FormData($("[name='xForm']")[0]),
      success : function(responseTxt){
         responseTxt = responseTxt.trim();
         eval(responseTxt);
      },
      enctype: 'multipart/form-data',
      processData: false,
      contentType: false,
      cache: false
   });
}
function afterNewSave(newRefId) {
   alert("New Record Inserted\nRef. ID: " + newRefId);
   //gotoscrn($("#hProg").val(),"");
}

function afterEditSave(refId) {
   alert("Record " + refId + " Succcessfully Updated");
   //gotoscrn($("#hProg").val(),"");
}

function local_tr_Click(empRefId,mode) {
   $("#hEmpRefid").val(empRefId); // hidden
   $("#EmpRefId").html(empRefId); // span
   $("#EntryEmpInformation, #DataEntry").show();
   $("#btnSAVE").prop("disabled",false);
   $.post("ctrl_EmploymentInformation.e2e.php",
   {
      t:"viewRecord",
      emprefid:empRefId
   },
   function(data,status) {
      var EmpInformation = 0;
      if (data == false) {
         $("#spanEmpName").html(empRefId + " : NO Employment Information!!!");
         $("#hRefId").val(0);
         $("#hmode").val("ADD");

         $("[name='date_HiredDate']").val("");
         $("[name='date_AssumptionDate']").val("");
         $("[name='date_ResignedDate']").val("");
         $("[name='date_RehiredDate']").val("");
         $("[name='date_StartDate']").val("");
         $("[name='date_EndDate']").val("");
         $("[name='sint_AgencyRefId']").val(0);
         $("[name='sint_PositionItemRefId']").val(0);
         $("[name='sint_PositionRefId']").val(0);
         $("[name='sint_OfficeRefId']").val(0);
         $("[name='sint_DepartmentRefId']").val(0);
         $("[name='sint_DivisionRefId']").val(0);
         $("[name='sint_EmpStatusRefId']").val(0);
         $("[name='sint_SalaryGradeRefId']").val(0);
         $("[name='sint_JobGradeRefId']").val(0);
         $("[name='sint_StepIncrementRefId']").val(0);
         $("[name='bint_WorkScheduleRefId']").val(0);
         $("[name='sint_PayrateRefId']").val(0);
         $("[name='sint_ProjectRefId']").val(0);
         $("[name='deci_SalaryAmount']").val(0);
         $("[name='deci_SalaryAmountTwo']").val(0);
      }
      else {
         try
         {
            //$("#dbg").html(data);
            data = JSON.parse(data);
            if (parseInt(data.RefId) > 0) {
               //alert(data.LastName + ", " + data.FirstName);
               $("#spanEmpName").html(data.LastName + ", " + data.FirstName);
               $("#hmode").val("EDIT");
               $("#hRefId").val(data.RefId);
               $("[name='date_HiredDate']").val(data.HiredDate);
               $("[name='date_AssumptionDate']").val(data.AssumptionDate);
               $("[name='date_ResignedDate']").val(data.ResignedDate);
               $("[name='date_RehiredDate']").val(data.RehiredDate);
               $("[name='date_StartDate']").val(data.StartDate);
               $("[name='date_EndDate']").val(data.EndDate);
               $("[name='sint_AgencyRefId']").val(data.AgencyRefId);
               $("[name='sint_PositionItemRefId']").val(data.PositionItemRefId);
               $("[name='sint_DepartmentRefId']").val(data.DepartmentRefId);
               $("[name='sint_PositionRefId']").val(data.PositionRefId);
               $("[name='sint_OfficeRefId']").val(data.OfficeRefId);
               $("[name='sint_DivisionRefId']").val(data.DivisionRefId);
               $("[name='InterimPositionRefId']").val(data.InterimPositionRefId);
               $("[name='InterimOfficeRefId']").val(data.InterimOfficeRefId);
               $("[name='InterimDivisionRefId']").val(data.InterimDivisionRefId);
               $("[name='sint_EmpStatusRefId']").val(data.EmpStatusRefId);
               $("[name='sint_SalaryGradeRefId']").val(data.SalaryGradeRefId);
               $("[name='sint_JobGradeRefId']").val(data.JobGradeRefId);
               $("[name='sint_StepIncrementRefId']").val(data.StepIncrementRefId);
               $("[name='sint_PayrateRefId']").val(data.PayrateRefId);
               $("[name='sint_ApptStatusRefId']").val(data.ApptStatusRefId);
               $("[name='sint_LocationsRefId']").val(data.LocationsRefId);
               $("[name='sint_SectionsRefId']").val(data.SectionsRefId);
               $("[name='sint_UnitsRefId']").val(data.UnitsRefId);
               $("[name='sint_ProjectRefId']").val(data.ProjectRefId);
               $("[name='sint_ResponsibilityCenterRefId']").val(data.ResponsibilityCenterRefId);
               $("[name='deci_SalaryAmount']").val(data.SalaryAmount);
               if (data.Inactive == 1) {
                  $("[name='sint_Inactive']").val("1");   
               } else {
                  $("[name='sint_Inactive']").val("0");
               }
               
               $("[name='char_TransferTo']").val(data.TransferTo);
               if ($("[name='date_ResignedDate']").val() != "" && 
                   $("[name='sint_Inactive']").val() == 0) {
                  $("#message").html('<span style="color:red">Resigned/Retired Date have value in this Employee but not in Inactive Status!!!</span'); 
               } else {
                  $("#message").html(''); 
               }
               if (data.Inactive > 0) {
                  $("#transfer").show();
               } else {
                  $("#transfer").hide();
               }
            } else {
               alert("Ooops!!! No RefId");
            }
         } catch (e) {
             if (e instanceof SyntaxError) {
                 alert(e.message);
             }
         }
      }
   });
}

function selectMe(emprefid) {
   local_tr_Click(emprefid);
}

function updateEmpActiveness(emprefid,inactive) {
   $.post("trn.e2e.php",
   {
      fn:"updateEmpActiveness",
      val: inactive,
      user: $("#hUser").val(),
      emprefid:emprefid
   },
   function(data,status){
      if (status == 'success') {
         if (data.trim() != "") {
            alert(data);
         }
      } else {
         alert("Error :" + data);
      }
   });
}




