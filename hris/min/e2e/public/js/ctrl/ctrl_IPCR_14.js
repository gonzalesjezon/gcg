$(document).ready (function () {
   var selected_emp = $("#SelectedEMP").val();
   if (selected_emp != "") {
      selectMe(selected_emp);
   } else {
      if (getValueByName("hUserGroup") == "COMPEMP") {
         selectMe($("#hEmpRefId").val());
      }
   }
   $("#insert_spms, #btnEDITIPCR, #btnBACKIPCR").hide();
   $("#btnINSERTSPMS").click(function () {
      var emprefid = $("#sint_EmployeesRefId").val();
      if (emprefid == "" || emprefid == 0){
         $.notify("No Employee Selected.");
      } else {
         $("#spmsList").hide();
         $("#insert_spms").show();
      }
   });
   $("#btnCANCELIPCR, #btnBACKIPCR").click(function () {
      var prog = $("#hProg").val();
      gotoscrn(prog,"");
   });
   
   $("#btnPRINT").click (function () {
      var rptFile = "prnIPCR";
      var url = "ReportCaller.e2e.php?file=" + rptFile + "&";
      $("#prnModal").modal();
      $("#rptContent").attr("src",url);
   });
   $("#deleterow").click(function () {
      var last = "";
      $("[id*='row_']").each(function () {
         last = $(this).attr("id").split("_")[1];
      });
      if (last > 1) {
         $("#row_" + last).remove();   
      }
   });

   $("#addrow").click(function () {
      var last = "";
      var str = "";
      $("[id*='row_']").each(function () {
         last = $(this).attr("id").split("_")[1];
      });
      var idx = parseInt(last) + 1;
      str += '<tr id="row_' + idx + '">';
      str += '<td class="td-input">';
      str += '<input type="hidden" name="refid_'+ idx +'" id="refid_'+ idx +'" value="0">';
      str += '<input type="text" class="form-input" name="output_'+ idx +'" id="output_'+ idx +'">';
      str += '</td>';
      str += '<td class="td-input">';
      str += '<input type="text" class="form-input" name="si_'+ idx +'" id="si_'+ idx +'">';
      str += '</td>';
      str += '<td class="td-input">';
      str += '<input type="text" class="form-input" name="accomplishment_'+ idx +'" id="accomplishment_'+ idx +'">';
      str += '</td>';
      str += '<td class="td-input">';
      str += '<input type="text" class="form-input" name="q1_'+ idx +'" id="q1_'+ idx +'">';
      str += '</td>';
      str += '<td align="center">';
      str += '<input type="text" class="form-input" name="e2_'+ idx +'" id="e2_'+ idx +'">';
      str += '</td>';
      str += '<td align="center">';
      str += '<input type="text" class="form-input" name="t3_'+ idx +'" id="t3_'+ idx +'">';
      str += '</td>';
      str += '<td align="center">';
      str += '<input type="text" class="form-input" name="a4_'+ idx +'" id="a4_'+ idx +'">';
      str += '</td>';
      str += '<td class="td-input">';
      str += '<input type="text" class="form-input" name="remarks_'+ idx +'" id="remarks_'+ idx +'">';
      str += '</td>';
      str += '</tr>';
      $("#row_" + last +":last").after(str);
   });
   $("#btnSAVEIPCR, #btnEDITIPCR").click(function () {
      $(this).prop("disabled",true);
      $("#btnBACKIPCR, #btnCANCELIPCR").prop("disabled",true);
      $(this).html("Processing..");
   	var emprefid = $("#sint_EmployeesRefId").val();
   	if ($("#sint_Semester").val() == "") {
   		$.notify("Semester is Required");
   		return false;
   	}
   	if (emprefid == 0) {
   		$.notify("No Employee Selected");
   	} else {
         var count    = 0;
	      $("[id*='EntryIPCR_']").each(function () {
	         count++;
	      });
	      $("#count").val(count);

	      $.ajax({
		      url: "ajax_14.e2e.php",
		      type: "POST",
		      data: new FormData($("[name='xForm']")[0]),
		      success: function (responseTxt) {
		         eval(responseTxt);
		      },
		      enctype: 'multipart/form-data',
		      processData: false,
		      contentType: false,
		      cache: false
		   });	
   	}
   });
});
function afterSave(){
	alert("Successfully Save");
	gotoscrn ($("#hProg").val(), setAddURL());
}
function afterEdit() {
   alert("Successfully Update");
   gotoscrn ($("#hProg").val(), setAddURL());   
}
function viewInfo(refid,mode,dum){
   if (mode == 2) {
      getIPCR(refid);   
   } else if (mode == 3) {
      printIPCR(refid);
   }
   
}
function selectedItems(emprefid,lname,fname) {
   $(".list-group-item").removeClass("active");
   $("#" + emprefid).addClass("active");
   $("[name='sint_EmployeesRefId']").val(emprefid);
   $.get("changeSessionValue.e2e.php",
   {
      hGridTblHdr:"Division|Semester|Year",
      hGridTblFld:"DivisionRefId|Semester|Year",
      hGridTblId:"gridTable",
      hGridDBTable:"spms_ipcr",
      sql:"SELECT * FROM `spms_ipcr` WHERE `EmployeesRefId` = " + emprefid + " ORDER BY RefId Desc LIMIT 100",
      listAction:[true,true,true,""],
      empselected:emprefid
   },
   function(data,status){
      if (status=='success') {
         refreshTable(emprefid);
         tr_Click(emprefid);
      }
   });
}
function getIPCR(refid){
   $("#sint_IPCRRefId").val(refid);
   $.get("ajax_14.e2e.php",
   {
      fn:"getIPCR_detail",
      refid:refid
   },
   function(data,status){
      if (status=='success') {
         $("#canvas").html("");
         $("#canvas").html(data);
         $("[id*='delete_']").prop("disabled",true);
         $("#spmsList, #btnSAVEIPCR, #btnCANCELIPCR").hide();
         $("#insert_spms, #btnEDITIPCR, #btnBACKIPCR").show();
         $.get("ajax_14.e2e.php",
         {
            fn:"getIPCR",
            refid:refid
         },
         function(data,status){
            if (status=='success') {
               eval(data);
            }
         });
      }
   });
}
function refreshTable(emprefid) {
   $("#spGridTable").html("");
   $("#spGridTable").load("listRefresh.e2e.php",
   {
      EmployeesRefid : emprefid
   },
   function(responseTxt, statusTxt, xhr){
      if(statusTxt == "error")
         alert("Ooops Error: " + xhr.status + ": " + xhr.statusText);
         return false;
   });
}
function selectMe(emprefid) {
   $("#sint_EmployeesRefId").val(emprefid);
   $.get("EmpQuery.e2e.php",
   {
      emprefid:emprefid,
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUserRefId:$("#hUserRefId").val()
   },
   function(data,status) {
      $('#selectedEmployees').html("&nbsp;");
      if(status == "error") return false;
      if(status == "success"){
         var data = JSON.parse(data);
         try
         {
            setHTMLById("RefIdSelected",data.RefId);
            setValueById("hRefIdSelected",data.RefId);
            setValueByName("txtEmpId",data.AgencyId);
            setHTMLById("selectedEmployees",data.LastName + ", " + data.FirstName + " " + data.MiddleName);
            selectedItems(data.RefId,
                          data.LastName,
                          data.FirstName);
         }
         catch (e)
         {
            if (e instanceof SyntaxError) {
                alert(e.message);
            }
         }
      }
   });
}

function printIPCR(refid){
   $("#rptContent").attr("src","blank.htm");
   var rptFile = "rpt_IPCR_14";
   var url = "ReportCaller.e2e.php?file=" + rptFile;
   url += "&refid=" + refid;
   url += "&" + $("[name='hgParam']").val();
   $("#prnModal").modal();
   $("#rptContent").attr("src",url);
}