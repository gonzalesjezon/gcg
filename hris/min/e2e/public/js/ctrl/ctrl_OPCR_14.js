$(document).ready (function () {
   remIconDL();
   $("#insert_spms, #btnEDITIPCR, #btnBACKIPCR").hide();
   $("#btnINSERTSPMS").click(function () {
      $("#spmsList").hide();
      $("#insert_spms").show();
   });
   $("#btnCANCELIPCR, #btnBACKIPCR").click(function () {
      var prog = $("#hProg").val();
      gotoscrn(prog,"");
   });
   
   $("#btnPRINT").click (function () {
      var rptFile = "prnIPCR";
      var url = "ReportCaller.e2e.php?file=" + rptFile + "&";
      $("#prnModal").modal();
      $("#rptContent").attr("src",url);
   });
   $("#deleterow").click(function () {
      var last = "";
      $("[id*='row_']").each(function () {
         last = $(this).attr("id").split("_")[1];
      });
      if (last > 1) {
         $("#row_" + last).remove();   
      }
   });

   $("#addrow").click(function () {
      var last = "";
      var str = "";
      $("[id*='row_']").each(function () {
         last = $(this).attr("id").split("_")[1];
      });
      var idx = parseInt(last) + 1;
      str += '<tr id="row_' + idx + '">';
      str += '<td class="td-input">';
      str += '<input type="hidden" name="refid_'+ idx +'" id="refid_'+ idx +'" value="0">';
      str += '<input type="text" class="form-input" name="output_'+ idx +'" id="output_'+ idx +'">';
      str += '</td>';
      str += '<td class="td-input">';
      str += '<input type="text" class="form-input" name="si_'+ idx +'" id="si_'+ idx +'">';
      str += '</td>';
      str += '<td class="td-input">';
      str += '<input type="text" class="form-input" name="accomplishment_'+ idx +'" id="accomplishment_'+ idx +'">';
      str += '</td>';
      str += '<td class="td-input">';
      str += '<input type="text" class="form-input" name="q1_'+ idx +'" id="q1_'+ idx +'">';
      str += '</td>';
      str += '<td align="center">';
      str += '<input type="text" class="form-input" name="e2_'+ idx +'" id="e2_'+ idx +'">';
      str += '</td>';
      str += '<td align="center">';
      str += '<input type="text" class="form-input" name="t3_'+ idx +'" id="t3_'+ idx +'">';
      str += '</td>';
      str += '<td align="center">';
      str += '<input type="text" class="form-input" name="a4_'+ idx +'" id="a4_'+ idx +'">';
      str += '</td>';
      str += '<td class="td-input">';
      str += '<input type="text" class="form-input" name="remarks_'+ idx +'" id="remarks_'+ idx +'">';
      str += '</td>';
      str += '</tr>';
      $("#row_" + last +":last").after(str);
   });
   $("#btnSAVEIPCR, #btnEDITIPCR").click(function () {
      $(this).prop("disabled",true);
      $("#btnBACKIPCR, #btnCANCELIPCR").prop("disabled",true);
      $(this).html("Processing..");
   	var emprefid = $("#sint_EmployeesRefId").val();
   	if ($("#sint_Semester").val() == "") {
   		$.notify("Semester is Required");
   		return false;
   	}
   	if (emprefid == 0) {
   		$.notify("No Employee Selected");
   	} else {
         var count    = 0;
	      $("[id*='EntryIPCR_']").each(function () {
	         count++;
	      });
	      $("#count").val(count);

	      $.ajax({
		      url: "ajax_14.e2e.php",
		      type: "POST",
		      data: new FormData($("[name='xForm']")[0]),
		      success: function (responseTxt) {
		         eval(responseTxt);
		      },
		      enctype: 'multipart/form-data',
		      processData: false,
		      contentType: false,
		      cache: false
		   });	
   	}
   });
});
function afterSave(){
	alert("Successfully Save");
	gotoscrn ($("#hProg").val(), setAddURL());
}
function afterEdit() {
   alert("Successfully Update");
   gotoscrn ($("#hProg").val(), setAddURL());   
}
function viewInfo(refid,mode,dum){
   if (mode == 2) {
      getOPCR(refid);   
   } else if (mode == 3) {
      printIPCR(refid);
   }
   
}

function getOPCR(refid){
   $("#sint_IPCRRefId").val(refid);
   $.get("ajax_14.e2e.php",
   {
      fn:"getOPCR_detail",
      refid:refid
   },
   function(data,status){
      if (status=='success') {
         $("#canvas").html("");
         $("#canvas").html(data);
         $("[id*='delete_']").prop("disabled",true);
         $("#spmsList, #btnSAVEIPCR, #btnCANCELIPCR").hide();
         $("#insert_spms, #btnEDITIPCR, #btnBACKIPCR").show();
         $.get("ajax_14.e2e.php",
         {
            fn:"getOPCR",
            refid:refid
         },
         function(data,status){
            if (status=='success') {
               eval(data);
            }
         });
      }
   });
}


function printIPCR(refid){
   $("#rptContent").attr("src","blank.htm");
   var rptFile = "rpt_OPCR_14";
   var url = "ReportCaller.e2e.php?file=" + rptFile;
   url += "&refid=" + refid;
   url += "&" + $("[name='hgParam']").val();
   $("#prnModal").modal();
   $("#rptContent").attr("src",url);
}