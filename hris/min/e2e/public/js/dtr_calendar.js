$(document).ready(function () {
  $("#dtr").click(function () {
     $("#dtr_modal").modal(500);
  });
  $("#btnCancelDetails").click(function () {
     $(".details--").prop("disabled",true);
     $("#dtr_modal").modal("hide");
  });
   $(".time--").each(function(){
      $(this).keydown(function (e) {
          if(e.keyCode == 13) {
            var objName = $(this).attr("name");
            var kEntry = $(this).attr("kEntry");
            DTRSave(kEntry,objName);
            e.preventDefault();
            return false;
          }
      });
   });
  $("[id*='btnCancel_']").each(function () {
     $(this).click(function () {
         var punch = $(this).attr("punch");
         var CurrentTimeValue = $("#hCurrentTimeValue").val();
         $(this).prop("disabled",true);
         $("[name='entry_"+punch+"']").val(CurrentTimeValue);
         $("[id='btnSave_"+punch+"'],[name='entry_"+punch+"'], #DTRRemarks").prop("disabled",true);
         $("[id='btnEdit_"+punch+"']").prop("disabled",false);
         $("[id*='btnDelete_"+punch+"']").prop("disabled",false);
         $("#hCurrentTimeValue").val("");
     });
  });

  $("[id*='btnEdit_']").each(function () {
     $(this).click(function () {
        var punch = $(this).attr("punch");
        $(this).prop("disabled",true);
        $("[id*='btnDelete_"+punch+"']").prop("disabled",true);
        $("#hCurrentTimeValue").val($("[name='entry_"+punch+"']").val());
        $("[id='btnSave_"+punch+"'],[name='entry_"+punch+"'],[id='btnCancel_"+punch+"'], #DTRRemarks").prop("disabled",false);
        $("[name='entry_"+punch+"']").focus();
        $("[name='entry_"+punch+"']").select();
     });
  });

   $("[id*='btnSave_']").each(function () {
      $(this).click(function () {
         var punch = $(this).attr("punch");
         var kEntry = $(this).attr("idx");;
         var objName = "entry_" + punch;
         var remarks = $("#DTRRemarks").val();
         var value = $("[name='"+objName+"']").val();
         if (value == "") {
            $.notify("Cannot Save Empty Logs.");
            $("[name='"+objName+"']").focus();
            return false;
         } else {
            DTRSave(kEntry,objName,remarks);   
            $(this).prop("disabled",true);
            // if (remarks == "") {
            //    $.notify("Remark is required.");
            //    $("#DTRRemarks").focus();
            //    return false;
            // } else {
               
            // }   
         }
      });
   });
   $("[id*='btnCancel_'], [id*='btnSave_']").prop("disabled",true);
   $("#btnProcess, #btnReProcess").click(function () {
      var emprefid      = $("[name='txtRefId']").val();
      var month         = getValueByName("hNewMonth");
      var year          = getValueByName("hNewYear");
      var companyID     = $("[name='hCompanyID']").val();
      var dateSelected  = $("#DateSelected").html();
      var hUser         = $("#hUser").val();
      var dummyMonth    = $("#hDummyMonth").val();
      var dummyYear     = $("#hDummyYear").val();
      if (month == 0) {
        DTRProcess(emprefid,dummyMonth,dummyYear);
      } else {
        DTRProcess(emprefid,month,year);  
      }
   });
   $("[id*='btnDelete_']").each(function () {
      $(this).click(function () {
         var this_date = $("#DateSelected").html();
         var punch = $(this).attr("punch");
         var kEntry = $(this).attr("idx");;
         var objName = "entry_" + punch;
         var value = $("[name='"+objName+"']").val();
         var emprefid = $("[name='txtRefId']").val();
         if (value == "") {
            $.notify("No Logs to delete");
            return false;
         } else {
            delete_logs(this_date,kEntry,objName,emprefid);
         }
      });
   });
   
});
function delete_logs(this_date,kentry,objname,emprefid) {
   $.get("DTRTrn.e2e.php",
      {  
         fn: "delete_logs",
         emprefid: emprefid,
         date:this_date,
         kentry:kentry,
         obj:objname
      },
   function(data,status) {
      if (status == "success") {
         try {
            eval(data);
         }
         catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
      }
   });
}
function DTRProcess(emprefid,month,year) {
   document.getElementById("overlay").style.display = "block";
   //alert(emprefid + " -> " + month + " ->" + year);
   $.get("DTRTrn.e2e.php",
      {  
         fn: "DTRProcess",
         emprefid: emprefid,
         month: month,
         year: year
      },
   function(data,status) {
      if (status == "success") {
         try {
            eval(data);
         }
         catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
      }
   });
}
function GetDTRSummary(emprefid,month,year,companyID,dateSelected,hUser) {
   //document.getElementById("overlay").style.display = "block";
   $.get("DTRTrn.e2e.php",
      {  
         fn: "GetDTRSummary",
         emprefid: emprefid,
         month: month,
         year: year
      },
   function(data,status) {
      if (status == "success") {
         try {
            eval(data);
         }
         catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
      }
   });
   /*$.get("DTRProcess.e2e.php",
      {  
         emprefid: emprefid,
         hNewMonth: month,
         hNewYear: year,
         hCompanyID: companyID,
         DateSelected: dateSelected,
         user: hUser
      },
   function(data,status) {
      //alert(data);
      //alert(status);
      if (status == "success") {
         try {
            eval(data);
         }
         catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
      }
   });*/
}
function DTRSave(kEntry,objName,remarks) {
   //alert($("[name='"+ objName +"']").attr("checktime"));
   //alert($("[name='"+ objName +"']").val());
   $.post("DTRTrn.e2e.php",
   {
       fn:"DTRSave",
       EmpRefId:$("[name='txtRefId']").val(),
       date:$("#hDate").val(),
       punch:$("[name='"+ objName +"']").val(),
       checktime:$("[name='"+ objName +"']").attr("checktime"),
       kEntry:kEntry,
       hUser:$("#hUser").val(),
       remarks:remarks
   },
   function(data,status) {
      if (status == "success") {
         try {
            eval(data);
            punch = objName.split("_")[1];
            $("[id*='btnEdit_'], [id*='btnDelete_']").prop("disabled",false);
            $("[id='btnCancel_" + punch + "'], [name='" + objName + "'], [id*='btnSave_" + punch + "']").prop("disabled",true);
            $("#DTRRemarks").prop("disabled",true);
         }
         catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
       }
   });

   //alert($("[name='"+ objName +"']").attr("checktime"));

}
function afterNewSave(LastRefId) {
   alert("New Record " + LastRefId + " Succesfully Inserted");
}
function afterEditSave(RefId){
   alert("Record " + RefId + " Succesfully Updated");
}
var datePickerDivID = "datepicker";
var iFrameDivID = "datepickeriframe";

var dayArrayShort = new Array('Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa');
var dayArrayMed = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
var dayArrayLong = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
var monthArrayShort = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
var monthArrayMed = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');
var monthArrayLong = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

// these variables define the date formatting we're expecting and outputting.
// If you want to use a different format by default, change the defaultDateSeparator
// and defaultDateFormat variables either here or on your HTML page.
var defaultDateSeparator = "/";        // common values would be "/" or "."
var defaultDateFormat = "mdy"    // valid values are "mdy", "dmy", and "ymd"
var dateSeparator = defaultDateSeparator;
var dateFormat = defaultDateFormat;
var id = document.getElementById;

function displayDatePicker(dateFieldName, displayBelowThisObject, dtFormat, dtSep)
{
  var targetDateField = document.getElementsByName (dateFieldName).item(0);

  // if we weren't told what node to display the datepicker beneath, just display it
  // beneath the date field we're updating
  if (!displayBelowThisObject)
    displayBelowThisObject = targetDateField;
  // if a date separator character was given, update the dateSeparator variable
  if (dtSep)
    dateSeparator = dtSep;
  else
    dateSeparator = defaultDateSeparator;
  // if a date format was given, update the dateFormat variable
  if (dtFormat) dateFormat = dtFormat;
           else dateFormat = defaultDateFormat;

  var x = displayBelowThisObject.offsetLeft;
  var y = displayBelowThisObject.offsetTop + displayBelowThisObject.offsetHeight ;

  // deal with elements inside tables and such
  var parent = displayBelowThisObject;
  while (parent.offsetParent) {
    parent = parent.offsetParent;
    x += parent.offsetLeft;
    y += parent.offsetTop ;
  }
  drawDatePicker(targetDateField, x, y);
}
/**
Draw the datepicker object (which is just a table with calendar elements) at the
specified x and y coordinates, using the targetDateField object as the input tag
that will ultimately be populated with a date.

This function will normally be called by the displayDatePicker function.
*/
function drawDatePicker(targetDateField, x, y)
{
  var dt = getFieldDate(targetDateField.value );

  // the datepicker table will be drawn inside of a <div> with an ID defined by the
  // global datePickerDivID variable. If such a div doesn't yet exist on the HTML
  // document we're working with, add one.
  if (!document.getElementById(datePickerDivID)) {
    // don't use innerHTML to update the body, because it can cause global variables
    // that are currently pointing to objects on the page to have bad references
    //document.body.innerHTML += "<div id='" + datePickerDivID + "' class='dpDiv'></div>";
    var newNode = document.createElement("div");
    newNode.setAttribute("id", datePickerDivID);
    newNode.setAttribute("class", "dpDiv");
    newNode.setAttribute("style", "visibility: hidden;");
    document.body.appendChild(newNode);
  }

  // move the datepicker div to the proper x,y coordinate and toggle the visiblity
  var pickerDiv = document.getElementById(datePickerDivID);
  //pickerDiv.style.position = "absolute";
  //pickerDiv.style.left = x + "px";
  //pickerDiv.style.top = y + "px";
  //pickerDiv.style.visibility = (pickerDiv.style.visibility == "visible" ? "hidden" : "visible");
  pickerDiv.style.visibility = "visible";
  //pickerDiv.style.display = (pickerDiv.style.display == "block" ? "none" : "block");
  pickerDiv.style.display = "block";

  //pickerDiv.style.zIndex = 1;

  // draw the datepicker table
  refreshDatePicker(targetDateField.name, dt.getFullYear(), dt.getMonth(), dt.getDate());
}

function getAttendance(idx,obj) {
   if (obj[idx] == undefined) {
      return '<b class="NOENTRY">NO ENTRY</b>';
   } else {
      return '<strong>' + obj[idx] + '</strong>';
   }
}

function fillAttendance(year,month,xday) {
    var selected = $("#SelectedEMP").val();
    $("[name='hNewMonth']").val(month + 1);
    $("[name='hNewYear']").val(year);
    
    if (selected != "" || selected != undefined) {
      EmpRefId = selected;
    } else {
      if ($("[name='hUserGroup']").val() != "COMPEMP") {
        EmpRefId = $("[name='txtRefId']").val();
      } else {
        EmpRefId = $("[name='hEmpRefId']").val();
      }   
    }
   

   $.get("DTRTrn.e2e.php",
   {
      fn:"GetMonthAttendance",
      Month:month,
      Year:year,
      LastDay:xday,
      EmpRefId:EmpRefId,
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUserRefId:$("#hUserRefId").val()
   },
   function(data,status) {
      if (status == "success") {
         try {
            eval(data);
         }
         catch (e) {
            if (e instanceof SyntaxError) {
              alert(e.message);
            }
         }
      }
   });
}


/**
This is the function that actually draws the datepicker calendar.
*/
function refreshDatePicker(dateFieldName, year, month, day) {

   // if no arguments are passed, use today's date; otherwise, month and year
   // are required (if a day is passed, it will be highlighted later)
   var thisDay = new Date();

   if ((month >= 0) && (year > 0)) {
    thisDay = new Date(year, month, 1);
   } else {
    day = thisDay.getDate();
    thisDay.setDate(1);
   }


   // the calendar will be drawn as a table
   // you can customize the table elements with a global CSS style sheet,
   // or by hardcoding style and formatting elements below
   var crlf = "\r\n";
   var TABLE = "<table cols=7 class='dpTable' border=1>" + crlf;
   var xTABLE = "</table>" + crlf;
   var TR = "<tr class='dpTR'>";
   var TR_title = "<tr class='dpTitleTR'>";
   var TR_days = "<tr class='dpDayTR'>";
   var TR_todaybutton = "<tr class='dpTodayButtonTR'>";
   var xTR = "</tr>" + crlf;
   var TD = "<td class='dpTD' onMouseOut='this.className=\"dpTD\";' onMouseOver=' this.className=\"dpTDHover\";' ";
   var TD_title = "<td colspan=5 class='dpTitleTD'>";
   var TD_buttons = "<td class='dpButtonTD'>";
   var TD_todaybutton = "<td colspan=7 class='dpTodayButtonTD'>";
   var TD_days = "<td class='dpDayTD'>";
   var xTD = "</td>" + crlf;
   var DIV_title = "<div class='dpTitleText'>";
   var DIV_selected = "<div class='dpDayHighlight'>";
   var xDIV = "</div>";
   var EmpRefId = 3;

   //data = "{}";
   var html = TABLE;
   // this is the title bar, which displays the month and the buttons to
   // go back to a previous month or forward to the next month
   html += TR_title;
   html += TD_buttons + getButtonCode(dateFieldName, thisDay, -1, "&lt;") + xTD;
   html += TD_title + DIV_title + monthArrayLong[ thisDay.getMonth()] + " " + thisDay.getFullYear() + xDIV + xTD;
   html += TD_buttons + getButtonCode(dateFieldName, thisDay, 1, "&gt;") + xTD;
   html += xTR;

   // this is the row that indicates which day of the week we're on
   html += TR_days;
   for(i = 0; i < dayArrayMed.length; i++)
    html += TD_days + dayArrayMed[i] + xTD;
   html += xTR;

   // now we'll start populating the table with days of the month
   html += TR;
   xdata = "{}";
   // first, the leading blanks
   for (i = 0; i < thisDay.getDay(); i++)
    html += TD + "&nbsp;" + xTD;
   // now, the days of the month
   do {
    dayNum = thisDay.getDate();
    monthNum = thisDay.getMonth() + 1;
    if (dayNum <= 9) dayNum = "0" + dayNum;
    if (monthNum <= 9) monthNum = "0" + monthNum;
    FullDate = thisDay.getFullYear() + "-" + monthNum + "-" + dayNum;
    var daycontent = '';
        daycontent += '<div class="dayNum" title="' + FullDate + '">' + dayNum + '</div>';
        daycontent += '<div>';
        daycontent += '<div style="text-align:left;color:blue;font-weight:600;"></div>';
        daycontent += '<div class="row"><div class="col-xs-6 txt-right">TIME IN :</div><div class="col-xs-6 txt-left" checktime="" id="TI_'+FullDate+'">' + getAttendance("TI_" + FullDate,xdata) + '</div></div>';
        daycontent += '<div class="row"><div class="col-xs-6 txt-right">LUNCH OUT :</div><div class="col-xs-6 txt-left" checktime="" id="LO_'+FullDate+'">' + getAttendance("LO_" + FullDate,xdata) + '</div></div>';
        daycontent += '<div class="row"><div class="col-xs-6 txt-right">LUNCH IN :</div><div class="col-xs-6 txt-left" checktime="" id="LI_'+FullDate+'">' + getAttendance("LI_" + FullDate,xdata) + '</div></div>';
        daycontent += '<div class="row"><div class="col-xs-6 txt-right">TIME OUT :</div><div class="col-xs-6 txt-left" checktime="" id="TO_'+FullDate+'">' + getAttendance("TO_" + FullDate,xdata) + '</div></div>';
        daycontent += '</div>';

     var chClass = 'dpDayHighlightTD';
     if (dayNum == day) chClass = 'dpTodayTD';


    html += "<td class='dayContent--' id='" + FullDate + "' style='padding:10px; width: 14.28%;' onclick='dayContent_Click(this.id);' ondblclick='dayContent_dblClick(this.id);' class='daycontent-- " + chClass + "'>";
    html += "<div class='row' id='content_" + FullDate + "'>";
    html += daycontent;
    html += "</div>";
    html += "</td>" + crlf;

    // if this is a Saturday, start a new row
    if (thisDay.getDay() == 6) html += xTR + TR;
    // increment the day
    thisDay.setDate(thisDay.getDate() + 1);

   } while (thisDay.getDate() > 1)

   // fill in any trailing blanks
   if (thisDay.getDay() > 0) {
    for (i = 6; i > thisDay.getDay(); i--)
      html += TD + "&nbsp;" + xTD;
   }
   html += xTR;

   // add a button to allow the user to easily return to today, or close the calendar
   var today = new Date();
   var todayString = "Today is " + dayArrayMed[today.getDay()] + ", " + monthArrayMed[ today.getMonth()] + " " + today.getDate();
   //html += TR_todaybutton + TD_todaybutton;
   //html += "<button class='dpTodayButton' onClick='setToday(\"" + dateFieldName + "\");'>Today</button> ";
   //html += "<button class='dpTodayButton' onClick='updateDateField(\"" + dateFieldName + "\");'>Close</button>";
   //html += xTD + xTR;

   // and finally, close the table
   html += xTABLE;
   document.getElementById("CalHolder").innerHTML = html;
   // add an "iFrame shim" to allow the datepicker to display above selection lists
   adjustiFrame();
   fillAttendance(year,month,dayNum);
}

function dayContent_Click(objID) {
   if ($("#hDate").val() != objID) {
      setValueByName("worksched","");
      setValueByName("DTRRemarks","");
      setValueByName("tdyHours","");
      setValueByName("tdyAbsent","");
      setValueByName("tdyLate1","");
      setValueByName("tdyUT1","");
      setValueByName("tdyLate2","");
      setValueByName("tdyUT2","");
      setValueByName("tdyOT","");
      setValueByName("TOTDays","");
      setValueByName("TOTHours","");
      setValueByName("TOTAbsent","");
      setValueByName("TOTLate","");
      setValueByName("TOTUT","");
      setValueByName("TOTOT","");
      setValueByName("DayEQ","");
      setValueByName("HoursEQ","");
      setValueByName("AbsentEQ","");
      setValueByName("LateEQ","");
      setValueByName("UTEQ","");
      setValueByName("OTEQ","");
      setValueByName("VLEarned","");
      setValueByName("SLEarned","");
      setValueByName("VLBal","");
      setValueByName("SLBal","");
      setValueByName("COC","");
   }
   $("#hDate").val(objID);
   $("#DateSelected").html(objID);
}

function dayContent_dblClick(objID) {
    
   if ($("[name='hUserGroup']").val() != "COMPEMP") {
      EmpRefId = $("[name='txtRefId']").val();
   } else {
      EmpRefId = $("[name='hEmpRefId']").val();
      return false;
   }
   $("#hDate").val(objID);
   $("#DateSelected").html(objID);
   $("[class*='dayContent--']").removeClass("bgIntroBlack");
   $("#" + objID).addClass("bgIntroBlack");
   $.get("DTRTrn.e2e.php",
   {
      fn:"FillDetails",
      date:objID,
      EmpRefId:EmpRefId,
      hCompanyID:$("#hCompanyID").val(),
      hBranchID:$("#hBranchID").val(),
      hEmpRefId:$("#hEmpRefId").val(),
      hUserRefId:$("#hUserRefId").val()

   },
   function(data,status) {
      if (status == "success") {
         try {
            $(".details--").val("");
            eval(data);
            $("#dtr_modal").modal();
            var emprefid      = EmpRefId;
            var month         = getValueByName("hNewMonth");
            var year          = getValueByName("hNewYear");
            var companyID     = $("[name='hCompanyID']").val();
            var dateSelected  = $("#DateSelected").html();
            var hUser         = $("#hUser").val();
            //GetDTRSummary(emprefid,month,year,companyID,dateSelected,hUser);
         }
         catch (e) {
            if (e instanceof SyntaxError) {
              alert(e.message);
            }
         }
      }
   });
}

function setToday(dateFieldName) {
   var d = new Date();
   var mm = d.getMonth() + 1;
   var dd = d.getDate();
   var dd = d.getDate();
   var yy = d.getFullYear();
   var dayString = "00" + dd
   var monthString = "00" + mm
   dayString = dayString.substring(dayString.length - 2);
   monthString = monthString.substring(monthString.length - 2);
   var dtstr = monthString + "/" +  dayString + "/" + yy;
   updateDateField(dateFieldName,dtstr);
}
/**
Convenience function for writing the code for the buttons that bring us back or forward
a month.
*/
function getButtonCode(dateFieldName, dateVal, adjust, label)
{
  var newMonth = (dateVal.getMonth () + adjust) % 12;
  var newYear = dateVal.getFullYear() + parseInt((dateVal.getMonth() + adjust) / 12);
  if (newMonth < 0) {
    newMonth += 12;
    newYear += -1;
  }
  $("#hNewMonth").val(newMonth);
  $("#hNewYear").val(newYear);
  return "<button class='btn-cls4-tree' style='width:90%' onClick='refreshDatePicker(\"" + dateFieldName + "\", " + newYear + ", " + newMonth + ");'>" + label + "</button>";
}


/**
Convert a JavaScript Date object to a string, based on the dateFormat and dateSeparator
variables at the beginning of this script library.
*/
function getDateString(dateVal)
{
  var dayString = "00" + dateVal.getDate();
  var monthString = "00" + (dateVal.getMonth()+1);
  dayString = dayString.substring(dayString.length - 2);
  monthString = monthString.substring(monthString.length - 2);

  switch (dateFormat) {
    case "dmy" :
      return dayString + dateSeparator + monthString + dateSeparator + dateVal.getFullYear();
    case "ymd" :
      return dateVal.getFullYear() + dateSeparator + monthString + dateSeparator + dayString;
    case "mdy" :
    default :
      return monthString + dateSeparator + dayString + dateSeparator + dateVal.getFullYear();
  }
}


/**
Convert a string to a JavaScript Date object.
*/
function getFieldDate(dateString)
{
  var dateVal;
  var dArray;
  var d, m, y;

  try {
    dArray = splitDateString(dateString);
    if (dArray) {
      switch (dateFormat) {
        case "dmy" :
          d = parseInt(dArray[0], 10);
          m = parseInt(dArray[1], 10) - 1;
          y = parseInt(dArray[2], 10);
          break;
        case "ymd" :
          d = parseInt(dArray[2], 10);
          m = parseInt(dArray[1], 10) - 1;
          y = parseInt(dArray[0], 10);
          break;
        case "mdy" :
        default :
          d = parseInt(dArray[1], 10);
          m = parseInt(dArray[0], 10) - 1;
          y = parseInt(dArray[2], 10);
          break;
      }
      dateVal = new Date(y, m, d);
    } else if (dateString) {
      dateVal = new Date(dateString);
    } else {
      dateVal = new Date();
    }
  } catch(e) {
    dateVal = new Date();
  }

  return dateVal;
}
/**
Try to split a date string into an array of elements, using common date separators.
If the date is split, an array is returned; otherwise, we just return false.
*/
function splitDateString(dateString)
{
  var dArray;
  if (dateString.indexOf("/") >= 0)
    dArray = dateString.split("/");
  else if (dateString.indexOf(".") >= 0)
    dArray = dateString.split(".");
  else if (dateString.indexOf("-") >= 0)
    dArray = dateString.split("-");
  else if (dateString.indexOf("\\") >= 0)
    dArray = dateString.split("\\");
  else
    dArray = false;
  return dArray;
}
function updateDateField(dateFieldName, dateString)
{
  var targetDateField = document.getElementsByName (dateFieldName).item(0);
  if (dateString)
    targetDateField.value = dateString;

  var pickerDiv = document.getElementById(datePickerDivID);
  pickerDiv.style.visibility = "hidden";
  pickerDiv.style.display = "none";

  adjustiFrame();
  targetDateField.focus();
  // after the datepicker has closed, optionally run a user-defined function called
  // datePickerClosed, passing the field that was just updated as a parameter
  // (note that this will only run if the user actually selected a date from the datepicker)
  if ((dateString) && (typeof(datePickerClosed) == "function"))
    datePickerClosed(targetDateField);
}
function adjustiFrame(pickerDiv, iFrameDiv)
{
  // we know that Opera doesn't like something about this, so if we
  // think we're using Opera, don't even try
  var is_opera = (navigator.userAgent.toLowerCase().indexOf("opera") != -1);
  if (is_opera)
    return;

  // put a try/catch block around the whole thing, just in case
  try {
    if (!document.getElementById(iFrameDivID)) {
      // don't use innerHTML to update the body, because it can cause global variables
      // that are currently pointing to objects on the page to have bad references
      //document.body.innerHTML += "<iframe id='" + iFrameDivID + "' src='javascript:false;' scrolling='no' frameborder='0'>";
      var newNode = document.createElement("iFrame");
      newNode.setAttribute("id", iFrameDivID);
      newNode.setAttribute("src", "javascript:false;");
      newNode.setAttribute("scrolling", "no");
      newNode.setAttribute ("frameborder", "0");
      document.body.appendChild(newNode);
    }

    if (!pickerDiv)
      pickerDiv = document.getElementById(datePickerDivID);
    if (!iFrameDiv)
      iFrameDiv = document.getElementById(iFrameDivID);

    try {
      iFrameDiv.style.position = "absolute";
      iFrameDiv.style.width = pickerDiv.offsetWidth;
      iFrameDiv.style.height = pickerDiv.offsetHeight ;
      iFrameDiv.style.top = pickerDiv.style.top;
      iFrameDiv.style.left = pickerDiv.style.left;
      iFrameDiv.style.zIndex = pickerDiv.style.zIndex - 1;
      iFrameDiv.style.visibility = pickerDiv.style.visibility ;
      iFrameDiv.style.display = pickerDiv.style.display;
    } catch(e) {
    }

  } catch (ee) {
  }
}
/*--------------------------------------------
 Date Validation :
--------------------------------------------*/
  // Declaring valid date character, minimum year and maximum year
  var dtCh= "/";
  var minYear=1900;
  var maxYear=2100;
function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}
function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}
function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   }
   return this
}
function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
    if (dtStr!="") {
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("The month of the date is must be from 1 - 12. (mm/dd/yyyy)")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Day in Month is invalid")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
	}
return true
}
function ValidateDate(objdate){
	if (isDate(objdate.value)==false) {
		objdate.focus();
        objdate.value='';
		return false;
	}
    return true;
}
/*
ELSE IF chHTMLTagType = "function_compareDate" THEN DO:
   ASSIGN
'function compareDate(dt1,dt2) ~{'
  'ndt1 = new Date();'
  'ndt2 = new Date();'
  'ndt1 = dt1.replace("/","");'
  'ndt2 = dt2.replace("/","");'
  'if (ndt1>ndt2) return 1;'
            'else return 0;'
'~}'.
END.
ELSE IF chHTMLTagType = "function_getListDateCriteria" THEN DO:
   ASSIGN
  'function getListDateCriteria(objChkBox,objDate1,objDate2) ~{'
   'if (objChkBox.checked==true) ~{'
   	'if (objDate1.value!="" && objDate2.value!="") ~{'
   	    'if (compareDate(String(objDate1.value),String(objDate2.value))==0) ~{'
   		   'cl+="Y!"+objDate1.value+"!"+objDate2.value+"|";'
   		'~}'
   		'else ~{'
   		    'cl+="N!!|";'
  			'alert("Date Range Error: " + objDate1.value + " - " + objDate2.value)'
   		    'return 1;'
   		'~}'
   	'~}'
   	'else ~{ cl+="Y!"+objDate1.value+"!"+objDate2.value+"|"; ~}'
   '~}'
   'else ~{ cl+="N!!|"; ~}'
   'return 0;'
  '~}'.
END.
*/
var doc=document.MyForm;
var cl="";
function compareDate(dt1,dt2) {
  if (Date.parse(dt1) > Date.parse(dt2)) return 1;
                                    else return 0;
}
function getListDateCriteria(objChkBox,objDate1,objDate2,chLabel,p) {
   if (objChkBox.checked==true) {
   	if (objDate1.value!="" && objDate2.value!="") {
   	    if (compareDate(String(objDate1.value),String(objDate2.value))==0) {
   		   cl+="Y!"+objDate1.value+"!"+objDate2.value+"|";
   		}
   		else {
   		    cl+="N!!|";
  			alert(chLabel + "\nDate Range Error: " + objDate1.value + " - " + objDate2.value)
   		    return 1;
   		}
   	}
   	else { cl+="Y!"+objDate1.value+"!"+objDate2.value+"|"; }
   }
   else { cl+="N!!|"; }
   p.value = cl;
   return 0;
}



